﻿import { tOneNOV } from "./PTTS_DS21";

export interface DS1984 {
    [x: string]: any;
    ID: number;
    ReceiptNumber: string;
    District: string;
    ReceivingOfficerName: string;
    ReceivingOfficerTitle: string;
    BoxNumberedFrom: number;
    BoxNumberedTo: number;
    ReceivingOfficerSignature: string;
    DistributorName: string;
    DistributorSignature: string;
    DistributorSignedDate: Date;
    TaxRegNumber: string;
    FormStatus: string;
    AcknowledgedBy: string;
    ModifiedOn: Date;
    ModifiedBy: string;
    RecOfficerRefNo: string;
    DistributorRefNo: string;
    DS1983ReceiptNo: string;
    DistrictId: number;
    CreatedBy: string;
    CreatedDate: Date;
    Paycode: string;
    IsVoid: boolean;

}

export interface efDS1984 {
    ID: number;
    ReceiptNumber: string;
    District: string;
    ReceivingOfficerName: string;
    ReceivingOfficerTitle: string;
    BoxNumberedFrom: number;
    BoxNumberedTo: number;
    ReceivingOfficerSignature: string;
    DistributorName: string;
    DistributorSignature: string;
    DistributorSignedDate: Date;
    TaxRegNumber: string;
    FormStatus: string;
    AcknowledgedBy: string;
    ModifiedOn: Date;
    ModifiedBy: string;
    RecOfficerRefNo: string;
    DistributorRefNo: string;
    DS1983ReceiptNo: string;
    DistrictId: number;
    CreatedBy: string;
    CreatedDate: string;
    Paycode: string;
    IsVoid: boolean;


}

export enum DS1984Actions {
    ADD_DS1984 = "ADD_DS1984",
    ADD_NOVs = "TWENTY_NOVS",
    UPDATE_DS1984 = "UPDATE_DS1984",
    DELETE_DS1984 = "DELETE_DS1984",
    COMPLETE_DS1984 = "COMPLETE_DS1984",
    REVERT2INCOMPLETE_DS1984 = "REVERT2INCOMPLETE_DS1984",
    RESET_DS1984 = "RESET_DS1984List",
}

interface DS1984ActionType<T, P> {
    type: T;
    payload: P;
}

export type DS1984Action =
    | DS1984ActionType<typeof DS1984Actions.ADD_DS1984, DS1984>
    | DS1984ActionType<typeof DS1984Actions.ADD_NOVs, tOneNOV[]>
    | DS1984ActionType<typeof DS1984Actions.COMPLETE_DS1984, number>
    | DS1984ActionType<typeof DS1984Actions.REVERT2INCOMPLETE_DS1984, number>
    | DS1984ActionType<typeof DS1984Actions.UPDATE_DS1984, DS1984>
    | DS1984ActionType<typeof DS1984Actions.DELETE_DS1984, number>
    ;
