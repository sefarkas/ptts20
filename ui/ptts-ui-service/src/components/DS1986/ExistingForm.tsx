﻿import DateFnsUtils from "@date-io/date-fns"; // https://github.com/date-fns/date-fns/issues/1734
import { ErrorMessage } from "@hookform/error-message";
import { Button, FormControl, InputLabel, Link, MenuItem, Select } from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useEffect, useLayoutEffect, useState } from "react"; // , useReducer
import { Controller, useForm } from "react-hook-form"; // , FormProvider
import { isNullOrUndefined } from 'util';
import { history } from "../../configureStore";
import { DS1986, DS1986problems, tSpecialDistrict, UsedTicketRange } from '../../model';
import { allServices } from '../../services';
import { PTTSfunctions } from '../basicComponents';
import { ErrorMessageContainer, ErrorSummary } from '../basicComponents/validation';

type FormData = {
	ds1986id: string;
};

export function ExistingDS1986Form(props: any) {
	//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props);
	//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props.match.params.onercpt);

	const [isLoading, setLoading] = React.useState(false);

	const [lastgoodID, setlastgoodID] = useState("");

	const [oneDS1986form, setoneDS1986form] = useState<DS1986>(
		{
			ID: 0,
			ReferenceNo: "",
			Name: "",
			District: "",
			BadgeNo: "",
			NovasNo: "",
			Ds1984No: "",
			DS1986ErrorId: 0,
			DS1986BDate: new Date(),
			OtherRemarks: "",
			Remarks: "",
			CreatedBy: "",
			CreatedDate: new Date(),
			ModifiedBy: "",
			ModifiedDate: new Date(),
			DS1984District: 0,
			ReceiptNo: "",
			DS1984DistrictName: "",
		});

	const [IsAlreadyUsedTktRangeState, setIsAlreadyUsedTktRangeState] = useState(false);

	const [IDState, setIDState] = useState(props.match.params.oneid);
	const [ReferenceNoState, setReferenceNoState] = useState("");
	const [NameState, setNameState] = useState("");
	const [DistrictState, setDistrictState] = useState("");
	const [BadgeNoState, setBadgeNoState] = useState("");
	const [NovasNoState, setNovasNoState] = useState("00000000");
	const [Ds1984NoState, setDs1984NoState] = useState("");
	const [DS1986ErrorIdState, setDS1986ErrorIdState] = useState(11);
	const [DS1986BDateState, setDS1986BDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [OtherRemarksState, setOtherRemarksState] = useState("");
	const [RemarksState, setRemarksState] = useState("");
	const [CreatedByState, setCreatedByState] = useState("");
	const [CreatedDateState, setCreatedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [ModifiedByState, setModifiedByState] = useState("");
	const [ModifiedDateState, setModifiedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [DS1984DistrictIdState, setDS1984DistrictIdState] = useState(0);
	const [ReceiptNoState, setReceiptNoState] = useState("");
	const [DS1984DistrictNameState, setDS1984DistrictNameState] = useState("PTTS expects a choice for District DS-1984.");

	const [IsDS1984ReceiptState, setIsDS1984ReceiptState] = useState(isNullOrUndefined(Ds1984NoState) ? false : true);

	let dummyDSspecialDistrict: tSpecialDistrict = {
		Id: 0,
		PayCode: "",
		Description: "",
		Remarks: "",
		SplinterDisrtict: false,
		MappingDistictNTI: "",
	}

	const dummyProblemList: DS1986problems = {
		Id: 0,
		Code1: "",
		Description: "",
		Type: "",
		OrderSequence: 0,
	}

	const [dsList, setdsList] = useState([dummyDSspecialDistrict]); // list of district names
	const [dsProblemList, setdsProblemList] = useState([dummyProblemList]); // list of problem types being reported
	let dsDistrictMenuItemJSX: any | undefined = undefined; // JSX of menu items
	let dsProblemMenuItemJSX: any | undefined = undefined; // JSX of menu items

	function updateModifiedInfo() {
		if (ModifiedDateState.valueOf !== PTTSfunctions.Commonfunctions.todaysdate) {
			setModifiedDateState(PTTSfunctions.Commonfunctions.todaysdate);
		}

		const curUser: any = localStorage.getItem('user');
		const j: any = JSON.parse(curUser);
		const loggedInUser: string = j.data.userPreferences.userName;
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Modified by ", loggedInUser);
		setModifiedByState(loggedInUser);

	};

	const handleOnChange = (e: any) => {
		// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handleOnChange e is ");
		// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e)
		//   if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
		switch (e.target.name) {
			//case "fldID": setIDState(e.target.value); break;
			case "fldReferenceNo": {
				const lclRefNo = e.target.value;
				if (parseInt(lclRefNo) > 0) {
					if (lclRefNo.toString().length === 7) {
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("REF#", lclRefNo);
						(async () => {
							setLoading(true);

							const response: any = await
								allServices.EISgetServices.EISemployeeDetailsRN(lclRefNo); // axios always returns an object with a "data" child
							const rdata: any = response.data;
							if (!rdata) {
								 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("REF# lookup in-progress.");
								setLoading(true);
							}
							else {
								 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("REF# lookup found data to evaluate.");
								 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
								if (PTTSfunctions.Commonfunctions2.isNotDefined(rdata.employeeModel)) { // employeeModel is a branch in the returned JSON
									setReferenceNoState("Invalid REF#: " + lclRefNo);
								}
								else {
									try {

										if (isNullOrUndefined(rdata.jobModel.dsnyInformation)) { throw "rdata.jobModel.dsnyInformation missing" };
										let eisPayCodeCorrelID: string = rdata.jobModel.dsnyInformation.payCodeId.correlation
										eisPayCodeCorrelID = eisPayCodeCorrelID.substring(eisPayCodeCorrelID.length - 4);
										setDistrictState( // this is an integral part of the DS-1984 Receipt# that will be composed by UID
											// actual paycode may be different; real one needs a lookup using this CorrelationID string
											eisPayCodeCorrelID
										);
										if (isNullOrUndefined(rdata.employeeModel)) { throw "rdata.employeeModel missing" };
										setNameState(rdata.employeeModel.names[0].firstName.substring(0, 1)
											+ ' ' + rdata.employeeModel.names[0].lastName);
										setBadgeNoState(isNullOrUndefined(rdata.jobModel.badgeNumber)
											? "NA"
											: rdata.jobModel.badgeNumber.badgeNumber)
										setReferenceNoState(lclRefNo);
									}
									catch (e) {
										alert(e.toString() + " using (" + lclRefNo + ").");
										setBadgeNoState("type one in later");
									}

								}
								setLoading(false);
							}
						})();

					}
					else {
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("numeric but not 7 digits -- officer")
					}
				}
				else {
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("non-numeric string");
				};
				break;
			}
			//case "fldName": setNameState(e.target.value); break;
			//case "fldDistrict": {
			//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handleOnChange fldDistrict");
			//	setDistrictState(e.target.value);
			//	break;
			//}
			case "fldBadgeNo": setBadgeNoState(e.target.value); break;
			case "fldNovasNo": setNovasNoState(e.target.value); break;
			case "fldDs1984No": setDs1984NoState(e.target.value); break;
			case "fldDS1986ErrorId": {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handleOnChange fldDS1986ErrorId");
				if (e.target.value > 0) {
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fldDS1986ErrorId", e.target.value);
					setDS1986ErrorIdState(e.target.value);
				}
				else {
					setDS1986ErrorIdState(11);
				}
				break;
			}
			case "fldDS1986BDate": setDS1986BDateState(e.target.value); break;
			case "fldOtherRemarks": setOtherRemarksState(e.target.value); break;
			case "fldRemarks": setRemarksState(e.target.value); break;
			//case "fldCreatedBy": setCreatedByState(e.target.value); break;
			//case "fldCreatedDate": {
			//	setCreatedDateState(e.target.value.toString());
			//	break;
			//}
			//case "fldModifiedBy": setModifiedByState(e.target.value); break;
			//case "fldModifiedDate": setModifiedDateState(e.target.value); break;
			//case "fldDS1984District": {
			//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handleOnChange fldDS1984District");
			//	setDS1984DistrictIdState(e.target.value);
			//	break;
			//}
			//case "fldReceiptNo": setReceiptNoState(e.target.value); break;
			//case "fldDS1984DistrictName": setDS1984DistrictIdState(e.target.value); break;

			default:
				// code block
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.value);
		}

		updateModifiedInfo()

	}

	const handleOnDateChange = (e: any) => {
		setCreatedDateState(e.toString());

		updateModifiedInfo()
	}

	const handleDropDownChangeErrorId = (event: any, fld: string) => {

		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handleDropDownChange for ", fld);
		switch (fld) {
			case "fldDS1986ErrorId": {
				// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fld", fld);
				//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(dsProblemList);
				// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("ErrorID: ", event.target.innerText);
				if (event.target.innerText !== undefined) {
					let strID: string = event.target.innerText.substr(0, 4);
					// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("in-progress strID to be saved: ", strID);
					strID = strID.substr(1, 2); // second character, length of two
					// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Error strID to be saved: ", strID);
					const intID: number = Number.parseInt(strID);
					if (!intID) {
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Error intID to be saved: ", intID);
					}
					else {
						// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Expected problem type: ", dsProblemList[intID - 11].Description);
						if (intID !== DS1986ErrorIdState) {
							(async () => {
								await setDS1986ErrorIdState(intID);
							})();
						}
					}
				}
				break;
			}
			default:
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("default");
				// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(event.target);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fld", fld);
		}

		updateModifiedInfo()

		return;
	};


	const handleDropDownChangeDS1984District = (e: object, selectedId: any) => {

		if (selectedId.props.value > 0) {
			setDS1984DistrictIdState(selectedId.props.value);
		}
		else {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("selectedId is ", selectedId);
			alert("selectedId has unexpectd value.");
		}

		updateModifiedInfo()

		return;
	};


	useEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("initial useEffect triggered for " + IDState);
		if (!isNullOrUndefined(IDState)) {
			if (IDState.length > 0 && IDState !== lastgoodID) {
				setlastgoodID(IDState);
				(async () => {
					setLoading(true);
					const response = await allServices.PTTSgetServices.anDS1986(IDState);
					const rspJson = await response.data;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("api response for ", IDState);
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rspJson);
					if (!isNullOrUndefined(rspJson)) {
						if (rspJson.ID !== undefined) {
							setIDState(rspJson.ID);
							setReferenceNoState(rspJson.ReferenceNo);
							setNameState(rspJson.Name);
							setDistrictState(rspJson.District);
							setBadgeNoState(rspJson.BadgeNo);
							setNovasNoState(rspJson.NovasNo);
							setDs1984NoState(rspJson.Ds1984No);
							setDS1986ErrorIdState(rspJson.DS1986ErrorId);
							setDS1986BDateState(rspJson.DS1986BDate);
							setOtherRemarksState(rspJson.OtherRemarks);
							setRemarksState(rspJson.Remarks);
							setCreatedByState(rspJson.CreatedBy);
							setCreatedDateState(rspJson.CreatedDate);
							setModifiedByState(rspJson.ModifiedBy);
							setModifiedDateState(rspJson.ModifiedDate);
							setDS1984DistrictIdState(rspJson.DS1984District);
							setReceiptNoState(rspJson.ReceiptNo);
							setDS1984DistrictNameState(rspJson.DS1984DistrictName);

							const pIsAlreadyUsedTktRange: boolean = fIsAlreadyUsedTktRange("IsDS1984Tkt", "Low", rspJson.NovasNo);
							 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("init useEffect result: ", pIsAlreadyUsedTktRange);
							setIsAlreadyUsedTktRangeState(pIsAlreadyUsedTktRange);

						}
						else {
							alert("rspJson.ID is undefined");
                        }
					}
					else {
						alert("isNullOrUndefined(rspJson) is true");
					}
					setLoading(false);
				})();
			}
		}


	}, []);

	useEffect(() => {
		for (const objIn of dsList) {
			if (objIn.Id === DS1984DistrictIdState && objIn.Id > 0) {
				dummyDSspecialDistrict = objIn;
				break;
            }
        }
		setDS1984DistrictNameState(
			!dummyDSspecialDistrict 
			? "PTTS cannot save until a responsible District is chosen."
			: dummyDSspecialDistrict.MappingDistictNTI > ""
					? dummyDSspecialDistrict.MappingDistictNTI
					: dummyDSspecialDistrict.Description

			// !oneChoice.MappingDistictNTI ? oneChoice.Description : oneChoice.MappingDistictNTI
		);
	}, [DS1984DistrictIdState])

	useEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useEffect triggered for " + lastgoodID);
		if (!isNullOrUndefined(IDState)) {
			if (IDState.length > 0 && IDState !== lastgoodID) {
				setlastgoodID(IDState);
				(async () => {
					setLoading(true);
					const response = await allServices.PTTSgetServices.anDS1986(IDState);
					const rspJson = await response.data;
					if (!isNullOrUndefined(rspJson)) {
						if (rspJson.BoxNumberedFrom !== undefined) {
							setoneDS1986form({
								ID: rspJson.ID,
								ReferenceNo: rspJson.ReferenceNo,
								Name: rspJson.Name,
								District: rspJson.District,
								BadgeNo: rspJson.BadgeNo,
								NovasNo: rspJson.NovasNo,
								Ds1984No: rspJson.Ds1984No,
								DS1986ErrorId: rspJson.DS1986ErrorId,
								DS1986BDate: rspJson.DS1986BDate,
								OtherRemarks: rspJson.OtherRemarks,
								Remarks: rspJson.Remarks,
								CreatedBy: rspJson.CreatedBy,
								CreatedDate: rspJson.CreatedDate,
								ModifiedBy: rspJson.ModifiedBy,
								ModifiedDate: rspJson.ModifiedDate,
								DS1984District: rspJson.DS1984District,
								ReceiptNo: rspJson.ReceiptNo,
								DS1984DistrictName: rspJson.DS1984DistrictName,
							});
						}
					}
					setLoading(false);

				})();
			}
		}
	}, [lastgoodID]);

	useLayoutEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ds1984NoState, DS1986ErrorIdState useEffect started.");
		if ((!dsDistrictMenuItemJSX && !DS1984DistrictIdState) || (!dsProblemMenuItemJSX && DS1986ErrorIdState < 12)) {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Build the two dropdown lists.");
			(async () => {
				const response = await allServices.PTTSgetServices.refSpecialDistricts(); // axios always returns an object with a "data" child
				const rdata: tSpecialDistrict[] = response.data;

				const response2 = await allServices.PTTSgetServices.refProblemList(); // axios always returns an object with a "data" child
				const rdata2: DS1986problems[] = response2.data;

				if (!rdata || !rdata2) {
					setLoading(true);
				}
				else {
					const rdataDesc: tSpecialDistrict[] = rdata.sort(function (a, b) {
						var nameA = !a.Description ? "" : a.Description.toUpperCase(); // ignore upper and lowercase
						var nameB = !b.Description ? "" : b.Description.toUpperCase(); // ignore upper and lowercase
						if (nameA < nameB) {
							return -1;
						}
						if (nameA > nameB) {
							return 1;
						}
						// names must be equal
						return 0;
					}).slice(0, 150); // ASC
					await setdsList(rdataDesc);
					dsDistrictMenuItemJSX = makeDistrictMenuItemJSX();

					const rdataDesc2: DS1986problems[] = rdata2.sort(function (a, b) {
						var nameA = !a.OrderSequence ? 0 : a.OrderSequence; // ignore upper and lowercase
						var nameB = !b.OrderSequence ? 0 : b.OrderSequence; // ignore upper and lowercase
						if (nameA < nameB) {
							return -1;
						}
						if (nameA > nameB) {
							return 1;
						}
						// names must be equal
						return 0;
					}).slice(0, 10); // ASC
					await setdsProblemList(rdataDesc2);
					dsProblemMenuItemJSX = makeProblemListMenu();

				}
			})();
		}

		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ds1984NoState, DS1986ErrorIdState useEffect finished.");
	},
		[Ds1984NoState, DS1986ErrorIdState]
	);

	useEffect(() => {
		(async () => {
			if (Ds1984NoState.length === 22) {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useLayoutEffect runs IsAlreadyUsedTktRange for ", NovasNoState);
				const pIsAlreadyUsedTktRange: boolean = await fIsAlreadyUsedTktRange("IsDS1984Tkt", "Low", NovasNoState);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useEffect result for pIsDS1984Receipt: ", pIsAlreadyUsedTktRange);
				setIsAlreadyUsedTktRangeState(pIsAlreadyUsedTktRange);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("after setIsAlreadyUsedTktRange: ", IsAlreadyUsedTktRangeState);
				setLoading(pIsAlreadyUsedTktRange);
			}
			else {
				setIsAlreadyUsedTktRangeState(false);
			}
		})();
	}, [NovasNoState, Ds1984NoState]);

	const { register, handleSubmit, errors, formState } = useForm<FormData>();

	const defaultValues = {
		Native: "",
		TextField: "",
		Select: "",
		ReactSelect: "",
		Checkbox: false,
		switch: false,
		RadioGroup: ""
	};

	const { control } = useForm({ defaultValues });

	const submitForm = async (data: FormData, e: any) => {
		if (DistrictState.length < 3) {
			alert("PTTS expects you to pick the location you delivered to.");
		}
		else if (DS1986ErrorIdState < 12) {
			alert("PTTS expects you to categorize the problem.");
		}
		else {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DistrictState is ", DistrictState);
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS1986ErrorIdState is ", DS1986ErrorIdState);
			const oneDS1986form: DS1986 = { // take the individual hook const and set each oneDS1986form object properties
				ID: IDState,
				ReferenceNo: ReferenceNoState,
				Name: NameState,
				District: DistrictState,
				BadgeNo: BadgeNoState,
				NovasNo: NovasNoState,
				Ds1984No: Ds1984NoState,
				DS1986ErrorId: DS1986ErrorIdState,
				DS1986BDate: new Date(DS1986BDateState),
				OtherRemarks: OtherRemarksState,
				Remarks: RemarksState,
				CreatedBy: CreatedByState,
				CreatedDate: new Date(CreatedDateState),
				ModifiedBy: ModifiedByState,
				ModifiedDate: new Date(ModifiedDateState),
				DS1984District: DS1984DistrictIdState,
				ReceiptNo: ReceiptNoState,
				DS1984DistrictName: DS1984DistrictNameState,
			}
			try {

				const response = await allServices.PTTSiudServices.iudDS1986(oneDS1986form);
				// response is the new receipt number
				if (isNullOrUndefined(response.data) || response.data === "[object Object]") {
					// "An error occurred while executing the command definition. See the inner exception for details.|
					// DS1984 is not created for the specified ticket range.Please have Enf HQ create 1984 for this box. "
					const importantPart: string = response.data
						.ExceptionMessage.replace("An error occurred while executing the command definition. See the inner exception for details.|", "");
					alert("I am having trouble with saving DS1986 values.  " + importantPart);
					// any error message is in the CustomerOrderNo property in the controller
				}
				else {
					history.push('/PTTS/existingDS1986/' + response.data);
				}
			}
			catch (err) {
				// anything else
				alert("I am having trouble with saving DS1986 values.  " + err.toString());
				// return err;
			};

		}

		// e.target.reset(); // reset after form submit
	};


	const onGoToParent = () => {
		if (IsDS1984ReceiptState) {
			history.push('/PTTS/existingDS1984/' + Ds1984NoState);
		}
		else {
			alert("There is no DS-1984 " + { Ds1984NoState } + " associated with this DS-1986.");
		}
	};

	const onCancel = () => {
		history.push('/PTTS/home');
	};

	const newDS1986 = () => {
		history.push('/PTTS/newDS1986');
	};

	const classes = useStyles();

	function fIsAlreadyUsedTktRange(aDSform: string, LowHigh: string, TgtVal: string): boolean {
		let rdata: UsedTicketRange[] | undefined = undefined;
		let pIsAlreadyUsedTktRange: boolean = false;
		if (Ds1984NoState.length !== 22) {
			return false;
		}

		if (Ds1984NoState.length === 22 
			&& LowHigh === "Low"
			? Number.parseInt(TgtVal) > 0
			: Number.parseInt(TgtVal) > 0) {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Test a ticket range");
			const ECBfromState: string =
				TgtVal.length < 9
					? PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(Number.parseInt(TgtVal))
					: TgtVal
			const ECBtoState: string =
				TgtVal.length < 9
					? PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(Number.parseInt(TgtVal))
					: TgtVal;
			(async () => {
				const tryThisRange = (
					{
						ECBSummonsNumberFrom: LowHigh === "Low"
							? TgtVal
							: ECBfromState,
						ECBSummonsNumberTo: LowHigh === "High"
							? TgtVal
							: ECBtoState,
						DSform: aDSform,
						ReceiptNumber: Ds1984NoState,
					}
				);
				setLoading(true);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("tryThisRange");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(IsAlreadyUsedTktRangeState);
				pIsAlreadyUsedTktRange = false;
				const response = await allServices.PTTSgetServices.refUsedTicketNumbers(tryThisRange);
				// axios always returns an object with a "data" child
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("response");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(response);
				rdata = await response.data;
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("rdata");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
				if (!rdata) {
					pIsAlreadyUsedTktRange = false;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ticket is not in the range on the related DS-1984.");
				}
				else {
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("test the ticket range");
					pIsAlreadyUsedTktRange =
						!rdata[0]
							? false :
							!rdata[0].ReceiptNumber
								? false
								: true
					pIsAlreadyUsedTktRange =
						pIsAlreadyUsedTktRange ? rdata[0].DSform.substr(-4) === "OKay" : false;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange 1", pIsAlreadyUsedTktRange);
					pIsAlreadyUsedTktRange =
						pIsAlreadyUsedTktRange ? TgtVal >= rdata[0].ECBSummonsNumberFrom
							&& rdata[0].ECBSummonsNumberFrom > "0"
							: false;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange 2", pIsAlreadyUsedTktRange);
					pIsAlreadyUsedTktRange =
						pIsAlreadyUsedTktRange ? TgtVal <= rdata[0].ECBSummonsNumberTo : false;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange 3", pIsAlreadyUsedTktRange);

				}
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("IsAlreadyUsedTktRange at end of ASYNC");
				setIsAlreadyUsedTktRangeState(pIsAlreadyUsedTktRange);
				setLoading(false);
				return pIsAlreadyUsedTktRange;
			})();

		}
		else {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange outside of IF: ", pIsAlreadyUsedTktRange);
			setLoading(true);

		};
		return pIsAlreadyUsedTktRange;

	};

	function renderValidationMsgs() {
		return (
			<Grid item xs={12}>
				<ErrorSummary errors={errors} />
				{!IsAlreadyUsedTktRangeState && IsDS1984ReceiptState ? <div>NOV# {NovasNoState} is not in the ticket range of DS-1984(s) {Ds1984NoState}.</div> : <div>&nbsp;</div>}
				{!IsDS1984ReceiptState ? <div>DS-1984 Receipt Number does not exist.</div> : <div>&nbsp;</div>}
			</Grid>
		)
	}

	const renderFromDistrictMenuItemJSX = () => {
		if (isNullOrUndefined(dsDistrictMenuItemJSX) || dsDistrictMenuItemJSX.length < 2) {
			dsDistrictMenuItemJSX = makeDistrictMenuItemJSX();
		}
		return dsDistrictMenuItemJSX;
	};

	const renderProblemListMenu = () => {
		if (isNullOrUndefined(dsProblemMenuItemJSX)) {
			dsProblemMenuItemJSX = makeProblemListMenu();
		}
		return dsProblemMenuItemJSX;
	};

	function renderSelectForDS1986ErrorIdState() {
		// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS1986ErrorIdState in renderSelectForDS1986ErrorIdState is: ", DS1986ErrorIdState)
		// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(dsProblemList[20 - 11]);
		const curVal: string = DS1986ErrorIdState > 11 && dsProblemList.length > 1
			? "(" + DS1986ErrorIdState.toString() + ")&nbsp;" + dsProblemList[DS1986ErrorIdState - 11].Description
			: "working ...";
		// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("curVal is ", curVal);

		return (

			<FormControl className={classes.stylefortopacknowledgementtext}>
				<div className={classes.stylefortopacknowledgementtext}>
					
					<InputLabel className={classes.stylefortopacknowledgementtext}
						id="demo-simple-select-labelErrorId">
						{DS1986ErrorIdState > 11 && dsProblemList.length > 1
							? `Change current choice using  -->`
							: `Make a choice using `
						}
						<br /><br />Problem Type:
					</InputLabel>
				</div>
				<div>&nbsp;</div>
				<Controller

					as={
						<Select
							className={classes.stylefortopdistributionrowtextwide}
							labelId="demo-simple-select-labelErrorId"
							id="demo-simple-selectErrorId"
							value={DS1986ErrorIdState}
							onMouseDown={
								(e) =>
									handleDropDownChangeErrorId(e, "fldDS1986ErrorId")
							}
							name="fldDS1986ErrorId"
						>
							{renderProblemListMenu()}
						</Select>
					}
					name="Select"
					id="fldDS1986ErrorId"
					control={control}
					defaultValue={!DS1986ErrorIdState ? 11 : DS1986ErrorIdState}
				/>
				<div>&nbsp;</div>
				<ErrorMessage
					errors={errors}
					name="fldDS1986ErrorId"
					as={<ErrorMessageContainer />}
				/>
			</FormControl>

		);
	}

	function renderSelectForDS1984DistrictIdState() {
		// JSX has to call this with renderDS1984DistrictIdState(), including ()
		return (
			<FormControl className={classes.stylefortopacknowledgementtext}>

				<InputLabel className={classes.stylefortopdistributionrowtextwide}
					id="demo-simple-select-labelDistrict">
					<div>Location responsible for NOV#&nbsp;{NovasNoState}</div>
					<div>&nbsp;</div>
					<div>
						{`Change current choice (${DS1984DistrictNameState}) using -->`}
					</div>
				</InputLabel>
				<div>&nbsp;</div>
				<Controller

					as={({ onChange, onBlur, value, name }) => (
						<Select
							ref={register({
								required: { value: true, message: "You must pick a location where you accepted the 25 pack." },
								minLength: { value: 3, message: "You have to pick a location where you accepted the 25 pack" }
							})}
							className={classes.stylefortopdistributionrowtextwide}
							labelId="demo-simple-select-labelDistrict"
							id="demo-simple-selectDistrict"
							onChange={handleDropDownChangeDS1984District}
							name="FldDS1984District"
							value={!DS1984DistrictIdState ? 0 : DS1984DistrictIdState}
						>
							{renderFromDistrictMenuItemJSX()}
						</Select>
					)}
					name="Select"
					id="IDofFldDS1984District"
					control={control}
					defaultValue={!DS1984DistrictIdState ? 0 : DS1984DistrictIdState}
				/>
				<div>&nbsp;</div>
				<ErrorMessage
					errors={errors}
					name="fldDS1984District"
					as={<ErrorMessageContainer />}
				/>
			</FormControl>
		);
	}

	const renderActionButtons = () => {
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("oneDS1986form.DS1986ReceiptNo is ", oneDS1986form.DS1986ReceiptNo);
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(oneDS1986form.DS1986ReceiptNo);
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(!isNullOrUndefined(oneDS1986form.DS1986ReceiptNo));
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(!oneDS1986form.DS1986ReceiptNo ? 0 : oneDS1986form.DS1986ReceiptNo.length);
			if (!isNullOrUndefined(oneDS1986form.ID)) // prevent save/update
			{
				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							type="submit"
							onClick={handleSubmit(submitForm)}
						>
							Update
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="secondary"
							onClick={() => newDS1986()}
						>
							Make new DS-1986
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}
			else if (PTTSfunctions.Commonfunctions2.isNotDefined(oneDS1986form.ID)) // prevent save/update
			{
				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={() => newDS1986()}
						>
							Make new DS-1986
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}
			else {

				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							type="submit"
							onClick={handleSubmit(submitForm)}
						>
							Update
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary">
							Make New DS-1986
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}

		};

	const makeDistrictMenuItemJSX = () => {
		if (!dsList) {
			return (
				<MenuItem
					key={"KeyForDS1984Dist" + "0"}
					value={0}
				>
					{`working ...`}
				</MenuItem>
				);
		};
		return dsList.map(oneChoice => {
			// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("district: ", DS1984DistrictIdState, !oneChoice.MappingDistictNTI ? oneChoice.Description === DS1984DistrictIdState : oneChoice.MappingDistictNTI === DS1984DistrictIdState);
			return (
				<MenuItem
					selected={oneChoice.Id === DS1984DistrictIdState}
					key={"KeyForDS1984Dist" + oneChoice.Id.toString()}
					value={oneChoice.Id}
				>
					{!oneChoice.MappingDistictNTI ? oneChoice.Description : oneChoice.MappingDistictNTI}
				</MenuItem>);
		});
	};

	const makeProblemListMenu = () => {
		// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("JSX for ProblemListMenu");
		if (!dsProblemList) {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS1986ErrorIdState.Id is ", DS1986ErrorIdState);
			return (
				<MenuItem
					key={DS1986ErrorIdState.toString()}
					value={DS1986ErrorIdState}
				>
					{`working on translating ${DS1986ErrorIdState} into a description`}
				</MenuItem>
				)
		};
		return dsProblemList.map(oneChoice => {
			// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("oneChoice");
			// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(oneChoice);
			// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("oneChoice.Id is ", oneChoice.Id);
			// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("oneChoice.Id === DS1986ErrorIdState is ", oneChoice.Id === DS1986ErrorIdState);
			return (
				<MenuItem
				selected={oneChoice.Id === DS1986ErrorIdState}
				key={oneChoice.Id.toString()}
				value={oneChoice.Id}
				>
				{`(${oneChoice.Id}) ${oneChoice.Description}`}
				</MenuItem>
			);
		});
	};

	if (isLoading) {
		return (<div>Loading ...</div>);
	}
	else {

		return (
            <div>
                <div onSubmit={handleSubmit(submitForm)}>

                    <Grid className={classes.formposition}>
                        <Grid>
                            <Grid container className={classes.darkborder}>
                                <Grid item xs={2} className={classes.styleforlogo}>
                                    <img src={PTTSfunctions.Logos.DSNYLogo.logoImg} alt="Sanitation Enforcement" />
                                </Grid>
                                <Grid item xs={9} className={classes.styleforformtitle}>
                                    <div className={classes.styleforformtitletext}>THE CITY OF NEW YORK Department of Sanitation</div>
                                    <div className={classes.styleforformtitletext}>DS1986B ENFORCEMENT SUMMONS TRACKING REPORT. DS1986B(11-13)</div>
                                </Grid>
                            </Grid>
                            <div>&nbsp;</div>
                            <div>&nbsp;</div>
                            <Grid container>
                                {renderValidationMsgs()}
                                <Grid item xs={12}>
                                    {renderActionButtons()}
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid container className={classes.darkborder}>
                            <Grid item xs={12} >
                                &nbsp;
							</Grid>
                        </Grid>
                        <Grid container className={classes.darkborder}>
                            <Grid item xs={2} className={classes.stylefortopacknowledgement}>
                                <div>DS-1986 Receipt#:&nbsp;</div>
                            </Grid>
                            <Grid item xs={10} className={classes.thinborderLeft}>
								<div><b>{ReceiptNoState}</b></div>
                            </Grid>
                        </Grid>
                        <Grid container className={classes.darkborder}>
                            <Grid item xs={1} className={classes.stylefortopacknowledgement}>
                                <div>NOV#:</div>
                            </Grid>
                            <Grid item xs={3} className={classes.thinborderLeft}>
                                <div >
                                    <input className={classes.stylefortopdistributionrowtext} type="text"
                                        onChange={(e) => handleOnChange(e)}
                                        name="fldNovasNo"
                                        placeholder="eight digits"
                                        ref={register({
                                            required: { value: true, message: "PTTS expects a NOV#." },
                                            pattern: {
                                                value: /^[1-2]\d{7}$/,
                                                message: "Enter an eight digit NOV# starting with either '1' or '2'."
                                            },
                                            minLength: { value: 8, message: "NOV# is too short." },
                                            maxLength: { value: 8, message: "NOV# is to long." },
                                            // validate: (fldNOV) => fIsAlreadyUsedTktRange("DS1986", "Low", fldNOV),
                                        })}
                                        value={NovasNoState}
                                    />
                                    <div>&nbsp;</div>
                                    <ErrorMessage
                                        errors={errors}
                                        name="fldNovasNo"
                                        as={<ErrorMessageContainer />}
                                    />
                                </div>
                            </Grid>
                            <Grid item xs={1} className={classes.thinborderLeft}>
                                <div>DS-1984</div><div>Receipt Number</div>
                            </Grid>
                            <Grid item xs={4} className={classes.stylefortopacknowledgement}>
                                <div>
                                    <input className={classes.stylefortopacknowledgement}
                                        type="text" onChange={(e) => handleOnChange(e)}
                                        name="fldDs1984No"
                                        ref={register({
                                            required: { value: true, message: "PTTS expects a NOV#." },
                                            pattern: {
                                                value: /^[4]\d{8}[A-Z]\d{12}$/,
                                                message: "Enter a 22 character receipt # starting with '4' (4YYYYMMDD[4-char PAYcode]123456789)."
                                            },
                                            minLength: { value: 22, message: "receipt # is too short." },
                                            maxLength: { value: 22, message: "receipt # is to long." },
                                            // validate: (fldDs1984NoVal) => fIsDS1984Receipt("IsDS1984", "Low", fldDs1984NoVal),
                                        })}
                                        value={Ds1984NoState}
                                    />
                                    <div>&nbsp;</div>
                                    <ErrorMessage
                                        errors={errors}
                                        name="fldDs1984No"
                                        as={<ErrorMessageContainer />}
                                    />
                                    {
                                        !IsDS1984ReceiptState
                                            ? <div></div>
											: <div onClick={onGoToParent}><Link href="#">Click <b>here</b> to see this DS-1984.</Link></div>
                                    }
                                </div>
                            </Grid>
                            <Grid item xs={3} className={classes.stylefortopacknowledgementRight}>
								<div>
									<MuiPickersUtilsProvider utils={DateFnsUtils}>
										<KeyboardDatePicker
											disableToolbar
											name="fldCreatedDate"
											variant="inline"
											format="MM/dd/yyyy"
											margin="normal"
											id="date-picker-inline"
											label={`DS-1986 ${IDState} created on `}
											value={CreatedDateState}
											onChange={(e) => handleOnDateChange(e)}
											KeyboardButtonProps={{
												'aria-label': 'change date',
											}}
										/>
									</MuiPickersUtilsProvider>
								</div>
                            </Grid>
                        </Grid>
                        <Grid container className={classes.darkborder}>
                            <Grid item xs={4} className={classes.stylefortopacknowledgement}>
								<div>District DS-1984:<div><b>
									{DS1984DistrictNameState}
								</b></div></div>
                            </Grid>
                            <Grid item xs={8} className={classes.thinborderLeft}>
                                <div>{renderSelectForDS1984DistrictIdState()}</div>
                            </Grid>
                        </Grid>
                        <Grid container className={classes.darkborder}>
                            <Grid item xs={1} className={classes.stylefortopacknowledgement}>
                                <div>Employee REF#</div>
                            </Grid>
							<Grid item xs={2} className={classes.thinborderLeft}>
								<div >
									<input className={classes.thinborderLeftBoldContent}
										type="text"
                                        onChange={(e) => handleOnChange(e)}
                                        name="fldReferenceNo"
                                        placeholder="REF# w/zeroes"
                                        ref={register({
                                            required: { value: true, message: "PTTS expects a reference number of the employee responsible for the NOV#." },
                                            pattern: {
                                                value: /^[A-Za-z]|([\w]+['][\w]+)|^\d{7}$/,
                                                message: "Employee: Enter a seven digit REF# so PTTS can look up employee information."
                                            }
                                        })}
                                        value={ReferenceNoState}
                                    />
                                    <div>&nbsp;</div>
                                    <ErrorMessage
                                        errors={errors}
                                        name="fldReferenceNo"
                                        as={<ErrorMessageContainer />}
                                    />
                                </div>
                            </Grid>
                            <Grid item xs={2} className={classes.stylefortopacknowledgementRight}>
                                <div>Employee Name:</div>
                            </Grid>
                            <Grid item xs={2} className={classes.thinborderLeft}>
								<div><b>{NameState}</b></div>
                            </Grid>
                            <Grid item xs={1} className={classes.stylefortopacknowledgementRight}>
                                <div>Assigned District</div>
                            </Grid>
                            <Grid item xs={2} className={classes.thinborderLeft}>
								<div><b>{DistrictState}</b></div>
                            </Grid>
                            <Grid item xs={1} className={classes.stylefortopacknowledgementRight}>
                                <div>Badge #</div>
                            </Grid>
                            <Grid item xs={1} className={classes.thinborderLeft}>
								<div><b>{BadgeNoState}</b></div>
                            </Grid>
                        </Grid>
                        <Grid container className={classes.darkborder}>
                            <Grid item xs={4} className={classes.stylefortopacknowledgement}>
                                <div>Problem Type:</div>
								<div><b>{DS1986ErrorIdState > 11 && dsProblemList.length > 1 
									? dsProblemList[DS1986ErrorIdState - 11].Description
									: "PTTS expects the problem to be categorized."}
									</b></div>
                            </Grid>
                            <Grid item xs={4} className={classes.thinborderLeft}>
                                <div>{renderSelectForDS1986ErrorIdState()}</div>
                            </Grid>
                            <Grid item xs={2} className={classes.thinborderLeft}>
                                <div className={classes.stylefortopacknowledgementRight}>Others:</div>
                            </Grid>
                            <Grid item xs={2} className={classes.thinborderLeft}>
                                <div >
                                    <input className={classes.stylefortopacknowledgement}
                                        type="textArea" onChange={(e) => handleOnChange(e)}
                                        name="fldOtherRemarks" ref={register}
                                        value={OtherRemarksState}
                                    />
                                </div>
                            </Grid>
                        </Grid>
                        <Grid item xs={12} className={classes.darkborder}>
                            <Grid item xs={4} className={classes.thinborderLeft}>
                                <div>Remarks:</div>
                            </Grid>
                            <Grid item xs={8} className={classes.thinborderLeft}>
                                <div >
									<input className={classes.stylefortopdistributionrowtextsuperwide}
                                        type="text" onChange={(e) => handleOnChange(e)}
                                        name="fldRemarks" ref={register}
                                        value={RemarksState}
                                    />
                                </div>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            {renderActionButtons()}
                        </Grid>
                    </Grid>

                </div>
            </div>
		)
	}

};


const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5,
		marginTop: 0
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const useStyles = makeStyles({
	root: {
		flexGrow: 1,
		height: "100%",
		paddingLeft: 15,
		paddingRight: 15,

	},
	formposition: {
		marginLeft: 280,
		width: "100%",
	},
	darkborder: {
		borderWidth: 3,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderTop: {
		borderTopWidth: 2,
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderLeft: {
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderTopWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderLeftBoldContent: {
		borderLeftWidth: 0,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderTopWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
		fontWeight: 700,
	},
	styleforlogo: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforsubmit: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforformtitle: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
	},
	styleforformtitletext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "black",
		minWidth: 500,
	},
	styleforformorder: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "center",
	},
	styleforformordertext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "center",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 500,
		color: "black",
	},
	styleforticketrange: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
	},
	stylefortopacknowledgement: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	stylefortopacknowledgementRight: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "right",
	},
	stylefortopacknowledgementtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 320,
		minWidth: 130
	},
	stylefortopdistributionrow: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
		maxWidth: 300,
		wordWrap: "break-word",
	},
	stylefortopdistributionrowtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
		wordWrap: "break-word",
	},
	stylefortopdistributionrowtextwide: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		minWidth: 300,
		maxWidth: 600,
	},
	stylefortopdistributionrowtextsuperwide: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		minWidth: "100%",
	},
	styleforformreceipt: {
		verticalAlign: "center",
		borderTopStyle: "none",
	},
	styleforformreceipttext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		display: "inline",
		marginLeft: 10,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
		textAlign: "center",
	},
	styleforformreceipttextred: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "red",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
	},
	button: {
		marginTop: 20,
	},

	formstyle: {
		maxWidth: "100%",
		padding: 0,
	},

	input: {
		display: "block",
		borderRadius: 4,
		border: "1px solid",
		padding: "10px 15px",
		marginBottom: 10,
		fontSize: 14,
		maxWidth: 400,
		minWidth: 200,
	},

	reactselect: {
		maxWidth: 400,
	},

	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginBottom: 13,
		marginTop: 20,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
	},
	buttonSubmit: {
		background: "#0e8c1b",
		color: "white",
		border: "none",
		marginTop: 20,
		padding: 20,
		fontSize: 16,
		fontWeight: 100,
		width: 200,
	}
});
