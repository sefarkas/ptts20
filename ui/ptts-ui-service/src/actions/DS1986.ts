﻿import { DS1986Action, DS1986Actions, DS1986 } from "../model";

function fAddDS1986(oDS1986: DS1986): DS1986Action {
	return {
		type: DS1986Actions.ADD_DS1986,
		payload: oDS1986,
	};
}

function fUptDS1986(oDS1986: DS1986): DS1986Action {
	return {
		type: DS1986Actions.UPDATE_DS1986,
		payload: oDS1986,
	};
}


// Async Function expample with redux-thunk
function fCompleteDS1986(DS1986Id: number, dispatchRef: any) {
	// here you could do API eg

	return dispatchRef({ type: DS1986Actions.COMPLETE_DS1986, payload: DS1986Id });
}

function fUncompleteDS1986(DS1986Id: number): DS1986Action {
	return {
		type: DS1986Actions.REVERT2INCOMPLETE_DS1986,
		payload: DS1986Id,
	};
}

function fDS1986deleteTodo(DS1986Id: number): DS1986Action {
	return {
		type: DS1986Actions.DELETE_DS1986,
		payload: DS1986Id,
	};
}


export const allActions = {
	addDS1986: fAddDS1986,
	uptDS1986: fUptDS1986,
	completeDS1986: fCompleteDS1986,
	uncompleteDS1986: fUncompleteDS1986,
	DS1986deleteTodo: fDS1986deleteTodo
}
