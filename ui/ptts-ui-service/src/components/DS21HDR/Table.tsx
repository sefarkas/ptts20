﻿// prettier-ignore
import { ErrorMessage } from "@hookform/error-message";
import {
    Divider, FormControl, Grid, Input, InputLabel, Link, Paper, Table, TableBody, TableCell,
    TableHead, TableRow
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { allActions } from "../../actions";
import { allServices } from '../../services';
import { DS21HDRActions, tDS21HDR, UsedTicketRange } from "../../model";
import { PTTSfunctions } from "../basicComponents";
import { ErrorSummary, ErrorMessageContainer } from "../basicComponents/validation";
import { dummies } from "../../reducers";

type FormData = {
	fldNOVnumber: number;
}

export function DS21HDRtable(props: any) { // expect a table of DS21s for a particular user
	// very similar to DS1984 list because there is one DS21 for each DS1984
	// DS21 form is a ledger of 25 NOVs.

	const classes = useStyles();
	const dispatch = useDispatch();
	const mountedRef = useRef(true);
	let history = useHistory();

	const { register, handleSubmit, errors, setError } = useForm<FormData>();

	const contentparentstate: any = useSelector((state: any) => state);
	const contentparent: any = useSelector((state: any) => state.DS21HDRReducer);
	const currDS21hdr: tDS21HDR[] = !contentparent.ST_DS21HDRrows ? dummies.dummyDS21HDR : contentparent.ST_DS21HDRrows;
	const currDS21hdrFLTRD: tDS21HDR[] = !contentparent.ST_DS21HDRrowsFLTRD ? dummies.dummyDS21HDR : contentparent.ST_DS21HDRrowsFLTRD;

	const [IsValidNOVnumberState, setIsValidNOVnumberState] = useState(false);
	const [NOVnumberState, setNOVnumberState] = useState("");
	const [Loading, setLoading] = useState(
		!currDS21hdr
			? true
			: !currDS21hdr.length
				? true
				: (
					currDS21hdr.length < 2
					|| (
						!currDS21hdrFLTRD ? true
						: !currDS21hdrFLTRD[0]
							? true
								: currDS21hdrFLTRD[0].DS1984Number.length < 1
					)
				)
	);
	const [DS198xrcpt, setDS198xrcpt] = useState(!props
		? ""
		: !props.DS198xrcpt
					? ""
			: props.DS198xrcpt);
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS198xrcpt ", DS198xrcpt);

	React.useEffect(() => {
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("init useEffect: start");

		setLoading(true);
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS198xrcpt is ", DS198xrcpt);
        if (!DS198xrcpt ? true : !DS198xrcpt.length ? true : DS198xrcpt.length > 0 ? false : true) {
            if (!contentparent.ST_DS21HDRrows) {
                if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("#1 no dsList yet");
                setLoading(true);
                (allActions.DS21acts.DS21HDRList(dispatch, currDS21hdr));
            }
            else if (contentparent.ST_DS21HDRrows.length < 2) {
                if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("#1 dsList less than two items");
                setLoading(true);
                (allActions.DS21acts.DS21HDRList(dispatch, currDS21hdr));
            }
        }
        else if (DS198xrcpt.substring(0, 1) === "4") {
            if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("partial DS21HDR list", DS198xrcpt);
            if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("init useEffect: get DSHDR for ", DS198xrcpt);
            allActions.DS21acts.DS21HDRRelatedToOneDS1984List(dispatch, DS198xrcpt);
        }
        else {
            alert("unexpected props sent to DS21HDR Tables component.");
        }

	}, []);

	React.useEffect(() => {
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("contentparent.ST_DS21HDRrows: start");

		const rdata: any = contentparent.ST_DS21HDRrows; // work around  "Type 'blah' has no compatible call signatures"; rdata.sort as DS21HDR[] raises typescript exception
		if (rdata !== undefined) {
			if (!DS198xrcpt
				? true
				: !DS198xrcpt.length
					? true
					: DS198xrcpt.length > 0
						? false // only one DS21HDR row for each DS198xrcpt, no filtering needed
						: true) {
				const dsListFiltered: tDS21HDR[] = PTTSfunctions.ADfunctions.DS21hdrIAWpayCodesExtractedFromADGroupMembership(rdata, contentparentstate)
				const dsListFilteredSorted: tDS21HDR[] = dsListFiltered.sort(function (a: tDS21HDR, b: tDS21HDR) {
					var nameA = !a.DS1984Number ? "" : a.DS1984Number.toUpperCase(); // ignore upper and lowercase
					var nameB = !b.DS1984Number ? "" : b.DS1984Number.toUpperCase(); // ignore upper and lowercase
					if (nameA < nameB) {
						return 1;
					}
					if (nameA > nameB) { 
						return -1;// DESC
					}
					// names must be equal
					return 0;
				})//.slice(0, 29);

				dispatch({ type: DS21HDRActions.aDS21HDRROWSFLTRDadd, payload: dsListFilteredSorted });
			}
			else {
				dispatch({ type: DS21HDRActions.aDS21HDRROWSFLTRDadd, payload: rdata });
				// system has a trigger based upon ST_DS21HDRrowsFLTRD
			}
        }


	}, [contentparent.ST_DS21HDRrows])

	React.useEffect(() => {
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("contentparent.ST_DS21HDRrowsFLTRD useEffect");
		console.log("set loading value to: ",
			(contentparentstate.giantReducer.ST_ADGROUPS.length > 1
			|| contentparentstate.giantReducer.ST_ADGROUPS[0].userName !== "xyz"
			|| (DS198xrcpt.length < 1))
			? (contentparent.ST_DS21HDRrowsFLTRD.length < 2)
			: !(DS198xrcpt.length > 0)
				 // filtering doesn't matter just show the DS21HDRrow for given DS198xrcpt
		)
		console.log("set Loading based upon this many rows ready to display: ", contentparent.ST_DS21HDRrowsFLTRD.length);
		if (contentparentstate.giantReducer.ST_ADGROUPS.length > 1
			|| contentparentstate.giantReducer.ST_ADGROUPS[0].userName !== "xyz" // xyz is default before AD functions run
			|| DS198xrcpt.length < 1 // filtering doesn't matter just show the DS21HDRrow for given DS198xrcpt
		) { setLoading(contentparent.ST_DS21HDRrowsFLTRD.length < 2); }
		if (DS198xrcpt.length > 0 ) {
			setLoading(false);  // ST_DS21HDRrowsFLTRD list is ready to display
		}
	}, [contentparent.ST_DS21HDRrowsFLTRD])

	function handleGetNOVsForThisDS21(rcpt: string) {
		mountedRef.current = false
		history.push('/PTTS/DS21Card/' + rcpt)
	}

	function fIsValidNOVnumber(aDSform: string, LowHigh: string, TgtVal: string): boolean {
		let rdata: UsedTicketRange[] | undefined = undefined;
		let pIsValidNOVnumber: boolean = false;

		const searchECBsummonsNumber: string =
			TgtVal.length < 9
				? PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(Number.parseInt(TgtVal))
				: TgtVal;
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Find a ticket range with NOV ", searchECBsummonsNumber);

		(async () => {
			const tryThisRange = (
				{
					ECBSummonsNumberFrom: LowHigh === "Low"
						? TgtVal
						: searchECBsummonsNumber,
					ECBSummonsNumberTo: LowHigh === "High"
						? TgtVal
						: searchECBsummonsNumber,
					DSform: aDSform,
					ReceiptNumber: "",
				}
			);
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("tryThisRange");
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange);
			pIsValidNOVnumber = false;
			const response = await allServices.PTTSgetServices.refUsedTicketNumbers(tryThisRange);
			// axios always returns an object with a "data" child
			rdata = await response.data;
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange)
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
			if (!rdata) {
				pIsValidNOVnumber = false;
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ticket number bad based on no rdata object.");
				setIsValidNOVnumberState(pIsValidNOVnumber);
			}
			else if (!rdata.length ? true : rdata.length === 0) {
				pIsValidNOVnumber = false;
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ticket number bad based on empty [].");
				setIsValidNOVnumberState(pIsValidNOVnumber);
			}
			else {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("found a ticket range for ", searchECBsummonsNumber);
				pIsValidNOVnumber =
					Number.parseInt(tryThisRange.ECBSummonsNumberFrom) > 0
						&& Number.parseInt(tryThisRange.ECBSummonsNumberTo) > 0
						? true : false;
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber and non-zeroes");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(pIsValidNOVnumber);

				//if (!pIsValidNOVnumber) {
				//	if (!rdata![rdata.length - 1]) { // https://stackoverflow.com/questions/55190059/typescript-property-type-does-not-exist-on-type-never
				//		alert(" Cannot find a DS-1984 for this summons number.");
				//	}
				//}
				//else {
				//	alert("NOV Number is OKay.");
				//	setNOVnumberState(searchECBsummonsNumber);
				//}

				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber at end of ASYNC");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(pIsValidNOVnumber);
				setIsValidNOVnumberState(pIsValidNOVnumber);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("IsValidNOVnumberState at end of ASYNC");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(IsValidNOVnumberState);
				return pIsValidNOVnumber;
			}

		})();

		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber after ASYNC", pIsValidNOVnumber);
		return IsValidNOVnumberState;

	};

	const handleLinkToParent1984 = (e: any) => {
		mountedRef.current = false;
		history.push('/PTTS/existingDS1984/' + e.target.textContent)
	}

	function handleLinkToDS1704Bulk(sDS1984: any): void {
		mountedRef.current = false;
		history.push('/PTTS/DS1704Bulk/' + sDS1984.toString())
    }

	const handleOnChange = (e: any) => {
		e.preventDefault();

		switch (e.target.name) {

			case "fldNOVnumber":
				let dummy2n: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				if (isNaN(dummy2n)) {
					alert("NOV number is NaN.");
					break;
				}
				if (dummy2n > 299999999) {
					alert("Expected first summons number is supposed to start with 1 or 2 and be nine-digits.  You entered a number that is too big.")
					break;
				} // less than 8 digits, not-a-number, or nine or more digits starting with 3 (Sanitation only prefixes 1 or 2)
				//            12345678
				setNOVnumberState(dummy2n.toString());
				if (dummy2n > 99999999) {
					dummy2n = Math.trunc(dummy2n / 10);
					//const msg1: string = "check NOV number" + dummy2n.toString();
					//alert(msg1);
					const dummy2: string = PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(dummy2n);
					if (!fIsValidNOVnumber("IsGoodNOVnumber", "Low", dummy2)) {
						// true means validation is OKay, value is proper
						if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fIsValidNOVnumber has not found NOV number.")
                    }
				}
				break;
			default:
				// code block
				const msg: string = "No onChange for " + e.target.name;
				 alert(msg);
		}
	}

	return ( // DS21HDR is a table of DS21Cards; each card has a list of 25 NOVs
		<Grid container>
			<Grid item xs={12} component={Paper} className={classes.paper}>
			<FormControl >
				<ErrorSummary errors={errors} />
					
				<InputLabel htmlFor="idNOVnumber" id="LabelForNOVnumber">{NOVnumberState.length < 9
					? <div id="searchForNOVnumber">Search for this ECB Summons Number: {NOVnumberState} </div>
					: IsValidNOVnumberState
						? < div id="gotoNOVnumber" > {NOVnumberState} is valid; click 'Go to {NOVnumberState}'.</div>
						: < div id="foreignNOVnumber" > {NOVnumberState} is not on any existing DS-1984.</div>}
				</InputLabel>
				<Input className={classes.styleforticketrangetext}
					type="number" 
					id="idNOVnumber"
					onChange={(e) => handleOnChange(e)}
					placeholder="9-digit ECB Summons Number"
					name="fldNOVnumber"
					ref={register({
						required: { value: true, message: "PTTS expects the first NOV number (nine digits)." },
						min: {
							value: 100000000,
							message: "Too small. Individual ECB Summons are nine digit numbers starting with 1 or 2."
						},
						max: {
							value: 299999999,
							message: "Too big.  Individual ECB Summons are nine digit numbers starting with 1 or 2."
						},
					})}
					value={NOVnumberState}
				// defaultValue versus value --> value lets me change characters in the currently active control textbox
				/>
				<div>&nbsp;</div>
				<ErrorMessage
					errors={errors}
					name="fldNOVnumber"
					as={<ErrorMessageContainer />}
				/>
				{IsValidNOVnumberState
					?
						<button name="btnSubmit" type="submit" onClick={(e) => { e.preventDefault(); history.push('/PTTS/NOVdetails/' + NOVnumberState); }}>Go to {NOVnumberState}</button>
					:
					<div>&nbsp;</div>
                }
				<Divider />
				</FormControl>
			</Grid>
			<Grid item xs={12} component={Paper} className={classes.paper}>

				<Table className={classes.table} aria-label="a dense table" >
					<TableHead>
						<TableRow>
							<TableCell size="small" align="right">DS21</TableCell>
							<TableCell size="small" align="center">Ticket Range</TableCell>
							<TableCell size="small" align="right">Last modified on</TableCell>
							<TableCell size="small" align="left">Supervisor Name</TableCell>
							<TableCell size="small" align="left">Modified By</TableCell>
							<TableCell size="medium" align="left">DS1984</TableCell>
							<TableCell size="medium" align="left">Void All</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{
							// @ts-ignore: Object is possibly 'null'.
							Loading || !currDS21hdrFLTRD //  || !dsListPayCodeFilter || dsListPayCodeFilter === undefined
								|| currDS21hdrFLTRD[0].DS1984Number.length < 2
								? <TableRow><TableCell size="small">working ...</TableCell></TableRow>
								: currDS21hdrFLTRD.map((n: tDS21HDR) => {
									//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("n is:");
									//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(n);
									if (!(n.DS1984Number.length > 0)) {
										return (<TableRow key={"-1"}><TableCell size="small" key={"0"}>none to show</TableCell></TableRow>)
									}
									else {
										return (
											<TableRow
												key={("000000000" + n.DS1984Number.toString()).substr(-8) + "row"}
												hover
											>
												<TableCell size="small" key={n.DS1984Number} align="right" onClick={() => handleGetNOVsForThisDS21(n.DS1984Number)}><Link href="#">DS21 Card</Link></TableCell>
												<TableCell size="small" key={("000000000" + n.DS1984Number.toString()).substr(-8) + "TKTRNG"} align="center">{`${n.BoxNumberedFrom} - ${n.BoxNumberedTo}`}</TableCell>
												<TableCell size="small" key={("000000000" + n.DS1984Number.toString()).substr(-8) + "MODFON"} align="right">{!n.ModifiedOn ? "na" : PTTSfunctions.Commonfunctions.formatDateMMdYYYY(n.ModifiedOn)}</TableCell>
												<TableCell size="small" key={("000000000" + n.DS1984Number.toString()).substr(-8) + "SUPERVNAME"} align="left">{n.OfficerInChargeName}</TableCell>
												<TableCell size="small" key={("000000000" + n.DS1984Number.toString()).substr(-8) + "MODBY"} align="left">{n.ModifiedBy}</TableCell>
												<TableCell size="medium" key={("000000000" + n.DS1984Number.toString()).substr(-8) + "LINK84"} align="left" onClick={handleLinkToParent1984}><Link href="#">{n.DS1984Number}</Link></TableCell>
												<TableCell size="medium" key={("000000000" + n.DS1984Number.toString()).substr(-8) + "LINK1704"} align="left" onClick={() => handleLinkToDS1704Bulk(n.DS1984Number)}><Link href="#">DS 1704</Link></TableCell>
											</TableRow>
										);
									}
								}
								)}
					</TableBody>
				</Table>
			</Grid>

		</Grid>
	);
}

const useStyles = makeStyles({
	paper: {
		width: "100%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 0,
		overflowX: "visible",
	},
	table: {
		width: "100%",
		margingTop: 80,
		minWidth: 1000,
		display:"inline-block",
	},
	cell: {
		minWidth: 100,
		maxWidth: 220,
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 100,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 550,
		minWidth: 300,
	}
});
