﻿import { Grid, makeStyles, Paper } from '@material-ui/core';
import React from 'react';


export function Instructions() {
	const classes = useStyles();

	return (
		<div className={classes.styleforBorder}>
			<Grid item xs={6} component={Paper} className={classes.styleforInstructions}>
				<div >
					INSTRUCTION : <br />
					<ol className={classes.styleforOL}>
						<li>Search for this ECB Summons Number: then '<i>Enter</i>' to open a screen for looking at or revising that NOV. </li>
						<li>Display the <i>DS21 Form</i> related to Ticket #s range entered in the Form DS1984 (which ‘ Receipt No ’ link is in column ‘ DS1984 ’): click on the <i>DS21 Form</i> link in the column ‘DS21’. </li>
						<li>Display the <i>DS1984 Form</i> for the Ticket #s range of the DS21 Form: click on the ‘<i>Receipt No</i>’ link in the column ‘DS1984’. </li>
					</ol>
				</div>
			</Grid>
			<Grid item xs={6} component={Paper} className={classes.paper}>
					&nbsp;
			</Grid>
		</div>
        )
}

const useStyles = makeStyles({
	paper: {
		width: "100%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 360,
		overflowX: "visible",
	},
	styleforInstructions: {
		marginBottom: 13,
		marginTop: 10,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 12,
		fontWeight: "normal",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "red",
		maxWidth: 550,
		minWidth: 300,
	},
	styleforOL: {
		listStyleType: "decimal"
	},
	styleforBorder: {
		borderStyle: "solid",
		borderWidth: 2,
    }
});