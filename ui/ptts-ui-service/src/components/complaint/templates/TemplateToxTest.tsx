﻿import { Button, Box } from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';
import React, { useEffect } from 'react';
import ReactSelect from "react-select";
import { useForm } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers';
import styles from "./templateStyles";

interface Props {
	open: boolean;
	onSubmit: (statementValue: string) => void;
	onClose: (event: any) => void;
	respondent: string;
	incidentDate: string;
}

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const StyledButtonGreen = withStyles({
	root: {
		background: '#2b995f',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const customStyles = {
	control: (base: any) => ({
		...base,
		minHeight: '30px',
		height: '30px',
	}),
	dropdownIndicator: (base: any) => ({
		...base,
		padding: 0
	}),
	clearIndicator: (base: any) => ({
		...base,
		padding: 0
	}),
	valueContainer: (base: any) => ({
		...base,
		height: '30px',
		padding: '0 6px'
	}),
	indicatorContainer: (base: any) => ({
		height: '30px'
	})
};

const titleOpts = [
	{ value: 1, label: "A/M" },
	{ value: 2, label: "S/W" },
	{ value: 3, label: "Supv" },
	{ value: 4, label: "GS" },
	{ value: 5, label: "Other" }
];

export function TemplateToxTest(props: Props) {
	const classes = styles();
	const [templateValues, setTemplateValues] = React.useState({ title: { value: 0, label: "" } });
	
	const dialogSchema = yup.object().shape({
		title: yup.string().required("Required field"),
		urineTestDate: yup.string().required("Required field"),		
		urineReportedTo: yup.string().required("Required field"),
		urinePositiveFor: yup.string().required("Required field"),
		breathTestDate: yup.string().required("Required field"),
		breathReportedTo: yup.string().required("Required field"),		
		supervisionOf: yup.string().required("Required field"),
		breathBAC: yup.string().required("Required field")
	});

	const { register, handleSubmit, errors, setValue, clearErrors, getValues } = useForm({
		resolver: yupResolver(dialogSchema)
	});

	const formatDate = (value: string) => {
		let date = value.split('-');
		const year = String(date[0]).split('-');
		const month = String(date[1]).split('-');
		const day = String(date[2]).split('-');
		return month + "/" + day + "/" + year;
	}

	React.useEffect(() => {
		register({ name: "title" });
		register({ name: "urineTestDate" });
		register({ name: "urineReportedTo" });
		register({ name: "urinePositiveFor" });
		register({ name: "breathTestDate" });
		register({ name: "breathReportedTo" });
		register({ name: "supervisionOf" });
		register({ name: "breathBAC" });
	}, []);

	React.useEffect(() => {
		setValue("urineTestDate", props.incidentDate);		
	}, [props.incidentDate]);

	React.useEffect(() => {
		setValue("urineTestDate", props.incidentDate);
		setValue("urineReportedTo", "");
		setValue("urinePositiveFor", "");
		setValue("breathTestDate", "");
		setValue("breathReportedTo", "");
		setValue("supervisionOf", "");
		setValue("breathBAC", "");

		clearErrors("title");
		clearErrors("urineTestDate");
		clearErrors("urineReportedTo");
		clearErrors("urinePositiveFor");
		clearErrors("breathTestDate");
		clearErrors("breathReportedTo");
		clearErrors("supervisionOf");
		clearErrors("breathBAC");
	}, [props.open]);

	const onSubmit = (data: any) => {
		const title = templateValues.title.label === "Other" ? "" : templateValues.title.label + " ";
		let templateOutput = "On " + formatDate(data.urineTestDate) + ", " + title + props.respondent + ", reported to (the) " + data.urineReportedTo + " as ordered. ";
		templateOutput = templateOutput + "A urine sample of toxicology testing was taken. These results were positive for " + data.urinePositiveFor + " ";
		templateOutput = templateOutput + "On " + formatDate(data.breathTestDate) + ", " + title + props.respondent + " reported to (the) " + data.breathReportedTo + " as ordered. ";
		templateOutput = templateOutput + "A breathalyzer test was conducted under the supervision of " + data.supervisionOf + " ";
		templateOutput = templateOutput + "The result was a positive BAC of " + data.breathBAC + ".";
		props.onSubmit(templateOutput);
	};

	const handleChange = (e: any, control?: string) => {
		if (control === "urineTestDate") {
			setValue("urineTestDate", e.target.value);			
			clearErrors("urineTestDate");
		}
		else if (control === "title") {
			const selected = titleOpts.filter((d) => { return d.value === e.value; })[0].label;
			setTemplateValues({
				...templateValues,
				title: { value: e.value, label: selected }
			});
			setValue("title", e.value);
			clearErrors("title");
		}
		else if (control === "urineReportedTo") {
			setValue("urineReportedTo", e.target.value);
			clearErrors("urineReportedTo");
		}
		else if (control === "urinePositiveFor") {
			setValue("urinePositiveFor", e.target.value);
			clearErrors("urinePositiveFor");
		}
		else if (control === "breathTestDate") {
			setValue("breathTestDate", e.target.value);
			clearErrors("breathTestDate");
		}
		else if (control === "breathReportedTo") {
			setValue("breathReportedTo", e.target.value);
			clearErrors("breathReportedTo");
		}
		else if (control === "supervisionOf") {
			setValue("supervisionOf", e.target.value);
			clearErrors("supervisionOf");
		}
		else if (control === "breathBAC") {
			setValue("breathBAC", e.target.value);
			clearErrors("breathBAC");
		}
	};

	const handleCancel = (event: any) => {
		props.onClose("");
	};

	const pad = (n: any) => {
		return ("00" + n).slice(-2);
	}

	const todaysDate = () => {
		const tempDate = new Date();
		const currDate = tempDate.getFullYear() + '-' + pad((tempDate.getMonth() + 1)) + '-' + pad((tempDate.getDate()));
		return currDate;
	}

	return (
		<form autoComplete="off">
			<Box className={classes.box}>
				<Grid container className={classes.grid} spacing={0}>
					<Grid item xs={12} className={classes.gridCellTemplate}>
						<label className={classes.labelTemplateText}>On </label>
						{errors.urineTestDate && <label className={classes.error}>* </label>}
						<input type="date"
							name="urineTestDate"
							defaultValue={props.incidentDate}
							placeholder="Select Date"
							className={classes.datepicker}
							max={todaysDate()}
							onChange={e => handleChange(e, "urineTestDate")}
						/>
						<label className={classes.labelTemplateText}> , </label>
						{errors.title && <label className={classes.error}>* </label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})}
							/></div>
						<label className={classes.labelTemplateText}>{props.respondent} reported to (the) </label>
						{errors.urineReportedTo && <label className={classes.error}>* </label>}
						<input name="urineReportedTo" className={classes.input} onChange={e => handleChange(e, "urineReportedTo")} />
						<label className={classes.labelTemplateText}> as ordered. A urine sample of toxicology testing was taken. These results were positive for </label>
						{errors.urinePositiveFor && <label className={classes.error}>* </label>}
						<input name="urinePositiveFor" className={classes.input} onChange={e => handleChange(e, "urinePositiveFor")} />
						<label className={classes.labelTemplateText}>. On </label>
						{errors.breathTestDate && <label className={classes.error}>* </label>}
						<input type="date"
							name="breathTestDate"
							placeholder="Select Date"
							className={classes.datepicker}							
							max={todaysDate()}
							onChange={e => handleChange(e, "breathTestDate")}
						/>
						<label className={classes.labelTemplateText}>, </label>
						{errors.title && <label className={classes.error}>* </label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})}
							/></div>
						<label className={classes.labelTemplateText}>{props.respondent} reported to (the) </label>
						{errors.breathReportedTo && <label className={classes.error}>* </label>}
						<input name="breathReportedTo" className={classes.input} onChange={e => handleChange(e, "breathReportedTo")} />
						<label className={classes.labelTemplateText}> as ordered. A breathalyzer test was conducted under the supervision of </label>
						{errors.supervisionOf && <label className={classes.error}>* </label>}
						<input name="supervisionOf" className={classes.input} onChange={e => handleChange(e, "supervisionOf")} />
						<label className={classes.labelTemplateText}>. The result was a positive BAC of </label>
						{errors.breathBAC && <label className={classes.error}>* </label>}
						<input name="breathBAC" className={classes.input} onChange={e => handleChange(e, "breathBAC")} />
						<label className={classes.labelTemplateText}>.</label>
					</Grid>
				</Grid>
			</Box>
			<StyledButtonGreen
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleSubmit(onSubmit)}
			>Confirm
				    </StyledButtonGreen>
			<StyledButton
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleCancel}>
				Cancel
				</StyledButton>
		</form>
	);
}

