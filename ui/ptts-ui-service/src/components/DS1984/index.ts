﻿export { DS1984Dialog } from "./Dialog";
export { DS1984Table } from "./Table";
export { NewDS1984Form as DS1984Form } from "./BlankTemplate";
export { ExistingDS1984Form } from "./ExistingForm";

export interface DS1984basics {
    DS1984receiptNumber: string,
    DS1984fromTktNumber: number,
    DS1984toTktNumber: number,
    DS1984DistrictState: string
};