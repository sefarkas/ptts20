﻿// prettier-ignore
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
//import Draggable from 'react-draggable';
import { useActions } from "../../actions";
import * as ECB603Actions from "../../actions/ECB603";
import Background from '../../assets/images/DS249_1204.png';

interface Props {
	open: boolean;
	onClose: () => void;
}

export function ECB603Dialog(props: Props) {
	const { open, onClose } = props;
	const classes = useStyles();
	const [newECB603Text, setNewECB603Text] = React.useState("");
	const cECB603Actions: any = useActions(ECB603Actions);

	const handleCreate = (event: any) => {
		cECB603Actions.insECB603({
			id: Math.random(),
			completed: false,
			ECB603ReceiptNo: event.target.value,
			TicketRange: '1000-1999',
			CartonNoRange: '10-89',
			NoOfParts: 4,
			ModifiedOn: Date.now,
			ModifiedBy: 'sef'
		});
		// reset text if user reopens the dialog
		setNewECB603Text("new");
		onClose();

	};

	const handleClose = () => {
		// reset text if user reopens the dialog
		setNewECB603Text("");
		onClose();

	};

	const handleUpdate = (event: any) => {
		cECB603Actions.uptECB603({
			id: Math.random(),
			completed: false,
			ECB603ReceiptNo: event.target.value,
			TicketRange: '1000-1999',
			CartonNoRange: '10-89',
			NoOfParts: 4,
			ModifiedOn: Date.now,
			ModifiedBy: 'sef'
		});
		setNewECB603Text("updated");

	};

	return (
		<Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
			<DialogTitle>Add a new ECB603</DialogTitle>
			<DialogContent>
				<DialogContentText>
					THE CITY OF NEW YORK Department of Sanitation<br />
					NOTICE OF VIOLATION & HEARING Form ECB603
				</DialogContentText>
				<TextField
					autoFocus
					margin="dense"
						id="ECB603ReceiptNo"
					label="RECEIPT NUMBER"
					type="string"
					fullWidth
					value={newECB603Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
				<TextField
					margin="dense"
					id="NoOfParts"
					label="Numb of Parts"
					type="string"
					size="medium"
					required
					value={newECB603Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
				<TextField
					margin="dense"
					id="CartonNoRange"
					label="Carton No Range"
					type="string"
					size="small"
					required
					value={newECB603Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
			</DialogContent>
			<DialogActions>
				<Button onClick={handleUpdate} color="secondary">
					Save/Update
				</Button>
				<Button onClick={handleCreate} color="primary">
					MAKE NEW ECB603
				</Button>
				<Button onClick={handleClose} color="primary">
					Done
				</Button>
			</DialogActions>

		</Dialog>

	);
}

const useStyles = makeStyles({
	newECB603: {
		flexGrow: 1,
		height: "10%",
		paddingLeft: 15,
		paddingRight: 15,
		opacity: 60,
		backgroundImage: `url( ${Background} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '82px 103px',
		backgroundPosition: 'left top',
	},
	pending: {
		backgroundColor: "white",
	},
	approved: {
		backgroundColor: "yellow"
	},
	textField: {
		width: "80%",
		margin: 20,
	},
});
