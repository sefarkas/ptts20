﻿// prettier-ignore
import {
	Button,
	FormControl, FormControlLabel, FormLabel, Grid, Paper, Radio, RadioGroup, Table, TableBody, TableCell,
	TableHead, TableRow, withStyles
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { allActions } from "../../../actions";
import { tOneNOV, tDS21_TicketStatus, tBulkUpdateRow, BulkUpdateNOV_ACTION, DS21CardActions } from "../../../model";
import { allServices } from '../../../services';
import { PTTSfunctions } from "../../basicComponents";
import Loader from "../../basicComponents/Loader";
import { dummies } from "../../../reducers";
import { DS21CardReducer, dummyBulkUpdateThisNOV, INITIAL_STATE, tInterForInitialState } from "../../../reducers/DS21Card";

type FormData = {
	fldNOVnumber: number;
}

export function BulkUpdateFooter(props: tInterForInitialState) { // expect a table of with up to 20 rows with NOV#s.

	const classes = useStyles();
	const dispatch = useDispatch();
	const mountedRef = React.useRef(true);

	let eventCurrentTarget: any = null;

	const { register, handleSubmit, errors, setError } = useForm<FormData>();
	//const contentparent: tInterForInitialState | any = !props ? INITIAL_STATE : props; // = useSelector((state: any) => state.DS21CardReducer);
	const [contentparent, setContentParent] = useState<tInterForInitialState | any>(!props ? INITIAL_STATE : props);
	const [dsAttributesOfOneNOV, setdsAttributesOfOneNOV] = useState<tOneNOV>(contentparent.ST_ATTRIBUTESOFONENOV);

	const [isLoadingNOVftr, setLoadingNOVftr] = useState(!contentparent.ST_20BulkUpdateTheseNOVs ? true : contentparent.ST_20BulkUpdateTheseNOVs.length < 2 ? true : false);
	const [isReadyToDraw, setReadyToDraw] = useState(false);
	const [isLoadingTSO, setLoadingTSO] = useState(false);
	const [dsTSOs, setdsTSOs] = useState([dummies.dummyTicketStatusOption]);
	const [currentTSO, setCurrentTSO] = useState<number>(dummies.dummyTicketStatusOption.ID);
	const [renderTSO, setRenderTSO] = useState({ renderThis: [(<div key="1">initial</div>)] });
	const [AllNOVsHaveThisTSOinCommonState, setAllNOVsHaveThisTSOinCommonState] = useState(0);
	const [NOVnumberState, setNOVnumberState] = useState(!contentparent.ST_20BulkUpdateTheseNOVs ? "" : contentparent.ST_20BulkUpdateTheseNOVs[0].NOVnumber);


	function loggedInUser(): string {
		return (localStorage.getItem('userid') + "").toString(); // to avoid possibly null error
		//const j: any = JSON.parse(curUser);
		//return j.data.userPreferences.userName.toString();
	}

	const handleOptionChangeTL = (event: any) => {
		let newSelectionTL: number = -1;
		if (PTTSfunctions.Commonfunctions2.isNotDefined(event.target.value)) {
			eventCurrentTarget = event.target;
			alert("OptionChange event object is marked 'notDefined'");
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(eventCurrentTarget);
		}
		else {
			newSelectionTL = Number.parseInt(event.target.value);
			setCurrentTSO(newSelectionTL);
		}
		return newSelectionTL;
	};

	const renderEmpty = () => {
		return (<div>&nbsp;</div>);
	}

	const submitNOV = async (data: FormData, e: any) => {
		e.target.reset(); // reset after form submit
	}

	const submitForm = async (data: FormData, e: any) => {
		switch (e.target.name) {
			case "Clear All":
				const newFirstRow: tBulkUpdateRow = {
					Id: dummyBulkUpdateThisNOV.Id,
					NOVnumber: dummyBulkUpdateThisNOV.NOVnumber,
					Message: dummyBulkUpdateThisNOV.Message, // selection made from the dropdown list
					NOVnumberIsValid: dummyBulkUpdateThisNOV.NOVnumberIsValid,
					TicketStatusID: dummyBulkUpdateThisNOV.TicketStatusID,
				};
				setLoadingNOVftr(!allActions.DS21acts.iudBulkUpdateNOVlist(newFirstRow, dispatch, BulkUpdateNOV_ACTION.RST));
				break;
			case "Update":
				// pick the currently selected TSO
				dsTSOs.forEach((opt: tDS21_TicketStatus) => {
					return;
				})
				// update the database for NOV#s in contentparent.ST_20BulkUpdateTheseNOVs

				// call back to update the REDUX Store of contentparent.ST_20BulkUpdateTheseNOVs
				break;
			default:
				// code block
				const msg: string = "No onChange for " + e.target.name;
				alert(msg);
		}
		// e.target.reset(); // reset after form submit
	}

	const cbIudNOVs = async (response: any) => {
		const oneNOV: tOneNOV = response.data;
		oneNOV.TicketStatusID = currentTSO;
		const lclUser: string = (localStorage.getItem('userid') + "").toString();
		if (await allServices.PTTSiudServices.iudNOV(oneNOV, lclUser)) {
			dispatch({
				type: DS21CardActions.UPDATE_NOV,
				payload: oneNOV,
			});
		}
		else {
			return;
		}
		setAllNOVsHaveThisTSOinCommonState(currentTSO);
	}

	const er = (response: any) => {
		alert(response);
		return;
	}

	function renderMenuTSO(currentTSO: number, isLoadingTSO: boolean, isLoadingNOV: boolean,
		dsTSOs: tDS21_TicketStatus[], dsAttributesOfOneNOV: tOneNOV) {
		if (currentTSO > 0) {
			return PTTSfunctions.Commonfunctions2.fTSOjsxElement(currentTSO,
				false, false,
				dsTSOs, dsAttributesOfOneNOV);

		}
		else {
			return dsTSOs.map((oneChoice: tDS21_TicketStatus) => {
				return (
					<FormControlLabel
						disabled={false}
						key={oneChoice.ID}
						value={oneChoice.ID}
						control={<Radio />}
						label={oneChoice.Description}
					/>
				);
			})
		}
	};

	const cbOneNOVget = (response: any) => {
		let rdata: tOneNOV = response.data;
		console.log("successful NOV retrieval", rdata);
		const allRows: tBulkUpdateRow[] = contentparent.ST_20BulkUpdateTheseNOVs;
		const indRow: number = allRows.findIndex((aRow: tBulkUpdateRow) => aRow.NOVnumber === rdata.ECBSummonsNumber);
		if (indRow > 0) {
			const uptRow: tBulkUpdateRow = allRows[indRow];
			if (uptRow.TicketStatusID === rdata.TicketStatusID) {
				setCurrentTSO(rdata.TicketStatusID);
				if (contentparent.ST_20BulkUpdateTheseNOVs.length === 2) {
					setAllNOVsHaveThisTSOinCommonState(rdata.TicketStatusID);
				}
				else {
					// test against all NOVs in allRows[]
					const differentRow: number = allRows.findIndex((aRow: tBulkUpdateRow) => aRow.TicketStatusID != rdata.TicketStatusID);
					if (differentRow > 0) { setAllNOVsHaveThisTSOinCommonState(0) } // first row [0] always has an empty-string NOV#
					else { setAllNOVsHaveThisTSOinCommonState(rdata.TicketStatusID) }
                }
			}
			else {
				if (uptRow.TicketStatusID < 0) {
					uptRow.TicketStatusID = rdata.TicketStatusID;
				}
				setLoadingNOVftr(!allActions.DS21acts.iudBulkUpdateNOVlist(uptRow, dispatch, BulkUpdateNOV_ACTION.UPT));
			}
		}
		else {
			alert("Cannot find NOV# " + rdata.ECBSummonsNumber + " in the list of entered tickets.")
			setLoadingNOVftr(true);
		}
	}

	React.useEffect(() => {
		if (currentTSO > 0) {
			const lclNOVtoUpdate: tBulkUpdateRow[] = contentparent.ST_20BulkUpdateTheseNOVs;
			lclNOVtoUpdate.forEach(async (aRow: tBulkUpdateRow) => {
				if (aRow.NOVnumber > "" && aRow.TicketStatusID !== currentTSO) {
					await allServices.PTTSgetServices.oneNOVwithCallBack(aRow.NOVnumber, cbIudNOVs, er)
				}
			})
		}

	}, [currentTSO]);

	React.useEffect(() => {
		setLoadingTSO(true);
	}, [])

	React.useEffect(() => {
		if (NOVnumberState > "") {
			allServices.PTTSgetServices.oneNOVwithCallBack(NOVnumberState, cbOneNOVget, er)
        }
	}, [NOVnumberState])

	React.useEffect(() => {
		// if (isLoadingNOV) { return };

		if ((!contentparent.ST_20BulkUpdateTheseNOVs ? 0 : contentparent.ST_20BulkUpdateTheseNOVs.length) < 2) // assumes must have at least one dummy row
		{
			setCurrentTSO(!contentparent.ST_20BulkUpdateTheseNOVs ? -100 : contentparent.ST_20BulkUpdateTheseNOVs[0].TicketStatusID);
			setAllNOVsHaveThisTSOinCommonState(!contentparent.ST_20BulkUpdateTheseNOVs ? 0 : contentparent.ST_20BulkUpdateTheseNOVs[0].TicketStatusID);
		}
		else if ((!contentparent.ST_20BulkUpdateTheseNOVs ? 0 : contentparent.ST_20BulkUpdateTheseNOVs.length) === 2) {
			if (contentparent.ST_20BulkUpdateTheseNOVs[1].TicketStatusID !== currentTSO
				&& contentparent.ST_20BulkUpdateTheseNOVs[1].TicketStatusID > 0) {
				setCurrentTSO(contentparent.ST_20BulkUpdateTheseNOVs[1].TicketStatusID);
				setAllNOVsHaveThisTSOinCommonState(contentparent.ST_20BulkUpdateTheseNOVs[1].TicketStatusID);
			}
			else {
				// TicketStatus hasn't been determined yet
				setNOVnumberState(contentparent.ST_20BulkUpdateTheseNOVs[1].NOVnumber) // artificial to trigger useEffect
            }
		}
		else {

			let bSame: boolean = false;
			let arrTSO: number[] = [];
			for (let i: number = 1; i < contentparent.ST_20BulkUpdateTheseNOVs.length; i++) { // does not have most recent ST_20BulkUpdateTheseNOVs
				if (contentparent.ST_20BulkUpdateTheseNOVs[i].NOVnumber.length > 0) { arrTSO.push(contentparent.ST_20BulkUpdateTheseNOVs[i].TicketStatusID) }
			}
			bSame = PTTSfunctions.Commonfunctions2.arrAllEqual(arrTSO);
			if (bSame || contentparent.ST_20BulkUpdateTheseNOVs.length === 2) {
				setAllNOVsHaveThisTSOinCommonState(arrTSO[0])
			}
			else {
				setAllNOVsHaveThisTSOinCommonState(0)
			}
        }
		setReadyToDraw(true)
	}, [contentparent.ST_20BulkUpdateTheseNOVs])

	React.useEffect(() => {
		// ticket status options
		if (isLoadingTSO) {
			setRenderTSO({
				renderThis: [(
					<div key="2"> working ...</div >
				)]
			});
		}
		else if ((!contentparent.ST_20BulkUpdateTheseNOVs ? 0 : contentparent.ST_20BulkUpdateTheseNOVs.length) < 2) {
			setRenderTSO({
				renderThis: [(
					<div key="3">Add a valid NOV# to the list above.</div>
				)]
			});
		}
		else if ((!contentparent.ST_20BulkUpdateTheseNOVs ? 0 : contentparent.ST_20BulkUpdateTheseNOVs.length) === 2) {
			console.log("renderMenuTSO AllNOVsHaveThisTSOinCommonState", AllNOVsHaveThisTSOinCommonState);
			console.log("renderMenuTSO currentTSO", currentTSO);
			setRenderTSO({ // ticket status options
				renderThis: [(
					<React.Fragment key="keyRF" >
						<FormControl component="fieldset" key="keyFC" >
							<FormLabel component="legend" key="keyFL">Ticket Location</FormLabel>
							<RadioGroup aria-label="Ticket Location" name="optTicketLocation" key="keyRG"
								value={contentparent.ST_20BulkUpdateTheseNOVs[1].TicketStatusID} onChange={handleOptionChangeTL}>
								{renderMenuTSO(contentparent.ST_20BulkUpdateTheseNOVs[1].TicketStatusID, isLoadingTSO, isLoadingNOVftr,
									dsTSOs, dsAttributesOfOneNOV)}
							</RadioGroup>
						</FormControl>
					</React.Fragment>
				)]
			});
		}
		else {
			console.log("2nd renderMenuTSO AllNOVsHaveThisTSOinCommonState", AllNOVsHaveThisTSOinCommonState);
			console.log("2nd renderMenuTSO currentTSO", currentTSO);
			setRenderTSO({ // ticket status options
				renderThis: [(
					<React.Fragment key="keyRF" >
						<FormControl component="fieldset" key="keyFC" >
							<FormLabel component="legend" key="keyFL">Ticket Location</FormLabel>
							<RadioGroup aria-label="Ticket Location" name="optTicketLocation" key="keyRG"
								value={AllNOVsHaveThisTSOinCommonState} onChange={handleOptionChangeTL}>
								{renderMenuTSO(AllNOVsHaveThisTSOinCommonState, isLoadingTSO, isLoadingNOVftr,
									dsTSOs, dsAttributesOfOneNOV)}
							</RadioGroup>
						</FormControl>
					</React.Fragment>
				)]
			});
		}
	}, [AllNOVsHaveThisTSOinCommonState, contentparent.ST_20BulkUpdateTheseNOVs, isLoadingTSO]);

	React.useEffect(() => { if (!isReadyToDraw && mountedRef.current) { mountedRef.current = false; } }, [isReadyToDraw]);

	React.useEffect(() => {
		const bTSOs: boolean = dsTSOs.length < 2;
		setLoadingTSO(bTSOs) // true means the dataset isn't finished loading; expected a half dozen or more items in this dataset
	}, [dsTSOs])

	const cbTSO = (responseTSO: any) => {
		let rBoolean: boolean = false;
		if (!responseTSO) {
			rBoolean = false;
		}
		else {
			const rdataTSO: tDS21_TicketStatus[] = responseTSO.data;
			if (!rdataTSO) {
				rBoolean = false;
			}
			else {
				let ind: number = 0;
				for (ind = 0; ind < rdataTSO.length; ind++) { rdataTSO[ind].Id = ind + 1; }
				const rdataTSOsorted: tDS21_TicketStatus[] = rdataTSO.sort(function (a, b) {
					var nameA = !a.ID;
					var nameB = !b.ID;
					if (nameA < nameB) {
						return -1;
					}
					if (nameA > nameB) {
						return 1;
					}
					// names must be equal
					return 0;
				}); // ASC
				setdsTSOs(rdataTSOsorted);
				rBoolean = true;
			}
		}
		if (rBoolean) { setLoadingTSO(!rBoolean) };
	}

	React.useEffect(() => {
		if (isLoadingTSO) {
			allServices.PTTSgetServices.refTicketStatusOptionsWithCallback(cbTSO, er); // axios always returns an object with a "data" child
			setLoadingTSO(false);
		}
	}, [isLoadingTSO])

	if (isLoadingTSO) {
		return (<div>gather options<Loader /></div>);
	}
	if (!isReadyToDraw) {
		return (<div>Determining whether NOV#s have a common location.<Loader /></div>);
	}
	else {
		console.log("footer: currentTSO ", currentTSO);
		console.log("footer: AllNOVsHaveThisTSOinCommonState", AllNOVsHaveThisTSOinCommonState);
		return (

			<Grid container>
				<Grid item xs={12} component={Paper} className={classes.paper}>
					<FormControl onSubmit={handleSubmit(submitForm)} >
						<Table>
							<TableHead>
								<TableRow>
									<TableCell size="small" align="right">&nbsp;</TableCell>
									<TableCell size="small" align="center">NOV# above set to:</TableCell>
									<TableCell size="medium" align="center">Message</TableCell>
									<TableCell size="small" align="right">&nbsp;</TableCell>
									<TableCell size="small" align="left">&nbsp;</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								<TableRow>
									<TableCell size="small" align="right">Select one:</TableCell>
									<TableCell size="small" align="center">
										{renderTSO.renderThis}
									</TableCell>
									<TableCell size="medium" align="left">&nbsp;</TableCell>
									<TableCell size="small" align="right">
										<StyledButton className={classes.button}
											id="btnExecute_BUL"
											variant="outlined"
											color="primary" type="submit"
											onClick={handleSubmit(submitForm)} > Update
											</StyledButton>
									</TableCell>
									<TableCell size="small" align="left">
										<StyledButton className={classes.button}
											id="btnClearAll_BUL"
											variant="outlined"
											color="primary" type="submit"
											onClick={handleSubmit(submitForm)} > Clear All</StyledButton>
									</TableCell>
								</TableRow>
							</TableBody>
						</Table>
					</FormControl>
				</Grid>
			</Grid>


		)

	}			 // each voided NOV has one associated DS1704A; this is the UI creating the DS1704A record
}

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5,
		marginTop: 0
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const useStyles = makeStyles({
	paper: {
		width: "100%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 0,
		marginTop: 10,
		overflowX: "visible",
	},
	table: {
		width: "100%",
		margingTop: 80,
		minWidth: 1000,
		display: "inline-block",
	},
	cell: {
		minWidth: 100,
		maxWidth: 220,
	},
	button: {
		marginTop: 20,
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 10,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 14,
		fontWeight: "normal",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 550,
		minWidth: 300,
	}
});
