﻿// prettier-ignore
import { ErrorMessage } from "@hookform/error-message";
import {
    Button,
    Divider, FormControl, FormControlLabel, FormLabel, Grid, Input, InputLabel, Link, Paper, Radio, RadioGroup, Table, TableBody, TableCell,
    TableHead, TableRow, withStyles
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { allActions } from "../../../actions";
import { tOneNOV, UsedTicketRange, tBulkUpdateRow, BulkUpdateNOV_ACTION } from "../../../model";
import { allServices } from '../../../services';
import { PTTSfunctions } from "../../basicComponents";
import Loader from "../../basicComponents/Loader";
import { ErrorMessageContainer, ErrorSummary } from "../../basicComponents/validation";
import { dummyBulkUpdateThisNOV, INITIAL_STATE, tInterForInitialState } from "../../../reducers/DS21Card";


type FormData = {
	fldNOVnumber: number;
}

export function BulkUpdateTable(props: tInterForInitialState) { // expect a table of with up to 20 rows with NOV#s.

	const classes = useStyles();
	const dispatch = useDispatch();
	type HTMLElementEvent<T extends HTMLElement> = Event & {
		target: T;
	}
	let history = useHistory();
	const { register, handleSubmit, errors, setError } = useForm<FormData>();
	//const contentparent: tInterForInitialState | any = !props ? INITIAL_STATE : props; // useSelector((state: any) => state.DS21CardReducer);
	const [contentparent, setContentParent] = useState<tInterForInitialState | any>(!props ? INITIAL_STATE : props);

	const [NOVnumberState, setNOVnumberState] = useState(""); // (!props.aNovNumber ? "" : props.aNovNumber); // match.params because of using ROUTER to get here
	const [SearchForNOVnumberState, setSearchForNOVnumberState] = useState(!contentparent.ST_20BulkUpdateTheseNOVs ? "" : contentparent.ST_20BulkUpdateTheseNOVs[0].NOVnumber); 

	const [IsValidNOVnumberState, setIsValidNOVnumberState] = useState(false);
	const [isLoading, setLoading] = useState(true);

	const cbOneNOVupt = (response: any) => {
		let rdata: tOneNOV = response.data;
		console.log("successful NOV update");
		const allRows: tBulkUpdateRow[] = contentparent.ST_20BulkUpdateTheseNOVs;
		const indRow: number = allRows.findIndex((aRow: tBulkUpdateRow) => aRow.NOVnumber === rdata.ECBSummonsNumber);
		if (indRow > 0) {
			const uptRow: tBulkUpdateRow = allRows[indRow];
			uptRow.TicketStatusID = rdata.TicketStatusID;
			setLoading(!allActions.DS21acts.iudBulkUpdateNOVlist(uptRow, dispatch, BulkUpdateNOV_ACTION.UPT));
		}
		else {
			alert("Cannot find NOV# " + rdata.ECBSummonsNumber + " in the list of entered tickets.")
			setLoading(true);
        }
    }

	React.useEffect(() => {
		const newFirstRow: tBulkUpdateRow = {
			Id: dummyBulkUpdateThisNOV.Id,
			NOVnumber: dummyBulkUpdateThisNOV.NOVnumber,
			Message: dummyBulkUpdateThisNOV.Message, // selection made from the dropdown list
			NOVnumberIsValid: dummyBulkUpdateThisNOV.NOVnumberIsValid,
			TicketStatusID: dummyBulkUpdateThisNOV.TicketStatusID
		};
		console.log("initial react useEffect for Table");
		setLoading(!allActions.DS21acts.iudBulkUpdateNOVlist(newFirstRow, dispatch, BulkUpdateNOV_ACTION.RST));
	}, [])

	React.useEffect(() => { console.log("new props detected", props); setContentParent(props) }, [props])

	React.useEffect(() => {

		if (!IsValidNOVnumberState && NOVnumberState !== ""
			&& (!contentparent.ST_20BulkUpdateTheseNOVs ? "" : contentparent.ST_20BulkUpdateTheseNOVs[0].NOVnumber) !== NOVnumberState) {
			// this allows user to type in each digit until the entry is same as one of the existing NOV#s
			const addRow: tBulkUpdateRow[] = [{ // workaround for push object into array is by ref in javascript; need a copy
				Id: dummyBulkUpdateThisNOV.Id,
				NOVnumber: dummyBulkUpdateThisNOV.NOVnumber,
				Message: dummyBulkUpdateThisNOV.Message,
				NOVnumberIsValid: dummyBulkUpdateThisNOV.NOVnumberIsValid,
				TicketStatusID: dummyBulkUpdateThisNOV.TicketStatusID
			}];
			addRow[0].NOVnumber = NOVnumberState;
			setLoading(!allActions.DS21acts.iudBulkUpdateNOVlist(addRow[0], dispatch, BulkUpdateNOV_ACTION.UPT));
		}
		else if (IsValidNOVnumberState && NOVnumberState !== "") {
			// find NOV# in ST_20BulkUpdateTheseNOVs
			const allRows: tBulkUpdateRow[] = contentparent.ST_20BulkUpdateTheseNOVs;
			const indRow: number = allRows.findIndex((aRow: tBulkUpdateRow) => aRow.NOVnumber === NOVnumberState);
			if (indRow > 0) {
				const aRow: tBulkUpdateRow = allRows[indRow];
				// determine its TicketStatusID
				if (aRow.TicketStatusID < 0) {
					// update ST_20BulkUpdateTheseNOVs
					allServices.PTTSgetServices.oneNOVwithCallBack(NOVnumberState, cbOneNOVupt, er)
				}
				else {
					setLoading(false);
				}
            }
			else {
				setLoading(false);
			}
        }
		else {
			setLoading(false);
		}

	}, [contentparent.ST_20BulkUpdateTheseNOVs, IsValidNOVnumberState]);

	React.useEffect(() => {
		if ((!contentparent.ST_20BulkUpdateTheseNOVs ? 0 : contentparent.ST_20BulkUpdateTheseNOVs.length) === 1 && !isLoading) {
			// allow the same NOV# to be entered as the most recent entry made
			// before clearing the list
			setNOVnumberState("");
		}
	}, [isLoading, contentparent.ST_20BulkUpdateTheseNOVs]);

	React.useEffect(() => {
		setIsValidNOVnumberState(false);
		fIsValidNOVnumber("IsGoodNOVnumber", "Low", Number.parseInt(NOVnumberState));
	}, [NOVnumberState])

	React.useEffect(() => {
		if (IsValidNOVnumberState && NOVnumberState > "") {

			const addRow: tBulkUpdateRow[] = [{
				Id: contentparent.ST_20BulkUpdateTheseNOVs.length + 1,
				NOVnumber: NOVnumberState,
				Message: "", // selection made from the dropdown list
				NOVnumberIsValid: IsValidNOVnumberState,
				TicketStatusID: -13
			}];

			setLoading(!allActions.DS21acts.iudBulkUpdateNOVlist(addRow[0], dispatch, BulkUpdateNOV_ACTION.ADD));
		}
		else if (!IsValidNOVnumberState && NOVnumberState > "") {
			const sameFirstRow: tBulkUpdateRow[] = [{
				Id: 1,
				NOVnumber: NOVnumberState,
				Message: "", // selection made from the dropdown list
				NOVnumberIsValid: IsValidNOVnumberState,
				TicketStatusID: -11
			}];
			setLoading(!allActions.DS21acts.iudBulkUpdateNOVlist(sameFirstRow[0], dispatch, BulkUpdateNOV_ACTION.UPT));
		}
		;
	}, [IsValidNOVnumberState])

	function loggedInUser(): string {
		return (localStorage.getItem('userid') + "").toString(); // to avoid possibly null error
		//const j: any = JSON.parse(curUser);
		//return j.data.userPreferences.userName.toString();
	}

	function handleGetNOV(aNOV: tBulkUpdateRow) {
		if (aNOV.NOVnumber.length < 8) {return}
		history.push('/PTTS/NOVdetails/' + aNOV.NOVnumber)
	}

	function fIsValidNOVnumber(aDSform: string, LowHigh: string, TgtVal: number) {
		//let pIsValidNOVnumber: boolean = false;
		try {
			//(async () => {
			if (!TgtVal || TgtVal < 10000000) {
				return ;
			}
			else {
				let rdata: UsedTicketRange | undefined = undefined;

				const searchECBsummonsNumber: string =
					(TgtVal < 100000000
						? PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(TgtVal)
						: TgtVal).toString();
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Find a ticket range with NOV ", searchECBsummonsNumber);

				//if (!pIsValidNOVnumber) {
				//( async () => {
				const tryThisRange: UsedTicketRange = (
					{
						ECBSummonsNumberFrom: LowHigh === "Low"
							? TgtVal.toString()
							: searchECBsummonsNumber,
						ECBSummonsNumberTo: LowHigh === "High"
							? TgtVal.toString()
							: searchECBsummonsNumber,
						DSform: aDSform,
						ReceiptNumber: "",
					}
				);
				//if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("tryThisRange");
				//if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange);
				// pIsValidNOVnumber = false;
				(async () => {
					if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("call refUsedTicketNumbersWithCallback");
					await allServices.PTTSgetServices.refUsedTicketNumbersWithCallback(tryThisRange, cb, er)
				})();
				//const response: any = await allServices.PTTSgetServices.refUsedTicketNumbers(tryThisRange)

				//// axios always returns an object with a "data" child
				//rdata = await response.data;
				////if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange)
				////if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);

				//// return (async () => {
				//	if (!rdata) {
				//		return ;
				//		// pIsValidNOVnumber = false;
				//		// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ticket number bad based on no rdata object.");
				//		// setIsValidNOVnumberState(false);
				//	}
				//	else if (!rdata.length ? true : rdata.length === 0) {
				//		return ;
				//		//pIsValidNOVnumber = false;
				//		//if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(`Ticket number ${searchECBsummonsNumber} bad based on empty [].`);
				//		// setIsValidNOVnumberState(false);
				//	}
				//	else {
				//		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("found a ticket range for ", searchECBsummonsNumber);

				//		pIsValidNOVnumber =
				//			Number.parseInt(tryThisRange.ECBSummonsNumberFrom) > 0
				//				&& Number.parseInt(tryThisRange.ECBSummonsNumberTo) > 0
				//				? true : false;

				//		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("IsValidNOVnumberState at end of ASYNC", IsValidNOVnumberState);
				//	}
				//if (pIsValidNOVnumber) { await setIsValidNOVnumberState(pIsValidNOVnumber) };
				//})
				// return //	await pIsValidNOVnumber;
            }
			//}
		// })();
		}
		catch (error) {
			alert("BulkUpdateLocation / Table: " + error);
			throw (error); // false;
		}

		// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber after ASYNC", pIsValidNOVnumber);
		
	};

	const cb = (response: any) => {
		let pIsValidNOVnumber: boolean = false;
		// axios always returns an object with a "data" child
		const rdata: UsedTicketRange = response.data;
		if (!rdata) {
			return;
		}
		else if (!rdata.length ? true : rdata.length === 0) {
			return;
		}
		else {
			pIsValidNOVnumber =
				Number.parseInt(rdata[0].ECBSummonsNumberFrom) > 0
				&& Number.parseInt(rdata[0].ECBSummonsNumberTo) > 0
					? true : false;
		}
		if (pIsValidNOVnumber) { setIsValidNOVnumberState(pIsValidNOVnumber) };
	}

	const er = (response: any) => {
		alert("BulkUpdateLocation/Table/er: " + response);
	}

	const handleOnChange = (e: any) => {
		e.preventDefault();
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handleOnChange");
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e);

		switch (e.target.name) {
			case "fldNOVnumberFromTableFirstRow":
				let dummy3n: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				// less than 8 digits, not-a-number, or nine or more digits starting with 3 (Sanitation only prefixes 1 or 2)
				if (dummy3n === NaN) {
					alert("NOV number only has digits. (" + e.target.value +")");
					break;
				}

				if (dummy3n > 299999999) {
					alert("Expected first summons number is supposed to start with 1 or 2 and be nine-digits.  You entered a number that is too big.")
					break;
				}
				if (dummy3n < 99999999) {
					const editFirstRow: tBulkUpdateRow[] = [{
						Id: 1,
						NOVnumber: dummy3n.toString(),
						Message: "too small", // selection made from the dropdown list
						NOVnumberIsValid: false,
						TicketStatusID: -14
					}];
					if (e.target.value.substr(0,1) > "3") {
						editFirstRow[0].Message = "first digit of NOV# is 1 or 2"
					}
					setLoading(!allActions.DS21acts.iudBulkUpdateNOVlist(editFirstRow[0], dispatch, BulkUpdateNOV_ACTION.UPT));
				}
				//            12345678
				if (dummy3n > 99999999) {
					dummy3n = Math.trunc(dummy3n / 10);
					const dummy3: string = PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(dummy3n);
					const matchesExisting: number = 
						contentparent.ST_20BulkUpdateTheseNOVs.findIndex((aRow: tBulkUpdateRow) => aRow.NOVnumber === dummy3)
					if (e.target.value.substr(0, 8) === dummy3.substr(0, 8)
						&& e.target.value.substr(8, 1) !== dummy3.substr(8, 1)) {
						alert("You entered the wrong check-sum (last digit).  Did you intend NOV# " + dummy3 + "?");
                    }
					else if (matchesExisting > 0 || dummy3 === NOVnumberState) {
						alert("You are entering the same NOV# as before, see item # " + (matchesExisting+1).toString() );
					}
					else {
						setNOVnumberState(dummy3);
                    }

				}
				break;
			case "fldNOVnumber":
				let dummy2n: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				if (isNaN(dummy2n)) {
					alert("NOV number is NaN.");
					break;
				}
				if (dummy2n > 299999999) {
					alert("Expected first summons number is supposed to start with 1 or 2 and be nine-digits.  You entered a number that is too big.")
					break;
				} // less than 8 digits, not-a-number, or nine or more digits starting with 3 (Sanitation only prefixes 1 or 2)
				//            12345678
				if (dummy2n > 99999999) {
					dummy2n = Math.trunc(dummy2n / 10);
					//const msg1: string = "check NOV number" + dummy2n.toString();
					//alert(msg1);
					setSearchForNOVnumberState(dummy2n.toString());
					const dummy2: string = PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(dummy2n);

					(async () => {
						await fIsValidNOVnumber("IsGoodNOVnumber", "Low", Number.parseInt(dummy2));
						if (!IsValidNOVnumberState) {
							// true means validation is OKay, value is proper
							if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fIsValidNOVnumber has not found NOV number.")
						}
						else {
							setNOVnumberState(dummy2) // will cause a React.useEffect
						}
					})();

				}
				break;
			default:
				// code block
				const msg: string = "No onChange for " + e.target.name;
				alert(msg);
		}
	}

	const handleRemoveNOV = (chosenNOV: string) => {
		try {
			const lcllstOfRows: tBulkUpdateRow[] = contentparent.ST_20BulkUpdateTheseNOVs;
			// find the NOV# in lstOfRows
			const oneIndex = lcllstOfRows.findIndex((aRow: tBulkUpdateRow) => aRow.NOVnumber === chosenNOV);
			if (!!oneIndex) {
				const aRow: tBulkUpdateRow = lcllstOfRows[oneIndex];
				// remove the NOV#
				if (allActions.DS21acts.iudBulkUpdateNOVlist(aRow, dispatch, BulkUpdateNOV_ACTION.DEL)) {
					setNOVnumberState(""); // trigger the contentparent change (again?)
				};
			}
			else {
				alert(chosenNOV + " is not a valid NOV#.");
			}
		}
		catch (error) {
			alert(error.toString());
        }

	};

	const renderEmpty = () => {
		return (<div>&nbsp;</div>);
	}

	const submitNOV = async (data: FormData, e: any) => {
		e.target.reset(); // reset after form submit
	}

	const submitForm = async (data: FormData, e: any) => {
		if (e.target.innerText = "Clear All") {
			const newFirstRow: tBulkUpdateRow = {
				Id: dummyBulkUpdateThisNOV.Id,
				NOVnumber: dummyBulkUpdateThisNOV.NOVnumber,
				Message: dummyBulkUpdateThisNOV.Message, // selection made from the dropdown list
				NOVnumberIsValid: dummyBulkUpdateThisNOV.NOVnumberIsValid,
				TicketStatusID: dummyBulkUpdateThisNOV.TicketStatusID
			};
			setLoading(!allActions.DS21acts.iudBulkUpdateNOVlist(newFirstRow, dispatch, BulkUpdateNOV_ACTION.RST));
        }
		// e.target.reset(); // reset after form submit
	}

	const renderFirstTableRow = (arrN: tBulkUpdateRow[]) => {
		console.log("arrN is: ", arrN);
		const n: tBulkUpdateRow = arrN[0];
		return (
			<TableRow key={("000000000" + n.Id.toString()).substr(-8) + "row"}>
				<TableCell size="small" key={("000000000" + n.Id.toString()).substr(-8) + "ID"} align="center">{`${n.Id}`}</TableCell>
				<TableCell size="medium" key={("000000000" + n.Id.toString()).substr(-8) + "TKTNO"} align="left" onClick={() => handleGetNOV(n.NOVnumberIsValid ? n : dummyBulkUpdateThisNOV)}>
					<FormControl id={("000000000" + n.Id.toString()).substr(-8) + "FCid"}>
						{/* https://stackoverflow.com/questions/65395896/there-are-multiple-inputbase-components-inside-a-formcontrol */}
						<Input className={classes.styleforticketrangetext}
							type="number"
							id="idNOVnumberFromTableFirstRow"
							onChange={(e) => handleOnChange(e.target.value)}
							placeholder="9-digit ECB Summons Number"
							name="fldNOVnumberFromTableFirstRow"
							value={n.NOVnumber}
						// defaultValue versus value --> value lets me change characters in the currently active Input control
						/>
						<div>&nbsp;</div>
						<ErrorMessage
							errors={errors}
							name="fldNOVnumberFromTableFirstRow"
							as={<ErrorMessageContainer />}
						/>
					</FormControl>
				</TableCell>
				<TableCell size="medium" key={("000000000" + n.Id.toString()).substr(-8) + "MESSAGE"} align="left">{n.Message}</TableCell>
				{(n.NOVnumber.length === 0)
					? <TableCell>&larr; Type or paste a NOV#</TableCell>
					: (n.NOVnumberIsValid)
						?
						<TableCell>
							{/* &larr; Type a NOV# */}
							<button onClick={() => setNOVnumberState(n.NOVnumber)}>Add</button>
						</TableCell>
						:
						<TableCell size="small" key={("000000000" + n.Id.toString()).substr(-8) + "REMOVE"} align="left">
							<button onClick={() => handleRemoveNOV(n.NOVnumber)}>Remove</button>
						</TableCell>
				}
			</TableRow>
		)
    }

	const renderTableRow = (n: tBulkUpdateRow) => {
		console.log("n is: ", n);
		if (n.Id !== 1) { // NOV# already entered and verified
			return (
				<TableRow key={("000000000" + n.Id.toString()).substr(-8) + "row"}>
					<TableCell size="small" key={("000000000" + n.Id.toString()).substr(-8) + "ID"} align="center">{`${n.Id}`}</TableCell>
					<TableCell size="medium" key={("000000000" + n.Id.toString()).substr(-8) + "TKTNO"} align="left" onClick={() => handleGetNOV(n.NOVnumberIsValid ? n : dummyBulkUpdateThisNOV)}>
						<FormControl id={("000000000" + n.Id.toString()).substr(-8) + "FCid"}>
							{/* https://stackoverflow.com/questions/65395896/there-are-multiple-inputbase-components-inside-a-formcontrol */}
							<Input className={classes.styleforticketrangetext}
								type="number"
								id="idNOVnumberEnteredAndVerified"
								name="fldNOVnumberEnteredAndVerified"
								value={n.NOVnumber}
							// defaultValue versus value --> value lets me change characters in the currently active Input control
							/>
						</FormControl>
					</TableCell>
					<TableCell size="medium" key={("000000000" + n.Id.toString()).substr(-8) + "MESSAGE"} align="left">{n.Message}</TableCell>
					<TableCell size="small" key={("000000000" + n.Id.toString()).substr(-8) + "REMOVE"} align="left">
						<button name="btnNOVnumberEnteredAndVerified"
							 onClick={() => handleRemoveNOV(n.NOVnumber)}>Remove</button>
					</TableCell>
				</TableRow>
			)
		}
		else { // NOV# being enetered in the first row
			return ( 
				<TableRow key={("000000000" + n.Id.toString()).substr(-8) + "row"}>
					<TableCell size="small" key={("000000000" + n.Id.toString()).substr(-8) + "ID"} align="center">{`${n.Id}`}</TableCell>
					<TableCell size="medium" key={("000000000" + n.Id.toString()).substr(-8) + "TKTNO"} align="left" onClick={() => handleGetNOV(n.NOVnumberIsValid ? n : dummyBulkUpdateThisNOV)}>
						<FormControl id={("000000000" + n.Id.toString()).substr(-8) + "FCid"}>
							{/* https://stackoverflow.com/questions/65395896/there-are-multiple-inputbase-components-inside-a-formcontrol */}
							<Input className={classes.styleforticketrangetext}
								type="number"
								id="idNOVnumberFromTable"
								onChange={(e) => handleOnChange(e)}
								placeholder="9-digit ECB Summons Number"
								name="fldNOVnumberFromTableFirstRow"
								value={n.NOVnumber}
							// defaultValue versus value --> value lets me change characters in the currently active Input control
							/>
							<div>&nbsp;</div>
							<ErrorMessage
								errors={errors}
								name="fldNOVnumberFromTableFirstRow"
								as={<ErrorMessageContainer />}
							/>
						</FormControl>
					</TableCell>
					<TableCell size="medium" key={("000000000" + n.Id.toString()).substr(-8) + "MESSAGE"} align="left">{n.Message}</TableCell>
					{(n.NOVnumber.length === 0)
						? <TableCell>&larr; Type or paste a NOV#</TableCell>
						: (n.NOVnumberIsValid)
							?
							<TableCell>
								{/* &larr; Type a NOV# */}
								<button onClick={() => setNOVnumberState(n.NOVnumber) }>Add</button>
							</TableCell>
							:
							<TableCell size="small" key={("000000000" + n.Id.toString()).substr(-8) + "REMOVE"} align="left">
								<button onClick={() => handleRemoveNOV(n.NOVnumber)}>Remove</button>
							</TableCell>
					}
				</TableRow>
				)
        }

    }

	if (isLoading) {
		return (<Loader />);
	}
	else {
		return (
			<Grid container>
				<Grid item xs={12} component={Paper} className={classes.paper}>
					<FormControl  >
						<ErrorSummary errors={errors} />

						<InputLabel htmlFor="idNOVnumber">{SearchForNOVnumberState.length < 9
							? <div>Search for this ECB Summons Number: {SearchForNOVnumberState} </div>
							: IsValidNOVnumberState
								? < div > {SearchForNOVnumberState} is valid; click 'Go to {SearchForNOVnumberState}'.</div>
								: < div > {SearchForNOVnumberState} is not on any existing DS-1984.</div>}
						</InputLabel>
						<Input className={classes.styleforticketrangetext}
							type="number"
							id="idNOVnumber"
							onChange={(e) => handleOnChange(e)}
							placeholder="9-digit ECB Summons Number"
							name="fldNOVnumber"
							ref={register({
								required: { value: true, message: "PTTS expects the first NOV number (nine digits)." },
								min: {
									value: 100000000,
									message: "Too small. Individual ECB Summons are nine digit numbers starting with 1 or 2."
								},
								max: {
									value: 299999999,
									message: "Too big.  Individual ECB Summons are nine digit numbers starting with 1 or 2."
								},
							})}
							value={SearchForNOVnumberState}
						// defaultValue versus value --> value lets me change characters in the currently active control textbox
						/>
						<div>&nbsp;</div>
						<ErrorMessage
							errors={errors}
							name="fldNOVnumber"
							as={<ErrorMessageContainer />}
						/>
						{IsValidNOVnumberState
							?
							<button name="btnNOVnumber" type="submit" onClick={(e) => { e.preventDefault(); history.push('/PTTS/NOVdetails/' + SearchForNOVnumberState); }}>Go to {SearchForNOVnumberState}</button>
							:
							<div>&nbsp;</div>
						}
						<Divider />
					</FormControl>
				</Grid>
				<Grid item xs={12} component={Paper} className={classes.paper}>
					<FormControl  >
						<ErrorSummary errors={errors} />
						<Table className={classes.table} aria-label="a dense table" >
						<TableHead>
							<TableRow>
								<TableCell size="small" align="left">Description</TableCell>
								<TableCell size="medium" align="left">Ticket No</TableCell>
								<TableCell size="medium" align="right">Message</TableCell>
								<TableCell size="small" align="left">&nbsp;</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
								{
									isLoading //  || !dsListPayCodeFilter || dsListPayCodeFilter === undefined
										|| !contentparent.ST_20BulkUpdateTheseNOVs
										? <TableRow>
											<TableCell size="small">waiting for redux store content ... {isLoading ? "(loading)" : " (contentparent.ST_20BulkUpdateTheseNOVs empty) "}
											</TableCell>
										</TableRow>
										: !contentparent.ST_20BulkUpdateTheseNOVs.length 
											? renderFirstTableRow(contentparent.ST_20BulkUpdateTheseNOVs)
											: contentparent.ST_20BulkUpdateTheseNOVs.map((n: tBulkUpdateRow) => {
												console.log("render row: " + n.Id + " of " + contentparent.ST_20BulkUpdateTheseNOVs.length);
												return (
													renderTableRow(n)
														)
										})
							}
						</TableBody>
						</Table>
					</FormControl>
				</Grid>

			</Grid>

		)
	}
}

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5,
		marginTop: 0
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);


const useStyles = makeStyles({
	paper: {
		width: "100%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 0,
		marginTop: 10,
		overflowX: "visible",
	},
	table: {
		width: "100%",
		margingTop: 80,
		minWidth: 1000,
		display: "inline-block",
	},
	cell: {
		minWidth: 100,
		maxWidth: 220,
	},
	button: {
		marginTop: 20,
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 10,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 14,
		fontWeight: "normal",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 550,
		minWidth: 300,
	}
});
