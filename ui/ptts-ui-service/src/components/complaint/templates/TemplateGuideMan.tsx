﻿import { Button, Box } from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';
import React, { useEffect } from 'react';
import { useForm } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers';
import styles from "./templateStyles";

interface Props {
	open: boolean;
	onSubmit: (statementValue: string) => void;
	onClose: (event: any) => void;
	respondent: string;
	incidentDate: string;
}

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const StyledButtonGreen = withStyles({
	root: {
		background: '#2b995f',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

export function TemplateGuideMan(props: Props) {
	const classes = styles();

	const dialogSchema = yup.object().shape({
		onDate: yup.string().required("Required field"),
		atTime: yup.string().required("Required field"),
		vehicleCode: yup.string().required("Required field"),
		collisionAt: yup.string().required("Required field")
	});

	const { register, handleSubmit, errors, setValue, clearErrors } = useForm({
		resolver: yupResolver(dialogSchema)
	});

	const formatDate = (value: string) => {
		let date = value.split('-');
		const year = String(date[0]).split('-');
		const month = String(date[1]).split('-');
		const day = String(date[2]).split('-');
		return month + "/" + day + "/" + year;
	}

	React.useEffect(() => {
		register({ name: "onDate" });
		register({ name: "atTime" });
		register({ name: "vehicleCode" });
		register({ name: "collisionAt" });
	}, []);

	React.useEffect(() => {
		setValue("onDate", props.incidentDate);
	}, [props.incidentDate]);

	React.useEffect(() => {
		setValue("onDate", props.incidentDate);
		setValue("atTime", "");
		setValue("vehicleCode", "");
		setValue("collisionAt", "");

		clearErrors("onDate");
		clearErrors("atTime");
		clearErrors("vehicleCode");
		clearErrors("collisionAt");
	}, [props.open]);

	const onSubmit = (data: any) => {
		let templateOutput = "The Safety Unit has determined you were negligent on " + formatDate(data.onDate) + " at approximately " + data.atTime + " hours. While guiding Vehicle Code # " + data.vehicleCode;
		templateOutput = templateOutput + " in a backward direction you were involved in a collision at " + data.collisionAt + " that resulted in property damage and/or personal injury.";		
		props.onSubmit(templateOutput);
	};

	const handleChange = (e: any, control?: string) => {
		if (control === "onDate") {
			setValue("onDate", e.target.value);
			clearErrors("onDate");
		}
		else if (control === "atTime") {
			setValue("atTime", e.target.value);
			clearErrors("atTime");
		}
		else if (control === "vehicleCode") {
			setValue("vehicleCode", e.target.value);
			clearErrors("vehicleCode");
		}
		else if (control === "collisionAt") {
			setValue("collisionAt", e.target.value);
			clearErrors("collisionAt");
		}		
	};

	const handleCancel = (event: any) => {
		props.onClose("");
	};

	const pad = (n: any) => {
		return ("00" + n).slice(-2);
	}

	const todaysDate = () => {
		const tempDate = new Date();
		const currDate = tempDate.getFullYear() + '-' + pad((tempDate.getMonth() + 1)) + '-' + pad((tempDate.getDate()));
		return currDate;
	}

	return (
		<form autoComplete="off">
			<Box className={classes.box}>
				<Grid container className={classes.grid} spacing={0}>
					<Grid item xs={12} className={classes.gridCellTemplate}>
						<label className={classes.labelTemplateText}>The Safety Unit has determined you were negligent on </label>
						{errors.onDate && <label className={classes.error}>* </label>}
						<input type="date"
							name="onDate"
							defaultValue={props.incidentDate}
							placeholder="Select Date"
							className={classes.datepicker}
							max={todaysDate()}
							onChange={e => handleChange(e, "onDate")}
						/>
						<label className={classes.labelTemplateText}> at approximately </label>
						{errors.atTime && <label className={classes.error}>* </label>}
						<input name="atTime" className={classes.input} onChange={e => handleChange(e, "atTime")} />
						<label className={classes.labelTemplateText}> hours. While guiding Vehicle Code # </label>
						{errors.vehicleCode && <label className={classes.error}>* </label>}
						<input name="vehicleCode" className={classes.inputLong} onChange={e => handleChange(e, "vehicleCode")} />
						<label className={classes.labelTemplateText}> in a backward direction you were involved in a collision at </label>
						{errors.collisionAt && <label className={classes.error}>* </label>}
						<input name="collisionAt" className={classes.inputLong} onChange={e => handleChange(e, "collisionAt")} />
						<label className={classes.labelTemplateText}> that resulted in property damage and/or personal injury.</label>
					</Grid>
				</Grid>
			</Box>
			<StyledButtonGreen
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleSubmit(onSubmit)}
			>Confirm
				    </StyledButtonGreen>
			<StyledButton
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleCancel}>
				Cancel
				</StyledButton>
		</form>
	);
}

