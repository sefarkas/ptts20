// prettier-ignore
import { Button, Link } from "@material-ui/core";
import Modal from "react-modal";
import { makeStyles } from "@material-ui/styles";
import { withStyles } from '@material-ui/core/styles';
import * as React from "react";
import ReactSelect, { components }  from "react-select";
import { useForm } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import { history } from "../../configureStore";
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers';
import { referenceActions } from '../../actions';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";

interface Props {
	open: boolean;
	onClose: () => void;
}

library.add(faSearch);

const CaretDownIcon = () => {
	return <FontAwesomeIcon icon="search" />;
};

const DropdownIndicator = (props: any) => {
	return (
		<components.DropdownIndicator {...props}>
			<CaretDownIcon />
		</components.DropdownIndicator>
	);
};

const LoginLogo = {
	logoImg: require('../../assets/images/dsny_logo.png')
}

const StyledButton = withStyles({
	root: {
		background: '#2b995f',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

export function ComplaintDialog(props: Props) {
	const { open, onClose } = props;
	const classes = useStyles();
	const [employeeOpts, setEmployeeOpts] = React.useState([{ value: 0, label: "" }]);
	const [isLoading, setLoading] = React.useState(false);
	const [nameSearch, setNameSearch] = React.useState("");
	
	const dialogSchema = yup.object().shape({
		employeeRefNo: yup.string().required("Employee is a required field"),
	});

	const { register, handleSubmit, errors, setValue, clearErrors } = useForm({
		resolver: yupResolver(dialogSchema)	
	});

	React.useEffect(() => {
		register({ name: "employeeRefNo" });
	}, []);

	React.useEffect(() => {
		if (nameSearch.length > 2) { 
			(async () => {
				setLoading(true);
				const response = await referenceActions.employeeListByLastName(nameSearch);
				setLoading(false);

				const opts = response.data.map((person: any) => {
					return (
						{ value: person.employeeId, label: person.names[0].lastName + ', ' + person.names[0].firstName + ' #' + person.employeeId }
					)
				}).sort((a: any, b: any) => (a.label > b.label) ? 1 : -1);

				setEmployeeOpts(opts);
			})();
	    }
	}, [nameSearch]);

	const handleChange = (event: any) => {		
		if (event !== null && event !== undefined) {
			setValue("employeeRefNo", event.value);
			clearErrors("employeeRefNo");
		}
		else {
			setValue("employeeRefNo", "");
        }			
	};

	const handleInputChange = (e: any) => {	
		if (e !== null && e !== undefined) { 			
			if (e.length > 2 ) {				
				setNameSearch(e);				
			}
		}
	};

	const onSubmit = (data: any) => {	
		history.push('/ds249/newComplaint', data.employeeRefNo);
	};

	const backToList = (event: any) => {
		event.preventDefault();
		clearErrors("employeeRefNo");
		onClose();
	};

	Modal.setAppElement('#root');

	return (
		<Modal
			isOpen={open}
			shouldCloseOnOverlayClick={false}
			onRequestClose={onClose}
			style={
				{
					overlay: {
						// backgroundColor: "grey"
					},
					content: {
						backgroundColor: "#edeff0",
						padding: '20px',
						blockSize: '500px',
						position: "relative",
						top: "10%",
						left: "30%",
						maxWidth: "700px"
					}
				}
			}
		>  <div className={classes.divLogo}>
				<img src={LoginLogo.logoImg} alt="" className={classes.logo} /><span className={classes.divLogoText}> sanitation
				<span className={classes.divLogoText2}> Employee Disciplinary Complaint System</span>
					<span className={classes.divLogoText2}>  | </span>
					<span className={classes.divLogoText2}> DS249 </span></span>
			</div>
			<div className={classes.headerSpace}></div>
			<Link href="#" onClick={backToList} className={classes.link}>
				&lt; Back to List
			</Link>
			<h2 className={classes.h2}>New Complaint</h2>
			<form>
			<Grid container className={classes.grid} spacing={2}>
				<Grid item xs={1} />
				<Grid item xs={7}>
						<div className={classes.labelInstruction}>Please find the respondent you are filing the complaint against</div>										
						<label className={classes.label}>Search Employee</label>{errors.employeeRefNo && <label className={classes.error}>*</label>}										
						<ReactSelect
							onChange={handleChange}
							onInputChange={e => handleInputChange(e)}
							options={employeeOpts}
							name="employeeRefNo"							
							placeholder="Select..."	
							className={classes.reactselect}
							maxMenuHeight={220}
							components={{ DropdownIndicator }}
							theme={theme => ({
								...theme,
								borderRadius: 1,
								colors: {
									...theme.colors,
									primary25: '#d2f6e3',
									primary50: '#2b995f',
									primary: '#2b995f',
								},
							})}
						/>
						{errors.employeeRefNo && <p className={classes.error}>{errors.employeeRefNo.message}</p>}
				</Grid>
				<Grid item xs={3}>
					<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={handleSubmit(onSubmit)}>
						Continue
				</StyledButton>
					<Grid item xs={1} />
				</Grid>
				</Grid>
				</form>
		</Modal>
	);
}

const useStyles = makeStyles({
	dialog: {
		maxWidth: 200,
		minHeight: 900,
		margin: 20,
		backgroundColor: "lightGrey"
	},
	reactselect: {
		maxWidth: 500,		
		height: 25,		
	},
	input: {
		display: "block",
		width: "90%",
		borderRadius: 1,
		border: "1px solid",
		borderColor: "#c5c8c5",
		padding: "10px 10px",
		fontSize: 12,
		'&:focus': {
			outlineColor: '#2b995f',
		}
	},
	link: {
		color: "green",
		fontSize: 12
	},
	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "inline-block",
		marginBottom: 5,
		marginTop: 20,
		fontSize: 12,
		fontWeight: 800,
		color: "#2e2e2d",
	},
	labelInstruction: {
		marginBottom: 10,
		marginTop: 10,
		color: "#2e2e2d",
		fontSize: 12
	},
	button: {
		marginTop: "85px",		
		height: "38px",	
		width: "110px",
		'&:hover': {
			backgroundColor: "#7bb998"
		}		
	},
	grid: {
		background: "white",
		minHeight: 350,

	},
	dsnyLogo: {
		width: "50px",
		height: "50px",
		display: "block",
		"font-family": "Helvetica Neue",
		margin: "0 auto",
		paddingTop: "10%",
	},
	logo: {
		height: "20px",
		width: "20px"
	},
	divLogo: {
		backgroundColor: "#244514",
		margin: -20,
		display: "block",
		paddingLeft: "1%",
		paddingTop: "1%",
		borderColor: "#244514",
		verticalAlign: "middle"
	},
	divLogoText: {
		backgroundColor: "#244514",
		color: "white",
		fontSize: 14,
		fontWeight: 500,
		verticalAlign: "top"
	},
	divLogoText2: {
		color: "white",
		fontSize: 10,
		margin: "1%"
	},
	headerSpace: {
		height: 30
	},
	h2: {
		marginTop: "0px",
		marginBottom: "20px"
	},
	error: {
		fontSize: 12,
		color: "#ef1207",
	}
});
