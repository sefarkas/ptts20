﻿import { Button, Box } from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';
import React, { useEffect } from 'react';
import ReactSelect from "react-select";
import { useForm } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers';
import styles from "./templateStyles";

interface Props {
	open: boolean;
	onSubmit: (statementValue: string) => void;
	onClose: (event: any) => void;
	respondent: string;
	incidentDate: string;
}

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const StyledButtonGreen = withStyles({
	root: {
		background: '#2b995f',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const customStyles = {
	control: (base: any) => ({
		...base,
		minHeight: '30px',
		height: '30px',
	}),
	dropdownIndicator: (base: any) => ({
		...base,
		padding: 0
	}),
	clearIndicator: (base: any) => ({
		...base,
		padding: 0
	}),
	valueContainer: (base: any) => ({
		...base,
		height: '30px',
		padding: '0 2px'
	}),
	indicatorContainer: (base: any) => ({
		height: '30px'
	}),
	options: (base: any) => ({
		height: '0px',
		padding: '0 0px'
	})
};

const titleOpts = [
	{ value: 1, label: "A/M" },
	{ value: 2, label: "S/W" },
	{ value: 3, label: "Supv" },
	{ value: 4, label: "GS" },
	{ value: 5, label: "Other" }
];

export function TemplateAbsentWithoutAuthority(props: Props) {
	const classes = styles();
	const [templateValues, setTemplateValues] = React.useState({title: { value: 0, label: "" }});

	const dialogSchema = yup.object().shape({
		onDate: yup.string().required("Required field"),
		title: yup.string().required("Required field"),
		hoursDocked: yup.number().required("Required field")
	});

	const { register, handleSubmit, errors, setValue, clearErrors, getValues } = useForm({
		resolver: yupResolver(dialogSchema)
	});

	const formatDate = (value: string) => {
		let date = value.split('-');
		const year = String(date[0]).split('-');
		const month = String(date[1]).split('-');
		const day = String(date[2]).split('-');
		return month + "/" + day + "/" + year;
	}

	React.useEffect(() => {
		register({ name: "onDate" });
		register({ name: "onDateAlt" });
		register({ name: "title" });
		register({ name: "hoursDocked" });
	}, []);

	React.useEffect(() => {
		setValue("onDate", props.incidentDate);
	}, [props.incidentDate]);

	React.useEffect(() => {
		setValue("onDate", props.incidentDate);
		setValue("hoursDocked", 8);
		setValue("onDateAlt", "");		

		clearErrors("onDate");
		clearErrors("title");
		clearErrors("onDateAlt");
		clearErrors("hoursDocked");
	}, [props.open]);

	const onSubmit = (data: any) => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(formatDate(data.onDateAlt));
		const title = templateValues.title.label === "Other" ? "" : templateValues.title.label + " ";
		const altIncidentDate = data.onDateAlt === "" ? "" : ", " + formatDate(data.onDateAlt);
		let templateOutput = "On " + formatDate(data.onDate) + altIncidentDate + ", " + title + props.respondent + " was absent for his/her assigned shift, he/she did not call and had no authorization to be off. ";
		templateOutput = templateOutput + title + props.respondent + " has been marked absent and docked " + data.hoursDocked + " hours pay. This incident occurred on ";
		templateOutput = templateOutput + formatDate(data.onDate) + altIncidentDate + ".";
		props.onSubmit(templateOutput);
	};

	const handleChange = (e: any, control?: string) => {
		if (control === "onDate") {
			setValue("onDate", e.target.value);
			clearErrors("onDate");
		}
		else if (control === "title") {
			const selected = titleOpts.filter((d) => { return d.value === e.value; })[0].label;
			setTemplateValues({
				...templateValues,
				title: { value: e.value, label: selected }
			});
			setValue("title", e.value);
			clearErrors("title");
		}
		else if (control === "onDateAlt") {
			setValue("onDateAlt", e.target.value);
			clearErrors("onDateAlt");
		}	
		else if (control === "hoursDocked") {
			setValue("hoursDocked", e.target.value);
			clearErrors("hoursDocked");
		}
	};

	const handleCancel = (event: any) => {
		props.onClose("");
	};

	const pad = (n: any) => {
		return ("00" + n).slice(-2);
	}

	const todaysDate = () => {
		const tempDate = new Date();
		const currDate = tempDate.getFullYear() + '-' + pad((tempDate.getMonth() + 1)) + '-' + pad((tempDate.getDate()));
		return currDate;
	}

	return (
		<form autoComplete="off">
			<Box className={classes.box}>
				<Grid container className={classes.grid} spacing={0}>
					<Grid item xs={12} className={classes.gridCellTemplate}>
						<label className={classes.labelTemplateText}>On </label>
						{errors.onDate && <label className={classes.error}>*</label>}
						<input type="date"
							name="onDate"
							defaultValue={props.incidentDate}
							placeholder="Select Date"
							className={classes.datepicker}
							max={todaysDate()}
							onChange={e => handleChange(e, "onDate")}
						/>
						<label className={classes.labelTemplateText}>, </label>
						<input type="date"
							name="onDateAlt"
							placeholder="Select Date"
							className={classes.datepicker}
							max={todaysDate()}
							onChange={e => handleChange(e, "onDateAlt")}
						/>
						<label className={classes.labelTemplateText}>, </label>	
						{errors.title && <label className={classes.error}>*</label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})} /></div>
						<label className={classes.labelTemplateText}> {props.respondent} was absent for his/her assigned shift, he/she did not call and had no authorization to be off. </label>
						{errors.title && <label className={classes.error}>*</label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})} /></div>
						<label className={classes.labelTemplateText}> {props.respondent} has been marked absent and docked </label>						
						{errors.hoursDocked && <label className={classes.error}>*</label>}
						<input name="hoursDocked" className={classes.input} onChange={e => handleChange(e, "hoursDocked")} defaultValue={getValues("hoursDocked")} />
						<label className={classes.labelTemplateText}> hours pay. This incident occurred on </label>
						{errors.onDate && <label className={classes.error}>*</label>}
						<input type="date"
							name="onDate"
							defaultValue={getValues("onDate")} 
							placeholder="Select Date"
							className={classes.datepicker}
							max={todaysDate()}
							onChange={e => handleChange(e, "onDate")}
						/>
						<label className={classes.labelTemplateText}>, </label>
						<input type="date"
							name="onDateAlt"
							placeholder="Select Date"
							defaultValue={getValues("onDateAlt")} 
							className={classes.datepicker}
							max={todaysDate()}
							onChange={e => handleChange(e, "onDateAlt")}
						/>
						<label className={classes.labelTemplateText}>.</label>
					</Grid>
				</Grid>
			</Box>
			<StyledButtonGreen
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleSubmit(onSubmit)}
			>Confirm
				    </StyledButtonGreen>
			<StyledButton
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleCancel}>
				Cancel
				</StyledButton>
		</form>
	);
}

