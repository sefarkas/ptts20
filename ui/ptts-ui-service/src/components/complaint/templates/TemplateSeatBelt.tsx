﻿import { Button, Box } from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';
import React, { useEffect } from 'react';
import { useForm } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers';
import styles from "./templateStyles";

interface Props {
	open: boolean;
	onSubmit: (statementValue: string) => void;
	onClose: (event: any) => void;
	respondent: string;
	incidentDate: string;
}

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const StyledButtonGreen = withStyles({
	root: {
		background: '#2b995f',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);


export function TemplateSeatBelt(props: Props) {
	const classes = styles();

	const dialogSchema = yup.object().shape({
		onDate: yup.string().required("Required field"),
		atTime: yup.string().required("Required field"),
		vehicleCode: yup.string().required("Required field"),
		observedBy: yup.string().required("Required field"),
		location: yup.string().required("Required field")
	});

	const { register, handleSubmit, errors, setValue, clearErrors, getValues } = useForm({
		resolver: yupResolver(dialogSchema)
	});

	const formatDate = (value: string) => {
		let date = value.split('-');
		const year = String(date[0]).split('-');
		const month = String(date[1]).split('-');
		const day = String(date[2]).split('-');
		return month + "/" + day + "/" + year;
	}

	React.useEffect(() => {
		register({ name: "onDate" });
		register({ name: "atTime" });
		register({ name: "vehicleCode" });
		register({ name: "observedBy" });
		register({ name: "location" });
	}, []);

	React.useEffect(() => {
		setValue("onDate", props.incidentDate);
	}, [props.incidentDate]);

	React.useEffect(() => {
		setValue("onDate", props.incidentDate);
		setValue("atTime", "");
		setValue("vehicleCode", "");
		setValue("observedBy", "");
		setValue("location", "");

		clearErrors("onDate");
		clearErrors("atTime");
		clearErrors("vehicleCode");
		clearErrors("observedBy");
		clearErrors("location");
	}, [props.open]);

	const onSubmit = (data: any) => {
		let templateOutput = "On " + formatDate(data.onDate) + " at approximately " + data.atTime + " hours ypu were abserved by " + data.observedBy;
		templateOutput = templateOutput + " operating/occupying Vehicle Code # " + data.vehicleCode + " at " + data.location;
		templateOutput = templateOutput + " You were not wearing a seat belt at the time. This is a direct violation of the above orders and rules."
		props.onSubmit(templateOutput);
	};

	const handleChange = (e: any, control?: string) => {
		if (control === "onDate") {
			setValue("onDate", e.target.value);
			clearErrors("onDate");
		}
		else if (control === "atTime") {
			setValue("atTime", e.target.value);
			clearErrors("atTime");
		}
		else if (control === "vehicleCode") {
			setValue("vehicleCode", e.target.value);
			clearErrors("vehicleCode");
		}
		else if (control === "observedBy") {
			setValue("observedBy", e.target.value);
			clearErrors("observedBy");
		}
		else if (control === "location") {
			setValue("location", e.target.value);
			clearErrors("location");
		}
	};

	const handleCancel = (event: any) => {
		props.onClose("");
	};

	const pad = (n: any) => {
		return ("00" + n).slice(-2);
	}

	const todaysDate = () => {
		const tempDate = new Date();
		const currDate = tempDate.getFullYear() + '-' + pad((tempDate.getMonth() + 1)) + '-' + pad((tempDate.getDate()));
		return currDate;
	}

	return (
		<form autoComplete="off">
			<Box className={classes.box}>
				<Grid container className={classes.grid} spacing={0}>
					<Grid item xs={12} className={classes.gridCellTemplate}>
						<label className={classes.labelTemplateText}>On </label>
						{errors.onDate && <label className={classes.error}>* </label>}
						<input type="date"
							name="onDate"
							defaultValue={props.incidentDate}
							placeholder="Select Date"
							className={classes.datepicker}
							max={todaysDate()}
							onChange={e => handleChange(e, "onDate")}
						/>
						<label className={classes.labelTemplateText}> at approximately </label>
						{errors.atTime && <label className={classes.error}>* </label>}
						<input name="atTime" className={classes.input} onChange={e => handleChange(e, "atTime")} />
						<label className={classes.labelTemplateText}> hours you were observed by </label>
						{errors.observedBy && <label className={classes.error}>* </label>}
						<input name="observedBy" className={classes.inputLong} onChange={e => handleChange(e, "observedBy")} />
						<label className={classes.labelTemplateText}> operating/occupying Vehicle Code # </label>
						{errors.vehicleCode && <label className={classes.error}>* </label>}
						<input name="vehicleCode" className={classes.input} onChange={e => handleChange(e, "vehicleCode")} />
						<label className={classes.labelTemplateText}> at </label>
						{errors.location && <label className={classes.error}>* </label>}
						<input name="location" className={classes.inputLong} onChange={e => handleChange(e, "location")} />
						<label className={classes.labelTemplateText}> You were not wearing a seat belt at the time. This is a direct violation of the above orders and rules.</label>
					</Grid>
				</Grid>
			</Box>
			<StyledButtonGreen
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleSubmit(onSubmit)}
			>Confirm
				    </StyledButtonGreen>
			<StyledButton
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleCancel}>
				Cancel
				</StyledButton>
		</form>
	);
}

