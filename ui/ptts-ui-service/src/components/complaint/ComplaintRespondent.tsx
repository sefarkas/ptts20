﻿import React from 'react';
import { useFormContext } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import { Typography, FormControlLabel, Checkbox } from "@material-ui/core";
import styles from "./complaintStyles";
import ReactSelect from "react-select";

const PhotoIcon = {
	photoIcon: require('../../assets/images/photo_icon.png')
};

interface Props {
	employee: any;
	district: any;
	borough: any;
	districtOptions: any;
	boroughOptions: any;
	handleChange: (e: any, control?: string, idx?: number) => void;
	handleShowPhoto: () => void;
}

//React.useEffect(() => {
//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props.employeeNew);
//}, []);

export function ComplaintRespondent(props: Props) {
	const classes = styles();
	const methods = useFormContext();

	const formatDate = (value: string) => {
		let date = value.split('-');
		const year = String(date[0]).split('-');
		const month = String(date[1]).split('-');
		const day = String(date[2]).split('-');
		return month + "/" + day + "/" + year;
	}

	return (
		<Grid container className={classes.grid} spacing={0}>
			<Grid item xs={3}>
				<label className={classes.labelReadOnlyData}>Section 1</label>
				<label className={classes.labelSectionTitle}>Respondent Information</label>
			</Grid>
			<Grid container item xs={9} spacing={0}>
				<Grid item xs={6}>
					<label className={classes.label}>Name</label>
					<label className={classes.labelReadOnlyData}>{props.employee.name}</label>
				</Grid>
				<Grid item xs={6}>
					<label className={classes.label}>Date of Birth</label>
					<label className={classes.labelReadOnlyData}>{formatDate(props.employee.dateOfBirth)}</label>
				</Grid>
			</Grid>
			<Grid item xs={3}>				
				{props.employee.status === "ACTIVE" ?
					<div><img src={PhotoIcon.photoIcon} alt="" className={classes.logo} onClick={props.handleShowPhoto} />
					<label className={classes.link} onClick={props.handleShowPhoto}>Employee Photo</label></div> : null}	
			</Grid>
			<Grid container item xs={9} spacing={0}>
				<Grid item xs={6} className={classes.gridCell}>
					<label className={classes.label}>Home Address</label>
					<label className={classes.labelReadOnlyData}>{props.employee.homeAddress}</label>
				</Grid>
				<Grid item xs={6} className={classes.gridCell}>
					<label className={classes.label}>Payroll Location</label>
					<label className={classes.labelReadOnlyData}>{props.employee.payrollLocation}</label>
				</Grid>
				<Grid item xs={6} className={classes.gridCell}>
					<label className={classes.label}>Title</label>
					<label className={classes.labelReadOnlyData}>{props.employee.title}</label>
				</Grid>
				<Grid item xs={6} className={classes.gridCell}>
					<label className={classes.label}>Employee Reference No.</label>
					<label className={classes.labelReadOnlyData}>{props.employee.referenceNo}</label>
				</Grid>
				<Grid item xs={6} className={classes.gridCell}>
					<label className={classes.label}>Badge No.</label>
					<label className={classes.labelReadOnlyData}>{props.employee.badgeNo}</label>
				</Grid>
				<Grid item xs={6} className={classes.gridCell}>
					<label className={classes.label}>Chart Day No.</label>
					<label className={classes.labelReadOnlyData}>{props.employee.chartNo}</label>
				</Grid>
				<Grid item xs={6} className={classes.gridCell}>
					<label className={classes.label}>Hire Date</label>
					<label className={classes.labelReadOnlyData}>{formatDate(props.employee.hireDate)}</label>
				</Grid>
				<Grid item xs={6} className={classes.gridCell}>
					<label className={classes.label}>Promotion date</label>
					<label className={classes.labelReadOnlyData}>{formatDate(props.employee.promotionDate)}</label>
				</Grid>
				<Grid item xs={6}>
					<label className={classes.label}>Vacation Schedule</label>
					<label className={classes.labelReadOnlyData}>{props.employee.vacationSchedule}</label>
				</Grid>
				<Grid item xs={6}>
					<label className={classes.label}>Employee Is Under Probation</label>
					<FormControlLabel
						value="probationYes"
						control={<Checkbox size="small" color="default" checked={props.employee.underProbation === "true"} className={classes.checkbox} />}
						label={<Typography className={classes.label}>Yes</Typography>}
						labelPlacement="end"
					/><FormControlLabel
						value="probationNo"
						control={<Checkbox size="small" color="default" checked={props.employee.underProbation === "false"} className={classes.checkbox} />}
						label={<Typography className={classes.label}>No</Typography>}
						labelPlacement="end"
					/>
				</Grid>
				<Grid item xs={6} className={classes.gridCell}>
					<label className={classes.label}>District/Division</label>
					<ReactSelect
						name="district"
						onChange={e => props.handleChange(e, "district")}
						options={props.districtOptions}
						value={props.district}
						defaultValue={props.district}
						placeholder="Select..."
						className={classes.reactselect}
						theme={theme => ({
							...theme,
							borderRadius: 1,
							colors: {
								...theme.colors,
								primary25: '#d2f6e3',
								primary50: '#2b995f',
								primary: '#2b995f',
							},
						})}
					/>
				</Grid>
				<Grid item xs={6} className={classes.gridCell}>
					<label className={classes.label}>Borough</label>
					<ReactSelect
						name="borough"
						onChange={e => props.handleChange(e, "borough")}
						options={props.boroughOptions}
						value={props.borough}
						defaultValue={props.borough}
						placeholder="Select..."
						className={classes.reactselect}
						theme={theme => ({
							...theme,
							borderRadius: 1,
							colors: {
								...theme.colors,
								primary25: '#d2f6e3',
								primary50: '#2b995f',
								primary: '#2b995f',
							},
						})}
					/>
				</Grid>
			</Grid>
		</Grid>
	);
}

