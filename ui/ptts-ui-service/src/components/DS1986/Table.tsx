﻿// prettier-ignore
import {
	Checkbox, IconButton, Link, Paper, Table, TableBody, TableCell,
	TableContainer, TableHead, TableRow
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useState } from "react";
import * as DS1986Actions from "../../actions/DS1986";
import { history } from "../../configureStore";
import { DS1986 } from "../../model";
import { allServices } from '../../services';
import { PTTSfunctions } from '../../components/basicComponents';

export function DS1986Table(props: any) {
	const DS1984rcpt: string = !props ? ""
		: props.props;
	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS1984rcpt:", DS1984rcpt);
	const classes = useStyles();
	const dummyDS1986: DS1986 = {
		ID: 0,
		ReferenceNo: "",
		Name: "",
		District: "",
		BadgeNo: "",
		NovasNo: "",
		Ds1984No: "",
		DS1986ErrorId: 0,
		DS1986BDate: new Date(),
		OtherRemarks: "",
		Remarks: "",
		CreatedBy: "",
		CreatedDate: new Date(),
		ModifiedBy: "",
		ModifiedDate: new Date(),
		DS1984District: 0,
		ReceiptNo: "",
		DS1984DistrictName: "",
	}

	const [dsList, setdsList] = useState([dummyDS1986]);
	const [Loading, setLoading] = useState(false);

	React.useEffect(() => {
		(async () => {
			setLoading(true);
			let rdata: DS1986[] = [];
			if (!DS1984rcpt) {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("whole DS1986 list");
				const response2 = await allServices.PTTSgetServices.DS1986List(); // axios always returns an object with a "data" child
				rdata = response2.data;
			}
			else {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("partial DS1986 list", DS1984rcpt);
				const response1 = await allServices.PTTSgetServices.DS1986RelatedToOneDS1984List(DS1984rcpt); // axios always returns an object with a "data" child
				rdata = response1.data;
			}

			if (!rdata) {
				setLoading(true);
			}
			else {
				const rdataDesc: DS1986[] = rdata.sort(function (a, b) {
					var nameA = !a.ReceiptNo ? "" : a.ReceiptNo.toUpperCase(); // ignore upper and lowercase
					var nameB = !b.ReceiptNo ? "" : b.ReceiptNo.toUpperCase(); // ignore upper and lowercase
					if (nameA < nameB) {
						return 1;
					}
					if (nameA > nameB) {
						return -1;
					}
					// names must be equal
					return 0;
				}).slice(0, 29); // DESC
				setdsList(rdataDesc);
				setLoading(false);
			}
		})();
	}, []);

	const handleCellClick = (e: any) => {
		history.push('/PTTS/existingDS1986/' + e.target.textContent)
	}


	const handleLinkToParent1984 = (e: any) => {
		history.push('/PTTS/existingDS1984/' + e.target.textContent)
	}

	return (
		<TableContainer component={Paper}>
			<Table className={classes.table} size="small" aria-label="a dense table">
				<TableHead>
					<TableRow>
						<TableCell size="small" align="right">DS1986B ID</TableCell>
						<TableCell size="small" align="right">REF#</TableCell>
						<TableCell size="small" align="right">Name</TableCell>
						<TableCell size="small" align="right">NOV#</TableCell>
						<TableCell size="small" align="right">DS1984 Rcpt#</TableCell>
						<TableCell size="small" align="left">DS1986B Date</TableCell>
						<TableCell size="small" align="left">Remarks</TableCell>
						<TableCell size="small" align="left">Modified By</TableCell>
						<TableCell size="small" align="left">Modified On</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{
						// @ts-ignore: Object is possibly 'null'.
						Loading || !dsList || dsList === undefined ? <TableRow><TableCell size="small">working ...</TableCell></TableRow> : dsList.map((n: DS1986) => {
							//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("n is:");
							//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(n);
							if (!n.ID) {
								return (<TableRow key={"-1"}><TableCell size="small" key={"0"}>Working ...</TableCell></TableRow>)
							}
							else {
								return (
									<TableRow
										key={n.ID.toString() + "row"}
										hover
									>
										<TableCell size="small" key={n.ID.toString() + "IDno"} align="right" onClick={handleCellClick}><Link  href="#">{n.ID}</Link></TableCell>
										<TableCell size="small" key={n.ID.toString() + "REF"} align="right">{n.ReferenceNo}</TableCell>
										<TableCell size="small" key={n.ID.toString() + "NM"} align="right">{n.Name}</TableCell>
										<TableCell size="small" key={n.ID.toString() + "NOVNO"} align="right">{n.NovasNo}</TableCell>
										<TableCell size="small" key={n.ID.toString() + "LINK84"} align="right" onClick={handleLinkToParent1984}><Link  href="#">{n.Ds1984No}</Link></TableCell>
										<TableCell size="small" key={n.ID.toString() + "86DATE"} align="left">{PTTSfunctions.Commonfunctions.formatDateSlashes(n.DS1986BDate.toLocaleString())}</TableCell>
										<TableCell size="small" key={n.ID.toString() + "REMR"} align="left">{n.Remarks}</TableCell>
										<TableCell size="small" key={n.ID.toString() + "MODBY"} align="left" >{n.ModifiedBy}</TableCell>
										<TableCell size="small" key={n.ID.toString() + "MODON"} align="left">{n.ModifiedDate}</TableCell>
									</TableRow>
								);
							}

						})}
				</TableBody>
			</Table>
		</TableContainer>
	);
}

const useStyles = makeStyles({
	paper: {
		width: "70%",
		display: "block",
	},
	table: {
		width: "100%",
		marginLeft: 180,
	},
	cell: {
		minWidth: 100,
		maxWidth: 220,
	}
});
