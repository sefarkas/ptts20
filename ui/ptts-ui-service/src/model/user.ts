export interface Authentication {
    isLoggedIn: boolean;
  }

export const userAction = {
    LOGIN_REQUEST: 'USERS_LOGIN_REQUEST',
    LOGIN_SUCCESS: 'USERS_LOGIN_SUCCESS',
    LOGIN_FAILURE: 'USERS_LOGIN_FAILURE',
    
    LOGOUT: 'USERS_LOGOUT',
    MSG: 'SYSTEM_MSG_TO_USER', // some sort of exception
};

export interface AuthUserPref {
    emailId: string;
    fullName: string;
    userName: string;
    userRole: string;
}

export interface AuthResponse {
    reason: string;
    refreshToken: string;
    status: string;
    token: string;
    userPreferences: AuthUserPref;
}

export type tADgroup = {
    userName: string;
    userMemberOfADgroupList: string;
}

export type tUserDetails = {
    aDgroups: any;
    authenticated: boolean;
    authenticationMessage: string;
    createDate: string;
    email: string;
    firstName: string;
    isActive: number;
    lastLoginDate: string;
    lastName: string;
    password: any;
    roles: any,
    userId: number;
    userName: string;
    userRoles: any,
    voiceTelephoneNumber: string;
}

export type tUserNTIdetails = {
    NTIemployeeID: string;
    NTIemployeeType: number;
    NTIssn: string;
    NTIbadgeNumber: string;
    NTIlastName: string;
    NTIfirstName: string;
    NTImiddleName: string;
    NTItitleID: string;
    NTIsuffix: string;
    NTItitle: string;
    NTIchartNumber: string;
    NTIlocationID: string;
    NTIlocation: string;
    NTIlocationDesc: string;
    NTIhomePhoneNumber: string;
    NTIaddress1: string;
    NTIaddress2: string;
    NTIcity: string;
    NTIstate: string;
    NTIzip: string;
    NTIbirthDate: string;
    NTIdateLastWorked: string;
    NTIdateNYCHire: string;
    NTIdateDSNYHire: string;
    NTIeffectiveDate: string;
    NTIvacationSchedule: string;
    NTIphoto: string;
    NTIdateProbationEnd: string;
    NTImostRecentDate: string;
}