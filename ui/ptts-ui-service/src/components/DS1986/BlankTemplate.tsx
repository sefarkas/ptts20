﻿import { ErrorMessage } from "@hookform/error-message";
import { Button, FormControl, InputLabel, MenuItem , Select, Link } from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useEffect, useLayoutEffect, useState } from "react"; // , useReducer
import { Controller, useForm } from "react-hook-form"; // , FormProvider
import { isNullOrUndefined } from 'util';
import { allServices } from '../../services';
import { history } from "../../configureStore";
import { tSpecialDistrict, UsedTicketRange, DS1986problems } from '../../model';
import { PTTSfunctions } from '../basicComponents';
import { ErrorMessageContainer, ErrorSummary } from '../basicComponents/validation';
import { DS1984basics } from "../DS1984";

type postDataResult<T> = {
	success: boolean;
	errors?: { [P in keyof T]?: string[] };
};

type FormData = {
	fldID: number;
	fldReferenceNo: string;
	fldName: string;
	fldDistrict: string;
	fldBadgeNo: string;
	fldNovasNo: string;
	fldDs1984No: string;
	fldDS1986ErrorId: number;
	fldDS1986BDate: Date;
	fldOtherRemarks: string;
	fldRemarks: string;
	fldCreatedBy: string;
	fldCreatedDate: Date;
	fldModifiedBy: string;
	fldModifiedDate: Date;
	fldDS1984District: number;
	//fldReceiptNo: string;
	//fldDS1984DistrictName: string;
}

export function NewDS1986Form(props: any | undefined) {

	const classes = useStyles();
	//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props);
	let aParentDS1984: DS1984basics | any | undefined = props.history.location.state;

	const [IsAlreadyUsedTktRangeState, setIsAlreadyUsedTktRangeState] = useState(false);
	const [IsDS1984ReceiptState, setIsDS1984ReceiptState] = useState(isNullOrUndefined(aParentDS1984) ? false : true);

	const [isLoading, setIsLoading] = React.useState(true);

	const [NovasNoStateMin, setNovasNoStateMin] = useState(isNullOrUndefined(aParentDS1984) ? 9999999 : aParentDS1984.DS1984fromTktNumber);
	const [NovasNoStateMax, setNovasNoStateMax] = useState(isNullOrUndefined(aParentDS1984) ? 29999999 : aParentDS1984.DS1984toTktNumber);

	const [IDState, setIDState] = useState(0);
	const [ReferenceNoState, setReferenceNoState] = useState("");
	const [NameState, setNameState] = useState("");
	const [DistrictState, setDistrictState] = useState("");
	const [BadgeNoState, setBadgeNoState] = useState("");
	const [NovasNoState, setNovasNoState] = useState("00000000");
	const [Ds1984NoState, setDs1984NoState] = useState(isNullOrUndefined(aParentDS1984) ? "each DS-1986 must have a related DS-1984" : aParentDS1984.DS1984receiptNumber);
	const [DS1986ErrorIdState, setDS1986ErrorIdState] = useState(11);
	const [DS1986BDateState, setDS1986BDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [OtherRemarksState, setOtherRemarksState] = useState("");
	const [RemarksState, setRemarksState] = useState("");
	const [CreatedByState, setCreatedByState] = useState("");
	const [CreatedDateState, setCreatedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [ModifiedByState, setModifiedByState] = useState("");
	const [ModifiedDateState, setModifiedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [DS1984DistrictState, setDS1984DistrictState] = useState(isNullOrUndefined(aParentDS1984) ? "PTTS expects a choice for District DS-1984." : aParentDS1984.DS1984DistrictState);
	// const [ReceiptNoState, setReceiptNoState] = useState("created upon saving");
	// const [DS1984DistrictNameState, setDS1984DistrictNameState] = useState("");

	const dummyDSspecialDistrict: tSpecialDistrict = {
		Id: 0,
		PayCode: "",
		Description: "",
		Remarks: "",
		SplinterDisrtict: false,
		MappingDistictNTI: "",
	}

	const dummyProblemList: DS1986problems = {
		Id: 0,
		Code1: "",
		Description: "",
		Type: "",
		OrderSequence: 0,
	}

	const [dsList, setdsList] = useState([dummyDSspecialDistrict]);
	const [dsProblemList, setdsProblemList] = useState([dummyProblemList]);
	let dsListMenu: any | undefined = undefined;
	let dsProblemListMenu: any | undefined = undefined;

	const defaultValues = {
		Native: "",
		TextField: "",
		Select: "",
		ReactSelect: "",
		Checkbox: false,
		switch: false,
		RadioGroup: ""
	};

	const handleOnChange = (e: any) => {
		e.preventDefault();

		switch (e.target.name) {
			case "fldID": setIDState(e.target.value); break;
			case "fldReferenceNo": setReferenceNoState(e.target.value); break;
			case "fldName": setNameState(e.target.value); break;
			case "fldDistrict": {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handleOnChange fldDistrict");
				setDistrictState(e.target.value);
				break;
			}
			case "fldBadgeNo": setBadgeNoState(e.target.value); break;
			case "fldNovasNo": setNovasNoState(e.target.value); break;
			case "fldDs1984No": setDs1984NoState(e.target.value); break;
			case "fldDS1986ErrorId": {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handleOnChange fldDS1986ErrorId");
				if (e.target.value > 0) {
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fldDS1986ErrorId", e.target.value);
					setDS1986ErrorIdState(e.target.value);
				}
				else {
					setDS1986ErrorIdState(11);
				}
				break;
			}
			case "fldDS1986BDate": setDS1986BDateState(e.target.value); break;
			case "fldOtherRemarks": setOtherRemarksState(e.target.value); break;
			case "fldRemarks": setRemarksState(e.target.value); break;
			case "fldCreatedBy": setCreatedByState(e.target.value); break;
			case "fldCreatedDate": {
				setCreatedDateState(e.target.value.toString());
				break;
			}
			case "fldModifiedBy": setModifiedByState(e.target.value); break;
			case "fldModifiedDate": setModifiedDateState(e.target.value); break;
			case "fldDS1984District": {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handleOnChange fldDS1984District");
				setDS1984DistrictState(e.target.value);
				break;
			}
			//case "fldReceiptNo": setReceiptNoState(e.target.value); break;
			//case "fldDS1984DistrictName": setDS1984DistrictNameState(e.target.value); break;

			default:
				// code block
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.value);
		}

		if (CreatedDateState.valueOf !== PTTSfunctions.Commonfunctions.todaysdate) {
			setCreatedDateState(PTTSfunctions.Commonfunctions.todaysdate);
		}

		if (CreatedByState.length < 2) {
			const curUser: any = localStorage.getItem('user');
			const j: any = JSON.parse(curUser);
			const loggedInUser: string = j.data.userPreferences.userName;
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("created by ", loggedInUser);
			setCreatedByState(loggedInUser);
		}

	}

	const submitForm = async (data: FormData, e: any) => {
		if (DistrictState.length < 3) {
			alert("PTTS expects you to pick the location you delivered to.");
		}
		else if (DS1986ErrorIdState < 12) {
			alert("PTTS expects you to categorize the problem.");
		}
		else {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DistrictState is ", DistrictState);
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS1986ErrorIdState is ", DS1986ErrorIdState);
			const oneDS1986form = { // take the individual hook const and set each oneDS1986form object properties
				ID: IDState,
				ReferenceNo: ReferenceNoState,
				Name: NameState,
				District: DistrictState,
				BadgeNo: BadgeNoState,
				NovasNo: NovasNoState,
				Ds1984No: Ds1984NoState,
				DS1986ErrorId: DS1986ErrorIdState,
				DS1986BDate: DS1986BDateState,
				OtherRemarks: OtherRemarksState,
				Remarks: RemarksState,
				CreatedBy: CreatedByState,
				CreatedDate: CreatedDateState,
				ModifiedBy: ModifiedByState,
				ModifiedDate: ModifiedDateState,
				DS1984District: DS1984DistrictState,
				//ReceiptNo: ReceiptNoState,
				//DS1984DistrictName: DS1984DistrictNameState,
			}
			try {

				const response = await allServices.PTTSiudServices.iudDS1986(oneDS1986form);
				// response is the new receipt number
				if (isNullOrUndefined(response.data) || response.data === "[object Object]") {
					// "An error occurred while executing the command definition. See the inner exception for details.|
					// DS1984 is not created for the specified ticket range.Please have Enf HQ create 1984 for this box. "
					const importantPart: string = response.data
						.ExceptionMessage.replace("An error occurred while executing the command definition. See the inner exception for details.|", "");
					alert("I am having trouble with saving DS1986 values.  " + importantPart);
					// any error message is in the CustomerOrderNo property in the controller
				}
				else {
					history.push('/PTTS/existingDS1986/' + response.data);
				}
			}
			catch (err) {
				// anything else
				alert("I am having trouble with saving DS1986 values.  " + err.toString());
				// return err;
			};

		}

		// e.target.reset(); // reset after form submit
	};

	const onCancel = () => {
		history.push('/PTTS/home');
	};

	const onGoToParent = () => {
		if (IsDS1984ReceiptState) {
			history.push('/PTTS/existingDS1984/' + Ds1984NoState);
		}
		else {
			alert("There is no DS-1984 " + { Ds1984NoState } + " associated with this DS-1986.");
		}
	};

	const handleDropDownChange = (event: any, fld: string) => {

		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handleDropDownChange for ", fld);
		switch (fld) {
			case "fldDS1986ErrorId": {
				// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fld", fld);
				//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(dsProblemList);
				// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("ErrorID: ", event.target.innerText);
				if (event.target.innerText !== undefined) {
					let strID: string = event.target.innerText.substr(0, 4);
					// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("in-progress strID to be saved: ", strID);
					strID = strID.substr(1, 2); // second character, length of two
					// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Error strID to be saved: ", strID);
					const intID: number = Number.parseInt(strID);
					if (!intID) {
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Error intID to be saved: ", intID);
					}
					else {
						// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Expected problem type: ", dsProblemList[intID - 11].Description);
						if (intID !== DS1986ErrorIdState) {
							(async () => {
								const temp: string = DS1984DistrictState;
								await setDS1986ErrorIdState(intID);
								await setDS1984DistrictState(temp);
							})();
						}
					}
				}
				break;
			}
			case "fldDS1984District": {
				// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(event.target.value);
				// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fld", fld);
				// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(dsList);
				if (event.target.innerText !== undefined) {
					const strInnerText: string = event.target.innerText;
					if (strInnerText !== DS1984DistrictState) {
						setDS1984DistrictState(strInnerText);
                    }
				}
				break;
			}
			default:
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("default");
				// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(event.target);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fld", fld);
		}
		return;
	};

	const newDS1986 = () => { // used to clear the form
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("newDS1986 fired");
		setIDState(0);
		setReferenceNoState("");
		setNameState("");
		setDistrictState("");
		setBadgeNoState("");
		setNovasNoState("");
		setDs1984NoState(isNullOrUndefined(aParentDS1984) ? "enter the related 22 character DS-1984 rcpt# here" : aParentDS1984.DS1984receiptNumber);
		setDS1986ErrorIdState(11);
		setDS1986BDateState(PTTSfunctions.Commonfunctions.todaysdate);
		setOtherRemarksState("");
		setRemarksState("");
		setCreatedByState("");
		setCreatedDateState(PTTSfunctions.Commonfunctions.todaysdate);
		setModifiedByState("");
		setModifiedDateState(PTTSfunctions.Commonfunctions.todaysdate);
		setDS1984DistrictState("");
		//setReceiptNoState("");
		//setDS1984DistrictNameState("");
	};


	const makeListMenu = () => {
		if (!dsList) { return <div>still working ...</div> };
		return dsList.map(oneChoice => {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("district: ", DS1984DistrictState, !oneChoice.MappingDistictNTI ? oneChoice.Description === DS1984DistrictState : oneChoice.MappingDistictNTI === DS1984DistrictState);
			return (
				<MenuItem
					selected={!oneChoice.MappingDistictNTI ? oneChoice.Description === DS1984DistrictState : oneChoice.MappingDistictNTI === DS1984DistrictState}
					key={"KeyForDS1984Dist" + oneChoice.Id.toString()}
					value={!oneChoice.MappingDistictNTI ? oneChoice.Description : oneChoice.MappingDistictNTI}
				>
					{!oneChoice.MappingDistictNTI ? oneChoice.Description : oneChoice.MappingDistictNTI}
				</MenuItem>);
		});
	};

	const makeProblemListMenu = () => {
		if (!dsProblemList) { return <div>still working ...</div> };
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("JSX for ProblemListMenu");
		return dsProblemList.map(oneChoice => {
			return (
				<MenuItem
					key={oneChoice.Id.toString()}
					value={oneChoice.Id}
			>({oneChoice.Id})&nbsp;{oneChoice.Description}
			</MenuItem>);
		});
	};

	const renderActionButtons = () => {
		return (
			<div>
				<StyledButton
					className={classes.button}
					variant="outlined"
					color="primary"
					type="submit"
					onClick={handleSubmit(submitForm)}
				>
					Save/Acknowledge
						</StyledButton>
				<StyledButton
					className={classes.button}
					variant="outlined"
					color="primary"
					onClick={() => newDS1986()}
				>
					Reset
						</StyledButton>
				<StyledButton
					className={classes.button}
					variant="outlined"
					color="primary"
					onClick={onGoToParent}>
					Related DS-1984
					</StyledButton>
				<StyledButton
					className={classes.button}
					variant="outlined"
					color="primary"
					onClick={onCancel}>
					Cancel
					</StyledButton>
			</div>
		)
	};

	const renderListMenu = () => {
		if (isNullOrUndefined(dsListMenu) || dsListMenu.length < 2 ) {
			dsListMenu = makeListMenu();
		}
		return dsListMenu;
	};

	const renderProblemListMenu = () => {
		if (isNullOrUndefined(dsProblemListMenu)) {
			dsProblemListMenu = makeProblemListMenu();
		}
		return dsProblemListMenu;
	};

	const { register, handleSubmit, errors } = useForm<FormData>();

	const { control } = useForm({ defaultValues });

	function fIsDS1984Receipt(aDSform: string, LowHigh: string, TgtVal: string): boolean {
		let rdata: UsedTicketRange[] | undefined = undefined;
		let pIsDS1984Receipt: boolean = false;
		setIsLoading(true);

		if (LowHigh === "Low"
			? !TgtVal
				? false
				: !TgtVal.length
					? false
					: TgtVal.length === 22
			: false) {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("LowHigh has the expected value: ", LowHigh);
			const tryThisRange =
				(
					{
						ECBSummonsNumberFrom: "0",
						ECBSummonsNumberTo: "0",
						DSform: aDSform,
						ReceiptNumber: TgtVal,
					}
				);
			(() => {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("tryThisRange");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange);
				let response: any = null
				try {
					(async () => {
						response = await allServices.PTTSgetServices.refUsedTicketNumbers(tryThisRange);
						// axios always returns an object with a "data" child
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("response");
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(response);
						rdata = await response.data;
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("rdata");
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
						if (!rdata) {
							 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("No such DS-1984.");
							setIsLoading(false);
							return false;
						}
						else {
							 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS-1984 rdata exists");
							pIsDS1984Receipt =
								!rdata[0]
									? false :
									!rdata[0].ReceiptNumber
										? false
										: rdata[0].ReceiptNumber === TgtVal
											? true
											: false
							// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("TgtVal", TgtVal);
							//!rdata[0].ReceiptNumber
							//	?  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("rdata[0].ReceiptNumber undefined")
							//	:  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("rdata[0].ReceiptNumber", rdata[0].ReceiptNumber);
							// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsDS1984Receipt", pIsDS1984Receipt);
						}
						await setIsLoading(false);
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Result after fIsDS1984Receipt async: ", pIsDS1984Receipt);
					})();
				}
				catch (err) {
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("refUsedTicketNumbers err: ", err.message);
					return pIsDS1984Receipt;
				}
			})();
			return pIsDS1984Receipt;
		}
		else {
			setIsLoading(false);
			return pIsDS1984Receipt;
		}
	}

	function fIsAlreadyUsedTktRange(aDSform: string, LowHigh: string, TgtVal: string): boolean {
		let rdata: UsedTicketRange[] | undefined = undefined;
		let pIsAlreadyUsedTktRange: boolean = false;

		if (LowHigh === "Low"
			? Number.parseInt(TgtVal) > 0
			: Number.parseInt(TgtVal) > 0) {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Test a ticket range");
			const ECBfromState: string =
				TgtVal.length < 9
					? PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(Number.parseInt(TgtVal))
					: TgtVal
			const ECBtoState: string =
				TgtVal.length < 9
					? PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(Number.parseInt(TgtVal))
					: TgtVal;
			(async () => {
				const tryThisRange = (
					{
						ECBSummonsNumberFrom: LowHigh === "Low"
							? TgtVal
							: ECBfromState,
						ECBSummonsNumberTo: LowHigh === "High"
							? TgtVal
							: ECBtoState,
						DSform: aDSform,
						ReceiptNumber: Ds1984NoState,
					}
				);
				setIsLoading(true);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("tryThisRange");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(IsAlreadyUsedTktRangeState);
				pIsAlreadyUsedTktRange = false;
				const response = await allServices.PTTSgetServices.refUsedTicketNumbers(tryThisRange);
				// axios always returns an object with a "data" child
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("response");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(response);
				rdata = await response.data;
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("rdata");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
				if (!rdata) {
					pIsAlreadyUsedTktRange = false;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ticket is not in the range on the related DS-1984.");
				}
				else {
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("test the ticket range");
					pIsAlreadyUsedTktRange =
						!rdata[0]
							? false :
							!rdata[0].ReceiptNumber
								? false
								: true
					pIsAlreadyUsedTktRange =
						pIsAlreadyUsedTktRange ? rdata[0].DSform.substr(-4) === "OKay" : false;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange 1", pIsAlreadyUsedTktRange);
					pIsAlreadyUsedTktRange =
						pIsAlreadyUsedTktRange ? TgtVal >= rdata[0].ECBSummonsNumberFrom
							&& rdata[0].ECBSummonsNumberFrom > "0"
							: false;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange 2", pIsAlreadyUsedTktRange);
					pIsAlreadyUsedTktRange =
						pIsAlreadyUsedTktRange ? TgtVal <= rdata[0].ECBSummonsNumberTo : false;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange 3", pIsAlreadyUsedTktRange);

				}
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("IsAlreadyUsedTktRange at end of ASYNC");
				setIsAlreadyUsedTktRangeState(pIsAlreadyUsedTktRange);
				setIsLoading(false);
				return pIsAlreadyUsedTktRange;
			})();

		}
		else {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange outside of IF: ", pIsAlreadyUsedTktRange);
			setIsLoading(true);

		};
		return pIsAlreadyUsedTktRange;

	};

	useEffect(() => {
		dsListMenu = makeListMenu();
		dsProblemListMenu = makeProblemListMenu();
    }, [])

	useLayoutEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Init useEffect started.");
		if ((!dsListMenu && !DS1984DistrictState) || (!dsProblemListMenu && DS1986ErrorIdState < 12)) {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Build the two dropdown lists.");
			(async () => {
				const response = await allServices.PTTSgetServices.refSpecialDistricts(); // axios always returns an object with a "data" child
				const rdata: tSpecialDistrict[] = response.data;

				const response2 = await allServices.PTTSgetServices.refProblemList(); // axios always returns an object with a "data" child
				const rdata2: DS1986problems[] = response2.data;

				if (!rdata || !rdata2) {
					setIsLoading(true);
				}
				else {
					const rdataDesc: tSpecialDistrict[] = rdata.sort(function (a, b) {
						var nameA = !a.Description ? "" : a.Description.toUpperCase(); // ignore upper and lowercase
						var nameB = !b.Description ? "" : b.Description.toUpperCase(); // ignore upper and lowercase
						if (nameA < nameB) {
							return -1;
						}
						if (nameA > nameB) {
							return 1;
						}
						// names must be equal
						return 0;
					}).slice(0, 150); // ASC
					await setdsList(rdataDesc);
					dsListMenu = makeListMenu();

					const rdataDesc2: DS1986problems[] = rdata2.sort(function (a, b) {
						var nameA = !a.OrderSequence ? 0 : a.OrderSequence; // ignore upper and lowercase
						var nameB = !b.OrderSequence ? 0 : b.OrderSequence; // ignore upper and lowercase
						if (nameA < nameB) {
							return -1;
						}
						if (nameA > nameB) {
							return 1;
						}
						// names must be equal
						return 0;
					}).slice(0, 10); // ASC
					await setdsProblemList(rdataDesc2);
					dsProblemListMenu = makeProblemListMenu();
				}
			})();
		}
		if (!Ds1984NoState) {
			setIsLoading(true);
		}
		else {
			if (!aParentDS1984) {
				setIsDS1984ReceiptState(false);
				setIsLoading(false);
            }
			else if (!(!aParentDS1984.DS1984receiptNumber)) {
				if (IsDS1984ReceiptState && Ds1984NoState === aParentDS1984.DS1984receiptNumber) {
					setIsLoading(false);
					return;
				}
			}

			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Figure out whether DS1984 rcpt exists.");
			(() => {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Init useEffect Ds1984NoState: ", Ds1984NoState);

				if (Ds1984NoState.length === 22) {
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useEffect runs fIsDS1984Receipt for Init");
					const pIsDS1984Receipt: boolean = fIsDS1984Receipt("IsDS1984", "Low", Ds1984NoState);
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("test for DS1984 receipt # resulted in: ", pIsDS1984Receipt)
					setIsDS1984ReceiptState(pIsDS1984Receipt);
					setIsLoading(false);
				}
			})();
		}
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Init useEffect finished.");
	},
		[Ds1984NoState, DS1986ErrorIdState]
	);

	useEffect(() => {
		if (parseInt(ReferenceNoState) > 0) {
			if (ReferenceNoState.toString().length === 7) {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("REF#", ReferenceNoState);
				(async () => {
					setIsLoading(true);

					const response: any = await
						allServices.EISgetServices.EISemployeeDetailsRN(ReferenceNoState); // axios always returns an object with a "data" child
					const rdata: any = response.data;
					if (!rdata) {
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("REF# lookup in-progress.");
						setIsLoading(true);
					}
					else {
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("REF# lookup found data to evaluate.");
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
						if (PTTSfunctions.Commonfunctions2.isNotDefined(rdata.employeeModel)) {
							setReferenceNoState("Invalid REF#: " + ReferenceNoState);
						}
						else {
							try {

								if (isNullOrUndefined(rdata.jobModel.dsnyInformation)) { throw new Error("rdata.jobModel.dsnyInformation missing") };
								let eisPayCodeCorrelID: string = rdata.jobModel.dsnyInformation.payCodeId.correlation
								eisPayCodeCorrelID = eisPayCodeCorrelID.substring(eisPayCodeCorrelID.length - 4);
								setDistrictState( // this is an integral part of the DS-1984 Receipt# that will be composed by UID
									// actual paycode may be different; real one needs a lookup using this CorrelationID string
									eisPayCodeCorrelID
								);
								if (isNullOrUndefined(rdata.employeeModel)) { throw new Error("rdata.employeeModel missing") };
								setNameState(rdata.employeeModel.names[0].firstName.substring(0, 1)
									+ ' ' + rdata.employeeModel.names[0].lastName);
								setBadgeNoState(isNullOrUndefined(rdata.jobModel.badgeNumber)
									? "NA"
									: rdata.jobModel.badgeNumber.badgeNumber)
							}
							catch (e) {
								alert(e.toString() + " using (" + ReferenceNoState + ").");
								setBadgeNoState("type one in later");
							}

						}
						setIsLoading(false);
					}
				})();

			}
			else {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("numeric but not 7 digits -- officer")
			}
		}
		else {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("part of Officer's last name");
		};
	},
		[ReferenceNoState]
	);

	useEffect(() => {
		(async () => {
			if (Ds1984NoState.length === 22) {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useLayoutEffect runs IsAlreadyUsedTktRange for ", NovasNoState);
				const pIsAlreadyUsedTktRange: boolean = await fIsAlreadyUsedTktRange("IsDS1984Tkt", "Low", NovasNoState);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useEffect result for pIsDS1984Receipt: ", pIsAlreadyUsedTktRange);
				setIsAlreadyUsedTktRangeState(pIsAlreadyUsedTktRange);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("after setIsAlreadyUsedTktRange: ", IsAlreadyUsedTktRangeState);
				setIsLoading(pIsAlreadyUsedTktRange);
			}
			else {
				setIsAlreadyUsedTktRangeState(false);
			}
		})();
	}, [NovasNoState, Ds1984NoState.length]);

	function renderSelectForDS1986ErrorIdState() {
		return (
			<FormControl className={classes.stylefortopacknowledgementtext}>
				<InputLabel className={classes.stylefortopacknowledgementtext}
					id="demo-simple-select-labelErrorId">Category</InputLabel>
				<div>&nbsp;</div>
				<Controller
					
					as={
						<Select
							className={classes.stylefortopdistributionrowtextwide}
							labelId="demo-simple-select-labelErrorId"
							id="demo-simple-selectErrorId"
							value={DS1986ErrorIdState > 10 ? dsProblemList[DS1986ErrorIdState - 11].Description : ""}
							onMouseDown={
								(e) =>
								handleDropDownChange(e, "fldDS1986ErrorId")
							}
							name="fldDS1986ErrorId"
							ref={register({
								required: { value: true, message: "You must pick a problem type." },
								min: {value: 12, message: `Pick a problem category for ${NovasNoState}`},
							})}
						>
							{makeProblemListMenu()}
						</Select>
					}
					name="Select"
					id="fldDS1986ErrorId"
					control={control}
				/>
				<div>&nbsp;</div>
				<ErrorMessage
					errors={errors}
					name="fldDS1986ErrorId"
					as={<ErrorMessageContainer />}
				/>
			</FormControl>

		);
	}

	function renderSelectForDS1984DistrictState() {
		// JSX has to call this with renderDS1984DistrictState(), including ()
		return (
			<FormControl className={classes.stylefortopacknowledgementtext}>
				<InputLabel className={classes.stylefortopdistributionrowtextwide}
					id="demo-simple-select-labelDistrict"><div>Location responsible for&nbsp;{NovasNoState}</div></InputLabel>
				<div>&nbsp;</div>
				<Controller
					
					render={({ onChange, onBlur, value, name }) => (
						<Select
							ref={register({
								required: { value: true, message: "You must pick a location where you accepted the 25 pack." },
								minLength: { value: 3, message: "You have to pick a location where you accepted the 25 pack" }
							})}
							className={classes.stylefortopdistributionrowtextwide}
							labelId="demo-simple-select-labelDistrict"
							id="demo-simple-selectDistrict"
							onMouseDown={(e: any) =>
								{
									handleDropDownChange(e, "fldDS1984District");
								}
							}
							value={DS1984DistrictState}
						>
							{renderListMenu()}
						</Select>
					)}
					name="Select"
					id="IDofFldDS1984District"
					control={control}
					defaultValue={DS1984DistrictState}
				/>
				<div>&nbsp;</div>
				<ErrorMessage
					errors={errors}
					name="fldDS1984District"
					as={<ErrorMessageContainer />}
				/>
			</FormControl>
		);
	}

	function renderValidationMsgs() {
		return (
			<Grid item xs={12}>
				<ErrorSummary errors={errors} />
				{!IsAlreadyUsedTktRangeState && IsDS1984ReceiptState ? <div>NOV# {NovasNoState} is not in the ticket range of DS-1984(s) {Ds1984NoState}.</div> : <div>&nbsp;</div>}
				{!IsDS1984ReceiptState ? <div>DS-1984 Receipt Number does not exist.</div> : <div>&nbsp;</div>}
			</Grid>
		)
	}

	if (isLoading) {
		return (<div>Loading ...</div>);
	}
	else {


		// onSubmit is not triggered by type=submit StyledButtons; need an explicit onClick for saving/updating
		return (
			<div onSubmit={handleSubmit(submitForm)}>

				<Grid className={classes.formposition}>
					<Grid>
						<Grid container className={classes.darkborder}>
							<Grid item xs={2} className={classes.styleforlogo}>
								<img src={PTTSfunctions.Logos.DSNYLogo.logoImg} alt="Sanitation Enforcement" />
							</Grid>
							<Grid item xs={9} className={classes.styleforformtitle}>
								<div className={classes.styleforformtitletext}>THE CITY OF NEW YORK Department of Sanitation</div>
								<div className={classes.styleforformtitletext}>DS1986B ENFORCEMENT SUMMONS TRACKING REPORT. DS1986B(11-13)</div>
							</Grid>
						</Grid>
						<div>&nbsp;</div>
						<div>&nbsp;</div>
						<Grid container>
							{renderValidationMsgs()}
							<Grid item xs={12}>
								{renderActionButtons()}
							</Grid>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={12} >
							&nbsp;
							</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={3} className={classes.stylefortopacknowledgement}>
							<div>DS-1986 ID#:&nbsp;</div>
						</Grid>
						<Grid item xs={9} className={classes.thinborderLeft}>
							<div>{IDState}</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={1} className={classes.stylefortopacknowledgement}>
							<div>NOV#:</div>
						</Grid>
						<Grid item xs={3} className={classes.thinborderLeft}>
							<div >
								<input className={classes.stylefortopdistributionrowtext} type="text"
									onChange={(e) => handleOnChange(e)}
									name="fldNovasNo"
									placeholder="eight digits"
									ref={register({
										required: { value: true, message: "PTTS expects a NOV#." },
										pattern: {
											value: /^[1-2]\d{7}$/,
											message: "Enter an eight digit NOV# starting with either '1' or '2'."
										},
										minLength: { value: 8, message: "NOV# is too short." },
										maxLength: { value: 8, message: "NOV# is to long." },
										// validate: (fldNOV) => fIsAlreadyUsedTktRange("DS1986", "Low", fldNOV),
									})}
									value={NovasNoState}
								/>
								<div>&nbsp;</div>
								<ErrorMessage
									errors={errors}
									name="fldNovasNo"
									as={<ErrorMessageContainer />}
								/>
							</div>
						</Grid>
						<Grid item xs={2} className={classes.thinborderLeft}>
							<div>DS-1984 Receipt Number</div>
						</Grid>
						<Grid item xs={2} className={classes.stylefortopacknowledgement}>
							<div>
								<input className={classes.stylefortopacknowledgement}
									type="text" onChange={(e) => handleOnChange(e)}
									name="fldDs1984No"
									ref={register({
										required: { value: true, message: "PTTS expects a NOV#." },
										pattern: {
											value: /^[4]\d{8}[A-Z]\d{12}$/,
											message: "Enter a 22 character receipt # starting with '4' (4YYYYMMDD[4-char PAYcode]123456789)."
										},
										minLength: { value: 22, message: "receipt # is too short." },
										maxLength: { value: 22, message: "receipt # is to long." },
										// validate: (fldDs1984NoVal) => fIsDS1984Receipt("IsDS1984", "Low", fldDs1984NoVal),
									})}
									value={Ds1984NoState}
								/>
								<div>&nbsp;</div>
								<ErrorMessage
									errors={errors}
									name="fldDs1984No"
									as={<ErrorMessageContainer />}
								/>
								{
									!IsDS1984ReceiptState
										? <div></div>
										: <div onClick={onGoToParent}>go to:&nbsp;<div><Link  href="#">{Ds1984NoState}</Link></div></div>
								}
							</div>
						</Grid>
						<Grid item xs={1} className={classes.stylefortopacknowledgementRight}>
							<div>Date</div>
						</Grid>
						<Grid item xs={3} className={classes.thinborderLeft}>
							<div>{PTTSfunctions.Commonfunctions.formatDateSlashes((CreatedDateState.toString()))}</div>
							<div >
								<input className={classes.stylefortopdistributionrowtextwide}
									type="date"
									onChange={(e) => handleOnChange(e)}
									name="fldCreatedDate"
									placeholder="mm/d/yyyy"
									ref={register({
										required: { value: true, message: "PTTS expects a date." },
										pattern: {
											value: /^\d{2}\/\d{2}\/\d{4}|\d{2}\/\d{1}\/\d{4}|\d{1}\/\d{2}\/\d{4}|\d{1}\/\d{1}\/\d{4}|^\d{2}-\d{2}-\d{4}|\d{2}-\d{1}-\d{4}|\d{2}-\d{1}-\d{4}|\d{1}-\d{1}-\d{4}|\d{4}-\d{2}-\d{2}$/,
											message: "Enter either mm/dd/yyyy or m/dd/yyyy or mm/d/yyyy"
										}
									})}
									value={PTTSfunctions.Commonfunctions.formatDateSlashes((CreatedDateState.toString()))}
								/>
								<div>&nbsp;</div>
								<ErrorMessage
									errors={errors}
									name="fldCreatedDate"
									as={<ErrorMessageContainer />}
								/>
							</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={4} className={classes.stylefortopacknowledgement}>
							<div>District DS-1984:{DS1984DistrictState}</div>
							<div>{DS1984DistrictState.substr(0, 4) === "PTTS" ? `Make a choice using -->` : `Change current choice (${DS1984DistrictState}) using -->`}</div>
						</Grid>
						<Grid item xs={8} className={classes.thinborderLeft}>
							<div>{renderSelectForDS1984DistrictState()}</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={1} className={classes.stylefortopacknowledgement}>
							<div>Employee REF#</div>
						</Grid>
						<Grid item xs={2} className={classes.thinborderLeft}>
							<div >
								<input className={classes.thinborderLeft} type="text"
									onChange={(e) => handleOnChange(e)}
									name="fldReferenceNo"
									placeholder="REF# w/zeroes"
									ref={register({
										required: { value: true, message: "PTTS expects a reference number of the employee responsible for the NOV#." },
										pattern: {
											value: /^[A-Za-z]|([\w]+['][\w]+)|^\d{7}$/,
											message: "Employee: Enter a seven digit REF# so PTTS can look up employee information."
										}
									})}
									value={ReferenceNoState}
								/>
								<div>&nbsp;</div>
								<ErrorMessage
									errors={errors}
									name="fldReferenceNo"
									as={<ErrorMessageContainer />}
								/>
							</div>
						</Grid>
						<Grid item xs={2} className={classes.stylefortopacknowledgementRight}>
							<div>Employee Name:</div>
						</Grid>
						<Grid item xs={2} className={classes.thinborderLeft}>
							<div>{NameState}</div>
						</Grid>
						<Grid item xs={2} className={classes.stylefortopacknowledgementRight}>
							<div>Assigned District</div>
						</Grid>
						<Grid item xs={1} className={classes.thinborderLeft}>
							<div>{DistrictState}</div>
						</Grid>
						<Grid item xs={1} className={classes.stylefortopacknowledgementRight}>
							<div>Badge #</div>
						</Grid>
						<Grid item xs={1} className={classes.thinborderLeft}>
							<div>{BadgeNoState}</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={4} className={classes.stylefortopacknowledgement}>
							<div>Problem Type:</div>
							<div>{DS1986ErrorIdState > 11 ? dsProblemList[DS1986ErrorIdState - 11].Description : "PTTS expects the problem to be categorized."}</div>
							<div>{DS1986ErrorIdState > 11 ? `Change current choice (${dsProblemList[DS1986ErrorIdState - 11].Description}) using -->` : `Make a choice using -->`}</div>

						</Grid>
						<Grid item xs={4} className={classes.thinborderLeft}>
							<div>{renderSelectForDS1986ErrorIdState()}</div>
						</Grid>
						<Grid item xs={2} className={classes.thinborderLeft}>
							<div className={classes.stylefortopacknowledgementRight}>Others:</div>
						</Grid>
						<Grid item xs={2} className={classes.thinborderLeft}>
							<div >
								<input className={classes.stylefortopacknowledgement}
									type="textArea" onChange={(e) => handleOnChange(e)}
									name="fldOtherRemarks" ref={register}
									value={OtherRemarksState}
								/>
							</div>
						</Grid>
					</Grid>
					<Grid item xs={12} className={classes.darkborder}>
						<Grid item xs={4} className={classes.thinborderLeft}>
							<div>Remarks:</div>
						</Grid>
						<Grid item xs={8} className={classes.thinborderLeft}>
							<div >
								<input className={classes.stylefortopdistributionrowtextwide}
									type="text" onChange={(e) => handleOnChange(e)}
									name="fldRemarks" ref={register}
									value={RemarksState}
								/>
							</div>
						</Grid>
					</Grid>
					<Grid item xs={12}>
						{renderActionButtons()}
					</Grid>
				</Grid>

			</div>
		)
	}

};


const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5,
		marginTop: 0
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const useStyles = makeStyles({
	root: {
		flexGrow: 1,
		height: "100%",
		paddingLeft: 15,
		paddingRight: 15,

	},
	reactselect: {
		maxWidth: "calc(100% - 20px)",
		marginLeft: 10,
		marginTop: 0,
		fontSize: 12,
		'.control': {
			height: 25
		}
	},
	formposition: {
		marginLeft: 280,
		width: "100%",
	},
	darkborder: {
		borderWidth: 3,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderTop: {
		borderTopWidth: 2,
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderLeft: {
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderTopWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	styleforlogo: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforsubmit: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforformtitle: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
	},
	styleforformtitletext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "black",
		minWidth: 500,
	},
	styleforformorder: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "center",
	},
	styleforformordertext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "center",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 500,
		color: "black",
	},
	styleforticketrange: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
	},
	styleforticketrangetextTO: {
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "flex",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
	},
	stylefortopacknowledgement: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	stylefortopacknowledgementRight: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "right",
	},
	stylefortopacknowledgementtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 250,
		minWidth: 130
	},
	stylefortopdistributionrow: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
		maxWidth: 300,
	},
	stylefortopdistributionrowtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80
	},
	stylefortopdistributionrowtextwide: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		minWidth: 300,
		maxWidth: 600,
	},
	stylefortopdistributionrowtextdate: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 5,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		minWidth: 170,
		maxWidth: 210,
	},
	styleforformreceipt: {
		verticalAlign: "center",
		borderTopStyle: "none",
	},
	styleforformreceipttext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		display: "inline",
		marginLeft: 10,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
		textAlign: "center",
	},
	styleforformreceipttextred: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "red",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
	},
	button: {
		marginTop: 20,
	},

	formstyle: {
		maxWidth: "100%",
		padding: 0,
	},

	input: {
		display: "block",
		borderRadius: 4,
		border: "1px solid",
		padding: "10px 15px",
		marginBottom: 10,
		fontSize: 14,
		maxWidth: 400,
		minWidth: 200,
	},
	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginBottom: 13,
		marginTop: 20,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
	},
	buttonSubmit: {
		background: "#0e8c1b",
		color: "white",
		border: "none",
		marginTop: 20,
		padding: 20,
		fontSize: 16,
		fontWeight: 100,
		width: 200,
	}
});
