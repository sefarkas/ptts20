﻿import { Divider, FormControl, Grid, Input, InputLabel, Paper } from "@material-ui/core";
import { Theme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { Fragment, useState } from "react";
import { useForm } from "react-hook-form";
import { PTTSfunctions } from "../components/basicComponents";
import { ErrorMessageContainer, ErrorSummary } from "../components/basicComponents/validation";
import { ErrorMessage } from "@hookform/error-message";
import { DS21CardTable } from "../components/DS21Card";
import { DS21HDRtable } from "../components/DS21HDR";
import { UsedTicketRange } from "../model";
import { allServices } from '../services';
import { Instructions } from "../components/DS21HDR/Instructions";
import { history } from "../configureStore";
import { BulkUpdateTableAndFooter, Instructions as BulkUpdateInstructions } from '../components/DS21Card';


export function PTTSDS21HDRpage() { // expect all DS21s for a particular user

	return ( // expect all DS21s for a particular user
		<Fragment>
				<Instructions />
				<DS21HDRtable />
		</Fragment>
	);
}


export function PTTSbulkUpdatePage() { // expect up to 20 NOV#s from any DS21

	return ( // expect up to 20 NOV#s from any DS21
		<Fragment>
			<BulkUpdateInstructions />
			<BulkUpdateTableAndFooter />
		</Fragment>
	);
}
export function PTTSDS21HDRsubset(props: any) { // expect one row of DS21HDR related to a DS1984
	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("PTTSDS21HDRsubset props are: ", props);

	const classes = useStyles();

	const DS1984rcpt = !props ? ""
		: !props.match ? ""
			: !props.match.params ? ""
				: props.match.params.DS1984rcpt;

	return ( // expect one row of DS21HDR
		<Grid container className={classes.root}>
			<Grid item xs={12} >
				<div className={classes.paper} >
					DS21 List (for DS-1984 <b>{DS1984rcpt}</b>)
				</div>
			</Grid>
			<Grid item xs={12}>
				<DS21HDRtable DS198xrcpt={DS1984rcpt} />
			</Grid>
		</Grid>
	);
}

export function PTTSDS21CardPage(props: any) {
	const classes = useStyles();
	const DS1984rcpt = !props ? ""
		: !props.match ? ""
			: !props.match.params ? ""
				: props.match.params.DS1984rcpt;

	return (
		<Grid container className={classes.root}>
			<Grid item xs={12}>
				<DS21CardTable DS198xrcpt={DS1984rcpt}/>
			</Grid>
		</Grid>
	);
}


interface tProps {
	oneNOVnumber: string;
}

export function PTTSNOVcrossReferences(props: any) {
	const classes = useStyles();
	const { register, errors } = useForm<FormData>();

	const NOVnumber: tProps = !props ? ""
		: !props.match ? ""
			: !props.match.params ? ""
				: props.match.params.oneNOVnumber;
	const [IsValidNOVnumberState, setIsValidNOVnumberState] = useState(false);
	const [NOVnumberState, setNOVnumberState] = useState(NOVnumber.toString());

	function fIsValidNOVnumber(aDSform: string, LowHigh: string, TgtVal: string): boolean {
		let rdata: UsedTicketRange[] | undefined = undefined;
		let pIsValidNOVnumber: boolean = false;

		const searchECBsummonsNumber: string =
			TgtVal.length < 9
				? PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(Number.parseInt(TgtVal))
				: TgtVal;
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Find a ticket range with NOV ", searchECBsummonsNumber);

		(async () => {
			const tryThisRange = (
				{
					ECBSummonsNumberFrom: LowHigh === "Low"
						? TgtVal
						: searchECBsummonsNumber,
					ECBSummonsNumberTo: LowHigh === "High"
						? TgtVal
						: searchECBsummonsNumber,
					DSform: aDSform,
					ReceiptNumber: "",
				}
			);
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("tryThisRange");
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange);
			pIsValidNOVnumber = false;
			const response = await allServices.PTTSgetServices.refUsedTicketNumbers(tryThisRange);
			// axios always returns an object with a "data" child
			rdata = await response.data;
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange)
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
			if (!rdata) {
				pIsValidNOVnumber = false;
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ticket number bad based on no rdata object.");
				setIsValidNOVnumberState(pIsValidNOVnumber);
			}
			else if (!rdata.length ? true : rdata.length === 0) {
				pIsValidNOVnumber = false;
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ticket number bad based on empty [].");
				setIsValidNOVnumberState(pIsValidNOVnumber);
			}
			else {
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("found a ticket range for ", searchECBsummonsNumber);
				pIsValidNOVnumber =
					Number.parseInt(tryThisRange.ECBSummonsNumberFrom) > 0
						&& Number.parseInt(tryThisRange.ECBSummonsNumberTo) > 0
						? true : false;
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber and non-zeroes");
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(pIsValidNOVnumber);

				//if (!pIsValidNOVnumber) {
				//	if (!rdata![rdata.length - 1]) { // https://stackoverflow.com/questions/55190059/typescript-property-type-does-not-exist-on-type-never
				//		alert(" Cannot find a DS-1984 for this summons number.");
				//	}
				//}
				//else {
				//	alert("NOV Number is OKay.");
				//	setNOVnumberState(searchECBsummonsNumber);
				//}

				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber at end of ASYNC");
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(pIsValidNOVnumber);
				setIsValidNOVnumberState(pIsValidNOVnumber);
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("IsValidNOVnumberState at end of ASYNC");
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(IsValidNOVnumberState);
				return pIsValidNOVnumber;
			}

		})();

		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber after ASYNC", pIsValidNOVnumber);
		return IsValidNOVnumberState;

	};

	const handleOnChange = (e: any) => {
		e.preventDefault();

		switch (e.target.name) {

			case "fldNOVnumber":
				let dummy2n: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				if (isNaN(dummy2n)) {
					alert("NOV number is NaN.");
					break;
				}
				if (dummy2n > 299999999) {
					alert("Expected first summons number is supposed to start with 1 or 2 and be nine-digits.  You entered a number that is too big.")
					break;
				} // less than 8 digits, not-a-number, or nine or more digits starting with 3 (Sanitation only prefixes 1 or 2)
				//            12345678
				setNOVnumberState(dummy2n.toString());
				if (dummy2n > 99999999) {
					dummy2n = Math.trunc(dummy2n / 10);
					//const msg1: string = "check NOV number" + dummy2n.toString();
					//alert(msg1);
					const dummy2: string = PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(dummy2n);
					if (!fIsValidNOVnumber("IsGoodNOVnumber", "Low", dummy2)) {
						// true means validation is OKay, value is proper
						if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fIsValidNOVnumber has not found NOV number.")
					}
				}
				break;
			default:
				// code block
				const msg: string = "No onChange for " + e.target.name;
				alert(msg);
		}
	}
	return (
		<Grid container>
			<Grid item xs={12}>
				DS1983 DS1984 DS21Card NOVdetails DS156
			</Grid>
			<Grid item xs={12} component={Paper} className={classes.paper}>
				<FormControl  >
					<ErrorSummary errors={errors} />

					<InputLabel htmlFor="idNOVnumber">{NOVnumberState.length < 9
						? <div>Search for this ECB Summons Number: {NOVnumberState} </div>
						: IsValidNOVnumberState
							? < div > {NOVnumberState} is valid; click 'Go to {NOVnumberState}'.</div>
							: < div > {NOVnumberState} is not on any existing DS-1984.</div>}
					</InputLabel>
					<Input className={classes.styleforticketrangetext}
						type="number"
						id="idNOVnumber"
						onChange={(e) => handleOnChange(e)}
						placeholder="9-digit ECB Summons Number"
						name="fldNOVnumber"
						ref={register({
							required: { value: true, message: "PTTS expects the first NOV number (nine digits)." },
							min: {
								value: 100000000,
								message: "Too small. Individual ECB Summons are nine digit numbers starting with 1 or 2."
							},
							max: {
								value: 299999999,
								message: "Too big.  Individual ECB Summons are nine digit numbers starting with 1 or 2."
							},
						})}
						value={NOVnumberState}
					// defaultValue versus value --> value lets me change characters in the currently active control textbox
					/>
					<div>&nbsp;</div>
					<ErrorMessage
						errors={errors}
						name="fldNOVnumber"
						as={<ErrorMessageContainer />}
					/>
					{IsValidNOVnumberState
						?
						<button type="submit" onClick={(e) => { e.preventDefault(); history.push('/PTTS/NOVdetails/' + NOVnumberState); }}>Go to {NOVnumberState}</button>
						:
						<div>&nbsp;</div>
					}
					<Divider />
				</FormControl>
			</Grid>
		</Grid>
		)
}

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: 20,
		[theme.breakpoints.down("md")]: {
			paddingTop: 50,
			paddingLeft: 15,
			paddingRight: 15,
		},
	},
	paper: {
		width: "100%",
		display: "inline-block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 230,
		marginTop: 80,
		minWidth: 1000,
	},
	buttonContainer: {
		width: "100%",
		display: "flex",
		justifyContent: "flex-end",
	},

	button: {
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	dialog: {
		minHeight: 500
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 100,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 550,
		minWidth: 300,
	}
}));
