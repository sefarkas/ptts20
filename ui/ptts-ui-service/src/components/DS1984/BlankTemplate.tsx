﻿import { ErrorMessage } from "@hookform/error-message";
import { Button, FormControl, MenuItem, Select } from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useEffect, useState } from "react"; // , useReducer
import { Controller, useForm } from "react-hook-form"; // , FormProvider
import { isNullOrUndefined } from 'util';
import { allServices } from '../../services';
import { history } from "../../configureStore";
import { tSpecialDistrict, UsedTicketRange } from '../../model';
import { PTTSfunctions } from '../basicComponents';
import { ErrorMessageContainer, ErrorSummary } from '../basicComponents/validation';
import { DS1983basics } from "../DS1983";

type postDataResult<T> = {
	success: boolean;
	errors?: { [P in keyof T]?: string[] };
};

type FormData = {
	fldMessengerName: string;
	fldSummonsCount: number;
	fldBoxNumberedFrom: number;
	fldBoxNumberedTo: number;
	fldBorough: string;
	fldDistributorName: string;
	fldAcknowledgedBy: string;
	fldBoxNumber: number;
}

export function NewDS1984Form(props: any | undefined) {

	const classes = useStyles();

	let aParentDS1983: DS1983basics | any | undefined = props.history.location.state;

	let IsAlreadyUsedTktRangeState: boolean = false;

	const [isLoading, setLoading] = React.useState(false);

	const [AcknowledgedByState, setAcknowledgedByState] = useState("");
	const [BoxNumberedFromState, setBoxNumberedFromState] = useState("0");
	const [BoxNumberedFromStateMin, setBoxNumberedFromStateMin] = useState(isNullOrUndefined(aParentDS1983) ? 9999999 : aParentDS1983.DS1983fromTktNumber);
	const [BoxNumberedFromStateMax, setBoxNumberedFromStateMax] = useState(isNullOrUndefined(aParentDS1983) ? 29999999 : aParentDS1983.DS1983toTktNumber);
	const [BoxNumberedToState, setBoxNumberedToState] = useState("0");
	const [CreatedByState, setCreatedByState] = useState("");
	const [CreatedDateState, setCreatedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [DistributorNameState, setDistributorNameState] = useState("");
	const [DistributorRefNoState, setDistributorRefNoState] = useState("");
	const [DistributorSignatureState, setDistributorSignatureState] = useState("");
	const [DistributorSignedDateState, setDistributorSignedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [DistrictIdState, setDistrictIdState] = useState(0);
	const [DistrictState, setDistrictState] = useState("");
	const [DS1983ReceiptNoState, setDS1983ReceiptNoState] = useState(isNullOrUndefined(aParentDS1983) ? "created directly" : aParentDS1983.DS1983receiptNumber);
	const [FormStatusState, setFormStatusState] = useState("");
	const [IDState, setIDState] = useState(0);
	const [IsVoidState, setIsVoidState] = useState(false);
	const [ModifiedByState, setModifiedByState] = useState("");
	const [ModifiedOnState, setModifiedOnState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [PaycodeState, setPaycodeState] = useState("");
	const [ReceiptNumberState, setReceiptNumberState] = useState(isNullOrUndefined(aParentDS1983) ? "new created after save" : "");
	const [ReceivingOfficerNameState, setReceivingOfficerNameState] = useState("[REF# of Receiving Officer]");
	const [ReceivingOfficerSignatureState, setReceivingOfficerSignatureState] = useState("]");
	const [ReceivingOfficerTitleState, setReceivingOfficerTitleState] = useState("[filled out automatically]");
	const [RecOfficerRefNoState, setRecOfficerRefNoState] = useState("");
	const [TaxRegNumberState, setTaxRegNumberState] = useState("");

	const dummyDSspecialDistrict: tSpecialDistrict = {
		Id: 0,
		PayCode: "",
		Description: "",
		Remarks: "",
		SplinterDisrtict: false,
		MappingDistictNTI: "",
	}

	const [dsList, setdsList] = useState([dummyDSspecialDistrict]);

	const defaultValues = {
		Native: "",
		TextField: "",
		Select: "",
		ReactSelect: "",
		Checkbox: false,
		switch: false,
		RadioGroup: ""
	};

	const handleOnChange = (e: any) => {
		e.preventDefault();

		switch (e.target.name) {
			case "fldID": setIDState(e.target.value); break;
			case "fldReceiptNumber": setReceiptNumberState(e.target.value); break;
			case "fldDistrict": setDistrictState(e.target.value); break;
			case "fldReceivingOfficerName": setReceivingOfficerNameState(e.target.value); break;
			case "fldReceivingOfficerTitle": setReceivingOfficerTitleState(e.target.value); break;
			// case "fldBoxNumberedFrom": setBoxNumberedFromState(e.target.value); break;
			// case "fldBoxNumberedTo": setBoxNumberedToState(e.target.value); break;
			case "fldReceivingOfficerSignature": setReceivingOfficerSignatureState(e.target.value); break;
			//case "fldDistributorName": setDistributorNameState(e.target.value); break;
			case "fldDistributorNameREFNO":
				setDistributorSignedDateState(PTTSfunctions.Commonfunctions.todaysdate);
				setDistributorRefNoState(e.target.value);
				break; // useEffect will run a lookup to get name etc.
			case "fldDistributorSignature": setDistributorSignatureState(e.target.value); break;
			case "fldDistributorSignedDate": setDistributorSignedDateState(e.target.value); break;
			case "fldTaxRegNumber": setTaxRegNumberState(e.target.value); break;
			case "fldFormStatus": setFormStatusState(e.target.value); break;
			case "fldAcknowledgedBy": setAcknowledgedByState(e.target.value); break;
			case "fldModifiedOn": setModifiedOnState(e.target.value); break;
			case "fldModifiedBy": setModifiedByState(e.target.value); break;
			case "fldDistributorRefNo": setDistributorRefNoState(e.target.value); break;
			case "fldReceivingOfficerNameREFNO":
				setModifiedOnState(PTTSfunctions.Commonfunctions.todaysdate);
				setRecOfficerRefNoState(e.target.value);
				break; // useEffect will run a lookup to get name etc.
			case "fldDS1983ReceiptNo": setDS1983ReceiptNoState(e.target.value); break;
			case "fldDistrictId": setDistrictIdState(e.target.value); break;
			case "fldCreatedBy": setCreatedByState(e.target.value); break;
			case "fldCreatedDate": setCreatedDateState(e.target.value); break;
			case "fldPaycode": setPaycodeState(e.target.value); break;
			case "fldIsVoid": setIsVoidState(e.target.value); break;

			case "fldBoxNumberedFrom":
				IsAlreadyUsedTktRangeState = false;
				let dummy2n: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				if (isNaN(dummy2n)) {
					break;
				}
				if (dummy2n > 299999999) {
					alert("Expected first summons number is supposed to start with 1 or 2 and be nine-digits.  You entered a number that is too big.")
					break;
				} // less than 8 digits, not-a-number, or nine or more digits starting with 3 (Sanitation only prefixes 1 or 2)
				//            12345678
				if (dummy2n > 99999999) {
					dummy2n = Math.trunc(dummy2n / 10);
					const dummy2: string = PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(dummy2n);
					setBoxNumberedFromState(dummy2);
				}
				else {
					setBoxNumberedFromState(dummy2n.toString());
				};
				break;

			default:
				// code block
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.value);
		}

		if (CreatedDateState.valueOf !== PTTSfunctions.Commonfunctions.todaysdate) {
			setCreatedDateState(PTTSfunctions.Commonfunctions.todaysdate);
		}

		if (CreatedByState.length < 2) {
			const curUser: any = localStorage.getItem('user');
			const j: any = JSON.parse(curUser);
			const loggedInUser: string = j.data.userPreferences.userName;
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("created by ", loggedInUser);
			setCreatedByState(loggedInUser);
		}

	}

	const submitForm = async (data: FormData, e: any) => {
		if (DistrictState.length < 3) {
			alert("PTTS expects you to pick the location you delivered to.");
			setAcknowledgedByState("disputed");
		}
		else {
			const oneDS1984form = { // take the individual hook const and set each oneDS1984form object properties
				ID: IDState,
				ReceiptNumber: ReceiptNumberState,
				District: DistrictState,
				ReceivingOfficerName: ReceivingOfficerNameState,
				ReceivingOfficerTitle: ReceivingOfficerTitleState,
				BoxNumberedFrom: BoxNumberedFromState,
				BoxNumberedTo: BoxNumberedToState,
				ReceivingOfficerSignature: ReceivingOfficerSignatureState,
				DistributorName: DistributorNameState,
				DistributorSignature: DistributorSignatureState,
				DistributorSignedDate: DistributorSignedDateState,
				TaxRegNumber: TaxRegNumberState,
				FormStatus: FormStatusState,
				AcknowledgedBy: "Acknowledged", // AcknowledgedByState,
				ModifiedOn: ModifiedOnState,
				ModifiedBy: ModifiedByState,
				RecOfficerRefNo: RecOfficerRefNoState,
				DistributorRefNo: DistributorRefNoState,
				DS1983ReceiptNo: DS1983ReceiptNoState,
				DistrictId: DistrictIdState,
				CreatedBy: CreatedByState,
				CreatedDate: CreatedDateState,
				Paycode: PaycodeState,
				IsVoid: IsVoidState,
			}
			try {

				const response = await allServices.PTTSiudServices.iudDS1984(oneDS1984form);
				// response is the new receipt number
				if (isNullOrUndefined(response.data) || response.data === "[object Object]") {
					// "An error occurred while executing the command definition. See the inner exception for details.|
					// DS1983 is not created for the specified ticket range.Please have Enf HQ create 1983 for this box. "
					const importantPart: string = response.data
							.ExceptionMessage.replace("An error occurred while executing the command definition. See the inner exception for details.|", "");
					alert("I am having trouble with saving DS1984 values.  " + importantPart);
					// any error message is in the CustomerOrderNo property in the controller
				}
				else {
					history.push('/PTTS/existingDS1984/' + response.data);
				}
			}
			catch (err) {
		// anything else
				alert("I am having trouble with saving DS1984 values.  " + err.toString());
				// return err;
			};

		}

		// e.target.reset(); // reset after form submit
	};

	const onCancel = () => {
		history.push('/PTTS/home');
	};

	const handleDropDownChange = async (event: any) => {
		let newSelection: string = "Pick one";
		if (PTTSfunctions.Commonfunctions2.isNotDefined(event.target.innerText)) {
			eventCurrentTarget = event.target;
			alert("DropDown event object is marked 'notDefined'");
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(eventCurrentTarget);
		}
		else {
			newSelection = event.target.innerText;
			await setDistrictState(newSelection);
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Borough is" ,newSelection);
		}
		return newSelection;
	};

	const newDS1984 = () => { // used to clear the form
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("newDS1984 fired");
		setIDState(0);
		setReceiptNumberState("");
		setDistrictState("");
		setReceivingOfficerNameState("[REF# of Receiving Officer]");
		setReceivingOfficerTitleState("[filled out automatically]");
		setBoxNumberedFromState("0");
		setBoxNumberedToState("0");
		setReceivingOfficerSignatureState("");
		setDistributorNameState("");
		setDistributorSignatureState("");
		setDistributorSignedDateState(PTTSfunctions.Commonfunctions.todaysdate);
		setTaxRegNumberState("");
		setFormStatusState("");
		setAcknowledgedByState("");
		setModifiedOnState(PTTSfunctions.Commonfunctions.todaysdate);
		setModifiedByState("");
		setRecOfficerRefNoState("");
		setDistributorRefNoState("");
		setDS1983ReceiptNoState("");
		setDistrictIdState(0);
		setCreatedByState("");
		setCreatedDateState(PTTSfunctions.Commonfunctions.todaysdate);
		setPaycodeState("");
		setIsVoidState(false);
	};

	const renderActionButtons = () => {
		return (
			<div>
				<StyledButton
					className={classes.button}
					variant="outlined"
					color="primary"
					type="submit"
					onClick={handleSubmit(submitForm)}
				>
					Save/Acknowledge
						</StyledButton>
				<StyledButton
					className={classes.button}
					variant="outlined"
					color="primary"
					onClick={() => newDS1984()}
				>
					Reset
						</StyledButton>
				<StyledButton
					className={classes.button}
					variant="outlined"
					color="primary"
					onClick={onCancel}>
					Cancel
					</StyledButton>
			</div>
		)
	};

	const renderMenu = () => {
		if (!dsList) { return <div>still working ...</div> };
		return dsList.map(oneChoice => {
			return <MenuItem key={oneChoice.Id.toString()} value={oneChoice.Description}>{oneChoice.Description}</MenuItem>;
		});
	};

	const { register, handleSubmit, errors, setError } = useForm<FormData>();

	const { control } = useForm({ defaultValues });

	let eventCurrentTarget: any = null;

	function fIsAlreadyUsedTktRange(aDSform: string, LowHigh: string, TgtVal: string): boolean {
		let rdata: UsedTicketRange[] | undefined = undefined;
		let pIsAlreadyUsedTktRange: boolean = false;

		if (LowHigh === "Low"
			? Number.parseInt(BoxNumberedToState) > 0
			: Number.parseInt(BoxNumberedFromState) > 0)
		{
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Test a ticket range");
			const ECBfromState: string =
				BoxNumberedFromState.length < 9
					? PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(Number.parseInt(BoxNumberedFromState))
					: BoxNumberedFromState
			const ECBtoState: string =
				TgtVal.length < 9
					? PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(Number.parseInt(TgtVal) + 25 - 1)
					: TgtVal;
			(async () => {
				const tryThisRange = (
					{
						ECBSummonsNumberFrom: LowHigh === "Low"
							? TgtVal
							: ECBfromState,
						ECBSummonsNumberTo: LowHigh === "High"
							? TgtVal
							: ECBtoState,
						DSform: aDSform,
						ReceiptNumber: "",
					}
				);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("tryThisRange");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(IsAlreadyUsedTktRangeState);
				pIsAlreadyUsedTktRange = false;
				const response = await allServices.PTTSgetServices.refUsedTicketNumbers(tryThisRange);
				// axios always returns an object with a "data" child
				rdata = await response.data;
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange)
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
                if (!rdata) {
                    pIsAlreadyUsedTktRange = false;
                     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ticket range is so far unused based on no rdata object.");
                }
                else if (!rdata.length ? true : rdata.length === 0) {
                    pIsAlreadyUsedTktRange = false;
                     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ticket range is so far unused based on empty [].");
                }
                else {
                     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("already used ticket range");
                    pIsAlreadyUsedTktRange =
                        Number.parseInt(tryThisRange.ECBSummonsNumberFrom) > 0
                            && Number.parseInt(tryThisRange.ECBSummonsNumberTo) > 0
                            ? true : false;
                     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange and non-zeroes");
                     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(pIsAlreadyUsedTktRange);

                    if (pIsAlreadyUsedTktRange) {
                        let rdataLength: number = -1;
                        let pReceiptNumber: string = "";
                        if (!rdata![rdata.length - 1]) { // https://stackoverflow.com/questions/55190059/typescript-property-type-does-not-exist-on-type-never
                            rdataLength = 0
                            pReceiptNumber = " "
                        }
                        else {
                            rdataLength = rdata.length;
                            pReceiptNumber = " (" + rdata[0].ReceiptNumber + ") ";

                            alert(rdataLength > 1
                                ? rdataLength.toString() + " DS-1984s already use summons numbers in this range"
                                    + !pReceiptNumber ? "" : " e.g., " + pReceiptNumber + "."
                                : " Existing DS-1984" + pReceiptNumber + "already uses summons numbers in this range.");
                        }
                    }
                }
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange at end of ASYNC");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(pIsAlreadyUsedTktRange);
				IsAlreadyUsedTktRangeState = pIsAlreadyUsedTktRange;
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("IsAlreadyUsedTktRangeState at end of ASYNC");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(IsAlreadyUsedTktRangeState);
				return pIsAlreadyUsedTktRange;

			})();

		};
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange after ASYNC", pIsAlreadyUsedTktRange);
		return pIsAlreadyUsedTktRange;

	};

	useEffect(() => {
		(async () => {
			setLoading(true);
			const response = await allServices.PTTSgetServices.refSpecialDistricts(); // axios always returns an object with a "data" child
			const rdata: tSpecialDistrict[] = response.data;
			if (!rdata) {
				setLoading(true);
			}
			else {
				const rdataDesc: tSpecialDistrict[] = rdata.sort(function (a, b) {
					var nameA = !a.Description ? "" : a.Description.toUpperCase(); // ignore upper and lowercase
					var nameB = !b.Description ? "" : b.Description.toUpperCase(); // ignore upper and lowercase
					if (nameA < nameB) {
						return -1;
					}
					if (nameA > nameB) {
						return 1;
					}
					// names must be equal
					return 0;
				}).slice(0, 150); // ASC
				setdsList(rdataDesc);
				setLoading(false);
			}
		})();
	}, []);

	useEffect(() => { // something here prevents drawing the new DS1984 page
		let dummy1n: number = Number.parseInt(BoxNumberedFromState); // trick to do nothing when starting ticket number is less than 8 digits
		let dummy1: string = BoxNumberedFromState.toString();
		if (dummy1n < 300000000 || dummy1n === NaN) {
			let dummy2n: number = Number.parseInt(BoxNumberedToState);
			dummy2n = Math.trunc(dummy2n/10)
			if (dummy1n > 99999999 && dummy2n - dummy1n !== 25 - 1) { // needs a new to-box number
				// extract left eight digits to test for use on another DS-1984
				dummy1n = Math.trunc(dummy1n / 10) // strip the check-sum digit off the nine-digit summons number
				dummy1 = dummy1n.toString();

				const bRslt1: boolean = fIsAlreadyUsedTktRange("DS1984", "Low", dummy1);
				IsAlreadyUsedTktRangeState = bRslt1;

				if (!bRslt1) { // from-box number is OKay; create a to-box number with a check-sum digit
					const updatedTktNo2: string = PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(dummy1n + 25 - 1); // summons number is prefix and seven digit NOV#; 25 in a pack

					dummy1n > 9999999 // at least eight-digits
						? setBoxNumberedToState(updatedTktNo2)
						: dummy1n = 0; // if FROM NOV# is eight digits than set the to-NOV#
				}
				else {
					setBoxNumberedToState("0");
				}
				setBoxNumberedFromState(BoxNumberedFromState);
			}
			else if (dummy1n > 9999999 && dummy1n < 100000000) { // box number is eight digits and needs a check-sum suffix
				const bRslt2: boolean = fIsAlreadyUsedTktRange("DS1984", "Low", dummy1);
				IsAlreadyUsedTktRangeState = bRslt2;
				setBoxNumberedFromState(PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(dummy1n));
			}
			else {
				setBoxNumberedFromState(dummy1);
            }

		} // eight digit integer based upon 9 digit input
		else {
			alert("Low ticket number more than the expected nine digits.");
        }
	},
		[
			BoxNumberedFromState,
		]
	);

	useEffect(() => {
		if (parseInt(RecOfficerRefNoState) > 0) {
			if (RecOfficerRefNoState.toString().length === 7) {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("REF#", RecOfficerRefNoState);
				(async () => {
					setLoading(true);

					const response: any = await allServices.EISgetServices.EISemployeeDetailsRN(RecOfficerRefNoState); // axios always returns an object with a "data" child
					const rdata: any = response.data;
					if (!rdata) {
						setLoading(true);
					}
					else {
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
						if (PTTSfunctions.Commonfunctions2.isNotDefined(rdata.employeeModel)) {
							setRecOfficerRefNoState("Invalid REF#: " + RecOfficerRefNoState);
						}
						else {
							try {

								if (isNullOrUndefined(rdata.jobModel.dsnyInformation)) { throw "rdata.jobModel.dsnyInformation missing" };
								let eisPayCodeCorrelID: string = rdata.jobModel.dsnyInformation.payCodeId.correlation
								eisPayCodeCorrelID = eisPayCodeCorrelID.substring(eisPayCodeCorrelID.length - 4);
								setPaycodeState( // this is an integral part of the DS-1984 Receipt# that will be composed by UID
									// actual paycode may be different; real one needs a lookup using this CorrelationID string
									eisPayCodeCorrelID
								);
								setReceivingOfficerTitleState(rdata.jobModel.dsnyInformation.titleId.correlation);
								if (isNullOrUndefined(rdata.employeeModel)) { throw "rdata.employeeModel missing" };
								setReceivingOfficerNameState(rdata.employeeModel.names[0].firstName.substring(0, 1)
									+ ' ' + rdata.employeeModel.names[0].lastName);
								if (isNullOrUndefined(rdata.jobModel.badgeNumber)) { throw "rdata.jobModel.badgeNumber missing" };
								setTaxRegNumberState(isNullOrUndefined(rdata.jobModel.badgeNumber)
									? "NA"
									: rdata.jobModel.badgeNumber.badgeNumber);  // need to change this to TAX ID aka. Pension Number
							}
							catch (e) {
								alert(e.toString() + " using (" + RecOfficerRefNoState + ").");
								if (e.toString() === "rdata.jobModel.dsnyInformation missing") {
									setPaycodeState( // this is an integral part of the DS-1984 Receipt# that will be composed by UID
										// actual paycode may be different; real one needs a lookup using this CorrelationID string
										"UNKN"
									)
								};
								setReceivingOfficerTitleState("type it in later");
								setTaxRegNumberState("type one in later");
							}

						}
						setLoading(false);
					}
				})();

			}
			else {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("numeric but not 7 digits -- officer")
			}
		}
		else {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("part of Officer's last name");
		};
	},
		[RecOfficerRefNoState]
	);

	useEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Distributor lookup useEffect triggered");
		if (parseInt(DistributorRefNoState) > 0) {
			if (DistributorRefNoState.toString().length === 7) {
				(async () => {
					setLoading(true);
					const response: any = await allServices.EISgetServices.EISemployeeDetailsRN(DistributorRefNoState); // axios always returns an object with a "data" child
					const rdata: any = response.data;
					if (!rdata) {
						setLoading(true);
					}
					else {
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
						if (PTTSfunctions.Commonfunctions2.isNotDefined(rdata.employeeModel)) {
							setDistributorNameState("Invalid REF#: " + DistributorRefNoState);
						}
						else {
							setDistributorNameState(rdata.employeeModel.names[0].firstName.substring(0, 1) + ' ' + rdata.employeeModel.names[0].lastName);
						}
						setLoading(false);
					}
				})();

			}
			else {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("numeric but not 7 digits -- distributor")
			}
		}
		else {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("part of Distributor's last name");
		};
	},
		[DistributorRefNoState]
	);


	if (isLoading) {
		return (<div>Loading ...</div>);
	}
	else {


		// onSubmit is not triggered by type=submit StyledButtons; need an explicit onClick for saving/updating
		return (
			<FormControl onSubmit={handleSubmit(submitForm)}>
				<Grid className={classes.formposition}>
					<Grid container className={classes.darkborder}>
						<Grid item xs={2} className={classes.styleforlogo}>
							<img src={PTTSfunctions.Logos.DSNYLogo.logoImg} alt="Sanitation Enforcement" />
						</Grid>
						<Grid item xs={9} className={classes.styleforformtitle}>
							<div className={classes.styleforformtitletext}>THE CITY OF NEW YORK Department of Sanitation</div>
							<div className={classes.styleforformtitletext}>RECEIPT NO:&nbsp;
							<div className={classes.styleforformreceipttextred}>{ReceiptNumberState}</div></div>
							<div className={classes.styleforformtitletext}>ECB - SUMMONS ISSUED RECEIPT  DS 1984 (4-95)</div>
						</Grid>
					</Grid>
					<div>&nbsp;</div>
					<div>&nbsp;</div>
					<Grid container>
						<Grid item xs={12}>
							<ErrorSummary errors={errors} />
							{IsAlreadyUsedTktRangeState ? <div>Existing DS-1984(s) already claims tickets between {BoxNumberedFromState} and {BoxNumberedToState}.</div> : <div>&nbsp;</div>}
						</Grid>
						<Grid item xs={12}>
							{renderActionButtons()}
						</Grid>
					</Grid>
				</Grid>
				<Grid className={classes.formposition}>
					<Grid container className={classes.darkborder}>
						<Grid item xs={12} >
						<div className={classes.stylefortopacknowledgementtext}>I,&nbsp;
							<i>{ReceivingOfficerNameState}</i>&nbsp;working at&nbsp;<i>{ReceivingOfficerTitleState}</i>&nbsp;
							</div>
							<div className={classes.stylefortopacknowledgementtext}><br />&nbsp;assigned to this District or location:&nbsp;
						<Controller
								className={classes.stylefortopdistributionrowtextwide}
								as={
									<Select
										value={DistrictState ? DistrictState : " "}
										onMouseDown={handleDropDownChange}
										name="fldDistrict"
										ref={register({
											required: { value: true, message: "You must pick a location where you accepted the 25 pack." },
											minLength: { value: 3, message: "You have to pick a location where you accepted the 25 pack" }
										})}
									>
										{renderMenu()}
									</Select>
								}
								name="Select"
								id="SelectDistrict"
								control={control}
								defaultValue={DistrictState}
							/>
								<div>&nbsp;</div>
								<ErrorMessage
								errors={errors}
								name="fldDistrict"
								as={<ErrorMessageContainer />}
							/>
							</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={12} className={classes.stylefortopacknowledgement}>
							<div className={classes.stylefortopacknowledgementtext}>
								<div>
									&nbsp;acknowledged receipt of 25 (ECB) Notice of Violations
									</div>
								<div>&nbsp;numbered from:&nbsp;
								<input className={classes.styleforticketrangetext} type="number" onChange={(e) => handleOnChange(e)}
									name="fldBoxNumberedFrom"
									ref={register({
										required: { value: true, message: "PTTS expects the first NOV number (nine digits)." },
										min: {
											value: BoxNumberedFromStateMin*10,
											message: Number.parseInt(BoxNumberedFromStateMin)
												? "Too small. Parent DS-1983 only allows a nine digit individual NOV number more than " + BoxNumberedFromStateMin*10
												: "Too small. Individual NOV blanks have nine digit numbers starting with 1 or 2."
										},
										max: {
											value: (BoxNumberedFromStateMax+1)*10,
											message: Number.parseInt(BoxNumberedFromStateMax+1) 
												? "Too big.  Parent DS-1983 only allows a nine digit individual NOV number less than " + (BoxNumberedFromStateMax + 1)*10
												: "Too big.  Individual NOV blanks have nine digit numbers starting with 1 or 2."
										},
										validate: (fldVal) => !fIsAlreadyUsedTktRange("DS1984", "Low", fldVal),
										// true means validation is OKay, value is proper
									})}
									value={BoxNumberedFromState}
									// defaultValue versus value --> value lets me change characters in the currently active control textbox
								/>
							&nbsp;to:&nbsp;
								<input className={classes.styleforticketrangetext} type="number" onChange={(e) => handleOnChange(e)}
									name="fldBoxNumberedTo" ref={register}
									value={BoxNumberedToState}
									/>
								</div>
							</div>
						</Grid>
					</Grid>
					<Grid item xs={12} className={classes.darkborder}>
						<Grid className={classes.stylefortopacknowledgement} >
							<div className={classes.stylefortopacknowledgementtext}>
								<div>
									&nbsp;Notice of Violations must be issued in pre-wrapped packs of 25.
								</div>
								<div>
									&nbsp;All Notice of Violations (issued) will be written in numerical order.
								</div>
							</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={4}>
							<Grid container>
								<Grid item xs={12} >

									<div className={classes.stylefortopdistributionrow} >DISTRIBUTOR'S NAME</div>
									<div >
										<input className={classes.stylefortopdistributionrowtextwide} type="text"
											onChange={(e) => handleOnChange(e)}
											name="fldDistributorNameREFNO"
											placeholder="REF# w/zeroes"
											ref={register({
												required: { value: true, message: "PTTS expects a reference number for the Distributor." },
												pattern: {
													value: /^[A-Za-z]|([\w]+['][\w]+)|^\d{7}$/,
													message: "Distributor: Enter a seven digit REF# so PTTS can look up employee information."
												}
											})}
											defaultValue={DistributorNameState}
										/>
										<div>&nbsp;</div>
										<ErrorMessage
											errors={errors}
											name="fldDistributorNameREFNO"
											as={<ErrorMessageContainer />}
										/>
									</div>
								</Grid>
								<Grid item xs={12} >
									<div >SIGNATURE</div>
								</Grid>
							</Grid>
						</Grid>
						<Grid item xs={2}>
							<div >DATE</div>
							<div >
								<input className={classes.stylefortopdistributionrowtext}
									type="text" onChange={(e) => handleOnChange(e)}
									name="fldDistributorSignedDate" ref={register}
									value={PTTSfunctions.Commonfunctions.formatDateSlashes((DistributorSignedDateState))}
								/>
							</div>
						</Grid>
						<Grid item xs={4}  >
							<Grid container>
								<Grid item xs={12}>
									<div className={classes.stylefortopdistributionrow}>RECEIVING OFFICER'S NAME</div>
									<div className={classes.thinborderLeft} >
										<input className={classes.stylefortopdistributionrowtextwide}
											type="text" onChange={(e) => handleOnChange(e)}
											name="fldReceivingOfficerNameREFNO"
											placeholder="REF# w/zeroes"
											ref={register({
												required: { value: true, message: "PTTS expects a reference number for the Officer." },
												pattern: {
													value: /^[A-Za-z]|([\w]+['][\w]+)|^\d{7}$/,
													message: "Officer: Enter a seven digit REF# so PTTS can look up employee information."
												}
											})}
											defaultValue={ReceivingOfficerNameState}
										/>
										<div>&nbsp;</div>
										<ErrorMessage
											errors={errors}
											name="fldReceivingOfficerNameREFNO"
											as={<ErrorMessageContainer />}
										/>
									</div>
								</Grid>
								<Grid container className={classes.thinborderTop}>
									<Grid item xs={6}>
										<div >SIGNATURE</div>
									</Grid>
									<Grid item xs={6} className={classes.thinborderLeft}>
										<div className={classes.stylefortopdistributionrow}>TITLE</div>
										<div className={classes.thinborderLeft}>
											<input className={classes.stylefortopdistributionrowtext}
												type="text" onChange={(e) => handleOnChange(e)}
												name="fldReceivingOfficerTitleState" ref={register}
												value={ReceivingOfficerTitleState}
											/>
										</div>
									</Grid>
								</Grid>

							</Grid>
						</Grid>
						<Grid item xs={2}>
							<Grid container>
								<Grid item xs={12} className={classes.thinborderLeft}>
									<div className={classes.stylefortopdistributionrow}>TAX REG. #</div>
									<div className={classes.thinborderLeft}>
										<input className={classes.stylefortopdistributionrowtext} type="text" onChange={(e) => handleOnChange(e)} name="fldBadgeNo" ref={register}
											value={TaxRegNumberState}
										/>
									</div>
								</Grid>
								<Grid item xs={12} className={classes.thinborderTop}>
									<div className={classes.stylefortopdistributionrow} >DATE</div>
									<div className={classes.thinborderLeft}>
										<input className={classes.stylefortopdistributionrowtext} type="text" onChange={(e) => handleOnChange(e)} name="fldMessengerSignedDate" ref={register}
											value={PTTSfunctions.Commonfunctions.formatDateSlashes((ModifiedOnState))}
										/>
									</div>
								</Grid>
							</Grid>
						</Grid>
					</Grid>
					<Grid item xs={12} className={classes.stylefortopacknowledgement}>
						<div className={classes.stylefortopacknowledgementtext}>
							<StyledButton
								className={classes.button}
								variant="outlined"
								color="secondary"
								type="submit"
								onClick={handleSubmit(submitForm)}
							>
								Give 25 tickets to {TaxRegNumberState}
							</StyledButton>
							<StyledButton
								className={classes.button}
								variant="outlined"
								color="primary"
								onClick={onCancel}>
								Edit
						</StyledButton>
						</div>
					</Grid>
					<Grid item xs={12}>
						{renderActionButtons()}
					</Grid>
				</Grid>

			</FormControl>
		)
	}

};


const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5,
		marginTop: 0
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const useStyles = makeStyles({
	root: {
		flexGrow: 1,
		height: "100%",
		paddingLeft: 15,
		paddingRight: 15,

	},
	reactselect: {
		maxWidth: "calc(100% - 20px)",
		marginLeft: 10,
		marginTop: 0,
		fontSize: 12,
		'.control': {
			height: 25
		}
	},
	formposition: {
		marginLeft: 280,
	},
	darkborder: {
		borderWidth: 3,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderTop: {
		borderTopWidth: 2,
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderLeft: {
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderTopWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	styleforlogo: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforsubmit: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforformtitle: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
	},
	styleforformtitletext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "black",
		minWidth: 500,
	},
	styleforformorder: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "center",
	},
	styleforformordertext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "center",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 500,
		color: "black",
	},
	styleforticketrange: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
	},
	styleforticketrangetextTO: {
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "flex",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
	},
	stylefortopacknowledgement: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	stylefortopacknowledgementtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 250,
		minWidth: 130
	},
	stylefortopdistributionrow: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
		maxWidth: 300,
	},
	stylefortopdistributionrowtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80
	},
	stylefortopdistributionrowtextwide: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		minWidth: 300,
		maxWidth: 600,
	},
	stylefortopdistributionrowtextdate: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 5,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		minWidth: 170,
		maxWidth: 210,
	},
	styleforformreceipt: {
		verticalAlign: "center",
		borderTopStyle: "none",
	},
	styleforformreceipttext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		display: "inline",
		marginLeft: 10,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
		textAlign: "center",
	},
	styleforformreceipttextred: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "red",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
	},
	button: {
		marginTop: 20,
	},

	formstyle: {
		maxWidth: "100%",
		padding: 0,
	},

	input: {
		display: "block",
		borderRadius: 4,
		border: "1px solid",
		padding: "10px 15px",
		marginBottom: 10,
		fontSize: 14,
		maxWidth: 400,
		minWidth: 200,
	},
	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginBottom: 13,
		marginTop: 20,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
	},
	buttonSubmit: {
		background: "#0e8c1b",
		color: "white",
		border: "none",
		marginTop: 20,
		padding: 20,
		fontSize: 16,
		fontWeight: 100,
		width: 200,
	}
});
