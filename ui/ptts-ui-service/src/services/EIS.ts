﻿import axios from "axios";

// const API_URL = 'https://10.155.206.185/crusher/eis-gateway-service/api/eis/personnel/0001255';
// const API_URLhttp = 'http://10.155.226.39:11979/crusher/eis-gateway-service/api/eis/personnel/0001255';
const API_URLhttp = 'http://10.155.204.153:11979//crusher/eis-gateway-service/api/eis/personnel/0001255'; 

// request the token
// subscribe to this event and use the returned json to save your token to state or session storage
function fRequestAccessToken(data: any) {
    const loginInfo = `${data}&grant_type=password`;
    return fetch(`${API_URLhttp}Token`, {
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/x-www-form-urlencoded',
        }),
        body: loginInfo,
    })
        .then((response) => response.json())
        .catch ((error) => error);
    // in your case set state to returned token
}

//// use said token to authenticate request
//function requestUserInfo(token) {
//    return fetch(`${API_URL}api/participant/userinfo`, {
//        method: 'GET',
//        headers: new Headers({
//            Authorization: `Bearer ${token}`,
//        }),
//    })
//        .then((response) => response.json());
//}

function fEmployeeDetail(employeeId: any) {
    const token = fRequestAccessToken({ username: 'smart_userstg46', password: 'Change4dsny' });

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa('username:smart_userstg46&password:Change4dsny'),
        },
        //  body: JSON.stringify({ employeeId }),

    };
    // return fetch(`https://10.155.206.185/crusher/eis-gateway-service/api/eis/personnel/0001255`, requestOptions)
    return fetch(`http://10.155.226.39:11979/crusher/eis-gateway-service/api/eis/personnel/` + employeeId, requestOptions)
        //.then(handleResponse)
        .then(response => {
            return response.json;
        });
}


function fEmployeeDetailFromDS249(refNum: string) {
    // (async () => {
    // await
    return axios.get(`${process.env.REACT_APP_DS249_API}/Employee/GetEmployeeByRefNo/` + refNum)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
    //})
}

export const allServices = {
    employeeDetail: fEmployeeDetail,
    EISemployeeDetailsRN: fEmployeeDetailFromDS249, //EISService.employeeDetail,

}