﻿export interface ECB603 {
    [x: string]: any;
    CartonNoFrom: number;
    CartonNoTo: number;
    CartonQty: number;
    CreatedBy: string;
    CreatedOn: Date;
    CustomerOrderNo: string;
    DS1983ReceiptNo: string;
    FactoryJobNo: string;
    ID: number;
    IsVoid: boolean;
    ModifiedBy: string;
    ModifiedOn: Date;
    NoOfParts: number;
    OrderNo: string;
    ReceiptNo: string;
    TicketNoFrom: number;
    TicketNoTo: number;
    Completed?: boolean;
}

export interface efECB603 {
    CartonNoFrom: number;
    CartonNoTo: number;
    CartonQty: number;
    CreatedBy: string;
    CreatedOn: string;
    CustomerOrderNo: string;
    DS1983ReceiptNo: string;
    FactoryJobNo: string;
    ID: number;
    IsVoid: boolean;
    ModifiedBy: string;
    ModifiedOn: Date;
    NoOfParts: number;
    OrderNo: string;
    ReceiptNo: string;
    TicketNoFrom: number;
    TicketNoTo: number;
}

export enum ECB603Actions {
    ADD_ECB603 = "ADD_ECB603",
    UPDATE_ECB603 = "UPDATE_ECB603",
    DELETE_ECB603 = "DELETE_ECB603",
    COMPLETE_ECB603 = "COMPLETE_ECB603",
    REVERT2INCOMPLETE_ECB603 = "REVERT2INCOMPLETE_ECB603",
    RESET_ECB603 = "RESET_ECB603List",
}

interface ECB603ActionType<T, P> {
    type: T;
    payload: P;
}

export type ECB603Action =
    | ECB603ActionType<typeof ECB603Actions.ADD_ECB603, ECB603>
    | ECB603ActionType<typeof ECB603Actions.COMPLETE_ECB603, number>
    | ECB603ActionType<typeof ECB603Actions.REVERT2INCOMPLETE_ECB603, number>
    | ECB603ActionType<typeof ECB603Actions.UPDATE_ECB603, ECB603>
    | ECB603ActionType<typeof ECB603Actions.DELETE_ECB603, number>
    ;
