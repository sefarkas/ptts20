﻿import React from 'react';
import { useFormContext } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import { Button, Typography, TextareaAutosize, Container } from "@material-ui/core";
import styles from "./complaintStyles";
import { ComplaintCharge, FileCharge, FileObject } from "../complaint";
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/styles";
import { DropzoneArea } from "material-ui-dropzone";

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const StyledDropzoneArea = withStyles({
	text: {
		marginTop: "10px",
		marginBottom: "10px"
	}	
})(DropzoneArea);

interface Props {
	charges: any;
	chargeOptions: any;
	chargeToFileOptions: any;
	districtOptions: any;
	subChargeOptions: any;
	handleChange: (e: any, control?: string, idx?: number) => void;
	handleRemoveCharge: (event: any, idx: number) => void;	
	handleAddChargeOptnClicked: (event: any, optionSelected: string) => void;
	handleChargesClicked: (event: any) => void;
	showAddChargesOptns: boolean;
	files: any;
	dropFiles: (event: any) => void;
	removeFile: (deletedFileObject: any, index: number) => void;
	comments: string;
	handleCommentsChanged: (event: any) => void;
}

export function ComplaintCharges(props: Props) {
	const classes = styles();
	const classes2 = useStyles();
	const methods = useFormContext();


	return (
		<Grid container className={classes.grid} spacing={0}>
			<Grid item xs={3}>
				<label className={classes.labelReadOnlyData}>Section 2</label>
				<label className={classes.labelSectionTitle}>Rules And Orders Violated</label>
			</Grid>
			{props.charges.map(function (d: any, idx: number) {
				return (
					<ComplaintCharge key={idx} charge={d} idx={idx} chargeOptions={props.chargeOptions} subChargeOptions={props.subChargeOptions} districtOptions={props.districtOptions} handleChange={props.handleChange} handleRemoveCharge={props.handleRemoveCharge}/>
					)
				})}
			<Grid container item xs={12} spacing={0}>
				<Grid item xs={3} />
				<Grid container item xs={9} spacing={0}>
					<Grid item xs={12} className={classes.gridCell}>
						<StyledButton
							className={classes.buttonInner}
							variant="outlined"
							color="primary"
							onClick={props.handleChargesClicked}>
							+ Add Charges
				        </StyledButton>

						{props.showAddChargesOptns === true ?
							<div className={classes.divOptions}>
								<div className={classes.divOptionItem} onClick={e => props.handleAddChargeOptnClicked(e, "new")}><label className={classes.labelOptions}>Add charge</label></div>
								<div className={classes.divOptionItem} onClick={e => props.handleAddChargeOptnClicked(e, "same")}><label className={classes.labelOptions}>Add charge with same date & location</label></div>
							</div> : null}

					</Grid>
					<Grid container item xs={12} spacing={0}>
						<Grid item xs={6}>
							<label className={classes.labelX}>Comments</label></Grid>
						<Grid item xs={6}>
							<Typography className={classes.labelY}>{props.comments.length}/500</Typography></Grid>
						<Grid container item xs={12} className={classes.gridCell}>
							<TextareaAutosize name="comments" className={classes.textArea} rowsMin={6} placeholder="Enter Comments..." onChange={props.handleCommentsChanged} maxLength={500} />
							<label className={classes.label}>Supporting Documents</label>							
							{props.files.length > 0 ?
								<Container className={classes2.fileArea}>
									{props.files.map(function (d: any, idx: number) {
										return (
											<FileCharge
												key={idx}
												idx={idx}
												file={d}
												totalFiles={props.files.length}
												removeFile={props.removeFile}
												chargeToFileOptions={props.chargeToFileOptions}
												handleChange={props.handleChange}/>
										)
									})}</Container> : null}
							<StyledDropzoneArea
								dropzoneText="Drop files here or click to choose a file"
								onChange={props.dropFiles}
								dropzoneClass={classes2.dropFileArea}
								showFileNames={true}
								filesLimit={25}
								showPreviewsInDropzone={true}>
							</StyledDropzoneArea>
						</Grid>
					</Grid>
				</Grid>
			</Grid>
		</Grid>
	);
}

const useStyles = makeStyles({
	dropFileArea: {
		width: "95%",
		borderColor: "#c5c8c5",
		border: "1px dashed",
		minHeight: "120px",	
		overflowX: "hidden",		
		"&:hover": {
			overflowX: "scroll",
		},		
	},
	fileArea: {
		width: "95%",
		borderColor: "#c5c8c5",
		border: "1px dashed",
		marginLeft: "0px",
		marginRight: "0px",
		paddingLeft: "0px",
		paddingRight: "0px"
	},
}); //showPreviewsInDropzone={false}  //onDelete={props.removeFile(e.File, e.)}} 