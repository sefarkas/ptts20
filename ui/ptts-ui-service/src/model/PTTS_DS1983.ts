﻿export interface DS1983 {
    [x: string]: any;
    ReceiptNumber: string;
    MessengerName: string;
    SummonsCount: number;
    SummonsFromNumber: number;
    SummonsToNumber: number;
    Borough: string;
    DistributorName: string;
    DistributorSignature: string;
    DistributorSignedDate: Date;
    MessengerSignature: string;
    MessengerTitle: string;
    MessengerSignedDate: Date;
    AcknowledgedBy: string;
    ID: number;
    BoxNumber: number;
    FormStatus: string;
    ModifiedOn: Date;
    ModifiedBy: string;
    BadgeNo: string;
    DistributorRefNo: string;
    CreatedBy: string;
    CreatedDate: Date;
    DistrictID: number;
    PayCode: string;
    IsVoid: boolean;
}

export interface efDS1983 {
    ReceiptNumber: string;
    MessengerName: string;
    SummonsCount: number;
    SummonsFromNumber: number;
    SummonsToNumber: number;
    Borough: string;
    DistributorName: string;
    DistributorSignature: string;
    DistributorSignedDate: Date;
    MessengerSignature: string;
    MessengerTitle: string;
    MessengerSignedDate: Date;
    AcknowledgedBy: string;
    ID: number;
    BoxNumber: number;
    FormStatus: string;
    ModifiedOn: Date;
    ModifiedBy: string;
    BadgeNo: string;
    DistributorRefNo: string;
    CreatedBy: string;
    CreatedDate: string;
    DistrictID: number;
    PayCode: string;
    IsVoid: boolean;

}

export enum DS1983Actions {
    ADD_DS1983 = "ADD_DS1983",
    UPDATE_DS1983 = "UPDATE_DS1983",
    DELETE_DS1983 = "DELETE_DS1983",
    COMPLETE_DS1983 = "COMPLETE_DS1983",
    REVERT2INCOMPLETE_DS1983 = "REVERT2INCOMPLETE_DS1983",
    RESET_DS1983 = "RESET_DS1983List",
}

interface DS1983ActionType<T, P> {
    type: T;
    payload: P;
}

export type DS1983Action =
    | DS1983ActionType<typeof DS1983Actions.ADD_DS1983, DS1983>
    | DS1983ActionType<typeof DS1983Actions.COMPLETE_DS1983, number>
    | DS1983ActionType<typeof DS1983Actions.REVERT2INCOMPLETE_DS1983, number>
    | DS1983ActionType<typeof DS1983Actions.UPDATE_DS1983, DS1983>
    | DS1983ActionType<typeof DS1983Actions.DELETE_DS1983, number>
    ;
