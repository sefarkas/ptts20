import { ComplaintAction, ComplaintActions, Complaint } from "../model";
import createReducer from "./createReducer";

export const complaintList = createReducer<Complaint[]>([], {
	[ComplaintActions.ADD_COMPLAINT](state: Complaint[], action: ComplaintAction) {
		return [...state, action.payload];
	},
	[ComplaintActions.COMPLETE_COMPLAINT](state: Complaint[], action: ComplaintAction) {
		// search after Complaint item with the given id and set completed to true
		return state.map(t =>
			t.id === action.payload ? { ...t, completed: true } : t
		);
	},
	[ComplaintActions.REVERT2INCOMPLETE_COMPLAINT](state: Complaint[], action: ComplaintAction) {
		// search after Complaint item with the given id and set completed to false
		return state.map(t =>
			t.id === action.payload ? { ...t, completed: false } : t
		);
	},
	[ComplaintActions.DELETE_COMPLAINT](state: Complaint[], action: ComplaintAction) {
		// remove all Complaint with the given id
		return state.filter(t => t.id !== action.payload);
	},
});
