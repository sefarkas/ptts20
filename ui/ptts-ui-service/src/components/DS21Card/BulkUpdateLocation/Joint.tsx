﻿// prettier-ignore
import { ErrorMessage } from "@hookform/error-message";
import {
	Button,
	Divider, FormControl, FormControlLabel, FormLabel, Grid, Input, InputLabel, Link, Paper, Radio, RadioGroup, Table, TableBody, TableCell,
	TableHead, TableRow, withStyles
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { allActions } from "../../../actions";
import { tOneNOV, UsedTicketRange, tBulkUpdateRow, BulkUpdateNOV_ACTION, tDS21_TicketStatus, DS21CardActions } from "../../../model";
import { allServices } from '../../../services';
import { PTTSfunctions } from "../../basicComponents";
import Loader from "../../basicComponents/Loader";
import { ErrorMessageContainer, ErrorSummary } from "../../basicComponents/validation";
import { dummyBulkUpdateThisNOV } from "../../../reducers/DS21Card";
import { dummies } from "../../../reducers";
import { store } from "../../../reducers/PTTShookstore";

type FormData = {
	fldNOVnumber: number;
}

export function BulkUpdateTableAndFooter(props: any) { // expect a table of with up to 20 rows with NOV#s.

	const classes = useStyles();
	const dispatch = useDispatch();
	const mountedRef = React.useRef(true);

	let history = useHistory();
	let eventCurrentTarget: any = null;

	const { register, handleSubmit, errors, setError } = useForm<FormData>();

	const contentparent: any = useSelector((state: any) => state.DS21CardReducer);
	const [arr_20BulkUpdateTheseNOVs, set_20BulkUpdateTheseNOVs] = useState<tBulkUpdateRow[]>(contentparent.ST_20BulkUpdateTheseNOVs);
	const [NOVnumberState, setNOVnumberState] = useState(contentparent.ST_20BulkUpdateTheseNOVs[!contentparent ? 0 : contentparent.ST_20BulkUpdateTheseNOVs.length - 1].NOVnumber);
	const [SearchForNOVnumberState, setSearchForNOVnumberState] = useState(contentparent.ST_20BulkUpdateTheseNOVs[0].NOVnumber);

	const [IsValidNOVnumberState, setIsValidNOVnumberState] = useState(false);
	const [isLoading, setLoading] = useState(!contentparent.ST_20BulkUpdateTheseNOVs ? true : false);

	const [dsAttributesOfOneNOV, setdsAttributesOfOneNOV] = useState<tOneNOV>(contentparent.ST_ATTRIBUTESOFONENOV);
	//const [isLoadingNOVftr, setLoadingNOVftr] = useState(!contentparent.ST_20BulkUpdateTheseNOVs ? true : contentparent.ST_20BulkUpdateTheseNOVs.length < 2 ? true : false);
	const [isReadyToDraw, setReadyToDraw] = useState(false);
	const [isLoadingTSO, setLoadingTSO] = useState(false);
	const [dsTSOs, setdsTSOs] = useState([dummies.dummyTicketStatusOption]);
	const [currentTSO, setCurrentTSO] = useState<number>(dummies.dummyTicketStatusOption.ID);
	const [renderTSO, setRenderTSO] = useState({ renderThis: [(<div key="1">initial</div>)] });
	const [AllNOVsHaveThisTSOinCommonState, setAllNOVsHaveThisTSOinCommonState] = useState(0);

	const cb = (response: any) => {
		let pIsValidNOVnumber: boolean = false;
		// axios always returns an object with a "data" child
		const rdata: UsedTicketRange = response.data;
		if (!rdata) {
			return;
		}
		else if (!rdata.length ? true : rdata.length === 0) {
			return;
		}
		else {
			pIsValidNOVnumber =
				Number.parseInt(rdata[0].ECBSummonsNumberFrom) > 0
					&& Number.parseInt(rdata[0].ECBSummonsNumberTo) > 0
					? true : false;
		}
		arr_20BulkUpdateTheseNOVs[0].NOVnumberIsValid = pIsValidNOVnumber;
		if (NOVnumberState !== arr_20BulkUpdateTheseNOVs[0].NOVnumber) {
			setNOVnumberState(arr_20BulkUpdateTheseNOVs[0].NOVnumber);
		}
		if (pIsValidNOVnumber) {
			setIsValidNOVnumberState(pIsValidNOVnumber);
		}
	}

	const er = (response: any) => {
		alert("BulkUpdateLocation/Joint/er: " + response);
	}

	const cbOneNOVupt = (response: any) => {
		let rdata: tOneNOV = response.data;
		if (!rdata) { return; };

		const allRows: tBulkUpdateRow[] = arr_20BulkUpdateTheseNOVs; // contentparent.ST_20BulkUpdateTheseNOVs;
		const indRow: number = allRows.findIndex((aRow: tBulkUpdateRow) => aRow.NOVnumber === rdata.ECBSummonsNumber);
		//	alert("cbOneNOVupt -- Cannot find NOV# " + rdata.ECBSummonsNumber + " in the list of entered tickets.")
		allRows[indRow].NOVnumberIsValid = true; allRows[indRow].Message = "";
		allRows[indRow].TicketStatusID = rdata.TicketStatusID;
		if (allRows.length === 2) {
			setAllNOVsHaveThisTSOinCommonState(rdata.TicketStatusID)
		}
		arr_20BulkUpdateTheseNOVs_ADD(allRows[indRow]);
	}

	const cbIudNOVs = async (response: any) => {
		try {
			const oneNOV: tOneNOV = response.data;
			if (oneNOV.TicketStatusID !== currentTSO) {
				oneNOV.TicketStatusID = currentTSO;
				const currUser: string = loggedInUser();
				let bRslt: boolean = false;
				bRslt = await allServices.PTTSiudServices.iudNOV(oneNOV, currUser);
				if (bRslt) {
					alert("NOV " + oneNOV.ECBSummonsNumber + " ticket status changed to: " + oneNOV.TicketStatusID)
					// redraw the TSO list
					setAllNOVsHaveThisTSOinCommonState(0); // artificial to redraw TSO list and radio buttons
					setAllNOVsHaveThisTSOinCommonState(currentTSO); // assume cbIudNOVs only called when revising the TSO of the listed NOV#s
				}
				else {
					alert("NOV " + oneNOV.ECBSummonsNumber + " still has ticket status: " + response.data.TicketStatusID)
				}
			}
			else {
				alert("PTTS20 found " + (!response.data.ECBSummonsNumber ? "unknown NOV#" : response.data.ECBSummonsNumber) + " already has a ticket status of " + currentTSO);
            }

		}
		catch (error) {
			alert("Program could not update " + (!response.data.ECBSummonsNumber ? "unknown NOV#" : response.data.ECBSummonsNumber) + " (" + error.toString() + ")");
        }

	}

	const cbTSO = (responseTSO: any) => {
		let rBoolean: boolean = false;
		if (!responseTSO) {
			rBoolean = false;
		}
		else {
			const rdataTSO: tDS21_TicketStatus[] = responseTSO.data;
			if (!rdataTSO) {
				rBoolean = false;
			}
			else {
				let ind: number = 0;
				for (ind = 0; ind < rdataTSO.length; ind++) { rdataTSO[ind].Id = ind + 1; }
				const rdataTSOsorted: tDS21_TicketStatus[] = rdataTSO.sort(function (a, b) {
					var nameA = !a.ID;
					var nameB = !b.ID;
					if (nameA < nameB) {
						return -1;
					}
					if (nameA > nameB) {
						return 1;
					}
					// names must be equal
					return 0;
				}); // ASC
				setdsTSOs(rdataTSOsorted);
				rBoolean = true;
			}
		}
		if (rBoolean) {
			setLoadingTSO(!rBoolean);
			// setReadyToDraw(rBoolean);
		};
	}

	function addNewRowWithTicketStatus(x: tBulkUpdateRow) {
		if (x.NOVnumber.length == 9) {
			allServices.PTTSgetServices.oneNOVwithCallBack(x.NOVnumber, cbAddTktStat, er)
		}
		else {
			console.log("addNewRowWithTicketStatus with short summons number: ", x);
        }
	}

	const cbAddTktStat = (response: any) => {
		let rdata: tOneNOV = response.data;
		setIsValidNOVnumberState(true);

		const allRows: tBulkUpdateRow[] = arr_20BulkUpdateTheseNOVs; 
		const indRow: number = allRows.findIndex((aRow: tBulkUpdateRow) => aRow.NOVnumber === rdata.ECBSummonsNumber);
		if (indRow > -1) {
			allRows[indRow].NOVnumberIsValid = true;
			allRows[indRow].TicketStatusID = rdata.TicketStatusID;
			setLoading(true);
			arr_20BulkUpdateTheseNOVs_ADD(allRows[indRow]);
		}
		else {
			alert("cbAddTktStat -- Cannot find NOV# " + rdata.ECBSummonsNumber + " in the list of entered tickets.")
		}
	}

	function arr_20BulkUpdateTheseNOVs_UPT(x: tBulkUpdateRow) {
		let newST: tBulkUpdateRow[] = [];
		const matchesExisting: number =
			arr_20BulkUpdateTheseNOVs.findIndex((aRow: tBulkUpdateRow) => aRow.Id === x.Id);
		if (matchesExisting > -1) {
			newST = arr_20BulkUpdateTheseNOVs.filter((r: tBulkUpdateRow) => r.Id !== x.Id); // removes data entry row;
			newST.push(x);
			setLoading(false);
			// setReadyToDraw(true);
			newST = newST.sort((a, b) => a.Id - b.Id)
			set_20BulkUpdateTheseNOVs(newST);
		}
	}

	async function arr_20BulkUpdateTheseNOVs_CHK(x: tBulkUpdateRow) {
		const tryThisRange: UsedTicketRange = ( // only intended for the data entry row[0] NOV# (or partial)
			{ // other rows listed can only have valid NOV#
				ECBSummonsNumberFrom: x.NOVnumber,
				ECBSummonsNumberTo: x.NOVnumber,
				DSform: "IsGoodNOVnumber",
				ReceiptNumber: "",
				NOVnumberBeingConsidered: x.NOVnumber,
			});
		
		await allServices.PTTSgetServices.refUsedTicketNumbersWithCallback(tryThisRange, cb, er);
    }

	function arr_20BulkUpdateTheseNOVs_ADD(x: tBulkUpdateRow) {
		let newST: tBulkUpdateRow[] = [];
		const matchesExisting: number =
			arr_20BulkUpdateTheseNOVs.findIndex((aRow: tBulkUpdateRow) => aRow.Id === x.Id);
		if (matchesExisting > 0) {
			newST = arr_20BulkUpdateTheseNOVs.filter((r: tBulkUpdateRow) => r.NOVnumber !== x.NOVnumber); // removes any duplicates;
		}
		else {
			newST = arr_20BulkUpdateTheseNOVs.filter((r: tBulkUpdateRow) => r)
        }
		if (!x.NOVnumberIsValid || x.NOVnumber.length < 9) { // edit the data entry row
			newST[0].NOVnumber = x.NOVnumber;
			newST[0].NOVnumberIsValid = false;
			newST[0].Message = "too small"; // selection made from the dropdown list
			newST[0].TicketStatusID = -14;
			if (newST[0].NOVnumber .substr(0, 1) > "3") {
				newST[0].Message = "first digit of NOV# is 1 or 2"
			}
		}
		else {
			let lclLEN: number = newST.push(x); // add valid NOV to bottom of the list
			newST[0] = { // overwrite the data entry row[0]
				Id: 1,
				NOVnumber: "",
				Message: "",
				NOVnumberIsValid: false,
				TicketStatusID: -10,
			};
        }
		const newSTlgth: number = newST.length;
		for (let i: number = 0; i < newSTlgth; i++) {
			newST[i].Id = i + 1
			if (i > 0 && newST[i].NOVnumber.length === 9) { newST[i].NOVnumberIsValid = true }
		}
		set_20BulkUpdateTheseNOVs(newST);
	};

	function arr_20BulkUpdateTheseNOVs_RMV(x: tBulkUpdateRow) {
		let newST: tBulkUpdateRow[] = [];
		newST = arr_20BulkUpdateTheseNOVs.filter((r: tBulkUpdateRow) => r.NOVnumber !== x.NOVnumber); // removes any duplicates
		//newST.unshift({ // workaround for push object into array is by ref in javascript; need a copy
		//	Id: dummyBulkUpdateThisNOV.Id,
		//	NOVnumber: dummyBulkUpdateThisNOV.NOVnumber,
		//	Message: dummyBulkUpdateThisNOV.Message,
		//	NOVnumberIsValid: dummyBulkUpdateThisNOV.NOVnumberIsValid,
		//	TicketStatusID: dummyBulkUpdateThisNOV.TicketStatusID
		//}); // initialize first NOV# value in the list
		const newSTlgth: number = newST.length;
		for (let i: number = 0; i < newSTlgth; i++) {
			newST[i].Id = i + 1
		}
		set_20BulkUpdateTheseNOVs(newST);
	};

	const handleOptionChangeTL = (event: any) => {
		let newSelectionTL: number = -1;
		if (PTTSfunctions.Commonfunctions2.isNotDefined(event.target.value)) {
			eventCurrentTarget = event.target;
			alert("OptionChange event object is marked 'notDefined'");
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(eventCurrentTarget);
		}
		else {
			newSelectionTL = Number.parseInt(event.target.value);
			setCurrentTSO(newSelectionTL);
		}
		return newSelectionTL;
	};

	React.useEffect(() => {

		// if (!IsValidNOVnumberState && NOVnumberState !== "" && contentparent.ST_20BulkUpdateTheseNOVs[0].NOVnumber !== NOVnumberState) {
		if (!IsValidNOVnumberState && NOVnumberState !== "" && arr_20BulkUpdateTheseNOVs[0].NOVnumber !== NOVnumberState) {
			// this allows user to type in each digit until the entry is same as one of the existing NOV#s
			const addRow: tBulkUpdateRow[] = [{ // workaround for push object into array is by ref in javascript; need a copy
				Id: dummyBulkUpdateThisNOV.Id,
				NOVnumber: NOVnumberState,
				Message: dummyBulkUpdateThisNOV.Message,
				NOVnumberIsValid: !IsValidNOVnumberState,
				TicketStatusID: dummyBulkUpdateThisNOV.TicketStatusID
			}];
			// setLoading(!allActions.DS21acts.iudBulkUpdateNOVlist(addRow[0], dispatch, BulkUpdateTheseNOVs_ACTION.UPT));
			if (NOVnumberState.length === 9) { arr_20BulkUpdateTheseNOVs_CHK(addRow[0]); } // find out if NOV# is valid
			else { arr_20BulkUpdateTheseNOVs_UPT(addRow[0]); } // update the data entry row[0]
			
		}
		else if (IsValidNOVnumberState && NOVnumberState !== "") {
			// find NOV# in ST_20BulkUpdateTheseNOVs
			const allRows: tBulkUpdateRow[] = arr_20BulkUpdateTheseNOVs; // contentparent.ST_20BulkUpdateTheseNOVs;
			const indRow: number = allRows.findIndex((aRow: tBulkUpdateRow) => aRow.NOVnumber === NOVnumberState);
			if (indRow > -1) {
				const aRow: tBulkUpdateRow = allRows[indRow];
				// determine its TicketStatusID
				if (aRow.TicketStatusID < 0) {
					// update ST_20BulkUpdateTheseNOVs
					// setReadyToDraw(false);
					allServices.PTTSgetServices.oneNOVwithCallBack(aRow.NOVnumber, cbOneNOVupt, er)
				}
				else {
					setLoading(false);
				}
			}
			else {
				setLoading(false);
			}
		}
		else {
			setLoading(false);
		}

	}, [NOVnumberState, IsValidNOVnumberState]);

	React.useEffect(() => {
		// ticket status options
		if (isLoadingTSO && arr_20BulkUpdateTheseNOVs.length > 1) {
			setRenderTSO({
				renderThis: [(
					<div key="2"> working ...</div >
				)]
			});
		}
		// else if (!contentparent || contentparent.ST_20BulkUpdateTheseNOVs.length < 2) {
		else if (arr_20BulkUpdateTheseNOVs.length < 2) {
			setRenderTSO({
				renderThis: [(
					<div key="3">Add a valid NOV# to the list above.</div>
				)]
			});
		}
		// else if (contentparent.ST_20BulkUpdateTheseNOVs.length === 2) {
		else if (arr_20BulkUpdateTheseNOVs.length === 2) {
			console.log("renderMenuTSO AllNOVsHaveThisTSOinCommonState", AllNOVsHaveThisTSOinCommonState);
			console.log("renderMenuTSO currentTSO", currentTSO);
			setRenderTSO({ // ticket status options
				renderThis: [(
					<React.Fragment key="keyRF" >
						<FormControl component="fieldset" key="keyFC" >
							<FormLabel component="legend" key="keyFL">Ticket Location</FormLabel>
							<RadioGroup aria-label="Ticket Location" name="optTicketLocation" key="keyRG"
								value={arr_20BulkUpdateTheseNOVs[1].TicketStatusID} onChange={handleOptionChangeTL}>
								{renderMenuTSO(arr_20BulkUpdateTheseNOVs[1].TicketStatusID, isLoadingTSO,
									dsTSOs, dsAttributesOfOneNOV)}
							</RadioGroup>
						</FormControl>
					</React.Fragment>
				)]
			});
		}
		else {
			console.log("2nd renderMenuTSO AllNOVsHaveThisTSOinCommonState", AllNOVsHaveThisTSOinCommonState);
			console.log("2nd renderMenuTSO currentTSO", currentTSO);
			setRenderTSO({ // ticket status options
				renderThis: [(
					<React.Fragment key="keyRF" >
						<FormControl component="fieldset" key="keyFC" >
							<FormLabel component="legend" key="keyFL">Ticket Location</FormLabel>
							<RadioGroup aria-label="Ticket Location" name="optTicketLocation" key="keyRG"
								value={AllNOVsHaveThisTSOinCommonState} onChange={handleOptionChangeTL}>
								{renderMenuTSO(AllNOVsHaveThisTSOinCommonState, isLoadingTSO,
									dsTSOs, dsAttributesOfOneNOV)}
							</RadioGroup>
						</FormControl>
					</React.Fragment>
				)]
			});
		}
	}, [AllNOVsHaveThisTSOinCommonState, isLoadingTSO]);

	React.useEffect(() => { if (!isReadyToDraw && mountedRef.current) { mountedRef.current = false; } }, [isReadyToDraw]);

	React.useEffect(() => {
		const bTSOs: boolean = dsTSOs.length < 2;
		setLoadingTSO(bTSOs) // true means the dataset isn't finished loading; expected a half dozen or more items in this dataset
	}, [dsTSOs])

	React.useEffect(() => {
		if (isLoadingTSO || dsTSOs.length < 2) {
			allServices.PTTSgetServices.refTicketStatusOptionsWithCallback(cbTSO, er); // axios always returns an object with a "data" child
		}
	}, [isLoadingTSO])

	React.useEffect(() => {
		if (currentTSO > 0) {
			const lclNOVtoUpdate: tBulkUpdateRow[] = arr_20BulkUpdateTheseNOVs; // contentparent.ST_20BulkUpdateTheseNOVs;
			lclNOVtoUpdate.forEach(async (aRow: tBulkUpdateRow) => {
				if (aRow.NOVnumber > "" && aRow.TicketStatusID !== currentTSO) {
					await allServices.PTTSgetServices.oneNOVwithCallBack(aRow.NOVnumber, cbIudNOVs, er)
				}
			})
		}

	}, [currentTSO]);

	React.useEffect(() => {
		setLoadingTSO(true);
	}, []);

	React.useEffect(() => {
		setLoadingTSO(false);
		// setReadyToDraw(true);
	}, [renderTSO]);

	React.useEffect(() => {

		if (!arr_20BulkUpdateTheseNOVs) {
			setAllNOVsHaveThisTSOinCommonState(0);
		}
		else if (arr_20BulkUpdateTheseNOVs.length < 2) // assumes must have at least one dummy row
		{
			setAllNOVsHaveThisTSOinCommonState(!arr_20BulkUpdateTheseNOVs[0] ? 0 : arr_20BulkUpdateTheseNOVs[0].TicketStatusID);
		}
		else if (arr_20BulkUpdateTheseNOVs.length === 2) {
			if (arr_20BulkUpdateTheseNOVs[1].TicketStatusID !== currentTSO
				&& arr_20BulkUpdateTheseNOVs[1].TicketStatusID > 0) {
				setAllNOVsHaveThisTSOinCommonState(arr_20BulkUpdateTheseNOVs[1].TicketStatusID);
			}
			else {
				// TicketStatus hasn't been determined yet
				setNOVnumberState(arr_20BulkUpdateTheseNOVs[1].NOVnumber) // artificial to trigger useEffect
			}
		}
		else {

			let bSame: boolean = false;
			let arrTSO: number[] = [];
			for (let i: number = 1; i < arr_20BulkUpdateTheseNOVs.length; i++) {
				arrTSO.push(arr_20BulkUpdateTheseNOVs[i].TicketStatusID)
			}
			bSame = PTTSfunctions.Commonfunctions2.arrAllEqual(arrTSO);
			if (bSame || arr_20BulkUpdateTheseNOVs.length === 2) {
				setAllNOVsHaveThisTSOinCommonState(arrTSO[0])
			}
			else {
				setAllNOVsHaveThisTSOinCommonState(0)
			}
		}
		allActions.DS21acts.rplBulkUpdateEveryRow(arr_20BulkUpdateTheseNOVs, dispatch);
		setLoading(false);
	}, [arr_20BulkUpdateTheseNOVs]);

	function loggedInUser(): string {
		return (localStorage.getItem('userid') + "").toString(); // to avoid possibly null error
		//const j: any = JSON.parse(curUser);
		//return j.data.userPreferences.userName.toString();
	}

	function handleGetNOV(aNOV: tBulkUpdateRow) {
		if (aNOV.NOVnumber.length < 8) { return }
		history.push('/PTTS/NOVdetails/' + aNOV.NOVnumber)
	}

	function fIsValidNOVnumber(aDSform: string, LowHigh: string, TgtVal: number) {
		try {
			if (!TgtVal || TgtVal < 10000000) {
				return;
			}
			else {
				const searchECBsummonsNumber: string =
					(TgtVal < 100000000
						? PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(TgtVal)
						: TgtVal).toString();
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Find a ticket range with NOV ", searchECBsummonsNumber);

				const tryThisRange: UsedTicketRange = (
					{
						ECBSummonsNumberFrom: LowHigh === "Low"
							? TgtVal.toString()
							: searchECBsummonsNumber,
						ECBSummonsNumberTo: LowHigh === "High"
							? TgtVal.toString()
							: searchECBsummonsNumber,
						DSform: aDSform,
						ReceiptNumber: "",
					}
				);
				(async () => {
					if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("call refUsedTicketNumbersWithCallback");
					await allServices.PTTSgetServices.refUsedTicketNumbersWithCallback(tryThisRange, cb, er)
				})();
			}

		}
		catch (error) {
			alert("BulkUpdateLocation / Table: " + error);
			throw (error); // false;
		}

		// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber after ASYNC", pIsValidNOVnumber);

	};

	function renderMenuTSO(currentTSO: number, isLoadingTSO: boolean, 
		dsTSOs: tDS21_TicketStatus[], dsAttributesOfOneNOV: tOneNOV) {
		if (currentTSO > 0) {
			return PTTSfunctions.Commonfunctions2.fTSOjsxElement(currentTSO,
				false, false,
				dsTSOs, dsAttributesOfOneNOV);

		}
		else {
			return dsTSOs.map((oneChoice: tDS21_TicketStatus) => {
				return (
					<FormControlLabel
						disabled={false}
						key={oneChoice.ID}
						value={oneChoice.ID}
						control={<Radio />}
						label={oneChoice.Description}
					/>
				);
			})
		}
	};

	const handleOnChange = async (e: any) => {
		e.preventDefault();
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handleOnChange");
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e);

		switch (e.target.name) {
			case "fldNOVnumberFromTableFirstRow":
				let dummy3n: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				// less than 8 digits, not-a-number, or nine or more digits starting with 3 (Sanitation only prefixes 1 or 2)
				if (dummy3n === NaN) {
					setIsValidNOVnumberState(false);
					alert("NOV number only has digits. (" + e.target.value + ")");
					break;
				}

				if (dummy3n > 299999999) {
					setIsValidNOVnumberState(false);
					alert("Expected first summons number is supposed to start with 1 or 2 and be nine-digits.  You entered a number that is too big.")
					break;
				}
				if (dummy3n < 99999999) {
					setIsValidNOVnumberState(false);
					setNOVnumberState(dummy3n.toString()); // _ADD takes care of validation of short entries
				}
				//            12345678
				if (dummy3n > 99999999) {
					dummy3n = Math.trunc(dummy3n / 10);
					const dummy3: string = PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(dummy3n);
					const matchesExisting: number =
						arr_20BulkUpdateTheseNOVs.findIndex((aRow: tBulkUpdateRow) => aRow.NOVnumber === dummy3)
					if (e.target.value.substr(0, 8) === dummy3.substr(0, 8)
						&& e.target.value.substr(8, 1) !== dummy3.substr(8, 1)) {
						alert("You entered the wrong check-sum (last digit).  Did you intend NOV# " + dummy3 + "?");
						arr_20BulkUpdateTheseNOVs[0].Message = dummy3 + "?";
						setIsValidNOVnumberState(false);
						arr_20BulkUpdateTheseNOVs_UPT(arr_20BulkUpdateTheseNOVs[0]);
						break;
					}
					else if (matchesExisting > 0 || dummy3 === NOVnumberState) {
						alert("You are entering the same NOV# as before, see item # " + (matchesExisting + 1).toString());
						setLoading(false);
						break;
					}
					else {
						arr_20BulkUpdateTheseNOVs[0].NOVnumber = dummy3;
						await arr_20BulkUpdateTheseNOVs_CHK(arr_20BulkUpdateTheseNOVs[0]);
					}

				}
				break;
			case "fldNOVnumber":
				let dummy2n: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				if (isNaN(dummy2n)) {
					alert("NOV number is NaN.");
					break;
				}
				if (dummy2n > 299999999) {
					alert("Expected first summons number is supposed to start with 1 or 2 and be nine-digits.  You entered a number that is too big.")
					break;
				} // less than 8 digits, not-a-number, or nine or more digits starting with 3 (Sanitation only prefixes 1 or 2)
				//            12345678
				setNOVnumberState(dummy2n.toString());
				if (dummy2n > 99999999) {
					dummy2n = Math.trunc(dummy2n / 10);
					//const msg1: string = "check NOV number" + dummy2n.toString();
					//alert(msg1);
					const dummy2: string = PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(dummy2n);
					dummy2n = parseInt(dummy2);
					fIsValidNOVnumber("IsGoodNOVnumber", "Low", dummy2n);
				}
				break;
			default:
				// code block
				const msg: string = "No onChange for " + e.target.name;
				alert(msg);
		}
	}

	const handleAddNOV = (typedInNOV: string) => {

	}

	const handleRemoveNOV = (chosenNOV: string) => {
		setLoading(true);
		try {
			const lcllstOfRows: tBulkUpdateRow[] = arr_20BulkUpdateTheseNOVs; // contentparent.ST_20BulkUpdateTheseNOVs;
			// find the NOV# in lstOfRows
			const oneIndex: number = lcllstOfRows.findIndex((aRow: tBulkUpdateRow) => aRow.NOVnumber === chosenNOV);
			const aRow: tBulkUpdateRow = lcllstOfRows[oneIndex];
			arr_20BulkUpdateTheseNOVs_RMV(aRow);// remove the NOV#

		}
		catch (error) {
			alert(error.toString());
		}

	};

	const renderEmpty = () => {
		return (<div>&nbsp;</div>);
	}

	const submitNOV = async (data: FormData, e: any) => {
		e.target.reset(); // reset after form submit
	}

	const submitForm = async (data: FormData, e: any) => {
		switch (e.target.name) {
			case "Clear All":
				const newFirstRow: tBulkUpdateRow = {
					Id: dummyBulkUpdateThisNOV.Id,
					NOVnumber: dummyBulkUpdateThisNOV.NOVnumber,
					Message: dummyBulkUpdateThisNOV.Message, // selection made from the dropdown list
					NOVnumberIsValid: dummyBulkUpdateThisNOV.NOVnumberIsValid,
					TicketStatusID: dummyBulkUpdateThisNOV.TicketStatusID,
				};
				setLoading(!allActions.DS21acts.iudBulkUpdateNOVlist(newFirstRow, dispatch, BulkUpdateNOV_ACTION.RST));
				// setLoadingNOVftr(!allActions.DS21acts.iudBulkUpdateNOVlist(newFirstRow, dispatch, BulkUpdateTheseNOVs_ACTION.RST));
				break;
			default:
				// code block
				const msg: string = "No onChange for " + e.target.name;
				alert(msg);
		}
		// e.target.reset(); // reset after form submit
	}

	const renderFirstTableRow = (arrN: tBulkUpdateRow[]) => {
		console.log("arrN is: ", arrN);
		const n: tBulkUpdateRow = arrN[0];
		return (
			<TableRow key={("000000000" + n.Id.toString()).substr(-8) + "row"}>
				<TableCell size="small" key={("000000000" + n.Id.toString()).substr(-8) + "ID"} align="center">{`${n.Id}`}</TableCell>
				<TableCell size="medium" key={("000000000" + n.Id.toString()).substr(-8) + "TKTNO"} align="left" onClick={() => handleGetNOV(n.NOVnumberIsValid ? n : dummyBulkUpdateThisNOV)}>
					<FormControl id={("000000000" + n.Id.toString()).substr(-8) + "FCid"}>
						{/* https://stackoverflow.com/questions/65395896/there-are-multiple-inputbase-components-inside-a-formcontrol */}
						<Input className={classes.styleforticketrangetext}
							type="number"
							id="idNOVnumberFromTableFirstRow"
							onChange={(e) => handleOnChange(e.target.value)}
							placeholder="9-digit ECB Summons Number"
							name="fldNOVnumberFromTableFirstRow"
							value={n.NOVnumber}
						// defaultValue versus value --> value lets me change characters in the currently active Input control
						/>
						<div>&nbsp;</div>
						<ErrorMessage
							errors={errors}
							name="fldNOVnumberFromTableFirstRow"
							as={<ErrorMessageContainer />}
						/>
					</FormControl>
				</TableCell>
				<TableCell size="medium" key={("000000000" + n.Id.toString()).substr(-8) + "MESSAGE"} align="left">{n.Message}</TableCell>
				{(n.NOVnumber.length === 0)
					? <TableCell>&larr; Type or paste a NOV#</TableCell>
					: (n.NOVnumberIsValid)
						?
						<TableCell>
							{/* &larr; Type a NOV# */}
							<button onClick={() => handleAddNOV(n.NOVnumber)}>Add</button>
						</TableCell>
						:
						<TableCell size="small" key={("000000000" + n.Id.toString()).substr(-8) + "REMOVE"} align="left">
							<button onClick={() => handleRemoveNOV(n.NOVnumber)}>Remove</button>
						</TableCell>
				}
			</TableRow>
		)
	}

	const renderTableRow = (n: tBulkUpdateRow) => {
		console.log("n is: ", n);
		if (n.Id !== 1) { // NOV# already entered and verified
			return (
				<TableRow key={("000000000" + n.Id.toString()).substr(-8) + "row"}>
					<TableCell size="small" key={("000000000" + n.Id.toString()).substr(-8) + "ID"} align="center">{`${n.Id}`}</TableCell>
					<TableCell size="medium" key={("000000000" + n.Id.toString()).substr(-8) + "TKTNO"} align="left" onClick={() => handleGetNOV(n.NOVnumberIsValid ? n : dummyBulkUpdateThisNOV)}>
						<FormControl id={("000000000" + n.Id.toString()).substr(-8) + "FCid"}>
							{/* https://stackoverflow.com/questions/65395896/there-are-multiple-inputbase-components-inside-a-formcontrol */}
							<Input className={classes.styleforticketrangetext}
								type="number"
								id="idNOVnumberEnteredAndVerified"
								name="fldNOVnumberEnteredAndVerified"
								value={n.NOVnumber}
							// defaultValue versus value --> value lets me change characters in the currently active Input control
							/>
						</FormControl>
					</TableCell>
					<TableCell size="medium" key={("000000000" + n.Id.toString()).substr(-8) + "MESSAGE"} align="left">{n.Message}</TableCell>
					<TableCell size="small" key={("000000000" + n.Id.toString()).substr(-8) + "REMOVE"} align="left">
						<button name="btnNOVnumberEnteredAndVerified"
							onClick={() => handleRemoveNOV(n.NOVnumber)}>Remove</button>
					</TableCell>
				</TableRow>
			)
		}
		else { // NOV# being enetered in the first row
			return (
				<TableRow key={("000000000" + n.Id.toString()).substr(-8) + "row"}>
					<TableCell size="small" key={("000000000" + n.Id.toString()).substr(-8) + "ID"} align="center">{`${n.Id}`}</TableCell>
					<TableCell size="medium" key={("000000000" + n.Id.toString()).substr(-8) + "TKTNO"} align="left" onClick={() => handleGetNOV(n.NOVnumberIsValid ? n : dummyBulkUpdateThisNOV)}>
						<FormControl id={("000000000" + n.Id.toString()).substr(-8) + "FCid"}>
							{/* https://stackoverflow.com/questions/65395896/there-are-multiple-inputbase-components-inside-a-formcontrol */}
							<Input autoFocus className={classes.styleforticketrangetext}
								type="number"
								id="idNOVnumberFromTable"
								onChange={(e) => handleOnChange(e)}
								placeholder="9-digit ECB Summons Number"
								name="fldNOVnumberFromTableFirstRow"
								value={n.NOVnumber}
							// defaultValue versus value --> value lets me change characters in the currently active Input control
							/>
							<div>&nbsp;</div>
							<ErrorMessage
								errors={errors}
								name="fldNOVnumberFromTableFirstRow"
								as={<ErrorMessageContainer />}
							/>
						</FormControl>
					</TableCell>
					<TableCell size="medium" key={("000000000" + n.Id.toString()).substr(-8) + "MESSAGE"} align="left">{n.Message}</TableCell>
					{(n.NOVnumber.length === 0)
						? <TableCell>&larr; Type or paste a NOV#</TableCell>
						: (n.NOVnumberIsValid)
							?
							<TableCell>
								{/* &larr; Type a NOV# */}
								<button onClick={() => setNOVnumberState(n.NOVnumber)}>Add</button>
							</TableCell>
							:
							<TableCell size="small" key={("000000000" + n.Id.toString()).substr(-8) + "REMOVE"} align="left">
								<button onClick={() => handleRemoveNOV(n.NOVnumber)}>Remove</button>
							</TableCell>
					}
				</TableRow>
			)
		}

	}

	if (isLoading) {
		return (<Loader />);
	}
	else {
		return (
			<Grid container>
				<Grid item xs={12} component={Paper} className={classes.paper}>
					<FormControl  >
						<ErrorSummary errors={errors} />

						<InputLabel htmlFor="idNOVnumber">{SearchForNOVnumberState.length < 9
							? <div>Search for this ECB Summons Number: {SearchForNOVnumberState} </div>
							: IsValidNOVnumberState
								? < div > {SearchForNOVnumberState} is valid; click 'Go to {SearchForNOVnumberState}'.</div>
								: < div > {SearchForNOVnumberState} is not on any existing DS-1984.</div>}
						</InputLabel>
						<Input className={classes.styleforticketrangetext}
							type="number"
							id="idNOVnumber"
							onChange={(e) => handleOnChange(e)}
							placeholder="9-digit ECB Summons Number"
							name="fldNOVnumber"
							ref={register({
								required: { value: true, message: "PTTS expects the first NOV number (nine digits)." },
								min: {
									value: 100000000,
									message: "Too small. Individual ECB Summons are nine digit numbers starting with 1 or 2."
								},
								max: {
									value: 299999999,
									message: "Too big.  Individual ECB Summons are nine digit numbers starting with 1 or 2."
								},
							})}
							value={SearchForNOVnumberState}
						// defaultValue versus value --> value lets me change characters in the currently active control textbox
						/>
						<div>&nbsp;</div>
						<ErrorMessage
							errors={errors}
							name="fldNOVnumber"
							as={<ErrorMessageContainer />}
						/>
						{IsValidNOVnumberState
							?
							<button name="btnNOVnumber" type="submit" onClick={(e) => { e.preventDefault(); history.push('/PTTS/NOVdetails/' + SearchForNOVnumberState); }}>Go to {SearchForNOVnumberState}</button>
							:
							<div>&nbsp;</div>
						}
						<Divider />
					</FormControl>
				</Grid>
				<Grid item xs={12} component={Paper} className={classes.paper}>
					<FormControl  >
						<ErrorSummary errors={errors} />
						<Table className={classes.table} aria-label="a dense table" >
							<TableHead>
								<TableRow>
									<TableCell size="small" align="left">Description</TableCell>
									<TableCell size="medium" align="left">Ticket No</TableCell>
									<TableCell size="medium" align="right">Message</TableCell>
									<TableCell size="small" align="left">&nbsp;</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{
									isLoading //  || !dsListPayCodeFilter || dsListPayCodeFilter === undefined
										|| !arr_20BulkUpdateTheseNOVs
										? <TableRow>
											<TableCell size="small">waiting for redux store content ...
											</TableCell>
										</TableRow>
										: !arr_20BulkUpdateTheseNOVs.length
											? renderFirstTableRow(arr_20BulkUpdateTheseNOVs)
											: arr_20BulkUpdateTheseNOVs.map((n: tBulkUpdateRow) => {
												console.log("render row: " + n.Id + " of " + arr_20BulkUpdateTheseNOVs.length);
												return (
													renderTableRow(n)
												)
											})
								}
							</TableBody>
						</Table>
					</FormControl>
				</Grid>
				{isLoadingTSO ?
					<div>Determining whether NOV#s have a common location.<Loader /></div>
						: <Grid container>
							<Grid item xs={12} component={Paper} className={classes.paper}>
								<FormControl onSubmit={handleSubmit(submitForm)} >
									<Table>
										<TableHead>
											<TableRow>
												<TableCell size="small" align="right">&nbsp;</TableCell>
												<TableCell size="small" align="center">NOV# above set to:</TableCell>
												<TableCell size="medium" align="center">Message</TableCell>
												<TableCell size="small" align="right">&nbsp;</TableCell>
												<TableCell size="small" align="left">&nbsp;</TableCell>
											</TableRow>
										</TableHead>
										<TableBody>
											<TableRow>
												<TableCell size="small" align="right">Select one:</TableCell>
												<TableCell size="small" align="center">
													{renderTSO.renderThis}
												</TableCell>
												<TableCell size="small" align="left">
													<StyledButton className={classes.button}
														id="btnClearAll_BUL"
														variant="outlined"
														color="primary" type="submit"
														onClick={handleSubmit(submitForm)} > Clear All</StyledButton>
												</TableCell>
											</TableRow>
										</TableBody>
									</Table>
								</FormControl>
							</Grid>
						</Grid>
				}

			</Grid>
		)
	}
}

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5,
		marginTop: 0
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);


const useStyles = makeStyles({
	paper: {
		width: "100%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 0,
		marginTop: 10,
		overflowX: "visible",
	},
	table: {
		width: "100%",
		margingTop: 80,
		minWidth: 1000,
		display: "inline-block",
	},
	cell: {
		minWidth: 100,
		maxWidth: 220,
	},
	button: {
		marginTop: 20,
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 10,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 14,
		fontWeight: "normal",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 550,
		minWidth: 300,
	}
});
