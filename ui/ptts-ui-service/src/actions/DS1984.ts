﻿import { isNullOrUndefined } from "util";
import { PTTSfunctions } from "../components/basicComponents";
import { DS1984, DS1984Action, DS1984Actions } from "../model";

export type KnownActionDS1984 = DS1984Action;

function fAddDS1984(oDS1984: DS1984): DS1984Action {
	return {
		type: DS1984Actions.ADD_DS1984,
		payload: oDS1984,
	};
}

function fUptDS1984(oDS1984: DS1984): DS1984Action {
	return {
		type: DS1984Actions.UPDATE_DS1984,
		payload: oDS1984,
	};
}


// Async Function expample with redux-thunk
function fCompleteDS1984(DS1984Id: number, dispatchRef: any) {
	// here you could do API eg

	return dispatchRef({ type: DS1984Actions.COMPLETE_DS1984, payload: DS1984Id });
}

function fUncompleteDS1984(DS1984Id: number): DS1984Action {
	return {
		type: DS1984Actions.REVERT2INCOMPLETE_DS1984,
		payload: DS1984Id,
	};
}

function fDS1984deleteTodo(DS1984Id: number): DS1984Action {
	return {
		type: DS1984Actions.DELETE_DS1984,
		payload: DS1984Id,
	};
}


function DS1984sIAWpayCodesExtractedFromADGroupMembership(dsList: DS1984[], content: any): DS1984[] {
	const dsListFiltered: DS1984[] = [];
	const PAYC: any = PTTSfunctions.ADfunctions.PTTSuserIsMemberOf(content.giantReducer.ST_ADGROUPS);

	if (isNullOrUndefined(PAYC[0])) {
			return dsList.slice(0, 29)
	}
	else {
		/*
		 * ST_ADGROUPS: {
				userName: 'xyz',
				userMemberOfADgroupList: 'xyz'
			},
		 * */
		const PAYCasc: string[] = PAYC.sort(function (a: string, b: string) {
			var nameA = !a ? "" : a.toUpperCase(); // ignore upper and lowercase
			var nameB = !b ? "" : b.toUpperCase(); // ignore upper and lowercase
			if (nameA < nameB) {
				return -1;
			}
			if (nameA > nameB) {
				return 1;
			}
			// names must be equal
			return 0;
		})
		const PAYCuniq: string[] = [];
		let lastPAYC: string = "lastPAYC";
		let currPAYC: string = "";
		for (var iIndex = 0; iIndex < PAYCasc.length; iIndex++) {
			currPAYC = PAYCasc[iIndex];
			currPAYC = isNullOrUndefined(currPAYC) ? "" : currPAYC.substr(0, 4);
			if (lastPAYC !== currPAYC && lastPAYC.length > 0 && currPAYC.length > 0) {
				PAYCuniq.push(currPAYC);
				lastPAYC = currPAYC;
			};
		}

		for (var iIndex = 0; iIndex < PAYCuniq.length; iIndex++) {
			for (var jIndex = 0; jIndex < dsList.length; jIndex++) {
				if (dsList[jIndex].ReceiptNumber.indexOf(PAYCuniq[iIndex]) > -1) {
					dsListFiltered.push(dsList[jIndex]);
				}
			};
		}

	}

	return dsListFiltered;
}

export const allActions = {
	addDS1984: fAddDS1984,
	uptDS1984: fUptDS1984,
	completeDS1984: fCompleteDS1984,
	uncompleteDS1984: fUncompleteDS1984,
	DS1984deleteTodo: fDS1984deleteTodo,
	DS1984sIAWpayCodesExtractedFromADGroupMembership,

}