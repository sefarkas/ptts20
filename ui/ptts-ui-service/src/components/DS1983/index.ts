﻿export { DS1983Dialog } from "./Dialog";
export { DS1983Table } from "./Table";
export { NewDS1983Form as DS1983Form } from "./BlankTemplate";
export { ExistingDS1983Form } from "./ExistingForm";

export interface DS1983basics {
    DS1983receiptNumber: string,
    DS1983fromTktNumber: number,
    DS1983toTktNumber: number
};