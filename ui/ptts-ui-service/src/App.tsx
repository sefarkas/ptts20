// prettier-ignore
import { AppBar, Badge, Divider, Drawer as DrawerMui, Hidden, IconButton, List, ListItem, ListItemIcon, ListItemText, Toolbar, useMediaQuery } from "@material-ui/core";
import { Theme, useTheme } from "@material-ui/core/styles";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import FormatListNumberedIcon from "@material-ui/icons/FormatListNumbered";
import HomeIcon from "@material-ui/icons/Home";
import MenuIcon from "@material-ui/icons/Menu";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { Fragment, useContext, useEffect, useState } from "react";
import { ReactReduxContext, useDispatch, useSelector } from "react-redux";
import { Route, Router } from "react-router-dom";
import { allActions } from './actions';
import { withRoot } from "./AppWithRoot";
import { PTTSfunctions } from "./components/basicComponents";
import { DS1983Form, ExistingDS1983Form } from "./components/DS1983";
import { DS1984Form, ExistingDS1984Form } from "./components/DS1984";
import { DS1986Form, ExistingDS1986Form } from "./components/DS1986";
import { ECB603Form, ExistingECB603Form } from "./components/ECB603";
import PrivateRoute, { PrivateRouteHQ } from "./components/PrivateRoute";
import { history } from "./configureStore";
import { Complaint, ECB603 } from "./model";
import {
    ComplaintsPage,

    HomePage, LoginPage, LogoutPage, NewComplaintPage, PageForNOVDetails,
    PTTSDS1983, PTTSDS1984, PTTSDS1986, PTTSDS21CardPage, PTTSDS21HDRpage, PTTSDS21HDRsubset,
	PTTSECB603, PTTSHomePage, PTTSNOVcrossReferences, PageForDS1704A, PTTSbulkUpdatePage
} from "./pages";
import { tGiantState } from "./reducers";

function useStore() {
	// https://medium.com/@dawchihliou/replacing-react-reduxs-connect-with-react-hooks-1c65e2a6909
	const { store } = useContext(ReactReduxContext)
	const { getState, dispatch, subscribe } = store
	const [storeState, setStoreState] = useState(getState())

	// subscribe only once
	useEffect(() => subscribe(() => {
		setStoreState(getState());
	}), []);

	return [storeState, dispatch]
}

export function useSelectors(...selectors: any[]) {
	// https://medium.com/@dawchihliou/replacing-react-reduxs-connect-with-react-hooks-1c65e2a6909
	const [state] = useStore();
	return selectors.map(selector => selector(state));
}

const Routes = (props: { dispatchRef: any, passedInContent: tGiantState }) =>   {
	const classes = useStyles();
	// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Routes content is:");
	// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props.passedInContent);
	// startup with Logout Page to make sure Application Local Storage is clear
	// order of routes matters, each is checked when you launch history.push
	return (
		<div>
			<div className={classes.content}>
				<Route exact={true} path="/" component={LogoutPage} />
				{/*<Route exact={true} path="/PTTS/login" render={() => <LoginPage {...props.dispatchRef} {...props.passedInContent} />} /> */}
				<Route exact={true} path="/PTTS/login" render={() => <LoginPage />} />
				<Route exact={true} path="/PTTS/logout" render={() => <LogoutPage />} />

				{/*<Route exact={true} path="/PTTS/" render={() => <PTTSHomePage {...props.dispatchRef} {...props.passedInContent} />} />
				<Route exact={true} path="/PTTS/home" render={() => <PTTSHomePage  {...props.dispatchRef} {...props.passedInContent} />} />*/}
				<Route exact={true} path="/PTTS/" render={() => <PTTSHomePage  />} />
				<Route exact={true} path="/PTTS/home" render={() => <PTTSHomePage   />} />

				<PrivateRoute exact={true} path="/ds249/" component={HomePage} />
				<PrivateRoute exact={true} path="/ds249/home" component={HomePage} />
				<PrivateRoute exact={true} path="/ds249/complaints" component={ComplaintsPage} />
				<PrivateRoute exact={true} path="/ds249/newComplaint" component={NewComplaintPage} />
				<PrivateRoute exact={true} path="/PTTS/existingDS1983/:onercpt" component={ExistingDS1983Form} />
				<PrivateRoute exact={true} path="/PTTS/DS1984" component={PTTSDS1984} />
				<PrivateRoute exact={true} path="/PTTS/existingDS1984/:onercpt" component={ExistingDS1984Form} />
				<PrivateRoute exact={true} path="/PTTS/DS1986" component={PTTSDS1986} />
				<PrivateRoute exact={true} path="/PTTS/newDS1986" component={DS1986Form} />
				<PrivateRoute exact={true} path="/PTTS/existingDS1986/:oneid" component={ExistingDS1986Form} />

				<PrivateRoute exact={true} path="/PTTS/BulkUpdateLocation" component={PTTSbulkUpdatePage} />
				<PrivateRoute exact={true} path="/PTTS/DS21HDR" component={PTTSDS21HDRpage} />
				<PrivateRoute exact={true} path="/PTTS/DS21HDR/:DS1984rcpt" component={PTTSDS21HDRsubset} />
				<PrivateRoute exact={true} path="/PTTS/DS21Card/:DS1984rcpt" component={PTTSDS21CardPage} />
				<PrivateRoute exact={true} path="/PTTS/NOVdetails/:aSingleNOVnumber" component={PageForNOVDetails} />
				<PrivateRoute exact={true} path="/PTTS/NOVxref/:oneNOVnumber" component={PTTSNOVcrossReferences} />
				<PrivateRoute exact={true} path="/PTTS/DS1704/:oneNOVnumber" component={PageForDS1704A} />
				<PrivateRoute exact={true} path="/PTTS/DS1704Bulk/:oneDS1984rcpt" component={PageForDS1704A} />

				<PrivateRouteHQ exact={true} path="/PTTS/newECB603" component={ECB603Form} />
				<PrivateRouteHQ exact={true} path="/PTTS/existingECB603/:onercpt" component={ExistingECB603Form} />
				<PrivateRouteHQ exact={true} path="/PTTS/newDS1983" component={DS1983Form} />
				<PrivateRouteHQ exact={true} path="/PTTS/newDS1984" component={DS1984Form} />
				<PrivateRouteHQ exact={true} path="/PTTS/ECB603" component={PTTSECB603} />
				<PrivateRouteHQ exact={true} path="/PTTS/DS1983" component={PTTSDS1983} />
				<PrivateRouteHQ exact={true} path="/PTTS/DS1704A" component={PageForDS1704A} />
				</div>
		</div>
	);

}

const Drawer = (props: { dispatchRef: any, content: tGiantState} ) => {
	const classes = useStyles();
	const aList = props.content.complaintList; 
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("App Drawer");
	return (
		<div>
			<div className={classes.drawerHeader} />
			<Divider />
			<List>
				<ListItem button onClick={() => history.push("/PTTS/home")}>
					<ListItemIcon>
						<HomeCustomIcon aList={aList} />
					</ListItemIcon>
					<ListItemText primary="Home" />
				</ListItem>
			</List>
			<Divider />

			<List>
				<ListItem button onClick={() => history.push("/PTTS/newECB603")}>
					<ListItemIcon>
						<FormatListNumberedIcon />
					</ListItemIcon>
					<ListItemText primary="New ECB603" />
				</ListItem>
			</List>
			<List>
				<ListItem button onClick={() => history.push("/PTTS/newDS1983")}>
					<ListItemIcon>
						<FormatListNumberedIcon />
					</ListItemIcon>
					<ListItemText primary="New DS-1983" />
				</ListItem>
			</List>
			<List>
				<ListItem button onClick={() => history.push("/PTTS/newDS1984")}>
					<ListItemIcon>
						<FormatListNumberedIcon />
					</ListItemIcon>
					<ListItemText primary="New DS-1984" />
				</ListItem>
			</List>
			<List>
				<ListItem button onClick={() => history.push("/PTTS/newDS1986")}>
					<ListItemIcon>
						<FormatListNumberedIcon />
					</ListItemIcon>
					<ListItemText primary="New DS-1986" />
				</ListItem>
			</List>
			<List>
				<ListItem button onClick={() => history.push('/PTTS/BulkUpdateLocation/') }>
					<ListItemIcon>
						<FormatListNumberedIcon />
					</ListItemIcon>
					<ListItemText primary="Bulk Update Status of Tickets" />
				</ListItem>
			</List>
			<List>
				<ListItem button onClick={() => {
					allActions.LoginADacts.actionTologoff("anything", props.dispatchRef);
					history.push("/PTTS/logout");

				}}>
					<ListItemIcon>
						<ExitToAppIcon />
					</ListItemIcon>
					<ListItemText primary="Logout" />
				</ListItem>
			</List>
			<List>
				<ListItem>
					<PTTSfunctions.ADfunctions.RenderPTTSmembership />
				</ListItem>
			</List>
		</div>
	);
}

const NavMenuJSX = (props: {
	dispatchRef: any, content: tGiantState,
	handleDrawerToggle: any
}) => {
	const classes = useStyles();
	const [mobileOpen, setMobileOpen] = React.useState(true);
	return (
		<Fragment>
			< Hidden mdUp >
				<DrawerMui
					variant="temporary"
					anchor={"left"}
					open={mobileOpen}
					classes={{
						paper: classes.drawerPaper,
					}}
					onClose={props.handleDrawerToggle}
					ModalProps={{
						keepMounted: true, // Better open performance on mobile.
					}}
				>
					<Drawer dispatchRef={props.dispatchRef} content={props.content}  />
				</DrawerMui>
			</Hidden >

			<Hidden smDown>
				<DrawerMui
					variant="permanent"
					open
					classes={{
						paper: classes.drawerPaper,
					}}
				>
					<Drawer dispatchRef={props.dispatchRef} content={props.content} />
				</DrawerMui>
			</Hidden>
		</Fragment >
	);

}

function App(props: any) {
	const classes = useStyles();
	const [mobileOpen, setMobileOpen] = React.useState(true);

	const dispatch = useDispatch();
	const content: tGiantState = useSelector((state: any) => state.giantReducer);

	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("App content/dispatch has:");
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(content);
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(!dispatch ? "no dispatch obj" : dispatch);

	const theme = useTheme();

	const isMobile = useMediaQuery(theme.breakpoints.up('sm'));

	const handleDrawerToggle = () => {
		setMobileOpen(!mobileOpen);
	};


	if (content.ST_LOGINAD) {

		return (
			<Router history={history}>
				<div className={classes.root}>
					<div className={classes.appFrame}>
						<AppBar className={classes.appBar}>
							<Toolbar>
								<IconButton
									color="inherit"
									aria-label="open drawer"
									onClick={handleDrawerToggle}
									className={classes.navIconHide}>
									<MenuIcon />
								</IconButton>
								<img src={PTTSfunctions.Logos.DSNYLogo.logoImg} alt="" className={classes.logo} />
								<span className={classes.divLogoText}> sanitation </span>
								<span className={classes.divLogoText2}> Enforcement Paper Ticket Tracking System</span>
							</Toolbar>
						</AppBar>
						<NavMenuJSX dispatchRef={dispatch} content={content} handleDrawerToggle={handleDrawerToggle}   />
						<Routes dispatchRef={dispatch} passedInContent={content} />
					</div>
				</div>
			</Router>
		);
    }
	return (
		<Router history={history}>
			<div className={classes.root}>
				<div className={classes.appFrame}>
					<AppBar className={classes.appBar}>
						<Toolbar>
							<IconButton
								color="inherit"
								aria-label="open drawer"
								onClick={handleDrawerToggle}
								className={classes.navIconHide}>
								<MenuIcon />
							</IconButton>
							<img src={PTTSfunctions.Logos.DSNYLogo.logoImg} alt="" className={classes.logo} />
							<span className={classes.divLogoText}> sanitation </span>
							<span className={classes.divLogoText2}> Enforcement Paper Ticket Tracking System</span>
						</Toolbar>
					</AppBar>
					<Routes dispatchRef={dispatch} passedInContent={content} />
				</div>
			</div>
		</Router>
	);
};

function HomeCustomIcon(props: { aList: Complaint[] | ECB603[] }) {
	//let uncompletedTodos: any = !props.aList ? [] : props.aList.every((t: any) => !t.completed ? true : t.completed !== false);

	// if (uncompletedTodos.length > 0) {
	if (false) {
		return (
			<Badge color="secondary" badgeContent={1}>
				<HomeIcon />
			</Badge>
		);
	} else {
		return <HomeIcon />;
	}
}

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		width: "100%",
		height: "100%",
		zIndex: 1,
		overflowY: "scroll",
		overflowX: "hidden"
	},
	appFrame: {
		position: "relative",
		display: "flex",
		width: "100%",
		height: "100%",
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		position: "absolute",
		backgroundColor: "#244514",
	},
	navIconHide: {
		[theme.breakpoints.up("md")]: {
			display: "none",
		},
	},
	drawerHeader: { ...theme.mixins.toolbar },
	drawerPaper: {
		width: drawerWidth,
		backgroundColor: theme.palette.background.default,
		[theme.breakpoints.up("md")]: {
			width: drawerWidth,
			position: "relative",
			height: "100%",
		},
	},
	content: {
		backgroundColor: theme.palette.background.default,
		width: "100%",
		height: "calc(100% - 56px)",
		marginTop: 56,
		marginLeft: 0, // drawerWidth,
		[theme.breakpoints.up("sm")]: {
			height: "calc(100% - 64px)",
			marginTop: 64,
		},
	},
	logo: {
		height: "50px",
		width: "50px"
	},	
	divLogoText: {
		color: "white",
		fontSize: 30,
		fontWeight: 500,
		verticalAlign: "top",
		margin: ".5%"
	},
	divLogoText2: {
		color: "white",
		fontSize: 16,
		margin: ".5%"
	}
}));

export default withRoot(App);
