﻿import React from "react";
import { useForm, FormProvider } from "react-hook-form";
import {
	Checkbox,
	Typography,
	FormControlLabel,
	Button
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import bData from "../assets/boroughData.json";
import dData from "../assets/districtData.json";
import cData from "../assets/charges.json";
import titleOpts from "../assets/employeeTitles.json";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers';
import { history } from "../configureStore";
import {
	ComplaintTemplate,
	ComplaintWitnesses,
	ComplaintRespondent,
	ComplaintCharges,
	ComplaintStatement,
	RespondentPhoto
} from "../components/complaint";
import { referenceActions } from '../actions';

library.add(faSearch);

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,		
		color: 'white',			
		padding: '0 5px',		
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12		
	},
})(Button);

const StyledButtonGreen = withStyles({
	root: {
		background: '#2b995f', //'#287734',		
		borderRadius: 3,
		border: 0,
		color: 'white',		
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const districtData = dData.district.map((d) => {
	return (
		{ value: d.id, label: d.name }
	)
}).sort((a, b) => (a.label > b.label) ? 1 : -1);

const chargeData =  cData.charges.map((d: any) => {
	return (
		{ value: d.id, label: d.name }
	)
}).sort((a, b) => (a.label > b.label) ? 1 : -1);


const boroughData = bData.borough.map((boro) => {
	return (
		{ value: boro.id, label: boro.name }
	)
}).sort((a, b) => (a.label > b.label) ? 1 : -1);

const titleOptions = titleOpts.titles.map((d, idx) => {
	return (
		{ value: idx, label: d.name }
	)
}).sort((a, b) => (a.label > b.label) ? 1 : -1);

export function NewComplaintPage(data: any) {
	const classes = useStyles();

	const schema = yup.object().shape({
		district: yup.string().required("District is a required field"),
        borough: yup.string().required("Borough is a required field"),
	});

	const methods = useForm({ resolver: yupResolver(schema) });
	const { register, handleSubmit, setValue, clearErrors, errors } = methods;

	const [chargeData, setChargeData] = React.useState([]);
	const [subChargeOptions, setSubChargeOptions] = React.useState([{value: 0, label: ""}]);

	const [employee, setEmployee] = React.useState({
		name: "",
		respondent: "",
		referenceNo: "",
		status: "",
		dateOfBirth: "",
		homeAddress: "",
		payrollLocation: "",
		title: "",
		isCivilian: false,
		badgeNo: "",
		chartNo: "",
		hireDate: "",
		promotionDate: "",
		vacationSchedule: "",
		underProbation: "false",
		district: 0,
		borough: 0
	});
	const [complRefData, setComplRefData] = React.useState({});

	const [district, setDistrict] = React.useState({ value: 0, label: "" });
	const [borough, setBorough] = React.useState({ value: 0, label: "" });	
	const [comments, setComments] = React.useState("");
	const [complainants, setComplainants] = React.useState([{ name: { value: 0, label: "" }, title: "", district: "", statement: "" }]);
	const [witnesses, setWitnesses] = React.useState([{ witnessName: { value: 0, label: "" }, additionalInfo: "" , nonDSNY: false, nonDSNYWitnessName: ""}]);
	const [charges, setCharges] = React.useState([{ chargeName: { value: 0, label: "" }, subChargeName: { value: 0, label: "" }, showSubCharges: false, incidentLocation: {value: 0, label: ""}, incidentDate: ""} ]);
	const [files, setFiles] = React.useState([{ fileName: { value: 0, label: "" } }]);

	const [complainantOpts, setComplainantOpts] = React.useState([{ value: 0, label: "" }]);
	const [witnessOpts, setWitnessOpts] = React.useState([{ value: 0, label: "" }]);
	const [complainantSearch, setComplainantSearch] = React.useState("");
	const [witnessSearch, setWitnessSearch] = React.useState("");

	const [showAddChargesOptns, setShowAddChargesOptns] = React.useState(false);
	const [showTemplate, setShowTemplate] = React.useState(false);
	const [showPhoto, setShowPhoto] = React.useState(false);
	const [photoURL, setPhotoURL] = React.useState("");

	const [isLoading, setLoading] = React.useState(false);

	React.useEffect(() => {

     	register({ name: "district" });
		register({ name: "borough" });
		register({ name: "charges" });
		register({ name: "complainants"});
	 	register({ name: "witnesses" });
		register({ name: "comments" });		

		(async () => {
			setLoading(true);
			const response = await referenceActions.chargesWithSubcharges();
			setLoading(false);
			setChargeData(response.data);
		})();
		
		(async () => {
			setLoading(true);
			const response = await referenceActions.employeeByRefNumber(data.location.state);
			setLoading(false);				
			
			let middleName = "";
			if (response.data.employeeModel.names[0].middleName !== undefined) {
				middleName = response.data.employeeModel.names[0].middleName.charAt(0);				
			}

			let badgeNumber = "";
			if ((response.data.jobModel.badgeNumber !== undefined) && (response.data.jobModel.badgeNumber !== null)) {
				badgeNumber = response.data.jobModel.badgeNumber.badgeNumber;
			}

			let probation = "false";
			if (response.data.jobModel.jobInformation.probationDate !== undefined) {
				probation = "true";
			}

			let location = "";
			if (localStorage.getItem('payrollLocations')) {
				const payrollLocations = JSON.parse(localStorage.getItem('payrollLocations')!);				
				location = payrollLocations.data.filter((r: any) => r.correlationId.correlation.match(response.data.jobModel.jobLocation.businessUnitAddressId.correlation))[0].description;				
            }
			
			setEmployee({
				name: response.data.employeeModel.names[0].lastName + ', ' + response.data.employeeModel.names[0].firstName + ' ' + middleName,
				respondent: response.data.employeeModel.names[0].firstName + ' ' + response.data.employeeModel.names[0].lastName,
				referenceNo: response.data.employeeModel.employeeId,
				status: response.data.jobModel.employeeStatusId.correlation,
				dateOfBirth: response.data.employeeModel.birthDate,
				homeAddress: response.data.employeeModel.addresses[0].line1 + ' ' + response.data.employeeModel.addresses[0].city + ', ' + response.data.employeeModel.addresses[0].stateId.correlation.replace('_', ' ') + ' ' + response.data.employeeModel.addresses[0].zipCode,
				payrollLocation: location,
				title: response.data.jobModel.jobInformation.jobTypeId.correlation.replace(/(_)/g, ' '),
				isCivilian: (response.data.jobModel.dsnyInformation.titleId.correlation === "CIV") ? true : false,
				badgeNo: badgeNumber,
				chartNo: " ",
				hireDate: response.data.jobModel.jobAssignment.hireDate,
				promotionDate: response.data.jobModel.jobInformation.jobEntryDate,
				vacationSchedule: " ",
				underProbation: probation,
				district: 0,
				borough: 0
			});			

			setPhotoURL("https://msswva-dsnmyd01.csc.nycnet/myDSNYProd/Common/EmployeePhoto.ashx?pms=" + data.location.state);
		})();

		// TODO: need to map district and borough

        // 	setDistrict({
		//	value: Number(employeeData[0].district),
		//	label: districtData.filter((d) => {	return d.value === Number(employeeData[0].district);})[0].label
		//});

		//setBorough({
		//	value: Number(employeeData[0].borough),
		//	label: boroughData.filter((d) => { return d.value === Number(employeeData[0].borough); })[0].label
		//});

	}, []);

	React.useEffect(() => {
		if (complainantSearch.length > 2) {
			(async () => {
				setLoading(true);
				const response = await referenceActions.employeeListByLastName(complainantSearch);
				setLoading(false);

				const opts = response.data.map((person: any) => {
					return (
						{ value: person.employeeId, label: person.names[0].lastName + ', ' + person.names[0].firstName }
					)
				}).sort((a: any, b: any) => (a.label > b.label) ? 1 : -1);

				setComplainantOpts(opts);
			})();
		}
	}, [complainantSearch]);

	React.useEffect(() => {
		if (witnessSearch.length > 2) {
			(async () => {
				setLoading(true);
				const response = await referenceActions.employeeListByLastName(witnessSearch);
				setLoading(false);

				const opts = response.data.map((person: any) => {
					return (
						{ value: person.employeeId, label: person.names[0].lastName + ', ' + person.names[0].firstName }
					)
				}).sort((a: any, b: any) => (a.label > b.label) ? 1 : -1);

				setWitnessOpts(opts);
			})();
		}
	}, [witnessSearch]);

	const chargeOptions = chargeData.map((d: any, idx: any) => {
		return (
			{ value: d.primaryChargeId, label: d.name + " " + d.description, needsSubCharge: d.needsSubCharge, subCharges: d.relatedSubChargeVM}
		)
	}).sort((a, b) => (a.label > b.label) ? 1 : -1);

	const dropFiles = (event: any) => {
		//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(event[0]);
	};

	const createOption = (index: number, inputValue: string) => ({
		value: index,
		label: inputValue
	});

	const handleCloseTemplate = (statementValue: string) => {		
		if (statementValue !== "") {			
			let updatedTemplate = [...complainants];
			updatedTemplate[complainants.length - 1] = {
				...complainants[complainants.length - 1], statement: statementValue
			}
			setComplainants(updatedTemplate);
			setValue("complainants", updatedTemplate);
		}

		setShowTemplate(false);
	};

	const handleOpenTemplate = () => {
		setShowTemplate(true);
	};

	const handleShowPhoto = () => {
		setShowPhoto(true);
	};

	const handleHidePhoto = () => {
		setShowPhoto(false);
	};

	const handleChange = (e: any, control?: string, idx?: number) => {
		if (control === "district") {
			setDistrict({
				value: e.value,
				label: districtData.filter((d) => { return d.value === e.value; })[0].label
			});

			setValue("district", e.value);
		}
		else if (control === "borough") {
			setBorough({
				value: e.value,
				label: boroughData.filter((d) => { return d.value === e.value; })[0].label
			});

			setValue("borough", e.value);
		}
		else if (control === "witness") {
			const selectedWitness = witnessOpts.filter((d) => { return d.value === e.value; })[0].label;

			let updatedWitnesses = [...witnesses];
			updatedWitnesses[Number(idx)] = {
				...witnesses[Number(idx)], witnessName: { value: e.value, label: selectedWitness }
			}

			setWitnesses(updatedWitnesses);
			setValue("witnesses", updatedWitnesses);
			clearErrors("witnessName" + idx);
		}
		else if (control === "witnessInfo") {
			let updatedWitnesses = [...witnesses];
			updatedWitnesses[Number(idx)] = {
				...witnesses[Number(idx)], additionalInfo: e.target.value
			}

			setWitnesses(updatedWitnesses);
			setValue("witnesses", updatedWitnesses);
		}
		else if (control === "nonDSNY") {
			let updatedWitnesses = [...witnesses];
			updatedWitnesses[Number(idx)] = {
				...witnesses[Number(idx)], nonDSNY: e.target.checked, witnessName: { value: 0, label: "" }, nonDSNYWitnessName: ""
			}

			setWitnesses(updatedWitnesses);
			setValue("witnesses", updatedWitnesses);
		}
		else if (control === "nonDSNYWitnessName") {
			let updatedWitnesses = [...witnesses];
			updatedWitnesses[Number(idx)] = {
				...witnesses[Number(idx)], nonDSNYWitnessName: e.target.value
			}

			setWitnesses(updatedWitnesses);
			setValue("witnesses", updatedWitnesses);
		}
		else if (control === "charges") {
			const selectedCharge = chargeOptions.filter((d) => { return d.value === e.value; })[0].label;
			const needsSubCharge = chargeOptions.filter((d) => { return d.value === e.value; })[0].needsSubCharge;

			let updatedCharges = [...charges];
			updatedCharges[Number(idx)] = {
				...charges[Number(idx)], chargeName: { value: e.value, label: selectedCharge }, subChargeName: { value: 0, label: "" }, showSubCharges: needsSubCharge
			}

			if (needsSubCharge) { 
			const options = chargeOptions.filter((d: any) => { return d.value === e.value; })[0].subCharges.map((d: any, idx: any) => {
				return (
					{ value: d.subChargeId, label: d.name }
				)
			}).sort((a: any, b: any) => (a.label > b.label) ? 1 : -1);

			setSubChargeOptions(options);
	     	}

			setCharges(updatedCharges);
			setValue("charges", updatedCharges);			
		}
		else if (control === "subcharges") {
			const selectedSubCharge = subChargeOptions.filter((d: any) => { return d.value === e.value; })[0].label;

			let updatedSubCharges = [...charges];
			updatedSubCharges[Number(idx)] = {
				...charges[Number(idx)], subChargeName: { value: e.value, label: selectedSubCharge }
			}

			setCharges(updatedSubCharges);
			setValue("charges", updatedSubCharges);
		}
		else if (control === "incidentLocation") {
			const selectedLocation = districtData.filter((d) => { return d.value === e.value; })[0].label;

			let updatedSubCharges = [...charges];
			updatedSubCharges[Number(idx)] = {
				...charges[Number(idx)], incidentLocation: { value: e.value, label: selectedLocation }
			}

			setCharges(updatedSubCharges);
			setValue("charges", updatedSubCharges);
		}
		else if (control === "incidentDate") {
			let updatedSubCharges = [...charges];
			updatedSubCharges[Number(idx)] = {
				...charges[Number(idx)], incidentDate: e.target.value
			}

			setCharges(updatedSubCharges);
			setValue("charges", updatedSubCharges);
		}
	};

	const handleComplainantChange = (e: any, control?: string, idx?: number) => {
		let updatedComplainants = [...complainants];
		
		if (control === "name") {
			const selectedComplainant = complainantOpts.filter((d) => { return d.value === e.value; })[0].label;

			updatedComplainants[Number(idx)] = {
				...complainants[Number(idx)], name: { value: e.value, label: selectedComplainant }
			}
		}
		//else if (control === "title") {
		////	const selected = titleOptions.filter((d) => { return d.value === e.value; })[0].label;

		//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.value);

		//	updatedComplainants[Number(idx)] = {
		//		...complainants[Number(idx)], title: e.target.value
		//	}	
		//	setValue("complainants", updatedComplainants);
		//}
		//else if (control === "district") {
		//	const selected = districtData.filter((d) => { return d.value === e.value; })[0].label;

		//	updatedComplainants[Number(idx)] = {
		//		...complainants[Number(idx)], district: { value: e.value, label: selected }
		//	}			
		//}
		else if (control === "statement") {
			updatedComplainants[Number(idx)] = {
					...complainants[Number(idx)], statement: e.target.value }			
			}					
		
		setComplainants(updatedComplainants);
		setValue("complainants", updatedComplainants);
	}

	const handleComplainantRefDataChange = (title: string, location: string, idx: number) => {
		let updatedComplainants = [...complainants];		

		updatedComplainants[Number(idx)] = {
				...complainants[Number(idx)], title: title, district: location
		}	
		
		setComplainants(updatedComplainants);
		setValue("complainants", updatedComplainants);		
	}

	const handleInputChange = (e: any, caller: string) => {
		if (e !== null && e !== undefined) {
			if (e.length > 2) {
				if (caller === "complainant") {
					setComplainantSearch(e);
				}
				else
				{
					setWitnessSearch(e);
				}
			}
		}
	};

	const handleCommentsChanged = (event: any) => {
		if (event.target.value !== null && event.target.value !== undefined) {
			const value = event.target.value;
			if (value.length <= 500 ) {
				setComments(event.target.value);
				setValue("comments", event.target.value);
			}
		}
	}

	const handleAddWitness = (event: any) => {
		const newWitness = { witnessName: { value: 0, label: "" }, additionalInfo: "", nonDSNY: false, nonDSNYWitnessName: "" };
		setWitnesses([...witnesses, { ...newWitness }]);
	}

	const handleAddComplainant = (event: any) => {
		const newComplainant = { name: { value: 0, label: "" }, title: "", district: "", statement: "" };
		setComplainants([...complainants, { ...newComplainant }]);
	}

	const handleRemoveWitness = (event: any, idx: number) => {
		const updatedWitnesses = witnesses.filter((_, i) => i !== idx);
		setWitnesses(updatedWitnesses);
		setValue("witnesses", updatedWitnesses);			
	}

	const handleChargesClicked = (event: any) => {
		if (showAddChargesOptns === false) {
			setShowAddChargesOptns(true);
		}
		else
		{
			setShowAddChargesOptns(false);
        }
	}

	const handleAddChargeOptnClicked = (event: any, optionSelected: string) => {
		
		if (optionSelected === "new") {
			const newCharge = { chargeName: { value: 0, label: "" }, subChargeName: { value: 0, label: "" }, showSubCharges: false, incidentLocation: { value: 0, label: ""}, incidentDate: "" };
			setCharges([...charges, { ...newCharge }]);
		}
		else {			
			const location = charges[charges.length - 1].incidentLocation;
			const newCharge = { chargeName: { value: 0, label: "" }, subChargeName: { value: 0, label: "" }, showSubCharges: false, incidentLocation: location, incidentDate: charges[charges.length - 1].incidentDate };
			setCharges([...charges, { ...newCharge }]);
		}

		setShowAddChargesOptns(false);
	}

	const handleRemoveCharge = (event: any, idx: number) => {
		const updatedCharges = charges.filter((_, i) => i !== idx);
		setCharges(updatedCharges);
		setValue("charges", updatedCharges);
	}

	const handleRemoveFile = (event: any, idx: number) => {
		const updatedFiles = files.filter((_, i) => i !== idx);
		setFiles(updatedFiles);
		setValue("files", updatedFiles);
	}

	const handleRemoveComplainant = (event: any, idx: number) => {
		const updatedComplainants = complainants.filter((_, i) => i !== idx);
		setComplainants(updatedComplainants);
		setValue("complainants", updatedComplainants);
	}

	const onSubmit = (data: any) => {
		// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(errors.witnesses);
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handle submit");
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(data);
	};

	const onCancel = ()  => {
		history.push('/PTTS/home');
	};

	return (
		<div className={classes.formContainer}>
			<div>
				<FormProvider {...methods}>
				<form>
					<div><h1 className={classes.h2}>New Complaint</h1></div>
						<ComplaintTemplate open={showTemplate} onClose={handleCloseTemplate} respondent={employee.respondent} incidentDate={charges[0].incidentDate} />
						<RespondentPhoto open={showPhoto} respondentPhotoURL={photoURL} onClose={handleHidePhoto} />
						<ComplaintRespondent
							employee={employee}
							district={district}
							borough={borough}
							districtOptions={districtData}
							boroughOptions={boroughData}
							handleChange={handleChange}
							handleShowPhoto={handleShowPhoto}/>
					    <ComplaintCharges
							charges={charges}
							chargeOptions={chargeOptions}
							chargeToFileOptions={chargeData}
							districtOptions={districtData}
							handleChange={handleChange}
							subChargeOptions={subChargeOptions}
							handleRemoveCharge={handleRemoveCharge}
							handleAddChargeOptnClicked={handleAddChargeOptnClicked}
							handleChargesClicked={handleChargesClicked}
							showAddChargesOptns={showAddChargesOptns}
							dropFiles={dropFiles}
							files={files}
							comments={comments}
							removeFile={handleRemoveFile}
							handleCommentsChanged={handleCommentsChanged} />	
						<ComplaintStatement
							complainants={complainants}
							districtOptions={districtData}
							titleOptions={titleOptions}
							complainantOptions={complainantOpts}
							handleComplainantChange={handleComplainantChange}
							handleInputChange={handleInputChange}
							handleRemoveComplainant={handleRemoveComplainant}
							handleAddComplainant={handleAddComplainant}
							handleOpenTemplate={handleOpenTemplate}
							handleComplainantRefDataChange={handleComplainantRefDataChange}/>								
						<ComplaintWitnesses
							witnesses={witnesses}
							witnessOptions={witnessOpts}
							handleChange={handleChange}
							handleRemoveWitness={handleRemoveWitness}
							handleAddWitness={handleAddWitness}
							handleInputChange={handleInputChange} />					
					<Grid container className={classes.gridActions} spacing={0}>	
						<Grid item xs={12}><div className={classes.divMargin}>
							<FormControlLabel
									name="statemenOfRights"
									control={<Checkbox size="small" color="default" checked={true} />}
							label={<Typography className={classes.label}>Provide the Statement of Rights in the DS249 complaint</Typography>}
							labelPlacement="end"					
						/></div></Grid>
						<Grid item xs={12}><StyledButtonGreen
						className={classes.button}
						variant="outlined"
						color="primary"
						onClick={handleSubmit(onSubmit)}>
							Submit
				    </StyledButtonGreen>
					<StyledButton
						className={classes.button}
						variant="outlined"
						color="primary">
						Save as Draft
				</StyledButton>
				<StyledButton
						className={classes.button}
						variant="outlined"
						color="primary"
						onClick={onCancel}>
								Cancel
				</StyledButton></Grid>
				</Grid>
					</form>
				</FormProvider>
			</div>
		</div>
	);
}

const useStyles = makeStyles({	
	reactselect: {
		maxWidth: "90%",		
		height: 20,
		marginBottom: 15,			
	},
	link: {
		color: "green",
		fontSize: 12,		
	},
	logo: {
		height: "20px",
		width: "22px",
		verticalAlign: "middle",
		marginLeft: "-2px;"
	},
	deleteIcon: {
		height: "20px",
		width: "25px",
		verticalAlign: "middle",
		marginLeft: "-2px;"
	},
	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",		
		fontSize: 12,
		fontWeight: 800,
		color: "#2e2e2d",
	},	
	labelSectionTitle: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",		
		fontSize: 14,
		fontWeight: 900,
		color: "#2e2e2d",
	},
	labelReadOnlyData: {
		marginBottom: 10,
		marginTop: 10,
		color: "#2e2e2d",
		fontSize: 12,
		fontWeight: 500
	},
	labelX: {
		lineHeight: 2,
		textAlign: "left",		
		fontSize: 12,
		fontWeight: 800,
		color: "#2e2e2d",
		width: "90%"
	},
	labelY: {
		lineHeight: 2,
		textAlign: "right",
		fontSize: 12,
		fontWeight: 800,
		color: "#2e2e2d",
		width: "90%"
	},
	labelRemove: {
		lineHeight: 2,
		textAlign: "right",
		fontSize: 12,
		fontWeight: 800,
		color: "#bf1650",
		width: "90%"
	},
	labelRemoveXS3: {
		lineHeight: 2,
		textAlign: "right",
		fontSize: 12,
		fontWeight: 800,
		color: "#bf1650",
		width: "80%"
	},
	labelOptions: {
		fontSize: 13,
		fontWeight: 700,
		color: "#2e2e2d",
		marginLeft: "15px"		
    },
	button: {
		margin: "10px",
		color: "green",
		width: "110px",
		height: "35px",
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	buttonInner: {
		marginTop: "10px",
		paddingTop: "0px",
		marginRight: "20px",
		width: "125px",
		height: "30px",
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	buttonTemplate: {
		marginTop: "30px",
		paddingTop: "0px",
		marginRight: "20px",
		width: "125px",
		height: "30px",
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	buttonInnerDelete: {
		marginTop: "10px",
		marginRight: "20px",
		width: "125px",
		height: "30px",
		backgroundColor: "#952717",
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	grid: {
		background: "white",
		marginLeft: "20px",
		marginRight: "20px",
		width: "calc(100% - 40px)",
		paddingTop: "10px",
		paddingLeft: "10px",
		paddingBottom: "10px",
		marginBottom: "7px;"
	},
	gridCell: {		
		paddingBottom: "5px",		
	},
	gridSpacerCell: {
		paddingBottom: "0px",
	},
	control: {
		backgroundColor: "#edeff0"
	},
	formContainer: {
		backgroundColor: "#edeff0",
		width: "100%",
		fontSize: 12,
		fontWeight: 500
	},	
	placeholder: {
		fontSize: 12,
		fontWeight: 500,
		fontFamily: "Roboto, Helvetica, Arial, sans-serif",
    },
	h2: {
		marginTop: "5px",
		marginBottom: "15px",
		marginLeft: "20px",
		fontSize: 26

	},
	h5: {
		marginTop: "5px",
		marginBottom: "10px",		
		fontSize: 14
	},
	gridActions: {
		background: "#edeff0",
		marginLeft: "20px",
		marginRight: "20px",
		width: "calc(100% - 40px)",
	},
	divMargin: {
		marginLeft: "10px",
	},	
	divOptions: {
		width: "45%",
		backgroundColor: "#edeff0",			
	},
	divOptionItem: {		
		paddingTop: "7px",
		paddingBottom: "7px",
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	datepicker:	{
		width: "90%",				
		height: 38,
		marginBottom: 0,
		fontSize: 12,
		borderRadius: 1,		
	},
	reactselectLarge: {
		width: "95%",		
		height: 20,
		marginBottom: 15
	},
	textArea: {
		width: "95%",
		borderColor: "#c5c8c5",
		border: "1px solid",		
	},
	dropFileArea: {
		width: "95%",
		borderColor: "#c5c8c5",
		border: "1px dashed",
		minHeight: "120px",		
	},
	input: {
		display: "block",
		width: "90%",
		borderRadius: 1,
		border: "1px solid",
		borderColor: "#c5c8c5", 
		padding: "10px 10px",
		fontSize: 12,		
		'&:focus': {
			outlineColor: '#2b995f', 						
		}
	},	
	MuiDropzoneAreaText: {
		marginBottom: "10px"
	},
	checkbox: {
		paddingTop: "0px",
		paddingBottom: "0px"
		
	},
	});
