﻿export interface DS1704Reasons {
    [x: string]: any;
    Id: number;
    Code1: string;
    Description: string;
    Type: string;
    OrderSequence: number;
}

export type aDS1704rows = {
    id: number; // sequential 1 to 20
    POC: string; // place of occurrence -- district issuing ticket
    NOVnumber: string;
    NOVdate: string; // YYYY-MM-DD
    Address: string; // where ticket was issued to respondent
    ReasonForVoid: string; // selection made from the dropdown list
    VerifiedBy: string; // uid of the supervisors who authorized the void
}


export interface tDS1704 {
    [x: string]: any;
    ECBSummonsNumber: string;
    TimeServed?: Date;
    PlaceOfOccurence: string;
    Remarks: string;
    ModifiedBy: string;
    ModifiedOn?: Date;
    VoidDate?: Date;
    VoidReasonId?: number;
    CreatedBy: string;
    CreatedDate?: Date;
    IsActive?: boolean;
    ApprovedBy: string;
    District: string;
}

export interface efDS1704 {
    ECBSummonsNumber: string;
    TimeServed?: any;
    PlaceOfOccurence: string;
    Remarks: string;
    ModifiedBy: string;
    ModifiedOn: Date;
    VoidDate: Date;
    VoidReasonId: number;
    CreatedBy: string;
    CreatedDate: Date;
    IsActive: boolean;
    ApprovedBy: string;
    District: string;
}


export interface efDS1704iud {
    ECBSummonsNumber: string;

    PlaceOfOccurence: string;
    Remarks: string;
    ModifiedBy: string;

    VoidDate: Date;
    VoidReasonId: number;

}


export enum DS1704Actions {
    ADD_DS1704 = "ADD_DS1704",
    UPDATE_DS1704 = "UPDATE_DS1704",
    DELETE_DS1704 = "DELETE_DS1704",
    COMPLETE_DS1704 = "COMPLETE_DS1704",
    REVERT2INCOMPLETE_DS1704 = "REVERT2INCOMPLETE_DS1704",
    RESET_DS1704 = "RESET_DS1704List",
}

interface DS1704ActionType<T, P> {
    type: T;
    payload: P;
}

export type DS1704Action =
    | DS1704ActionType<typeof DS1704Actions.ADD_DS1704, tDS1704>
    | DS1704ActionType<typeof DS1704Actions.COMPLETE_DS1704, number>
    | DS1704ActionType<typeof DS1704Actions.REVERT2INCOMPLETE_DS1704, number>
    | DS1704ActionType<typeof DS1704Actions.UPDATE_DS1704, tDS1704>
    | DS1704ActionType<typeof DS1704Actions.DELETE_DS1704, number>
    ;
