﻿// import { BASE_URL } from "./constantsAPI";

export const API_NTI_AD_URI = {

    ntiProfile: "http://10.155.230.134:1958/api/NTI/GetUserNTIprofile", // GET
    // ntiProfile: BASE_URL + "NTI/GetUserNTIprofile", // GET

    adAuth: "https://localhost:5001/api/User/ValidateUserCredentials", // PUT
    adDetails: "https://localhost:5001/api/User/UserDetails", // PUT
    adGroups: "https://localhost:5001/api/User/UserADgroupMembership", // PUT

    // Requests: "http://10.155.206.106:30707/api/Requester/LoadOnbGrid",
    // Requests: "http://mstlva-dsnsmt20.csc.nycnet:30707/api/Requester/LoadOnbGrid",
    // Requests: "http://localhost:60252/api/Requester/LoadOnbGrid",


}
