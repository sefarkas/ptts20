import { userAction } from '../model';

export const INITIAL_STATE = localStorage.getItem('user') ? { loggedIn: true } : {};

export const  authentication = (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case userAction.LOGIN_REQUEST:
      return {
          ...state,loggingIn: true,
        
      };
    case userAction.LOGIN_SUCCESS:
      return {
          ...state, loggedIn: true,
      };
    case userAction.LOGIN_FAILURE:
          return { ...state, loggedIn: false};
    case userAction.LOGOUT:
          return { ...state,loggedIn: false};
    default:
      return { ...state }
  }
}
