﻿import { commonf as Logos } from './Logo';
import { commonf as Commonfunctions } from './functions';
import { commonf2 as Commonfunctions2 } from './functions';

import { commonf as ADfunctions } from './PTTSadGroups';

export const PTTSfunctions = {
    Logos,
    Commonfunctions,
    Commonfunctions2,
    ADfunctions
};