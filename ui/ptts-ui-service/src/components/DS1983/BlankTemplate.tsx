﻿import { ErrorMessage } from "@hookform/error-message";
import { Button, FormControl, MenuItem, Select } from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useEffect, useState } from "react"; // , useReducer
import { Controller, useForm } from "react-hook-form"; // , FormProvider
import { isNullOrUndefined } from 'util';
import { allServices } from '../../services';
import { history } from "../../configureStore";
import { ECB603, tSpecialDistrict, UsedTicketRange } from '../../model';
import { PTTSfunctions } from '../basicComponents';
import { ErrorMessageContainer, ErrorSummary } from '../basicComponents/validation';

type postDataResult<T> = {
	success: boolean;
	errors?: { [P in keyof T]?: string[] };
};

type FormData = {
	fldMessengerName: string;
	fldSummonsCount: number;
	fldSummonsFromNumber: number;
	fldSummonsToNumber: number;
	fldBorough: string;
	fldDistributorName: string;
	fldAcknowledgedBy: string;
	fldBoxNumber: number;
}

export function NewDS1983Form(props: any | undefined) {

	let anECB603: ECB603 | any | undefined  = props.history.location.state;

	const [IsAlreadyUsedTktRangeState, setIsAlreadyUsedTktRangeState] = useState(false);

	const [isLoading, setLoading] = React.useState(false);

	const [AcknowledgedByREFnoState, setAcknowledgedByREFnoState] = useState("");
	const [AcknowledgedByState, setAcknowledgedByState] = useState("");
	const [BadgeNoState, setBadgeNoState] = useState("");
	const [BoroughListState, setBoroughListState] = useState([{}]);
	const [BoroughState, setBoroughState] = useState("");
	const [BoxNumberState, setBoxNumberState] = useState(0);
	const [CreatedByState, setCreatedByState] = useState("");
	const [CreatedDateState, setCreatedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [DistributorNameState, setDistributorNameState] = useState("");
	const [DistributorREFnoState, setDistributorREFnoState] = useState("");
	const [DistributorSignatureState, setDistributorSignatureState] = useState("");
	const [DistributorSignedDateState, setDistributorSignedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [DistrictIDState, setDistrictIDState] = useState(0);
	const [FormStatusState, setFormStatusState] = useState("");
	const [IDState, setIDState] = useState(0);
	const [IsVoidState, setIsVoidState] = useState(false);
	const [MessengerNameState, setMessengerNameState] = useState("");
	const [MessengerPayCodeState, setMessengerPayCodeState] = useState("");
	const [MessengerREFnoState, setMessengerREFnoState] = useState("");
	const [MessengerSignatureState, setMessengerSignatureState] = useState("");
	const [MessengerSignedDateState, setMessengerSignedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [MessengerTitleState, setMessengerTitleState] = useState("");
	const [ModifiedByState, setModifiedByState] = useState("");
	const [ModifiedOnState, setModifiedOnState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [ReceiptNumberState, setReceiptNumberState] = useState("new created after save");
	const [SummonsCountState, setSummonsCountState] = useState(1000);
	const [SummonsFromNumberState, setSummonsFromNumberState] = useState(0);
	const [SummonsToNumberState, setSummonsToNumberState] = useState(0);

	const dummyDSspecialDistrict: tSpecialDistrict = {
		Id: 0,
		PayCode: "",
		Description: "",
		Remarks: "",
		SplinterDisrtict: false,
		MappingDistictNTI: "",
	}
	const [dsList, setdsList] = useState([dummyDSspecialDistrict]);

    const defaultValues = {
        Native: "",
        TextField: "",
        Select: "",
        ReactSelect: "",
        Checkbox: false,
        switch: false,
        RadioGroup: ""
    };

    const handleOnChange = (e: any) => {
        e.preventDefault();

        switch (e.target.name) {
            //case "fldReceiptNumber": setReceiptNumberState(e.target.value); break;
            case "fldSummonsCount": setSummonsCountState(e.target.value); break;
            // case "fldSummonsFromNumber": setSummonsFromNumberState(e.target.value); break;
            //case "fldSummonsToNumber":
            //	setIsAlreadyUsedTktRangeState(false);
            //	fIsAlreadyUsedTktRange("DS1983", "High", e.target.value);
            //	break;
            case "fldBorough": setBoroughState(e.target.value); break;
            // case "fldDistributorName": setDistributorNameState(e.target.value); break;
            //case "fldDistributorSignature": setDistributorSignatureState(e.target.value); break;
            //case "fldDistributorSignedDate": setDistributorSignedDateState(e.target.value); break;
            case "fldDistributorNameREFNO":
				setDistributorSignedDateState(PTTSfunctions.Commonfunctions.todaysdate);
                setDistributorREFnoState(e.target.value);
                break; // useEffect will run a lookup to get name etc.
            case "fldMessengerSignature": setMessengerSignatureState(e.target.value); break;
            //case "fldMessengerTitle": setMessengerTitleState(e.target.value); break;
            //case "fldMessengerSignedDate": setMessengerSignedDateState(e.target.value); break;
            case "fldMessengerNameREFNO":
				setMessengerSignedDateState(PTTSfunctions.Commonfunctions.todaysdate);
                setMessengerREFnoState(e.target.value);
                break; // useEffect will run a lookup to get name etc.
            case "fldAcknowledgedByREFNO":
                setAcknowledgedByREFnoState(e.target.value);
                setFormStatusState("Acknowledged");
                break; // useEffect will run a lookup to get name etc.
            //case "fldID": setIDState(e.target.value); break;
            case "fldBoxNumber": setBoxNumberState(e.target.value); break;
            //case "fldFormStatus": setFormStatusState(e.target.value); break;
            //case "fldModifiedOn": setModifiedOnState(e.target.value); break;
            //case "fldModifiedBy": setModifiedByState(e.target.value); break;
            case "fldBadgeNo": setBadgeNoState(e.target.value); break;
            //case "fldCreatedBy": setCreatedByState(e.target.value); break;
            //case "fldCreatedDate": setCreatedDateState(e.target.value); break;
            case "fldDistrictID": setDistrictIDState(e.target.value); break;
            //case "fldPayCode": setMessengerPayCodeState(e.target.value); break;
            case "fldIsVoid": setIsVoidState(e.target.value); break;

            case "fldSummonsFromNumber":
                setIsAlreadyUsedTktRangeState(false);
                let dummy2: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
                setSummonsFromNumberState(dummy2);
                let updatedTktNo2 = dummy2 + Number.parseInt(SummonsCountState.toString()) - 1;
                dummy2 > 9999999 && SummonsCountState > 0 ? setSummonsToNumberState(updatedTktNo2) : setSummonsToNumberState(0);
                dummy2 = 0;
                //fIsAlreadyUsedTktRange("DS1983", "Low", e.target.value);
                //fIsAlreadyUsedTktRange("DS1983", "High", updatedTktNo2.toString())
                break;

            default:
                // code block
                 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
                 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.value);
        }

		if (CreatedDateState.valueOf !== PTTSfunctions.Commonfunctions.todaysdate) {
			setCreatedDateState(PTTSfunctions.Commonfunctions.todaysdate);
        }

		if (CreatedByState.length < 2) {
			const curUser: any = localStorage.getItem('user');
			const j: any = JSON.parse(curUser);
			const loggedInUser: string = j.data.userPreferences.userName;
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("created by ", loggedInUser);
			setCreatedByState(loggedInUser);
        }

    }

    const submitForm = async (data: FormData, e: any) => {
		if (BoroughState.length < 3) {
			alert("PTTS expects you to pick the location you delivered to.");
		}
		else if (BoxNumberState < 1) {
			alert("PTTS expects you to acknowledge a particular Box Number.");
		}
		else {
			const oneDS1983form = { // take the individual hook const and set each oneDS1983form object properties
				AcknowledgedBy: AcknowledgedByState,
				BadgeNo: BadgeNoState,
				Borough: BoroughState,
				BoxNumber: BoxNumberState,
				CreatedBy: CreatedByState,
				CreatedDate: CreatedDateState,
				DistributorName: DistributorNameState,
				DistributorRefNo: DistributorREFnoState,
				DistributorSignature: DistributorSignatureState,
				DistributorSignedDate: DistributorSignedDateState,
				DistrictID: DistrictIDState,
				FormStatus: FormStatusState,
				ID: IDState,
				IsVoid: IsVoidState,
				MessengerName: MessengerNameState,
				MessengerSignature: MessengerSignatureState,
				MessengerSignedDate: MessengerSignedDateState,
				MessengerTitle: MessengerTitleState,
				ModifiedBy: ModifiedByState,
				ModifiedOn: ModifiedOnState,
				PayCode: MessengerPayCodeState,
				ReceiptNumber: ReceiptNumberState,
				SummonsCount: SummonsCountState,
				SummonsFromNumber: SummonsFromNumberState,
				SummonsToNumber: SummonsToNumberState,
			}
			const response = await allServices.PTTSiudServices.iudDS1983(oneDS1983form);
			// response is the new receipt number
			if (!response) {
				alert("I am having trouble with saving DS1983 values.");
				// any error message is in the CustomerOrderNo property in the controller
			}
			else {
				history.push('/PTTS/existingDS1983/' + response.data);
			}
        }

        // e.target.reset(); // reset after form submit
    };

    const classes = useStyles();

	const onCancel = () => {
        history.push('/PTTS/home');
    };

    const handleDropDownChange = async (event: any) => {
		let newSelection: string = "Pick one";
		if (PTTSfunctions.Commonfunctions2.isNotDefined(event.target.innerText)) {
            eventCurrentTarget = event.target;
			alert("DropDown event object is marked 'notDefined'");
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(eventCurrentTarget);
        }
		else {
			newSelection = event.target.innerText;
            await setBoroughState(newSelection);
            //  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Borough is" ,newSelection);
        }
		return newSelection;
    };

    const newDS1983 = () => { // used to clear the form
         if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("newDS1983 fired");
        setReceiptNumberState("");
        setMessengerNameState("");
        setSummonsCountState(0);
        setSummonsFromNumberState(0);
        setSummonsToNumberState(0);
        setBoroughState("");
        setDistributorNameState("");
        setDistributorSignatureState("");
		setDistributorSignedDateState(PTTSfunctions.Commonfunctions.todaysdate);
        setMessengerSignatureState("");
        setMessengerTitleState("");
		setMessengerSignedDateState(PTTSfunctions.Commonfunctions.todaysdate);
        setAcknowledgedByState("");
        setIDState(0);
        setBoxNumberState(0);
        setFormStatusState("");
		setModifiedOnState(PTTSfunctions.Commonfunctions.todaysdate);
        setModifiedByState("");
        setBadgeNoState("");
        setDistributorREFnoState("");
        setCreatedByState("");
		setCreatedDateState(PTTSfunctions.Commonfunctions.todaysdate);
        setDistrictIDState(0);
        setMessengerPayCodeState("");
        setIsVoidState(false);

    };

    const renderActionButtons = () => {
        return (
            <div>
                <StyledButton
                    className={classes.button}
                    variant="outlined"
                    color="primary"
                    type="submit"
                    onClick={handleSubmit(submitForm)}
                >
                    Save/Acknowledge
						</StyledButton>
                <StyledButton
                    className={classes.button}
                    variant="outlined"
                    color="primary"
                    onClick={() => newDS1983()}
                >
                    Reset
						</StyledButton>
                <StyledButton
                    className={classes.button}
                    variant="outlined"
                    color="primary"
                    onClick={onCancel}>
                    Cancel
					</StyledButton>
            </div>
        )
    };

    const renderMenu = () => {
        if (!dsList) { return <div>still working ...</div> };
        return dsList.map(oneChoice => {
            return <MenuItem key={oneChoice.Id.toString()} value={oneChoice.Description}>{oneChoice.Description}</MenuItem>;
        });
    };

	const { register, handleSubmit, errors, setError } = useForm<FormData>();

	const { control } = useForm({ defaultValues });

	let eventCurrentTarget: any = null;

	function fIsAlreadyUsedTktRange(aDSform: string, LowHigh: string, TgtVal: string): boolean {
		let rdata: UsedTicketRange[] | undefined = undefined;
		let pIsAlreadyUsedTktRange: boolean = false;

		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("LowHigh", LowHigh);
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("TgtVal", TgtVal);
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("SummonsFromNumberState", SummonsFromNumberState);
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("SummonsToNumberState", SummonsToNumberState);
		if (LowHigh === "Low" ? SummonsToNumberState > 0 : SummonsFromNumberState > 0) {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Test a ticket range");
			(async () => {
				const tryThisRange = (
					{
						ECBSummonsNumberFrom: LowHigh === "Low" ? Number.parseInt(TgtVal) : SummonsFromNumberState,
						ECBSummonsNumberTo: LowHigh === "High" ? Number.parseInt(TgtVal)
							: (Number.parseInt(TgtVal)
								+ SummonsCountState - 1).toString(),
						DSform: aDSform,
						ReceiptNumber: "",
					}
				);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("tryThisRange");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(IsAlreadyUsedTktRangeState);
				pIsAlreadyUsedTktRange = false;
				const response = await allServices.PTTSgetServices.refUsedTicketNumbers(tryThisRange);
				// axios always returns an object with a "data" child
				rdata = await response.data;
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange)
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
				if (!rdata) {
					pIsAlreadyUsedTktRange = false;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ticket range is so far unused based on no rdata object.");
				}
				else if (!rdata.length ? true : rdata.length = 0) {
					pIsAlreadyUsedTktRange = false;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ticket range is so far unused based on empty [].");
				}
				else {
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("already used ticket range");
					pIsAlreadyUsedTktRange =
						tryThisRange.ECBSummonsNumberFrom > 0
							&& tryThisRange.ECBSummonsNumberTo > 0
							? true : false;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange and non-zeroes");
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(pIsAlreadyUsedTktRange);

					if (pIsAlreadyUsedTktRange) {
						let rdataLength: number = -1;
						let pReceiptNumber: string = "";
						if (!rdata![rdata.length - 1]) { // https://stackoverflow.com/questions/55190059/typescript-property-type-does-not-exist-on-type-never
							rdataLength = 0
							pReceiptNumber = " "
						}
						else {
							rdataLength = rdata.length;
							pReceiptNumber = " (" + rdata[0].ReceiptNumber + ") ";

							alert(rdataLength > 1
								? rdataLength.toString() + " DS-1983s already use summons numbers in this range"
									+ !pReceiptNumber ? "" : " e.g., " + pReceiptNumber + "."
								: " Existing DS-1983" + pReceiptNumber + "already uses summons numbers in this range.");
						}
					}
				}
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange at end of ASYNC");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(pIsAlreadyUsedTktRange);
				setIsAlreadyUsedTktRangeState(pIsAlreadyUsedTktRange);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("IsAlreadyUsedTktRangeState at end of ASYNC");
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(IsAlreadyUsedTktRangeState);
				return pIsAlreadyUsedTktRange;

			})();

		};
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsAlreadyUsedTktRange after ASYNC", pIsAlreadyUsedTktRange);
		return pIsAlreadyUsedTktRange;

	};

	useEffect(() => {
		(async () => {
			setLoading(true);
			const response = await allServices.PTTSgetServices.refSpecialDistricts(); // axios always returns an object with a "data" child
			const rdata: tSpecialDistrict[] = response.data;
			if (!rdata) {
				setLoading(true);
			}
			else {
				const rdataDesc: tSpecialDistrict[] = rdata.sort(function (a, b) {
					var nameA = !a.Description ? "" : a.Description.toUpperCase(); // ignore upper and lowercase
					var nameB = !b.Description ? "" : b.Description.toUpperCase(); // ignore upper and lowercase
					if (nameA < nameB) {
						return -1;
					}
					if (nameA > nameB) {
						return 1;
					}
					// names must be equal
					return 0;
				}).slice(0, 150); // ASC
				setdsList(rdataDesc);
				setLoading(false);
			}
		})();
	}, []);

	useEffect(() => {
		const bRslt: boolean = fIsAlreadyUsedTktRange("DS1983", "Low", SummonsFromNumberState.toString());
		setIsAlreadyUsedTktRangeState(bRslt);
	},
		[
			SummonsFromNumberState,
		]
	);

	useEffect(() => {
		if (parseInt(MessengerREFnoState) > 0) {
			if (MessengerREFnoState.toString().length === 7) {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("REF#", MessengerREFnoState);
				(async () => {
					setLoading(true);

					const response: any = await allServices.EISgetServices.EISemployeeDetailsRN(MessengerREFnoState); // axios always returns an object with a "data" child
					const rdata: any = response.data;
					if (!rdata) {
						setLoading(true);
					}
					else {
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
						if (PTTSfunctions.Commonfunctions2.isNotDefined(rdata.employeeModel)) {
							setMessengerNameState("Invalid REF#: " + MessengerREFnoState);
						}
						else {
							try {

								if (isNullOrUndefined(rdata.jobModel.dsnyInformation)) { throw "rdata.jobModel.dsnyInformation missing" };
								let eisPayCodeCorrelID: string = rdata.jobModel.dsnyInformation.payCodeId.correlation
								eisPayCodeCorrelID = eisPayCodeCorrelID.substring(eisPayCodeCorrelID.length - 4);
								setMessengerPayCodeState( // this is an integral part of the DS-1983 Receipt# that will be composed by UID
									// actual paycode may be different; real one needs a lookup using this CorrelationID string
									eisPayCodeCorrelID
								);
								setMessengerTitleState(rdata.jobModel.dsnyInformation.titleId.correlation);
								if (isNullOrUndefined(rdata.employeeModel)) { throw "rdata.employeeModel missing" };
								setMessengerNameState(rdata.employeeModel.names[0].firstName.substring(0, 1)
									+ ' ' + rdata.employeeModel.names[0].lastName);
								if (isNullOrUndefined(rdata.jobModel.badgeNumber)) { throw "rdata.jobModel.badgeNumber missing" };
								setBadgeNoState(isNullOrUndefined(rdata.jobModel.badgeNumber)
									? "NA"
									: rdata.jobModel.badgeNumber.badgeNumber);
							}
							catch(e){
								alert(e.toString() + " using (" + MessengerREFnoState  + ").");
								if (e.toString() === "rdata.jobModel.dsnyInformation missing") {
									setMessengerPayCodeState( // this is an integral part of the DS-1983 Receipt# that will be composed by UID
										// actual paycode may be different; real one needs a lookup using this CorrelationID string
										"UNKN"
									)
								};
								setMessengerTitleState("type it in later");
								setBadgeNoState("type one in later");
							}

						}
						setLoading(false);
					}
				})();

			}
			else {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("numeric but not 7 digits")
			}
		}
		else {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("part of Messenger's last name");
		};
	},
		[MessengerREFnoState]
	);

	useEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Distributor lookup useEffect triggered");
		if (parseInt(DistributorREFnoState) > 0) {
			if (DistributorREFnoState.toString().length === 7) {
				(async () => {
					setLoading(true);
					const response: any = await allServices.EISgetServices.EISemployeeDetailsRN(DistributorREFnoState); // axios always returns an object with a "data" child
					const rdata: any = response.data;
					if (!rdata) {
						setLoading(true);
					}
					else {
						 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
						if (PTTSfunctions.Commonfunctions2.isNotDefined(rdata.employeeModel)) {
							setDistributorNameState("Invalid REF#: " + DistributorREFnoState);
						}
						else {
							setDistributorNameState(rdata.employeeModel.names[0].firstName.substring(0,1) + ' ' + rdata.employeeModel.names[0].lastName);
                        }
						setLoading(false);
					}
				})();

			}
			else {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("numeric but not 7 digits")
			}
		}
		else {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("part of Distributor's last name");
		};
	},
		[DistributorREFnoState]
	);

	useEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Acknowledged By lookup useEffect triggered");
		if (parseInt(AcknowledgedByREFnoState) > 0) {
			if (AcknowledgedByREFnoState.toString().length === 7) {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("REF#", AcknowledgedByREFnoState);
				(async () => {
					setLoading(true);
					const response: any = await allServices.EISgetServices.EISemployeeDetailsRN(AcknowledgedByREFnoState); // axios always returns an object with a "data" child
					const rdata: any = response.data;
					if (!rdata) {
						setLoading(true);
					}
					else {
						if (PTTSfunctions.Commonfunctions2.isNotDefined(rdata.employeeModel)) {
							setAcknowledgedByState("Invalid REF#: " + AcknowledgedByState);
						}
						else {
							setAcknowledgedByState(rdata.employeeModel.names[0].firstName.substring(0, 1) + ' ' + rdata.employeeModel.names[0].lastName);
						}
						setLoading(false);
					}
				})();

			}
			else {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("numeric but not 7 digits")
			}
		}
		else {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("part of Acknowledged By's last name");
		};
	},
		[AcknowledgedByREFnoState]
	);

	if (!isNullOrUndefined(anECB603)) {
		setReceiptNumberState(isNullOrUndefined(anECB603) ? "new created after save" : anECB603.DS1983ReceiptNo)
		setSummonsFromNumberState(isNullOrUndefined(anECB603) ? 0 : anECB603.TicketNoFrom);
		setSummonsToNumberState(isNullOrUndefined(anECB603) ? 0 : anECB603.TicketNoTo);
	}

	if (isLoading) {
		return (<div>Loading ...</div>);
	}
	else {


		// onSubmit is not triggered by type=submit StyledButtons; need an explicit onClick for saving/updating
		return (
			<FormControl onSubmit={handleSubmit(submitForm)}>
				<Grid className={classes.formposition}>
					<Grid container className={classes.darkborder}>
						<Grid item xs={2} className={classes.styleforlogo}>
							<img src={PTTSfunctions.Logos.DSNYLogo.logoImg} alt="Sanitation Enforcement" />
						</Grid>
						<Grid item xs={9} className={classes.styleforformtitle}>
							<div className={classes.styleforformtitletext}>THE CITY OF NEW YORK Department of Sanitation</div>
							<div className={classes.styleforformtitletext}>ECB - SUMMONS DISTRIBUTION RECEIPT  DS 1983 (4-99)</div>
							<div className={classes.styleforformtitletext}>RECEIPT NO:&nbsp;
							<div className={classes.styleforformreceipttextred}>{ReceiptNumberState}</div></div>
							<div className={classes.styleforformtitletext}>BOX NO:&nbsp;
								<input type="number" onChange={(e) => handleOnChange(e)}
									name="fldBoxNumber"
									ref={register({
										required: { value: true, message: "PTTS expects a box number." },
										min: {
											value: 1,
											message: "Too small.  Which box number did you deliver?",
										},
									})}
									defaultValue={BoxNumberState}
								/>
								<div>&nbsp;</div>
								<ErrorMessage
									errors={errors}
									name="fldBoxNumber"
									as={<ErrorMessageContainer />}
								/>
							</div>
						</Grid>
					</Grid>
					<div>&nbsp;</div>
					<div>&nbsp;</div>
					<Grid container>
						<Grid item xs={12}>
							<ErrorSummary errors={errors} />
							{IsAlreadyUsedTktRangeState ? <div>Existing DS-1983(s) already claims tickets between {SummonsFromNumberState} and {SummonsToNumberState}.</div> : <div>&nbsp;</div>}
						</Grid>
						<Grid item xs={12}>
							{renderActionButtons()}
						</Grid>
					</Grid>
				</Grid>
				<Grid className={classes.formposition}>
					<Grid container className={classes.darkborder}>
						<Grid item xs={12} className={classes.stylefortopacknowledgement}>
							<div className={classes.stylefortopacknowledgementtext}>
								<input className={classes.stylefortopacknowledgementtext} type="text"
									onChange={(e) => handleOnChange(e)} name="fldAcknowledgedByREFNO"
									defaultValue={AcknowledgedByState}
									placeholder="REF# w/zeroes"
									ref={register({
										required: { value: true, message: "PTTS expects a reference number for the person acknowledging." },
										pattern: {
											value: /^[A-Za-z]|([\w]+['][\w]+)|^\d{7}$/,
											message: "Acknowledged by: Either seven digit REF# or first-initial then last name."
										}
									})}
								/> acknowledged receipt of
								<input className={classes.stylefortopacknowledgementtext} type="number"
									onChange={(e) => handleOnChange(e)} name="fldSummonsCount"
									value={SummonsCountState}
								/>&nbsp;(ECB) Notice of Violations
								<div>&nbsp;</div>
								<ErrorMessage
									errors={errors}
									name="fldSummonsCount"
									as={<ErrorMessageContainer />}
								/>
							</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={5} className={classes.styleforticketrange}>
							<div className={classes.styleforticketrangetext}>
								Numbered from:&nbsp;
								<input className={classes.styleforticketrangetext} type="number"
									onChange={(e) => handleOnChange(e)} name="fldSummonsFromNumber"
									ref={register({
										required: { value: true, message: "PTTS expects the first summons number (eight digits)." },
										min: {
											value: 10000000,
											message: "Too small.  Summones numbers have eight digits starting with 1 or 2."
										},
										max: {
											value: 29999999,
											message: "Too big.  Summones numbers have eight digits starting with 1 or 2."
										},
										validate: (fldVal) => !fIsAlreadyUsedTktRange("DS1983", "Low", fldVal),
												// true means validation is OKay, value is proper
									})}
									defaultValue={SummonsFromNumberState}
								/>
								<div>&nbsp;</div>
								<ErrorMessage
									errors={errors}
									name="fldSummonsFromNumber"
									as={<ErrorMessageContainer />}
								/>
							</div>
						</Grid>
						<Grid item xs={5} className={classes.styleforticketrange}>
							<div className={classes.styleforticketrangetextTO}>
								&nbsp;to:&nbsp;{SummonsToNumberState}
							</div>
						</Grid>
					</Grid>
					<Grid item xs={12} className={classes.darkborder}>
						<Grid className={classes.stylefortopacknowledgement} >
							<div className={classes.stylefortopacknowledgementtext}>
								<label className={classes.stylefortopacknowledgementtext}>for delivery to&nbsp;</label>
								<Controller
									className={classes.stylefortopdistributionrowtextwide} 
									as={
										<Select
											value={BoroughState ? BoroughState : " "}
											onMouseDown={handleDropDownChange}
											name="fldBorough"
											ref={register({
												required: { value: true, message: "You must pick a location you delivered to." },
												minLength: {value: 3, message: "You have to pick a location you delivered to."}
											})}
										>
											{renderMenu()}
										</Select>
									}
									name="Select"
									id="SelectDistrict"
									control={control}
									defaultValue={BoroughState}
								/>
								<div>&nbsp;</div>
								<ErrorMessage
									errors={errors}
									name="fldBorough"
									as={<ErrorMessageContainer />}
									/>
							</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={4}>
							<Grid container>
								<Grid item xs={12} >

									<div className={classes.stylefortopdistributionrow} >DISTRIBUTOR'S NAME</div>
									<div >
										<input className={classes.stylefortopdistributionrowtextwide} type="text"
											onChange={(e) => handleOnChange(e)} name="fldDistributorNameREFNO"
											placeholder="REF# w/zeroes"
											ref={register({
												required: { value: true, message: "PTTS expects a reference number for the Distributor." },
												pattern: {
													value: /^[A-Za-z]|([\w]+['][\w]+)|^\d{7}$/,
													message: "Distributor: Enter a seven digit REF# so PTTS can look up employee information."
												}
											})}
											defaultValue={DistributorNameState}
										/>
									</div>
								</Grid>
								<Grid item xs={12} >
									<div >SIGNATURE</div>
								</Grid>
							</Grid>
						</Grid>
						<Grid item xs={2}>
							<div >DATE</div>
							<div >
								<input className={classes.stylefortopdistributionrowtextdate}
									type="date"
									onChange={(e) => handleOnChange(e)}
									name="fldDistributorSignedDate"
									ref={register}
									value={PTTSfunctions.Commonfunctions.formatDateSlashes((DistributorSignedDateState))}
								/>
							</div>
						</Grid>
						<Grid item xs={4}  >
							<Grid container>
								<Grid item xs={12}>
									<div className={classes.stylefortopdistributionrow}>MESSENGERS NAME</div>
									<div className={classes.thinborderLeft} >
										<input className={classes.stylefortopdistributionrowtextwide}
											type="text"
											placeholder="REF# w/zeroes"
											onChange={(e) => handleOnChange(e)} name="fldMessengerNameREFNO"
											ref={register({
												required: { value: true, message: "PTTS expects a reference number for the Messenger." },
												pattern: {
													value: /^[A-Za-z]|([\w]+['][\w]+)|^\d{7}$/,
													message: "Messenger: Enter a seven digit REF# so PTTS can look up employee information."
												}
											})}
											defaultValue={MessengerNameState}
										/>
										<div>&nbsp;</div>
										<ErrorMessage
											errors={errors}
											name="fldMessengerName"
											as={<ErrorMessageContainer />}
										/>
									</div>
								</Grid>
								<Grid container className={classes.thinborderTop}>
									<Grid item xs={6}>
										<div >SIGNATURE</div>
									</Grid>
									<Grid item xs={6} className={classes.thinborderLeft}>
										<div className={classes.stylefortopdistributionrow}>TITLE</div>
										<div className={classes.thinborderLeft}>
											<div className={classes.stylefortopdistributionrowtext}>
												{MessengerTitleState}
											</div>
										</div>
									</Grid>
								</Grid>

							</Grid>
						</Grid>
						<Grid item xs={2}>
							<Grid container>
								<Grid item xs={12} className={classes.thinborderLeft}>
									<div className={classes.stylefortopdistributionrow}>BADGE NO</div>
									<div className={classes.thinborderLeft}>
										<div className={classes.stylefortopdistributionrowtext}>{BadgeNoState}
										</div>
									</div>
								</Grid>
								<Grid item xs={12} className={classes.thinborderTop}>
									<div className={classes.stylefortopdistributionrow} >DATE</div>
									<div className={classes.thinborderLeft}>
										<input className={classes.stylefortopdistributionrowtextdate}
											type="date"
											onChange={(e) => handleOnChange(e)} name="fldMessengerSignedDate"
											ref={register}
											value={PTTSfunctions.Commonfunctions.formatDateSlashes((MessengerSignedDateState))}
										/>
									</div>
								</Grid>
							</Grid>
						</Grid>
					</Grid>
					<Grid item xs={12} className={classes.darkborder}>
						<div className={classes.stylefortopacknowledgementtext}>
							
							<div className={classes.stylefortopacknowledgementtext}>
								I ({ AcknowledgedByState }) am in possession of { SummonsCountState } (ECB) Notices, box #{BoxNumberState}
							</div>
							<StyledButton
								className={classes.button}
								variant="outlined"
								color="primary"
								type="submit"
								onClick={handleSubmit(submitForm)}
							>
								Acknowledge
							</StyledButton>
							<StyledButton
								className={classes.button}
								variant="outlined"
								color="secondary"
								onClick={onCancel}>
								Send back to Distributor
						</StyledButton>
						</div>
					</Grid>
					<Grid item xs={12}>
						{renderActionButtons()}
					</Grid>
				</Grid>

			</FormControl>
)
	}

};


const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5,
		marginTop: 0
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const useStyles = makeStyles({
	root: {
		flexGrow: 1,
		height: "100%",
		paddingLeft: 15,
		paddingRight: 15,

	},
	reactselect: {
		maxWidth: "calc(100% - 20px)",
		marginLeft: 10,
		marginTop: 0,
		fontSize: 12,
		'.control': {
			height: 25
		}
	},
	formposition: {
		marginLeft: 280,
	},
	darkborder: {
		borderWidth: 3,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderTop: {
		borderTopWidth: 2,
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderLeft: {
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderTopWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	styleforlogo: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforsubmit: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforformtitle: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
	},
	styleforformtitletext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "black",
		minWidth: 500,
	},
	styleforformorder: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "center",
	},
	styleforformordertext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "center",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 500,
		color: "black",
	},
	styleforticketrange: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
	},
	styleforticketrangetextTO: {
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "flex",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
	},
	stylefortopacknowledgement: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	stylefortopacknowledgementtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 250,
		minWidth: 130
	},
	stylefortopdistributionrow: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
		maxWidth: 300,
	},
	stylefortopdistributionrowtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80
	},
	stylefortopdistributionrowtextwide: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		minWidth: 300,
		maxWidth: 600,
	},
	stylefortopdistributionrowtextdate: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 5,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		minWidth: 170,
		maxWidth: 210,
	},
	styleforformreceipt: {
		verticalAlign: "center",
		borderTopStyle: "none",
	},
	styleforformreceipttext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		display: "inline",
		marginLeft: 10,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
		textAlign: "center",
	},
	styleforformreceipttextred: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "red",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
	},
	button: {
		marginTop: 20,
	},

	formstyle: {
		maxWidth: "100%",
		padding: 0,
	},

	input: {
		display: "block",
		borderRadius: 4,
		border: "1px solid",
		padding: "10px 15px",
		marginBottom: 10,
		fontSize: 14,
		maxWidth: 400,
		minWidth: 200,
	},
	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginBottom: 13,
		marginTop: 20,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
	},
	buttonSubmit: {
		background: "#0e8c1b",
		color: "white",
		border: "none",
		marginTop: 20,
		padding: 20,
		fontSize: 16,
		fontWeight: 100,
		width: 200,
	}
});
