﻿import { DS1983Action, DS1983Actions, DS1983 } from "../model";

function fAddDS1983(oDS1983: DS1983): DS1983Action {
	return {
		type: DS1983Actions.ADD_DS1983,
		payload: oDS1983,
	};
}

export function fUptDS1983(oDS1983: DS1983): DS1983Action {
	return {
		type: DS1983Actions.UPDATE_DS1983,
		payload: oDS1983,
	};
}


function fCompleteDS1983(DS1983Id: number, dispatchRef: any) {
	return  dispatchRef({ type: DS1983Actions.COMPLETE_DS1983, payload: DS1983Id });
}

function fUncompleteDS1983(DS1983Id: number): DS1983Action {
	return {
		type: DS1983Actions.REVERT2INCOMPLETE_DS1983,
		payload: DS1983Id,
	};
}

function fDS1983deleteTodo(DS1983Id: number): DS1983Action {
	return {
		type: DS1983Actions.DELETE_DS1983,
		payload: DS1983Id,
	};
}

export const allActions = {
	addDS1983: fAddDS1983,
	uptDS1983: fUptDS1983,
	completeDS1983: fCompleteDS1983,
	uncompleteDS1983: fUncompleteDS1983,
	DS1983deleteTodo: fDS1983deleteTodo
}