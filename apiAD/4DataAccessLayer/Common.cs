﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.IO;
using System.Web;
using Microsoft.Extensions.Configuration;
// using Microsoft.IdentityModel.Protocols;

namespace ONB.DataAccess
{
    public class Common
    {
        private static IConfigurationRoot Configuration
        {
            get; set;
        }
        //public SqlConnection OpenConnection()
        //{
        //    try
        //    {
        //        //string strConn = "Data Source=MSDWVSDSNYSQL01.csc.nycnet\\DEVE08RI1;Initial Catalog=DonateNYC; Persist Security Info=True; User ID=sl_devadmin; Password=dsny1234; MultipleActiveResultSets=True; pooling=true; connection lifetime=120; max pool size=200";
        //        string strConn = Configuration["connString"].ToString();
        //        SqlConnection conn = new SqlConnection(strConn);
        //        conn.Open();
        //        return conn;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public SqlConnection CMSOpenConnection()
        //{
        //    try
        //    {
        //        string strConn = Configuration["CMSconnString"].ToString();
        //        SqlConnection conn = new SqlConnection(strConn);
        //        conn.Open();
        //        return conn;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static DataSet ExecuteStoredProcedureDataSet( string ProcedureName, SqlParameter[] Parms, SqlConnection conn )
        {
            DataSet ds = new DataSet();

            SqlCommand sqlCommand = null;

            try
            {
                sqlCommand = new SqlCommand
                {
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = ProcedureName
                };

                // Clearing previous Parameters if present
                sqlCommand.Parameters.Clear();

                // Looping through the parameters
                if ((Parms != null))
                {
                    foreach (SqlParameter sqlParm in Parms)
                    {
                        sqlCommand.Parameters.Add( sqlParm );
                    }
                }
                // Execute                        
                SqlDataAdapter da = new SqlDataAdapter( sqlCommand );
                da.Fill( ds );
                return ds;

            }
            catch (SqlException sqlEx)
            {
                string val = string.Empty;
                val = GetParameterValueString( Parms );
                throw new System.Exception( sqlEx.Message + " (see: " + sqlCommand.CommandText + ")" + " (Parameters: " + val + ")" );
            }
            catch (System.Exception ex)
            {
                throw new System.Exception( ex.Message );
            }
        }

        /// <summary>
        /// ExecuteStoredProcedureDataTable
        /// </summary>
        /// <param name="ProcedureName"></param>
        /// <param name="Parms"></param>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static DataTable ExecuteStoredProcedureDataTable( string ProcedureName, SqlParameter[] Parms, SqlConnection conn )
        {
            DataTable dt = new DataTable();
            SqlCommand sqlCommand = null;
            try
            {
                sqlCommand = new SqlCommand
                {
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = ProcedureName
                };
                sqlCommand.Parameters.Clear();
                if ((Parms != null))
                {
                    foreach (SqlParameter sqlParm in Parms)
                    {
                        sqlCommand.Parameters.Add( sqlParm );
                    }
                }
                SqlDataAdapter da = new SqlDataAdapter( sqlCommand );
                da.Fill( dt );
                return dt;
            }
            catch (SqlException sqlEx)
            {
                string val = string.Empty;
                val = GetParameterValueString( Parms );
                throw new System.Exception( sqlEx.Message + " (see: " + sqlCommand.CommandText + ")" + " (Parameters: " + val + ")" );
            }
            catch (System.Exception ex)
            {
                throw new System.Exception( ex.Message );
            }
        }
        public static string GetParameterValueString( SqlParameter[] param )
        {
            string value = string.Empty;

            try
            {
                if ((param != null))
                {
                    foreach (SqlParameter sqlParm in param)
                    {
                        value = value + sqlParm.ParameterName + ":" + sqlParm.Value + ";";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return value;
        }
        public static SqlDataReader ExecuteStoredProcedureReader( string ProcedureName, SqlParameter[] Parms, SqlConnection conn )
        {
            SqlCommand sqlCommand = null;
            SqlDataReader Reader = default( SqlDataReader );

            try
            {
                sqlCommand = new SqlCommand
                {
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = ProcedureName
                };

                // Clearing previous Parameters if present
                sqlCommand.Parameters.Clear();

                // Looping through the parameters
                if ((Parms != null))
                {
                    foreach (SqlParameter sqlParm in Parms)
                    {
                        sqlCommand.Parameters.Add( sqlParm );
                    }
                }
                // Execute                             
                Reader = sqlCommand.ExecuteReader();
                return Reader;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        private static object ExecuteScalar( string sql, ref SqlConnection conn )
        {
            try
            {
                SqlCommand sqlCommand = default( SqlCommand );
                object result = null;

                sqlCommand = new SqlCommand
                {
                    Connection = conn,
                    CommandType = CommandType.Text,
                    CommandText = sql
                };

                result = sqlCommand.ExecuteScalar();

                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string Encrypt( string strPassword )
        {
            string EncryptionKey = Configuration["EncryptionKey"];
            byte[] clearBytes = Encoding.Unicode.GetBytes( strPassword );
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes( EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 } );
                encryptor.Key = pdb.GetBytes( 32 );
                encryptor.IV = pdb.GetBytes( 16 );
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream( ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write ))
                    {
                        cs.Write( clearBytes, 0, clearBytes.Length );
                        cs.Close();
                    }
                    strPassword = Convert.ToBase64String( ms.ToArray() );
                }
            }
            return strPassword;
        }
        public string Decrypt( string strPassword )
        {
            string EncryptionKey = Configuration["EncryptionKey"];
            byte[] cipherBytes = Convert.FromBase64String( strPassword );
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes( EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 } );
                encryptor.Key = pdb.GetBytes( 32 );
                encryptor.IV = pdb.GetBytes( 16 );
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream( ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write ))
                    {
                        cs.Write( cipherBytes, 0, cipherBytes.Length );
                        cs.Close();
                    }
                    strPassword = Encoding.Unicode.GetString( ms.ToArray() );
                }
            }
            return strPassword;
        }
        public static void LogException( string msg )
        {
            string strConn = "Data Source=10.155.196.187\\DSNY_OnBoarding,49791;Initial Catalog=DSNY_OnBoarding; Persist Security Info=True; User ID=ONBUIonbuiONBUIonbui; Password=onbuiONBUIonbui; MultipleActiveResultSets=True; pooling=true; connection lifetime=120; max pool size=200";
            // string strConn = Configuration["connString"];


            var builder = new ConfigurationBuilder()
                .SetBasePath( Directory.GetCurrentDirectory() )
                .AddJsonFile( "appsettings.json" )
                .AddJsonFile( "appsettings.Development.json" );

            Configuration = builder.Build();

            // string strConn = Configuration["AppSettings:connString"];

            SqlConnection sqlConn = new SqlConnection( strConn );
            try
            {
                sqlConn.Open();
                SqlCommand sqlCmd = new SqlCommand( "dbo.SP_LogException", sqlConn )
                {
                    CommandType = CommandType.StoredProcedure
                };
                sqlCmd.Parameters.AddWithValue( "@ExceptionMsg", msg );
                sqlCmd.Parameters.AddWithValue( "@ExceptionType", "" );
                sqlCmd.Parameters.AddWithValue( "@StackTrace", "" );
                sqlCmd.ExecuteNonQuery();
            }
            catch (Exception exErr)
            {
                LogExceptiontoFile( exErr );
            }
            finally
            {
                sqlConn.Dispose();
            }
        }


        public static void LogException( Exception ex )
        {
            //string strConn = "Data Source=MSDWVSDSNYSQL01.csc.nycnet\\DEVE08RI1;Initial Catalog=DonateNYC; Persist Security Info=True; User ID=sl_devadmin; Password=dsny1234; MultipleActiveResultSets=True; pooling=true; connection lifetime=120; max pool size=200";
            //    string strConn = Configuration["connString"];
            string strConn = "Data Source=MSDWVA-DSNSCA02.dsnyad.nycnet\\DSNY_DEV;Initial Catalog=DonateNYC; Persist Security Info=True; User ID=PTTSpttsPTTSptts; Password=pttsPTTSptts; MultipleActiveResultSets=True; pooling=true; connection lifetime=120; max pool size=200";


            var builder = new ConfigurationBuilder()
                .SetBasePath( Directory.GetCurrentDirectory() )
                .AddJsonFile( "appsettings.json" )
                .AddJsonFile( "appsettings.Development.json" );

            Configuration = builder.Build();

            // string strConn = Configuration["AppSettings:connString"];

            SqlConnection sqlConn = new SqlConnection( strConn );
            try
            {
                sqlConn.Open();
                SqlCommand sqlCmd = new SqlCommand( "dbo.SP_LogException", sqlConn )
                {
                    CommandType = CommandType.StoredProcedure
                };
                if (ex.InnerException.Data.Count > 0)
                    sqlCmd.Parameters.AddWithValue( "@ExceptionMsg", ex.Message.ToString() + " | " + ex.InnerException.ToString() );
                else
                    sqlCmd.Parameters.AddWithValue( "@ExceptionMsg", ex.Message.ToString() );
                sqlCmd.Parameters.AddWithValue( "@ExceptionType", ex.GetType().Name.ToString() );
                sqlCmd.Parameters.AddWithValue( "@StackTrace", ex.StackTrace.ToString() );
                sqlCmd.ExecuteNonQuery();
            }
            catch (Exception exErr)
            {
                LogExceptiontoFile( exErr );
            }
            finally
            {
                sqlConn.Dispose();
            }
        }

        private void CreateDirectoryIfNotExists( string NewDirectory )
        {
            try
            {
                if (!Directory.Exists( NewDirectory ))
                {
                    Directory.CreateDirectory( NewDirectory );
                }
            }
            catch (IOException err)
            {
                throw new System.Exception( err.Message + " CreateDirectoryIfNotExists " + NewDirectory, err.InnerException );
            }
        }

        /*
        public string SaveMedia(int Id, HttpPostedFile file, string Mediafor)
        {
            try
            {
                string[] imageLocations = new string[] { };
                if (Mediafor == "Announcement")
                {
                    imageLocations = ConfigurationManager.AppSettings["AnnouncementMediaLocationKey"].Split(',');
                }

                string sourcePath = string.Empty;
                string targetPath = string.Empty;
                string filename = string.Empty;
                string storedfilename = string.Empty;

                for (int i = 0; i < imageLocations.Length; i++)
                {
                    string dirName = imageLocations[i] + Id.ToString();

                    CreateDirectoryIfNotExists(dirName);

                    if (i == 0)
                    {
                        sourcePath = dirName;
                        byte[] binaryWriteArray = new byte[file.InputStream.Length];
                        file.InputStream.Read(binaryWriteArray, 0, (int)file.InputStream.Length);

                        string postedfileName = file.FileName;
                        if (postedfileName.Contains("\\"))
                        {
                            postedfileName = Path.GetFileName(file.FileName);
                        }

                        string fileExtension = System.IO.Path.GetExtension(postedfileName);
                        string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(postedfileName);
                        filename = fileNameWithoutExtension + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + fileExtension;

                        FileStream objfilestream = new FileStream(System.IO.Path.Combine(dirName, filename), FileMode.Create, FileAccess.ReadWrite);
                        objfilestream.Write(binaryWriteArray, 0, binaryWriteArray.Length);
                        objfilestream.Close();
                    }
                    else if (i >= 1)
                    {
                        targetPath = dirName;
                        if (System.IO.Directory.Exists(sourcePath))
                        {
                            string sourceFile = System.IO.Path.Combine(sourcePath, filename);
                            string destFile = System.IO.Path.Combine(targetPath, filename);
                            System.IO.File.Copy(sourceFile, destFile, true);
                        }
                    }
                }
                return filename;
            }
            catch (Exception exErr)
            {
                throw new System.Exception(exErr.Message + " NewDirectory " + " See: DSNY.SP_SaveMedia ", exErr.InnerException);
            }
        }


    */

        /*
    public void DeleteMedia(int Id, string mediaName, string Mediafor)
    {
        try
        {
            string[] imageLocations = new string[] { };
            if (Mediafor == "Announcement")
            {
                imageLocations = ConfigurationManager.AppSettings["AnnouncementMediaLocationKey"].Split(',');
            }
            for (int i = 0; i < imageLocations.Length; i++)
            {
                string FileDirectory = imageLocations + Id.ToString() + "//";
                string FileToDelete = FileDirectory + mediaName.Replace("\"", "");
                if (File.Exists(FileToDelete))
                {
                    File.Delete(FileToDelete);
                }
            }
        }
        catch (Exception exErr)
        {
            throw new System.Exception(exErr.Message + "See: DeleteMedia(" + Id.ToString() + "," + mediaName + ")", exErr.InnerException);
        }
    }
    */
        public static void LogExceptiontoFile( Exception ex )
        {
            StreamWriter sw;
            FileStream fileStream;
            //DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;

            string logFilePath = Configuration["ErrorLogLocation"];
            logFilePath = logFilePath + System.DateTime.Now.ToString( "MMddyyyy_HHmmss" ) + "_ExcpLog" + "." + "txt";
            logFileInfo = new FileInfo( logFilePath );
            //logDirInfo = new DirectoryInfo( logFileInfo.DirectoryName );
            //if (!logDirInfo.Exists) logDirInfo.Create();
            try
            {
                fileStream = new FileStream( logFilePath, FileMode.Append );
            }
            catch 
            {
                fileStream = logFileInfo.Create();
            }

            sw = new StreamWriter( fileStream );
            sw.WriteLine( "********** {0} **********", DateTime.Now );
            if (ex.InnerException != null)
            {
                sw.Write( "Inner Exception Type: " );
                sw.WriteLine( ex.InnerException.GetType().ToString() );
                sw.Write( "Inner Exception: " );
                sw.WriteLine( ex.InnerException.Message );
                sw.Write( "Inner Source: " );
                sw.WriteLine( ex.InnerException.Source );
                if (ex.InnerException.StackTrace != null)
                {
                    sw.WriteLine( "Inner Stack Trace: " );
                    sw.WriteLine( ex.InnerException.StackTrace );
                }
            }
            sw.Write( "Exception Type: " );
            sw.WriteLine( ex.GetType().ToString() );
            sw.WriteLine( "Exception: " + ex.Message );
            sw.WriteLine( "Stack Trace: " );
            if (ex.StackTrace != null)
            {
                sw.WriteLine( ex.StackTrace );
                sw.WriteLine();
            }
            sw.Close();
        }

    }
}