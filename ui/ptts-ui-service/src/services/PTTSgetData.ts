﻿import axios from "axios";
import { PTTSfunctions } from "../components/basicComponents";
import {
    DS21HDRActions, DS21CardActions, tDS21Card, tOneNOV,
    UsedTicketRange
} from '../model';
import { dummies } from "../reducers";

// ----

function fusedTicketRange(tryThis: UsedTicketRange) {
/*   return (async () => {*/
        
    return axios.post(`${process.env.REACT_APP_PTTS20_API}/GetUsedTicketRange`, tryThis)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
/*    })*/
}

function fusedTicketRangeWithCallBack(tryThis: UsedTicketRange, callbackSuccess: any, callbackFail: any) {
    console.log("attempt axios with fusedTicketRangeWithCallBack");
    axios.post(`${process.env.REACT_APP_PTTS20_API}/GetUsedTicketRange`, tryThis)
        .then(function (response: any) {
            callbackSuccess(response);
        })
        .catch(function (error) {
            callbackFail(error);
        });
}
// ----

function fanECB603(ECBreceiptNo: string) {
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/GetECB603List/` + ECBreceiptNo)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function fecb603List() {
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/GetECB603List`)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

// ----

function fanDS1983(ECBreceiptNo: string) {
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS1983List/` + ECBreceiptNo)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function fDS1983List() {
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS1983List`)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

// ----
function fanDS1984withCallBack(DS1984receiptNo: string, callbackSuccess: any, callbackFail: any) {
    axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS1984List/` + DS1984receiptNo) // returns IList<DS21> from C#
        .then(function (response) {
            callbackSuccess(response);
        })
        .catch(function (error) {
            callbackFail(error);
        });
}

function fanDS1984(DS1984receiptNo: string) {
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS1984List/` + DS1984receiptNo)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function fDS1984List() {
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS1984List`)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function fDS1984sRelatedToOneDS1983List(DS1983receiptNo: string) {
    //(async () => {
    //    await
          return   axios.get(`${process.env.REACT_APP_PTTS20_API}/GetRelatedDS1984s/` + DS1983receiptNo)
            .then((response: any) => {
                return response;
            },
                (error: { toString: () => any; }) => {
                    return error;
                });
    //})
}

// ----


function fanDS1986(DS1986IdNo: string) {
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS1986BList/` + DS1986IdNo)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function fDS1986List() {
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS1986BList`)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function fDS1986problemList() {
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS1986BproblemList`)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });}

function fDS1704ReasonList(callbackSuccess: any, callbackFailure: any) {
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/ReferenceData/GetDS1704ReasonList`)
        .then((response: any) => {
            callbackSuccess(response);
        },
            (error: { toString: () => any; }) => {
                callbackFailure(error);
            });
}

function fDS1986sRelatedToOneDS1984List(DS1984receiptNo: string) {
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/GetRelatedDS1986s/` + DS1984receiptNo)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

// ----


async function fDS21HDRList(YYYYMMDD: string) { // every DS21 
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("started services fDS21HDRList");
    return await axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS21HDRListNewest/` + YYYYMMDD)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return null;
            });
}

function fDS21NOVList() { // Get every individual NOV from DS21 table 
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS21NOVList`)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

async function f25DS21NOVs(DS1984receiptNo: string) { // one DS1984 per DS21
    return await axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS21NOVList/` + DS1984receiptNo) // returns IList<DS21> from C#
        .then((response: any) => { 
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}


function foneNOV(ECBsummonsNumber: string) { // NOV numbers are unique
    return new Promise(function (resolve, reject) {
        axios.get(`${process.env.REACT_APP_PTTS20_API}/GetNOVdetail/` + ECBsummonsNumber) // returns IList<DS21> from C#
            .then((response: any) => {
                // resolve(response);
                PTTSfunctions.Commonfunctions2.sleep(500).then(() => { resolve(response) })
            },
                (error: { toString: () => any; }) => {
                    reject(error);
                });
    })
}

function foneNOVwithCallBack(ECBsummonsNumber: string, callbackSuccess: any, callbackFail: any) {
    axios.get(`${process.env.REACT_APP_PTTS20_API}/GetNOVdetail/` + ECBsummonsNumber) // returns IList<DS21> from C#
        .then(function (response) {
            callbackSuccess(response);
        })
        .catch(function (error) {
            callbackFail(error);
        });
}

function foneDS1704(ECBsummonsNumber: string) { // one DS1704A4 per NOV
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS1704List/` + ECBsummonsNumber) // returns IList<DS21> from C#
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function foneDS1704withCallBack(ECBsummonsNumber: string, callbackSuccess: any, callbackFail: any) { // one DS1704A4 per NOV
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/GetOneDS1704/` + ECBsummonsNumber) // returns <DS1704> from C#
        .then(function (response) {
            callbackSuccess(response);
        })
        .catch(function (error) {
            callbackFail(error);
        });
}

function fDS21HDRRelatedToOneDS1984List(dispatchRef: any, DS1984receiptNo: string): boolean {
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("started fDS21HDRRelatedToOneDS1984List");
    let bRslt: boolean = false;
    axios.get(`${process.env.REACT_APP_PTTS20_API}/GetOneDS21HDR/` + DS1984receiptNo)
        .then((response: any) => {
            dispatchRef(success(response.data));
            if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("finished fDS21HDRRelatedToOneDS1984List");
            bRslt = true;
        },
            (error: { toString: () => any; }) => {
                dispatchRef(failure(error.toString()));
                if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("error fDS21HDRRelatedToOneDS1984List");
                bRslt = false;
            });
    return bRslt;

    function success(rdata: any) { return { type: DS21HDRActions.aDS21HDRROWSadd, rdata } };
    function failure(error: any) { return { type: DS21HDRActions.HDRmsg, error } }

}

function fDS21HDRRelatedToOneNOV(dispatchRef: any, NOVnumber: string): boolean {
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("started fDS21HDRRelatedToOneNOV");
    let bRslt: boolean = false;
    axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS21HDRforThisNOV/` + NOVnumber)
        .then((response: any) => {
            dispatchRef(success(response.data));
            if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("finished fDS21HDRRelatedToOneNOV");
            bRslt = true;
        },
            (error: { toString: () => any; }) => {
                dispatchRef(failure(error.toString()));
                if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("error fDS21HDRRelatedToOneNOV");
                bRslt = false;
            });
    return bRslt;

    function success(rdata: any) { return { type: DS21HDRActions.aDS21HDRROWSadd, rdata } };
    function failure(error: any) { return { type: DS21HDRActions.HDRmsg, error } }

}

// ----

function fSpecialDistrictsList() {
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/ReferenceData/GetSpecialDistricts`)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}


function fViolationCodes() {
    //(async () => {
    //    await
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/ReferenceData/GetViolationCodes`)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
    //})
}

function fTicketStatusOptions() {
    //(async () => {
    //    await
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/ReferenceData/GetTicketStatusOptions`)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
    //})
}

function fTicketStatusOptionsWithCallBack(callbackSuccess: any, callbackFail: any) {
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/ReferenceData/GetTicketStatusOptions`)
            .then(function (response: any) {
                callbackSuccess(response);
            })
        .catch(function (error) {
            callbackFail(error);
        });
}

function fViolationTypes() {
    //(async () => {
    //    await
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/ReferenceData/GetViolationTypes`)
        .then((response: any) => {
            return response;
            
        },
            (error: { toString: () => any; }) => {
                return error;
            });
    //})
}

export const allServices: any = {
    anECB603: fanECB603,
    ecb603List: fecb603List,
    anDS1983: fanDS1983,
    DS1983List: fDS1983List,

    anDS1984: fanDS1984,
    anDS1984withCallBack: fanDS1984withCallBack,
    DS1984List: fDS1984List,
    DS1984RelatedToOneDS1983List: fDS1984sRelatedToOneDS1983List,

    DS21HDRList: fDS21HDRList, // every DS21 for this user and in his PAYC and AD Groups
    DS21HDRRelatedToOneDS1984List: fDS21HDRRelatedToOneDS1984List, // a single DS21 header record

    oneDS21Card: f25DS21NOVs, // get 25 NOVs for a card given a DS1984 receipt number
    oneNOV: foneNOV, // get details for a single NOV from one row of table DS21
    oneNOVwithCallBack: foneNOVwithCallBack, // get details for a single NOV return to callback function name when ready
    DS21NOVList: fDS21NOVList, // get every NOV from table DS21
    oneDS1704: foneDS1704withCallBack, // foneDS1704,

    anDS1986: fanDS1986,
    DS1986List: fDS1986List,
    refProblemList: fDS1986problemList,
    DS1986RelatedToOneDS1984List: fDS1986sRelatedToOneDS1984List,

    refReasonList: fDS1704ReasonList,

    refSpecialDistricts: fSpecialDistrictsList,
    refUsedTicketNumbers: fusedTicketRange,
    refUsedTicketNumbersWithCallback: fusedTicketRangeWithCallBack,
    refViolationCodes: fViolationCodes,
    refViolationTypes: fViolationTypes,
    refTicketStatusOptions: fTicketStatusOptions,
    refTicketStatusOptionsWithCallback: fTicketStatusOptionsWithCallBack
};

