import { Button, Grid, Typography } from "@material-ui/core";
import { Theme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { ComplaintDialog } from "../components/complaint";
import { ComplaintTable } from "../components";
import { withStyles } from '@material-ui/core/styles';

const StyledButton = withStyles({
	root: {
		background: '#2b995f',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '15 15 15 15px',
	},
	label: {
		color: 'white',		
	},
})(Button);

export function HomePage() {
	const classes = useStyles();
	const [open, setOpen] = React.useState(false);

	const handleClose = () => {
		setOpen(false);
	};

	const handleAddComplaint = () => {
		setOpen(true);
	};

	return (
		<Grid container className={classes.root}>
			<ComplaintDialog open={open} onClose={handleClose}/>
			<Grid item xs={6}>
				<Typography variant="h4" gutterBottom>
					Complaint List
				</Typography>
			</Grid>
			<Grid item xs={6}>
				<div className={classes.buttonContainer}>					
					<StyledButton
						className={classes.button}
						variant="contained"
						color="secondary"
						onClick={handleAddComplaint}
					>
						+ Add Complaint
					</StyledButton>
				</div>
			</Grid>
			<Grid item xs={12}>
				<ComplaintTable />
			</Grid>
		</Grid>
	);
}

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: 20,
		[theme.breakpoints.down("md")]: {
			paddingTop: 50,
			paddingLeft: 15,
			paddingRight: 15,
		},
	},

	buttonContainer: {
		width: "100%",
		display: "flex",
		justifyContent: "flex-end",
	},

	button: {
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	dialog: {
		minHeight: 500
	}
}));
