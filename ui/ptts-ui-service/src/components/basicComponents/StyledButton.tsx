﻿import { Button } from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';

export const StyledButton = () => withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

export const StyledButtonGreen = withStyles({
	root: {
		background: '#2b995f', //'#287734',		
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

