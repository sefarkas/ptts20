﻿import * as React from "react";
import { ErrorMessage } from "@hookform/error-message";
import { FieldErrors } from "react-hook-form"; // , FormProvider


// https://codesandbox.io/s/react-hook-form-errors-sx9ol?file=/src/App.tsx:1904-1932

export type ErrorSummaryProps<T> = {
	errors: FieldErrors<T>;
};

export type ErrorMessageContainerProps = {
	children?: React.ReactNode;
};

// https://codesandbox.io/s/react-hook-form-errors-sx9ol?file=/src/App.tsx:1904-1932

// ...

// https://codesandbox.io/s/react-hook-form-errors-sx9ol?file=/src/App.tsx:1904-1932

export function ErrorSummary<T>({ errors }: ErrorSummaryProps<T>) {
	if (Object.keys(errors).length === 0) {
		return null;
	}
	return (
		<div className="error-summary">
			{Object.keys(errors).map((fieldName) => (
				<ErrorMessage errors={errors} name={fieldName as any} as="div" key={fieldName} />
			))}
		</div>
	);
}

export const ErrorMessageContainer = ({ children }: ErrorMessageContainerProps) => (
	<span className="error">{children}</span>
);

// https://codesandbox.io/s/react-hook-form-errors-sx9ol?file=/src/App.tsx:1904-1932

