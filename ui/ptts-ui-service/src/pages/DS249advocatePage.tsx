﻿import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useEffect, useState, useReducer } from "react";
import Background from '../assets/images/DS249_1204.png';

function reducer(state: any, action: { atype: string, thepayload: string }) {
	const { atype, thepayload } = action;
	return { ...state, [atype]: thepayload };
}

const useStyles = makeStyles({
	root: {
		flexGrow: 1,
		height: "100%",
		paddingLeft: 15,
		paddingRight: 15,
		opacity: 60,
		backgroundImage: `url( ${Background} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '820px 1030px',
		backgroundPosition: 'left top',
	},
	orGate1: {
		backgroundColor: 'cyan',
		color: 'white',
		height: '40px',
		width: '26px',
		borderTopLeftRadius: '13px',
		borderTopRightRadius: '13px',
		textAlign: 'center',
		textAnchor: 'middle',
	},
	orGate2: {
		backgroundColor: 'white',
		marginTop: '-20px',
		height: '20px',
		width: '26px',
		borderTopLeftRadius: '13px',
		borderTopRightRadius: '13px',
	},
	orGate3: {
		backgroundColor: 'black',
		marginTop: '-20px',
		marginLeft: '12px',
		width: '2px',
		borderLeftColor: 'black',
		borderLeftWidth: '2px',
	},
	andGate1: {
		backgroundColor: 'cyan',
		color: 'white',
		height: '30px',
		width: '26px',
		borderTopLeftRadius: '13px',
		borderTopRightRadius: '13px',
		textAlign: 'center',
		textAnchor: 'middle',
	},
	andGate2: {
		backgroundColor: 'black',
		marginLeft: '12px',
		width: '2px',
		borderLeftColor: 'black',
		borderLeftWidth: '2px',
	},
	dateIssued: {
		marginTop: 25,
		marginLeft: 500,
		width: 100,
	},
	borough: {
		marginTop: 20,
		marginLeft: 30,
		width: 100,
	},
	district: {
		marginTop: -20,
		marginLeft: 260,
		width: 100,
	},
	fullname: {
		marginLeft: 30,
		marginTop: 10,
		width: 200,
	},
	civilServiceTitle: {
		marginLeft: 520,
		marginTop: -20,
		width: 100,
	},
	badge: {
		marginLeft: 400,
		marginTop: -20,
		width: 100,
	},
	referenceno: {
		marginLeft: 620,
		marginTop: -20,
		width: 100,
	},
	dob: {
		marginLeft: 680,
		marginTop: -20,
		width: 80,
	},
	homeadd: {
		marginLeft: 30,
		marginTop: 10,
		width: 350,
	},
	apt: {
		marginLeft: 400,
		marginTop: -20,
		width: 100,
	},
	homeboro: {
		marginLeft: 530,
		marginTop: -20,
		width: 100,
	},
	homezip: {
		marginLeft: 680,
		marginTop: -20,
		width: 80,
	},
	dateIncident: {
		marginLeft: 30,
		marginTop: 10,
		width: 100,
	},
	districtIncident: {
		marginLeft: 200,
		marginTop: -20,
		width: 100,
	},
	dateAppointment: {
		marginLeft: 680,
		marginTop: -20,
		width: 80,
	},
	violationCharges: {
		marginLeft: 30,
		marginTop: 10,
		width: 710,
		height: 110,
	},
	violationDescription: {
		marginLeft: 30,
		marginTop: 20,
		width: 710,
		height: 360,
	},
	complaintantName: {
		marginLeft: 330,
		marginTop: 10,
		width: 150,
	},
	dateComplaintantSigned: {
		marginLeft: 680,
		marginTop: -20,
		width: 80,
	},
	dateRespondantSigned: {
		marginLeft: 680,
		marginTop: 15,
		width: 80,
	},
	respondantActionLeft: {
		marginLeft: 35,
		marginTop: 20,
		width: 40,
	},
	respondantActionRight: {
		marginLeft: 405,
		marginTop: -20,
		width: 40,
	},
	servedByLeft: {
		marginLeft: 150,
		marginTop: -10,
		width: 120,
	},
	servedByRight: {
		marginLeft: 505,
		marginTop: -20,
		width: 120,
	},
	dateServedRight: {
		marginLeft: 660,
		marginTop: -20,
		width: 100,
	},
	dateServedLeft: {
		marginLeft: 280,
		marginTop: -20,
		width: 100,
	},
	complaintantPrinted: {
		marginLeft: 30,
		marginTop: 15,
		width: 150,
	},
	complaintantLocation: {
		marginLeft: 30,
		marginTop: 10,
		width: 320,
	},
	complaintantTitle: {
		marginLeft: 420,
		marginTop: -20,
		width: 330,
	},
	witness1: {
		marginLeft: 420,
		marginTop: 10,
		width: 330,
	},
	suspendedBy: {
		marginLeft: 30,
		marginTop: 12,
		width: 220,
	},
	dateOfSuspendedOrder: {
		marginLeft: 280,
		marginTop: -20,
		width: 100,
	},
	suspensionLiftedBy: {
		marginLeft: 30,
		marginTop: 10,
		width: 220,
	},
	dateSuspensionLifted: {
		marginLeft: 280,
		marginTop: -20,
		width: 100,
	},
	witness2: {
		marginLeft: 420,
		marginTop: -20,
		width: 330,
	},
	witness3: {
		marginLeft: 420,
		marginTop: -20,
		width: 330,
	}	


});

export function DS249advocateCopy() {
	const classes = useStyles();
	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
		"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

	const initialState: any = {
		curYear: 0,
		curDay: "",
		curMonth: "",
		codeCat: "",
		complaint: {},
	};

	const [state, dispatch] = useReducer(reducer, initialState);
	// eslint-disable-next-line
	const [indexNo, setIndexNo] = useState(25400); //  IndexNo2 isa string representation of the IndexNo; however there can be more than one row for each distinct IndexNo2

	useEffect(() => {
		let dateObj = new Date();
		dispatch({ atype: "curMonth", thepayload: monthNames[dateObj.getMonth()] });
		dispatch({ atype: "curDay", thepayload: String(dateObj.getDate()).padStart(2, '0') });
		dispatch({ atype: "curYear", thepayload: dateObj.getFullYear().toString() });
	}, [monthNames]);

	useEffect(() => {
		const { curMonth, curDay, curYear } = state;
		setdteOutput(curMonth + '\n' + curDay + ',' + curYear); // need all three to be current values
	}, [state]);

	const [dteOutput, setdteOutput] = useState("");


	// const callAPIx: any = async (anIndexNo: number) => {
	//	const response1: any = await fetch('https://localhost:44333/api/DS249data');
	//	const data1: any = await response1.json();
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ data1 });
	//	const item = data1[7];
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ item });
	//	await setCodeCat(item);
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ codeCat });  // still null at this point ???

	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ IndexNo });
	//	const response2: any = await fetch('https://localhost:44333/IndexNumb?IndexNumb=' + anIndexNo);
	//	const data2: any = await response2.json();
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ data2 });
	//	await setComplaint(data2);
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ complaint }); // still null at this point ???
	// };

	useEffect(() => {
		const callAPI: any = (anyIndexNo: number) => {
			return fetch('https://localhost:44333/IndexNumb?IndexNumb=' + anyIndexNo.toString())
				.then(res => {
					return res.json();
				})
				.then(rcvd => {
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ rcvd });
					const { complaint } = state;
					if (rcvd !== complaint) {
						dispatch({ atype: "complaint", thepayload: rcvd });
					}
				})
				.then(() => {
					return fetch('https://localhost:44333/api/DS249data')
				})
				.then(somedata => {
					return somedata.json();
				})
				.then(rcvd2 => {
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ rcvd2 });
					const item = rcvd2[7];
					const { codeCat } = state;
					if (item !== codeCat) {
						dispatch({ atype: "codeCat", thepayload: item });
					}
				})
				.catch(err => {
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ err });
				});
		};
		callAPI(indexNo);
		}, [indexNo, state]);


	const { complaint, codeCat } = state;

	 // @ts-ignore: Object is possibly 'null'.
	const anything: any = !codeCat ? 'null' : codeCat.Category_Name;

	// @ts-ignore: Object is possibly 'null'.
	const RefNo: string = !complaint ? 'unavailable' : complaint.RespondentRefNumber;

	return (
		<div className={classes.root}>
			<div>{anything}
				</div>
			<div className={classes.dateIssued}>
				{dteOutput}
			</div>

			<div className={classes.borough}>
				Queens West
				</div>

			<div className={classes.district}>
				QW07
				</div>

			<div className={classes.referenceno}>
				{RefNo}
				</div>

			<div className={classes.fullname}>
				Steven E Farkas
				</div>

			<div className={classes.civilServiceTitle}>
				CSM
				</div>

			<div className={classes.dob}>
				7Feb1958
				</div>

			<div className={classes.badge}>
				Civilian
				</div>

			<div className={classes.homeadd}>
				40-28 College Point Blvd
				</div>

			<div className={classes.apt}>
				1709
				</div>

			<div className={classes.homeboro}>
				Queens
				</div>

			<div className={classes.homezip}>
				11354
				</div>

			<div className={classes.dateIncident}>
				10 Jun 2020
				</div>

			<div className={classes.districtIncident}>
				QW08
				</div>

			<div className={classes.dateAppointment}>
				23 Sep 2007
				</div>

			<div className={classes.violationCharges}>
				Code of Conduct non-compliance
				</div>

			<div className={classes.violationDescription}>
				description of the incident
				</div>

			<div className={classes.complaintantName}>
				Farkas' boss that day
				</div>

			<div className={classes.dateComplaintantSigned}>
				11 Jun 2020
				</div>

			<div className={classes.dateRespondantSigned}>
				12 Jun 2020
				</div>

			<div className={classes.respondantActionLeft}>
				L
				</div>

			<div className={classes.respondantActionRight}>
				R
				</div>

			<div className={classes.servedByLeft}>
				someone 1
				</div>

			<div className={classes.dateServedLeft}>
				14 Jun 2020
				</div>

			<div className={classes.servedByRight}>
				someone 2
				</div>

			<div className={classes.dateServedRight}>
				13 Jun 2020
				</div>

			<div className={classes.complaintantPrinted}>
				same as above
				</div>

			<div className={classes.complaintantLocation}>
				where chief works normally
				</div>

			<div className={classes.complaintantTitle}>
				chief
				</div>

			<div className={classes.witness1}>
				Witness One
				</div>

			<div className={classes.suspendedBy}>
				Borough Chief
				</div>

			<div className={classes.dateOfSuspendedOrder}>
				15Jun2020
				</div>

			<div className={classes.witness2}>
				Witness Two
				</div>

			<div className={classes.suspensionLiftedBy}>
				Assistant Commissioner
				</div>

			<div className={classes.dateSuspensionLifted}>
				30Jun2020
				</div>

			<div className={classes.witness3}>
				Witness Three
				</div>

		</div>
	);
}

