﻿export { NOVsearchTable } from "./Table"; // one DS21 per DS1984
export { EditableNOVdetail } from "./BlankTemplate"; // 25 NOV detail items per DS21 Card

export interface NOVbasics {
    DS1984receiptNumber: string,
    NovTktNumber: number,
    ECBSummonsNumber: string
};