﻿export const allServices = {
    employeeDetail: fEmployeeDetail,
    employeeList: fEmployeeList,
    districts: fDistricts,
    statementTemplates: fStatementTemplates
};

const API_URL = 'https://10.155.206.185/crusher/eis-gateway-service/api/eis/personnel/0001255';

// request the token
// subscribe to this event and use the returned json to save your token to state or session storage
function pfRequestAccessToken(data: any) {
    const loginInfo = `${data}&grant_type=password`;
    return fetch(`${API_URL}Token`, {
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/x-www-form-urlencoded',
        }),
        body: loginInfo,
    })
        .then((response) => response.json());

    // in your case set state to returned token
}

//// use said token to authenticate request
//function requestUserInfo(token) {
//    return fetch(`${API_URL}api/participant/userinfo`, {
//        method: 'GET',
//        headers: new Headers({
//            Authorization: `Bearer ${token}`,
//        }),
//    })
//        .then((response) => response.json());
//}

function fEmployeeDetail(employeeId: any) {
    const token = pfRequestAccessToken({ username: 'smart_userstg46', password: 'Change4dsny' });

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa('username:smart_userstg46&password:Change4dsny'),},
      //  body: JSON.stringify({ employeeId }),
        
    };

    return fetch(`https://10.155.206.185/crusher/eis-gateway-service/api/eis/personnel/0001255`, requestOptions)
        //.then(handleResponse)
        .then(response => {
         
            return response.json;
        });
}

function fEmployeeList() {
    // remove user from local storage to log user out
  //  localStorage.removeItem('user');
}

function fDistricts() {
    // remove user from local storage to log user out
  //  localStorage.removeItem('user');
}


function fStatementTemplates() {
    return fetch(`http://10.155.231.147:30000/api/ReferenceData/StatementTemplateType`)
        //.then(handleResponse)
        .then(res => res.json())
        .then(res => {
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fetching templates...");
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(res);
            //localStorage.setItem('templates', JSON.stringify(res));
           return res;
        })
        .catch((error: any) => { return error; });
}