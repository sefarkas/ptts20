﻿import axios from "axios";
import moment from "moment";
import { isNullOrUndefined } from "util";
import {
    ECB603, efECB603, DS1983, efDS1983, DS1984, efDS1984, tEfDS21HDR, DS1986, efDS1986, tOneNOV, tEfNOViud
} from '../model';
import { tDS1704, efDS1704, efDS1704iud } from "../model/PTTS_DS1704";
// ...

function fiudECB603(oneForm: ECB603) {
    const CreatedOnAsString: string = oneForm.CreatedOn.toString();
    const efexpected: efECB603 = {
        CartonNoFrom: oneForm.CartonNoFrom,
        CartonNoTo: oneForm.CartonNoTo,
        CartonQty: oneForm.CartonQty,
        CreatedBy: oneForm.CreatedBy,
        CreatedOn: // Oct  5 2020  4:19PM
            CreatedOnAsString.substr(4, 4)
            + CreatedOnAsString.substr(8, 2)
            + " " + CreatedOnAsString.substr(11, 4),
        CustomerOrderNo: oneForm.CustomerOrderNo,
        DS1983ReceiptNo: oneForm.DS1983ReceiptNo,
        FactoryJobNo: oneForm.FactoryJobNo,
        ID: oneForm.ID,
        IsVoid: oneForm.IsVoid,
        ModifiedBy: oneForm.ModifiedBy,
        ModifiedOn: oneForm.ModifiedOn,
        NoOfParts: oneForm.NoOfParts,
        OrderNo: oneForm.OrderNo,
        ReceiptNo: oneForm.ReceiptNo,
        TicketNoFrom: oneForm.TicketNoFrom,
        TicketNoTo: oneForm.TicketNoTo,

    };
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("efexpected");
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(efexpected);

    return axios.post(`${process.env.REACT_APP_PTTS20_API}/IUD/InsUptOneECB603`, efexpected)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function fiudDS1983(oneForm: DS1983) {
    const CreatedOnAsString: string = oneForm.CreatedDate.toString();
    const efexpected: efDS1983 = {
        CreatedDate: // Oct  5 2020  4:19PM
            CreatedOnAsString.substr(4, 4)
            + CreatedOnAsString.substr(8, 2)
            + " " + CreatedOnAsString.substr(11, 4),
        ReceiptNumber: oneForm.ReceiptNumber,
        MessengerName: oneForm.MessengerName,
        SummonsCount: oneForm.SummonsCount,
        SummonsFromNumber: oneForm.SummonsFromNumber,
        SummonsToNumber: oneForm.SummonsToNumber,
        Borough: oneForm.Borough,
        DistributorName: oneForm.DistributorName,
        DistributorSignature: oneForm.DistributorSignature,
        DistributorSignedDate: oneForm.DistributorSignedDate,
        MessengerSignature: oneForm.MessengerSignature,
        MessengerTitle: oneForm.MessengerTitle,
        MessengerSignedDate: oneForm.MessengerSignedDate,
        AcknowledgedBy: oneForm.AcknowledgedBy,
        ID: oneForm.ID,
        BoxNumber: oneForm.BoxNumber,
        FormStatus: oneForm.FormStatus,
        ModifiedOn: oneForm.ModifiedOn,
        ModifiedBy: oneForm.ModifiedBy,
        BadgeNo: oneForm.BadgeNo,
        DistributorRefNo: oneForm.DistributorRefNo,
        CreatedBy: oneForm.CreatedBy,
        DistrictID: oneForm.DistrictID,
        PayCode: oneForm.PayCode,
        IsVoid: oneForm.IsVoid,
    };
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("efexpected");
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(efexpected);

    return axios.post(`${process.env.REACT_APP_PTTS20_API}/IUD/InsUptOneDS1983`, efexpected)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function fiudDS1984(oneForm: DS1984) {
    const CreatedOnAsString: string = oneForm.CreatedDate.toString();
    const efexpected: efDS1984 = {
        CreatedDate: // Oct  5 2020  4:19PM
            CreatedOnAsString.substr(4, 4)
            + CreatedOnAsString.substr(8, 2)
            + " " + CreatedOnAsString.substr(11, 4),
        ID: oneForm.ID,
        ReceiptNumber: oneForm.ReceiptNumber,
        District: oneForm.District,
        ReceivingOfficerName: oneForm.ReceivingOfficerName,
        ReceivingOfficerTitle: oneForm.ReceivingOfficerTitle,
        BoxNumberedFrom: oneForm.BoxNumberedFrom,
        BoxNumberedTo: oneForm.BoxNumberedTo,
        ReceivingOfficerSignature: oneForm.ReceivingOfficerSignature,
        DistributorName: oneForm.DistributorName,
        DistributorSignature: oneForm.DistributorSignature,
        DistributorSignedDate: oneForm.DistributorSignedDate,
        TaxRegNumber: oneForm.TaxRegNumber,
        FormStatus: oneForm.FormStatus,
        AcknowledgedBy: oneForm.AcknowledgedBy,
        ModifiedOn: oneForm.ModifiedOn,
        ModifiedBy: oneForm.ModifiedBy,
        RecOfficerRefNo: oneForm.RecOfficerRefNo,
        DistributorRefNo: oneForm.DistributorRefNo,
        DS1983ReceiptNo: oneForm.DS1983ReceiptNo,
        DistrictId: oneForm.DistrictId,
        CreatedBy: oneForm.CreatedBy,
        // CreatedDate: oneForm.CreatedDate,
        Paycode: oneForm.Paycode,
        IsVoid: oneForm.IsVoid,

    };
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("efexpected");
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(efexpected);

    return axios.post(`${process.env.REACT_APP_PTTS20_API}/IUD/InsUptOneDS1984`, efexpected)
        .then((response: any) => {
            return response;
        }
        )
        .catch(err => {
            if (err.response) {
                // client received an error response (5xx, 4xx)
                return err.response;
            } else if (err.request) {
                // client never received a response, or request never left
                return err.request;
            } else {
                // anything else
                return err;
            }
        });
}

function fiudDS21HDR(oneCard: tEfDS21HDR): any {

    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("efexpected");
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(oneCard);

    // SPROC in the DB to update DS21HDR table only has a handful of parameters; the API will select
    // paraemters from the whole efDS21HDR row passed here as oneCard
    return axios.post(`${process.env.REACT_APP_PTTS20_API}/IUD/InsUptOneDS21HDR`, oneCard)
        .then((response: any) => {
            return response;
        }
        )
        .catch(err => {
            if (err.response) {
                // client received an error response (5xx, 4xx)
                return err.response.toString();
            } else if (err.request) {
                // client never received a response, or request never left
                return err.request.toString();
            } else {
                // anything else
                return err.toString();
            }
        });
}

function fiudDS1986(oneForm: DS1986) {
    const ModifiedOnAsString: string = oneForm.CreatedDate.toString();
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("ModifiedOnAsString is ", ModifiedOnAsString)
    const efexpected: efDS1986 = {
        ModifiedDate:
            // Oct  5 2020  4:19PM
            ModifiedOnAsString.substr(4, 4)
            + ModifiedOnAsString.substr(8, 2)
            + " " + ModifiedOnAsString.substr(11, 4),
        ID: oneForm.ID,
        ReferenceNo: oneForm.ReferenceNo,
        Name: oneForm.Name,
        District: oneForm.District,
        BadgeNo: oneForm.BadgeNo,
        NovasNo: oneForm.NovasNo,
        Ds1984No: oneForm.Ds1984No,
        DS1986ErrorId: oneForm.DS1986ErrorId,
        DS1986BDate: oneForm.DS1986BDate,
        OtherRemarks: oneForm.OtherRemarks,
        Remarks: oneForm.Remarks,
        CreatedBy: oneForm.CreatedBy,
        CreatedDate: oneForm.CreatedDate,
        ModifiedBy: oneForm.ModifiedBy,
        DS1984District: oneForm.DS1984District,
        ReceiptNo: oneForm.ReceiptNo,
        DS1984DistrictName: oneForm.DS1984DistrictName,
    };
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("efexpected");
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(efexpected);

    //(async () => {
    //    await
    return axios.post(`${process.env.REACT_APP_PTTS20_API}/IUD/InsUptOneDS1986`, efexpected)
        .then((response: any) => {
            return response;
        }
        )
        .catch(err => {
            if (err.response) {
                // client received an error response (5xx, 4xx)
                return err.response;
            } else if (err.request) {
                // client never received a response, or request never left
                return err.request;
            } else {
                // anything else
                return err;
            }
        });
    //})
}


function fiudDS1704(oneForm: tDS1704) {
    const ModifiedOnAsString: string = moment(oneForm.CreatedDate).format("YYYY-MM-DD");
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("ModifiedOnAsString is ", ModifiedOnAsString)
    const efexpected: efDS1704iud = {

        ECBSummonsNumber: oneForm.ECBSummonsNumber,

        PlaceOfOccurence: oneForm.PlaceOfOccurence,
        Remarks: oneForm.Remarks,
        ModifiedBy: oneForm.ModifiedBy,

        VoidDate: !oneForm.VoidDate ? new Date(1958, 2, 7) : oneForm.VoidDate,
        VoidReasonId: isNullOrUndefined(oneForm.VoidReasonId) ? 0 : oneForm.VoidReasonId,
    };
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("efexpected");
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(efexpected);

    if (oneForm.IsActive) {
        return axios.post(`${process.env.REACT_APP_PTTS20_API}/IUD/InsUptOneDS1704`, efexpected)
            .then((response: any) => {
                return response;
            }
            )
            .catch(err => {
                if (err.response) {
                    // client received an error response (5xx, 4xx)
                    return err.response;
                } else if (err.request) {
                    // client never received a response, or request never left
                    return err.request;
                } else {
                    // anything else
                    return err;
                }
            });
    }
    else {
        return axios.post(`${process.env.REACT_APP_PTTS20_API}/IUD/DelOneDS1704`, efexpected)
            .then((response: any) => {
                return response;
            }
            )
            .catch(err => {
                if (err.response) {
                    // client received an error response (5xx, 4xx)
                    return err.response;
                } else if (err.request) {
                    // client never received a response, or request never left
                    return err.request;
                } else {
                    // anything else
                    return err;
                }
            });
    }

    //})
}


async function fiudNOV(oneNOV: tOneNOV, currUser: string): Promise<boolean | undefined> {
    if (currUser.length < 3) { alert("cannot determine current user network ID"); }
    else if (oneNOV.ECBSummonsNumber.length < 3) { alert("dummy NOV passed to fiudNOV"); }
    else {
        const efexpected: tEfNOViud = {
            ModifiedOn: new Date(),
            ECBSummonsNumber: oneNOV.ECBSummonsNumber,
            Code: oneNOV.Code,
            DateServedMonth: oneNOV.DateServedMonth,
            DateServedDay: oneNOV.DateServedDay,
            DistOfOccurrence: oneNOV.DistOfOccurrence,
            Initial: oneNOV.Initial,
            Title: oneNOV.Title,
            DS1984Number: oneNOV.DS1984Number,
            Notarized: oneNOV.Notarized,
            Voided: oneNOV.Voided,
            ModifiedBy: currUser,
            TicketStatusID: oneNOV.TicketStatusID,
            ViolationType: oneNOV.ViolationType,
            DateServedYear: oneNOV.DateServedYear,
            CreatedBy: oneNOV.CreatedBy.length === 0 ? currUser : oneNOV.CreatedBy,
            CreatedOn: oneNOV.CreatedBy.length === 0 ? (new Date()).toLocaleDateString() : oneNOV.CreatedOn,
            DS21_TicketStatus: oneNOV.DS21_TicketStatus
        };
        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("efexpected");
        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(efexpected);

        //(async () => {
        //    await
        return await axios.post(`${process.env.REACT_APP_PTTS20_API}/IUD/InsUptOneNOV`, efexpected)
            .then((response: any) => {
                if (!isNaN(parseFloat(response.data)) && isFinite(response.data)) return true; //response;
                // https://stackoverflow.com/questions/9716468/pure-javascript-a-function-like-jquerys-isnumeric

                return false;
            }
            )
            .catch(err => {
                if (err.response) {
                    // client received an error response (5xx, 4xx)
                    throw err.response;
                } else if (err.request) {
                    // client never received a response, or request never left
                    throw err.request;
                } else {
                    // anything else
                    return false;
                }
            });
        //})
    }

}

// ...

export const allServices: any = {
    iudECB603: fiudECB603,
    iudDS1983: fiudDS1983,

    iudDS1984: fiudDS1984,

    iudDS21HDR: fiudDS21HDR, // iud for a DS21 Card means updating the DS21HDR table; HDR has status for the whole card
    // DS21 has attributes for each ECB Summons
    iudNOV: fiudNOV, // change attributes of one NOV on table DS21

    iudDS1986: fiudDS1986,

    iudDS1704: fiudDS1704,
};