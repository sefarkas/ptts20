﻿// prettier-ignore
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
//import Draggable from 'react-draggable';
import { useActions } from "../../actions";
import * as DS1984Actions from "../../actions/DS1984";
import Background from '../../assets/images/DS249_1204.png';

interface Props {
	open: boolean;
	onClose: () => void;
}

export function DS1984Dialog(props: Props) {
	const { open, onClose } = props;
	const classes = useStyles();
	const [newDS1984Text, setNewDS1984Text] = React.useState("");
	const cDS1984Actions: any = useActions(DS1984Actions);

	const handleCreate = (event: any) => {
		cDS1984Actions.insDS1984({
			id: Math.random(),
			completed: false,
			DS1984ReceiptNo: event.target.value,
			TicketRange: '1000-1999',
			CartonNoRange: '10-89',
			NoOfParts: 4,
			ModifiedOn: Date.now,
			ModifiedBy: 'sef'
		});
		// reset text if user reopens the dialog
		setNewDS1984Text("new");
		onClose();

	};

	const handleClose = () => {
		// reset text if user reopens the dialog
		setNewDS1984Text("");
		onClose();

	};

	const handleUpdate = (event: any) => {
		cDS1984Actions.uptDS1984({
			id: Math.random(),
			completed: false,
			DS1984ReceiptNo: event.target.value,
			TicketRange: '1000-1999',
			CartonNoRange: '10-89',
			NoOfParts: 4,
			ModifiedOn: Date.now,
			ModifiedBy: 'sef'
		});
		setNewDS1984Text("updated");

	};

	return (
		<Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
			<DialogTitle>Add a new DS1984</DialogTitle>
			<DialogContent>
				<DialogContentText>
					THE CITY OF NEW YORK Department of Sanitation<br />
					NOTICE OF VIOLATION & HEARING Form DS1984
				</DialogContentText>
				<TextField
					autoFocus
					margin="dense"
					id="DS1984ReceiptNo"
					label="RECEIPT NUMBER"
					type="string"
					fullWidth
					value={newDS1984Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
				<TextField
					margin="dense"
					id="BoxNumberedFrom"
					label="First Box Number"
					type="string"
					size="medium"
					required
					value={newDS1984Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
				<TextField
					margin="dense"
					id="BoxNumberedTo"
					label="Last Box Number"
					type="string"
					size="small"
					required
					value={newDS1984Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
			</DialogContent>
			<DialogActions>
				<Button onClick={handleUpdate} color="secondary">
					Save/Update
				</Button>
				<Button onClick={handleCreate} color="primary">
					MAKE NEW DS1984
				</Button>
				<Button onClick={handleClose} color="primary">
					Done
				</Button>
			</DialogActions>

		</Dialog>

	);
}

const useStyles = makeStyles({
	newDS1984: {
		flexGrow: 1,
		height: "10%",
		paddingLeft: 15,
		paddingRight: 15,
		opacity: 60,
		backgroundImage: `url( ${Background} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '82px 103px',
		backgroundPosition: 'left top',
	},
	pending: {
		backgroundColor: "white",
	},
	approved: {
		backgroundColor: "yellow"
	},
	textField: {
		width: "80%",
		margin: 20,
	},
});
