﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ONB.Models
{
    public class Users
    {
        public int UserId
        {
            get; set;
        }
        public string UserName
        {
            get; set;
        }
        public string Password
        {
            get; set;
        }
        public string FirstName
        {
            get; set;
        }
        public string LastName
        {
            get; set;
        }
        public string Email
        {
            get; set;
        }
        public string VoiceTelephoneNumber
        {
            get; set;
        }
        public DateTime CreateDate
        {
            get; set;
        }
        public DateTime LastLoginDate
        {
            get; set;
        }
        public int IsActive
        {
            get; set;
        }
        public string Roles
        {
            get; set;
        }
        public List<UserRoleType> UserRoles
        {
            get; set;
        }
        public string AuthenticationMessage
        {
            get; set;
        }
        public string ADgroups
        {
            get; set;
        }
        public bool Authenticated
        {
            get; set;
        }

    }
}