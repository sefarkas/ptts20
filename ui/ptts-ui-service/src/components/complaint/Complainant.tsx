﻿import { Typography, TextareaAutosize } from "@material-ui/core";
import React, { useEffect } from 'react';
import { useFormContext } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import ReactSelect, { components } from "react-select";
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers';
import styles from "./complaintStyles";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";
import { referenceActions } from '../../actions';

interface Props {
	idx: number;
	complainant: any;
	complainantOptions: any;
	districtOptions: any;
	titleOptions: any;
	handleComplainantChange: (e: any, control?: string, idx?: number) => void;
	handleRemoveComplainant: (event: any, idx: number) => void;	
	handleInputChange: (e: any, caller: string) => void;
	handleComplainantRefDataChange: (title: string, location: string, idx: number) => void;
}

library.add(faSearch);

const theme = createMuiTheme({
	overrides: {
		MuiTypography: {
			h5: {
				'&.MuiTypography-text': {
					fontSize: "1.2em",
				},
			},
		},

	},
});

const CaretDownIcon = () => {
	return <FontAwesomeIcon icon="search" />;
};

const DropdownIndicator = (props: any) => {
	return (
		<components.DropdownIndicator {...props}>
			<CaretDownIcon />
		</components.DropdownIndicator>
	);
};

const DeleteIcon = {
	deleteIcon: require('../../assets/images/delete_icon.png')
};

export function Complainant(props: Props) {
	const classes = styles();
	const { register, errors } = useFormContext();	
	const [isLoading, setLoading] = React.useState(false);

	React.useEffect(() => {

		if (props.complainant.name.value !== 0) {
			(async () => {
				setLoading(true);
				const response = await referenceActions.employeeByRefNumber(props.complainant.name.value);
				setLoading(false);

				props.handleComplainantRefDataChange(response.data.jobModel.jobInformation.jobTypeId.correlation.replace(/(_)/g, ' '),
					response.data.jobModel.jobLocation.departmentId.correlation.replace(/(_)/g, ' '), props.idx);
			})();
        }
	}, [props.complainant.name.value]);


	return (
		<React.Fragment key={"complainant" + props.idx}>
			<Grid container item xs={9} key={"gridcomplainant" + props.idx} spacing={0}>
				<Grid item xs={3}>
					<label className={classes.labelX} key={"labelcomplainant" + props.idx}>Complainant Name</label></Grid>
				<Grid item xs={3}>
					{props.idx > 0 ? <span onClick={e => props.handleRemoveComplainant(e, props.idx)}><Typography className={classes.labelRemoveXS3}><img src={DeleteIcon.deleteIcon} alt="" className={classes.deleteIcon} />Remove</Typography></span> : null}
				</Grid>
				<Grid item xs={6} />
				<Grid item xs={6} className={classes.gridCell}>
					<ReactSelect
						onChange={e => props.handleComplainantChange(e, "name", props.idx)}
						onInputChange={e => props.handleInputChange(e, "complainant")}						
						options={props.complainantOptions}
						components={{ DropdownIndicator }}
						value={props.complainant.name}
						defaultValue={props.complainant.name}
						placeholder="Enter Name or Ref No."
						className={classes.reactselect}
						theme={theme => ({
							...theme,
							borderRadius: 1,
							colors: {
								...theme.colors,
								primary25: '#d2f6e3',
								primary50: '#2b995f',
								primary: '#2b995f',
							},
						})}
					/></Grid><Grid item xs={6} />
				<Grid item xs={6} className={classes.gridCell}>
					<label className={classes.label} key={"labeltitle" + props.idx}>Title</label>
					<input value={props.complainant.title} className={classes.input} key={"title" + props.idx} readOnly={true} />					
				</Grid>
				<Grid item xs={6} className={classes.gridCell}>
					<label className={classes.label}>District/Division</label>
					<input value={props.complainant.district} className={classes.input} key={"complDistrict" + props.idx} readOnly={true} />					
				</Grid>
				<Grid container item xs={12} className={classes.gridCell} spacing={0}>
					<Grid item xs={6}>
						<label className={classes.labelX}>Statement</label></Grid>
					<Grid item xs={6}>
						<Typography className={classes.labelY}>{props.complainant.statement.length}/1000</Typography></Grid>
					<MuiThemeProvider theme={theme}>
						<TextareaAutosize
							className={classes.textArea}
							rowsMin={6}
							placeholder="Specify Acts Which Constitute Violations..."
							onChange={e => props.handleComplainantChange(e, "statement", props.idx)}
							maxLength={1000}
							value={props.complainant.statement} />
					</MuiThemeProvider>
				</Grid>
			</Grid>
			<Grid item xs={3} className={classes.gridSpacerCell} />
		</React.Fragment>
	);
}