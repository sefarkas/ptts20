export * from "./ComplaintsPage";
export * from "./HomePage";
export * from "./LoginPage";
export * from "./PTTS_HomePage";
export * from "./LogoutPage";
export * from "./PTTS_RPTECB603";
export * from "./PTTS_RPTDS1983";
export * from "./PTTS_RPTDS1984";
export * from "./PTTS_RPTDS1986";
export * from "./NewComplaintPage";
export * from "./PTTS_NOV";
export * from "./PTTS_RPTDS21";
export * from "./PTTS_DS1704A";

