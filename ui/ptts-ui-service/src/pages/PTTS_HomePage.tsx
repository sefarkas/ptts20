﻿import { Grid, Paper } from "@material-ui/core";
import { Theme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import axios from 'axios';
import * as React from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import BackgroundDS156 from '../assets/images/DS156.png';
import BackgroundDS1704 from '../assets/images/DS1704.png';
import BackgroundDS1983 from '../assets/images/DS1983.png';
import BackgroundDS1984 from '../assets/images/DS1984.png';
import BackgroundDS1986 from '../assets/images/DS1986B.png';
import BackgroundDS21 from '../assets/images/DS21.png';
import Background from '../assets/images/dsny_logo.png';
import BackgroundECB603 from '../assets/images/ECB603.png';
import { history } from "../configureStore";
import { tGiantState } from "../reducers";
import { useEffect, useRef } from "react";
import { tDS21HDRState } from "../reducers/DS21HDR";
import { PTTSfunctions } from "../components/basicComponents";

export const PTTSHomePage = () => { // = (props: { dispatchRef: any, content: tGiantState }) =>  {
	const classes = PTTShomePageStyles();

	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Home Page has this in content:");
	const stateFromSelector: tGiantState = useSelector((state: any) => state.giantReducer);
	const stateDS21fromSelector: tDS21HDRState = useSelector((state: any) => state.DS21HDRReducer); 
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(stateFromSelector);
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(stateDS21fromSelector);



	const mountedRef = useRef(true);
	useEffect(() => {

		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useEffect [] of Home Page selector/passed in");
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(stateFromSelector);

		if (
			!localStorage.getItem('userauth')
			|| stateFromSelector.ST_DELIBERATELOGOUT
			|| stateFromSelector.ST_LOGINAD === false

		) {
			mountedRef.current = false

			history.push('/PTTS/login')
		};

		return () => {
			mountedRef.current = false
		}
	}, [])

	if (!stateFromSelector ? false : PTTSfunctions.ADfunctions.isADinENFHQ(stateFromSelector.ST_ADGROUPS)) 
	{

		return (
			<Grid container className={classes.root}>
				<Grid item xs={6}>
					<Grid item xs={12}>
						<Paper className={classes.paperECB603}
							onClick={() => { history.push('/PTTS/ECB603') }}>
							ECB 603</Paper>
					</Grid>
					<Grid item xs={12}>
						<Paper className={classes.paperDS1983}
							onClick={() => { history.push('/PTTS/DS1983') }}>
							DS 1983</Paper>
					</Grid>
					<Grid item xs={12}>
						<Paper className={classes.paperDS1984}
							onClick={() => { history.push('/PTTS/DS1984') }}>
							DS 1984</Paper>
					</Grid>
				</Grid>
				<Grid item xs={6} >
					<Grid item xs={12} >
						<Paper className={classes.paperNoForm}>ECB</Paper>
					</Grid>
					<Grid item xs={12}>
						<Paper className={classes.paperNoForm}>Mail Room</Paper>
					</Grid>
					<Grid container>
						<Grid item xs={6}>
							<Paper className={classes.paperNoForm}>Ownership</Paper>
						</Grid>
						<Grid item xs={6} >
							<Paper className={classes.paperNoForm}>Research</Paper>
						</Grid>
					</Grid>
					<Grid item xs={12} >
						<Paper className={classes.paperNoForm}>
							<Paper className={classes.paperDS156}>DS-156</Paper>
							<div>Logging Unit</div>
						</Paper>
					</Grid>
				</Grid>
				<Grid item xs={12} >
					<Grid item xs={6} >
						<Paper className={classes.paperDS21}
							onClick={() => { history.push('/PTTS/DS21HDR/') }}>
							DS 21</Paper>
					</Grid>
					<Grid container justify="flex-start">
						<Grid item xs={6} >
							<Paper className={classes.paperDS1704}
								onClick={() => { history.push('/PTTS/DS1704A') }}>
								Bulk DS 1704</Paper>
						</Grid>
					</Grid>
					<Grid container justify="flex-start">
						<Grid item xs={6} >
							<Paper className={classes.paperDS1986}
								onClick={() => { history.push('/PTTS/DS1986') }}>
								DS 1986</Paper>
						</Grid>
					</Grid>
				</Grid>
			</Grid>
		);
	}
	else {
	return (
		<Grid container className={classes.root}>
			<Grid item xs={6}>
				<Grid item xs={12}>
					<Paper className={classes.paperDS1983}
						onClick={() => { alert("You are not configured for DS-1983s; only ENF HQ.") }}>
						DS 1983</Paper>
				</Grid>
				<Grid item xs={12}>
					<Paper className={classes.paperDS1984}
						onClick={() => { history.push('/PTTS/DS1984') }}>
						DS 1984</Paper>
				</Grid>
			</Grid>
			<Grid item xs={12} >
				<Grid item xs={6} >
					<Paper className={classes.paperDS21}
						onClick={() => { history.push('/PTTS/DS21HDR/') }}>
						DS 21</Paper>
				</Grid>
				<Grid container justify="flex-start">
					<Grid item xs={6} >
						<Paper className={classes.paperDS1704}>DS 1704</Paper>
					</Grid>
				</Grid>
				<Grid container justify="flex-start">
					<Grid item xs={6} >
						<Paper className={classes.paperDS1986}
							onClick={() => { history.push('/PTTS/DS1986') }}>
							DS 1986</Paper>
					</Grid>
				</Grid>
			</Grid>
		</Grid>
	);
    }

}

const PTTShomePageStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: 20,
		marginLeft: 0,
		[theme.breakpoints.down("md")]: {
			paddingTop: 50,
			paddingLeft: 15,
			paddingRight: 15,
		},
	},
	paper: {
		width: "100%",
		minWidth: 260,
		minHeight: '170px',
		display: "inline-block",
		textAlign: "center",
		marginTop: '10px',
		backgroundImage: `url( ${Background} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '130px 130px',
		backgroundPosition: 'left top',
		backgroundPositionY: '40px',
		height: '130px',
	},
	paperDS1704: {
		width: "100%",
		minWidth: 260,
		minHeight: '170px',
		display: "inline-block",
		textAlign: "center",
		marginTop: '10px',
		backgroundImage: `url( ${BackgroundDS1704} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '430px 430px',
		backgroundPosition: 'center top',
		backgroundPositionY: '40px',
		height: '130px',
	},
	paperDS1984: {
		width: "100%",
		minWidth: 260,
		minHeight: '170px',
		display: "inline-block",
		textAlign: "center",
		marginTop: '10px',
		backgroundImage: `url( ${BackgroundDS1984} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '430px 430px',
		backgroundPosition: 'center top',
		backgroundPositionY: '40px',
		height: '130px',
	},
	paperDS1986: {
		width: "100%",
		minWidth: 260,
		minHeight: '170px',
		display: "inline-block",
		textAlign: "center",
		marginTop: '10px',
		backgroundImage: `url( ${BackgroundDS1986} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '430px 430px',
		backgroundPosition: 'center top',
		backgroundPositionY: '40px',
		height: '130px',
	},
	paperDS156: {
		width: "100%",
		minWidth: 260,
		minHeight: '170px',
		display: "inline-block",
		textAlign: "center",
		marginTop: '10px',
		backgroundImage: `url( ${BackgroundDS156} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '430px 430px',
		backgroundPosition: 'center top',
		backgroundPositionY: '40px',
		height: '130px',
	},
	paperECB603: {
		width: "100%",
		minWidth: 260,
		minHeight: '170px',
		display: "inline-block",
		textAlign: "center",
		marginTop: '10px',
		backgroundImage: `url( ${BackgroundECB603} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '430px 430px',
		backgroundPosition: 'center top',
		backgroundPositionY: '40px',
		height: '130px',
	},
	paperDS1983: {
		width: "100%",
		minWidth: 260,
		minHeight: '170px',
		display: "inline-block",
		textAlign: "center",
		marginTop: '10px',
		backgroundImage: `url( ${BackgroundDS1983} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '430px 430px',
		backgroundPosition: 'center top',
		backgroundPositionY: '40px',
		height: '130px',
	},
	paperDS21: {
		width: "100%",
		minWidth: 260,
		minHeight: '170px',
		display: "inline-block",
		textAlign: "center",
		marginTop: '10px',
		backgroundImage: `url( ${BackgroundDS21} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '430px 430px',
		backgroundPosition: 'center top',
		backgroundPositionY: '40px',
		height: '130px',
	},
	paperNoForm: {
		width: "100%",
		minHeight: '35px',
		display: "inline-block",
		textAlign: "center",
	},
	buttonContainer: {
		width: "100%",
		display: "flex",
		justifyContent: "flex-end",
	},

	button: {
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	dialog: {
		minHeight: 500
	}
}));


