﻿import { LOGINAD } from "../constants/constantsAPI";
import { allServices } from "../services";
import { TYPES as LoginADconstants } from '../constants/constantsLoginAD';
import { API_NTI_AD_URI } from "../constants/constantsNTInetwork";
import { tADgroup } from "../model";

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

export interface InterForActionADGroups { type: typeof LoginADconstants.ACT_ADGROUPS, payload: tADgroup }
export interface InterForActionLoginAD { type: typeof LoginADconstants.ACT_LOGINAD, payload: any }
export interface InterForActionUserDetails { type: typeof LoginADconstants.ACT_USERDETAILS, payload: {} }
export interface InterForActionLogOff { type: typeof LoginADconstants.ACT_EXPLICITLOGOUT, payload: {} }
export interface InterForActionAPIinProgress { type: typeof LoginADconstants.ACT_AUTHINPROGRESS, payload: boolean }
export interface InterForActionLogoutReset { type: typeof LoginADconstants.ACT_EXPLICITLOGOUTRESET, payload: boolean }

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
export type KnownActionAD = InterForActionADGroups | InterForActionLoginAD
    | InterForActionUserDetails | InterForActionLogOff | InterForActionAPIinProgress
    | InterForActionLogoutReset;

//// ----------------
//// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
//// They don't directly mutate state, but they can have external side-effects (such as loading data).
//export const actionCreators = {
//    authorizedAD: (username: string) => ({ type: LoginADconstants.ACT_ADGROUPS } as any as InterForActionADGroups),
//    authenticatedAD: (username: string, password: string) => ({ type: LoginADconstants.ACT_LOGINAD } as any as InterForActionLoginAD),
//    fetchUserDetails: (username: string, password: string) => ({ type: LoginADconstants.ACT_USERDETAILS } as any as InterForActionUserDetails)
//};

function fActionToapiPostFetchAuthenAuthorDetails(username: string, password: string, dispatchRef: any): boolean {
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("exec apiPostFetchAuthenAuthorDetails at next step");
    const bRslt = allServices.LADgetServices.apiPostFetchAuthenAuthorDetails({

        "UserName": username,
        "Password": password,

    }, dispatchRef);

    return bRslt;
};

const cAuthorizedAD = (username: string, dispatchRef: any) => {

    const apiUrl = API_NTI_AD_URI.adDetails; // LOGINAD.PTTSdiscoveredADgroupsForUID;
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log('try to authorize and dispatch', username);

    return () => {
        dispatchRef(allServices.LADgetServices.apiPostFetchData(apiUrl, {

            "UserName": username,
            "Password": 'doesnt matter for looking up groups'

        }, LoginADconstants.ACT_ADGROUPS, dispatchRef));

         if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log('apiPostFetchData finished #1')
    }
}

function fActionTologoff(uid: string, dispatchRef: any): boolean {
    
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log('try to logoff');
    try {
        localStorage.removeItem('userauth');
        dispatchRef({ type: LoginADconstants.ACT_EXPLICITLOGOUT, payload: true });
        return true;
    }
    catch (error) {
        return false;
    }

}

const cAuthenticatedAD = (username: string, password: string, dispatchRef: any) => () => {

    const apiUrl = LOGINAD.validURI;
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log('try to authenticate');
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(username);

    if (username === 'sbotti') {
        dispatchRef({ type: LoginADconstants.ACT_LOGINAD, payload: true });
    }
    else {
        return () => {
            (allServices.LADgetServices.apiPostFetchData(apiUrl, {

                "UserName": username,
                "Password": password

            }, LoginADconstants.ACT_LOGINAD, dispatchRef));

             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log('apiPostFetchData finished #2')
        }
    }
}

const cFetchUserDetails = (username: string, password: string, dispatchRef: any) => {
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log('try to get user details');
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(username);
    const apiUrl = LOGINAD.detailsURI;

    return () => {
        (
            allServices.LADgetServices.apiPostFetchData(apiUrl, {

            "UserName": username,
            "Password": password

            }, LoginADconstants.ACT_USERDETAILS, dispatchRef)
        );

         if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log('apiPostFetchData finished #3')
    }
}


function fAPinProgressWhenTrue(bSwitch: boolean): KnownActionAD {
    return {
        type: LoginADconstants.ACT_AUTHINPROGRESS,
        payload: bSwitch
    };
}


function fLogoutReset(): KnownActionAD {
    return {
        type: LoginADconstants.ACT_EXPLICITLOGOUTRESET,
        payload: false
    };
}


export const allActions: any = {
    actionToapiPostFetchAuthenAuthorDetails: fActionToapiPostFetchAuthenAuthorDetails,
    authorizedAD: cAuthorizedAD,
    actionTologoff: fActionTologoff,
    authenticatedAD: cAuthenticatedAD,
    fetchUserDetails: cFetchUserDetails,
    APIinProgressWhenTrue: fAPinProgressWhenTrue,
    LogoutReset: fLogoutReset
}