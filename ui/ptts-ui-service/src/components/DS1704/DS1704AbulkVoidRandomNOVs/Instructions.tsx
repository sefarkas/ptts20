﻿import { Grid, makeStyles, Paper } from '@material-ui/core';
import React from 'react';


export function Instructions() {
	const classes = useStyles();

	return (
		<div className={classes.styleforBorder}>
			<Grid item xs={6} component={Paper} className={classes.styleforInstructions}>
				<div >
					INSTRUCTION : <br />
					<ol className={classes.styleforOL}>
						<li>Scan the paper ticket. </li>
						<li>After the <b>ticket No.</b> has inserted into the column <u>‘Ticket No.’</u>, <br />
							press the <b><u>‘Tab’</u></b> button on the keyboard:<br />
							the <b>‘District’</b> and <b>‘Date’</b> will be inserted.<br />
							<div className={classes.styleforTab}>For <u>Issued</u> Ticket # the inserted <b>Date</b> is the <u>Date of Issue</u>. </div>
							<div className={classes.styleforTab}>For <u>unused</u> Ticket # the inserted <b>Date</b> is the Date from the <u>DS1984 receipt #</u>.</div>
							</li>
						<li>For the <b>Issued</b> ticket only: <br />
							<div className={classes.styleforTab}>Type the <b>address</b> of the <u>Place of Occurrence</u> in the <u>‘Address’</u> column.</div>
							<div className={classes.styleforTab}>Select the <b>reason for Voiding</b> from the <u>‘Reason For Void’</u> drop-down list.</div>
						</li>
						<li>Click on the button <u>‘Void’</u>. The message displays <b>‘Ticket VOIDED Successfully’</b>.</li>
					</ol>
				</div>
			</Grid>
			<Grid item xs={6} component={Paper} className={classes.paper}>
				&nbsp;
			</Grid>
		</div>
	)
}

const useStyles = makeStyles({
	paper: {
		width: "100%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 360,
		overflowX: "visible",
	},
	styleforInstructions: {
		marginBottom: 13,
		marginTop: 10,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 12,
		fontWeight: "normal",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "red",
		maxWidth: 550,
		minWidth: 300,
	},
	styleforOL: {
		listStyleType: "decimal"
	},
	styleforBorder: {
		borderStyle: "solid",
		borderWidth: 2,
	},
	styleforTab: {
		marginLeft: '.5rem' 
    }
});