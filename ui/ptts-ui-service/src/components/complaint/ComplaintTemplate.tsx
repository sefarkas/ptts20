﻿import Modal from "react-modal";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import ReactSelect from "react-select";
import Grid from '@material-ui/core/Grid';
import {
	TemplateAWOL,
	TemplateNoDocCatA,
	TemplateNoDocCatB,
	TemplateNoDocCatC,
	TemplateLateness,
	TemplateNotHomeVisit,
	TemplateNotHomeTelephone,
	TemplateNoDocHVPCode,
	TemplateNoPhone,
	TemplateDisconnectedPhone,
	TemplateToxTest,
	TemplateMVA,
	TemplateLicense,
	TemplateGuideMan,
	TemplateRedLight,
	TemplateSeatBelt,
	TemplateAbsentWithoutAuthority,
	TemplateDeemedPrejudicial,
	TemplateDSNYTrialLetter,
	TemplateNoProof,
	TemplateDeniedProof
} from './templates';
import { referenceActions } from '../../actions';

interface Props {
	open: boolean;
	onClose: (statementValue: string) => void;
	respondent: string;
	incidentDate: string;	
}

export function ComplaintTemplate(props: Props) {
	const [templateData, setTemplateData] = React.useState([]);
	const [isLoading, setLoading] = React.useState(false);

	React.useEffect(() => {

		(async () => {
			setLoading(true);
			const response = await referenceActions.statementTemplates();
			setLoading(false);
			setTemplateData(response.data);
		})();

	}, []);

	const templates = templateData.map((t: any, idx: any) => {
		return (
			{ value: t.id, label: t.description, code: t.code }
		)
	}).sort((a, b) => (a.label > b.label) ? 1 : -1);

	const { open, onClose } = props;
	const classes = useStyles();
	const [template, setTemplate] = React.useState({ value: 1, label: "1.4 AWOL", code: "PAP2007-04" });

	const handleTemplateChange = (e: any) => {
		setTemplate({
			value: e.value,
			label: templates.filter((t) => { return t.value === e.value; })[0].label,
			code: templates.filter((t) => { return t.value === e.value; })[0].code
		});
	};

	const handleSubmit = (data: any) => {
		props.onClose(data);		
	};

	const handleCancel = (event: any) => {
		setTemplate({ value: 1, label: "1.4 AWOL", code: "PAP2007-04" });
		props.onClose("");
	};

	Modal.setAppElement('#root');

	return (
		<Modal
			isOpen={open}
			shouldCloseOnOverlayClick={false}
			onRequestClose={() => onClose("")}
			style={
				{
					overlay: {
						// backgroundColor: "grey"
					},
					content: {
						backgroundColor: "#edeff0",
						padding: '0 0px',
						blockSize: "471px",
						position: "absolute",
						top: "10%",
						left: "30%",
						bottom: "0px",
						maxWidth: "600px"						
					}
				}
			}
		>   <Grid container spacing={0}>
				<Grid item xs={12}>
					<h2 className={classes.h2}>Complainant Statement Template</h2>
				</Grid>
				<Grid item xs={12}>
					<h3 className={classes.h3}>Specify acts which constitute violations</h3>
				</Grid>
				<Grid container className={classes.grid} spacing={0}>
					<Grid item xs={12} className={classes.gridCell}>
						<label className={classes.label}>Template Type</label>	
						<ReactSelect							
							onChange={handleTemplateChange}
							options={templates}							
							value={template}
							placeholder="Select One"
							menuColor="green"
							className={classes.reactselect}
							theme={theme => ({
								...theme,
								borderRadius: 1,
								height: "30px",
								colors: {
									...theme.colors,
									primary25: '#d2f6e3',
									primary50: '#2b995f',
									primary: '#2b995f',									
								},
								spacing: {
									controlHeight: 25,
									menuGutter: 1,
									baseUnit: 2,
								},
							})}
						/></Grid>	
					<Grid item xs={12} className={classes.gridCell}><label className={classes.labelDisabled}>{template.code}</label></Grid>	
				</Grid>
				{template.label === "1.4 AWOL" ? <TemplateAWOL open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}	
				{template.label === "No Doc. Cat A" ? <TemplateNoDocCatA open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "No Doc. Cat B" ? <TemplateNoDocCatB open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "No Doc. Cat C" ? <TemplateNoDocCatC open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "Lateness" ? <TemplateLateness open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "Not Home Visit" ? <TemplateNotHomeVisit open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "Not Home Telephone" ? <TemplateNotHomeTelephone open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "No Doc. HVP Code" ? <TemplateNoDocHVPCode open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "No Phone" ? <TemplateNoPhone open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "Disconnected Phone" ? <TemplateDisconnectedPhone open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "Tox Test" ? <TemplateToxTest open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "MVA" ? <TemplateMVA open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "License" ? <TemplateLicense open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "Guide man" ? <TemplateGuideMan open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "Red Light" ? <TemplateRedLight open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "Seat Belt" ? <TemplateSeatBelt open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "Absent Without Authority" ? <TemplateAbsentWithoutAuthority open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "DSNY Trial Letter" ? <TemplateDSNYTrialLetter open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "Deemed Prejudicial" ? <TemplateDeemedPrejudicial open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "1.5 For No Proof" ? <TemplateNoProof open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}
				{template.label === "1.5 For Denied Proof" ? <TemplateDeniedProof open={props.open} onSubmit={handleSubmit} onClose={handleCancel} respondent={props.respondent} incidentDate={props.incidentDate} /> : null}

			</Grid>
		</Modal>
	);
}

const useStyles = makeStyles({	
	reactselect: {
		maxWidth: "calc(100% - 20px)",		
		marginLeft: 10,
		marginTop: 0,		
		fontSize: 12,
		'.control': {
			height: 25
		}
	},	
	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "inline-block",
		marginBottom: 0,
		marginTop: 5,
		marginLeft: 10,
		fontSize: 12,
		fontWeight: 800,
		color: "#2e2e2d",
	},
	labelDisabled: {
		marginBottom: 10,
		marginTop: 10,
		marginLeft: 10,
		paddingBottom: 10,
		color: "#959292",
		fontSize: 10
	},	
	grid: {
		background: "white",
		marginTop: "2px;"
	},	
	gridCell: {
		paddingTop: "2px",		
	},	
	h2: {
		marginTop: "10px",
		marginBottom: "5px",
		marginLeft: "10px",
		fontSize: 15,		
	},
	h3: {
		marginTop: "5px",
		marginBottom: "5px",
		marginLeft: "10px",
		fontSize: 11,
	}
});