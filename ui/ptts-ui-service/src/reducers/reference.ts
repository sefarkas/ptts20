﻿import { ReferenceAction, ReferenceActions, PayrollLocations } from "../model";
import createReducer from "./createReducer";

export const complaintList = createReducer<PayrollLocations[]>([], {
	[ReferenceActions.ADD_PAYROLL_LOCATIONS](state: PayrollLocations[], action: ReferenceAction) {
		return [...state, action.payload];
	},
	[ReferenceActions.FIND_PAYROLL_LOCATION](state: PayrollLocations[], action: ReferenceAction) {
		// search after Complaint item with the given id and set completed to true
		return state.map(t =>
			t.id === action.payload ? { ...t, completed: true } : t
		);
	},
	//[ComplaintActions.UNCOMPLETE_COMPLAINT](state: Complaint[], action: ReferenceAction) {
	//	// search after Complaint item with the given id and set completed to false
	//	return state.map(t =>
	//		t.id === action.payload ? { ...t, completed: false } : t
	//	);
	//},
	//[ComplaintActions.DELETE_COMPLAINT](state: Complaint[], action: ReferenceAction) {
	//	// remove all Complaint with the given id
	//	return state.filter(t => t.id !== action.payload);
	//},
});