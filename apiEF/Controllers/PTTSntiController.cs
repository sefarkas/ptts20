﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using PTTSapiEFdb;

namespace apiEF.Controllers
{
    [RoutePrefix( "PTTS20/api/nti" )]
    [EnableCors( origins: "*", headers: "*", methods: "*" )]
    // [EnableCors( origins: "https://localhost:18144", headers: "*", methods: "*" )]
    // has to refer to the client IP and port; see also WebApiConfig Register and Web.Config system.webServer handlers
    public class PTTSntiController : ApiController
    {
        [Route( "GetProfile/{refn}" )]
        public IQueryable<usp_GetTAXRegNoByRefNo_Result> GetNTIprofileWithSPROC( string refn )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            var q = db.usp_GetTAXRegNoByRefNo( refn, "anystring").AsQueryable();
            return q;
        }

    }
}
