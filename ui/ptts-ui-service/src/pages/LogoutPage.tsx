import React, { useEffect, Fragment } from "react";
import { useDispatch } from 'react-redux';
import { allActions } from '../actions';
import { history } from "../configureStore";


export function LogoutPage() {

    const dispatch = useDispatch();
    localStorage.removeItem("userauth");
    localStorage.removeItem("userid");
     useEffect(() => { 
        allActions.LoginADacts.actionTologoff("anystring", dispatch);
        history.push('/PTTS/login');
     }, []);
  

  return (
    <Fragment>
    </Fragment>
  );
}

