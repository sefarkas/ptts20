﻿import React from 'react';
import { useFormContext } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import ReactSelect from "react-select";
import styles from "./complaintStyles";

export interface FileObject {
	readonly file: File;
	readonly data: string | ArrayBuffer | null;
}

interface Props {	
	idx: number;
	file: any;
	chargeToFileOptions: any;	
	removeFile: (deletedFileObject: any, index: number) => void;
	totalFiles: number;
	handleChange: (e: any, control?: string, idx?: number) => void;
}

const DeleteIcon = {
	deleteIcon: require('../../assets/images/delete_icon.png')
};

export function FileCharge(props: Props) {
	const classes = styles();	
	const { register, errors } = useFormContext();
	// const methods = useFormContext();

	return (
		<React.Fragment key={"container" + props.idx}>
			<Grid container item xs={12} key={"gridcontainer" + props.idx} spacing={0}>
				<Grid item xs={9}>
					<img src={DeleteIcon.deleteIcon} alt="" className={classes.deleteFileIcon} onClick={e => props.removeFile(props.file, props.idx)} />
					<label className={classes.fileLink}>{props.file.path}</label></Grid>
				<Grid item xs={3}>
					<ReactSelect
						onChange={e => props.handleChange(e, "fileCharges", props.idx)}
						options={props.chargeToFileOptions}
						isMulti={true}
						isClearable={false}
						components={{IndicatorSeparator: () => null }}
						key={"fileCharge" + props.idx}
						value={props.file.fileCharges}
						defaultValue={0}
						className={classes.reactselect}						
						reg={register}
						theme={theme => ({
							...theme,							
							borderRadius: 1,							
							colors: {
								...theme.colors,
								primary25: '#d2f6e3',
								primary50: '#2b995f',
								primary: '#2b995f',
							},
						})} /> 
				</Grid>
						
			</Grid>
			{props.totalFiles > props.idx + 1 ? <hr className={classes.hr} /> : null}	

		</React.Fragment>
	);
}

