﻿// prettier-ignore
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
//import Draggable from 'react-draggable';
import { useActions } from "../../actions";
import * as DS1986Actions from "../../actions/DS1986";
import Background from '../../assets/images/DS249_1204.png';

interface Props {
	open: boolean;
	onClose: () => void;
}

export function DS1986Dialog(props: Props) {
	const { open, onClose } = props;
	const classes = useStyles();
	const [newDS1986Text, setNewDS1986Text] = React.useState("");
	const cDS1986Actions: any = useActions(DS1986Actions);

	const handleCreate = (event: any) => {
		cDS1986Actions.insDS1986({
			id: Math.random(),
			completed: false,
			DS1986ReceiptNo: event.target.value,
			TicketRange: '1000-1999',
			CartonNoRange: '10-89',
			NoOfParts: 4,
			ModifiedOn: Date.now,
			ModifiedBy: 'sef'
		});
		// reset text if user reopens the dialog
		setNewDS1986Text("new");
		onClose();

	};

	const handleClose = () => {
		// reset text if user reopens the dialog
		setNewDS1986Text("");
		onClose();

	};

	const handleUpdate = (event: any) => {
		cDS1986Actions.uptDS1986({
			id: Math.random(),
			completed: false,
			DS1986ReceiptNo: event.target.value,
			TicketRange: '1000-1999',
			CartonNoRange: '10-89',
			NoOfParts: 4,
			ModifiedOn: Date.now,
			ModifiedBy: 'sef'
		});
		setNewDS1986Text("updated");

	};

	return (
		<Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
			<DialogTitle>Add a new DS1986</DialogTitle>
			<DialogContent>
				<DialogContentText>
					THE CITY OF NEW YORK Department of Sanitation<br />
					NOTICE OF VIOLATION & HEARING Form DS1986
				</DialogContentText>
				<TextField
					autoFocus
					margin="dense"
					id="DS1986ReceiptNo"
					label="RECEIPT NUMBER"
					type="string"
					fullWidth
					value={newDS1986Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
				<TextField
					margin="dense"
					id="BoxNumberedFrom"
					label="First Box Number"
					type="string"
					size="medium"
					required
					value={newDS1986Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
				<TextField
					margin="dense"
					id="BoxNumberedTo"
					label="Last Box Number"
					type="string"
					size="small"
					required
					value={newDS1986Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
			</DialogContent>
			<DialogActions>
				<Button onClick={handleUpdate} color="secondary">
					Save/Update
				</Button>
				<Button onClick={handleCreate} color="primary">
					MAKE NEW DS1986
				</Button>
				<Button onClick={handleClose} color="primary">
					Done
				</Button>
			</DialogActions>

		</Dialog>

	);
}

const useStyles = makeStyles({
	newDS1986: {
		flexGrow: 1,
		height: "10%",
		paddingLeft: 15,
		paddingRight: 15,
		opacity: 60,
		backgroundImage: `url( ${Background} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '82px 103px',
		backgroundPosition: 'left top',
	},
	pending: {
		backgroundColor: "white",
	},
	approved: {
		backgroundColor: "yellow"
	},
	textField: {
		width: "80%",
		margin: 20,
	},
});
