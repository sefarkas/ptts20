﻿export interface UsedTicketRange {
    [x: string]: any;
    ECBSummonsNumberFrom: string;
    ECBSummonsNumberTo: string;
    DSform: string;
    ReceiptNumber: string;
    NOVnumberBeingConsidered?: string;
}


export interface tSpecialDistrict {
    [x: string]: any;
    Id: number;
    PayCode: string;
    Description: string;
    Remarks: string;
    SplinterDisrtict: boolean;
    MappingDistictNTI: string;
}

export enum SpecialDistrictActions {
    ADD_SpecialDistrict = "ADD_SpecialDistrict",
    UPDATE_SpecialDistrict = "UPDATE_SpecialDistrict",
    DELETE_SpecialDistrict = "DELETE_SpecialDistrict",
    COMPLETE_SpecialDistrict = "COMPLETE_SpecialDistrict",
    REVERT2INCOMPLETE_SpecialDistrict = "REVERT2INCOMPLETE_SpecialDistrict",
    RESET_SpecialDistrict = "RESET_SpecialDistrictList",
}

interface SpecialDistrictActionType<T, P> {
    type: T;
    payload: P;
}

export type SpecialDistrictAction =
    | SpecialDistrictActionType<typeof SpecialDistrictActions.ADD_SpecialDistrict, tSpecialDistrict>
    | SpecialDistrictActionType<typeof SpecialDistrictActions.COMPLETE_SpecialDistrict, number>
    | SpecialDistrictActionType<typeof SpecialDistrictActions.REVERT2INCOMPLETE_SpecialDistrict, number>
    | SpecialDistrictActionType<typeof SpecialDistrictActions.UPDATE_SpecialDistrict, tSpecialDistrict>
    | SpecialDistrictActionType<typeof SpecialDistrictActions.DELETE_SpecialDistrict, number>
    ;
