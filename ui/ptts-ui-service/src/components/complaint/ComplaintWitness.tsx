﻿import { Typography, FormControlLabel, Checkbox } from "@material-ui/core";
import React, { useEffect } from 'react';
import { useFormContext } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import ReactSelect, { components } from "react-select";
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers';
import styles from "./complaintStyles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";

interface Props {
	idx: number;
	witness: any;
	witnessOptions: any;
	handleChange: (e: any, control?: string, idx?: number) => void;
	handleRemoveWitness: (event: any, idx: number) => void;	
	handleInputChange: (e: any, caller: string) => void;
}

library.add(faSearch);

const CaretDownIcon = () => {
	return <FontAwesomeIcon icon="search" />;
};

const DropdownIndicator = (props: any) => {
	return (
		<components.DropdownIndicator {...props}>
			<CaretDownIcon />
		</components.DropdownIndicator>
	);
};

const DeleteIcon = {
	deleteIcon: require('../../assets/images/delete_icon.png')
};

export function ComplaintWitness(props: Props) {
	const classes = styles();	
	const { register, errors } = useFormContext();
	const [nonDSNYWitness, setNonDSNYWitness] = React.useState(false);

	const handleCheckboxChange = (e: any) => {
		setNonDSNYWitness(e.target.checked);
		props.handleChange(e, "nonDSNY", props.idx);
	}

	return (
		<React.Fragment key={"container" + props.idx}>
			<Grid container item xs={9} key={"gridcontainer" + props.idx} spacing={0}>
				<Grid item xs={3} className={classes.gridCell} key={"gridcellwitnessname" + props.idx}>
					<label className={classes.labelInline} key={"labelwitnessname" + props.idx}>Witness Name</label>
					<FormControlLabel
						value="nonDSNY"
						key={"dsny" + props.idx}						
						control={<Checkbox
							size="small"
							color="default"							
							className={classes.checkboxInner}
							key={"checkdsny" + props.idx}
							value={props.witness.nonDSNY}
							onChange={e => handleCheckboxChange(e)}
							/>}
						label={<Typography className={classes.label}>Non-DSNY</Typography>}
						labelPlacement="end"
					/>					
				</Grid>				
				<Grid item xs={3}>
					{props.idx > 0 ? <span onClick={e => props.handleRemoveWitness(e, props.idx)}><Typography className={classes.labelRemoveXS3}><img src={DeleteIcon.deleteIcon} alt="" className={classes.deleteIcon} />Remove</Typography></span> : null}
				</Grid>
				<Grid item xs={6} className={classes.gridCell}>
					<label className={classes.label} key={"labelwitnessinfo" + props.idx}>Additional Witness Information</label></Grid>
				<Grid item xs={6} className={classes.gridCell}>
					{nonDSNYWitness === false ? <ReactSelect
						onChange={e => props.handleChange(e, "witness", props.idx)}
						options={props.witnessOptions}
						onInputChange={e => props.handleInputChange(e, "witness")}
						components={{ DropdownIndicator }}
						key={"witnessname" + props.idx}
						value={props.witness.witnessName}
						defaultValue={props.witness.witnessName}
						placeholder="Enter Name or Ref No."
						className={classes.reactselect}
						reg={register}
						theme={theme => ({
							...theme,
							borderRadius: 1,
							colors: {
								...theme.colors,
								primary25: '#d2f6e3',
								primary50: '#2b995f',
								primary: '#2b995f',
							},
						})} /> : <input name="nonDSNYWitnessName" className={classes.input} onChange={e => props.handleChange(e, "nonDSNYWitnessName", props.idx)} />}
					{errors.witnesses && <label className={classes.error}>{errors.witnesses.message}</label>}
				</Grid>
				<Grid item xs={6} className={classes.gridCell}>
					<input value={props.witness.additionalInfo} className={classes.input} key={"witnessinfo" + props.idx} onChange={e => props.handleChange(e, "witnessInfo", props.idx)} />
				</ Grid>
			</Grid>
			<Grid item xs={3} className={classes.gridSpacerCell} />
		</React.Fragment>
	);
}

