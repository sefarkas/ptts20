﻿using System.Data.Entity.Migrations;
using System.Linq;
using System.Security.AccessControl;
using System.Web.Http;
using System.Web.Http.Cors;
using PTTSapiEFdb;

namespace apiEF.Controllers
{
    [RoutePrefix( "PTTS20/api/IUD" )]
    [EnableCors( origins: "*", headers: "*", methods: "*" )]
    // [EnableCors( origins: "https://localhost:18144", headers: "*", methods: "*" )]
    // has to refer to the client IP and port; see also WebApiConfig Register and Web.Config system.webServer handlers
    public class PTTSiudController : ApiController
    {
        private bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
        {
            System.Double result;
            return System.Double.TryParse(val, NumberStyle,
                System.Globalization.CultureInfo.CurrentCulture, out result);
        }

        [Route( "InsUptOneECB603" )]
        [HttpPost]
        public string FuncInsUptOneECB603( [FromBody] ECB603 jsonForm )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();

            long ID = jsonForm.ID; // zero for new Form            ;
            var NoOfParts = jsonForm.NoOfParts; // int,
            var CartonQty = jsonForm.CartonQty; //  int,
            var TicketNoFrom = jsonForm.TicketNoFrom; //  bigint,  
            var TicketNoTo = jsonForm.TicketNoTo; //  bigint,
            var CartonNoFrom = jsonForm.CartonNoFrom; //  bigint,  
            var CartonNoTo = jsonForm.CartonNoTo; //  bigint,
            var OrderNo = jsonForm.OrderNo; //  nvarchar( 50 ) = NULL,  
            var CustomerOrderNo = jsonForm.CustomerOrderNo; //  nvarchar(50) = NULL,  
            var FactoryJobNo = jsonForm.FactoryJobNo; //  nvarchar(150) = NULL,  
            var ModifiedBy = jsonForm.ModifiedBy; //  nvarchar(150),
            System.Data.Entity.Core.Objects.ObjectParameter identityId = new System.Data.Entity.Core.Objects.ObjectParameter( "identityId", 0 );

            try
            {
                db.ECB603_InsertRecord(ID,NoOfParts,CartonQty,
                        TicketNoFrom,TicketNoTo, CartonNoFrom, CartonNoTo,
                        OrderNo, CustomerOrderNo, FactoryJobNo, ModifiedBy, identityId );
                string buildReceiptNo = "";
                if (ID > 0)
                {
                    buildReceiptNo = jsonForm.ReceiptNo;
                }
                else
                {
                    buildReceiptNo = "000000000" + (ID > 0 ? ID.ToString() : identityId.Value.ToString());
                    buildReceiptNo = buildReceiptNo.Substring( buildReceiptNo.Length - 9, 9 );
                    var createMonth = jsonForm.CreatedOn.Substring( 0, 3 );
                    createMonth = createMonth.Replace( "Jan", "01" );
                    createMonth = createMonth.Replace( "Feb", "02" );
                    createMonth = createMonth.Replace( "Mar", "03" );
                    createMonth = createMonth.Replace( "Apr", "04" );
                    createMonth = createMonth.Replace( "May", "05" );
                    createMonth = createMonth.Replace( "Jun", "06" );
                    createMonth = createMonth.Replace( "Jul", "07" );
                    createMonth = createMonth.Replace( "Aug", "08" );
                    createMonth = createMonth.Replace( "Sep", "09" );
                    createMonth = createMonth.Replace( "Oct", "10" );
                    createMonth = createMonth.Replace( "Nov", "11" );
                    createMonth = createMonth.Replace( "Dec", "12" );
                    createMonth = createMonth.Replace( " ", "" );
                    buildReceiptNo = // Oct  5 2020  4:19PM
                        "6"
                        + jsonForm.CreatedOn.Substring( 7, 4 )
                        + createMonth
                        + (jsonForm.CreatedOn.Substring( 4, 2 ))
                        + "ENHQ" + buildReceiptNo;
                }
                return buildReceiptNo; //success                     
            }
            catch (System.Exception ex)
            {
                var errmsg = ex.Message;
                if (ex.InnerException != null)
                {
                    errmsg = errmsg + "|" + ex.InnerException.Message.ToString();
                }
                jsonForm.CustomerOrderNo = errmsg;
                return "000000000" + identityId.ToString(); // exception;
            }


        }

        [Route( "InsUptOneDS21HDR" )]
        [HttpPost]
        public string FuncInsUptOneDS21HDR( [FromBody] DS21_Header jsonForm )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            try
            {
                db.DS21_UpdateHeaderRecord(
                    jsonForm.Year, //Nullable<int> year, 
                    jsonForm.IssuingOfficerSignature, //string issuingOfficerSignature, 
                    jsonForm.Command, //string command, 
                    jsonForm.IssuingOfficerName, //string issuingOfficerName, 
                    jsonForm.TaxRegNumber, //string taxRegNumber, 
                    jsonForm.Agency, //string agency, 
                    jsonForm.DateCardStarted, //Nullable < System.DateTime > dateCardStarted, 
                    jsonForm.DateReturned,  //Nullable < System.DateTime > dateReturned, 
                    jsonForm.AssignedDistrict, //string assignedDistrict, 
                    jsonForm.IssuingOfficerTitle, //string issuingOfficerTitle, 
                    jsonForm.OfficerInChargeName, //string officerInChargeName, 
                    jsonForm.OfficerInChargeTitle, //string officerInChargeTitle, 
                    jsonForm.DateCardCompleted, //Nullable < System.DateTime > dateCardCompleted, 
                    jsonForm.Remarks, //string remarks, 
                    jsonForm.DS1984Number, //string dS1984Number, 
                    jsonForm.FormStatusID, //Nullable<int> formStatusID, 
                    jsonForm.ModifiedBy  //string modifiedBy
                    );
                return jsonForm.DS1984Number;
            }
            catch(System.Exception ex)
            {
                // "The UPDATE statement conflicted with the FOREIGN KEY constraint \"FK__DS21_Head__FormS__31EC6D26\". 
                // The conflict occurred in 
                // database \"PTTS20\", table \"dbo.DS21_Status\", column 'ID'.\r\nThe statement has been terminated."
                return ex.Message;
            }

        }

        [Route( "InsUptOneDS1983" )]
        [HttpPost]
        public string FuncInsUptOneDS1983( [FromBody] DS1983 jsonForm )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();

            long ID = jsonForm.ID; // zero for new Form            ;
            var ReceiptNumber = jsonForm.ReceiptNumber;
            var MessengerName = jsonForm.MessengerName;
            var SummonsCount = jsonForm.SummonsCount;
            var SummonsFromNumber = jsonForm.SummonsFromNumber;
            var SummonsToNumber = jsonForm.SummonsToNumber;
            var Borough = jsonForm.Borough;
            var DistributorName = jsonForm.DistributorName;
            var DistributorSignature = jsonForm.DistributorSignature;
            var DistributorSignedDate = jsonForm.DistributorSignedDate;
            var MessengerSignature = jsonForm.MessengerSignature;
            var MessengerTitle = jsonForm.MessengerTitle;
            var MessengerSignedDate = jsonForm.MessengerSignedDate;
            var AcknowledgedBy = jsonForm.AcknowledgedBy;
            var BoxNumber = jsonForm.BoxNumber;
            var FormStatus = jsonForm.FormStatus;
            // var ModifiedOn = jsonForm.ModifiedOn;
            var ModifiedBy = jsonForm.CreatedBy; // jsonForm.ModifiedBy; in the SPROC modified by is used for both created  by and modified by
            var BadgeNo = jsonForm.BadgeNo;
            var DistributorRefNo = jsonForm.DistributorRefNo;
            // var CreatedBy = jsonForm.CreatedBy;
            // var CreatedDate = jsonForm.CreatedDate;
            var DistrictID = jsonForm.DistrictID;
            var PayCode = jsonForm.PayCode;
            // var IsVoid = jsonForm.IsVoid;
            System.Data.Entity.Core.Objects.ObjectParameter identityId = new System.Data.Entity.Core.Objects.ObjectParameter( "identityId", typeof( System.Int64 ) );

            // Nullable<long> iD, string receiptNumber, string messengerName, 
            // Nullable< int > summonsCount, Nullable<long> summonsFromNumber, 
            //    Nullable< long > summonsToNumber, string borough, 
            //    string distributorName, string distributorSignature, 
            //    Nullable< System.DateTime > distributorSignedDate, 
            //    string messengerSignature, string messengerTitle, 
            //    Nullable< System.DateTime > messengerSignedDate, 
            //    string formStatus, string acknowledgedBy, Nullable< int > boxNumber, 
            //    string modifiedBy, ObjectParameter identityId, string badgeNo, 
            //    string distributorRefNo, string payCode, Nullable< int > districtId

            try
            {
                db.DS1983_InsertRecord( ID, ReceiptNumber,
                                    MessengerName,
                                    SummonsCount,
                                    SummonsFromNumber,
                                    SummonsToNumber,
                                    Borough,
                                    DistributorName,
                                    DistributorSignature,
                                    DistributorSignedDate,
                                    MessengerSignature,
                                    MessengerTitle,
                                    MessengerSignedDate,
                                    FormStatus,
                                    AcknowledgedBy,
                                    BoxNumber,
                                    ModifiedBy, // SPROC uses ModifiedBy for CreatedBy during original insert
                                    identityId,
                                    BadgeNo,
                                    DistributorRefNo,
                                    PayCode,
                                    DistrictID );


                //string buildReceiptNo;
                //if (ID > 0)
                //{
                //    buildReceiptNo = jsonForm.ReceiptNumber;
                //}
                //else
                //{
                //    buildReceiptNo = "000000000" + (ID > 0 ? ID.ToString() : identityId.Value.ToString());
                //    buildReceiptNo = buildReceiptNo.Substring( buildReceiptNo.Length - 9, 9 );
                //    var createMonth = jsonForm.CreatedDate.Substring( 0, 3 );
                //    createMonth = createMonth.Replace( "Jan", "01" );
                //    createMonth = createMonth.Replace( "Feb", "02" );
                //    createMonth = createMonth.Replace( "Mar", "03" );
                //    createMonth = createMonth.Replace( "Apr", "04" );
                //    createMonth = createMonth.Replace( "May", "05" );
                //    createMonth = createMonth.Replace( "Jun", "06" );
                //    createMonth = createMonth.Replace( "Jul", "07" );
                //    createMonth = createMonth.Replace( "Aug", "08" );
                //    createMonth = createMonth.Replace( "Sep", "09" );
                //    createMonth = createMonth.Replace( "Oct", "10" );
                //    createMonth = createMonth.Replace( "Nov", "11" );
                //    createMonth = createMonth.Replace( "Dec", "12" );
                //    createMonth = createMonth.Replace( " ", "" );
                //    buildReceiptNo = // Oct  5 2020  4:19PM
                //        "3"
                //        + jsonForm.CreatedDate.Substring( 7, 4 )
                //        + createMonth
                //        + (jsonForm.CreatedDate.Substring( 4, 2 ))
                //        + PayCode
                //        + buildReceiptNo;
                //}

                System.Int64 newID = System.Convert.ToInt64( identityId.Value );
                var brandNew1983 = db.DS1983.SingleOrDefault<DS1983>( aRow => aRow.ID == newID );
                if (brandNew1983 != null)
                {
                    return brandNew1983.ReceiptNumber;
                }
                else
                {
                    throw new System.Exception("Receipt number for ID " + System.Convert.ToInt64( identityId.Value).ToString() + " needs correction");
                }
            }
            catch (System.Exception ex)
            {
                var errmsg = ex.Message;
                if (ex.InnerException != null)
                {
                    errmsg = errmsg + "|" + ex.InnerException.Message.ToString();
                }
                jsonForm.ModifiedBy = errmsg;
                return "000000000" + identityId.ToString(); // exception;
            }


        }


        [Route( "InsUptOneDS1984" )]
        [HttpPost]
        public string FuncInsUptOneDS1984( [FromBody] DS1984 jsonForm )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();

            long ID = jsonForm.ID; // zero for new Form            ;
            var ReceiptNumber = jsonForm.ReceiptNumber;
            var District = jsonForm.District;
            var ReceivingOfficerName = jsonForm.ReceivingOfficerName;
            var ReceivingOfficerTitle = jsonForm.ReceivingOfficerTitle;
            var BoxNumberedFrom = jsonForm.BoxNumberedFrom;
            var BoxNumberedTo = jsonForm.BoxNumberedTo;
            var ReceivingOfficerSignature = jsonForm.ReceivingOfficerSignature;
            var DistributorName = jsonForm.DistributorName;
            var DistributorSignature = jsonForm.DistributorSignature;
            var DistributorSignedDate = jsonForm.DistributorSignedDate;
            var TaxRegNumber = jsonForm.TaxRegNumber;
            var FormStatus = jsonForm.FormStatus;
            var AcknowledgedBy = jsonForm.AcknowledgedBy;
            //var ModifiedOn = jsonForm.ModifiedOn;
            var ModifiedBy = jsonForm.CreatedBy; // jsonForm.ModifiedBy; in the SPROC modified by is used for both created  by and modified by
            var RecOfficerRefNo = jsonForm.RecOfficerRefNo;
            var DistributorRefNo = jsonForm.DistributorRefNo;
            var DS1983ReceiptNo = jsonForm.DS1983ReceiptNo;
            var DistrictId = jsonForm.DistrictId;
            //var CreatedBy = jsonForm.CreatedBy;
            //var CreatedDate = jsonForm.CreatedDate;
            var Paycode = jsonForm.Paycode;
            System.Data.Entity.Core.Objects.ObjectParameter identityId = new System.Data.Entity.Core.Objects.ObjectParameter( "identityId", typeof( System.Int64 ) );

            try
            {
                db.DS1984_InsertRecord( ID,
                                        ReceiptNumber,
                                        District,
                                        ReceivingOfficerName,
                                        ReceivingOfficerTitle,
                                        BoxNumberedFrom,
                                        BoxNumberedTo,
                                        DistributorName,
                                        DistributorSignature,
                                        DistributorSignedDate,
                                        ReceivingOfficerSignature,
                                        TaxRegNumber,
                                        FormStatus,
                                        AcknowledgedBy,
                                        ModifiedBy,
                                        identityId,
                                        RecOfficerRefNo,
                                        DistributorRefNo,
                                        Paycode,
                                        DistrictId
                                         );

                System.Int64 newID = System.Convert.ToInt64( identityId.Value );
                var brandNew1984 = db.DS1984.SingleOrDefault<DS1984>( aRow => aRow.ID == newID );
                if (brandNew1984 != null)
                {
                    return brandNew1984.ReceiptNumber;
                }
                else
                {
                    throw new System.Exception( "DS1984 Receipt number for ID " + System.Convert.ToInt64( identityId.Value ).ToString() + " needs correction" );
                }
            }
            catch (System.Exception ex)
            {
                var errmsg = ex.Message;
                if (ex.InnerException != null)
                {
                    errmsg = errmsg + "|" + ex.InnerException.Message.ToString();
                }
                jsonForm.ModifiedBy = errmsg;
                throw new System.Exception(errmsg); // "000000000" + identityId.ToString(); // exception;
            }


        }

        [Route( "InsUptOneDS1986" )]
        [HttpPost]
        public string FuncInsUptOneDS1986( [FromBody] DS1986B jsonForm )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();

            long ID = jsonForm.ID; // zero for new Form            ;
            var ReferenceNo = jsonForm.ReferenceNo;
            var Name = jsonForm.Name;
            var District = jsonForm.District;
            var BadgeNo = jsonForm.BadgeNo;
            var NovasNo = jsonForm.NovasNo;
            var Ds1984No = jsonForm.Ds1984No;
            var DS1986ErrorId = jsonForm.DS1986ErrorId;
            var DS1986BDate = jsonForm.DS1986BDate;
            var OtherRemarks = jsonForm.OtherRemarks;
            var Remarks = jsonForm.Remarks;
            var CreatedBy = jsonForm.CreatedBy;
            var CreatedDate = jsonForm.CreatedDate;
            var ModifiedBy = jsonForm.ModifiedBy;
            var ModifiedDate = jsonForm.ModifiedDate;
            var DS1984District = jsonForm.DS1984District;
            var ReceiptNo = jsonForm.ReceiptNo;
            var DS1984DistrictName = jsonForm.DS1984DistrictName;
            System.Data.Entity.Core.Objects.ObjectParameter identityId = new System.Data.Entity.Core.Objects.ObjectParameter( "identityId", typeof( System.Int64 ) );

            try
            {
                db.DS1986B_InsertUpdate( ID,
                                        ReferenceNo,
                                        Name,
                                        District,
                                        BadgeNo,
                                        NovasNo,
                                        Ds1984No,
                                        DS1986ErrorId,
                                        OtherRemarks,
                                        DS1986BDate,
                                        Remarks,
                                        CreatedBy,
                                        ModifiedBy,
                                        DS1984District.ToString(),
                                        identityId
                                         );

                System.Int64 newID = System.Convert.ToInt64( identityId.Value );
                var brandNew1986B = db.DS1986B.SingleOrDefault<DS1986B>( aRow => aRow.ID == newID );
                if (brandNew1986B != null)
                {
                    return brandNew1986B.ID.ToString();
                }
                else
                {
                    throw new System.Exception( "DS1986B Receipt number for ID " + System.Convert.ToInt64( identityId.Value ).ToString() + " needs correction" );
                }
            }
            catch (System.Exception ex)
            {
                var errmsg = ex.Message;
                if (ex.InnerException != null)
                {
                    errmsg = errmsg + "|" + ex.InnerException.Message.ToString();
                }
                jsonForm.ModifiedBy = errmsg;
                throw new System.Exception( errmsg ); // "000000000" + identityId.ToString(); // exception;
            }


        }
        
        [Route( "InsUptOneNOV" )]
        [HttpPost]
        public string FuncInsUptOneNOV( [FromBody] DS21 jsonForm )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            if (isNumeric( jsonForm.ECBSummonsNumber, System.Globalization.NumberStyles.Number ))
            {
                long ID = int.Parse( jsonForm.ECBSummonsNumber ); // zero for new Form            ;
                var varECBSummonsNumber = jsonForm.ECBSummonsNumber;
                var varCode = jsonForm.Code;
                var varViolationType = jsonForm.ViolationType;
                System.Nullable<int> varDateServedMonth = jsonForm.DateServedMonth;
                System.Nullable<int> varDateServedDay = jsonForm.DateServedDay;
                var varDistOfOccurrence = jsonForm.DistOfOccurrence;
                var varInitial = jsonForm.Initial;
                var varTitle = jsonForm.Title;
                var varDS1984Number = jsonForm.DS1984Number;
                System.Nullable<bool> varNotarized = jsonForm.Notarized;
                System.Nullable<bool> varVoided = jsonForm.Voided;
                System.Nullable<int> varTicketStatusID = jsonForm.TicketStatusID;
                var varModifiedBy = jsonForm.ModifiedBy;
                System.Nullable<long> varDateServedYear = jsonForm.DateServedYear;
                System.Data.Entity.Core.Objects.ObjectParameter identityId = new System.Data.Entity.Core.Objects.ObjectParameter( "identityId", typeof( System.Int64 ) );

                try
                {
                    db.DS21_UpdateRecord(
                        varECBSummonsNumber,
                        varCode,
                        varViolationType,
                        varDateServedMonth,
                        varDateServedDay,
                        varDistOfOccurrence,
                        varInitial,
                        varTitle,
                        varDS1984Number,
                        varNotarized,
                        varVoided,
                        varTicketStatusID,
                        varModifiedBy,
                        varDateServedYear
                                             );

                    var brandNewNOV = db.DS21.SingleOrDefault<DS21>( aRow => aRow.ECBSummonsNumber == varECBSummonsNumber );
                    if (brandNewNOV != null)
                    {
                        return brandNewNOV.ECBSummonsNumber;
                    }
                    else
                    {
                        throw new System.Exception( "DS21 NOV number for " + varECBSummonsNumber + " needs correction" );
                    }
                }
                catch (System.Exception ex)
                {
                    var errmsg = ex.Message;
                    if (ex.InnerException != null)
                    {
                        errmsg = errmsg + "|" + ex.InnerException.Message.ToString();
                    }
                    jsonForm.ModifiedBy = errmsg;
                    throw new System.Exception( errmsg ); // "000000000" + identityId.ToString(); // exception;
                }



            }
            else
            {
                throw new System.Exception( "NOV model has no ECB Summons Number." );
            }
        }

        [Route("InsUptOneDS1704")]
        [HttpPost]
        public string FuncInsUptOneDS1704([FromBody] DS1704 jsonForm)
        {
            PaperTicketsEntities db = new PaperTicketsEntities();

            var ECBSummonsNumber = jsonForm.ECBSummonsNumber;

            var PlaceOfOccurence = jsonForm.PlaceOfOccurence;
            var Remarks = jsonForm.Remarks;
            var ModifiedBy = jsonForm.ModifiedBy;

            var VoidDate = jsonForm.VoidDate;
            var VoidReasonId = jsonForm.VoidReasonId == null ? 0 : jsonForm.VoidReasonId;

            System.Data.Entity.Core.Objects.ObjectParameter identityId = new System.Data.Entity.Core.Objects.ObjectParameter("identityId", typeof(System.Int64));

            try
            {
                // SPROC performs internal look ups based upon inputs provided here.
                // Not all vars above are used to CRUD dbo.DS1704 
                db.DS1704_InsertUpdateRecord(ECBSummonsNumber,
                                            null, // timeServed,
                                            PlaceOfOccurence,
                                            Remarks,
                                            ModifiedBy,
                                            VoidDate,
                                            VoidReasonId.ToString()
                                         );

                System.Int64 newID = System.Convert.ToInt64(identityId.Value);
                var brandNew1704A = db.DS1704.SingleOrDefault<DS1704>(aRow => aRow.ECBSummonsNumber == ECBSummonsNumber);
                if (brandNew1704A != null)
                {
                    return brandNew1704A.ECBSummonsNumber;
                }
                else
                {
                    throw new System.Exception("DS1704A for ECBSummonsNumber " + ECBSummonsNumber + " needs correction");
                }
            }
            catch (System.Exception ex)
            {
                var errmsg = ex.Message;
                if (ex.InnerException != null)
                {
                    errmsg = errmsg + "|" + ex.InnerException.Message.ToString();
                }
                jsonForm.ModifiedBy = errmsg;
                throw new System.Exception(errmsg); // "000000000" + identityId.ToString(); // exception;
            }


        }


        [Route("DelOneDS1704")]
        [HttpPost]
        public string FuncDelOneDS1704([FromBody] DS1704 jsonForm)
        {
            PaperTicketsEntities db = new PaperTicketsEntities();

            var ECBSummonsNumber = jsonForm.ECBSummonsNumber;

            var PlaceOfOccurence = jsonForm.PlaceOfOccurence;
            var Remarks = jsonForm.Remarks;
            var ModifiedBy = jsonForm.ModifiedBy;

            var VoidDate = jsonForm.VoidDate;
            var VoidReasonId = jsonForm.VoidReasonId == null ? 0 : jsonForm.VoidReasonId;

            System.Data.Entity.Core.Objects.ObjectParameter identityId = new System.Data.Entity.Core.Objects.ObjectParameter("identityId", typeof(System.Int64));

            try
            {
                // SPROC performs internal look ups based upon inputs provided here.
                // Not all vars above are used to CRUD dbo.DS1704 
                db.DS1704_InsertUpdateRecord(ECBSummonsNumber,
                                            null, // timeServed,
                                            null, // PlaceOfOccurence,
                                            "unvoided on " + System.DateTime.Now.ToString(), // Remarks,
                                            ModifiedBy,
                                            null, // VoidDate,
                                            null // VoidReasonId.ToString()
                                         );

                System.Int64 newID = System.Convert.ToInt64(identityId.Value);
                var brandNew1704A = db.DS1704.SingleOrDefault<DS1704>(aRow => aRow.ECBSummonsNumber == ECBSummonsNumber);
                if (brandNew1704A != null)
                {
                    return brandNew1704A.ECBSummonsNumber;
                }
                else
                {
                    throw new System.Exception("DS1704A for ECBSummonsNumber " + ECBSummonsNumber + " needs correction");
                }
            }
            catch (System.Exception ex)
            {
                var errmsg = ex.Message;
                if (ex.InnerException != null)
                {
                    errmsg = errmsg + "|" + ex.InnerException.Message.ToString();
                }
                jsonForm.ModifiedBy = errmsg;
                throw new System.Exception(errmsg); // "000000000" + identityId.ToString(); // exception;
            }


        }

    }
}
