﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using apiEF.Models;
using Microsoft.EntityFrameworkCore.Internal;
using PTTSapiEFdb;
using static apiEF.Models.clsUsedTktRanges;

namespace apiEF.Controllers
{
    public partial class DS21HDR
    {
        public Nullable<int> Year
        {
            get; set;
        }
        public string IssuingOfficerSignature
        {
            get; set;
        }
        public string Command
        {
            get; set;
        }
        public string IssuingOfficerName
        {
            get; set;
        }
        public string TaxRegNumber
        {
            get; set;
        }
        public string Agency
        {
            get; set;
        }
        public Nullable<System.DateTime> DateCardStarted
        {
            get; set;
        }
        public Nullable<System.DateTime> DateReturned
        {
            get; set;
        }
        public string AssignedDistrict
        {
            get; set;
        }
        public string IssuingOfficerTitle
        {
            get; set;
        }
        public string OfficerInChargeName
        {
            get; set;
        }
        public string OfficerInChargeTitle
        {
            get; set;
        }
        public Nullable<System.DateTime> DateCardCompleted
        {
            get; set;
        }
        public string Remarks
        {
            get; set;
        }
        public string DS1984Number
        {
            get; set;
        }
        public Nullable<System.DateTime> ModifiedOn
        {
            get; set;
        }

        public string ModifiedBy
        {
            get; set;
        }
        public int FormStatusID
        {
            get; set;
        }
        public string IssuingOfficerRefNo
        {
            get; set;
        }
        public string CreatedBy
        {
            get; set;
        }
        public Nullable<System.DateTime> CreatedOn
        {
            get; set;
        }

        public Nullable<long> BoxNumberedFrom
        {
            get; set;
        }
        public Nullable<long> BoxNumberedTo
        {
            get; set;
        }
    }


    [RoutePrefix( "PTTS20/api" )]
    [EnableCors( origins: "*", headers: "*", methods: "*" )]
    // [EnableCors( origins: "https://localhost:18144", headers: "*", methods: "*" )]
    // has to refer to the client IP and port; see also WebApiConfig Register and Web.Config system.webServer handlers
    public class PTTSController : ApiController
    {
        private IQueryable<mdlUsedTktRanges> qECB603;
        private IQueryable<mdlUsedTktRanges> qDS1983;
        private IQueryable<mdlUsedTktRanges> qDS1984;
        // private IQueryable<mdlUsedTktRanges> qDS21HDR;
        private IQueryable<mdlUsedTktRanges> qIsGoodNOVnumber;
        private IQueryable<mdlUsedTktRanges> qDS1986;
        private IQueryable<mdlUsedTktRanges> qDS21;
        private IQueryable<mdlUsedTktRanges> qDS1704;

        [Route( "GetECB603List" )]
        public IQueryable<ECB603> GetECB603List()
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            var q = db.ECB603.AsQueryable();
            return q;
        }

        [Route( "GetECB603List/{rcpt}" )]
        public ECB603 GetECB603List(string rcpt)
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            IQueryable<ECB603> rslt = db.ECB603.AsQueryable();
            ECB603 oneFrm = rslt.FirstOrDefault<ECB603>( afrm => afrm.ReceiptNo == rcpt );
            return oneFrm;

        }

        [Route( "GetUsedTicketRange" )]
        [HttpPost]
        public List<mdlUsedTktRanges> GetUsedTicketRange(
            [FromBody]
            mdlUsedTktRanges dsform
            )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            try { 
                 switch (dsform.DSform)
                {
                    case "ECB603":
                        IQueryable<ECB603> rsltECB603 = db.ECB603.AsQueryable();
                        var qECB603pre = rsltECB603.Where( aRow =>
                                            aRow.TicketNoFrom.ToString().CompareTo( dsform.ECBSummonsNumberFrom ) >= 0
                                            &&
                                            aRow.TicketNoTo.ToString().CompareTo( dsform.ECBSummonsNumberTo ) <= 0
                                        );
                         qECB603 = from curRow in qECB603pre
                                   select new mdlUsedTktRanges {
                                    ECBSummonsNumberFrom = curRow.TicketNoFrom.ToString(),
                                    ECBSummonsNumberTo = curRow.TicketNoTo.ToString(),
                                    DSform = dsform.DSform,
                                    ReceiptNumber= curRow.ReceiptNo // mostly for diagnostics
                                 } ;
                        break;
                    case "DS1983":
                        IQueryable<DS1983> rsltDS1983 = db.DS1983.AsQueryable();
                        var qDS1983pre = rsltDS1983.Where( aRow =>
                        // is dsform.ECBSummonsNumberFrom between aRow.SummonsFromNumber and aRow.SummonsToNumber
                            dsform.ECBSummonsNumberFrom.CompareTo(  aRow.SummonsFromNumber.ToString()) >= 0
                            &&
                            dsform.ECBSummonsNumberFrom.CompareTo(  aRow.SummonsToNumber.ToString()) <= 0
                            ||
                            dsform.ECBSummonsNumberTo.CompareTo( aRow.SummonsFromNumber.ToString() ) >= 0
                            &&
                            dsform.ECBSummonsNumberTo.CompareTo( aRow.SummonsToNumber.ToString() ) <= 0
                        );

                        qDS1983 = from aRow in qDS1983pre
                                  select new mdlUsedTktRanges
                                {
                                    ECBSummonsNumberFrom = aRow.SummonsFromNumber.ToString(),
                                    ECBSummonsNumberTo = aRow.SummonsToNumber.ToString(),
                                    DSform = dsform.DSform,
                                    ReceiptNumber = aRow.ReceiptNumber // mostly for diagnostics
                                };
                        break;
                    case "DS1984":
                        IQueryable<DS1984> rsltDS1984 = db.DS1984.AsQueryable();
                        // summons range is in terms of 
                        //    a prefix single digit of 1 or 2 for Department of Sanitation ECB NOVs
                        //    then a seven digit sequential NOV # 
                        //    suffixed with a single digit checksum
                        //    BoxNumberedFrom and BoxNumberedTo come in as nine digit ECB summons numbers
                        var qDS1984pre = rsltDS1984.Where( aRow =>
                            dsform.ECBSummonsNumberFrom.CompareTo( aRow.BoxNumberedFrom.ToString() ) >= 0
                            &&
                            dsform.ECBSummonsNumberFrom.CompareTo( aRow.BoxNumberedTo.ToString() ) <= 0
                            ||
                            dsform.ECBSummonsNumberTo.CompareTo( aRow.BoxNumberedFrom.ToString() ) >= 0
                            &&
                            dsform.ECBSummonsNumberTo.CompareTo( aRow.BoxNumberedTo.ToString() ) <= 0
                        );

                        qDS1984 = from aRow in qDS1984pre
                                  select new mdlUsedTktRanges
                                    {
                                        ECBSummonsNumberFrom = aRow.BoxNumberedFrom.ToString().Substring( 0, 8 ),
                                        ECBSummonsNumberTo = aRow.BoxNumberedTo.ToString().Substring(0,8),
                                        DSform = dsform.DSform,
                                        ReceiptNumber = aRow.ReceiptNumber // mostly for diagnostics
                                    };
                        break;
                    case "IsDS1984":
                        IQueryable<DS1984> rsltIsDS1984 = db.DS1984.AsQueryable();
                        var qIsDS1984pre = rsltIsDS1984.Where( aRow =>
                            aRow.ReceiptNumber.Equals( dsform.ReceiptNumber ) == true
                            );

                        qDS1984 = from aRow in qIsDS1984pre
                                  select new mdlUsedTktRanges
                                  {
                                      ECBSummonsNumberFrom = "0",
                                      ECBSummonsNumberTo = "0",
                                      DSform = "DS1984_OKay",
                                      ReceiptNumber = aRow.ReceiptNumber // mostly for diagnostics
                                  };
                        break;
                    case "IsGoodNOVnumber":
                        IQueryable<DS1984> allDS1984s = db.DS1984.AsQueryable();

                        var qIsPreGoodTkt = allDS1984s.Where( aRow =>
                            dsform.ECBSummonsNumberFrom.Substring(0,8).CompareTo( aRow.BoxNumberedFrom.ToString().Substring( 0, 8 ) ) >= 0
                            &&
                            dsform.ECBSummonsNumberFrom.Substring( 0, 8 ).CompareTo( aRow.BoxNumberedTo.ToString().Substring( 0, 8 ) ) <= 0
                            ||
                            dsform.ECBSummonsNumberTo.Substring( 0, 8 ).CompareTo( aRow.BoxNumberedFrom.ToString().Substring( 0, 8 ) ) >= 0
                            &&
                            dsform.ECBSummonsNumberTo.Substring( 0, 8 ).CompareTo( aRow.BoxNumberedTo.ToString().Substring( 0, 8 ) ) <= 0
                        );

                        qIsGoodNOVnumber = from aRow in qIsPreGoodTkt
                                  select new mdlUsedTktRanges
                                  {
                                      ECBSummonsNumberFrom = aRow.BoxNumberedFrom.ToString().Substring( 0, 8 ),
                                      ECBSummonsNumberTo = aRow.BoxNumberedTo.ToString().Substring( 0, 8 ),
                                      DSform = "NOVnumber_OKay",
                                      ReceiptNumber = aRow.ReceiptNumber // mostly for diagnostics
                                  };
                        break;
                    case "IsDS1984Tkt":
                        IQueryable<DS1984> rsltIsDS1984Tkt = db.DS1984.AsQueryable();
                        var qIsDS1984preTkt = rsltIsDS1984Tkt.Where( aRow =>
                            aRow.ReceiptNumber.Equals( dsform.ReceiptNumber ) == true
                            );

                        qIsDS1984preTkt = qIsDS1984preTkt.Where( aRow =>
                            dsform.ECBSummonsNumberFrom.CompareTo( aRow.BoxNumberedFrom.ToString() ) >= 0
                            &&
                            dsform.ECBSummonsNumberFrom.CompareTo( aRow.BoxNumberedTo.ToString() ) <= 0
                            ||
                            dsform.ECBSummonsNumberTo.CompareTo( aRow.BoxNumberedFrom.ToString() ) >= 0
                            &&
                            dsform.ECBSummonsNumberTo.CompareTo( aRow.BoxNumberedTo.ToString() ) <= 0
                        );

                        qDS1984 = from aRow in qIsDS1984preTkt
                                  select new mdlUsedTktRanges
                                  {
                                      ECBSummonsNumberFrom = aRow.BoxNumberedFrom.ToString().Substring( 0, 8 ),
                                      ECBSummonsNumberTo = aRow.BoxNumberedTo.ToString().Substring( 0, 8 ),
                                      DSform = "DS1984_OKay",
                                      ReceiptNumber = aRow.ReceiptNumber // mostly for diagnostics
                                  };
                        break;
                    case "DS1986":
                            IQueryable<DS1984> rsltDS1984novrange = db.DS1984.AsQueryable();
                        // summons range is in terms of 
                        //    a prefix single digit of 1 or 2 for Department of Sanitation ECB NOVs
                        //    then a seven digit sequential NOV # 
                        //    suffixed with a single digit checksum
                        //    BoxNumberedFrom and BoxNumberedTo come in as nine digit ECB summons numbers
                        var qDS1986pre = rsltDS1984novrange.Where( a1984 =>
                            a1984.ReceiptNumber.Equals( dsform.ReceiptNumber ) == true
                            );

                        qDS1986 = from a1984 in qDS1986pre
                                      select new mdlUsedTktRanges
                                      {
                                          ECBSummonsNumberFrom = a1984.BoxNumberedFrom.ToString().Substring( 0, 8 ),
                                          ECBSummonsNumberTo = a1984.BoxNumberedTo.ToString().Substring( 0, 8 ),
                                          DSform = dsform.DSform + "_OKay",
                                          ReceiptNumber = a1984.ReceiptNumber // NOV# has to be in the range on one and only one DS-1984
                                      };

                        break;
                    case "DS21":
                        IQueryable<DS21> rsltDS21 = db.DS21.AsQueryable();
                        var qDS21pre = rsltDS21.Where( aRow =>
                            dsform.ECBSummonsNumberFrom.CompareTo( aRow.ECBSummonsNumber.ToString() ) >= 0
                            &&
                            dsform.ECBSummonsNumberFrom.CompareTo( aRow.ECBSummonsNumber.ToString() ) <= 0
                            ||
                            dsform.ECBSummonsNumberTo.CompareTo( aRow.ECBSummonsNumber.ToString() ) >= 0
                            &&
                            dsform.ECBSummonsNumberTo.CompareTo( aRow.ECBSummonsNumber.ToString() ) <= 0

                        );
                        qDS21 = from aRow in qDS21pre
                                select new mdlUsedTktRanges
                                      {
                                          ECBSummonsNumberFrom = aRow.ECBSummonsNumber.Substring( 0, 8 ),
                                          ECBSummonsNumberTo = aRow.ECBSummonsNumber.Substring( 0, 8 ),
                                          DSform = dsform.DSform,
                                          ReceiptNumber = aRow.DS1984Number // mostly for diagnostics
                                      };
                        break;
                    case "DS1704":
                        IQueryable<DS1704> rsltDS1704 = db.DS1704.AsQueryable();
                        var qDS1704pre = rsltDS1704.Where( aRow =>
                            dsform.ECBSummonsNumberFrom.CompareTo( aRow.ECBSummonsNumber.ToString() ) >= 0
                            &&
                            dsform.ECBSummonsNumberFrom.CompareTo( aRow.ECBSummonsNumber.ToString() ) <= 0
                            ||
                            dsform.ECBSummonsNumberTo.CompareTo( aRow.ECBSummonsNumber.ToString() ) >= 0
                            &&
                            dsform.ECBSummonsNumberTo.CompareTo( aRow.ECBSummonsNumber.ToString() ) <= 0

                        );
                        qDS1704 = from aRow in qDS1704pre
                                  select new mdlUsedTktRanges
                                      {
                                          ECBSummonsNumberFrom = aRow.ECBSummonsNumber.ToString(),
                                          ECBSummonsNumberTo = aRow.ECBSummonsNumber.ToString(),
                                          DSform = dsform.DSform,
                                          ReceiptNumber = dsform.DSform // mostly for diagnostics
                                  };
                        break;
                    default:
                        break;           
                }


                switch (dsform.DSform)
                {
                    case "ECB603":
                        return qECB603.ToList<mdlUsedTktRanges>();
                    case "DS1983":
                        return qDS1983.ToList<mdlUsedTktRanges>();
                    case "DS1984":
                        return qDS1984.ToList<mdlUsedTktRanges>();
                    case "IsDS1984":
                        return qDS1984.ToList<mdlUsedTktRanges>();
                    case "IsDS1984Tkt":
                        return qDS1984.ToList<mdlUsedTktRanges>();
                    case "IsGoodNOVnumber":
                        return qIsGoodNOVnumber.ToList<mdlUsedTktRanges>();
                    case "DS1986":
                        return qDS1986.ToList<mdlUsedTktRanges>();
                    case "DS21":
                        return qDS21.ToList<mdlUsedTktRanges>();
                    case "DS1704":
                        return qDS1704.ToList<mdlUsedTktRanges>();
                    default:
                        return new List<mdlUsedTktRanges>();
                }
           
            }
            catch(Exception ex)
            {
                var aList = new List<mdlUsedTktRanges>();
                var newMdl = new mdlUsedTktRanges();
                newMdl.DSform = ex.Message;
                if(!(ex.InnerException == null))
                {
                    newMdl.DSform = newMdl.DSform + " | " + ex.InnerException.Message.ToString();
                }
                aList.Add(newMdl);
                return aList;
            }
        }

        [Route( "GetDS1983List" )]
        public IQueryable<DS1983> GetDS1983List()
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            var q = db.DS1983.AsQueryable();
            return q;
        }

        [Route( "GetDS1983List/{rcpt}" )]
        public DS1983 GetDS1983List( string rcpt )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            IQueryable<DS1983> rslt = db.DS1983.AsQueryable();
            DS1983 oneFrm = rslt.FirstOrDefault<DS1983>( afrm => afrm.ReceiptNumber == rcpt );
            return oneFrm;
        }

        [Route( "GetDS1984List" )]
        public IQueryable<DS1984> GetDS1984List()
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            var q = db.DS1984.AsQueryable();
            return q;
        }


        [Route( "GetDS1984List/{rcpt}" )]
        public DS1984 GetDS1984List( string rcpt )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            IQueryable<DS1984> rslt = db.DS1984.AsQueryable();
            DS1984 oneFrm = rslt.FirstOrDefault<DS1984>( afrm => afrm.ReceiptNumber == rcpt );
            return oneFrm;
        }

        [Route( "GetRelatedDS1984s/{rcptDS1983}" )]
        public IQueryable<DS1984> GetRelatedDS1984s( string rcptDS1983 )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            IQueryable<DS1984> rslt = db.DS1984.AsQueryable();
            IQueryable<DS1984> q = rslt.Where( afrm => afrm.DS1983ReceiptNo == rcptDS1983 );
            return q;
        }

        [Route( "GetDS1986BList" )]
        public IQueryable<DS1986B> GetDS1986BList()
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            var q = db.DS1986B.AsQueryable();
            return q;
        }

        [Route( "GetDS1986BList/{idno}" )]
        public DS1986B GetDS1986BList( int idno )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            IQueryable<DS1986B> rslt = db.DS1986B.AsQueryable();
            DS1986B oneFrm = rslt.FirstOrDefault<DS1986B>( afrm => afrm.ID == idno );
            return oneFrm;
        }

        [Route( "GetDS1986BproblemList" )]
        public IQueryable<Code> GetDS1986BproblemList()
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            IQueryable<Code> rslt = db.Codes.AsQueryable();
            IQueryable<Code> q = rslt.Where( afrm => afrm.Type == "Ds1986B" );
            return q;
        }


        [Route( "GetRelatedDS1986s/{rcptDS1984}" )]
        public IQueryable<DS1986B> GetRelatedDS1986s( string rcptDS1984 )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            IQueryable<DS1986B> rslt = db.DS1986B.AsQueryable();
            IQueryable<DS1986B> q = rslt.Where( afrm => afrm.Ds1984No == rcptDS1984 );
            return q;
        }


        [Route( "GetDS21HDRList" )]
        public IQueryable<DS21HDR> GetDS21HDRList()
        {
            /*
             *  @BoroughList AS VARCHAR(MAX),
                @DistrictList as varchar(max),
                @ReceiptNumber as varchar(50),
                @ReferenceNo as varchar(10),
                @UserAccessDistricts as varchar(max),
                @User varchar(20),
                @RecOfficerRefNo as varchar(10),
                @IsHQUser int
             * */
            PaperTicketsEntities db = new PaperTicketsEntities();

            IQueryable<DS21_Header> rslt = db.DS21_Header.AsQueryable(); 
            IQueryable<DS1984> rsltDS1984 = db.DS1984.AsQueryable();

            var tbl = from item in rslt
                   join t in rsltDS1984 on item.DS1984Number equals t.ReceiptNumber
                   select new DS21HDR
                   {
                       Year = item.Year,
                       IssuingOfficerSignature = item.IssuingOfficerSignature,
                       Command = item.Command,
                       IssuingOfficerName = item.IssuingOfficerName,
                       TaxRegNumber = item.TaxRegNumber,
                       Agency = item.Agency,
                       DateCardStarted = item.DateCardStarted,
                       DateReturned = item.DateReturned,
                       AssignedDistrict = item.AssignedDistrict,
                       IssuingOfficerTitle = item.IssuingOfficerTitle,
                       OfficerInChargeName = item.OfficerInChargeName,
                       OfficerInChargeTitle = item.OfficerInChargeTitle,
                       DateCardCompleted = item.DateCardCompleted,
                       Remarks = item.Remarks,
                       DS1984Number = item.DS1984Number,
                       ModifiedOn = item.ModifiedOn,
                       ModifiedBy = item.ModifiedBy,
                       FormStatusID = item.FormStatusID,
                       IssuingOfficerRefNo = item.IssuingOfficerRefNo,
                       CreatedBy = item.CreatedBy,
                       CreatedOn = item.CreatedOn,
                       BoxNumberedFrom = t.BoxNumberedFrom,
                       BoxNumberedTo = t.BoxNumberedTo,
                   };

            return tbl;
        }


        [Route( "GetDS21HDRListNewest/{YYYYMMDD}" )]
        public IQueryable<DS21HDR> GetDS21HDRListNewest(string YYYYMMDD)
        {
            PaperTicketsEntities db = new PaperTicketsEntities();

            var dYYYYMMDD = YYYYMMDD.Substring( 4, 2 ) + "/" + YYYYMMDD.Substring( 6,2 ) + "/" + YYYYMMDD.Substring( 0,4 );
            var lclYYYYmmDD = Convert.ToDateTime( dYYYYMMDD );

            /*
             * FormStatusID/Description
                1	Less than 25 complete
                2	All 25 complete
                3	DS21 sent to borough
                4	DS21 received by borough
                5	DS21 rejected by borough
                6	DS21 sent to head quarters
                7	DS21 received by headquarters
                8	DS21 rejected by headquarters
             * */
            IQueryable<DS21_Header> rslt = db.DS21_Header.AsQueryable()
                .Where( aRow => aRow.ModifiedOn > lclYYYYmmDD || aRow.FormStatusID != 2 )
                .OrderByDescending( aRow => aRow.ModifiedOn ); // newest on top
            IQueryable<DS1984> rsltDS1984 = db.DS1984.AsQueryable();

            IQueryable<DS21HDR> tbl = from item in rslt
                      join t in rsltDS1984 on item.DS1984Number equals t.ReceiptNumber
                      select new DS21HDR
                      {
                          Year = item.Year,
                          IssuingOfficerSignature = item.IssuingOfficerSignature,
                          Command = item.Command,
                          IssuingOfficerName = item.IssuingOfficerName,
                          TaxRegNumber = item.TaxRegNumber,
                          Agency = item.Agency,
                          DateCardStarted = item.DateCardStarted,
                          DateReturned = item.DateReturned,
                          AssignedDistrict = item.AssignedDistrict,
                          IssuingOfficerTitle = item.IssuingOfficerTitle,
                          OfficerInChargeName = item.OfficerInChargeName,
                          OfficerInChargeTitle = item.OfficerInChargeTitle,
                          DateCardCompleted = item.DateCardCompleted,
                          Remarks = item.Remarks,
                          DS1984Number = item.DS1984Number,
                          ModifiedOn = item.ModifiedOn,
                          ModifiedBy = item.ModifiedBy,
                          FormStatusID = item.FormStatusID,
                          IssuingOfficerRefNo = item.IssuingOfficerRefNo,
                          CreatedBy = item.CreatedBy,
                          CreatedOn = item.CreatedOn,
                          BoxNumberedFrom = t.BoxNumberedFrom,
                          BoxNumberedTo = t.BoxNumberedTo,
                      };

            return tbl;
        }

        private string ConvertToYYYYmmDD( DateTime? dt )
        {
            string yyyyMMdd = "";
            System.Globalization.CultureInfo enUS = System.Globalization.CultureInfo.CreateSpecificCulture( "en-US" );
            System.Globalization.DateTimeFormatInfo dtfi = enUS.DateTimeFormat;
            // Modify the short date pattern.
            dtfi.ShortDatePattern = "yyyyMMdd"; // otherwise it'd be yyyy-MM-dd
            try
            {
                yyyyMMdd = ((DateTime)dt).ToString( "d", enUS );
            }
            catch
            {
                yyyyMMdd = "19800101";
            }
            return yyyyMMdd;
        }

        [Route( "GetOneDS21HDR/{oneDS1984rcpt}" )]
        public IQueryable<DS21HDR> GetOneDS21HDRrow(string oneDS1984rcpt)
        {
            /*
             *  @BoroughList AS VARCHAR(MAX),
                @DistrictList as varchar(max),
                @ReceiptNumber as varchar(50),
                @ReferenceNo as varchar(10),
                @UserAccessDistricts as varchar(max),
                @User varchar(20),
                @RecOfficerRefNo as varchar(10),
                @IsHQUser int
             * */
            PaperTicketsEntities db = new PaperTicketsEntities();

            IQueryable<DS21_Header> rslt = db.DS21_Header.AsQueryable();
            IQueryable<DS1984> rsltDS1984 = db.DS1984.AsQueryable().Where(r => r.ReceiptNumber == oneDS1984rcpt );

            var tbl = from item in rslt
                      join t in rsltDS1984 on item.DS1984Number equals t.ReceiptNumber
                      select new DS21HDR
                      {
                          Year = item.Year,
                          IssuingOfficerSignature = item.IssuingOfficerSignature,
                          Command = item.Command,
                          IssuingOfficerName = item.IssuingOfficerName,
                          TaxRegNumber = item.TaxRegNumber,
                          Agency = item.Agency,
                          DateCardStarted = item.DateCardStarted,
                          DateReturned = item.DateReturned,
                          AssignedDistrict = item.AssignedDistrict,
                          IssuingOfficerTitle = item.IssuingOfficerTitle,
                          OfficerInChargeName = item.OfficerInChargeName,
                          OfficerInChargeTitle = item.OfficerInChargeTitle,
                          DateCardCompleted = item.DateCardCompleted,
                          Remarks = item.Remarks,
                          DS1984Number = item.DS1984Number,
                          ModifiedOn = item.ModifiedOn,
                          ModifiedBy = item.ModifiedBy,
                          FormStatusID = item.FormStatusID,
                          IssuingOfficerRefNo = item.IssuingOfficerRefNo,
                          CreatedBy = item.CreatedBy,
                          CreatedOn = item.CreatedOn,
                          BoxNumberedFrom = t.BoxNumberedFrom,
                          BoxNumberedTo = t.BoxNumberedTo,
                      };

            return tbl;
        }

        [Route("GetDS21HDRforThisNOV/{oneNOVnumber}")]
        public IQueryable<DS21HDR> GetDS21HDRforThisNOV(string oneNOVnumber)
        {
            /*
             *  @BoroughList AS VARCHAR(MAX),
                @DistrictList as varchar(max),
                @ReceiptNumber as varchar(50),
                @ReferenceNo as varchar(10),
                @UserAccessDistricts as varchar(max),
                @User varchar(20),
                @RecOfficerRefNo as varchar(10),
                @IsHQUser int
             * */
            PaperTicketsEntities db = new PaperTicketsEntities();

            IQueryable<DS21_Header> rslt = db.DS21_Header.AsQueryable();
            IQueryable<DS1984> rsltDS1984 = db.DS1984.AsQueryable().Where(r => (r.BoxNumberedFrom <= int.Parse(oneNOVnumber) && r.BoxNumberedTo >= int.Parse(oneNOVnumber)));

            var tbl = from item in rslt
                      join t in rsltDS1984 on item.DS1984Number equals t.ReceiptNumber
                      select new DS21HDR
                      {
                          Year = item.Year,
                          IssuingOfficerSignature = item.IssuingOfficerSignature,
                          Command = item.Command,
                          IssuingOfficerName = item.IssuingOfficerName,
                          TaxRegNumber = item.TaxRegNumber,
                          Agency = item.Agency,
                          DateCardStarted = item.DateCardStarted,
                          DateReturned = item.DateReturned,
                          AssignedDistrict = item.AssignedDistrict,
                          IssuingOfficerTitle = item.IssuingOfficerTitle,
                          OfficerInChargeName = item.OfficerInChargeName,
                          OfficerInChargeTitle = item.OfficerInChargeTitle,
                          DateCardCompleted = item.DateCardCompleted,
                          Remarks = item.Remarks,
                          DS1984Number = item.DS1984Number,
                          ModifiedOn = item.ModifiedOn,
                          ModifiedBy = item.ModifiedBy,
                          FormStatusID = item.FormStatusID,
                          IssuingOfficerRefNo = item.IssuingOfficerRefNo,
                          CreatedBy = item.CreatedBy,
                          CreatedOn = item.CreatedOn,
                          BoxNumberedFrom = t.BoxNumberedFrom,
                          BoxNumberedTo = t.BoxNumberedTo,
                      };

            return tbl;
        }

        [Route( "GetDS21NOVList/{rcptDS1984}" )]
        public System.Collections.Generic.IList<DS21_OneRowWithTicketStatusVM> GetDS21NOVlist(string rcptDS1984)
        { // returns a iList instead of iQueryable because of ordering etc.
            try
            {
                PaperTicketsEntities db = new PaperTicketsEntities();
                IEnumerable<DS21> q = db.DS21.AsQueryable().Where(aRow => aRow.DS1984Number == rcptDS1984);
                IList<DS21withTicketStatus_DTO> modifiedDS21 = new List<DS21withTicketStatus_DTO>();
                DS21_TicketStatusVM dummyTS = new DS21_TicketStatusVM();
                q.ToList<DS21>().ForEach( aDS21 => modifiedDS21.Add(new DS21withTicketStatus_DTO() {DS21=aDS21, DS21_TicketStatus=dummyTS } ) );
                var objTS = db.DS21_TicketStatus.ToList();
                var ls = modifiedDS21.OrderBy(qi => qi.DS21.ECBSummonsNumber).Take(25).ToList(); //first 25 to a List object
                ls.ForEach( item => // 'manually assemble' the complex child object
                item.DS21_TicketStatus = new DS21_TicketStatusVM
                    {
                        ID = item.DS21.TicketStatusID,
                        // static table lookup 
                        Description = objTS.FirstOrDefault( lu => lu.ID == item.DS21.TicketStatusID ).Description,
                        OrderByThisTinyInt = objTS.FirstOrDefault( lu => lu.ID == item.DS21.TicketStatusID ).OrderByThisTinyInt,
                    } );

                IList<DS21_OneRowWithTicketStatusVM> rslt = new List<DS21_OneRowWithTicketStatusVM>();
                ls.ForEach( item =>
                    rslt.Add( new DS21_OneRowWithTicketStatusVM()
                        {
                        ECBSummonsNumber = item.DS21.ECBSummonsNumber,
                        Code = item.DS21.Code,
                        DateServedMonth = item.DS21.DateServedMonth,
                        DateServedDay = item.DS21.DateServedDay,
                        DistOfOccurrence = item.DS21.DistOfOccurrence,
                        Initial = item.DS21.Initial,
                        Title = item.DS21.Title,
                        DS1984Number = item.DS21.DS1984Number,
                        Notarized = item.DS21.Notarized,
                        Voided = item.DS21.Voided,
                        ModifiedOn = item.DS21.ModifiedOn,
                        ModifiedBy = item.DS21.ModifiedBy,
                        TicketStatusID = item.DS21.TicketStatusID,
                        ViolationType = item.DS21.ViolationType,
                        CreatedBy = item.DS21.CreatedBy,
                        CreatedOn = item.DS21.CreatedOn,
                        DateServedYear = item.DS21.DateServedYear,
                        DS21_TicketStatus = item.DS21_TicketStatus,
                    }
                    )
                );

                return rslt;
            }
            catch (System.Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = msg + " | " + ex.InnerException.ToString();
                }

                var dumy = new DS21_OneRowWithTicketStatusVM();
                dumy.DS21_TicketStatus.Description = msg;
                var lst = new System.Collections.Generic.List<DS21_OneRowWithTicketStatusVM>();
                lst.Add( dumy );
                return lst;
                
            }
        }

        [Route( "GetNOVdetail/{nov}" )]
        public DS21 GetDS21List( string nov )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            IQueryable<DS21> rslt = db.DS21.AsQueryable();
            DS21 oneFrm = rslt.FirstOrDefault<DS21>( afrm => afrm.ECBSummonsNumber == nov );
            return oneFrm;
        }

        [Route( "GetDS1704List" )]
        public IQueryable<DS1704> GetDS1704List()
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            var q = db.DS1704.AsQueryable();
            return q;
        }

        [Route( "GetOneDS1704/{nov}" )]
        public DS1704 GetOneDS1704( string nov)
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            IQueryable<DS1704> rslt = db.DS1704.AsQueryable();
            DS1704 oneFrm = rslt.FirstOrDefault<DS1704>( afrm => afrm.ECBSummonsNumber == nov);
            return oneFrm;
        }

        [Route( "GetDS156List" )]
        public System.Collections.Generic.IList<vw_GetDS156ReportData> GetDS156List()
        {
            try
            {
                PaperTicketsEntities db = new PaperTicketsEntities();
                var q = db.vw_GetDS156ReportData.AsQueryable().Take( 50 ).ToList();
                return q;
            }
            catch (System.Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = msg + " | " + ex.InnerException.ToString();
                }

                var dumy = new vw_GetDS156ReportData();
                dumy.Command = msg;
                var lst = new System.Collections.Generic.List<vw_GetDS156ReportData>();
                lst.Add( dumy );
                return lst;

            }


        }


    }
}
