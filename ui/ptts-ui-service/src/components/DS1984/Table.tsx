﻿// prettier-ignore
import {
    Link, Paper, Table, TableBody, TableCell,
    TableContainer, TableHead, TableRow
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
// import { history } from "../../configureStore";
import { useHistory } from "react-router-dom";
import { DS1984basics } from ".";
import { allActions } from "../../actions";
import { allServices } from '../../services';
import { DS1984 } from "../../model";


export function DS1984Table(props: any) {
	const DS198xrcpt: string = !props ? "" 
		: props.props;

	let history = useHistory();

	const classes = useStyles();
	const todaysDate: Date = new Date();
	const dummyDS1984: DS1984 = {
		ID: 0,
		ReceiptNumber: "",
		District: "",
		ReceivingOfficerName: "",
		ReceivingOfficerTitle: "",
		BoxNumberedFrom: 0,
		BoxNumberedTo: 0,
		ReceivingOfficerSignature: "",
		DistributorName: "",
		DistributorSignature: "",
		DistributorSignedDate: todaysDate,
		TaxRegNumber: "",
		FormStatus: "",
		AcknowledgedBy: "",
		ModifiedOn: todaysDate,
		ModifiedBy: "",
		RecOfficerRefNo: "",
		DistributorRefNo: "",
		DS1983ReceiptNo: "",
		DistrictId: 0,
		CreatedBy: "",
		CreatedDate: todaysDate,
		Paycode: "",
		IsVoid: false,
	}

	const content = useSelector((state: any) => state);

	const [dsList, setdsList] = useState([dummyDS1984]);
	const [Loading, setLoading] = useState(false);
	const [dsListPayCodeFilter, setdsListPayCodeFilter] = useState([dummyDS1984]);

	React.useEffect(() => {
		(async () => {
			setLoading(true);
			let rdata: DS1984[] = [];
			if (!DS198xrcpt) {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("whole DS1984 list");
				const response2 = await allServices.PTTSgetServices.DS1984List(); // axios always returns an object with a "data" child
				rdata = response2.data;
			}
			else if (DS198xrcpt.substring(0,1) === "3" ) {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("partial DS1984 list", DS198xrcpt);
				const response1 = await allServices.PTTSgetServices.DS1984RelatedToOneDS1983List(DS198xrcpt); // axios always returns an object with a "data" child
				rdata = response1.data;
            }
			else if (DS198xrcpt.substring(0, 1) === "4") {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("partial DS1984 list", DS198xrcpt);
				const response1 = await allServices.PTTSgetServices.anDS1984(DS198xrcpt); // axios always returns an object with a "data" child
				rdata = response1.data;
			}
			else {
				alert("unexpected props sent to DS1984 Tables component.");
            }

			if (!rdata) {
				setLoading(true);
			}
			else {
				const rdataDesc: DS1984[] = rdata.sort(function (a, b) {
					var nameA = !a.ReceiptNumber ? "" : a.ReceiptNumber.toUpperCase(); // ignore upper and lowercase
					var nameB = !b.ReceiptNumber ? "" : b.ReceiptNumber.toUpperCase(); // ignore upper and lowercase
					if (nameA < nameB) {
						return 1;
					}
					if (nameA > nameB) {
						return -1;
					}
					// names must be equal
					return 0;
				})//.slice(0, 29); // DESC
				setdsList(rdataDesc);
				setLoading(false);
			}
		})();
	}, []);

	React.useEffect(() => {
		const dsListFiltered: DS1984[] = allActions.DS1984acts.DS1984sIAWpayCodesExtractedFromADGroupMembership(dsList, content)

		setdsListPayCodeFilter(
			dsListFiltered
		);

	}, [dsList, content])

	const handleCellClick = (e: any) => {
		history.push('/PTTS/existingDS1984/' + e.target.textContent)
	}


	const handleLinkToParent1983 = (e: any) => {
		history.push('/PTTS/existingDS1983/' + e.target.textContent)
	}
	function handleRelatedDS1986s(rcpt: string) {
		history.push('/PTTS/someDS1986/' + rcpt)
	}

	function handleRelatedDS21s(rcpt: string) {
		history.push('/PTTS/DS21HDR/' + rcpt)
	}

	function handleNewDS1986s(rcpt: string, fromNumber: number, toNumber: number, district: string) {
		const payload: DS1984basics = {
			DS1984receiptNumber: rcpt,
			DS1984fromTktNumber: fromNumber,
			DS1984toTktNumber: toNumber,
			DS1984DistrictState: district
		};
		history.push('/PTTS/newDS1986/', payload)
	}

	return (
		<TableContainer component={Paper}>

			<Table className={classes.table} size="small" aria-label="a dense table">
				<TableHead>
					<TableRow>
						<TableCell size="small"  align="right">DS1984 Receipt No</TableCell>
						<TableCell size="small"  align="right">District</TableCell>
						<TableCell size="small"  align="right">Ticket Range</TableCell>
						<TableCell size="small"  align="right">Status</TableCell>
						<TableCell size="small"  align="right">Last modified on</TableCell>
						<TableCell size="small"  align="left">Distributor Name</TableCell>
						<TableCell size="small"  align="left">Receiving Officer</TableCell>
						<TableCell size="small"  align="left">DS21</TableCell>
						<TableCell size="small" align="left">Parent DS1983</TableCell>
						<TableCell size="small" align="left">new DS1986</TableCell>
						<TableCell size="small" align="left">related DS1986</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{
						// @ts-ignore: Object is possibly 'null'.
						Loading || !dsList || dsList === undefined
							? <TableRow><TableCell size="small">working ...</TableCell></TableRow>
							: dsListPayCodeFilter.map((n: DS1984) => {
							//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("n is:");
							//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(n);
								if (!n.ID) {
									return (<TableRow key={"-1"}><TableCell size="small" key={"0"}>Working ...</TableCell></TableRow>)
								}
								else {
									return (
										<TableRow
											key={("000000000" +n.ID.toString()).substr(-8) + "row"}
											hover
										>
											<TableCell size="small" key={("000000000" + n.ID.toString()).substr(-8) + "rcptno"} align="right" onClick={handleCellClick}><Link href="#">{n.ReceiptNumber}</Link></TableCell>
											<TableCell size="small" key={("000000000" + n.ID.toString()).substr(-8) + "DISTR"}  align="right">{n.District}</TableCell>
											<TableCell size="small" key={("000000000" + n.ID.toString()).substr(-8) + "TKTRNG"}  align="right">{n.BoxNumberedFrom}-{n.BoxNumberedTo}</TableCell>
											<TableCell size="small" key={("000000000" + n.ID.toString()).substr(-8) + "STATUS"}  align="right">{n.FormStatus}</TableCell>
											<TableCell size="small" key={("000000000" + n.ID.toString()).substr(-8) + "MODFON"}  align="right">{n.ModifiedOn}</TableCell>
											<TableCell size="small" key={("000000000" + n.ID.toString()).substr(-8) + "DISTRNAME"}  align="left">{n.DistributorName}</TableCell>
											<TableCell size="small" key={("000000000" + n.ID.toString()).substr(-8) + "RECEIOFFICNAME"}  align="left">{n.ReceivingOfficerName}</TableCell>
											<TableCell size="small" key={("000000000" + n.ID.toString()).substr(-8) + "LINK21"} onClick={() => handleRelatedDS21s(n.ReceiptNumber)}  align="left">Link to its DS21</TableCell>
											<TableCell size="small" key={("000000000" + n.ID.toString()).substr(-8) + "LINK83"} align="left" onClick={handleLinkToParent1983}><Link  href="#">{n.DS1983ReceiptNo}</Link></TableCell>
											<TableCell key={("000000000" + n.ID.toString()).substr(-8) + "NEW1986"} align="left" onClick={() => handleNewDS1986s(n.ReceiptNumber, n.SummonsFromNumber, n.SummonsToNumber, n.District)}><Link  href="#">Make new DS1986</Link></TableCell>
											<TableCell key={("000000000" + n.ID.toString()).substr(-8) + "URL1986"} align="left" onClick={() => handleRelatedDS1986s(n.ReceiptNumber)}><Link  href="#">Link to its DS1986s</Link></TableCell>
										</TableRow>
									);
								}
							}
						)}
				</TableBody>
			</Table>
		</TableContainer>
	);
}

const useStyles = makeStyles({
	paper: {
		width: "70%",
		display: "block",
	},
	table: {
		width: "100%",
		marginLeft: 180,
	},
	cell: {
		minWidth: 100,
		maxWidth: 220,
	}
});
