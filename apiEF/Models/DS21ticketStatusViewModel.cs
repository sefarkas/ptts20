﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PTTSapiEFdb;

namespace apiEF.Models
{
    public class DS21_OneRowWithTicketStatusVM
    {
        public string ECBSummonsNumber
        {
            get; set;
        }
        public string Code
        {
            get; set;
        }
        public Nullable<int> DateServedMonth
        {
            get; set;
        }
        public Nullable<int> DateServedDay
        {
            get; set;
        }
        public string DistOfOccurrence
        {
            get; set;
        }
        public string Initial
        {
            get; set;
        }
        public string Title
        {
            get; set;
        }
        public string DS1984Number
        {
            get; set;
        }
        public Nullable<bool> Notarized
        {
            get; set;
        }
        public Nullable<bool> Voided
        {
            get; set;
        }
        public Nullable<System.DateTime> ModifiedOn
        {
            get; set;
        }
        public string ModifiedBy
        {
            get; set;
        }
        public int TicketStatusID
        {
            get; set;
        }
        public string ViolationType
        {
            get; set;
        }
        public string CreatedBy
        {
            get; set;
        }
        public Nullable<System.DateTime> CreatedOn
        {
            get; set;
        }
        public Nullable<long> DateServedYear
        {
            get; set;
        }

        public virtual DS21_TicketStatusVM DS21_TicketStatus
        {
            get; set;
        }

    }

    public class DS21withTicketStatus_DTO
    {
        public virtual DS21 DS21
        {
            get; set;
        }

    public virtual DS21_TicketStatusVM DS21_TicketStatus
        {
            get; set;
        }
    }

    public partial class DS21_TicketStatusVM
    {
        public int ID
        {
            get;  set;
        }
        public string Description
        {
            get;  set;
        }
        public Nullable<short> OrderByThisTinyInt
        {
            get;  set;
        }
    }


    public partial class DS21VM : DS21
    {
    };
}