﻿
// Enforcement PTTS Managment
export const ENFHQ = [
    { userMemberOfADgroupList: "ENF__LEADER" },
    { userMemberOfADgroupList: "ENF__AGENT" },
    { userMemberOfADgroupList: "ENF__CLERK" },
    { userMemberOfADgroupList: "V100AGENT" },
    { userMemberOfADgroupList: "V100LEADER" },
    { userMemberOfADgroupList: "V100CLERK" },
];
