﻿import Modal from "react-modal";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import Grid from '@material-ui/core/Grid';

interface Props {
	open: boolean;
	onClose: () => void;
	respondentPhotoURL: string;
}

export function RespondentPhoto(props: Props) {	
	const classes = useStyles();

	Modal.setAppElement('#root');

	return (
		<Modal
			isOpen={props.open}
			shouldCloseOnOverlayClick={false}
			onRequestClose={() => props.onClose()}
			style={
				{
					overlay: {
						// backgroundColor: "grey"
					},
					content: {
						backgroundColor: "#edeff0",
						padding: '0 0px',
						blockSize: "410px",
						position: "absolute",
						top: "19%",
						left: "37%",
						bottom: "0px",
						maxWidth: "410px"
					}
				}
			}
		>   <Grid container className={classes.grid}>
				<Grid item xs={9}>		
					<h2 className={classes.h2}>Employee Photo</h2>
				</Grid>	
				<Grid item xs={3}>	
					<h2 onClick={props.onClose} className={classes.labelY}>X</h2>
				</Grid>	
				<Grid item container xs={12}>				
					<Grid item xs={12}>
						<img src={props.respondentPhotoURL} className={classes.image}/>
					</Grid>	
				</Grid>
			</Grid>
		</Modal>
	);
}

const useStyles = makeStyles({	
	grid: {
		background: "white",
		marginTop: "2px;"
	},	
	h2: {
		marginTop: "10px",
		marginBottom: "5px",
		marginLeft: "10px",
		display: "inline-block",
		fontSize: 15,
	},		
	labelY: {
		lineHeight: 2,
		textAlign: "right",		
		fontSize: 12,
		fontWeight: 800,
		color: "#2e2e2d",
		width: "90%"
	},
	image: {
		height: "340px",
		width: "272px",
		marginLeft: "68px",
		marginRight: "68px",
		marginBottom: "15px"
	},
});