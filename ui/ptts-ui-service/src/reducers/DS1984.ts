﻿import { Action } from 'redux';
import { KnownActionDS1984 } from '../actions';
import { DS1984, DS1984Action, DS1984Actions, efDS1984, tOneNOV } from "../model";
import createReducer from "./createReducer";
import { dummyNOV } from './DS21Card';

type KnownAction = KnownActionDS1984;

export const DS1984List = createReducer<DS1984[]>([], {
	[DS1984Actions.ADD_DS1984](state: DS1984[], action: DS1984Action) {
		return [...state, action.payload];
	},
	[DS1984Actions.COMPLETE_DS1984](state: DS1984[], action: DS1984Action) {
		// search after DS1984 item with the given id and set completed to true
		return state.map(t =>
			t.iD === action.payload ? { ...t, completed: true } : t
		);
	},
	[DS1984Actions.REVERT2INCOMPLETE_DS1984](state: DS1984[], action: DS1984Action) {
		// search after DS1984 item with the given id and set completed to false
		return state.map(t =>
			t.iD === action.payload ? { ...t, completed: false } : t
		);
	},
	[DS1984Actions.DELETE_DS1984](state: DS1984[], action: DS1984Action) {
		// remove all DS1984 with the given id
		return state.filter(t => t.iD !== action.payload);
	},
});


export const DS1984Reducer = (state: tInterForInitialState | undefined = INITIAL_STATE, incomingAction: Action) => {
	const action = incomingAction as KnownAction;
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS21HDRReducer hit. Looking for " + action.type);

	let whichID: number = -1;

	switch (action.type) {
		case DS1984Actions.ADD_DS1984:
			return {
				...state, ST_DS1984rows: action.payload
			}
		case DS1984Actions.ADD_NOVs:
			return {
				...state, ST_DS1984NOVs: action.payload
            }
		case DS1984Actions.UPDATE_DS1984:

			let i: number = -1;
			for (i = 0; i < state.ST_DS1984rows.length; i++) {
				if (state.ST_DS1984rows[i].BoxNumberedFrom <= action.payload.ECBSummonsNumber
					&& state.ST_DS1984rows[i].BoxNumberedTo >= action.payload.ECBSummonsNumber) {
					console.log("found it in the filtered HDR rows", i);
					whichID = i;
				}
			}
			if (whichID > -1) state.ST_DS1984rows[whichID] =
			{ // https://medium.com/hackernoon/redux-patterns-add-edit-remove-objects-in-an-array-6ee70cab2456
				...state.ST_DS1984rows[whichID],
				...action.payload
			}
			// state.ST_DS21HDRrowsFLTRD.splice(whichID, 1); // second parameter of splice is the number of elements to remove
			return {
				...state // , ST_DS21HDRrowsFLTRD: action.payload
			}
		default:
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("landed in default of the DS1984 reducer");
			return { ...state };

	}
};



const todaysDate: Date = new Date();

export const dummyDS1984: DS1984 = {
	ID: 0,
	ReceiptNumber: "",
	District: "",
	ReceivingOfficerName: "",
	ReceivingOfficerTitle: "",
	BoxNumberedFrom: 0,
	BoxNumberedTo: 0,
	ReceivingOfficerSignature: "",
	DistributorName: "",
	DistributorSignature: "",
	DistributorSignedDate: todaysDate,
	TaxRegNumber: "",
	FormStatus: "",
	AcknowledgedBy: "",
	ModifiedOn: todaysDate,
	ModifiedBy: "",
	RecOfficerRefNo: "",
	DistributorRefNo: "",
	DS1983ReceiptNo: "",
	DistrictId: 0,
	CreatedBy: "",
	CreatedDate: todaysDate,
	Paycode: "",
	IsVoid: false,
}

export type tInterForInitialState = { // all caps is signal to later developers to not change this value
	ST_DS1984rows: DS1984[];
	ST_DS1984NOVs: tOneNOV[];
	ST_DS1984msg: string;
	ST_CARDCOMPLETE: boolean;
};

export const INITIAL_STATE: tInterForInitialState = { // all caps is signal to later developers to not change this value
	ST_DS1984rows: [dummyDS1984],
	ST_DS1984NOVs: [dummyNOV],
	ST_DS1984msg: '',
	ST_CARDCOMPLETE: false,
};

