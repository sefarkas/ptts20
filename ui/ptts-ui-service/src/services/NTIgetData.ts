﻿import axios from "axios";

// ...

function fEmployeeDetailFromNTI(refNum: string) {
    // (async () => {
    // await
    return axios.get(`${process.env.REACT_APP_PTTS20_API}/nti/GetProfile/` + refNum)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

// ...

export const allServices: any = {

    NTIemployeeDetailsRN: fEmployeeDetailFromNTI, // PTTS1.0 SPROC
};