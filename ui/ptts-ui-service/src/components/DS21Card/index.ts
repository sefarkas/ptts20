﻿export { DS21Dialog } from "./Dialog";
export { DS21CardTable } from "./Table";
export { DS21CardBottom } from "./Template";

export interface DS21basics {
    DS21receiptNumber: string,
    DS21fromTktNumber: number,
    DS21toTktNumber: number,
    DS21DistrictState: string
};

export { BulkUpdateTable } from "./BulkUpdateLocation/Table"; // twenty rows of DS1704  NOV# pairs
export { Instructions } from "./BulkUpdateLocation/Instructions"; 
export { BulkUpdateFooter } from "./BulkUpdateLocation/Footer"; 
export { BulkUpdateTableAndFooter } from "./BulkUpdateLocation/Joint";