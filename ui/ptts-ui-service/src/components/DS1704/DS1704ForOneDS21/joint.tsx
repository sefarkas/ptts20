﻿// prettier-ignore
import {
    Button,
    FormControl,
	Grid, Input, InputLabel, Link, MenuItem, Paper, Select, Table, TableBody, TableCell,
	TableHead, TableRow, withStyles
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { allActions } from "../../../actions";
import { allServices } from '../../../services';
import { tOneNOV, tDS1704, DS1984, aDS1704rows, DS1986problems, DS1704Reasons } from "../../../model";
import Loader from "../../basicComponents/Loader";
import moment from "moment";
import { dummies } from "../../../reducers";
import { dummyNOV } from "../../../reducers/DS21Card";
import { ErrorMessage } from "@hookform/error-message";
import { ErrorMessageContainer } from "../../basicComponents/validation";

type FormData = {
	fldNOVnumber: number;
}

export function PageForDS1704BulkForDS21(props: any) { // expect a table of with 20 rows of DS1704As for up to NOV#s.

	//console.log("PageForDS1704BulkForDS21 props: ", props);

	const classes = useStyles();
	const dispatch = useDispatch();
	const mountedRef = useRef(true);
	const { register, handleSubmit, errors, setError } = useForm<FormData>();


	const defaultValues = {
		Native: "",
		TextField: "",
		Select: "",
		ReactSelect: "",
		Checkbox: false,
		switch: false,
		RadioGroup: ""
	};

	const { control } = useForm({ defaultValues });

	const contentparent1984: any = useSelector((state: any) => state.DS1984Reducer);

	const dummyDS1704ReasonList: DS1704Reasons = {
		Id: 0,
		Code1: "",
		Description: "",
		Type: "",
		OrderSequence: 0,
	}
	let history = useHistory();

	const [lstOfDS1704s, setlstOfDS1704s] = useState<aDS1704rows[]>([dummies.dummyDS1704row]);

	const [DS1984rcpt, setDS1984rcpt] = useState(!props.DS1984rcpt ? "" : props.DS1984rcpt);
	const [oneDS1984, setDS1984] = useState<DS1984>(!contentparent1984.ST_DS1984rows ? dummies.dummyDS1984 : contentparent1984.ST_DS1984rows);

	const contentparent: any = useSelector((state: any) => state.DS21CardReducer);
	const [ReasonForVoidState, setReasonForVoidState] = useState("");
	const [ds20NOVs, setds20NOVs] = useState<tOneNOV[]>(!contentparent.ST_25NOVS ? [dummies.dummyNOV] : contentparent.ST_25NOVS)

	const [isLoading, setLoading] = useState(true);
	const [undoShow, setundoShow]= useState(false);

	const [DS1704ReasonIdState, setDS1704ReasonIdState] = useState(11);
	const [dsDS1704ReasonList, setdsDS1704ReasonList] = useState<DS1704Reasons[]>([dummyDS1704ReasonList]);
	let dsListMenu: any | undefined = undefined;
	let dsDS1704ReasonListMenu: any | undefined = undefined;

	React.useLayoutEffect(() => {
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Init useEffect started.");
		if ((!dsListMenu ) || (!dsDS1704ReasonListMenu && DS1704ReasonIdState < 12)) {
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Build the two dropdown lists.");
			(async () => {

				const response2 = await allServices.PTTSgetServices.refReasonList(cbReason, er); // axios always returns an object with a "data" child

			})();
		}

		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Init useEffect finished.");
	},
		[DS1984rcpt, DS1704ReasonIdState]
	);



	React.useEffect(() => {
		if (DS1984rcpt.length > 9) {
			setLoading(true);
			allServices.PTTSgetServices.anDS1984withCallBack(DS1984rcpt, cb, er);
		}
	}, []);

	React.useEffect(() => {

		if (DS1984rcpt.substring(0, 1) === "4") {
			allActions.DS21acts.GetNOVsForOneCardWithCallBack(dispatch, DS1984rcpt, cbNOVs, er); // axios always returns an object with a "data" child
		}
		else {
			alert("unexpected props sent to PageForDS1704Bulk component.");
		}

		return () => {
			mountedRef.current = false
		}

	}, [oneDS1984])

	React.useEffect(() => {
		const lcllstOfDS1704s: aDS1704rows[] = [];
		let OneToTwenty: number = 0
		setLoading(true);
		// build 20 rows of lstDS1704 with NOV# etc. filled in
		ds20NOVs.forEach((item: tOneNOV) => {
			OneToTwenty = OneToTwenty + 1;
			const aRow: aDS1704rows = {
				id: OneToTwenty,
				POC: !item.DistOfOccurrence
					? ""
					: item.DistOfOccurrence, // place of occurrence -- district issuing ticket
				NOVnumber: item.ECBSummonsNumber,
				NOVdate: !item.DateServedYear
					? moment((new Date()).toUTCString()).format("YYYY-MM-DD")
					: item.DateServedYear.toString() + '-' + item.DateServedMonth.toString() + '-' + item.DateServedDay.toString(), // YYYY-MM-DD
				Address: !item.DistOfOccurrence
					? ""
					: item.DistOfOccurrence, // where ticket was issued to respondent
				ReasonForVoid: !lstOfDS1704s[OneToTwenty]
					? ""
					: lstOfDS1704s[OneToTwenty].ReasonForVoid.length < 2
						? ""
						: lstOfDS1704s[OneToTwenty].ReasonForVoid, // selection made from the dropdown list
				VerifiedBy: "", // uid of the supervisors who authorized the void
			};
			lcllstOfDS1704s.push(aRow)
			if (OneToTwenty > 19) { setlstOfDS1704s(lcllstOfDS1704s); };
		});
		if (OneToTwenty > 19 || ds20NOVs.length > 19) { setLoading(false); };
	}, [ds20NOVs])

	React.useEffect(() => {
		setLoading(true);
		lstOfDS1704s.forEach((item: aDS1704rows) => {
			item.ReasonForVoid = ReasonForVoidState
		});
		setLoading(false);
	}, [ReasonForVoidState])

	const cb = (response: any) => {
		let rdata1984: DS1984 = !response ? dummies.dummyDS1984 : response.data;
		setDS1984(rdata1984);
	}

	const cbNOVs = (rdata: any) => {
		setds20NOVs(rdata);
	}

	const cbReason = (response: any) => {
		setdsDS1704ReasonList(response.rdata)
		const rdata2: DS1704Reasons[] = response.data;

		if (!rdata2) {
			setLoading(true);
		}
		else {
			const rdataDesc2: DS1704Reasons[] = rdata2.sort(function (a, b) {
				var nameA = !a.OrderSequence ? 0 : a.OrderSequence; // ignore upper and lowercase
				var nameB = !b.OrderSequence ? 0 : b.OrderSequence; // ignore upper and lowercase
				if (nameA < nameB) {
					return -1;
				}
				if (nameA > nameB) {
					return 1;
				}
				// names must be equal
				return 0;
			}).slice(0, 10); // ASC
			setdsDS1704ReasonList(rdataDesc2);
			dsDS1704ReasonListMenu = makeDS1704ReasonListMenu();
		}
    }

	const er = (response: any) => {
		alert("DS1704Atable/er: " + response);
	}

	const handleDropDownChange = (event: any, fld: string) => {

		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handleDropDownChange for ", fld);
		switch (fld) {
			case "fldDS1704ReasonId": {
				// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fld", fld);
				//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(dsDS1704ReasonList);
				// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("ErrorID: ", event.target.innerText);
				if (event.target.innerText !== undefined) {
					let strID: string = event.target.innerText.substr(0, 4);
					// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("in-progress strID to be saved: ", strID);
					strID = strID.substr(1, 2); // second character, length of two
					// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Error strID to be saved: ", strID);
					const intID: number = Number.parseInt(strID);
					if (!intID) {
						if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Error intID to be saved: ", intID);
					}
					else {
						// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Expected problem type: ", dsDS1704ReasonList[intID - 11].Description);
						if (intID !== DS1704ReasonIdState) {
							(async () => {
								await setDS1704ReasonIdState(intID);
							})();
						}
					}
				}
				break;
			}
			default:
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("default");
				// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(event.target);
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fld", fld);
		}
		return;
	};

	const makeDS1704ReasonListMenu = () => {
		if (!dsDS1704ReasonList) { return <div>still working ...</div> };
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("JSX for DS1704ReasonListMenu");
		return dsDS1704ReasonList.map(oneChoice => {
			return (
				<MenuItem
					key={oneChoice.Id.toString()}
					value={oneChoice.Id}
				>({oneChoice.Id})&nbsp;{oneChoice.Description}
				</MenuItem>);
		});
	};

	function loggedInUser(): string {
		return (localStorage.getItem('userid') + "").toString(); // to avoid possibly null error
		//const j: any = JSON.parse(curUser);
		//return j.data.userPreferences.userName.toString();
	}

	function handleReason(e: any) {
		setReasonForVoidState(e.target.value)
    }

	function handleWithdrawDS1704forEachNOV() {
		// alert("CRUD dbo.DS1704");
		let iCounter: number = 0;
		lstOfDS1704s.forEach((item: aDS1704rows) => {
			iCounter++;
			let NOV: tOneNOV = ds20NOVs[iCounter - 1];
			const oneDS1704: tDS1704 = {
				ECBSummonsNumber: item.NOVnumber,
				TimeServed: new Date(),
				PlaceOfOccurence: "",
				Remarks: "",
				ModifiedBy: loggedInUser(),
				ModifiedOn: new Date(),
				VoidDate: new Date(),
				VoidReasonId: 0,
				CreatedBy: loggedInUser(),
				CreatedDate: new Date(),
				IsActive: false,
				ApprovedBy: "",
				District: "",
			}
			// SPROC[dbo].[DS1704_InsertUpdateRecord]
			try {
				if (!NOV.Notarized) {
					allServices.PTTSiudServices.iudDS1704(oneDS1704)
					allActions.DS1704acts.addDS1704(oneDS1704, dispatch);
				}
			}
			catch (error) {
				alert("PTTS_D1704Bulk trouble: " + error.toString())
			}
		})
	};

	function handleCreateDS1704forEachNOV() {
		// alert("CRUD dbo.DS1704");
		let iCounter: number = 0;
		lstOfDS1704s.forEach((item: aDS1704rows) => {
			iCounter++;
			let NOV: tOneNOV = ds20NOVs[iCounter-1];
			const oneDS1704: tDS1704 = {
				ECBSummonsNumber: item.NOVnumber,
				TimeServed: new Date(),
				PlaceOfOccurence: "",
				Remarks: ReasonForVoidState,
				ModifiedBy: loggedInUser(),
				ModifiedOn: new Date(),
				VoidDate: new Date(),
				VoidReasonId: 0,
				CreatedBy: loggedInUser(),
				CreatedDate: new Date(),
				IsActive: true,
				ApprovedBy: "",
				District: "",
			}
			// SPROC[dbo].[DS1704_InsertUpdateRecord]
			try {
				if (!NOV.Notarized) {
					allServices.PTTSiudServices.iudDS1704(oneDS1704)
					allActions.DS1704acts.addDS1704(oneDS1704, dispatch);
				}
				setundoShow(true);
			}
			catch (error) {
				alert("PTTS_D1704Bulk trouble: " + error.toString())
            }
		})
	};

	function renderSelectForDS1704ReasonIdState() {
		return (
			<FormControl className={classes.stylefortopacknowledgementtext}>
				<InputLabel className={classes.stylefortopacknowledgementtext}
					id="demo-simple-select-labelErrorId">Category</InputLabel>
				<div>&nbsp;</div>
				<Controller

					as={
						<Select
							className={classes.stylefortopdistributionrowtextwide}
							labelId="demo-simple-select-labelErrorId"
							id="demo-simple-selectErrorId"
							value={DS1704ReasonIdState > 10 ? dsDS1704ReasonList[DS1704ReasonIdState - 11].Description : ""}
							onMouseDown={
								(e) =>
									handleDropDownChange(e, "fldDS1704ReasonId")
							}
							name="fldDS1704ReasonId"
							ref={register({
								required: { value: true, message: "You must pick a problem type." },
								min: { value: 12, message: `Pick a void reason for ${DS1984rcpt}` },
							})}
						>
							{makeDS1704ReasonListMenu()}
						</Select>
					}
					name="Select"
					id="fldDS1704ReasonId"
					control={control}
				/>
				<div>&nbsp;</div>
				<ErrorMessage
					errors={errors}
					name="fldDS1704ReasonId"
					as={<ErrorMessageContainer />}
				/>
			</FormControl>

		);
	}



	const handleGetNOVForThisDS1704 = (e: any) => {
		history.push('/PTTS/NOVdetails/' + e.target.textContent)
	}

	const handleLinkToParent1984 = (e: any) => {
		history.push('/PTTS/existingDS1984/' + e.target.textContent)
	}

	const handleLinkToRelatedDS21 = (e: any) => {
		history.push('/PTTS/DS21Card/' + e.target.textContent)
	}

	const submitForm = async (data: FormData, e: any) => {
		switch (e.target.name) {
			case "Clear All":
				break;
			default:
				// code block
				const msg: string = "No onChange for " + e.target.name;
				alert(msg);
		}
		// e.target.reset(); // reset after form submit
	}

	if (isLoading) {
		return (<Loader />);
	}
	else {
		return (
			<Grid container>
				<Grid item xs={12} component={Paper} className={classes.paper}>
						<Table className={classes.table} aria-label="a dense table" >
						<TableHead>
							<TableRow>
								<TableCell align="left">Related DS1984&#9;<Link href="#" onClick={handleLinkToParent1984}>{DS1984rcpt}</Link></TableCell>
							</TableRow>
							<TableRow>
								<TableCell align="left">Related DS21&#9;&#9;<Link href="#" onClick={handleLinkToRelatedDS21}>{DS1984rcpt}</Link></TableCell>
							</TableRow>
							<TableRow>
								<TableCell size="small" align="right">District</TableCell>
								<TableCell size="small" align="center">Ticket No</TableCell>
								<TableCell size="small" align="right">Date of Issue/Date</TableCell>
								<TableCell size="small" align="left">Address</TableCell>
								<TableCell size="small" align="left">Reason for Void</TableCell>
								<TableCell size="medium" align="left">Verified By</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{

								// @ts-ignore: Object is possibly 'null'.
								isLoading //  || !dsListPayCodeFilter || dsListPayCodeFilter === undefined
									|| lstOfDS1704s.length < 2
									? <TableRow><TableCell size="small">building a list of 20 rows ...{!lstOfDS1704s ? "" : !lstOfDS1704s.length ? "" : lstOfDS1704s.length.toString() }</TableCell></TableRow>
									: lstOfDS1704s.map((n: aDS1704rows) => {
										//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("n is:");
										//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(n);
										return (
											<TableRow
												key={("000000000" + n.id.toString()).substr(-8) + "row"}
												hover
											>
												<TableCell size="small" key={("000000000" + n.id.toString()).substr(-8) + "POC"} align="right">{`${n.POC}`}</TableCell>
												<TableCell size="small" key={("000000000" + n.id.toString()).substr(-8) + "TKTNO"} align="left">
													<div><Link href="#" onClick={handleGetNOVForThisDS1704}>{n.NOVnumber}</Link></div>
												</TableCell>
												<TableCell size="small" key={("000000000" + n.id.toString()).substr(-8) + "DATE"} align="right">{!n.NOVdate ? "na" : moment(n.NOVdate).format("M/D/YYYY")}</TableCell>
												<TableCell size="small" key={("000000000" + n.id.toString()).substr(-8) + "ADDRESS"} align="left">{n.Address}</TableCell>
												<TableCell size="medium" key={("000000000" + n.id.toString()).substr(-8) + "REASON"} align="left" onClick={handleLinkToParent1984}><Link href="#">{n.ReasonForVoid}</Link></TableCell>
												<TableCell size="small" key={("000000000" + n.id.toString()).substr(-8) + "VERIFIEDBY"} align="left" onClick={handleLinkToParent1984}><Link href="#">{n.VerifiedBy}</Link></TableCell>
											</TableRow>
										);
									}
									)
							}
							<TableRow>
								<TableCell size="medium" className={classes.reason} >
										{/*<Input className={classes.styleforticketrangetext}*/}
										{/*	type="string"*/}
										{/*	id="idReason"*/}
										{/*	placeholder="PTTS expects one reason for listed NOVs."*/}
										{/*	onChange={handleReason}*/}
										{/*	name="fldReason"*/}
										{/*	ref={register({*/}
										{/*		required: { value: true, message: "PTTS expects a reason (applies to all NOVs on this page)." },*/}
										{/*	})}*/}
										{/*	value={ReasonForVoidState}*/}
										{/*// defaultValue versus value --> value lets me change characters in the currently active control textbox*/}
									{/*/>*/}
									{renderSelectForDS1704ReasonIdState}
								</TableCell>
								<TableCell size="small" align="left">
									<StyledButton className={classes.button}
										id="btnDS1704forAll"
										variant="outlined"
										color="primary" type="submit"
										onClick={handleSubmit(handleCreateDS1704forEachNOV)} >Void unNotarized NOVs</StyledButton>
								</TableCell>
								{undoShow ?
									<TableCell>
										<StyledButton className={classes.button}
											id="btnDS1704forAllWithdraw"
											variant="outlined"
											color="primary" type="submit"
											onClick={handleSubmit(handleWithdrawDS1704forEachNOV)} >unVoid all these NOVs</StyledButton>
									</TableCell>
									: <div>&nbsp;</div>
                                }

							</TableRow>
						</TableBody>
				</Table>
			</Grid>
			</Grid>
			)
	}			 // each voided NOV has one associated DS1704A; this is the UI creating the DS1704A record

}

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5,
		marginTop: 0
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);


const useStyles = makeStyles({
	paper: {
		width: "100%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 0,
		overflowX: "visible",
	},
	table: {
		width: "100%",
		margingTop: 80,
		minWidth: 1000,
		display: "inline-block",
	},
	button: {
		marginTop: 20,
	},
	cell: {
		minWidth: 100,
		maxWidth: 220,
	},
	reason: {
		minWidth: 300,
		maxWidth: 420,
    },
	styleforticketrangetext: {
		marginBottom: 0,
		marginTop: 0,
		lineHeight: 1,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 14,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 300,
		minWidth: 250,
	},
	stylefortopacknowledgementtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 250,
		minWidth: 130
	},
	stylefortopdistributionrowtextwide: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		minWidth: 300,
		maxWidth: 600,
	},
});
