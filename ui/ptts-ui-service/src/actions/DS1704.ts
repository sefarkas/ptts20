﻿import { DS1704Action, DS1704Actions, tDS1704 } from "../model";


export type KnownActionDS1704 = DS1704Action;


function fAddDS1704(oDS1704: tDS1704, dispatchRef: any): DS1704Action {
	return dispatchRef( {
		type: DS1704Actions.ADD_DS1704,
		payload: oDS1704,
	});
}

function fUptDS1704(oDS1704: tDS1704): DS1704Action {
	return {
		type: DS1704Actions.UPDATE_DS1704,
		payload: oDS1704,
	};
}


// Async Function expample with redux-thunk
function fCompleteDS1704(DS1704Id: number, dispatchRef: any) {
	// here you could do API eg

	return dispatchRef({ type: DS1704Actions.COMPLETE_DS1704, payload: DS1704Id });
}

function fUncompleteDS1704(DS1704Id: number): DS1704Action {
	return {
		type: DS1704Actions.REVERT2INCOMPLETE_DS1704,
		payload: DS1704Id,
	};
}

function fDS1704deleteTodo(DS1704Id: number): DS1704Action {
	return {
		type: DS1704Actions.DELETE_DS1704,
		payload: DS1704Id,
	};
}

export const allActions = {
	addDS1704: fAddDS1704,
	uptDS1704: fUptDS1704,
	completeDS1704: fCompleteDS1704,
	uncompleteDS1704: fUncompleteDS1704,
	DS1704deleteTodo: fDS1704deleteTodo
}