﻿// prettier-ignore
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
//import Draggable from 'react-draggable';
import { useActions } from "../../actions";
import * as DS1983Actions from "../../actions/DS1983";
import Background from '../../assets/images/DS249_1204.png';

interface Props {
	open: boolean;
	onClose: () => void;
}

export function DS1983Dialog(props: Props) {
	const { open, onClose } = props;
	const classes = useStyles();
	const [newDS1983Text, setNewDS1983Text] = React.useState("");
	const cDS1983Actions: any = useActions(DS1983Actions);

	const handleCreate = (event: any) => {
		cDS1983Actions.insDS1983({
			id: Math.random(),
			completed: false,
			DS1983ReceiptNo: event.target.value,
			TicketRange: '1000-1999',
			CartonNoRange: '10-89',
			NoOfParts: 4,
			ModifiedOn: Date.now,
			ModifiedBy: 'sef'
		});
		// reset text if user reopens the dialog
		setNewDS1983Text("new");
		onClose();

	};

	const handleClose = () => {
		// reset text if user reopens the dialog
		setNewDS1983Text("");
		onClose();

	};

	const handleUpdate = (event: any) => {
		cDS1983Actions.uptDS1983({
			id: Math.random(),
			completed: false,
			DS1983ReceiptNo: event.target.value,
			TicketRange: '1000-1999',
			CartonNoRange: '10-89',
			NoOfParts: 4,
			ModifiedOn: Date.now,
			ModifiedBy: 'sef'
		});
		setNewDS1983Text("updated");

	};

	return (
		<Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
			<DialogTitle>Add a new DS1983</DialogTitle>
			<DialogContent>
				<DialogContentText>
					THE CITY OF NEW YORK Department of Sanitation<br />
					NOTICE OF VIOLATION & HEARING Form DS1983
				</DialogContentText>
				<TextField
					autoFocus
					margin="dense"
					id="DS1983ReceiptNo"
					label="RECEIPT NUMBER"
					type="string"
					fullWidth
					value={newDS1983Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
				<TextField
					margin="dense"
					id="NoOfParts"
					label="Numb of Parts"
					type="string"
					size="medium"
					required
					value={newDS1983Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
				<TextField
					margin="dense"
					id="CartonNoRange"
					label="Carton No Range"
					type="string"
					size="small"
					required
					value={newDS1983Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
			</DialogContent>
			<DialogActions>
				<Button onClick={handleUpdate} color="secondary">
					Save/Update
				</Button>
				<Button onClick={handleCreate} color="primary">
					MAKE NEW DS1983
				</Button>
				<Button onClick={handleClose} color="primary">
					Done
				</Button>
			</DialogActions>

		</Dialog>

	);
}

const useStyles = makeStyles({
	newDS1983: {
		flexGrow: 1,
		height: "10%",
		paddingLeft: 15,
		paddingRight: 15,
		opacity: 60,
		backgroundImage: `url( ${Background} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '82px 103px',
		backgroundPosition: 'left top',
	},
	pending: {
		backgroundColor: "white",
	},
	approved: {
		backgroundColor: "yellow"
	},
	textField: {
		width: "80%",
		margin: 20,
	},
});
