﻿import { Grid, makeStyles, Paper } from '@material-ui/core';
import React from 'react';


export function Instructions() {
	const classes = useStyles();

	return (
		<div className={classes.styleforBorder}>
			<Grid item xs={6} component={Paper} className={classes.styleforInstructions}>
				<div >
					INSTRUCTION : <br />
					<ul className={classes.styleforOL}>
						<li>Select on the screen ‘Ticket Data’, ‘Ticket Location’ and ‘Ticket Status’.</li>
						<li>Update Ticket’s data: click on the button ’Update Ticket Details’ - the confirmation displays: 'Ticket Information Saved Successfully’.</li>
						<li>Display the updated DS156 Form: click on the link DS156 located inside the Form ‘Ticket Details’.</li>
						<li>Close the ‘Ticket Details’ screen by clicking on the close button ‘X’ on the upper-right corner of the screen.</li>
						<li>Update DS21 Form with entered/selected data: display the previously opened and non-updated yet FormDS21. Press the key ‘F5’ on the keyboard – the Form DS21 with the updated data is displayed.</li>
						<li>Display and Print the ‘Request to Void Summons’: click on the link DS1704 in the last column in the DS21 Form.</li>
					</ul>
				</div>
			</Grid>
			<Grid item xs={6} component={Paper} className={classes.paper}>
				&nbsp;
			</Grid>
		</div>
	)
}

const useStyles = makeStyles({
	paper: {
		width: "100%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 360,
		overflowX: "visible",
	},
	styleforInstructions: {
		marginBottom: 13,
		marginTop: 10,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 12,
		fontWeight: "normal",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "red",
		maxWidth: 550,
		minWidth: 300,
	},
	styleforOL: {
		listStyleType: "bullet"
	},
	styleforBorder: {
		borderStyle: "solid",
		borderWidth: 2,
	}
});