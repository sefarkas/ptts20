﻿import React from 'react';
import { useFormContext } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import { Button } from "@material-ui/core";
import styles from "./complaintStyles";
import { ComplaintWitness } from "../complaint";
import { withStyles } from '@material-ui/core/styles';
//import { StyledButton } from "../basicComponents";

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);


interface Props {
	witnesses: any;
	witnessOptions: any;
	handleChange: (e: any, control?: string, idx?: number) => void;
	handleRemoveWitness: (event: any, idx: number) => void;
	handleAddWitness: (event: any) => void;
	handleInputChange: (e: any, caller: string) => void;
}

export function ComplaintWitnesses(props: Props) {
	const classes = styles();
    const methods = useFormContext();

	return (
		<Grid container className={classes.grid} spacing={0}>
			<Grid container item xs={12} spacing={0}>
				<Grid item xs={3}>
					<label className={classes.labelReadOnlyData}>Section 4</label>
					<label className={classes.labelSectionTitle}>Witnesses</label>
				</Grid>
				{props.witnesses.map(function (d: any, idx: number) {
					return (
						<ComplaintWitness
							key={idx}
							witness={d}
							idx={idx}
							witnessOptions={props.witnessOptions}
							handleChange={props.handleChange}
							handleInputChange={props.handleInputChange}
							handleRemoveWitness={props.handleRemoveWitness} />
					)
				})}
			</Grid>
			<Grid container item xs={12} spacing={0}>
				<Grid item xs={3} />
				<Grid item xs={9}>
					<StyledButton
						className={classes.buttonInner}
						variant="outlined"
						color="primary"
						onClick={props.handleAddWitness}>
						+ Add Witness
				        </StyledButton>
				</Grid>
			</Grid>
		</Grid>
	);
}

