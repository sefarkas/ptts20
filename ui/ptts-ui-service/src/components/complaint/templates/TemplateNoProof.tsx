﻿import { Button, Box } from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';
import React, { useEffect } from 'react';
import ReactSelect from "react-select";
import { useForm } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers';
import styles from "./templateStyles";

interface Props {
	open: boolean;
	onSubmit: (statementValue: string) => void;
	onClose: (event: any) => void;
	respondent: string;
	incidentDate: string;
}

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const StyledButtonGreen = withStyles({
	root: {
		background: '#2b995f',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const customStyles = {
	control: (base: any) => ({
		...base,
		minHeight: '30px',
		height: '30px',
	}),
	dropdownIndicator: (base: any) => ({
		...base,
		padding: 0
	}),
	clearIndicator: (base: any) => ({
		...base,
		padding: 0
	}),
	valueContainer: (base: any) => ({
		...base,
		height: '30px',
		padding: '0 2px'
	}),
	indicatorContainer: (base: any) => ({
		height: '30px'
	}),
	options: (base: any) => ({
		height: '0px',
		padding: '0 0px'
	})
};

const timingOpts = [
	{ value: 1, label: "before" },
	{ value: 2, label: "after" }
];

const titleOpts = [
	{ value: 1, label: "A/M" },
	{ value: 2, label: "S/W" },
	{ value: 3, label: "Supv" },
	{ value: 4, label: "GS" },
	{ value: 5, label: "Other" }
];

const didToggleOpts = [
	{ value: 1, label: "did" },
	{ value: 2, label: "did not" }
];

export function TemplateNoProof(props: Props) {
	const classes = styles();
	const [templateValues, setTemplateValues] = React.useState({ title: { value: 0, label: "" }, didToggle: { value: 0, label: "" }, timing: { value: 0, label: "" } });

	const dialogSchema = yup.object().shape({
		onDate: yup.string().required("Required field"),
		assignedTo: yup.string().required("Required field"),
		title: yup.string().required("Required field"),
		shiftFrom: yup.string().required("Required field"),
		shiftTo: yup.string().required("Required field"),		
		didToggle: yup.string().required("Required field"),
		timing: yup.string().required("Required field"),
		incidentNumber: yup.number().required("Required field"),
		callTo: yup.string().required("Required field"),
		callAt: yup.string().required("Required field"),
		leaveFor: yup.string().required("Required field"),
		incidentsApproved: yup.number().required("Required field"),
		incidentsDenied: yup.number().required("Required field")
	});

	const { register, handleSubmit, errors, setValue, clearErrors } = useForm({
		resolver: yupResolver(dialogSchema)
	});

	const formatDate = (value: string) => {
		let date = value.split('-');
		const year = String(date[0]).split('-');
		const month = String(date[1]).split('-');
		const day = String(date[2]).split('-');
		return month + "/" + day + "/" + year;
	}

	React.useEffect(() => {
		register({ name: "template" });
		register({ name: "templateValues" });
		register({ name: "onDate" });
		register({ name: "assignedTo" });
		register({ name: "title" });
		register({ name: "shiftFrom" });
		register({ name: "shiftTo" });
		register({ name: "didToggle" });
		register({ name: "timing" });
		register({ name: "incidentNumber" });
		register({ name: "callTo" });
		register({ name: "callAt" });
		register({ name: "leaveFor" });
		register({ name: "incidentsApproved" });
		register({ name: "incidentsDenied" });
	}, []);

	React.useEffect(() => {
		setValue("onDate", props.incidentDate);
	}, [props.incidentDate]);

	React.useEffect(() => {
		setValue("onDate", props.incidentDate);
		setValue("assignedTo", "");
		setValue("shiftFrom", "");
		setValue("shiftTo", "");
		setValue("timing", "");
		setValue("didToggle", "");
		setValue("incidentNumber", "");
		setValue("callTo", "");
		setValue("callAt", "");
		setValue("leaveFor", "");
	    setValue("incidentsApproved", "");
		setValue("incidentsDenied", "");

		clearErrors("onDate");
		clearErrors("title");
		clearErrors("assignedTo");
		clearErrors("shiftFrom");
		clearErrors("shiftTo");
		clearErrors("timing");
		clearErrors("didToggle");
		clearErrors("incidentNumber");
		clearErrors("callTo");
		clearErrors("callAt");
		clearErrors("leaveFor");
		clearErrors("incidentsApproved");
		clearErrors("incidentsDenied");
	}, [props.open]);

	const onSubmit = (data: any) => {
		const title = templateValues.title.label === "Other" ? "" : templateValues.title.label + " ";
		let templateOutput = "On " + formatDate(data.onDate) + " " + title + props.respondent + " was assigned to " + data.assignedTo + " on the " + data.shiftFrom + " to " + data.shiftTo + " shift. "
		templateOutput = templateOutput + title + props.respondent + " called " + data.callTo + " at " + data.callAt + " requesting leave for " + data.leaveFor + ". ";
		templateOutput = templateOutput + "Emergency leave was granted pending proof. " + title + props.respondent + " failed to submit proof within 2 work days. As a result, ";
		templateOutput = templateOutput + title + props.respondent + " was marked absent and docked 8 hours pay. ";
		templateOutput = templateOutput + "This " + templateValues.didToggle.label + " occur " + templateValues.timing.label + " a scheduled day off. ";
		templateOutput = templateOutput + "This is " + title + props.respondent + "'s " + data.incidentNumber + " incident in a 12 month period, " + data.incidentsApproved;
		templateOutput = templateOutput + " prior emergencies were approved and " + data.incidentsDenied + " denied."; 
		props.onSubmit(templateOutput);
	};

	const handleChange = (e: any, control?: string) => {
		if (control === "onDate") {
			setValue("onDate", e.target.value);
			clearErrors("onDate");
		}
		else if (control === "title") {
			const selected = titleOpts.filter((d) => { return d.value === e.value; })[0].label;
			setTemplateValues({
				...templateValues,
				title: { value: e.value, label: selected }
			});
			setValue("title", e.value);
			clearErrors("title");
		}
		else if (control === "assignedTo") {
			setValue("assignedTo", e.target.value);
			clearErrors("assignedTo");
		}
		else if (control === "shiftFrom") {
			setValue("shiftFrom", e.target.value);
			clearErrors("shiftFrom");
		}
		else if (control === "shiftTo") {
			setValue("shiftTo", e.target.value);
			clearErrors("shiftTo");
		}
		else if (control === "didToggle") {
			const selected = didToggleOpts.filter((d) => { return d.value === e.value; })[0].label;
			setTemplateValues({
				...templateValues,
				didToggle: { value: e.value, label: selected }
			});
			setValue("didToggle", e.value);
			clearErrors("didToggle");
		}
		else if (control === "timing") {
			const selected = timingOpts.filter((d) => { return d.value === e.value; })[0].label;
			setTemplateValues({
				...templateValues,
				timing: { value: e.value, label: selected }
			});
			setValue("timing", e.value);
			clearErrors("timing");
		}
		else if (control === "incidentNumber") {
			setValue("incidentNumber", e.target.value);
			clearErrors("incidentNumber");
		}
		else if (control === "callTo") {
			setValue("callTo", e.target.value);
			clearErrors("callTo");
		}
		else if (control === "callAt") {
			setValue("callAt", e.target.value);
			clearErrors("callAt");
		}
		else if (control === "leaveFor") {
			setValue("leaveFor", e.target.value);
			clearErrors("leaveFor");
		}
		else if (control === "incidentsApproved") {
			setValue("incidentsApproved", e.target.value);
			clearErrors("incidentsApproved");
		}
		else if (control === "incidentsDenied") {
			setValue("incidentsDenied", e.target.value);
			clearErrors("incidentsDenied");
		}
	};

	const handleCancel = (event: any) => {
		props.onClose("");
	};

	const pad = (n: any) => {
		return ("00" + n).slice(-2);
	}

	const todaysDate = () => {
		const tempDate = new Date();
		const currDate = tempDate.getFullYear() + '-' + pad((tempDate.getMonth() + 1)) + '-' + pad((tempDate.getDate()));
		return currDate;
	}

	return (
		<form autoComplete="off">
			<Box className={classes.box}>
				<Grid container className={classes.grid} spacing={0}>
					<Grid item xs={12} className={classes.gridCellTemplate}>
						<label className={classes.labelTemplateText}>On </label>
						{errors.onDate && <label className={classes.error}>*</label>}
						<input type="date"
							name="onDate"
							defaultValue={props.incidentDate}
							placeholder="Select Date"
							className={classes.datepicker}
							max={todaysDate()}
							onChange={e => handleChange(e, "onDate")}
						/>
						<label className={classes.labelTemplateText}>, </label>
						{errors.title && <label className={classes.error}>*</label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})} /></div>
						<label className={classes.labelTemplateText}>{props.respondent} was assigned to </label>
						{errors.assignedTo && <label className={classes.error}>*</label>}
						<input name="assignedTo" className={classes.input} onChange={e => handleChange(e, "assignedTo")} />
						<label className={classes.labelTemplateText}> on the </label>
						{errors.shiftFrom && <label className={classes.error}>*</label>}
						<input name="shiftFrom" className={classes.input} onChange={e => handleChange(e, "shiftFrom")} />
						<label className={classes.labelTemplateText}> to </label>
						{errors.shiftTo && <label className={classes.error}>*</label>}
						<input name="shiftTo" className={classes.input} onChange={e => handleChange(e, "shiftTo")} />
						<label className={classes.labelTemplateText}> shift. </label>
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})}
							/></div>
						<label className={classes.labelTemplateText}>{props.respondent} called </label>
						{errors.callTo && <label className={classes.error}>*</label>}
						<input name="callTo" className={classes.inputLong} onChange={e => handleChange(e, "callTo")} />
						<label className={classes.labelTemplateText}> at </label>
						{errors.callAt && <label className={classes.error}>*</label>}
						<input name="callAt" className={classes.input} onChange={e => handleChange(e, "callAt")} />
						<label className={classes.labelTemplateText}> requesting leave for </label>
						{errors.leaveFor && <label className={classes.error}>*</label>}
						<input name="leaveFor" className={classes.inputLong} onChange={e => handleChange(e, "leaveFor")} />
						<label className={classes.labelTemplateText}>. Emergency leave was granted pending proof. </label>
						{errors.title && <label className={classes.error}>*</label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})} /></div>
						<label className={classes.labelTemplateText}>{props.respondent} failed to submit proof within 2 work days. As a result, </label>	
						{errors.title && <label className={classes.error}>*</label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})} /></div>
						<label className={classes.labelTemplateText}>{props.respondent} was marked absent and docked 8 hours pay. This </label>
						{errors.didToggle && <label className={classes.error}>*</label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "didToggle")}
								options={didToggleOpts}
								name="didToggle"
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})} /></div>
						<label className={classes.labelTemplateText}>occur </label>
						{errors.timing && <label className={classes.error}>*</label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "timing")}
								options={timingOpts}
								name="timing"
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})} /></div>
						<label className={classes.labelTemplateText}>a scheduled day off. This is </label>
						{errors.title && <label className={classes.error}>*</label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})} /></div>
						<label className={classes.labelTemplateText}>{props.respondent}'s </label>
						{errors.incidentNumber && <label className={classes.error}>*</label>}
						<input name="incidentNumber" className={classes.input} onChange={e => handleChange(e, "incidentNumber")} />
						<label className={classes.labelTemplateText}> incident in a 12 month period, </label>
						{errors.incidentsApproved && <label className={classes.error}>*</label>}
						<input name="incidentsApproved" className={classes.input} onChange={e => handleChange(e, "incidentsApproved")} />
						<label className={classes.labelTemplateText}> prior emergencies were approved and </label>
						{errors.incidentsDenied && <label className={classes.error}>*</label>}
						<input name="incidentsDenied" className={classes.input} onChange={e => handleChange(e, "incidentsDenied")} />
						<label className={classes.labelTemplateText}> denied.</label>
					</Grid>
				</Grid>
			</Box>
			<StyledButtonGreen
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleSubmit(onSubmit)}
			>Confirm
				    </StyledButtonGreen>
			<StyledButton
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleCancel}>
				Cancel
				</StyledButton>
		</form>
	);
}

