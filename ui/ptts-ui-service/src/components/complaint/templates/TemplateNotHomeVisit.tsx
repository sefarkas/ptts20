﻿import { Button, Box } from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';
import React, { useEffect } from 'react';
import ReactSelect from "react-select";
import { useForm } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers';
import styles from "./templateStyles";

interface Props {
	open: boolean;
	onSubmit: (statementValue: string) => void;
	onClose: (event: any) => void;
	respondent: string;
	incidentDate: string;
}

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const StyledButtonGreen = withStyles({
	root: {
		background: '#2b995f',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const customStyles = {
	control: (base: any) => ({
		...base,
		minHeight: '30px',
		height: '30px',
	}),
	dropdownIndicator: (base: any) => ({
		...base,
		padding: 0
	}),
	clearIndicator: (base: any) => ({
		...base,
		padding: 0
	}),
	valueContainer: (base: any) => ({
		...base,
		height: '30px',
		padding: '0 6px'
	}),
	indicatorContainer: (base: any) => ({
		height: '30px'
	})
};

const titleOpts = [
	{ value: 1, label: "A/M" },
	{ value: 2, label: "S/W" },
	{ value: 3, label: "Supv" },
	{ value: 4, label: "GS" },
	{ value: 5, label: "Other" }
];

const categoryOpts = [
	{ value: 1, label: "A" },
	{ value: 2, label: "B" },
	{ value: 3, label: "C" }
];

export function TemplateNotHomeVisit(props: Props) {
	const classes = styles();
	const [templateValues, setTemplateValues] = React.useState({ title: { value: 0, label: "" }, category: { value: 0, label: "" } });

	const dialogSchema = yup.object().shape({
		onDate: yup.string().required("Required field"),
		visitTime: yup.string().required("Required field"),
		title: yup.string().required("Required field"),
		category: yup.string().required("Required field"),
		visitAt: yup.string().required("Required field"),
		visitBy: yup.string().required("Required field")
	});

	const { register, handleSubmit, errors, setValue, clearErrors } = useForm({
		resolver: yupResolver(dialogSchema)
	});

	const formatDate = (value: string) => {
		let date = value.split('-');
		const year = String(date[0]).split('-');
		const month = String(date[1]).split('-');
		const day = String(date[2]).split('-');
		return month + "/" + day + "/" + year;
	}

	React.useEffect(() => {
		register({ name: "onDate" });
		register({ name: "title" });
		register({ name: "category" });
		register({ name: "visitTime" });
		register({ name: "visitAt" });
		register({ name: "visitBy" });
	}, []);

	React.useEffect(() => {
		setValue("onDate", props.incidentDate);
	}, [props.incidentDate]);

	React.useEffect(() => {
		setValue("onDate", props.incidentDate);
		setValue("visitTime", "");
		setValue("visitAt", "");
		setValue("visitBy", "");

		clearErrors("onDate");
		clearErrors("title");
		clearErrors("category");
		clearErrors("visitTime");
		clearErrors("visitAt");
		clearErrors("visitBy");
	}, [props.open]);

	const onSubmit = (data: any) => {
		const title = templateValues.title.label === "Other" ? "" : templateValues.title.label + " ";
		
		let templateOutput = "On " + formatDate(data.onDate) + " at " + data.visitTime + " hours a sick leave visit was made to ";
		templateOutput = templateOutput + title + props.respondent + ", a Category " + templateValues.category.label + " employee, at " + data.visitAt + " by " + data.visitBy;
		templateOutput = templateOutput + ", an authorized employee of the Supervised Sick Leave Unit. The employee was not at home and did not call for or receive permission from the Home Visitation Program to be away from home while on medical leave."
		props.onSubmit(templateOutput);
	};

	const handleChange = (e: any, control?: string) => {
		if (control === "onDate") {
			setValue("onDate", e.target.value);
			clearErrors("onDate");
		}
		else if (control === "title") {
			const selected = titleOpts.filter((d) => { return d.value === e.value; })[0].label;
			setTemplateValues({
				...templateValues,
				title: { value: e.value, label: selected }
			});
			setValue("title", e.value);
			clearErrors("title");
		}
		else if (control === "category") {
			const selected = categoryOpts.filter((d) => { return d.value === e.value; })[0].label;
			setTemplateValues({
				...templateValues,
				category: { value: e.value, label: selected }
			});
			setValue("category", e.value);
			clearErrors("category");
		}
		else if (control === "visitTime") {
			setValue("visitTime", e.target.value);
			clearErrors("visitTime");
		}
		else if (control === "visitAt") {
			setValue("visitAt", e.target.value);
			clearErrors("visitAt");
		}
		else if (control === "visitBy") {
			setValue("visitBy", e.target.value);
			clearErrors("visitBy");
		}
	};

	const handleCancel = (event: any) => {
		props.onClose("");
	};

	const pad = (n: any) => {
		return ("00" + n).slice(-2);
	}

	const todaysDate = () => {
		const tempDate = new Date();
		const currDate = tempDate.getFullYear() + '-' + pad((tempDate.getMonth() + 1)) + '-' + pad((tempDate.getDate()));
		return currDate;
	}

	return (
		<form autoComplete="off">
			<Box className={classes.box}>
				<Grid container className={classes.grid} spacing={0}>
					<Grid item xs={12} className={classes.gridCellTemplate}>
						<label className={classes.labelTemplateText}>On </label>
						{errors.onDate && <label className={classes.error}>* </label>}
						<input type="date"
							name="onDate"
							defaultValue={props.incidentDate}
							placeholder="Select Date"
							className={classes.datepicker}
							max={todaysDate()}
							onChange={e => handleChange(e, "onDate")}
						/>
						<label className={classes.labelTemplateText}> at </label>
						{errors.visitTime && <label className={classes.error}>* </label>}
						<input name="visitTime" className={classes.input} onChange={e => handleChange(e, "visitTime")} />
						<label className={classes.labelTemplateText}> hours a sick leave visit was made to </label>
						{errors.title && <label className={classes.error}>* </label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})}
							/></div>
						<label className={classes.labelTemplateText}>{props.respondent}, a category </label>
						{errors.category && <label className={classes.error}>* </label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "category")}
								options={categoryOpts}
								name="category"
								value={templateValues.category}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})}
							/></div>
						<label className={classes.labelTemplateText}>employee, at </label>
						{errors.visitAt && <label className={classes.error}>* </label>}
						<input name="visitAt" className={classes.inputLong} onChange={e => handleChange(e, "visitAt")} />
						<label className={classes.labelTemplateText}> by </label>
						{errors.visitBy && <label className={classes.error}>* </label>}
						<input name="visitBy" className={classes.inputLong} onChange={e => handleChange(e, "visitBy")} />
						<label className={classes.labelTemplateText}>, an authorized employee of the Supervised Sick Leave Unit. The employee was not at home and did not call for or receive permission from the Home Visitation Program to be away from home while on medical leave.</label>
					</Grid>
				</Grid>
			</Box>
			<StyledButtonGreen
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleSubmit(onSubmit)}
			>Confirm
				    </StyledButtonGreen>
			<StyledButton
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleCancel}>
				Cancel
				</StyledButton>
		</form>
	);
}

