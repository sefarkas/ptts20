﻿import { Button } from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useEffect, useState } from "react"; // , useReducer
import { useForm } from "react-hook-form"; // , FormProvider
import { isNullOrUndefined } from 'util';
import { PTTSfunctions } from '../../components/basicComponents';
import { allServices } from '../../services';
import { history } from "../../configureStore";
import { DS1983 } from '../../model';

type postDataResult<T> = {
	success: boolean;
	errors?: { [P in keyof T]?: string[] };
};

type FormData = {
	ds1983ReceiptNumber: string;
};

export function ExistingDS1983Form(props: any) {
	const classes = useStyles();

	const [isLoading, setLoading] = React.useState(false);

	const [lastgoodrcpt, setlastgoodrcpt] = useState("");
	const [rcpt, setrcpt] = useState(props.match.params.onercpt);

	const [oneDS1983form, setoneDS1983form] = useState<DS1983>(
		{
			ReceiptNumber: "",
			MessengerName: "",
			SummonsCount: 0,
			SummonsFromNumber: 0,
			SummonsToNumber: 0,
			Borough: "",
			DistributorName: "",
			DistributorSignature: "",
			DistributorSignedDate: new Date(),
			MessengerSignature: "",
			MessengerTitle: "",
			MessengerSignedDate: new Date(),
			AcknowledgedBy: "",
			ID: 0,
			BoxNumber: 0,
			FormStatus: "",
			ModifiedOn: new Date(),
			ModifiedBy: "",
			BadgeNo: "",
			DistributorRefNo: "",
			CreatedBy: "",
			CreatedDate: new Date(),
			DistrictID: 0,
			PayCode: "",
			IsVoid: false,

		});

	const [ReceiptNumberState, setReceiptNumberState] = useState("");
	const [MessengerNameState, setMessengerNameState] = useState("");
	const [SummonsCountState, setSummonsCountState] = useState(0);
	const [SummonsFromNumberState, setSummonsFromNumberState] = useState(0);
	const [SummonsToNumberState, setSummonsToNumberState] = useState(0);
	const [BoroughState, setBoroughState] = useState("");
	const [DistributorNameState, setDistributorNameState] = useState("");
	const [DistributorSignatureState, setDistributorSignatureState] = useState("");
	const [DistributorSignedDateState, setDistributorSignedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [MessengerSignatureState, setMessengerSignatureState] = useState("");
	const [MessengerTitleState, setMessengerTitleState] = useState("");
	const [MessengerSignedDateState, setMessengerSignedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [AcknowledgedByState, setAcknowledgedByState] = useState("");
	const [IDState, setIDState] = useState(0);
	const [BoxNumberState, setBoxNumberState] = useState(0);
	const [FormStatusState, setFormStatusState] = useState("");
	const [ModifiedOnState, setModifiedOnState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [ModifiedByState, setModifiedByState] = useState("");
	const [BadgeNoState, setBadgeNoState] = useState("");
	const [DistributorRefNoState, setDistributorRefNoState] = useState("");
	const [CreatedByState, setCreatedByState] = useState("");
	const [CreatedDateState, setCreatedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [DistrictIDState, setDistrictIDState] = useState(0);
	const [PayCodeState, setPayCodeState] = useState("");
	const [IsVoidState, setIsVoidState] = useState(false);

	useEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useEffect after onChange handle.");
		setoneDS1983form({ // take the individual hook const and set each oneDS1983form object properties
			ReceiptNumber: ReceiptNumberState,
			MessengerName: MessengerNameState,
			SummonsCount: SummonsCountState,
			SummonsFromNumber: SummonsFromNumberState,
			SummonsToNumber: SummonsToNumberState,
			Borough: BoroughState,
			DistributorName: DistributorNameState,
			DistributorSignature: DistributorSignatureState,
			DistributorSignedDate: new Date(DistributorSignedDateState),
			MessengerSignature: MessengerSignatureState,
			MessengerTitle: MessengerTitleState,
			MessengerSignedDate: new Date(MessengerSignedDateState),
			AcknowledgedBy: AcknowledgedByState,
			ID: IDState,
			BoxNumber: BoxNumberState,
			FormStatus: FormStatusState,
			ModifiedOn: new Date(ModifiedOnState),
			ModifiedBy: ModifiedByState,
			BadgeNo: BadgeNoState,
			DistributorRefNo: DistributorRefNoState,
			CreatedBy: CreatedByState,
			CreatedDate: new Date(CreatedDateState),
			DistrictID: DistrictIDState,
			PayCode: PayCodeState,
			IsVoid: IsVoidState,


		});
	},
		[
			ReceiptNumberState,
			MessengerNameState,
			SummonsCountState,
			SummonsFromNumberState,
			SummonsToNumberState,
			BoroughState,
			DistributorNameState,
			DistributorSignatureState,
			DistributorSignedDateState,
			MessengerSignatureState,
			MessengerTitleState,
			MessengerSignedDateState,
			AcknowledgedByState,
			IDState,
			BoxNumberState,
			FormStatusState,
			ModifiedOnState,
			ModifiedByState,
			BadgeNoState,
			DistributorRefNoState,
			CreatedByState,
			CreatedDateState,
			DistrictIDState,
			PayCodeState,
			IsVoidState,

		]
	);

	const handleOnChange = (e: any) => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
		switch (e.target.name) {
			case "fldReceiptNumber": setReceiptNumberState(e.target.value); break;
			case "fldMessengerName": setMessengerNameState(e.target.value); break;
			case "fldSummonsCount": setSummonsCountState(e.target.value); break;
			// case "fldSummonsFromNumber": setSummonsFromNumberState(e.target.value); break;
			// case "fldSummonsToNumber": setSummonsToNumberState(e.target.value); break;
			case "fldBorough": setBoroughState(e.target.value); break;
			case "fldDistributorName": setDistributorNameState(e.target.value); break;
			case "fldDistributorSignature": setDistributorSignatureState(e.target.value); break;
			case "fldDistributorSignedDate": setDistributorSignedDateState(e.target.value); break;
			case "fldMessengerSignature": setMessengerSignatureState(e.target.value); break;
			case "fldMessengerTitle": setMessengerTitleState(e.target.value); break;
			case "fldMessengerSignedDate": setMessengerSignedDateState(e.target.value); break;
			case "fldAcknowledgedBy": setAcknowledgedByState(e.target.value); break;
			case "fldID": setIDState(e.target.value); break;
			case "fldBoxNumber": setBoxNumberState(e.target.value); break;
			case "fldFormStatus": setFormStatusState(e.target.value); break;
			case "fldModifiedOn": setModifiedOnState(e.target.value); break;
			case "fldModifiedBy": setModifiedByState(e.target.value); break;
			case "fldBadgeNo": setBadgeNoState(e.target.value); break;
			case "fldDistributorRefNo": setDistributorRefNoState(e.target.value); break;
			case "fldCreatedBy": setCreatedByState(e.target.value); break;
			case "fldCreatedDate": setCreatedDateState(e.target.value); break;
			case "fldDistrictID": setDistrictIDState(e.target.value); break;
			case "fldPayCode": setPayCodeState(e.target.value); break;
			case "fldIsVoid": setIsVoidState(e.target.value); break;

			case "fldSummonsFromNumber":
				let dummy2: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				setSummonsFromNumberState(dummy2);
				let updatedTktNo2 = dummy2 + Number.parseInt(SummonsCountState.toString()) - 1;
				dummy2 > 9999999 && SummonsCountState > 0 ? setSummonsToNumberState(updatedTktNo2) : setSummonsToNumberState(0);
				dummy2 = 0;
				break;

			default:
				// code block
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.value);
		}
	}


	useEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("initial useEffect triggered for " + rcpt);
		if (!isNullOrUndefined(rcpt)) {
			if (rcpt.length > 0 && rcpt !== lastgoodrcpt) {
				setlastgoodrcpt(rcpt);
				(async () => {
					setLoading(true);
					const response = await allServices.PTTSgetServices.anDS1983(rcpt);
					const rspJson = await response.data;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("api response for ", rcpt);
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rspJson);
					if (!isNullOrUndefined(rspJson)) {
						if (rspJson.BoxNumber !== undefined) {
							setReceiptNumberState(rspJson.ReceiptNumber);
							setMessengerNameState(rspJson.MessengerName);
							setSummonsCountState(rspJson.SummonsCount);
							setSummonsFromNumberState(rspJson.SummonsFromNumber);
							setSummonsToNumberState(rspJson.SummonsToNumber);
							setBoroughState(rspJson.Borough);
							setDistributorNameState(rspJson.DistributorName);
							setDistributorSignatureState(rspJson.DistributorSignature);
							setDistributorSignedDateState(rspJson.DistributorSignedDate);
							setMessengerSignatureState(rspJson.MessengerSignature);
							setMessengerTitleState(rspJson.MessengerTitle);
							setMessengerSignedDateState(rspJson.MessengerSignedDate);
							setAcknowledgedByState(rspJson.AcknowledgedBy);
							setIDState(rspJson.ID);
							setBoxNumberState(rspJson.BoxNumber);
							setFormStatusState(rspJson.FormStatus);
							setModifiedOnState(rspJson.ModifiedOn);
							setModifiedByState(rspJson.ModifiedBy);
							setBadgeNoState(rspJson.BadgeNo);
							setDistributorRefNoState(rspJson.DistributorRefNo);
							setCreatedByState(rspJson.CreatedBy);
							setCreatedDateState(rspJson.CreatedDate);
							setDistrictIDState(rspJson.DistrictID);
							setPayCodeState(rspJson.PayCode);
							setIsVoidState(rspJson.IsVoid);


						}
					}
					setLoading(false);
				})();
			}
		}
	}, []);

	useEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useEffect triggered for " + rcpt);
		if (!isNullOrUndefined(rcpt)) {
			if (rcpt.length > 0 && rcpt !== lastgoodrcpt) {
				setlastgoodrcpt(rcpt);
				(async () => {
					setLoading(true);
					const response = await allServices.PTTSgetServices.anDS1983(rcpt);
					const rspJson = await response.data;
					if (!isNullOrUndefined(rspJson)) {
						if (rspJson.BoxNumber !== undefined) {
							setoneDS1983form({
								ReceiptNumber: rspJson.ReceiptNumber,
								MessengerName: rspJson.MessengerName,
								SummonsCount: rspJson.SummonsCount,
								SummonsFromNumber: rspJson.SummonsFromNumber,
								SummonsToNumber: rspJson.SummonsToNumber,
								Borough: rspJson.Borough,
								DistributorName: rspJson.DistributorName,
								DistributorSignature: rspJson.DistributorSignature,
								DistributorSignedDate: rspJson.DistributorSignedDate,
								MessengerSignature: rspJson.MessengerSignature,
								MessengerTitle: rspJson.MessengerTitle,
								MessengerSignedDate: rspJson.MessengerSignedDate,
								AcknowledgedBy: rspJson.AcknowledgedBy,
								ID: rspJson.ID,
								BoxNumber: rspJson.BoxNumber,
								FormStatus: rspJson.FormStatus,
								ModifiedOn: rspJson.ModifiedOn,
								ModifiedBy: rspJson.ModifiedBy,
								BadgeNo: rspJson.BadgeNo,
								DistributorRefNo: rspJson.DistributorRefNo,
								CreatedBy: rspJson.CreatedBy,
								CreatedDate: rspJson.CreatedDate,
								DistrictID: rspJson.DistrictID,
								PayCode: rspJson.PayCode,
								IsVoid: rspJson.IsVoid,

							});
						}
					}
					setLoading(false);

				})();
			}
		}
	}, [lastgoodrcpt]);

	const { register, handleSubmit, errors, formState } = useForm<FormData>();

	const submitForm = async (data: FormData, e: any) => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Submission starting", oneDS1983form);

		const response = await allServices.PTTSiudServices.iudDS1983(oneDS1983form);
		// response is the new receipt number
		if (!response) {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("I am having trouble with saving DS1983 values.");
			// any error message is in the CustomerOrderNo property in the controller
		}
		else {
			history.push('/PTTS/existingDS1983/' + oneDS1983form.ReceiptNo);
		}
		e.target.reset(); // reset after form submit
	};

	const onCancel = () => {
		history.push('/PTTS/home');
	};

	const newDS1983 = () => {
		history.push('/PTTS/newDS1983');
	};

	if (isLoading) {
		return (<div>Loading ...</div>);
	}
	else {

		const renderActionButtons = () => {
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("oneDS1983form.DS1983ReceiptNo is ", oneDS1983form.DS1983ReceiptNo);
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(oneDS1983form.DS1983ReceiptNo);
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(!isNullOrUndefined(oneDS1983form.DS1983ReceiptNo));
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(!oneDS1983form.DS1983ReceiptNo ? 0 : oneDS1983form.DS1983ReceiptNo.length);
			if (!isNullOrUndefined(oneDS1983form.DS1983ReceiptNo)) // prevent save/update
			{
				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							type="submit"
						>
							Update
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="secondary"
							onClick={() => newDS1983()}
						>
							Make new DS1983
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="secondary">
							Create DS-1983
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}
			else if (PTTSfunctions.Commonfunctions2.isNotDefined(oneDS1983form.DS1983ReceiptNo)) // prevent save/update
			{
				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={() => newDS1983()}
						>
							Make new DS1983
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}
			else {

				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							type="submit"
						>
							Update
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary">
							Create DS-1983
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}

		};

		return (
			<form onSubmit={handleSubmit(submitForm)}>
				<Grid className={classes.formposition}>
					<Grid container className={classes.darkborder}>
						<Grid item xs={2} className={classes.styleforlogo}>
							<img src={PTTSfunctions.Logos.DSNYLogo.logoImg} alt="Sanitation Enforcement" />
						</Grid>
						<Grid item xs={9} className={classes.styleforformtitle}>
							<div className={classes.styleforformtitletext}>THE CITY OF NEW YORK Department of Sanitation</div>
							<div className={classes.styleforformtitletext}>ECB - SUMMONS DISTRIBUTION RECEIPT  DS 1983 (4-99)</div>
							<div className={classes.styleforformtitletext}>RECEIPT NO:&nbsp;
							<div className={classes.styleforformreceipttextred}>{oneDS1983form.ReceiptNumber}</div></div>
							<div className={classes.styleforformtitletext}>BOX NO:&nbsp;
								<input type="number" onChange={(e) => handleOnChange(e)} name="fldBoxNumber" ref={register}
									value={BoxNumberState}
								/>
							</div>
						</Grid>
					</Grid>
					<div>&nbsp;</div>
					<div>&nbsp;</div>
					<Grid container>
						<Grid item xs={12}>
							{renderActionButtons()}
						</Grid>
					</Grid>
				</Grid>
				<Grid className={classes.formposition}>
					<Grid container className={classes.darkborder}>
						<Grid item xs={12} className={classes.stylefortopacknowledgement}>
							<div className={classes.stylefortopacknowledgementtext}>
								<input className={classes.stylefortopacknowledgementtext} type="text" onChange={(e) => handleOnChange(e)} name="fldAcknowledgedBy" ref={register}
									value={AcknowledgedByState}
								/> acknowledged receipt of 
								<input className={classes.stylefortopacknowledgementtext} type="number" onChange={(e) => handleOnChange(e)} name="fldSummonsCount" ref={register}
									value={SummonsCountState}
								/>&nbsp;(ECB) Notice of Violations
							</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={5} className={classes.styleforticketrange}>
							<div className={classes.styleforticketrangetext}>
								Numbered from:&nbsp;
								<input className={classes.styleforticketrangetext} type="number" onChange={(e) => handleOnChange(e)} name="fldSummonsFromNumber" ref={register}
									value={SummonsFromNumberState}
								/>
							</div>
						</Grid>
						<Grid item xs={5} className={classes.styleforticketrange}>
							<div className={classes.styleforticketrangetext}>
								&nbsp;to:&nbsp;
								<input className={classes.styleforticketrangetext} type="number" onChange={(e) => handleOnChange(e)} name="fldSummonsToNumber" ref={register}
									value={SummonsToNumberState}
								/>
							</div>
						</Grid>
					</Grid>
					<Grid item xs={12} className={classes.darkborder}>
						<Grid className={classes.stylefortopacknowledgement} >
							<div className={classes.stylefortopacknowledgementtext}>
								for delivery to&nbsp;
								<input className={classes.stylefortopacknowledgementtext} type="text" onChange={(e) => handleOnChange(e)} name="fldBorough" ref={register}
									value={BoroughState}
								/>
							</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={4}>
							<Grid container>
								<Grid item xs={12} >
			
									<div className={classes.stylefortopdistributionrow } >DISTRIBUTOR'S NAME</div>
									<div >
										<input className={classes.stylefortopdistributionrowtextwide} type="text" onChange={(e) => handleOnChange(e)} name="fldDistributorName" ref={register}
										value={DistributorNameState}
										/>
									</div>
								</Grid>
								<Grid item xs={12} >
									<div >SIGNATURE</div>
								</Grid>
							</Grid>
						</Grid>
						<Grid item xs={2}>
							<div >DATE</div>
							<div >
								<input className={classes.stylefortopdistributionrowtext} type="text" onChange={(e) => handleOnChange(e)} name="fldDistributorSignedDate" ref={register}
									value={PTTSfunctions.Commonfunctions.formatDateSlashes((DistributorSignedDateState))}
								/>
							</div>
						</Grid>
						<Grid item xs={4}  >
							<Grid container>
								<Grid item xs={12}>
									<div className={classes.stylefortopdistributionrow}>MESSENGERS NAME</div>
									<div className={classes.thinborderLeft} >
										<input className={classes.stylefortopdistributionrowtextwide} type="text" onChange={(e) => handleOnChange(e)} name="fldMessengerName" ref={register}
											value={MessengerNameState}
										/>
									</div>
								</Grid>
								<Grid container className={classes.thinborderTop}>
									<Grid item xs={6}>
										<div >SIGNATURE</div>
									</Grid>
									<Grid item xs={6} className={classes.thinborderLeft}>
										<div className={classes.stylefortopdistributionrow}>TITLE</div>
										<div className={classes.thinborderLeft}>
											<input className={classes.stylefortopdistributionrowtext} type="text" onChange={(e) => handleOnChange(e)} name="fldMessengerTitle" ref={register}
												value={MessengerTitleState}
											/>
										</div>
									</Grid>
								</Grid>

							</Grid>
						</Grid>
						<Grid item xs={2}>
							<Grid container>
								<Grid item xs={12} className={classes.thinborderLeft}>
									<div className={classes.stylefortopdistributionrow}>BADGE NO</div>
									<div className={classes.thinborderLeft}>
										<input className={classes.stylefortopdistributionrowtext} type="text" onChange={(e) => handleOnChange(e)} name="fldBadgeNo" ref={register}
											value={BadgeNoState}
										/>
									</div>
								</Grid>
								<Grid item xs={12} className={classes.thinborderTop}>
									<div className={classes.stylefortopdistributionrow} >DATE</div>
									<div className={classes.thinborderLeft}>
										<input className={classes.stylefortopdistributionrowtext} type="text" onChange={(e) => handleOnChange(e)} name="fldMessengerSignedDate" ref={register}
											value={PTTSfunctions.Commonfunctions.formatDateSlashes((MessengerSignedDateState))}
										/>
									</div>
								</Grid>
							</Grid>
						</Grid>
					</Grid>
					<Grid item xs={12} className={classes.stylefortopacknowledgement}>
						<div className={classes.stylefortopacknowledgementtext}>
							I am in possession of 1000 (ECB) Notices, box #
							<input className={classes.stylefortopacknowledgementtext} type="number" onChange={(e) => handleOnChange(e)} name="fldFactoryJobNo" ref={register}
								value={BoxNumberState}
							/>
							<StyledButton
								className={classes.button}
								variant="outlined"
								color="primary"
								type="submit"
							>
								Acknowledge
							</StyledButton>
							<StyledButton
								className={classes.button}
								variant="outlined"
								color="secondary"
								onClick={onCancel}>
								Send back to Distributor
						</StyledButton>
						</div>
					</Grid>
					<Grid item xs={12}>
						{renderActionButtons()}
					</Grid>
				</Grid>

			</form>
		)
	}

};


const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5,
		marginTop: 0
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const useStyles = makeStyles({
	root: {
		flexGrow: 1,
		height: "100%",
		paddingLeft: 15,
		paddingRight: 15,

	},
	formposition: {
		marginLeft: 280,
	},
	darkborder: {
		borderWidth: 3,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderTop: {
		borderTopWidth: 2,
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderLeft: {
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderTopWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	styleforlogo: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforsubmit: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforformtitle: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
	},
	styleforformtitletext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "black",
		minWidth: 500,
	},
	styleforformorder: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "center",
	},
	styleforformordertext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "center",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 500,
		color: "black",
	},
	styleforticketrange: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
	},
	stylefortopacknowledgement: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	stylefortopacknowledgementtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 320,
		minWidth: 130
	},
	stylefortopdistributionrow: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
		maxWidth: 300,
	},
	stylefortopdistributionrowtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80
	},
	stylefortopdistributionrowtextwide: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		minWidth: 300,
		maxWidth: 600,
	},
	styleforformreceipt: {
		verticalAlign: "center",
		borderTopStyle: "none",
	},
	styleforformreceipttext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		display: "inline",
		marginLeft: 10,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
		textAlign: "center",
	},
	styleforformreceipttextred: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "red",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
	},
	button: {
		marginTop: 20,
	},

	formstyle: {
		maxWidth: "100%",
		padding: 0,
	},

	input: {
		display: "block",
		borderRadius: 4,
		border: "1px solid",
		padding: "10px 15px",
		marginBottom: 10,
		fontSize: 14,
		maxWidth: 400,
		minWidth: 200,
	},

	reactselect: {
		maxWidth: 400,
	},

	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginBottom: 13,
		marginTop: 20,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
	},
	buttonSubmit: {
		background: "#0e8c1b",
		color: "white",
		border: "none",
		marginTop: 20,
		padding: 20,
		fontSize: 16,
		fontWeight: 100,
		width: 200,
	}
});
