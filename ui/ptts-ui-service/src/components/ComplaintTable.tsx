// prettier-ignore
import { Checkbox, IconButton, Paper, Table, TableBody, TableCell, TableHead, TableRow } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useSelector } from "react-redux";
import { useActions } from "../actions";
import * as ComplaintActions from "../actions/complaint";
import { Complaint } from "../model";
import { tGiantState } from "../reducers";

export function ComplaintTable() {
	const classes = useStyles();
	const complaintList = useSelector((state: tGiantState) => state.complaintList);
	const complaintActions = useActions(ComplaintActions);

	const onRowClick = (complaint: Complaint) => {
		if (complaint.completed) {
			complaintActions.uncompleteComplaint(complaint.id);
		} else {
			complaintActions.completeComplaint(complaint.id);
		}
	};

	return (
		<Paper className={classes.paper}>
			<Table className={classes.table}>
				<TableHead>
					<TableRow>
						<TableCell padding="default">Completed</TableCell>
						<TableCell padding="default">Index</TableCell>
						<TableCell padding="default">Respondent</TableCell>
						<TableCell padding="default">Text</TableCell>
						<TableCell padding="default">Delete</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{complaintList.map((n: Complaint) => {
						return (
							<TableRow
								key={n.id}
								hover
								onClick={event => onRowClick(n)}
							>
								<TableCell padding="none">
									<Checkbox checked={n.completed} />
								</TableCell>
								<TableCell padding="none">{n.id}</TableCell>
								<TableCell padding="none">{n.respondent}</TableCell>
								<TableCell padding="none">{n.text}</TableCell>
								<TableCell padding="none">
									<IconButton
										aria-label="Delete"
										color="default"
										onClick={() =>
											complaintActions.deleteTodo(n.id)
										}
									>
										<DeleteIcon />
									</IconButton>
								</TableCell>
							</TableRow>
						);
					})}
				</TableBody>
			</Table>
		</Paper>
	);
}

const useStyles = makeStyles({
	paper: {
		width: "100%",
		minWidth: 260,
		display: "inline-block",
	},
	table: {
		width: "100%",
	},
});
