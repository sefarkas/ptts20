﻿// prettier-ignore
import { Checkbox, IconButton, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useState } from "react";
import { allActions } from "../../actions";
import { history } from "../../configureStore";
import { ECB603 } from "../../model";
import { allServices } from '../../services';
import { PTTSfunctions } from '../basicComponents';

export function ECB603Table() {
	console.log("start ECB603 table");
	const classes = useStyles();
	const todaysDate: Date = new Date();
	const dummyECB603: ECB603 = {
		CartonNoFrom: 0,
		CartonNoTo: 0,
		CartonQty: 0,
		CreatedBy: "",
		CreatedOn: todaysDate,
		CustomerOrderNo: "",
		DS1983ReceiptNo: "",
		FactoryJobNo: "",
		ID: 0,
		IsVoid: false,
		ModifiedBy: "",
		ModifiedOn: todaysDate,
		NoOfParts: 0,
		OrderNo: "",
		ReceiptNo: "",
		TicketNoFrom: 0,
		TicketNoTo: 0,
	}

	const [ecblist, setecblist] = useState([dummyECB603]);
	const [Loading, setLoading] = useState(false);

	React.useEffect(() => {
		(async () => {
			setLoading(true);
			const response = await allServices.PTTSgetServices.ecb603List(); // axios always returns an object with a "data" child
			const rdata: ECB603[] = response.data;
			if (!rdata) {
				setLoading(true);
			}
			else {
				const rdataDesc: ECB603[] = rdata.sort(function (a, b) {
					var nameA = !a.ReceiptNo ? "" : a.ReceiptNo.toUpperCase(); // ignore upper and lowercase
					var nameB = !b.ReceiptNo ? "" : b.ReceiptNo.toUpperCase(); // ignore upper and lowercase
					if (nameA < nameB) {
						return 1;
					}
					if (nameA > nameB) {
						return -1;
					}
					// names must be equal
					return 0;
				}).slice(0, 29); // DESC
				setecblist(rdataDesc);
				setLoading(false);
			}
		})();
	}, []);

	// React.useEffect(() => {
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("ecblist is");
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(ecblist);
	// }, [ecblist]);

	const handleCellClick = (e: any) => {
		history.push('/PTTS/existingECB603/' + e.target.textContent)
    }

	return (
		<TableContainer component={Paper}>
			<Table className={classes.table} size="small" aria-label="a dense table">
				<TableHead>
					<TableRow>
						<TableCell size="small" align="right">ECB603 Receipt No</TableCell>
						<TableCell size="small" align="right">Ticket Range</TableCell>
						<TableCell size="small" align="right">Carton No Range</TableCell>
						<TableCell size="small" align="right">No of Parts</TableCell>
						<TableCell size="small" align="right">Modified on</TableCell>
						<TableCell size="small" align="left">Modified by</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{
						// @ts-ignore: Object is possibly 'null'.
						Loading || !ecblist || ecblist === undefined ? <TableRow><TableCell>working ...</TableCell></TableRow> : ecblist.map((n: ECB603) => {
							//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("n is:");
							//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(n);
							if (!n.ID) {
								return (<TableRow key= {"-1"}><TableCell key={"0"}>Working ...</TableCell></TableRow>)
							}
							else {
								return (
								<TableRow
									key={n.ID.toString() + "row"}
									hover
								>
										<TableCell key={n.ID.toString() + "rcptno"} size="small" align="right" onClick={handleCellClick}>{n.ReceiptNo}</TableCell>
										<TableCell key={n.ID.toString() + "TKTRNG"} size="small" align="right">{n.TicketNoFrom}-{n.TicketNoTo}</TableCell>
										<TableCell key={n.ID.toString() + "CARRNG"} size="small" align="right">{n.CartonNoTo}-{n.CartonNoFrom}</TableCell>
										<TableCell key={n.ID.toString() + "PARRNG"} size="small" align="right">{n.NoOfParts}</TableCell>
										<TableCell key={n.ID.toString() + "MODBY"} size="small" align="right">{n.ModifiedOn}</TableCell>
										<TableCell key={n.ID.toString() + "MODON"} size="small" align="left">{n.ModifiedBy}</TableCell>
										<TableCell size="small" align="left" key={n.ID.toString() + "ico"} >
										<IconButton
											aria-label="Delete"
											color="default"
											onClick={() =>
												allActions.ECB603acts.ECB603deleteTodo(n.ID)
											}
										>
											<DeleteIcon />
										</IconButton>
									</TableCell>
								</TableRow>
							);
                            }

						})}
				</TableBody>
			</Table>
		</TableContainer>
	);
}

const useStyles = makeStyles({
	paper: {
		width: "70%",
		display: "block",
	},
	table: {
		width: "100%",
		marginLeft: 220,
	},
	cell: {
		minWidth: 100,
		maxWidth: 280,
    }
});
