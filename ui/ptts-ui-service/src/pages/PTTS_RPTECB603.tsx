﻿import { Button, Grid, Typography } from "@material-ui/core";
import { Theme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { ECB603Dialog, ECB603Table } from "../components/ECB603";
import { withStyles } from '@material-ui/core/styles';

const StyledButton = withStyles({
	root: {
		background: '#2b995f',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '15 15 15 15px',
	},
	label: {
		color: 'white',
	},
})(Button);


export function PTTSECB603() {
	console.log("start ECB603");
	const classes = useStyles();
	const [open, setOpen] = React.useState(false);

	const handleClose = () => {
		setOpen(false);
	};

	const handleAddECB603 = () => {
		setOpen(true);
	};

	return (
		<Grid container className={classes.root}>
			<ECB603Dialog open={open} onClose={handleClose} />
			<Grid item xs={6}>
				<Typography variant="h4" gutterBottom>
					ECB603 List
				</Typography>
			</Grid>
			<Grid item xs={6}>
				<div className={classes.buttonContainer}>
					<StyledButton
						className={classes.button}
						variant="contained"
						color="secondary"
						onClick={handleAddECB603}
					>
						+ Add ECB603
					</StyledButton>
				</div>
			</Grid>
			<Grid item xs={12}>
				<ECB603Table />
			</Grid>
		</Grid>
	);
}

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: 20,
		[theme.breakpoints.down("md")]: {
			paddingTop: 50,
			paddingLeft: 15,
			paddingRight: 15,
		},
	},

	buttonContainer: {
		width: "100%",
		display: "flex",
		justifyContent: "flex-end",
	},

	button: {
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	dialog: {
		minHeight: 500
	}
}));
