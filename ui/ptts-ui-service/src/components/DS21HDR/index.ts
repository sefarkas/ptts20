﻿export { DS21HDRtable } from "./Table"; // one DS21 per DS1984

export interface DS21HDRbasics {
    DS21HDRreceiptNumber: string,
    DS21HDRfromTktNumber: number,
    DS21HDRtoTktNumber: number,
    DS21HDRDistrictState: string
};