﻿// prettier-ignore
import {
    Checkbox, Grid, Link, Paper, Table, TableBody, TableCell,
    TableContainer, TableHead, TableRow
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { allActions } from "../../actions";
import { tOneNOV } from "../../model";
import { DS21CardBottom } from ".";
import { dummies } from "../../reducers";

import DateFnsUtils from '@date-io/date-fns';
import {
	MuiPickersUtilsProvider,
	
} from '@material-ui/pickers';
import { PTTSfunctions } from "../basicComponents";

export function DS21CardTable(props: any) { // 25 NOVs per DS21
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("props for DS21NOVsTable");
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props);

	const [DS1984rcpt, setDS198xrcpt] = useState(!props
		? ""
		: !props.DS198xrcpt
			? ""
			: props.DS198xrcpt);
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS198xrcpt ", DS1984rcpt);

	let history = useHistory();
	const dispatch = useDispatch();
	const classes = useStyles();
	const mountedRef = useRef(true);

	const contentparent: any = useSelector((state: any) => state.DS21CardReducer);
	const currDS21CardNOVs: tOneNOV[] =
		!contentparent
			? dummies.dummyNOV
			: !contentparent.ST_25NOVS
				? dummies.dummyNOV
				: contentparent.ST_25NOVS;

	const currUser: string = useSelector((state: any) => state.giantReducer.ST_USERDETAILS.userName);

	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS21 Card Table state content is:");
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(currDS21CardNOVs);
	const [Loading, setLoading] = useState(
		!currDS21CardNOVs
			? true
			: !currDS21CardNOVs.length
				? true
				: (
					currDS21CardNOVs.length < 2
					|| (
						!currDS21CardNOVs ? true
							: !currDS21CardNOVs[0]
								? true
								: currDS21CardNOVs[0].DS1984Number.length < 1
					)
				)
	);

	const [NotarizedOrVoid, setNotarizedOrVoid] = useState(-1);

	function handleGetDetails(ECBsummonsNumber: string) {
		// alert("history.push('/PTTS/NOVdetails/' + ECBsummonsNumber.toString())");
		// aSingleNOVnumber
		history.push('/PTTS/NOVdetails/' + ECBsummonsNumber);
	}

	function handleGetDS1704(ECBsummonsNumber: string) {
		alert("history.push('/PTTS/oneDS1704/' + ECBsummonsNumber.toString())");
	}

	function handleGetDS1984() {
		// alert("history.push('/PTTS/existingDS1984/" + DS1984rcpt + "'");
		history.push('/PTTS/existingDS1984/' + DS1984rcpt)
	}

	function handleNotorizedVoidNOV(e: any, pECBSummonsNumber: string) {
		currDS21CardNOVs.forEach(function (aRow: tOneNOV) {
			if (aRow.ECBSummonsNumber === pECBSummonsNumber) {
				try {
					switch (e.target.name) {
						case "chkVoided":
							aRow.Voided = !aRow.Voided;
							//aRow.ModifiedBy = currUser;
							//aRow.ModifiedOn = PTTSfunctions.Commonfunctions.formatDate(new Date());
							break;
						case "chkNotarized":
							aRow.Notarized = !aRow.Notarized;
							aRow.ModifiedBy = currUser;
							aRow.ModifiedOn = PTTSfunctions.Commonfunctions.formatDate(new Date());
							break;
						default:
							// code block
							if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
							if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.value);
					}
				}
				catch(error) {
					alert(error.toString());
				}
            }
        })
		allActions.DS21acts.NOVnotarizedVoided(currDS21CardNOVs, dispatch, pECBSummonsNumber, currUser);
    }

	React.useEffect(() => {
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("init useEffect: start");

		setLoading(true);

		if (DS1984rcpt.substring(0, 1) === "4") {
			allActions.DS21acts.GetNOVsForOneCard(dispatch, DS1984rcpt); // axios always returns an object with a "data" child
		}
		else {
			alert("unexpected props sent to DS21Card Tables component.");
		}


		return () => {
			mountedRef.current = false
		}

	}, [])

	React.useEffect(() => {
		if (!currDS21CardNOVs
			? false
			: !currDS21CardNOVs.length
				? false
				: currDS21CardNOVs.length < 2
					? false
					: true) {

			let cntNotarizedOrVoid: number = 0;
			currDS21CardNOVs.forEach(function (aRow) {
				if (aRow.Voided || aRow.Notarized) {
					cntNotarizedOrVoid = cntNotarizedOrVoid + 1;
				}
			})
			setNotarizedOrVoid(cntNotarizedOrVoid);
		}

	}, [currDS21CardNOVs])

	React.useEffect(() => {
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("NotarizedOrVoid useEffect");
		if (!currDS21CardNOVs
			? false
			: !currDS21CardNOVs.length
				? false
				: currDS21CardNOVs.length < 2
					? false
					: true) { // expect to have 25 NOVs in the REDUX STORE

			setLoading(false);
		}
	},
		[NotarizedOrVoid])



	if (NotarizedOrVoid < 0) {  //initialized state is negative one
		return (<div>Working on determining status for provided DS-1984 Receipt Number ...</div>);
	}
	else {	// DS21HDR is a table of DS21Cards; each card has a list of 25 NOVs 
			// 25 rows in Table DS21 makes one card
			// DS21card display has a grid including a link in each row to a single NOV
		return (  
			<Grid container>
				<Grid item xs={12}>
					<TableContainer component={Paper}>
						<Table className={classes.table} size="small" aria-label="a dense table">
							<TableHead>
								<TableRow><TableCell><b>DS1984 #:</b><Link href="#" onClick={() => handleGetDS1984()}>{DS1984rcpt}</Link></TableCell></TableRow>
								<TableRow>
									<TableCell size="small" align="right">Enter<br />Ticket<br />Details</TableCell>
									<TableCell size="small" align="right">Ticket<br />Voided</TableCell>
									<TableCell size="small" align="right">Ticket<br />Notarized</TableCell>
									<TableCell size="small" align="right">ECB<br />Summons<br />Number</TableCell>
									<TableCell size="small" align="right">Code</TableCell>
									<TableCell size="medium" align="left">Ticket Service Type</TableCell>
									<TableCell size="small" align="left">Date<br />Served<br />Month</TableCell>
									<TableCell size="small" align="left">Date<br />Served<br />Day</TableCell>
									<TableCell size="small" align="left">Dist.<br />of<br />Occurrence</TableCell>
									<TableCell size="small" align="left">Initial</TableCell>
									<TableCell size="small" align="left">Title</TableCell>
									<TableCell size="small" align="left">Ticket Status</TableCell>
									<TableCell size="small" align="left">View<br />DS1704</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{
									// @ts-ignore: Object is possibly 'null'.
									Loading // || !dsNOVlistForOneCard || dsNOVlistForOneCard === undefined
										|| !currDS21CardNOVs[0].ECBSummonsNumber
										? <TableRow><TableCell size="small">working ...</TableCell></TableRow>
										: currDS21CardNOVs.map((n: tOneNOV) => {
											//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("n is:");
											//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(n);
											if (!n.ECBSummonsNumber) {
												return (<TableRow key={"-1"}><TableCell size="small" key={"0"}>Working ...</TableCell></TableRow>)
											}
											else {
												return (
													<TableRow
														key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "row"}
														hover
													>
														<TableCell size="small" key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "URLNOV"} align="right" onClick={() => handleGetDetails(n.ECBSummonsNumber)}><Link href="#">Details</Link></TableCell>
														<TableCell size="small" key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "VOID"} align="right">
															<Checkbox name="chkVoided" onClick={(e) => handleNotorizedVoidNOV(e, n.ECBSummonsNumber)} checked={!n.Voided ? false : n.Voided} />
														</TableCell>
														<TableCell size="small" key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "NOTAR"} align="right">
															<Checkbox name="chkNotarized" onClick={(e) => handleNotorizedVoidNOV(e, n.ECBSummonsNumber)} checked={!n.Notarized ? false : n.Notarized} />
														</TableCell>
														<TableCell size="small" key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "NOVNUMB"} align="right">{n.ECBSummonsNumber}</TableCell>
														<TableCell size="small" key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "VCODE"} align="right">{n.Code}</TableCell>
														<TableCell size="medium" key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "SRVTYP"} align="left">{n.ViolationType}</TableCell>
														<TableCell size="small" key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "MM"} align="left">{n.DateServedMonth}</TableCell>
														<TableCell size="small" key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "DD"} align="left">{n.DateServedDay}</TableCell>
														<TableCell size="small" key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "POC"} align="left">{n.DistOfOccurrence}</TableCell>
														<TableCell size="small" key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "INIT"} align="left">{n.Initial}</TableCell>
														<TableCell size="small" key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "TLE"} align="left">{n.Title}</TableCell>
														<TableCell size="small" key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "STAT"} align="left">{n.DS21_TicketStatus.Description}</TableCell>
														<TableCell key={("000000000" + n.ECBSummonsNumber.toString()).substr(-8) + "URLDS1704"} align="left" onClick={() => handleGetDS1704(n.ECBSummonsNumber)}><Link href="#">{!n.IsTicketVoid ? "" : "DS1704"}</Link></TableCell>
													</TableRow>
												);
											}
										}
										)}
							</TableBody>
						</Table>
					</TableContainer>
				</Grid>

				<Grid item xs={12}>
					<MuiPickersUtilsProvider utils={DateFnsUtils}>
					< DS21CardBottom onercpt={DS1984rcpt} NotarizedOrVoid={NotarizedOrVoid} />
					</MuiPickersUtilsProvider>
				</Grid>
			</Grid>
			);
    }

}

const useStyles = makeStyles({
	paper: {
		width: "70%",
		display: "block",
	},
	table: {
		width: "100%",
		marginLeft: 0,
	},
	cell: {
		minWidth: 100,
		maxWidth: 220,
	}
});
