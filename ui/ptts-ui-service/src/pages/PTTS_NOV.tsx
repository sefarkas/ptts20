﻿import * as React from 'react';
import { useEffect, Fragment, useState } from 'react';
import { EditableNOVdetail } from "../components/SingleNOV";


export function PageForNOVDetails(props: any) {
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(" PageForNOVDetails props:");
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props);

    const [NOVnumber, setNOVnumber] = useState(props.match.params.aSingleNOVnumber); // match.params because of using ROUTER to get here

    return (
        <Fragment>
            <EditableNOVdetail oneNOVnumber={NOVnumber}/>
        </Fragment>
    );
}

