﻿export const TYPES = {
    ACT_LOGINAD: 'ActiveDirectoryLogin',
    ACT_EXPLICITLOGOUT: 'ExplicitLogout',
    ACT_EXPLICITLOGOUTRESET: 'LogoutReset',
    ACT_ADGROUPS: 'userMemberOfADgroupList',
    ACT_USERDETAILS: 'userDetailsFromAD',
    ACT_ERRMSG: 'placeToStoreMsg',
    ACT_ONELOGINOBJ: 'AuthADGroupUserDetails',
    ST_LOGINAD: 'StateOfActiveDirectoryLogin',
    ACT_AUTHINPROGRESS: 'APIinProgressWhenTrue',
    WEBDEV: 'webDev' // initial userName in the redux store
}
