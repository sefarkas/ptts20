﻿
namespace apiEF.Models
{
    public class clsUsedTktRanges
    {
         public partial class mdlUsedTktRanges
        {

            public string ECBSummonsNumberFrom
            {
                get; set;
            }

            public string ECBSummonsNumberTo
            {
                get; set;
            }

            public string DSform
            {
                get; set;
            }
            public string ReceiptNumber
            {
                get; set;
            }
        }
    }
}