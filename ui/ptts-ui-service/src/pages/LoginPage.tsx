import React, { useEffect, useRef } from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { makeStyles } from "@material-ui/styles";
import { useSelector, useDispatch } from 'react-redux';
import { allActions } from '../actions';
import { history } from "../configureStore";
import { ErrorMessage } from "@hookform/error-message";
import { PTTSfunctions } from '../components/basicComponents';
import {ErrorMessageContainer } from '../components/basicComponents/validation';
import { tGiantState } from "../reducers";

// const SignupSchema = yup.object().shape({
//    userName: yup.string().required(),
//    password: yup
//      .string()
//      .required(),
//    // age: yup
//    // .number()
//    // .required()
//    // .positive()
//    // .integer(),
//  });



export const LoginPage = () => { //  (propsFromApp: { dispatchRef: any, content: tGiantState }) =>  {

	const classes = useStyles();

    const dispatch = useDispatch();
    const LOGIN = useSelector((state: tGiantState) => state.ST_LOGINAD);
    const stateFromSelector: tGiantState = useSelector((state: any) => state.giantReducer);

    const { register, handleSubmit, errors } = useForm<FormData>();

    const [didSubmit, setdidSubmit] = React.useState(false);
    const [isDoneAuthenticating, setisDoneAuthenticating] = React.useState(true); // default is no API call in-progress
    const [count, setcount] = React.useState(0);
    const [AuthMsg, setAuthMsg] = React.useState("Enter DSNYAD Credentials.");

    const [UIDState, setUIDState] = React.useState("");
    let curuid: string | null = "";
    const [PWDState, setPWDState] = React.useState("");

    useEffect(() => {
        const LclUid: string = localStorage.getItem("userid") + "";
        const isLclUid: boolean = LclUid.length > 0 && LclUid !== "null";


        if (
            UIDState.length > 0
            &&
            (
                localStorage.getItem('userauth') === "false"
            )
            && !stateFromSelector.ST_AUTHINPROGRESS
            && AuthMsg !== `${UIDState}/pwd is blocked; try again.`)
        {
            setAuthMsg(`${UIDState}/pwd is blocked; try again.`);
            return;
        }

        if (isLclUid && !stateFromSelector) {
            // if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("clear local storage");
            // prepare local storage for new values
            // allow for multiple / different UID attempts
            localStorage.removeItem("userauth");
            localStorage.removeItem("userid");
        }
        //else if (
        //    localStorage.getItem("userauth") === "false"
        //    && !stateFromSelector.ST_AUTHINPROGRESS
        //) {
        //    setAuthMsg(`humph; likely wrong password for ${UIDState}. Try again.`);
        //}
        else if
            (
            (!stateFromSelector ? false : stateFromSelector.ST_LOGINAD)
            && !stateFromSelector.ST_AUTHINPROGRESS
        ) {
            if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("try to go to home page");
            try { history.push('/PTTS/home', stateFromSelector); }
            catch (error) { if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("history push exception ", error.toString()); }
        }
        else if (
            (!stateFromSelector ? false : stateFromSelector.ST_FAILEDLOGINATTEMPTS > 0)
            && (!stateFromSelector ? false : !stateFromSelector.ST_LOGINAD)
            && localStorage.getItem('userauth') !== "true" && !stateFromSelector.ST_AUTHINPROGRESS
        ) {
            if ("Unrecognized credentials; try again." !== AuthMsg
                && AuthMsg !== "Enter DSNYAD Credentials."
                && AuthMsg !== "type in user ID and password"
            ) { setAuthMsg("Unrecognized credentials; try again."); }
            setdidSubmit(false);
        }
        else if (
            (
                !stateFromSelector ? false : stateFromSelector.ST_FAILEDLOGINATTEMPTS > 0
                    && !stateFromSelector ? false : stateFromSelector.ST_LOGINAD
                    && !stateFromSelector.ST_USERDETAILS
            )
            ||
            (
                !stateFromSelector
                    ? false
                    : !stateFromSelector.ST_USERDETAILS

                    && localStorage.getItem("userauth") === "true"
                    && !stateFromSelector.ST_AUTHINPROGRESS
            )
        ) {
            if ("Unrecognized credentials; try again." !== AuthMsg
                && AuthMsg !== "Enter DSNYAD Credentials."
                && AuthMsg !== "type in user ID and password"
                && AuthMsg !== `${UIDState} has credentials, but no employee reference number.`
            ) { setAuthMsg(`${UIDState} has credentials, but no employee reference number.`); }
            setdidSubmit(false);
        }
        else if (
            (!stateFromSelector ? false : stateFromSelector.ST_AUTHINPROGRESS)

        ) {
            if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("so far, unauthenticated; working ...");

            if ("so far, unauthenticated; working ..." !== AuthMsg
                && AuthMsg !== "Enter DSNYAD Credentials."
                && AuthMsg !== "type in user ID and password"
            ) {
                setAuthMsg("so far, unauthenticated; working ...");
            }
        }
        else if (!stateFromSelector.ST_AUTHINPROGRESS
            && !stateFromSelector.ST_USERDETAILS
            && stateFromSelector.ST_LOGINAD
                && AuthMsg !== `${UIDState} has credentials, but no employee reference number.`
                && count > 0
                )
                { setAuthMsg(`${UIDState} has credentials, but no employee reference number.`);
        }
        else {
            // needed for login after a logout versus first time
            if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("type in user ID and password");
            if ("type in user ID and password" !== AuthMsg
                && AuthMsg !== "Enter DSNYAD Credentials."
                && AuthMsg !== `${UIDState} has credentials, but no employee reference number.`
                && count === 0
            )
            { setAuthMsg("type in user ID and password"); }
        }
        stateFromSelector.ST_AUTHINPROGRESS ? setcount(count + 1) : setcount(0);

    }, [stateFromSelector.ST_AUTHINPROGRESS])

    const handleOnChange = (e: any) => {
        e.preventDefault();

        switch (e.target.name) {
            case "fldUID":
                if (!didSubmit) { setdidSubmit(false); } // order matters
                if (AuthMsg !== "Enter DSNYAD Credentials.") { setAuthMsg("Enter DSNYAD Credentials."); }
                if (PWDState !== "") { setPWDState(""); }
                if (!!localStorage.getItem("userauth")) { localStorage.removeItem("userauth"); }
                setUIDState(e.target.value);
                break;
            case "fldPWD":
                if (!didSubmit) { setdidSubmit(false); } // order matters
                if (AuthMsg !== "Enter DSNYAD Credentials.") { setAuthMsg("Enter DSNYAD Credentials."); }
                setPWDState(e.target.value);
                break;

            default:
                // code block
                 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
                 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.value);
        }
    }

    const onSubmit = (data: any) => {
        localStorage.removeItem("userauth");
        if (didSubmit) {
            PTTSfunctions.Commonfunctions.RefreshPage();
        }
        else {
            setdidSubmit(true);
            setisDoneAuthenticating(false); // needed for re-login after successful logout
        }
        curuid = UIDState;
        setAuthMsg(`checking ${UIDState} ...`);
    };

    useEffect(() => {
        if (didSubmit && stateFromSelector.ST_AUTHINPROGRESS ) {
            //if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("after didSubmit Login Page: selector/passed in");
            //if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(stateFromSelector);
            alert("Stop clicking the PTTS 'Log In' button.  PTTS is busy.");
            return;
        }
        if (didSubmit && UIDState.length > 3 && PWDState.length > 7) {
            (async () => {
                // result of following is true when attempt to authenticate etc. UID is finished
                // isDone=true doesn't mean UID has passed AD.
                // isDone=false also means API ran into an exception
                if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("exec actionToapiPostFetchAuthenAuthorDetails");
                await dispatch(allActions.LoginADacts.APIinProgressWhenTrue(true))
                setAuthMsg("PTTS executing commands in other systems; those ought to respond in less than a minute, otherwise try again.");
                const b: boolean = await allActions.LoginADacts.actionToapiPostFetchAuthenAuthorDetails(UIDState, PWDState, dispatch);
                if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("actionToapiPostFetchAuthenAuthorDetails return was ", b);
                if (!mountedRef.current) return null
            })().then(() => {
                if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("then after async await ");
                if (
                    !stateFromSelector
                        ? false
                        : !stateFromSelector.ST_USERDETAILS

                        && localStorage.getItem("userauth") === "true"
                        && !stateFromSelector.ST_AUTHINPROGRESS
                ) {
                    setAuthMsg(`${UIDState} has credentials, but no employee reference number.`);
                }

                setdidSubmit(false);
                setPWDState("");
                return;
            });
        }
        else if (didSubmit && PWDState.length < 8) {
            setAuthMsg(`${UIDState} pw is too short.`);
            setdidSubmit(false);
        }
        else if (didSubmit && UIDState.length < 3) {
            setAuthMsg(`${UIDState} uid is too short.`);
            setdidSubmit(false);
        }
        ;
    }, [didSubmit])

    useEffect(() => {
        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("login isDone or AuthMsg");
        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("isDone", isDoneAuthenticating);
        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("AuthMsg", AuthMsg);
        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("count", count);
        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(stateFromSelector);

        if (!stateFromSelector.ST_AUTHINPROGRESS) {
            let sAuthMsg = AuthMsg;
            curuid = localStorage.getItem("userid");
            if (curuid && curuid.length > 0 && count > 0 && stateFromSelector.ST_LOGINAD
                && AuthMsg !== `${curuid} is valid.`) {
                sAuthMsg = `${curuid} is valid.`;
            }

            let howManyADgroups: number = 5;
            const bAnyADgroups: boolean = !(!stateFromSelector ? true : !stateFromSelector.ST_ADGROUPS)
            if (bAnyADgroups) {
                howManyADgroups = !stateFromSelector ? 0 : stateFromSelector.ST_ADGROUPS.length;
            }

            let lclUserAuth: string = ""
            if (!localStorage.getItem("userauth")) {
                lclUserAuth = "false"
            }
            else {
                lclUserAuth = "true"
            }

            if (lclUserAuth === "true"
                && UIDState.length > 0
                && !stateFromSelector.ST_USERDETAILS
                && !stateFromSelector.ST_AUTHINPROGRESS
            ) {
                sAuthMsg = `${UIDState} has credentials, but no employee reference number.`;
                alert(`PTTS expects a user ID for a person; not a system UID (${UIDState})`);
            }
            else if (
                localStorage.getItem("userauth") !== "true"
                && (!UIDState ? false : !UIDState.length ? false : UIDState.length > 3)
                && !stateFromSelector.ST_AUTHINPROGRESS
                && AuthMsg !== `humph; likely wrong password for ${UIDState}. Locked account? Try again.`
                && AuthMsg !== "Unexpected PTTS behavior. Try again at least once."
            ) {
                if (localStorage.getItem("userauth") !== "false") {
                    sAuthMsg = "Unexpected PTTS behavior (" + localStorage.getItem("userauth") +"). API for network credentials may be too slow. Try again at least once.";
                }
                else {
                    sAuthMsg = `humph; likely wrong password for ${UIDState}. Locked account? Try again.`;
                }
        }
        else if (
                (
                    !didSubmit
                    || UIDState.length === 0
                    || PWDState.length === 0
                )
            && lclUserAuth === "false"
            && AuthMsg !== "Enter DSNYAD Credentials."
            && count === 0 //  AuthMsg !== "so far, unauthenticated; working ..."
            && AuthMsg !== `${UIDState} has credentials, but no employee reference number.`) {
                sAuthMsg = "Enter DSNYAD Credentials."; // return to default value after logout
            }

            else if (
                (!stateFromSelector ? false : !stateFromSelector.ST_USERDETAILS)
                && UIDState.length > 0
                && didSubmit
                && AuthMsg !== `working on ${UIDState}/pwd.`
            ) {
                sAuthMsg = `working on ${UIDState}/pwd.`;
            }
            else if (
                (!UIDState ? false : UIDState.length > 0)
                    && PWDState.length > 0
                    && howManyADgroups > 1 // at least one set up by default in REDUCER
                    && didSubmit
                    && !stateFromSelector
                    ? false
                    : !stateFromSelector.ST_USERDETAILS
                        ? false
                        : stateFromSelector.ST_USERDETAILS.userName === UIDState
            ) {
                if (AuthMsg !== `${UIDState} is OKay.`) {
                    sAuthMsg = `${UIDState} is OKay.`;
                }
            }
            else if (
                (!UIDState ? false : UIDState.length > 0)
                    && stateFromSelector.ST_AUTHINPROGRESS
                    && AuthMsg !== `gathering details for ${UIDState}.`
            ) {
                sAuthMsg = `gathering details for ${UIDState}.`;
            }
            else {
                console.log(`still working on ${UIDState}. ${count}`);
            }

            setAuthMsg(sAuthMsg);
        }

    }, [stateFromSelector.ST_AUTHINPROGRESS])

    useEffect(() => {
        if (
            UIDState.length > 0
            && didSubmit
            &&
            (
                localStorage.getItem('userauth') === "false" 
                &&
                count === 0 // occurs after REDUX for LoginAD
            )
            && AuthMsg !== `${UIDState}/pwd was blocked by AD; try again.`) {
            setAuthMsg(`${UIDState}/pwd was blocked by AD; try again.`);
        }
    }, [count])

    const mountedRef = useRef(true);
    useEffect(() => {

        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useEffect [] of Login Page selector/passed in");
        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(stateFromSelector);
        dispatch(allActions.LoginADacts.LogoutReset);

        if (
            localStorage.getItem('userauth') === "true"
            && stateFromSelector.ST_DELIBERATELOGOUT === false
            && stateFromSelector.ST_LOGINAD === true

        ) {
            mountedRef.current = false

            history.push('/PTTS/home')
        }
        else {
            setPWDState("");
        };


        return () => {
            mountedRef.current = false
        }
    }, [])

    return (
        <div className={classes.login}>
            <div className={classes.LoginTitle}>PTTS</div>
            <br />
            <div className={classes.pttsLogo}>
                <img src={PTTSfunctions.Logos.PTTSLogo.logoImg} alt="PTTS" />
            </div>
            <br />

            <form>
                <div className={classes.LoginCFC}>

                    <input type="text" className={classes.LoginCFCInput}
                        name="fldUID"
                        placeholder="Network User ID"
                        onChange={(e) => handleOnChange(e)}
                        ref={register({
                            required: { value: true, message: "PTTS expects a User ID." },
                        })} />
                    <div className={classes.error}><ErrorMessage
                        errors={errors}
                        name="fldUID"
                        as={<ErrorMessageContainer />}
                    /></div>
                    <br />
                    <input type="password" className={classes.LoginCFCInput}
                        name="fldPWD" placeholder="current-password"
                        autoComplete="on"
                        onChange={(e) => handleOnChange(e)}
                        ref={register({
                            required: { value: true, message: "PTTS expects a network password." },
                        })}
                    />
                    <div className={classes.error}><ErrorMessage
                        errors={errors}
                        name="fldPWD"
                        as={<ErrorMessageContainer />}
                    /></div>
                    <br />

                    <input className={classes.buttonSubmit} type="submit" value={!didSubmit ? "Log In" : "Reload this page." } onClick={handleSubmit(onSubmit)} />
                    <div className={classes.CSSforAuthMsg} >{AuthMsg}</div>
                </div>

            </form>
            <div className={classes.Version}>Version 20.0</div>
        </div>
    );


}

const marLeft: number = -360;

const useStyles = makeStyles({
    root: {
        height: "100%",
        paddingLeft: 15,
        paddingRight: 15,
    },

    formContainer: {
        height: "90%",
        display: "grid",
        flexDirection: "column",
        "grid-template-columns": "1fr 1fr",
        "grid-gap": 20,
        maxWidth: 800,
    },

    button: {
        marginTop: 20,
    },

    formstyle: {
        maxWidth: "100%",
        padding: 20,
    },

    input: {
        display: "block",
        width: "100%",
        borderRadius: 4,
        border: "1px solid",
        padding: "10px 15px",
        marginBottom: 10,
        fontSize: 14,
    },

    label: {
        lineHeight: 2,
        textAlign: "left",
        display: "block",
        marginBottom: 13,
        marginTop: 20,
        fontSize: 14,
        fontWeight: 500,
        color: "black",
    },


    dsnyLogo: {
        width: 200,
        height: "auto",
        display: "block",
        "font-family": "Helvetica Neue",
        margin: "0 auto",
        paddingTop: "18%",
    },

    pttsLogo: {
        display: "block",
        "font-family": "Helvetica Neue",
        paddingTop: "18%",
        objectFit: "contain",
    },


    login: {
        textAlign: "center",
        "background-color": "#eee",
        height: "100vh",
        "margin-top": "-50px",
    },

    LoginTitle: {
        fontSize: 16,
        "font-family": "Helvetica, Courier",
        fontWeight: "bold",
        color: "#333333",
        marginTop: 10,
        textAlign: "center",
        display: "block",
    },

    LoginCFC: {
        margin: "0 auto",
        width: 340,
        height: "auto",
        padding: 50,
        // background: "#fff",
        "box-shadow": "0 3px 8px 0 rgba(0,0,0,0.12), 0 3px 8px 0 rgba(0,0,0,0.24)",
    },

    LoginCFCInput: {
        width: "100%",
        height: 38,
        paddingTop: 0,
        display: "block",
        border: "1px solid #DFDFDF",
        borderRadius: 3,
        color: "black !important",
        fontSize: 14,
        "font-family": "Helvetica, Courier !important",
    },


    Version: {
        marginTop: 10,
        marginRight: "auto",
        marginLeft: "auto",
        width: 350,
        paddingLeft: 264,
        color: "#BBBBBB",
        fontSize: 14,
    },

    error: {
        color: "#bf1650",
    },

    buttonSubmit: {
        background: "#0e8c1b",
        color: "white",
        border: "none",
        marginTop: 20,
        padding: 20,
        fontSize: 16,
        fontWeight: 100,
        width: 200,
    },

    CSSforAuthMsg: {
        maxWidth: "45ch",
        marginTop: 5,
        padding: 20,
    }
});
