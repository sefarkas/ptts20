﻿export interface PayrollLocations {
    id: number;
    active: boolean;
    code: string;
    correlation: string;
    description: string;
    shortDescription: string;
    deleted: boolean;
}

export enum ReferenceActions {
    ADD_PAYROLL_LOCATIONS = "ADD_PAYROLL_LOCATIONS",
    FIND_PAYROLL_LOCATION = "FIND_PAYROLL_LOCATION",
};

interface ReferenceActionType<T, P> {
    type: T;
    payload: P;
}

export type ReferenceAction =
    | ReferenceActionType<typeof ReferenceActions.ADD_PAYROLL_LOCATIONS, any>
    | ReferenceActionType<typeof ReferenceActions.FIND_PAYROLL_LOCATION, any>
    ;