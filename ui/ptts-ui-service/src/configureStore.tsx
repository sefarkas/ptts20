import { createBrowserHistory } from 'history';
import * as localforage from 'localforage';
import { applyMiddleware, compose, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import { PersistConfig, persistReducer, persistStore } from 'redux-persist';
import thunk from 'redux-thunk';
import { rootReducer } from './reducers';
// import { INITIAL_STATE as INIT_DS21HDR } from './reducers/DS21HDR';

const persistConfig: PersistConfig<any> = {
	key: "root",
	version: 1,
	storage: localforage,
	blacklist: [],
};

const logger = (createLogger as any)();
const history = createBrowserHistory();

// const Env = process.env.REACT_APP_ENV_TYPE!; // exclamation point is necessary for reading this value
// console.log("process.env.NODE_ENV", process.env.NODE_ENV);
const dev = process.env.NODE_ENV === "development"; // Env === "1";  

let middleware = dev
	? composeWithDevTools(applyMiddleware(thunk, logger))
	: applyMiddleware(thunk);

//if (dev) {
//	middleware = composeWithDevTools(middleware);
//}

const persistedReducer = persistReducer(persistConfig, rootReducer(history));

export default () => {
	const store = createStore(persistedReducer, {}, middleware) as any;
	const persistor = persistStore(store);
	return { store, persistor };
};

export { history };
