﻿// prettier-ignore
import {
	Link, Paper, Table, TableBody, TableCell,
	TableContainer, TableHead, TableRow, Grid
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
// import { history } from "../../configureStore";
import { useHistory } from "react-router-dom";
import { NOVbasics } from ".";
import { allActions } from "../../actions";
import { allServices } from '../../services';
import { PTTSfunctions } from '../basicComponents';
import { tDS21HDR, DS1984 } from "../../model";

export function NOVsearchTable(props: any) { // expect a table of DS21s for a particular user
	// very similar to DS1984 list because there is one DS21 for each DS1984
	// DS21 form is a ledger of 25 NOVs.
	const NOVnumber: string = !props ? ""
		: props.props;

	let history = useHistory();

	const classes = useStyles();

	const inThisInstant: Date = new Date(); // PTTSfunctions.Commonfunctions.todaysdate);
	const inThisInstant_yyyyMMdd: string = PTTSfunctions.Commonfunctions.formatDateYYYYmmDD(inThisInstant);
	const dummyDS21HDR: tDS21HDR = {
		ID: 0,
		Year: 0,
		IssuingOfficerSignature: "",
		Command: "",
		IssuingOfficerName: "",
		TaxRegNumber: "",
		Agency: 0,
		DateCardStarted: inThisInstant,
		DateReturned: inThisInstant,
		AssignedDistrict: "",
		IssuingOfficerTitle: "",
		OfficerInChargeName: "",
		OfficerInChargeTitle: "",
		DateCardCompleted: inThisInstant,
		Remarks: "",
		DS1984Number: "",
		ModifiedOn: inThisInstant,
		ModifiedBy: "",
		FormStatusID: 0,
		IssuingOfficerRefNo: "",
		CreatedBy: "",
		CreatedOn: inThisInstant,
		BoxNumberedFrom: 0,
		BoxNumberedTo: 0,
	}

	const content = useSelector((state: any) => state);

	const [dsList, setdsList] = useState([dummyDS21HDR]);
	const [Loading, setLoading] = useState(false);
	const [dsListPayCodeFilter, setdsListPayCodeFilter] = useState([dummyDS21HDR]);

	React.useEffect(() => {
		(async () => {
			setLoading(true);
			let rdata: tDS21HDR[] = [];
			if (!NOVnumber) {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("look up one NOV", NOVnumber);
				const response2 = await allServices.PTTSgetServices.DS21HDRList(); // axios always returns an object with a "data" child
				rdata = response2.data;
				// DS21s assigned to current user
				// DS21s in his AD Group
			}
			else {
				alert("unexpected props sent to DS21HDR Tables component.");
			}

			if (!rdata) {
				setLoading(true);
			}
			else {
				const rdataDesc: tDS21HDR[] = rdata.sort(function (a, b) {
					var nameA = !a.ReceiptNumber ? "" : a.ReceiptNumber.toUpperCase(); // ignore upper and lowercase
					var nameB = !b.ReceiptNumber ? "" : b.ReceiptNumber.toUpperCase(); // ignore upper and lowercase
					if (nameA < nameB) {
						return 1;
					}
					if (nameA > nameB) {
						return -1;
					}
					// names must be equal
					return 0;
				})//.slice(0, 29); // DESC
				setdsList(rdataDesc);
				setLoading(false);
			}
		})();
	}, []);

	React.useEffect(() => {
		const dsListFiltered: tDS21HDR[] = PTTSfunctions.ADfunctions.DS21hdrIAWpayCodesExtractedFromADGroupMembership(dsList, content)

		setdsListPayCodeFilter(
			dsListFiltered
		);

	}, [dsList])

	function handleRelatedDS21s(rcpt: string) {
		history.push('/PTTS/DS21NOVLIST/' + rcpt)
	}

	const handleLinkToParent1984 = (e: any) => {
		history.push('/PTTS/existingDS1984/' + e.target.textContent)
	}

	return (
		<Grid container>
			<Grid item xs={12} component={Paper} className={classes.paper}>

				<Table className={classes.table} aria-label="a dense table" >
					<TableHead>
						<TableRow>
							<TableCell size="small" align="right">DS21</TableCell>
							<TableCell size="small" align="center">Ticket Range</TableCell>
							<TableCell size="small" align="right">Last modified on</TableCell>
							<TableCell size="small" align="left">Supervisor Name</TableCell>
							<TableCell size="small" align="left">Modified By</TableCell>
							<TableCell size="medium" align="left">DS1984</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{
							// @ts-ignore: Object is possibly 'null'.
							Loading || !dsListPayCodeFilter || dsListPayCodeFilter === undefined
								? <TableRow><TableCell size="small">working ...</TableCell></TableRow>
								: dsListPayCodeFilter.map((n: tDS21HDR) => {
									//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("n is:");
									//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(n);
									if (!n.DS1984Number) {
										return (<TableRow key={"-1"}><TableCell size="small" key={"0"}>Working ...</TableCell></TableRow>)
									}
									else {
										return (
											<TableRow
												key={("000000000" + n.DS1984Number.toString()).substr(-8) + "row"}
												hover
											>
												<TableCell size="small" key={n.DS1984ReceiptNo} align="right" onClick={() => handleRelatedDS21s(n.ReceiptNumber)}><Link href="#">DS21 Form</Link></TableCell>
												<TableCell size="small" key={("000000000" + n.DS1984Number.toString()).substr(-8) + "TKTRNG"} align="center">{`${n.BoxNumberedFrom} - ${n.BoxNumberedTo}`}</TableCell>
												<TableCell size="small" key={("000000000" + n.DS1984Number.toString()).substr(-8) + "MODFON"} align="right">{n.ModifiedOn}</TableCell>
												<TableCell size="small" key={("000000000" + n.DS1984Number.toString()).substr(-8) + "SUPERVNAME"} align="left">{n.OfficerInChargeName}</TableCell>
												<TableCell size="small" key={("000000000" + n.DS1984Number.toString()).substr(-8) + "MODBY"} align="left">{n.ModifiedBy}</TableCell>
												<TableCell size="medium" key={("000000000" + n.DS1984Number.toString()).substr(-8) + "LINK84"} align="left" onClick={handleLinkToParent1984}><Link href="#">{n.DS1984Number}</Link></TableCell>
											</TableRow>
										);
									}
								}
								)}
					</TableBody>
				</Table>
			</Grid>

		</Grid>
	);
}

const useStyles = makeStyles({
	paper: {
		width: "100%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 360,
		overflowX: "visible",
	},
	table: {
		width: "100%",
		margingTop: 80,
		minWidth: 1000,
		display: "inline-block",
	},
	cell: {
		minWidth: 100,
		maxWidth: 220,
	}
});
