import { allServices as REFgetServices }  from './reference';
import { allServices as EISgetServices }  from './EIS';
import { allServices as LADgetServices }  from './LoginAD';
import { allServices as PTTSgetServices } from './PTTSgetData';
import { allServices as NTIgetServices }  from './NTIgetData';
import { allServices as PTTSiudServices } from './PTTSiud';

export const allServices = {
    EISgetServices,
    LADgetServices,
    NTIgetServices,
    PTTSgetServices,
    PTTSiudServices,
    REFgetServices,
}