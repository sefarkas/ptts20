﻿import { Action } from 'redux';
import { KnownActionDS1704 } from '../actions';
import { aDS1704rows, DS1704Action, DS1704Actions, tDS1704 } from "../model";
import createReducer from "./createReducer";

type KnownAction = KnownActionDS1704;


export const DS1704List = createReducer<tDS1704[]>([], {
	[DS1704Actions.ADD_DS1704](state: tDS1704[], action: DS1704Action) {
		return [...state, action.payload];
	},
	[DS1704Actions.COMPLETE_DS1704](state: tDS1704[], action: DS1704Action) {
		// search after DS1704 item with the given id and set completed to true
		return state.map(t =>
			t.iD === action.payload ? { ...t, completed: true } : t
		);
	},
	[DS1704Actions.REVERT2INCOMPLETE_DS1704](state: tDS1704[], action: DS1704Action) {
		// search after DS1704 item with the given id and set completed to false
		return state.map(t =>
			t.iD === action.payload ? { ...t, completed: false } : t
		);
	},
	[DS1704Actions.DELETE_DS1704](state: tDS1704[], action: DS1704Action) {
		// remove all DS1704 with the given id
		return state.filter(t => t.iD !== action.payload);
	},
});


export const DS1704Reducer = (state: tInterForInitialState | undefined = INITIAL_STATE, incomingAction: Action) => {
    const action = incomingAction as KnownAction;
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS21HDRReducer hit. Looking for " + action.type);

    let whichID: number = -1;

    switch (action.type) {
        case DS1704Actions.ADD_DS1704:
            return {
                ...state, ST_DS1704rows: action.payload
            }
        case DS1704Actions.UPDATE_DS1704:

            let i: number = -1;
            for (i = 0; i < state.ST_DS1704rows.length; i++) {
                if (state.ST_DS1704rows[i].ECBSummonsNumber === action.payload.ECBSummonsNumber) {
                    console.log("found it in the filtered HDR rows", i);
                    whichID = i;
                }
            }
            if (whichID > -1) state.ST_DS1704rows[whichID] =
            { // https://medium.com/hackernoon/redux-patterns-add-edit-remove-objects-in-an-array-6ee70cab2456
                ...state.ST_DS1704rows[whichID],
                ...action.payload
            }
            // state.ST_DS21HDRrowsFLTRD.splice(whichID, 1); // second parameter of splice is the number of elements to remove
            return {
                ...state // , ST_DS21HDRrowsFLTRD: action.payload
            }
        default:
            if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("landed in default of the DS1704 reducer");
            return { ...state };

    }
};



const todaysDate: Date = new Date();
const inThisInstant: any = new Date().toLocaleDateString();
const julianZero: Date = new Date(0); // PTTSfunctions.Commonfunctions.todaysdate);

export const dummyDS1704: tDS1704 = {
	ECBSummonsNumber: "",
	TimeServed: inThisInstant,
	PlaceOfOccurence: "",
	Remarks: "",
	ModifiedBy: "",
	ModifiedOn: todaysDate,
	VoidDate: todaysDate,
	VoidReasonId: 0,
	CreatedBy: "",
	CreatedDate: todaysDate,
	IsActive: false,
	ApprovedBy: "",
	District: "",
}

export const dummyDS1704row: aDS1704rows = {
    id: 0, // sequential 1 to 20
    POC: "", // place of occurrence -- district issuing ticket
    NOVnumber: "",
    NOVdate: "", // YYYY-MM-DD
    Address: "", // where ticket was issued to respondent
    ReasonForVoid: "", // selection made from the dropdown list
    VerifiedBy: "",
}

export type tInterForInitialState = { // all caps is signal to later developers to not change this value
	ST_DS1704rows: tDS1704[];
	ST_DS1704msg: string;
	ST_CARDCOMPLETE: boolean;
};

export const INITIAL_STATE: tInterForInitialState = { // all caps is signal to later developers to not change this value
    ST_DS1704rows: [dummyDS1704],
    ST_DS1704msg: '',
    ST_CARDCOMPLETE: false,
};

