﻿import { tSpecialDistrict } from "./PTTS_referenceData"

export enum BulkUpdateNOV_ACTION {
    ADD = 1,
    DEL = 0,
    UPT = 2,
    RST = 3,
}

export type tBulkUpdateRow = {
    [x: string]: any;
    Id: number; // sequential 1 to 20
    NOVnumber: string; // NOV# from any DS21
    Message: string; // result of the update action
    NOVnumberIsValid: boolean;
    TicketStatusID: number;
}

export type tViolationCode = {
    Id: number;
    ViolationCode1: string ;
    ReportMappingId?: number ;
}

export type tTicketServiceType = {
    Id: number;
    ViolationType1: string;
    SortOrder?: number;
}

export type tOneNOV = { // 25 NOVs per DS21
    [x: string]: any;
    ECBSummonsNumber: string;
    Code: string;
    DateServedMonth: number;
    DateServedDay: number;
    DistOfOccurrence: string;
    Initial: string;
    Title: string;
    DS1984Number: string;
    Notarized: boolean;
    Voided: boolean;
    ModifiedOn: string;
    ModifiedBy: string;
    TicketStatusID: number;
    ViolationType: string;
    CreatedBy: string;
    CreatedOn: string;
    DateServedYear: number;
    DS21_TicketStatus: tDS21_TicketStatus;
}

export type tEfDS21NOV = {
    ECBSummonsNumber: string;
    Code: string;
    DateServedMonth: number;
    DateServedDay: number;
    DistOfOccurrence: string;
    Initial: string;
    Title: string;
    DS1984Number: string;
    Notarized: boolean;
    Voided: boolean;
    ModifiedOn: Date;
    ModifiedBy: string;
    TicketStatusID: number;
    ViolationType: string;
    DateServedYear: number;
    CreatedBy: string;
    CreatedOn: string;
}

export type tDS21Card ={ // 25 NOVs per DS21 -- intended for DS21 Card Display Page (DS21Card/Table.tsx)
    [x: string]: any;
    ID: number,
    IssuingOfficerSignature: string;
    Command: string;
    IssuingOfficerName: string;
    TaxRegNumber: string;
    Agency: number;
    DateCardStarted: Date;
    DateReturned: Date;
    AssignedDistrict: string;
    IssuingOfficerTitle: string;
    OfficerInChargeName: string;
    OfficerInChargeTitle: string;
    DateCardCompleted: Date;
    Remarks: string;
    DS1984Number: string;
    DateCompleted_MM: string; // left paded with zeroes
    DateCompleted_DD: string; // left paded with zeroes
    ModifiedOn: Date;
    ModifiedBy: string;
    CreatedBy: string;
    CreatedOn: Date;
    FormStatusID: number;
    //DateCardStartedYYYYMMDD: string;
    //DateReturnedYYYYMMDD: string;
    //DateCardCompletedYYYYMMDD: string;
    //ModifiedOnYYYYMMDD: string;
    //CreatedOnYYYYMMDD: string;
}

export type tDS21_TicketStatus ={
    [x: string]: any;
    ID: number;
    Description: string;
    OrderByThisTinyInt: number;
}

export type tEfNOViud ={
    ECBSummonsNumber: string;
    Code: string;
    DateServedMonth: number;
    DateServedDay: number;
    DistOfOccurrence: string;
    Initial: string;
    Title: string;
    DS1984Number: string;
    Notarized: boolean;
    Voided: boolean;
    ModifiedOn: Date;
    ModifiedBy: string;
    TicketStatusID: number;
    ViolationType: string;
    DateServedYear: number;
    CreatedBy: string;
    CreatedOn: string;
    DS21_TicketStatus: tDS21_TicketStatus;
}

export type tDS21HDR ={ // one DS21 per DS1984
    [x: string]: any;
    ID: number,
    Year: number;
    IssuingOfficerSignature: string;
    Command: string;
    IssuingOfficerName: string;
    TaxRegNumber: string;
    Agency: number;
    DateCardStarted: Date;
    DateReturned: Date;
    AssignedDistrict: string;
    IssuingOfficerTitle: string;
    OfficerInChargeName: string;
    OfficerInChargeTitle: string;
    DateCardCompleted: Date;
    Remarks: string;
    DS1984Number: string;
    ModifiedOn: Date;
    ModifiedBy: string;
    FormStatusID: number;
    IssuingOfficerRefNo: string;
    CreatedBy: string;
    CreatedOn: Date;
    BoxNumberedFrom: number;
    BoxNumberedTo: number;
}

export type tEfDS21HDR ={
    Year: number;
    IssuingOfficerSignature: string;
    Command: string;
    IssuingOfficerName: string;
    TaxRegNumber: string;
    Agency: string;
    DateCardStarted: Date;
    DateReturned: Date;
    AssignedDistrict: string;
    IssuingOfficerTitle: string;
    OfficerInChargeName: string;
    OfficerInChargeTitle: string;
    DateCardCompleted: Date;
    Remarks: string;
    DS1984Number: string;
    ModifiedOn: Date;
    ModifiedBy: string;
    FormStatusID: number;
    IssuingOfficerRefNo: string;
    CreatedBy: string;
    CreatedOn: Date;
    BoxNumberedFrom: number;
    BoxNumberedTo: number;
    DS21TicketStatus: tDS21_TicketStatus;
}

export type tEfDS21HDRiud ={
    Year: number,
    IssuingOfficerSignature: string,
    Command: string,
    IssuingOfficerName: string,
    TaxRegNumber: string,
    Agency: string,
    DateCardStarted: Date,
    DateReturned: Date,
    AssignedDistrict: string,
    IssuingOfficerTitle: string,
    OfficerInChargeName: string,
    OfficerInChargeTitle: string,
    DateCardCompleted: Date,
    Remarks: string,
    DS1984Number: string,
    FormStatusID: number,
    ModifiedBy: string,
}

export type tNOV ={
    ECBSummonsNumber: string;

}

export enum DS21HDRActions {
    aDS21HDRROWSadd = "DS21_HDR_ROWS",
    aDS21HDRROWSFLTRDadd = "DS21_HDR_ROWS_FILTERED",
    aDS21HDRROWSupt = "FIX_DS21_HDR_ROWS",
    aDS21HDRROWSFLTRDupt = "FIX_DS21_HDR_ROWS_FILTERED",
    HDRmsg = 'SYSTEM_MSG_TO_USER', // some sort of exception
    REVERT2INCOMPLETE_DS21 = "REVERT2INCOMPLETE_DS21",
}


export enum DS21CardActions {
    UPT_BulkUpdateThisNOV = "ChangeFirstNOV_BUL",
    ADD_BulkUpdateTheseNOVs = "ADD_oneNOV_BUL",
    DEL_BulkUpdateTheseNOVs = "REMOVE_oneNOV_BUL",
    RST_BulkUpdateTheseNOVs = "RESET_oneNOV_BUL",
    RPL_BulkUpdateTheseNOVs = "Replace_BulkUpdateTheseNOVs_array",
    UPDATE_DS21CARD = "UPDATE_DS21",
    UPDATE_NOV = "UPDATE_NOV",
    COMPLETE_DS21CARD = "COMPLETE_DS21",
    REVERT2INCOMPLETE_DS21 = "REVERT2INCOMPLETE_DS21",
    aDS21_CARDNOV_TABLE = "NOVs_For_One_DS21Card",
    aDS21_MENU_TST = "TICKET_SERVICE_TYPES",
    aDS21_MENU_POC = "DISTRICT_OF_OCCURENCE_OPTIONS",
    aDS21_MENU_VC = "VIOLATION_CODE_OPTIONS",
    CardMSG = 'SYSTEM_MSG_TO_USER', // some sort of exception
}

interface DS21ActionType<T, P> {
    type: T;
    payload: P;
}

export type DS21HDRAction = 
    DS21ActionType<typeof DS21HDRActions.aDS21HDRROWSadd, any | tDS21HDR[]>
    | DS21ActionType<typeof DS21HDRActions.aDS21HDRROWSFLTRDadd, any | tDS21HDR[]>
    | DS21ActionType<typeof DS21HDRActions.aDS21HDRROWSupt, any | tDS21HDR[]>
    | DS21ActionType<typeof DS21HDRActions.aDS21HDRROWSFLTRDupt, any | tDS21HDR[]>
    | DS21ActionType<typeof DS21HDRActions.HDRmsg, string>
    | DS21ActionType<typeof DS21HDRActions.REVERT2INCOMPLETE_DS21, string>
    ;


export type DS21CardAction =
    | DS21ActionType<typeof DS21CardActions.UPDATE_DS21CARD, {} | tDS21HDR>
    | DS21ActionType<typeof DS21CardActions.UPT_BulkUpdateThisNOV, tBulkUpdateRow>
    | DS21ActionType<typeof DS21CardActions.ADD_BulkUpdateTheseNOVs, tBulkUpdateRow>
    | DS21ActionType<typeof DS21CardActions.RPL_BulkUpdateTheseNOVs, tBulkUpdateRow[]>
    | DS21ActionType<typeof DS21CardActions.DEL_BulkUpdateTheseNOVs, tBulkUpdateRow>
    | DS21ActionType<typeof DS21CardActions.RST_BulkUpdateTheseNOVs, any>
    | DS21ActionType<typeof DS21CardActions.UPDATE_NOV, {} | tOneNOV>
    | DS21ActionType<typeof DS21CardActions.aDS21_CARDNOV_TABLE, any | tOneNOV[]>
    | DS21ActionType<typeof DS21CardActions.aDS21_MENU_POC, {} | tSpecialDistrict[]>
    | DS21ActionType<typeof DS21CardActions.aDS21_MENU_TST, {} | tTicketServiceType[]>
    | DS21ActionType<typeof DS21CardActions.aDS21_MENU_VC, {} | tViolationCode[]>
    | DS21ActionType<typeof DS21CardActions.CardMSG, string>
    | DS21ActionType<typeof DS21CardActions.REVERT2INCOMPLETE_DS21, string>
    ;

