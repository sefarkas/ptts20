﻿
import _ from 'lodash';
import { Action } from 'redux';
import { KnownActionAD as KnownAction } from '../actions/LoginAD';
import { TYPES as LoginADconstants } from '../constants/constantsLoginAD';
import { tUserDetails, tADgroup } from '../model';

export type InterForInitialState = { // all caps is signal to later developers to not change this value
    ST_AUTHINPROGRESS: false,
    ST_LOGINAD: boolean,
    ST_DELIBERATELOGOUT: boolean,
    ST_USERDETAILS: tUserDetails,
    ST_ADGROUPS: tADgroup[],
    ST_MSG: string,
    ST_FAILEDLOGINATTEMPTS: number
};

export const INITIAL_STATE: InterForInitialState = { // all caps is signal to later developers to not change this value
    ST_AUTHINPROGRESS: false,
    ST_LOGINAD: false,
    ST_DELIBERATELOGOUT: false,
    ST_USERDETAILS: {
        aDgroups: null,
        authenticated: false,
        authenticationMessage: "init",
        createDate: "0001-01-01T00:00:00",
        email: "webDev@dsny.nyc.gov",
        firstName: "Web",
        isActive: 0,
        lastLoginDate: "0001-01-01T00:00:00",
        lastName: "developer",
        password: null,
        roles: null,
        userId: 0,
        userName: LoginADconstants.WEBDEV,
        userRoles: null,
        voiceTelephoneNumber: "(212) 555-1212",
    },
    ST_ADGROUPS: [{
        userName: 'xyz',
        userMemberOfADgroupList: 'xyz'
    }],
    ST_MSG: '',
    ST_FAILEDLOGINATTEMPTS: 0
};

export type tLoginADState = {
    readonly LoginADReducer: InterForInitialState;
};

export const LoginADReducer = (state: InterForInitialState
    | undefined = INITIAL_STATE
    , incomingAction: Action) => {

    const action = incomingAction as KnownAction;
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("LoginADReducer hit. Looking for " + action.type);

    switch (action.type) {
        case LoginADconstants.ACT_AUTHINPROGRESS:
            return {
                ...state,
                ST_AUTHINPROGRESS: action.payload
            }
        case LoginADconstants.ACT_ONELOGINOBJ:

            return {
                ...state, ST_LOGINAD: action.payload[0] // expected either true, false, or undefined 
                , ST_ADGROUPS: action.payload[1]
                , ST_USERDETAILS: action.payload[2]
                , ST_NTIPROFILE: action.payload[3]
                , ST_DELIBERATELOGOUT: false // the fact your're trying to log in it no longer matters if you logged out
                , ST_FAILEDLOGINATTEMPTS: action.payload[0] ? 0 : state.ST_FAILEDLOGINATTEMPTS + 1
                , ST_AUTHINPROGRESS: false
            };
        case LoginADconstants.ACT_LOGINAD:
            return {
                ...state,
                ST_LOGINAD: action.payload,
                ST_DELIBERATELOGOUT: !action.payload // expected either true, false, or undefined // , ST_TABLE: 'force ONB to pick a new component -- 1'
                , ST_FAILEDLOGINATTEMPTS: action.payload ? 0 : state.ST_FAILEDLOGINATTEMPTS + 1
            };
        case LoginADconstants.ACT_EXPLICITLOGOUTRESET:
            state = INITIAL_STATE;
            return {
                state, 
                ST_DELIBERATELOGOUT: action.payload,
            };
        case LoginADconstants.ACT_EXPLICITLOGOUT:
            return {
                ...state, ST_LOGINAD: !action.payload,
                ST_DELIBERATELOGOUT: action.payload,
                ST_REHYDRATING: !action.payload,
                ST_FAILEDLOGINATTEMPTS: action.payload ? 0 : state.ST_FAILEDLOGINATTEMPTS,
                ST_ADGROUPS: null,
                ST_USERDETAILS: null,
            };

        case LoginADconstants.ACT_ADGROUPS:
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("put this payload in ST_ADGROUPS " + action.payload);
            return {

                ...state, ST_ADGROUPS: action.payload // , ST_TABLE: 'force ONB to pick a new component -- 2'
            };
        case LoginADconstants.ACT_USERDETAILS:
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("put this payload in ST_USERDETAILS ");
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(action.payload);
            return {
                ...state, ST_USERDETAILS: action.payload // , ST_TABLE: 'force ONB to pick a new component -- 3'
            };
        case LoginADconstants.ACT_ERRMSG:
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("put this payload in ST_ADGROUPS " + action.payload);
            return {

                ...state, ST_MSG: action.payload
            };
        default:
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("landed in default of the login AD reducer, action.type was ", action.type);
            return { ...state }
    }
};
