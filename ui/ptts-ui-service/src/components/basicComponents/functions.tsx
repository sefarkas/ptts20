﻿import { FormControlLabel, Radio } from "@material-ui/core";
import moment from "moment";
import React from "react";
import { isNullOrUndefined } from "util";
import { tDS21_TicketStatus, tOneNOV, UsedTicketRange } from "../../model";
import { allServices } from "../../services";

function RefreshPage(): void {
	window.location.reload(false);
}

function arrAllEqual(arr: any[]): boolean {
	return arr.every(v => v === arr[0])
}

function formatDate(date: Date): string {
	var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + ((d.getDate() + 1).toLocaleString()),
		year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [year, month, day].join('-');
};

function todaysdate(): string {
	const dateToday: Date = new Date();
	const rslt: string = formatDate(dateToday);
	return rslt;
};

const todaysUTCdate = () => {
	const dateToday: Date = new Date();
	const rslt: string = dateToday.toUTCString();
	return rslt;
};

const todaysDatePaded = () => {
	const tempDate = new Date();
	const currDate: string = tempDate.getFullYear() + '-' + padForDate((tempDate.getMonth() + 1)) + '-' + padForDate((tempDate.getDate()));
	return currDate;
}

const anyDatePaded = (tempDate: Date) => {
	const dt = new Date(tempDate);
	const currDate: string = dt.getFullYear() + '-' + padForDate((dt.getMonth() + 1)) + '-' + padForDate((dt.getDate()));
	return currDate;
}

function formatDateMMdYYYY(date: Date): string {
	try {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + ((d.getDate()).toLocaleString()),
			year = d.getFullYear();

		if (month.length < 2)
			month = '0' + month;
		if (day.length < 2)
			day = '0' + day;

		return [month, day, year].join('/').toString();
	}
	catch (error) {
		return date.toDateString();
	}

};


export function formatDateYYYYmmDD(date: Date): string {
	try {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + ((d.getDate()).toLocaleString()),
			year = d.getFullYear();

		if (month.length < 2)
			month = '0' + month;
		if (day.length < 2)
			day = '0' + day;

		return [year, month, day].join('').toString();
	}
	catch{
		return date.toDateString();
	}

};

function MomentToDate(YYYYmmDD: any): Date {
	try {
		// alert(YYYYmmDD);
		const str: string = moment(YYYYmmDD, "YYYY-MM-DD").toDate().toLocaleDateString();
		// alert(str);
		const dte: Date = new Date(str);
		// alert(dte);
		return dte;
	}
	catch (error) {
		alert("MomentToDate exception: " + error.toString() + YYYYmmDD.toString());
		return new Date(0);
	}

}

function padForDate(n: any)  {
	return ("00" + n).slice(-2);
}

type DateOrAny = Date | any;
function isDateType(anObj: DateOrAny): anObj is Date {
	if (!(anObj as Date).getDate) { return false }; // .getDate is a valid attribute of type Date
	return true;
}

function DateToYYYYmmDD(aDate: Date | string): string  {
	if (isDateType(aDate)) {
		console.log("aDate #1", aDate);
		console.log("DateToYYYYmmDD #1", (aDate.getFullYear() + '-' + padForDate((aDate.getMonth() + 1)) + '-' + padForDate((aDate.getDate()))).substr(0, 10));
		return (aDate.getFullYear() + '-' + padForDate((aDate.getMonth() + 1)) + '-' + padForDate((aDate.getDate()))).substr(0, 10);
	}

	else if (aDate.toString().includes("/")) {
		console.log("aDate.toString() #2", aDate.toString());
		console.log("DateToYYYYmmDD #2", aDate.toString().substr(-4) + "-" + padForDate(aDate.substr(0, 2).replace("/", "")) + "-" + padForDate(aDate.substr(3, 2).replace("/", "")));

		return aDate.toString().substr(-4) + "-" + padForDate(aDate.substr(0, 2).replace("/", "")) + "-" + padForDate(aDate.substr(3, 2).replace("/", ""));
	}
	else {
		const sDte: string = aDate.toString(); // YYYY-MM-DDT##:##:##
		console.log("sDte #3", sDte);
		console.log("DateToYYYYmmDD #3", sDte.substr(0, 4) + "-" + sDte.substr(5, 2) + "-" + sDte.substr(8, 2));
		return sDte.substr(0, 4) + "-" + sDte.substr(5, 2) + "-" + sDte.substr(8, 2);
    }
}

function pad(n: number, width: number): string {
	// pad left with zero; however NOV# and ECB Summons #
	// always start with 1 or 2, so this function is unlikely to
	// return other than n.toString().
	let strN = n.toString();
	strN = strN + '';
	return strN.length >= width ? strN :
		new Array(width - strN.length + 1).join('0') + strN;
}

function Append8WithECBcheckSum(hEightDigitNOVnum: number): string {

	if (isNaN(hEightDigitNOVnum)) { throw Error("NOV# sent to Append8WithECBcheckSum is NaN.") };

	if (hEightDigitNOVnum.toString().length > 9) { throw Error("NOV# sent to Append8WithECBcheckSum is too long/big.") };

	// if hEightDigitNOVnum comes in with nine digits assume it already has correct ECB checksum suffix
	if (hEightDigitNOVnum > 99999999) { return hEightDigitNOVnum.toString() };

	if (hEightDigitNOVnum.toString().length < 8) { throw Error("NOV# sent to Append8WithECBcheckSum is too short/small.") };

	let strResult: string = ""
	let hResult: number = 0;
	let hTestThis: number = 0;

	hTestThis = 12 - (hEightDigitNOVnum % 11) - 1
	if (hTestThis === 10) { hResult = hEightDigitNOVnum * 10 } // Then

	else if (hTestThis === 11) // Then
	{ hResult = hEightDigitNOVnum * 10 }

	else { hResult = hEightDigitNOVnum * 10 + hTestThis }

	// End If

	strResult = pad(hResult, 9) // 9 DIGITS

	return strResult;
};

function isNotDefined<T>(a: T | null | undefined): a is (null | undefined) {
	return a === null || a === undefined;
} // https://github.com/microsoft/TypeScript/issues/27041


function IsValidNOVnumber(aDSform: string, LowHigh: string, TgtVal: string): boolean {
	let rdata: UsedTicketRange[] | undefined = undefined;
	let pIsValidNOVnumber: boolean = false;

	const searchECBsummonsNumber: string =
		TgtVal.length < 9
			? Append8WithECBcheckSum(Number.parseInt(TgtVal))
			: TgtVal;
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Find a ticket range with NOV ", searchECBsummonsNumber);

	(async () => {
		const tryThisRange = (
			{
				ECBSummonsNumberFrom: LowHigh === "Low"
					? TgtVal
					: searchECBsummonsNumber,
				ECBSummonsNumberTo: LowHigh === "High"
					? TgtVal
					: searchECBsummonsNumber,
				DSform: aDSform,
				ReceiptNumber: "",
			}
		);
		pIsValidNOVnumber = false;
		const response = await allServices.PTTSgetServices.refUsedTicketNumbers(tryThisRange);
		// axios always returns an object with a "data" child
		rdata = await response.data;
		if (!rdata) {
			pIsValidNOVnumber = false;
		}
		else if (!rdata.length ? true : rdata.length === 0) {
			pIsValidNOVnumber = false;
		}
		else {
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("found a ticket range for ", searchECBsummonsNumber);
			pIsValidNOVnumber =
				Number.parseInt(tryThisRange.ECBSummonsNumberFrom) > 0
					&& Number.parseInt(tryThisRange.ECBSummonsNumberTo) > 0
					? true : false;

		}

	})();
	return pIsValidNOVnumber;

};


function formatDateSlashes(dateWithHypens: string): string{
	try {
		let date = dateWithHypens.split('-');
		const year = String(date[0]).split('-');
		const month = String(date[1]).split('-');
		const day = String(date[2]).split('-');
		return month + "/" + day + "/" + year;
	}
	catch {
		let jsDate: Date = new Date(dateWithHypens);
		return jsDate.toLocaleDateString();
    }

}


function parseArraytoReactSelect(jsonResponse: any) {
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("parseArraytoReactSelect response is:");
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(jsonResponse);

	const result: any = [];

	const listKeys = Object.keys(jsonResponse);
	listKeys.forEach(function (anItem) {
		const items = Object.keys(jsonResponse[anItem]);
		items.forEach(function (oneChoice) {
			const item = { id: '', value: "", label: "" }
			const itemId = Object.keys(jsonResponse[anItem][oneChoice])[0];
			const itemDesc = Object.keys(jsonResponse[anItem][oneChoice])[1];
			item.id = itemId.replace('TypeID', '');
			item.value = jsonResponse[anItem][oneChoice][itemId];
			item.label = jsonResponse[anItem][oneChoice][itemDesc];
			result.push(item);
		}
		)
	}
	)

	return result;
}


function parseArraytoReactSelect_IA(jsonResponse: any): any {
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("parseArraytoReactSelect response is:");
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(jsonResponse);

	const result: any = [];
	// tslint:disable-next-line:prefer-for-of
	// for (let i = 0; i < jsonResponse.length; i++)
	// {
	//     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("response[i]");
	//     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(jsonResponse[i]);

	//    const item: AnItem = { id: '', value: "", label: "" }
	//    item.id = jsonResponse[i].id;
	//    item.value = jsonResponse[i].name;
	//    item.label = jsonResponse[i].name
	//    result.push(item);
	// }

	const listKeys = Object.keys(jsonResponse.data);
	listKeys.forEach(function (anItem) {
		const item = { id: '', value: -1, DescrShort: "", DescrLong: "" }
		const itemId = Object.keys(jsonResponse.data[anItem])[0];
		const itemShort = Object.keys(jsonResponse.data[anItem])[2];
		const itemLong = Object.keys(jsonResponse.data[anItem])[3];
		item.id = itemId.replace('typeID', 'TypeID_IA');
		item.value = jsonResponse.data[anItem][itemId];
		item.DescrShort = jsonResponse.data[anItem][itemShort];
		item.DescrLong = jsonResponse.data[anItem][itemLong];
		result.push(item);
	}
	)

	return result;
}

function IsValidDate(m: number, d: number, y: number): boolean{
	try {
		const dte: Date = new Date(y, m, d, 0, 0, 0, 0);
		return true;
	}
	catch {
		return false;
	}
}


function sleep(ms: number) {
	return new Promise(resolve => setTimeout(resolve, ms));
}


function fTSOjsxElement(currentTSO: number,isLoadingTSO: boolean, isLoadingNOV: boolean,
	dsTSOs: tDS21_TicketStatus[], dsAttributesOfOneNOV: tOneNOV) {

	if (isLoadingTSO || isLoadingNOV) { return <div>still working ...</div> };

	return dsTSOs.map(oneChoice => {

		if (oneChoice.ID === 1) {
			oneChoice.Description = "District has Ticket Possession"
		}
		/*
			*
			* 		                    Ticket Status ID
														A	B	C	D	E	F	G
		
A	District Has Ticket Possession	                1	T	F	F	F	F	F	F
when 1 then disable/enable -->							d	e	d	d	d	d	d
B	Ticket Sent To Enforcement HQ	                6	F	T	F	F	F	F	F
when 6 then disable/enable -->							d	d	e	e	d	d	d
C	Logging Unit Received Ticket	                7	F	F	T	F	F	F	F
when 7 then disable/enable -->							d	d	d	e	e	e	e
D	Logging Unit Rejected Ticket Back to District	9	F	F	F	T	F	F	F
when 9 then disable/enable -->							e	d	d	d	d	d	d
E	Ownership Unit has Ticket Possession	        11	F	F	F	F	T	F	F
when 11 then disable/enable -->							d	d	d	e	d	e	e
F	Research Unit Has Ticket Possession	            14	F	F	F	F	F	T	F
when 14 then disable/enable -->							d	d	d	e	e	d	e
G	Mailroom Has Ticket Possession					10	F	F	F	F	F	F	T
when 10 then disable/enable -->							d	d	d	e	e	e	d
		
		d: disabled
		e: enabled
			*
			* */
		let lclEnabled: boolean = (
			currentTSO === oneChoice.ID
				? false
				: true
		);
		if (isNullOrUndefined(dsAttributesOfOneNOV.Voided) ? false : dsAttributesOfOneNOV.Voided) {
			lclEnabled = false
			switch (currentTSO) {
				case 0:
					try {
						if (6 === oneChoice.ID) {
							lclEnabled = true;
						}
						else {
							lclEnabled = false;
						}
					}
					catch (error) {
						alert(error.toString());
					}
					break;
				case 1:
					{
						if (6 === oneChoice.ID) {
							lclEnabled = true;
						}
						break;
					}
				case 6:
					{
						if (
							7 === oneChoice.ID
						) {
							lclEnabled = true;
						}
						else {
							lclEnabled = false;
						}
						break;
					}
				default:
					lclEnabled = false;
			}
		}
		else {
			switch (currentTSO) {
				case 0:
					try {
						if (6 === oneChoice.ID) {
							lclEnabled = true;
						}
						else {
							lclEnabled = false;
						}
					}
					catch (error) {
						alert(error.toString());
					}
					break;
				case 1:
					try {
						if (2 === oneChoice.ID
							|| 6 === oneChoice.ID) {
							lclEnabled = true;
						}
						else {
							lclEnabled = false;
						}
					}
					catch (error) {
						alert(error.toString());
					}
					break;
				case 2:
					try {
						if (6 === oneChoice.ID) {
							lclEnabled = true;
						}
						else {
							lclEnabled = false;
						}
					}
					catch (error) {
						alert(error.toString());
					}
					break;
				case 6:
					try {
						if (
							7 === oneChoice.ID
							|| 9 === oneChoice.ID
						) {
							lclEnabled = true;
						}
						else {
							lclEnabled = false;
						}
					}
					catch (error) {
						alert(error.toString());
					}
					break;
				case 7:
					try {
						if (
							9 === oneChoice.ID
							|| 11 === oneChoice.ID
							|| 14 === oneChoice.ID
							|| 10 === oneChoice.ID
						) {
							lclEnabled = true;
						}
						else {
							lclEnabled = false;
						}
					}
					catch (error) {
						alert(error.toString());
					}
					break;
				case 9:
					try {
						if (
							1 === oneChoice.ID
						) {
							lclEnabled = true;
						}
						else {
							lclEnabled = false;
						}
					}
					catch (error) {
						alert(error.toString());
					}
					break;
				case 10:
					try {
						if (
							9 === oneChoice.ID
							|| 11 === oneChoice.ID
							|| 14 === oneChoice.ID
						) {
							lclEnabled = true;
						}
						else {
							lclEnabled = false;
						}
					}
					catch (error) {
						alert(error.toString());
					}
					break;
				case 11:
					try {
						if (
							9 === oneChoice.ID
							|| 10 === oneChoice.ID
							|| 14 === oneChoice.ID
						) {
							lclEnabled = true;
						}
						else {
							lclEnabled = false;
						}
					}
					catch (error) {
						alert(error.toString());
					}
					break;
				case 12:
					try {
						if (
							1 === oneChoice.ID
							|| 6 === oneChoice.ID
						) {
							lclEnabled = true;
						}
						else {
							lclEnabled = false;
						}
					}
					catch (error) {
						alert(error.toString());
					}
					break;
				case 14:
					try {
						if (
							9 === oneChoice.ID
							|| 11 === oneChoice.ID
							|| 10 === oneChoice.ID
						) {
							lclEnabled = true;
						}
						else {
							lclEnabled = false;
						}
					}
					catch (error) {
						alert(error.toString());
					}
					break;
				default:
					lclEnabled = (lclEnabled)
			}


		}
		// console.log("oneChoice.ID: ", oneChoice.ID);
		// console.log("currentTSO: ", currentTSO);
		// console.log("lclEnabled: ", lclEnabled);

		if (isNullOrUndefined(dsAttributesOfOneNOV.Voided) ? false : dsAttributesOfOneNOV.Voided) {
			// console.log("return 5");
			return (
				
				<FormControlLabel
					disabled={!lclEnabled}
					key={oneChoice.ID}
					value={oneChoice.ID}
					control={<Radio />}
					label={oneChoice.Description}
				/>
					
			);
		}
		else if ((oneChoice.ID === 1) && (currentTSO === 12)) {
			// console.log("return 1");
			return (
				
				<FormControlLabel
					disabled={true}
					key={oneChoice.ID}
					value={currentTSO}
					control={<Radio />}
					label={"District has Ticket Possession"}
				/>
					
			);
		}
		else if ((oneChoice.ID === 1) && (currentTSO === 2 || currentTSO === 13 || currentTSO === 0)) {
			// console.log("return 2");
			return (
				
				<FormControlLabel
					disabled={true}
					key={oneChoice.ID}
					value={currentTSO}
					control={<Radio />}
					label={"District has Ticket Possession"}
					/>
					

			);
		}
		else if (oneChoice.ID === 13 || oneChoice.ID === 2 || oneChoice.ID === 12) {
			// console.log("return 3");
			return (<div key={oneChoice.ID}></div>)
		}
		else {
			// console.log("return 4");
			return (
				
					<FormControlLabel
					disabled={!lclEnabled}
					key={oneChoice.ID}
					value={oneChoice.ID}
					control={<Radio />}
					label={oneChoice.Description}
					/>
					

			);
		}

	});
};


// ...

type tCommonF = {
	todaysdate(): string;
	todaysDatePaded(): string;
	formatDate(date: Date): string;
	formatDateSlashes(dateWithHypens: string): string;
	formatDateMMdYYYY(date: Date): string;
	formatDateYYYYmmDD(date: Date): string;
	Append8WithECBcheckSum(hEightDigitNOVnum: number): string;
	pad(n: number, width: number): string;
	IsValidNOVnumber(aDSform: string, LowHigh: string, TgtVal: string): boolean;
	IsValidDate(m: number, d: number, y: number): boolean
	parseArraytoReactSelect(jsonResponse: any): any;
	parseArraytoReactSelect_IA(jsonResponse: any): any;
	RefreshPage(): void;
	MomentToDate(sDate: string): Date;
	DateToYYYYmmDD(aDate: Date | string): string;
}

export const commonf: tCommonF = {
	todaysdate,
	todaysDatePaded,
	formatDate,
	formatDateSlashes,
	formatDateMMdYYYY,
	formatDateYYYYmmDD,
	MomentToDate,
	Append8WithECBcheckSum,
	pad,
	IsValidNOVnumber,
	IsValidDate,
	parseArraytoReactSelect,
	parseArraytoReactSelect_IA,
	RefreshPage,
	DateToYYYYmmDD,
}

export const commonf2: any = {
	anyDatePaded,
	todaysUTCdate,
	isNotDefined,
	isDateType,
	fTSOjsxElement,
	sleep,
	arrAllEqual,
}