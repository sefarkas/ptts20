﻿import { Button, Box } from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';
import React, { useEffect } from 'react';
import ReactSelect from "react-select";
import { useForm } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers';
import styles from "./templateStyles";
import moment from 'moment';

interface Props {
	open: boolean;
	onSubmit: (statementValue: string) => void;
	onClose: (event: any) => void;
	respondent: string;
	incidentDate: string;
}

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const StyledButtonGreen = withStyles({
	root: {
		background: '#2b995f',
		borderRadius: 1,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const customStyles = {
	control: (base: any) => ({
		...base,
		minHeight: '30px',
		height: '30px',
	}),
	dropdownIndicator: (base: any) => ({
		...base,
		padding: 0
	}),
	clearIndicator: (base: any) => ({
		...base,
		padding: 0
	}),
	valueContainer: (base: any) => ({
		...base,
		height: '30px',
		padding: '0 6px'
	}),
	indicatorContainer: (base: any) => ({
		height: '30px'
	})
};

const titleOpts = [
	{ value: 1, label: "A/M" },
	{ value: 2, label: "S/W" },
	{ value: 3, label: "Supv" },
	{ value: 4, label: "GS" },
	{ value: 5, label: "Other" }
];

const categoryOpts = [
	{ value: 1, label: "A" },
	{ value: 2, label: "B" },
	{ value: 3, label: "C" }
];

export function TemplateNoPhone(props: Props) {
	const classes = styles();
	const [templateValues, setTemplateValues] = React.useState({ title: { value: 0, label: "" }, category: { value: 0, label: "" }});
	const [dateMin, setDateMin] = React.useState("");

	const dialogSchema = yup.object().shape({
		onDate: yup.string().required("Required field"),
		title: yup.string().required("Required field"),
		category: yup.string().required("Required field"),		
		failedContactDate: yup.string().required("Required field"),
		reportedDate: yup.string().required("Required field")		
	});

	const { register, handleSubmit, errors, setValue, clearErrors, getValues } = useForm({
		resolver: yupResolver(dialogSchema)
	});

	const formatDate = (value: string) => {
		let date = value.split('-');
		const year = String(date[0]).split('-');
		const month = String(date[1]).split('-');
		const day = String(date[2]).split('-');
		return month + "/" + day + "/" + year;
	}

	React.useEffect(() => {
		register({ name: "onDate" });
		register({ name: "title" });
		register({ name: "category" });		
		register({ name: "failedContactDate" });
		register({ name: "reportedDate" });
	}, []);

	React.useEffect(() => {
		setValue("onDate", props.incidentDate);
	}, [props.incidentDate]);

	React.useEffect(() => {
		setValue("onDate", props.incidentDate);
		setValue("failedContactDate", "");
		setValue("reportedDate", "");

		clearErrors("onDate");
		clearErrors("title");
		clearErrors("category");
		clearErrors("failedContactDate");
		clearErrors("reportedDate");
	}, [props.open]);

	const onSubmit = (data: any) => {
		const title = templateValues.title.label === "Other" ? "" : templateValues.title.label + " ";

		let templateOutput = "On " + formatDate(data.onDate) + ", " + title + props.respondent + ", a category " + templateValues.category.label + " employee reported sick and began medical leave. ";
		templateOutput = templateOutput + title + props.respondent + " claimed he/she had no phone and was placed out with no phone, and by doing so is required to contact the DSNY H.C.F. Supervisor each day they are on medical leave providing an update on their medical condition until their first visit to the DSNY H.C.F. ";
		templateOutput = templateOutput + title + props.respondent + " failed to contact the DSNY H.C.F. on " + formatDate(data.failedContactDate) + ". ";
		templateOutput = templateOutput + title + props.respondent + " reported to the DSNY H.C.F. on " + formatDate(data.reportedDate) + " and was resumed back to work.";
		props.onSubmit(templateOutput);
	};

	const handleChange = (e: any, control?: string) => {
		if (control === "onDate") {
			setValue("onDate", e.target.value);
			setDateMin(e.target.value);
			clearErrors("onDate");

			if (moment(getValues("failedContactDate")).isBefore(e.target.value)) {
				setValue("failedContactDate", "");
			};

			if (moment(getValues("reportedDate")).isBefore(e.target.value)) {
				setValue("reportedDate", "");
			};
		}
		else if (control === "title") {
			const selected = titleOpts.filter((d) => { return d.value === e.value; })[0].label;
			setTemplateValues({
				...templateValues,
				title: { value: e.value, label: selected }
			});
			setValue("title", e.value);
			clearErrors("title");
		}
		else if (control === "category") {
			const selected = categoryOpts.filter((d) => { return d.value === e.value; })[0].label;
			setTemplateValues({
				...templateValues,
				category: { value: e.value, label: selected }
			});
			setValue("category", e.value);
			clearErrors("category");
		}
		else if (control === "reportedDate") {
			setValue("reportedDate", e.target.value);
			clearErrors("reportedDate");
		}
		else if (control === "failedContactDate") {
			setValue("failedContactDate", e.target.value);
			clearErrors("failedContactDate");
		}
	};

	const handleCancel = (event: any) => {
		props.onClose("");
	};

	const pad = (n: any) => {
		return ("00" + n).slice(-2);
	}

	const todaysDate = () => {
		const tempDate = new Date();
		const currDate = tempDate.getFullYear() + '-' + pad((tempDate.getMonth() + 1)) + '-' + pad((tempDate.getDate()));
		return currDate;
	}

	return (
		<form autoComplete="off">
			<Box className={classes.box}>
				<Grid container className={classes.grid} spacing={0}>
					<Grid item xs={12} className={classes.gridCellTemplate}>
						<label className={classes.labelTemplateText}>On </label>
						{errors.onDate && <label className={classes.error}>* </label>}
						<input type="date"
							name="onDate"
							defaultValue={props.incidentDate}
							placeholder="Select Date"
							className={classes.datepicker}
							max={todaysDate()}
							onChange={e => handleChange(e, "onDate")}
						/>
						<label className={classes.labelTemplateText}> , </label>
						{errors.title && <label className={classes.error}>* </label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})}
							/></div>
						<label className={classes.labelTemplateText}>{props.respondent}, a category </label>
						{errors.category && <label className={classes.error}>* </label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "category")}
								options={categoryOpts}
								name="category"
								value={templateValues.category}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})}
							/></div>
						<label className={classes.labelTemplateText}>employee reported sick and began medical leave. </label>
						{errors.title && <label className={classes.error}>* </label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})}
							/></div>
						<label className={classes.labelTemplateText}>{props.respondent} claimed he/she had no phone and was placed out with no phone, and by doing so is required to contact the DSNY H.C.F. Supervisor each day they are on medical leave providing an update on their medical condition until their first visit to the DSNY H.C.F. </label>
						{errors.title && <label className={classes.error}>* </label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})}
							/></div>
						<label className={classes.labelTemplateText}>{props.respondent} failed to contact the DSNY H.C.F. on </label>
						{errors.failedContactDate && <label className={classes.error}>* </label>}
						<input type="date"
							name="failedContactDate"
							placeholder="Select Date"
							className={classes.datepicker}
							min={dateMin}
							max={todaysDate()}
							onChange={e => handleChange(e, "failedContactDate")}
						/>
						{errors.title && <label className={classes.error}>* </label>}
						<div style={{ display: 'inline-block', maxWidth: 100, marginRight: 3 }}>
							<ReactSelect
								onChange={e => handleChange(e, "title")}
								options={titleOpts}
								name="title"
								value={templateValues.title}
								placeholder=""
								className={classes.reactselectsmall}
								styles={customStyles}
								theme={theme => ({
									...theme,
									borderRadius: 1,
									colors: {
										...theme.colors,
										primary25: '#d2f6e3',
										primary50: '#2b995f',
										primary: '#2b995f',
									},
									spacing: {
										controlHeight: 25,
										menuGutter: 1,
										baseUnit: 2,
									},
								})}
							/></div>
						<label className={classes.labelTemplateText}>{props.respondent} reported to the DSNY H.C.F. on </label>							
						{errors.reportedDate && <label className={classes.error}>* </label>}
						<input type="date"
							name="reportedDate"
							placeholder="Select Date"
							className={classes.datepicker}
							min={dateMin}
							max={todaysDate()}
							onChange={e => handleChange(e, "reportedDate")}
						/>
						<label className={classes.labelTemplateText}> and was resumed back to work.</label>
					</Grid>
				</Grid>
			</Box>
			<StyledButtonGreen
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleSubmit(onSubmit)}
			>Confirm
				    </StyledButtonGreen>
			<StyledButton
				className={classes.button}
				variant="outlined"
				color="primary"
				onClick={handleCancel}>
				Cancel
				</StyledButton>
		</form>
	);
}

