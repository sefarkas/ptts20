﻿import axios from "axios";
import moment from "moment";
import { PTTSfunctions } from "../components/basicComponents";
import {
    DS21HDRAction, DS21HDRActions, tDS21HDR, tOneNOV, DS21CardAction,
    DS21CardActions, tDS21Card, tEfDS21HDR, tSpecialDistrict, tViolationCode,
    tTicketServiceType, tBulkUpdateRow, BulkUpdateNOV_ACTION
} from "../model";
import { dummyBulkUpdateThisNOV, dummyNOV } from "../reducers/DS21Card";
import { allServices } from "../services";

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

//export interface InterForActionUpdateDS21HDR { type: typeof DS21Actions.UPDATE_DS21, payload: {} | DS21HDR };
//export interface InterForActionDS21Complete { type: typeof DS21Actions.COMPLETE_DS21, payload: boolean };
//export interface InterForActionDs21UnComplete { type: typeof DS21Actions.REVERT2INCOMPLETE_DS21, payload: boolean };
//export interface InterForActionSetNOVsForDS21Card { type: typeof DS21Actions.DS21_CARD_TABLE, payload: any | oneNOV[] };
//export interface InterForActionErrHandler { type: typeof DS21Actions.MSG, payload: string };

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
export type KnownActionDS21Card = DS21CardAction;
export type KnownActionDS21HDR = DS21HDRAction;
	//InterForActionUpdateDS21HDR
	//| InterForActionDS21Complete
	//| InterForActionDs21UnComplete
	//| InterForActionSetNOVsForDS21Card
	//| InterForActionErrHandler;

function fReplaceBulkUpdateEveryRow(allRows: tBulkUpdateRow[], dispatchRef: any) {
    dispatchRef({
        type: DS21CardActions.RPL_BulkUpdateTheseNOVs, payload: allRows
    });
}

function fIudBulkUpdateTheseNOVs(aRow: tBulkUpdateRow,
    dispatchRef: any, ADD_REM_UPT: BulkUpdateNOV_ACTION): boolean {
    try {
        if (ADD_REM_UPT === BulkUpdateNOV_ACTION.ADD) {
            dispatchRef({
                type: DS21CardActions.ADD_BulkUpdateTheseNOVs, payload: aRow
            });
        }
        if (ADD_REM_UPT === BulkUpdateNOV_ACTION.DEL) {
            dispatchRef({
                type: DS21CardActions.DEL_BulkUpdateTheseNOVs, payload: aRow
                }
            );
        }
        if (ADD_REM_UPT === BulkUpdateNOV_ACTION.UPT) {
            dispatchRef({
                type: DS21CardActions.UPT_BulkUpdateThisNOV, payload: {
                    Id: aRow.Id,
                    NOVnumber: aRow.NOVnumber,
                    Message: aRow.Message,
                    NOVnumberIsValid: aRow.NOVnumberIsValid,
                    TicketStatusID: aRow.TicketStatusID
                }
            });
        }

        if (ADD_REM_UPT === BulkUpdateNOV_ACTION.RST) {
            dispatchRef({
                type: DS21CardActions.RST_BulkUpdateTheseNOVs, payload: [{
                    Id: dummyBulkUpdateThisNOV.Id,
                    NOVnumber: dummyBulkUpdateThisNOV.NOVnumber,
                    Message: dummyBulkUpdateThisNOV.Message,
                    NOVnumberIsValid: dummyBulkUpdateThisNOV.NOVnumberIsValid
                }]
            });
        }
    }
    catch (error) {
        alert(error.toString());
        return false;
    }
    return true;
}

function fIudDS21Card(dispatchRef: any, oneDS21HDRrow: tDS21Card): void {
    try {

        dispatchRef( {
		    type: DS21CardActions.UPDATE_DS21CARD,
		    payload: oneDS21HDRrow,
        });

    }
    catch (error) {
        alert(error.toString());
    }
}

function fIudNOV(dispatchRef: any, oneNOV: tOneNOV, loggedInUser: string): boolean {
    try {
        if (allServices.PTTSiudServices.iudNOV(oneNOV, loggedInUser)) {
            dispatchRef({
                type: DS21CardActions.UPDATE_NOV,
                payload: oneNOV,
            });
        }
        else {
            return false;
        }

        return true;
    }
    catch (error) {
        alert(error.toString());
        return false;
    }
}

function fNotarizedVoided(TwentyFiveNOVwithNewVal: tOneNOV[], dispatchRef: any, pECBSummonsNumber: string, currUser: string): void {
    try {

        dispatchRef({ type: DS21CardActions.aDS21_CARDNOV_TABLE, payload: TwentyFiveNOVwithNewVal });
        TwentyFiveNOVwithNewVal.forEach(function (aRow: tOneNOV) {
            if (aRow.ECBSummonsNumber === pECBSummonsNumber) {
                if(!allServices.PTTSiudServices.iudNOV(aRow, currUser)) alert("allServices.PTTSiudServices.iudNOV did not update " + aRow.ECBSummonsNumber);
            }
        })
    }
    catch (error) {
        alert(error.toString());
    }

}

function fDS21MENU_POC(arr: tSpecialDistrict[], dispatchRef: any): boolean {
    try {
      //  console.log("enter fDS21MENU_POC", arr);
        dispatchRef({ type: DS21CardActions.aDS21_MENU_POC, payload: arr });
    }
    catch (error) {
        alert(error.toString());
        return false;
    }
    return true;
}
function fDS21MENU_TST(arr: tTicketServiceType[], dispatchRef: any): boolean {
    try {
      //  console.log("enter fDS21MENU_TST", arr);
        dispatchRef({ type: DS21CardActions.aDS21_MENU_TST, payload: arr });
    }
    catch (error) {
        alert(error.toString());
        return false;
    }
    return true;
}
function fDS21MENU_VC(arr: tViolationCode[], dispatchRef: any): boolean {
    try {
       // console.log("enter fDS21MENU_VC", arr);
        dispatchRef({ type: DS21CardActions.aDS21_MENU_VC, payload: arr });
    }
    catch (error) {
        alert(error.toString());
        return false;
    }
    return true;
}

//function fCompleteDS21HDR(DS21Id: number): DS21Action {
//	return {
//		type: DS21Actions.COMPLETE_DS21CARD,
//		payload: DS21Id,
//	};
//}

//function fUncompleteDS21HDR(DS21Id: number): DS21Action {
//	return {
//		type: DS21Actions.REVERT2INCOMPLETE_DS21,
//		payload: DS21Id,
//	};
//}


//async function fGetNOVsForOneCard(DS1984number: string, dispatch: any): Promise<boolean> {
//    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("started action fGetNOVsForOneCard");
//    let bRslt: boolean = false;
//	try {

//        const response: any = await allServices.PTTSgetServices.oneDS21Card(DS1984number) // axios always returns an object with a "data" child
//		const rdata: tOneNOV[] = response.data;
//        dispatch(success(rdata));
//        bRslt = true;
//	}
//	catch (error) {
//        dispatch(failure(error.toString()));
//        bRslt = false;
//	}
//    return bRslt;

//	function success(lstOfNOVs: tOneNOV[]): DS21Action {
//		return { type: DS21Actions.aDS21_CARDNOV_TABLE, payload: lstOfNOVs };
//	};
//	function failure(error: string): DS21Action {
//		return { type: DS21Actions.CardMSG, payload: error };
//	};
//}


async function fDS21HDRList(dispatchRef: any, currDS21hdr: tDS21HDR[]): Promise<boolean> { // every DS21 
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("started action DS21HDRList");
	let bRslt: boolean = false;

	let YYYYMMDD: string = '20200901';
	console.log("fDS21HDRList has currDS21hdr");
	console.log(currDS21hdr);
	if (!currDS21hdr ? false : currDS21hdr.length < 1 ? false : true) {
		YYYYMMDD = '20190101';
	}
	else {
		// most recent ModifiedOn:
		await currDS21hdr.sort(function (a: tDS21HDR, b: tDS21HDR) {
			var yyyymmddA = !a.ModifiedOn ? "" : a.ModifiedOn.toLocaleDateString(); 
			var yyyymmddB = !b.ModifiedOn ? "" : b.ModifiedOn.toLocaleDateString();
			if (yyyymmddA < yyyymmddB) {
				return 1;
			}
			if (yyyymmddA > yyyymmddB) { // DESC
				return -1;
			}
			// names must be equal
			return 0;
		}).slice(0);
		const modOn: any = await currDS21hdr[0].ModifiedOn;
		console.log("fDS21HDRList has modOn of: ");
		console.log(modOn);
		YYYYMMDD = PTTSfunctions.Commonfunctions.formatDateMMdYYYY(modOn);
    }

		console.log("fDS21HDRList has YYYYMMDD of: ", YYYYMMDD);

	// assumes DS21 header rows are never deleted ...

    //await axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS21HDRListNewest/` + YYYYMMDD)
    //    .then(
    //        (response: any) => {
    //            const rdata: tDS21HDR[] = response.data;
    //            dispatchRef(success(rdata));
    //            bRslt = true;
    //        },
    //        (error: { toString: () => any; }) => {
    //            dispatchRef(failure(error.toString()));
    //            bRslt = false;
    //        }
    //);
    try {

        const response: any = await allServices.PTTSgetServices.DS21HDRList(YYYYMMDD) // axios always returns an object with a "data" child
        const rdata: tDS21HDR[] = response.data;
        dispatchRef(success(rdata));
        bRslt = true;
    }
    catch (error) {
        dispatchRef(failure(error.toString()));
        bRslt = false;
    }
    return bRslt;

    function success(rdata: tDS21HDR[]): DS21HDRAction { return { type: DS21HDRActions.aDS21HDRROWSadd, payload: rdata } };
    function failure(error: string): DS21HDRAction { return { type: DS21HDRActions.HDRmsg, payload: error } }
}

function fDS21HDRsyncWithCard(dispatchRef: any, aDS21Card: tDS21Card,
    currUser: string, issuingOfficerRefNo: string,
    smallestNOVnumber: number, largestNOVnumber: number): boolean {
    // JavaScript passes objects by reference, so delays in updating 
    // the attributes of aDS21Card by HOOKs affects values
    // being copied into aDS21HDRforCard
    try {
        // convert tDS21Card to tDS21HDR 
        const aDS21HDRforCard: tDS21HDR = {
            ID: aDS21Card.ID,
            Year: parseInt(aDS21Card.CreatedOn.toString().substr(0,4)),
            IssuingOfficerSignature: aDS21Card.IssuingOfficerSignature,
            Command: aDS21Card.Command,
            IssuingOfficerName: aDS21Card.IssuingOfficerName,
            TaxRegNumber: aDS21Card.TaxRegNumber,
            Agency: aDS21Card.Agency,
            DateCardStarted: aDS21Card.DateCardStarted,
            DateReturned: aDS21Card.DateReturned,
            AssignedDistrict: aDS21Card.AssignedDistrict,
            IssuingOfficerTitle: aDS21Card.IssuingOfficerTitle,
            OfficerInChargeName: aDS21Card.OfficerInChargeName,
            OfficerInChargeTitle: aDS21Card.OfficerInChargeTitle,
            DateCardCompleted: aDS21Card.DateCardCompleted,
            Remarks: aDS21Card.Remarks,
            DS1984Number: aDS21Card.DS1984Number,
            ModifiedOn: (
                parseInt(aDS21Card.CreatedOn.toString().substr(0, 4)) > 1969
                ? !aDS21Card.ModifiedOn
                    ? aDS21Card.CreatedOn
                    : parseInt(aDS21Card.ModifiedOn.toString().substr(0, 4)) > 1969
                        ? aDS21Card.ModifiedOn
                        : new Date(0)
                : parseInt(aDS21Card.ModifiedOn.toString().substr(0, 4)) > 1969
                ? aDS21Card.ModifiedOn
                    : new Date(0)
            ),
            ModifiedBy: !aDS21Card.ModifiedBy ? "" : aDS21Card.ModifiedBy.length > 2 ? aDS21Card.ModifiedBy : currUser,
            FormStatusID: aDS21Card.FormStatusID,
            IssuingOfficerRefNo: issuingOfficerRefNo,
            CreatedBy: aDS21Card.CreatedBy,
            CreatedOn: aDS21Card.CreatedOn,
            BoxNumberedFrom: smallestNOVnumber,
            BoxNumberedTo: largestNOVnumber,
        }
        console.log("fDS21HDRsyncWithCard aDS21Card ");
        console.log(aDS21Card);
        console.log("fDS21HDRsyncWithCard aDS21HDRforCard ");
        console.log(aDS21HDRforCard);
        dispatchRef({ type: DS21CardActions.UPDATE_DS21CARD, payload: aDS21Card })
        dispatchRef({ type: DS21HDRActions.aDS21HDRROWSupt, payload: aDS21HDRforCard })
        dispatchRef({ type: DS21HDRActions.aDS21HDRROWSFLTRDupt, payload: aDS21HDRforCard })
        return true;
    }
    catch (error) {
        console.log("fDS21HDRsyncWithCard", error.toString());
        console.log("aDS21Card.CreatedOn.getFullYear() ", aDS21Card.CreatedOn.toString().substr(0, 4));
        console.log(aDS21Card);
        return false;

    }
}

async function fDS21istOf25NOVs(dispatchRef: any, DS1984receiptNo: string): Promise<boolean> { // one DS1984 per DS21
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("started action DS21istOf25NOVs");
    let bRslt: boolean = false;

    await axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS21NOVList/` + DS1984receiptNo) // returns IList<DS21> from C#
        .then(
            (response: any) => {
                const rdata: tOneNOV[] = response.data;
                dispatchRef(success(rdata));
                bRslt = true;
            },
            (error: { toString: () => any; }) => {
                dispatchRef(failure(error.toString()));
                bRslt = false;
            }
        );
    return bRslt;

    function success(rdata: tOneNOV[]): DS21CardAction { return { type: DS21CardActions.aDS21_CARDNOV_TABLE, payload: rdata } };
    function failure(error: string): DS21CardAction { return { type: DS21CardActions.CardMSG, payload: error } }

}

function fDS21istOf25NOVsWithCallBack(dispatchRef: any, DS1984receiptNo: string, callbackSuccess: any, callbackFail: any) { // one DS1984 per DS21
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("started action DS21istOf25NOVs");
    let bRslt: boolean = false;

    axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS21NOVList/` + DS1984receiptNo) // returns IList<DS21> from C#
        .then(
            (response: any) => {
                const rdata: tOneNOV[] = response.data;
                dispatchRef(success(rdata));
                callbackSuccess(rdata);
            },
            (error: { toString: () => any; }) => {
                dispatchRef(failure(error.toString()));
                callbackFail(error.toString());
            }
        );
    return bRslt;

    function success(rdata: tOneNOV[]): DS21CardAction { return { type: DS21CardActions.aDS21_CARDNOV_TABLE, payload: rdata } };
    function failure(error: string): DS21CardAction { return { type: DS21CardActions.CardMSG, payload: error } }

}
async function bDS21HDRforThisNOV(dispatchRef: any, NOVnumber: string): Promise<boolean> {
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("started action GetDS21HDRforThisNOV");
    let bRslt: boolean = false;
    await axios.get(`${process.env.REACT_APP_PTTS20_API}/GetDS21HDRforThisNOV/` + NOVnumber)
        .then(
            (response: any) => {
                const rdata: tDS21HDR[] = response.data;
                dispatchRef(success(rdata));
                bRslt = true;
            },
            (error: { toString: () => any; }) => {
                dispatchRef(failure(error.toString()));
                bRslt = false;
            }
        );
    return bRslt;

    function success(rdata: tDS21HDR[]): DS21HDRAction {
        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("bDS21HDRforThisNOV success");
        return { type: DS21HDRActions.aDS21HDRROWSadd, payload: rdata }
    };
    function failure(error: string): DS21HDRAction {
        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("bDS21HDRforThisNOV failure: ", error);
        return { type: DS21HDRActions.HDRmsg, payload: error }
    }
}

async function bDS21HDRRelatedToOneDS1984List(dispatchRef: any,DS1984receiptNo: string ): Promise < boolean > { // only one DS21 for a single DS1984
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("started action DS21HDRRelatedToOneDS1984List");
    let bRslt: boolean = false;
    await axios.get(`${process.env.REACT_APP_PTTS20_API}/GetOneDS21HDR/` + DS1984receiptNo)
        .then(
            (response: any) => {
                const rdata: tDS21HDR[] = response.data;
                dispatchRef(success(rdata));
                bRslt = true;
            },
            (error: { toString: () => any; }) => {
                dispatchRef(failure(error.toString()));
                bRslt = false;
            }
        );
    return bRslt;

    function success(rdata: tDS21HDR[]): DS21HDRAction {
        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS21HDRRelatedToOneDS1984List success");
        return { type: DS21HDRActions.aDS21HDRROWSadd, payload: rdata }
    };
    function failure(error: string): DS21HDRAction {
        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS21HDRRelatedToOneDS1984List failure: ", error);
        return { type: DS21HDRActions.HDRmsg, payload: error }
    }

}


export const allActions = {
    iudBulkUpdateNOVlist: fIudBulkUpdateTheseNOVs,
    rplBulkUpdateEveryRow: fReplaceBulkUpdateEveryRow,
    iudDS21Card: fIudDS21Card,
    iudNOV: fIudNOV,
    // completeDS21HDR: fCompleteDS21HDR,
    // uncompleteDS21HDR: fUncompleteDS21HDR,
    GetNOVsForOneCard: fDS21istOf25NOVs,
    GetNOVsForOneCardWithCallBack: fDS21istOf25NOVsWithCallBack,
    DS21HDRList: fDS21HDRList,
    DS21HDRRelatedToOneDS1984List: bDS21HDRRelatedToOneDS1984List,
    DS21HDRRelatedToOneNOV: bDS21HDRforThisNOV,
    NOVnotarizedVoided: fNotarizedVoided,
    DS21HDRsyncWithCard: fDS21HDRsyncWithCard,
    MenuChoicesPOC: fDS21MENU_POC,
    MenuChoicesTST: fDS21MENU_TST,
    MenuChoicesVC: fDS21MENU_VC,
}