import React, { useState } from "react";
import { useForm, Controller } from "react-hook-form";
import ReactSelect from "react-select";
import {
  TextField,
  Checkbox,
  Select,
  MenuItem,
  Switch,
  RadioGroup,
  FormControlLabel,
  Radio,
  Button
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Grid from '@material-ui/core/Grid';


let renderCount = 0;

const options = [
	{ value: "chocolate", label: "Chocolate" },
	{ value: "strawberry", label: "Strawberry" },
	{ value: "vanilla", label: "Vanilla" }
  ];
  
  const defaultValues = {
	Native: "",
	TextField: "",
	Select: "",
	ReactSelect: "",
	Checkbox: false,
	switch: false,
	RadioGroup: ""
  };

export function ComplaintsPage() {
	const classes = useStyles();
	const { register, reset, control } = useForm({ defaultValues });
  	const [data] = useState(null);
  	renderCount++;

	return (
		<div className={classes.root}>
			<div>
				<form className={classes.formstyle}>
				<h1>React Hook Form Input</h1>
				<p>React Hook Form work with controlled component.</p>
				<span className="counter">Render Count: {renderCount}</span>

				
				<Grid container spacing={2}>
					<Grid item xs={6}>
						<label className={classes.label}>Native Input:</label>
						<input name="Native" ref={register}  className={classes.input}/>
					</Grid>
					<Grid item xs={6}>
						<label className={classes.label}>MUI Checkbox</label>
						<Controller
						as={<Checkbox />}
						name="Checkbox"
						type="checkbox"
						control={control}
						/>
					</Grid>
					<Grid item xs={6}>
						<label className={classes.label}>Radio Group</label>
						<Controller
						as={
							<RadioGroup aria-label="gender" name="gender1">
							<FormControlLabel
								value="female"
								control={<Radio />}
								label="Female"
							/>
							<FormControlLabel
								value="male"
								control={<Radio />}
								label="Male"
							/>
							</RadioGroup>
						}
						name="RadioGroup"
						control={control}
						/>
					</Grid>
					<Grid item xs={6}>
						<label className={classes.label}>MUI TextField</label>
						<Controller as={<TextField />} name="TextField" control={control} />
					</Grid>
					<Grid item xs={6}>
						<label className={classes.label}>MUI Select</label>
						<Controller
						as={
							<Select>
							<MenuItem value={10}>Ten</MenuItem>
							<MenuItem value={20}>Twenty</MenuItem>
							<MenuItem value={30}>Thirty</MenuItem>
							</Select>
						}
						name="Select"
						control={control}
						/>
					</Grid>
					<Grid item xs={6}>
						<label className={classes.label}>MUI Switch</label>
						<Controller
						as={<Switch value="checkedA" />}
						type="checkbox"
						name="switch"
						control={control}
						/>
					</Grid>
					<Grid item xs={6}>
						<label className={classes.label}>React Select</label>
						<Controller
						as={<ReactSelect />}
						options={options}
						name="ReactSelect"
						isClearable
						control={control}
						className={classes.reactselect}
						/>
					</Grid>

				
				</Grid>
    
				
				{data && (
				<pre style={{ textAlign: "left", color: "white" }}>
					{JSON.stringify(data, null, 2)}
				</pre>
				)}

				<Button
					className={classes.button}
				variant="outlined"
				color="primary"
				onClick={() => {
					reset(defaultValues);
				}}
				>
				Reset Form
				</Button>
			</form>
	  	</div>
	  </div>
	);
}

const useStyles = makeStyles({
	root: {
		flexGrow: 1,
		height: "100%",
		paddingLeft: 15,
		paddingRight: 15,
		
	},

	button: {
		marginTop: 20,
	},
	
	formstyle: {
		maxWidth: "100%",
		padding: 0,
	  },

	  input: {
		display: "block",
		width: "100%",
		borderRadius: 4,
		border :"1px solid",
		padding: "10px 15px",
		marginBottom: 10,
		fontSize: 14,
		maxWidth: 400,
	  },

	  reactselect:{
		maxWidth: 400,
	  },

	  label: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginBottom: 13,
		marginTop: 20,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
	  },
});
