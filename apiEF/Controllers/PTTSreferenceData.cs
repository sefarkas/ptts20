﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using PTTSapiEFdb;

namespace apiEF.Controllers
{
    [RoutePrefix( "PTTS20/api/ReferenceData" )]
    [EnableCors( origins: "*", headers: "*", methods: "*" )]
    // [EnableCors( origins: "https://localhost:18144", headers: "*", methods: "*" )]
    // has to refer to the client IP and port; see also WebApiConfig Register and Web.Config system.webServer handlers
    public class PTTSrefernceDataController : ApiController
    {
        [Route( "GetSpecialDistricts" )]
        [HttpGet]
        public IQueryable<SpecialDistrict> GetSpecialDistrictsList()
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            var q = db.SpecialDistricts.AsQueryable();
            return q;
        }

        [Route( "GetSpecialDistricts/{id}" )]
        [HttpGet]
        public SpecialDistrict GetOneSpecialDistricts( int id )
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            IQueryable<SpecialDistrict> rslt = db.SpecialDistricts.AsQueryable();
            SpecialDistrict oneDistrict = rslt.FirstOrDefault<SpecialDistrict>( adst => adst.Id == id );
            return oneDistrict;

        }

        [Route( "GetViolationCodes" )]
        [HttpGet]
        public IQueryable<ViolationCode> methGetViolationCodes()
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            var q = db.ViolationCodes.AsQueryable();
            return q;
        }


        [Route( "GetViolationTypes" )]
        [HttpGet]
        public IQueryable<ViolationType> methGetViolationTypes()
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            var q = db.ViolationTypes.AsQueryable();
            return q;
        }

        [Route( "GetTicketStatusOptions" )]
        [HttpGet]
        public IList<DS21_TicketStatus> methGetTicketStatusOptions()
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            IQueryable<DS21_TicketStatus> q = db.DS21_TicketStatus.AsQueryable()
                .Where( aRow => aRow.OrderByThisTinyInt > -1 );
            IList<DS21_TicketStatus> ls = q.ToList<DS21_TicketStatus>();
            ls = ls.OrderBy(aRow => aRow.OrderByThisTinyInt).ToList();
            return ls;
        }

        [Route("GetDS1704ReasonList")]
        [HttpGet]
        public IList<VoidReason> methGetDS1704ReasonList()
        {
            PaperTicketsEntities db = new PaperTicketsEntities();
            IQueryable<VoidReason> q = db.VoidReasons.AsQueryable()
                .Where(aRow => aRow.Id > -1);
            IList<VoidReason> ls = q.ToList<VoidReason>();
            ls = ls.OrderBy(aRow => aRow.Id).ToList();
            return ls;
        }
    }
}
