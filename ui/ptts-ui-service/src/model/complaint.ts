export interface Complaint {
    id: number;
    respondent: string;
    referenceNumber: string;
    text: string;
    completed: boolean;
}

export enum ComplaintActions {
    ADD_COMPLAINT = "ADD_COMPLAINT",
    DELETE_COMPLAINT = "DELETE_COMPLAINT",
    COMPLETE_COMPLAINT = "COMPLETE_COMPLAINT",
    REVERT2INCOMPLETE_COMPLAINT = "INCOMPLETE_COMPLAINT",
    START_NEW_COMPLAINT = "START_NEW_COMPLAINT"
}

interface ComplaintActionType<T, P> {
  type: T;
  payload: P;
}

export type ComplaintAction =
  ComplaintActionType<typeof ComplaintActions.ADD_COMPLAINT, Complaint>
  | ComplaintActionType<typeof ComplaintActions.COMPLETE_COMPLAINT, number>
  | ComplaintActionType<typeof ComplaintActions.REVERT2INCOMPLETE_COMPLAINT, number>
  | ComplaintActionType<typeof ComplaintActions.DELETE_COMPLAINT, number>
  | ComplaintActionType<typeof ComplaintActions.START_NEW_COMPLAINT, string>
;
