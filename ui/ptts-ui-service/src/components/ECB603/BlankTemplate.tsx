﻿import { Button } from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useEffect, useState } from "react"; // , useReducer
import { useForm } from "react-hook-form"; // , FormProvider
import { isNullOrUndefined } from 'util';
import { PTTSfunctions} from '../basicComponents';
import { allServices } from '../../services';
import { history } from "../../configureStore";
import { ECB603 } from '../../model';

type postDataResult<T> = {
	success: boolean;
	errors?: { [P in keyof T]?: string[] };
};

type FormData = {
	ecbReceiptnumber: string;
};

export function NewECB603Form(props: any) {

	const [isLoading, setLoading] = React.useState(false);

	const [oneECB603form, setoneECB603form] = useState<ECB603>(
		{
			CartonNoFrom: 0,
			CartonNoTo: 0,
			CartonQty: 0,
			CreatedBy: "currentUserToDo",
			CreatedOn: new Date(),
			CustomerOrderNo: "",
			DS1983ReceiptNo: "3YYYYMMDDL1400000#####",
			FactoryJobNo: "8",
			ID: 1301,
			IsVoid: false,
			ModifiedOn: new Date(),
			ModifiedBy: "currentUserToDo",
			NoOfParts: 0,
			OrderNo: "0#-######-A",
			ReceiptNo: "6YYYYMMDDENHQ0000#####",
			TicketNoFrom: 1,
			TicketNoTo: 1000,
			Completed: false
		});

	const [rcpt, setrcpt] = useState(oneECB603form.ReceiptNo);

	const [CartonNoFromState, setCartonNoFromState] = useState(0);
	const [CartonNoToState, setCartonNoToState] = useState(0);
	const [CartonQtyState, setCartonQtyState] = useState(0);
	const [CreatedByState, setCreatedByState] = useState("uidToDo");
	const [CreatedOnState, setCreatedOnState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [CustomerOrderNoState, setCustomerOrderNoState] = useState("text");
	const [DS1983ReceiptNoState, setDS1983ReceiptNoState] = useState("null");
	const [FactoryJobNoState, setFactoryJobNoState] = useState("text");
	const [IDState, setIDState] = useState(0);
	const [IsVoidState, setIsVoidState] = useState(false);
	const [ModifiedByState, setModifiedByState] = useState("uidToDo");
	const [ModifiedOnState, setModifiedOnState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [NoOfPartsState, setNoOfPartsState] = useState(0);
	const [OrderNoState, setOrderNoState] = useState("text");
	const [ReceiptNoState, setReceiptNoState] = useState("text");
	const [TicketNoFromState, setTicketNoFromState] = useState(0);
	const [TicketNoToState, setTicketNoToState] = useState(0);
	const [CompletedState, setCompletedState] = useState(false);


	useEffect(() => {
		setLoading(true);
		setoneECB603form({ // take the individual hook const and set each oneECB603form object properties
			CartonNoFrom: CartonNoFromState,
			CartonNoTo: CartonNoToState,
			CartonQty: CartonQtyState,
			CreatedBy: CreatedByState,
			CreatedOn: new Date(CreatedOnState),
			CustomerOrderNo: CustomerOrderNoState,
			DS1983ReceiptNo: "null",
			FactoryJobNo: FactoryJobNoState,
			ID: IDState,
			IsVoid: IsVoidState,
			ModifiedBy: "useEffect",
			ModifiedOn: new Date(ModifiedOnState),
			NoOfParts: NoOfPartsState,
			OrderNo: OrderNoState,
			ReceiptNo: ReceiptNoState,
			TicketNoFrom: TicketNoFromState,
			TicketNoTo: TicketNoToState,
			Completed: CompletedState,

		});
		setLoading(false);

	 }, [
		 CartonNoFromState,
		 CartonNoToState,
		 CartonQtyState,
		 CreatedByState,
		// CreatedOnState, should be set when object is first created; no update allowed
		 CustomerOrderNoState,
		 DS1983ReceiptNoState,
		 FactoryJobNoState,
		 IDState,
		 IsVoidState,
		 ModifiedByState,
		 ModifiedOnState,
		 NoOfPartsState,
		 OrderNoState,
		 ReceiptNoState,
		 TicketNoFromState,
		// TicketNoToState, recalculated when TicketNoFromState or CartonQtyState changes
		 CompletedState,
		]
	 );

	const { register, handleSubmit, errors, formState, reset } = useForm<FormData>();


	const submitForm = async (data: FormData, e: any) => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Submission starting", oneECB603form);

		const response = await allServices.PTTSiudServices.iudECB603(oneECB603form);
		// response is the new receipt number
		if (!response) {
			alert("I am having trouble with saving ECB603 values.");
			// any error message is in the CustomerOrderNo property in the controller
		}
		else {
			history.push('/PTTS/existingECB603/' + response.data);
        }
 		e.target.reset(); // reset after form submit
	};

	const onCancel = () => {
		history.push('/PTTS/home');
	};

	const handleOnChange = (e: any) => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("handle on change event")
		switch (e.target.name) {
			case "fldCartonNoFrom": setCartonNoFromState(e.target.value); break;
			case "fldCartonNoTo":
				setCartonNoToState(e.target.value);
				break;
			case "fldCartonQty":
				let dummy1: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				setCartonQtyState(dummy1);
				let dummy3: number = Number.parseInt(TicketNoFromState.toString());
				let updatedTktNo = dummy1 + Number.parseInt(TicketNoFromState.toString()) - 1;
				dummy3 > 9999999 && dummy1 > 0 ? setTicketNoToState(updatedTktNo) : dummy1 = 0;
				break;
			case "fldCreatedBy": setCreatedByState(e.target.value); break;
			// case "fldCreatedOn": setCreatedOnState(e.target.value); break;
			case "fldCustomerOrderNo": setCustomerOrderNoState(e.target.value); break;
			case "fldDS1983ReceiptNo": setDS1983ReceiptNoState(e.target.value); break;
			case "fldFactoryJobNo": setFactoryJobNoState(e.target.value); break;
			case "fldID": setIDState(e.target.value); break;
			case "fldIsVoid": setIsVoidState(e.target.value); break;
			case "fldModifiedBy": setModifiedByState(e.target.value); break;
			case "fldModifiedOn": setModifiedOnState(e.target.value); break;
			case "fldNoOfParts": setNoOfPartsState(e.target.value); break;
			case "fldOrderNo": setOrderNoState(e.target.value); break;
			case "fldReceiptNo": setReceiptNoState(e.target.value); break;
			case "fldTicketNoFrom":
				let dummy2: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				setTicketNoFromState(dummy2);
				let updatedTktNo2 = dummy2 + Number.parseInt(CartonQtyState.toString()) - 1;
				dummy2 > 9999999 && CartonQtyState > 0 ? setTicketNoToState(updatedTktNo2) : dummy2 = 0;
				break;
			// case "fldTicketNoTo": setTicketNoToState(e.target.value); break;
			case "fldCompleted": setCompletedState(e.target.value); break;


			default:
			// code block
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.value);
		}
    }

	const newECB603 = () => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("newECB603 fired");
		setCartonNoFromState(0);
		setCartonNoToState(0);
		setCartonQtyState(0);
		setCreatedByState("currentUserToDo");
		setCreatedOnState(PTTSfunctions.Commonfunctions.todaysdate);
		setCustomerOrderNoState("11 digits");
		setDS1983ReceiptNoState("null"); // 3YYYYMMDDL1400000#####"
		setFactoryJobNoState("8");
		setIDState(0);
		setIsVoidState(false);
		setModifiedByState("currentUserToDo");
		setModifiedOnState(PTTSfunctions.Commonfunctions.todaysdate);
		setNoOfPartsState(0);
		setOrderNoState("0#-######-A (Vanguard#)");
		setReceiptNoState("6YYYYMMDDENHQ0000#####");
		setTicketNoFromState(1);
		setTicketNoToState(1000);
		setCompletedState(false);
	};

	const classes = useStyles();

	if (isLoading) {
		return (<div>Loading ...</div>);
	}
	else {

		const renderActionButtons = () => {
			if (!isNullOrUndefined(oneECB603form.DS1983ReceiptNo)) // prevent save/update
			{
				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							type="submit"
						>
							Save
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="secondary"
							onClick={() => newECB603()}
						>
							Reset
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}
			else if (PTTSfunctions.Commonfunctions2.isNotDefined(oneECB603form.DS1983ReceiptNo)) // prevent save/update
			{
				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={() => newECB603()}
						>
							Reset
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}
			else {

				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							type="submit"
						>
							Save
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={() => newECB603()}
						>
							Reset
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}
		};

		return (
			<form onSubmit={handleSubmit(submitForm)}>
				<Grid className={classes.formposition}>
					<Grid container className={classes.darkborder}>
						<Grid item xs={2} className={classes.styleforlogo}>
							<img src={PTTSfunctions.Logos.DSNYLogo.logoImg} alt="Sanitation Enforcement" />
						</Grid>
						<Grid item xs={9} className={classes.styleforformtitle}>
							<div className={classes.styleforformtitletext}>THE CITY OF NEW YORK Department of Sanitation</div>
							<div className={classes.styleforformtitletext}>NOTICE OF VIOLATION & HEARING Form ECB603</div>
						</Grid>
					</Grid>
					<div>&nbsp;</div>
					<div>&nbsp;</div>
					<Grid container>
						<Grid item xs={12}>
							{renderActionButtons()}
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={11} className={classes.styleforformreceipt}>
							<div className={classes.styleforformreceipttext}>RECEIPT NUMBER</div>
							<div className={classes.styleforformreceipttext}>{oneECB603form.ReceiptNo}</div>
						</Grid>

					</Grid>
				</Grid>
				<Grid className={classes.formposition}>
					<Grid container className={classes.darkborder}>
						<Grid item xs={2} className={classes.styleforformreceipt}>
							<div className={classes.styleforformreceipttext}>No. of Parts<br/></div>
							<div className={classes.styleforformreceipttext}>
								<input type="number" onChange={(e) => handleOnChange(e) } name="fldNoOfParts" ref={register}
									value={oneECB603form.NoOfParts}
								/>
							</div>
						</Grid>
						<Grid item xs={8} className={classes.styleforformtitle}>
							<div className={classes.styleforformtitletext}>NOTICE OF VIOLATION & HEARING Form ECB603</div>
						</Grid>
						<Grid item xs={2} className={classes.styleforformtitle}>
							<div className={classes.styleforformreceipttext}>QTY. THIS CARTON</div>
							<div className={classes.styleforformreceipttext}>
								<input type="number" onChange={(e) => handleOnChange(e) } name="fldCartonQty" ref={register}
									value={oneECB603form.CartonQty}
								/>
							</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={5} className={classes.styleforformorder}>
							<div className={classes.styleforformordertext}>First Ticket Number</div>
							<div className={classes.styleforformordertext}>
								<input type="text" onChange={(e) => handleOnChange(e) } name="fldTicketNoFrom" ref={register}
									value={oneECB603form.TicketNoFrom}
								/>
							</div>
						</Grid>
						<Grid item xs={5} className={classes.styleforformorder}>
							<div className={classes.styleforformordertext}>Last Ticket Number (r/o)</div>
							<div className={classes.styleforformordertext}>
								<input type="text" onChange={(e) => handleOnChange(e)} name="fldTicketNoTo" ref={register}
									value={oneECB603form.TicketNoTo}
								/>
							</div>
						</Grid>
						<Grid item xs={2} className={classes.styleforformorder}>
							<div className={classes.styleforformreceipttext}>CARTON NUMBER</div>
							<div className={classes.styleforformreceipttext}>
								<input type="number" onChange={(e) => handleOnChange(e)} name="fldCartonNoTo" ref={register}
									value={oneECB603form.CartonNoTo}
								/>
								<br />of<br />
								<input type="number" onChange={(e) => handleOnChange(e)} name="fldCartonNoFrom" ref={register}
									value={oneECB603form.CartonNoFrom}
								/>
							</div>
						</Grid>

					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={10} className={classes.styleforformorder}>
							<div className={classes.styleforformordertext}>OUR ORDER NO.</div>
							<div className={classes.styleforformordertext}>
								<input type="text" onChange={(e) => handleOnChange(e) } name="fldOrderNo" ref={register}
									value={oneECB603form.OrderNo}
								/>
							</div>
							<div className={classes.styleforformordertext}>CUSTOMER ORDER NO.</div>
							<div className={classes.styleforformordertext}>
								<input type="text" onChange={(e) => handleOnChange(e)} name="fldCustomerOrderNo" ref={register}
									value={oneECB603form.CustomerOrderNo}
								/>
							</div>
						</Grid>
						<Grid item xs={2} className={classes.styleforformorder}>
							<div className={classes.styleforformreceipttext}>FACTORY JOB NO.</div>
							<div className={classes.styleforformreceipttext}>
								<input type="text" onChange={(e) => handleOnChange(e) } name="fldFactoryJobNo" ref={register}
									value={oneECB603form.FactoryJobNo}
								/>
							</div>
						</Grid>

					</Grid>
					<Grid item xs={12}>
						{renderActionButtons()}
					</Grid>
				</Grid>

			</form>
		)
	}

};


const useStyles = makeStyles({
	root: {
		flexGrow: 1,
		height: "100%",
		paddingLeft: 15,
		paddingRight: 15,

	},
	formposition: {
		marginLeft: 280,
	},
	darkborder: {
		borderWidth: 3,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	styleforlogo: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforsubmit: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforformtitle: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
	},
	styleforformtitletext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "black",
	},
	styleforformorder: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "center",
	},
	styleforformordertext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "center",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 500,
		color: "black",
	},
	styleforformreceipt: {
		verticalAlign: "center",
		borderTopStyle: "none",
	},
	styleforformreceipttext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		display: "inline",
		marginLeft: 10,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
		textAlign: "center",
	},
	button: {
		marginTop: 20,
	},

	formstyle: {
		maxWidth: "100%",
		padding: 0,
	},

	input: {
		display: "block",
		borderRadius: 4,
		border: "1px solid",
		padding: "10px 15px",
		marginBottom: 10,
		fontSize: 14,
		maxWidth: 400,
		minWidth: 200,
	},

	reactselect: {
		maxWidth: 400,
	},

	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginBottom: 13,
		marginTop: 20,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
	},
	buttonSubmit: {
		background: "#0e8c1b",
		color: "white",
		border: "none",
		marginTop: 20,
		padding: 20,
		fontSize: 16,
		fontWeight: 100,
		width: 200,
	}
});

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);
