import { useMemo } from "react";
import { useDispatch } from "react-redux";
import { bindActionCreators } from "redux";

import { INITIAL_STATE as INITDS1704 } from "../reducers/DS1704";
import { INITIAL_STATE as INITDS21CARD } from "../reducers/DS21Card";
import { INITIAL_STATE as INITDS21HDR } from "../reducers/DS21HDR";
import { INITIAL_STATE as INITLOGINAD } from "../reducers/LoginADreducer";
import { allActions as NTIacts } from './actionsNTIprofile';
import { allActions as DS1983acts } from './DS1983';
import { allActions as DS1984acts } from './DS1984';
import { allActions as DS21acts } from './DS21';
import { allActions as ECB603acts } from './ECB603';
import { allActions as DS1704acts } from './DS1704';
import { allActions as LoginADacts } from './LoginAD';

export * from './DS1983';
export * from './DS1984';
export * from './DS1986';
export * from './DS21';
export * from './ECB603';
export * from './DS1704';
export * from './LoginAD';
export * from './reference';

export const allActions = {
	LoginADacts,
	ECB603acts,
	DS1704acts,
	DS1983acts,
	DS1984acts,
	DS21acts,
	NTIacts, 
}

export function useActions(actions: any, deps?: any): any {
	const dispatch = useDispatch();
	return useMemo(
		() => {
			if (Array.isArray(actions)) {
				return actions.map(a => bindActionCreators(a, dispatch));
			}
			return bindActionCreators(actions, dispatch);
		},
	    [dispatch,actions]
		// deps ? [dispatch, ...deps] : deps
	);
}

const intialState = {
	LOGINAD: INITLOGINAD,
	DS1704: INITDS1704,
	DS21HDR: INITDS21HDR,
	DS21CARD: INITDS21CARD
};


interface oneActionType<T, P> {
	type: T;
	payload: P;
}

