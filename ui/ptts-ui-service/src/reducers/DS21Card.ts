﻿// DS21Card is a set of 25 NOVs and one row of DS21HDR appearing as the attributes in the bottom of the page
import _ from 'lodash';
import { Action } from 'redux';
import { KnownActionDS21Card } from '../actions/DS21';
import { PTTSfunctions } from '../components/basicComponents';
import { tBulkUpdateRow, DS21CardActions, tDS21Card, tDS21_TicketStatus, tOneNOV, tSpecialDistrict, tTicketServiceType, tViolationCode } from '../model';

type KnownAction = KnownActionDS21Card;

export type tInterForInitialState = { // all caps is signal to later developers to not change this value
	ST_25NOVS: tOneNOV[];
	ST_20BulkUpdateTheseNOVs: tBulkUpdateRow[];
	ST_DS21HDRATTRIBUTES: tDS21Card;
	ST_DS21CardMSG: string;
	ST_DS21_MENU_POC: tSpecialDistrict[];
	ST_DS21_MENU_TST: tTicketServiceType[];
	ST_DS21_MENU_VC: tViolationCode[];
	ST_ATTRIBUTESOFONENOV: tOneNOV;
};

const inThisInstant: any = new Date().toLocaleDateString();
const todaysDate: Date = new Date();
const JulianZero: Date = new Date(0);

export const dummyBulkUpdateThisNOV: tBulkUpdateRow = Object.freeze({
	Id: 1,
	NOVnumber: "",
	Message: "",
	NOVnumberIsValid: false,
	TicketStatusID: -10,
})

export const dummyDSspecialDistrict: tSpecialDistrict = {
	Id: 0,
	PayCode: "",
	Description: "",
	Remarks: "",
	SplinterDisrtict: false,
	MappingDistictNTI: "",
}

export const dummyViolationCode: tViolationCode = {
	Id: 0,
	ViolationCode1: "",
	ReportMappingId: 0,
}

export const dummyServiceType: tTicketServiceType = {
	Id: 0,
	ViolationType1: "",
	SortOrder: 0,
}

export const dummyTicketStatusOption: tDS21_TicketStatus = {
	ID: 0,
	Description: "",
	OrderByThisTinyInt: 0,
}

export const dummyNOV: tOneNOV = {
	ECBSummonsNumber: "",
	Code: "",
	DateServedMonth: 0,
	DateServedDay: 0,
	DistOfOccurrence: "",
	Initial: "",
	Title: "",
	DS1984Number: "",
	Notarized: false,
	Voided: false,
	ModifiedOn: inThisInstant,
	ModifiedBy: "",
	TicketStatusID: 0,
	ViolationType: "",
	CreatedBy: "",
	CreatedOn: inThisInstant,
	DateServedYear: 0,
	DS21_TicketStatus: { ID: 0, Description: "dummy", OrderByThisTinyInt: 0},
}

export const dummyDS21Card: tDS21Card = {
	ID: 0, // SQL Server Table row ID 
	IssuingOfficerSignature: "",
	Command: "",
	IssuingOfficerName: "",
	TaxRegNumber: "",
	Agency: 0,
	DateCardStarted: JulianZero,
	DateReturned: JulianZero,
	AssignedDistrict: "",
	IssuingOfficerTitle: "",
	OfficerInChargeName: "",
	OfficerInChargeTitle: "",
	DateCardCompleted: JulianZero,
	Remarks: "",
	DS1984Number: "",
	DateCompleted_MM: "00", // left paded with zeroes
	DateCompleted_DD: "00", // left paded with zeroes
	ModifiedOn: todaysDate,
	ModifiedBy: "",
	CreatedBy: "",
	CreatedOn: todaysDate,
	FormStatusID: -1,
	//DateCardStartedYYYYMMDD: formatDateYYYYmmDD(JulianZero),
	//DateReturnedYYYYMMDD: formatDateYYYYmmDD(JulianZero),
	//DateCardCompletedYYYYMMDD: formatDateYYYYmmDD(JulianZero),
	//ModifiedOnYYYYMMDD: formatDateYYYYmmDD(JulianZero),
	//CreatedOnYYYYMMDD: formatDateYYYYmmDD(JulianZero),

}

export const INITIAL_STATE: tInterForInitialState = { // all caps is signal to later developers to not change this value
	ST_25NOVS: [],
	ST_20BulkUpdateTheseNOVs: [{
		Id: 1,
		NOVnumber: "",
		Message: "",
		NOVnumberIsValid: false,
		TicketStatusID: -9,
	}],
	ST_DS21HDRATTRIBUTES: dummyDS21Card,
	ST_DS21CardMSG: '',
	ST_DS21_MENU_POC: [],
	ST_DS21_MENU_TST: [],
	ST_DS21_MENU_VC: [],
	ST_ATTRIBUTESOFONENOV: dummyNOV,
};

 export type tDS21CardState = {
	readonly DS21CardReducer: tInterForInitialState;
 };


	//UPDATE_DS21 = "UPDATE_DS21",
	//COMPLETE_DS21 = "COMPLETE_DS21",
	//REVERT2INCOMPLETE_DS21 = "REVERT2INCOMPLETE_DS21",
	//DS21_CARD_TABLE = "NOVs_For_One_DS21Card",
	//MSG = 'SYSTEM_MSG_TO_USER', // some sort of exception

export const DS21CardReducer = (state: tInterForInitialState
	| undefined | any = INITIAL_STATE
	, incomingAction: Action) => {

	const action = incomingAction as KnownAction;
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("giantReducer hit. Looking for " + action.type);

	switch (action.type) {
		case DS21CardActions.RPL_BulkUpdateTheseNOVs:
			const RPL_ap: tBulkUpdateRow[] = action.payload; // assume arr at least one memeber
			return {
				...state,
				ST_20BulkUpdateTheseNOVs: RPL_ap
			}
		case DS21CardActions.UPT_BulkUpdateThisNOV: // iterate the first NOVnumber as it builds toward a valid NOV#
			const UPT_ap: tBulkUpdateRow = action.payload; // assume arr has only one memeber
			try {
				if (state.ST_20BulkUpdateTheseNOVs.length === 1
					&& state.ST_20BulkUpdateTheseNOVs[0].NOVnumber === UPT_ap[0].NOVnumber
					&& UPT_ap[0].Id === 1) { // the first item is supposed to be an empty string for NOVnumber
					// when the first item also has an empty string, just return state
					return {
						...state
					}
				}
				else if (state.ST_20BulkUpdateTheseNOVs[0].NOVnumber !== UPT_ap[0].NOVnumber
					&& UPT_ap[0].Id === 1 && !UPT_ap[0].NOVnumberIsValid) {
					// the first item is supposed to be an empty string or partical NOVnumber
					const arrUpdated: tBulkUpdateRow[] = state.ST_20BulkUpdateTheseNOVs.filter((r: tBulkUpdateRow) => r.Id !== UPT_ap[0].Id);
					arrUpdated.unshift(UPT_ap[0]);
					return {
						...state,
						ST_20BulkUpdateTheseNOVs: arrUpdated
					}
				}
				else if (state.ST_20BulkUpdateTheseNOVs[0].NOVnumber !== UPT_ap[0].NOVnumber
					&& UPT_ap[0].Id === 1 && UPT_ap[0].NOVnumberIsValid) {
					// the first item is supposed to be an empty string for NOVnumber
					// once a valid NOVnumber is entered
					const arrUpdated: tBulkUpdateRow[] = state.ST_20BulkUpdateTheseNOVs.filter((r: tBulkUpdateRow) => r.Id !== UPT_ap[0].Id);
					arrUpdated.concat(UPT_ap[0]);
					arrUpdated.unshift(dummyBulkUpdateThisNOV);
					return {
						...state,
						ST_20BulkUpdateTheseNOVs: arrUpdated
					}
				}
				else if (UPT_ap[0].NOVnumber === "") {
					const arrDefault: tBulkUpdateRow[] = state.ST_20BulkUpdateTheseNOVs;
					arrDefault.unshift(UPT_ap[0]);
					return { // replace the first member with UPT_ap[0]; first member ought to be the one with "" for NOVnumber
						...state,
						ST_20BulkUpdateTheseNOVs: arrDefault
					}
				}
				else {
					return { ...state }
				};
			}
			catch (error) {
				alert(error.toString());
				return { ...state }
            }
		case DS21CardActions.RST_BulkUpdateTheseNOVs:
			return {
				...state
				, ST_20BulkUpdateTheseNOVs: INITIAL_STATE.ST_20BulkUpdateTheseNOVs
			}
		case DS21CardActions.ADD_BulkUpdateTheseNOVs:
			const ADD_ap: tBulkUpdateRow = action.payload;
			const lnth: number = state.ST_20BulkUpdateTheseNOVs.length;
			if (ADD_ap.NOVnumberIsValid) {
				let newST: tBulkUpdateRow[] = [];
				if (lnth > 1) {
					newST = state.ST_20BulkUpdateTheseNOVs.filter((r: tBulkUpdateRow) => r.Id !== ADD_ap.Id)
				};
				if (newST.length > 1) {
					newST.concat(ADD_ap)
				}
				else { newST.push(ADD_ap) };
				newST.unshift({ // workaround for push object into array is by ref in javascript; need a copy
					Id: dummyBulkUpdateThisNOV.Id,
					NOVnumber: dummyBulkUpdateThisNOV.NOVnumber,
					Message: dummyBulkUpdateThisNOV.Message,
					NOVnumberIsValid: dummyBulkUpdateThisNOV.NOVnumberIsValid,
					TicketStatusID: dummyBulkUpdateThisNOV.TicketStatusID
				}); // initialize first NOV# value in the list
				const newSTlgth: number = newST.length;
				for (let i: number = 0; i < newSTlgth; i++) {
					newST[i].Id = i+1
				}

				return {
					...state,
					ST_20BulkUpdateTheseNOVs: newST
				};
			};
			if (lnth === 0) {
				return {
					...state,
					ST_20BulkUpdateTheseNOVs: state.ST_20BulkUpdateTheseNOVs.concat(ADD_ap[0])
				};
			};
			return { ...state };
		 case DS21CardActions.DEL_BulkUpdateTheseNOVs:
			const oneRowDEL: tBulkUpdateRow | any = action.payload;
			const arrRemaining: tBulkUpdateRow[] = state.ST_20BulkUpdateTheseNOVs.filter((r: tBulkUpdateRow) => r.NOVnumber !== oneRowDEL.NOVnumber );
			const arrlgth: number = arrRemaining.length;
			for (let i: number = 0; i < arrlgth; i++) {
				arrRemaining[i].Id = i + 1
			}
			return {
				...state,
				ST_20BulkUpdateTheseNOVs: arrRemaining 
			};
		//	return _.omit(state.ST_20BulkUpdateTheseNOVs, action.payload.id); // omit takes care of making the copy of state
		case DS21CardActions.aDS21_MENU_POC:
			return {
				...state, ST_DS21_MENU_POC: action.payload
			};
		case DS21CardActions.aDS21_MENU_TST:
			return {
				...state, ST_DS21_MENU_TST: action.payload
			};
		case DS21CardActions.aDS21_MENU_VC:
			return {
				...state, ST_DS21_MENU_VC: action.payload
			};
		case DS21CardActions.UPDATE_DS21CARD: // to coordinate values with the DS21s listed for each DS1984
			return {
				...state, ST_DS21HDRATTRIBUTES: action.payload
			};
		case DS21CardActions.aDS21_CARDNOV_TABLE:
			return {
				...state, ST_25NOVS: action.payload
			};
		case DS21CardActions.CardMSG:
			{
				return { ...state, ST_DS21CardMSG: "landed in default of the DS21Card reducer" }
			}
		//case DS21CardActions.REVERT2INCOMPLETE_DS21:
		//	{
		//		return { ...state, ST_CARDCOMPLETE: action.payload }
  //          }
		case DS21CardActions.UPDATE_NOV:
			return {
				...state, ST_ATTRIBUTESOFONENOV: action.payload
            }
		default:
			  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("landed in default of the DS21Card reducer");
			return { ...state };

    }
};


