/* tslint:disable*/
import * as React from 'react';

class Loader extends React.Component<any,any> {
    render(){
        return(
            <div className="loader center">
                working...<i className="fa fa-cog fa-spin" />
            </div>
        );
    }
}
export default Loader;
