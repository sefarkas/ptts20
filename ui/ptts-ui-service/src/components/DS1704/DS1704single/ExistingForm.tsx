﻿// prettier-ignore
import { ErrorMessage } from "@hookform/error-message";
import {
	Divider, FormControl, Grid, Input, InputLabel, Link, Paper
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import moment from "moment";
import * as React from "react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { allActions } from "../../../actions";
import { tDS1704, tDS21HDR, tOneNOV, UsedTicketRange, aDS1704rows } from "../../../model";
import { dummies } from "../../../reducers";
import { allServices } from '../../../services';
import { PTTSfunctions } from "../../basicComponents";
import Loader from "../../basicComponents/Loader";
import { ErrorMessageContainer, ErrorSummary } from "../../basicComponents/validation";

type FormData = {
	fldNOVnumber: number;
}



// this REACT component assumes
// 1. The DS21HDR row appropriate for selected NOV (ST_DS21HDRrowsFLTRD) is already in the REDUX Store.
// 2. this component is called from a page that already set up ST_ATTRIBUTESOFONENOV for the selected NOV.
// 3. This component is set up to show existing DS1704, or a blank to make a new one.
export function DS1704Asingle(props: any) {

	const classes = useStyles();
	const dispatch = useDispatch();

	let history = useHistory();
	const { register, handleSubmit, errors, setError } = useForm<FormData>();

	const [NOVnumberState, setNOVnumberState] = useState(props.aNovNumber); // match.params because of using ROUTER to get here

	const contentparentstate: any = useSelector((state: any) => state);
	const contentparent: any = useSelector((state: any) => state.DS21CardReducer);
	const [dsAttributesOfOneNOV, setdsAttributesOfOneNOV] = useState<tOneNOV>(contentparent.ST_ATTRIBUTESOFONENOV);
	console.log("contentparent of DS1704Asingle");
	console.log(contentparent);
	const [oneDS21HDRrow, setOneDS21HDRrow] = useState<tDS21HDR>(!contentparent.ST_DS21HDRATTRIBUTES ? dummies.dummyDS21HDR : contentparent.ST_DS21HDRATTRIBUTES);
	const [oneDS1704, setoneDS1704] = useState(contentparentstate.DS1704Reducer.ST_DS1704rows);

	const [IsValidNOVnumberState, setIsValidNOVnumberState] = useState(false);
	const [isLoading, setLoading] = useState(false); // useEffect [] will test for legitimate NOV# and then try to pull DS1704 info
	const [DateServedState, setDateServedState] = useState("");

	React.useEffect(() => {
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("init useEffect: start");

		(async () => {
			try {
				if (oneDS1704.ECBsummonsNumber = NOVnumberState) {
					setIsValidNOVnumberState(true);
					setLoading(false);
				}
				else {
					const bRslt: boolean = await fIsValidNOVnumber("IsGoodNOVnumber", "High", NOVnumberState);
					setIsValidNOVnumberState(bRslt);
					if (bRslt) {
						alert(`NOV# passed to DS1704 single page is valid (${NOVnumberState})`);
					}
                }

			}
			catch {
				setLoading(true);
			}
		})();

		if (!(oneDS21HDRrow.BoxNumberedFrom <= NOVnumberState
				&& oneDS21HDRrow.To >= NOVnumberState 		)) {
			// update oneDS21HDRrow
			allActions.DS21acts.DS21HDRRelatedToOneDS1984List(dispatch, NOVnumberState);
        }

	}, [])

	React.useEffect(() => {

		// get a row from [PTTS20].[dbo].[DS1704] if one exists for NOVnumberState
		//if (isLoading) {
		//	(async () => {
		//		let rdata1704: tOneDS1704 = dummyDS1704;
		//		try {
		//			const response1 = await allServices.PTTSgetServices.oneDS1704(NOVnumberState); // axios always returns an object with a "data" child
		//			rdata1704 = response1.data;
		//			setoneDS1704(rdata1704);
		//			setLoading(false);
		//		}
		//		catch {
		//			rdata1704 = dummyDS1704;
		//			setLoading(true);
		//		}
		//		setoneDS1704(rdata1704);
		//	})();
		//}

		if (contentparent.ST_ATTRIBUTESOFONENOV === dummies.dummyNOV
			|| (contentparent.ST_ATTRIBUTESOFONENOV.ECBSummonsNumber !== NOVnumberState)) {
			(async () => {
				let rdata: tOneNOV = dummies.dummyNOV;
				try {
					//const response1 = await allServices.PTTSgetServices.oneNOV(NOVnumberState); // axios always returns an object with a "data" child
					//rdata = response1.data;
					//let rBoolean: boolean = false;
					//try {
					//	rBoolean = allActions.DS21acts.iudNOV(dispatch, rdata, loggedInUser())
					//	// rBoolean = await allServices.PTTSiudServices.iudNOV(dsAttributesOfOneNOV, loggedInUser()); // axios always returns an object with a "data" child

					//	const dte: Date = new Date(rdata.DateServedYear,
					//		rdata.DateServedMonth - 1,
					//		rdata.DateServedDay);
					//	const strDte: string = moment(dte.toUTCString()).format('YYYY-MM-DD');
					//	console.log("allServices.PTTSgetServices.oneNOV strDte: ", strDte);
					//	setDateServedState(strDte);
					//	setdsAttributesOfOneNOV(rdata); // replace NOV with details with newly retrieved

					//}
					//catch (error) {
					//	alert(error.toString());
					//	rBoolean = false;
					//}
					//if (rBoolean) {
					//	console.log("successful NOV update");
					//}
					//setIsValidNOVnumberState(rBoolean);

					allServices.PTTSgetServices.oneNOVwithCallBack(NOVnumberState, cb, er);

				}
				catch {
					rdata = dummies.dummyNOV;
					setIsValidNOVnumberState(false);
				}
			})();
		}

		if (contentparent.ST_ATTRIBUTESOFONENOV.ECBSummonsNumber !== dsAttributesOfOneNOV.ECBSummonsNumber) {

			const dte: Date = new Date(contentparent.ST_ATTRIBUTESOFONENOV.DateServedYear,
				contentparent.ST_ATTRIBUTESOFONENOV.DateServedMonth - 1,
				contentparent.ST_ATTRIBUTESOFONENOV.DateServedDay);
			const strDte: string = moment(dte).format('YYYY-MM-DD');
			console.log("strDte after NOV# changes: ", strDte);
			setDateServedState(strDte);

			setdsAttributesOfOneNOV(contentparent.ST_ATTRIBUTESOFONENOV);
		}

	}, [isLoading]);

	React.useEffect(() => {
		console.log("useEffect for valid NOV", IsValidNOVnumberState)
		setLoading(!IsValidNOVnumberState);
	}, [IsValidNOVnumberState])

	React.useEffect(() => {

		const dte: Date = new Date(dsAttributesOfOneNOV.DateServedYear,
			dsAttributesOfOneNOV.DateServedMonth - 1,
			dsAttributesOfOneNOV.DateServedDay);
		const strDte: string = moment(dte).format('YYYY-MM-DD');
		console.log("strDte for dsAttributesOfOneNOV change: ", strDte);
		setDateServedState(strDte);

	}, [dsAttributesOfOneNOV.ECBSummonsNumber,
	dsAttributesOfOneNOV.DateServedYear,
	dsAttributesOfOneNOV.DateServedMonth,
	dsAttributesOfOneNOV.DateServedDay,
	])

	const cb = (response: any) => {
		let rdata: tOneNOV = dummies.dummyNOV;
		rdata = response.data;  // axios always returns an object with a "data" child
		let rBoolean: boolean = false;
		try {
			rBoolean = allActions.DS21acts.iudNOV(dispatch, rdata, loggedInUser())
			// rBoolean = await allServices.PTTSiudServices.iudNOV(dsAttributesOfOneNOV, loggedInUser()); // axios always returns an object with a "data" child

			const dte: Date = new Date(rdata.DateServedYear,
				rdata.DateServedMonth - 1,
				rdata.DateServedDay);
			const strDte: string = moment(dte.toUTCString()).format('YYYY-MM-DD');
			console.log("allServices.PTTSgetServices.oneNOV strDte: ", strDte);
			setDateServedState(strDte);
			setdsAttributesOfOneNOV(rdata); // replace NOV with details with newly retrieved

		}
		catch (error) {
			alert(error.toString());
			rBoolean = false;
		}
		if (rBoolean) {
			console.log("successful NOV update");
		}
		setIsValidNOVnumberState(rBoolean);

	}

	const er = (response: any) => {
		alert("DS1704Asingle/er: " + response);
	}

	function loggedInUser(): string {
		return (localStorage.getItem('userid') + "").toString(); // to avoid possibly null error
		//const j: any = JSON.parse(curUser);
		//return j.data.userPreferences.userName.toString();
	}

	function handleCreateDS1704A(aDS1704: aDS1704rows) {
		// alert("CRUD dbo.DS1704");
		console.log("CRUD dbo.DS1704 with ", aDS1704);
		if (aDS1704.ReasonForVoid === null || aDS1704.ReasonForVoid.length < 2) {
			alert("PTTS needs you to choose a reason for the void from the drop down.");
			return;
		}
		const DS1704row: tDS1704 = {
			ECBSummonsNumber: dsAttributesOfOneNOV.ECBSummonsNumber,
			TimeServed: new Date(),
			PlaceOfOccurence: aDS1704.Address,
			Remarks: "",
			ModifiedBy: loggedInUser(),
			ModifiedOn: new Date(), // determined by SPROC [dbo].[DS1704_InsertUpdateRecord]
			VoidDate: new Date(),
			VoidReasonId: parseInt(aDS1704.ReasonForVoid),
			CreatedBy: loggedInUser(), // determined by SPROC [dbo].[DS1704_InsertUpdateRecord]
			CreatedDate: new Date(), // determined by SPROC [dbo].[DS1704_InsertUpdateRecord]
			IsActive: true, // determined by SPROC [dbo].[DS1704_InsertUpdateRecord]
			ApprovedBy: "", // determined by SPROC [dbo].[DS1704_InsertUpdateRecord]
			District: "", // determined by SPROC [dbo].[DS1704_InsertUpdateRecord]
		}
	};

	function handleGetDS21CardForThisDS1704(rcpt: string) {
		history.push('/PTTS/DS21Card/' + rcpt)
	}


	function handleGetNOVForThisDS1704(NOVno: string) {
		history.push('/PTTS/NOVdetails/' + NOVno)
	}

	function fIsValidNOVnumber(aDSform: string, LowHigh: string, TgtVal: string): boolean {
		let rdata: UsedTicketRange[] | undefined = undefined;
		let pIsValidNOVnumber: boolean = false;

		const searchECBsummonsNumber: string =
			TgtVal.length < 9
				? PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(Number.parseInt(TgtVal))
				: TgtVal;
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Find a ticket range with NOV ", searchECBsummonsNumber);

		(async () => {
			const tryThisRange = (
				{
					ECBSummonsNumberFrom: LowHigh === "Low"
						? TgtVal
						: searchECBsummonsNumber,
					ECBSummonsNumberTo: LowHigh === "High"
						? TgtVal
						: searchECBsummonsNumber,
					DSform: aDSform,
					ReceiptNumber: "",
				}
			);
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("tryThisRange");
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange);
			pIsValidNOVnumber = false;
			const response = await allServices.PTTSgetServices.refUsedTicketNumbers(tryThisRange);
			// axios always returns an object with a "data" child
			rdata = await response.data;
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange)
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
			if (!rdata) {
				pIsValidNOVnumber = false;
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ticket number bad based on no rdata object.");
				setIsValidNOVnumberState(pIsValidNOVnumber);
			}
			else if (!rdata.length ? true : rdata.length === 0) {
				pIsValidNOVnumber = false;
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(`Ticket number ${searchECBsummonsNumber} bad based on empty [].`);
				setIsValidNOVnumberState(pIsValidNOVnumber);
			}
			else {
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("found a ticket range for ", searchECBsummonsNumber);
				pIsValidNOVnumber =
					Number.parseInt(tryThisRange.ECBSummonsNumberFrom) > 0
						&& Number.parseInt(tryThisRange.ECBSummonsNumberTo) > 0
						? true : false;
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber and non-zeroes", pIsValidNOVnumber);

				//if (!pIsValidNOVnumber) {
				//	if (!rdata![rdata.length - 1]) { // https://stackoverflow.com/questions/55190059/typescript-property-type-does-not-exist-on-type-never
				//		alert(" Cannot find a DS-1984 for this summons number.");
				//	}
				//}
				//else {
				//	alert("NOV Number is OKay.");
				//	setNOVnumberState(searchECBsummonsNumber);
				//}

				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber at end of ASYNC", pIsValidNOVnumber);
				setIsValidNOVnumberState(pIsValidNOVnumber);
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("IsValidNOVnumberState at end of ASYNC", IsValidNOVnumberState);
				return pIsValidNOVnumber;
			}

		})();

		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber after ASYNC", pIsValidNOVnumber);
		return pIsValidNOVnumber;

	};

	const handleLinkToParent1984 = (e: any) => {
		history.push('/PTTS/existingDS1984/' + e.target.textContent)
	}

	const handleOnChange = (e: any) => {
		e.preventDefault();

		switch (e.target.name) {

			case "fldNOVnumber":
				let dummy2n: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				if (isNaN(dummy2n)) {
					alert("NOV number is NaN.");
					break;
				}
				if (dummy2n > 299999999) {
					alert("Expected first summons number is supposed to start with 1 or 2 and be nine-digits.  You entered a number that is too big.")
					break;
				} // less than 8 digits, not-a-number, or nine or more digits starting with 3 (Sanitation only prefixes 1 or 2)
				//            12345678
				setNOVnumberState(dummy2n.toString());
				if (dummy2n > 99999999) {
					dummy2n = Math.trunc(dummy2n / 10);
					//const msg1: string = "check NOV number" + dummy2n.toString();
					//alert(msg1);
					const dummy2: string = PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(dummy2n);
					if (!fIsValidNOVnumber("IsGoodNOVnumber", "Low", dummy2)) {
						// true means validation is OKay, value is proper
						if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fIsValidNOVnumber has not found NOV number.")
					}
				}
				break;
			default:
				// code block
				const msg: string = "No onChange for " + e.target.name;
				alert(msg);
		}
	}


	if (isLoading) {
		return (<Loader />);
	}
	else {
		return (
			<Grid container>
				<Grid item xs={12} component={Paper} className={classes.paper}>
					<FormControl  >
						<ErrorSummary errors={errors} />

						<InputLabel htmlFor="idNOVnumber">{NOVnumberState.length < 9
							? <div>Search for this ECB Summons Number: {NOVnumberState} </div>
							: IsValidNOVnumberState
								? < div > {NOVnumberState} is valid; click 'Show ...' below.</div>
								: < div > {NOVnumberState} is not on any existing DS-1984.</div>}
						</InputLabel>
						<Input className={classes.styleforticketrangetext}
							type="number"
							id="idNOVnumber"
							onChange={(e) => handleOnChange(e)}
							placeholder="9-digit ECB Summons Number"
							name="fldNOVnumber"
							ref={register({
								required: { value: true, message: "PTTS expects the first NOV number (nine digits)." },
								min: {
									value: 100000000,
									message: "Too small. Individual ECB Summons are nine digit numbers starting with 1 or 2."
								},
								max: {
									value: 299999999,
									message: "Too big.  Individual ECB Summons are nine digit numbers starting with 1 or 2."
								},
							})}
							value={NOVnumberState}
						// defaultValue versus value --> value lets me change characters in the currently active control textbox
						/>
						<div>&nbsp;</div>
						<ErrorMessage
							errors={errors}
							name="fldNOVnumber"
							as={<ErrorMessageContainer />}
						/>
						{IsValidNOVnumberState
							?
							<button type="submit" onClick={(e) => { e.preventDefault(); alert('Submitted NOVnumber!'); }}>Show links to related forms for {NOVnumberState}</button>
							:
							<div>&nbsp;</div>
						}
						<Divider />
					</FormControl>
				</Grid>
				<Grid item xs={12} className={classes.formposition}>
					<Grid container className={classes.darkborder}>
						<Grid item xs={3} className={classes.styleforlogo}>
							<img src={PTTSfunctions.Logos.DSNYLogo.logoImg} alt="Sanitation Enforcement" />
						</Grid>
						<Grid item xs={9} className={classes.styleforformtitletext}>
							THE CITY OF NEW YORK DEPARTMENT OF SANITATION<br />
						REQUEST TO VOID SUMMONS DS 1704 (5-99)
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={12} >
							[Later]{NOVnumberState}
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={8}>
							[EMPTY]
						</Grid>
						<Grid item xs={2} className={classes.borderleft}>
							<div>VOID DATE</div>
							<div className={classes.styleforcontent}>[MM/DD/YYYY]</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={4} >
							<div className={classes.styleforlabel}>TO:</div>
							<div className={classes.styleforcontent}>(ADJUDICATING AGENCY - ECB - PVB)</div>
						</Grid>
						<Grid item xs={8} className={classes.borderleft}>
							<div className={classes.styleforlabel}>FROM:</div>
							<div className={classes.styleforcontent}>SUMMONS CONTROL UNIT</div>
							<div className={classes.styleforcontent}>1824 Shore Parkway</div>
							<div className={classes.styleforcontent}>Brooklyn, NY 11214 (718) 714-2779</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={12} component={Paper} >
							<div className={classes.styleforlabel}>SUBJECT:</div>
							<div className={classes.styleforcontent}>REQUEST TO VOID SUMMONS</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={3} component={Paper} >
							<div className={classes.styleforlabel}>SUMMONS NO.</div>
							<Link href="#" className={classes.styleforlink}
								onClick={() => handleGetNOVForThisDS1704(NOVnumberState)}>{NOVnumberState}</Link>
						</Grid>
						<Grid item xs={3} component={Paper} className={classes.borderleft}>
							<div className={classes.styleforlabel}>VIOLATION:</div>
							<div className={classes.styleforcontent}>{dsAttributesOfOneNOV.Code}</div>
						</Grid>
						<Grid item xs={6} component={Paper} className={classes.borderleft}>
							<div className={classes.styleforlabel}>ISSUING OFFICER / AGENT:</div>
							<div className={classes.styleforcontent}>{dsAttributesOfOneNOV.CreatedBy}</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={2} component={Paper} >
							<div className={classes.styleforlabel}>SERVED DATE</div>
							<div className={classes.styleforcontent}>{dsAttributesOfOneNOV.DateServedMonth + "/" + dsAttributesOfOneNOV.DateServedDay + "/" + dsAttributesOfOneNOV.DateServedYear}</div>
						</Grid>
						<Grid item xs={2} component={Paper} className={classes.borderleft}>
							<div className={classes.styleforlabel}>TIME SERVED</div>
							<div className={classes.styleforcontent}>{`N/A`}</div>
						</Grid>
						<Grid item xs={2} component={Paper} className={classes.borderleft}>
							<div className={classes.styleforlabel}>REGISTRY NO.</div>
							<div className={classes.styleforcontent}>{oneDS21HDRrow.TaxRegNumber}</div>
						</Grid>
						<Grid item xs={4} component={Paper} className={classes.borderleft}>
							<div className={classes.styleforlabel}>COMMAND</div>
							<div className={classes.styleforcontent}>{oneDS21HDRrow.AssignedDistrict}</div>
						</Grid>
						<Grid item xs={2} component={Paper} className={classes.borderleft}>
							<div className={classes.styleforlabel}>DISTRICT</div>
							<div className={classes.styleforcontent}>{oneDS1704.District}</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={12} component={Paper} >
							<div className={classes.styleforlabel}>PLACE OF OCCURRENCE</div>
							<div className={classes.styleforcontent}>{oneDS1704.PlaceOfOccurence}</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={12} component={Paper} >
							<div className={classes.styleforlabel}>REMARKS</div>
							<div className={classes.styleforcontent}>{oneDS1704.Remarks}</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={4} component={Paper} >
							<div className={classes.styleforlabel}>ISSUING OFFICER / AGENT</div>
							<div className={classes.styleforcontent}>{oneDS21HDRrow.IssuingOfficerName}</div>
						</Grid>
						<Grid item xs={4} component={Paper} className={classes.borderleft}>
							<div className={classes.styleforlabel}>APPROVED BY</div>
							<div className={classes.styleforcontent}>{oneDS1704.ApprovedBy}</div>
						</Grid>
						<Grid item xs={4} component={Paper} className={classes.borderleft}>
							<div className={classes.styleforlabel}>TITLE</div>
							<div className={classes.styleforcontent}>[LATER]</div>
						</Grid>
					</Grid>
				</Grid>

			</Grid>


		)

	}			 // each voided NOV has one associated DS1704A; this is the UI creating the DS1704A record

}

const useStyles = makeStyles({
	styleforformtitle: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
	},
	styleforformtitletext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "black",
	},
	styleforlabel: {
		marginBottom: 5,
		marginTop: 0,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 0,
		fontSize: 12,
		fontWeight: 300,
		color: "black",
	},
	styleforcontent: {
		marginBottom: 13,
		marginTop: 0,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 0,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
	},
	styleforlink: {
		marginBottom: 13,
		marginTop: 0,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 0,
		fontSize: 14,
		fontWeight: 500,
		color: "blue",
		'&:hover': {
			color: "hotpink",
		},
		'&:visited': {
			color: "green",
		},
		'&:active': {
			color: "blue",
		},
	},
	styleforlogo: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	darkborder: {
		borderWidth: 1,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	borderleft: {
		borderLeftWidth: 1,
		borderLeftStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	formposition: {
		marginLeft: 10,
		marginBottom: 20,
	},
	paper: {
		width: "100%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 360,
		overflowX: "visible",
	},
	table: {
		width: "100%",
		margingTop: 80,
		minWidth: 1000,
		display: "inline-block",
	},
	cell: {
		minWidth: 100,
		maxWidth: 220,
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 100,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 550,
		minWidth: 300,
	}
});
