﻿// prettier-ignore
import { ErrorMessage } from "@hookform/error-message";
import {
	Divider, FormControl, Grid, Input, InputLabel, Link, Paper, Table, TableBody, TableCell,
	TableHead, TableRow
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { allServices } from '../../../services';
import { tOneNOV, UsedTicketRange, tDS1704, DS1984, aDS1704rows } from "../../../model";
import { PTTSfunctions } from "../../basicComponents";
import { ErrorSummary, ErrorMessageContainer } from "../../basicComponents/validation";
import Loader from "../../basicComponents/Loader";
import moment from "moment";
import { dummies } from "../../../reducers";

type FormData = {
	fldNOVnumber: number;
}

export function DS1704Atable(props: any) { // expect a table of with 20 rows of DS1704As for up to NOV#s.

	const classes = useStyles();
	const dispatch = useDispatch();
	const { register, handleSubmit, errors, setError } = useForm<FormData>();
	const contentparent1984: any = useSelector((state: any) => state.DS1984Reducer);

	let history = useHistory();

	const [lstOfDS1704s, setlstOfDS1704s] = useState<aDS1704rows[]>([]);

	const [NOVnumberState, setNOVnumberState] = useState(!props.aNovNumber ? "" : props.aNovNumber); // match.params because of using ROUTER to get here
	const [DS1984rcpt, setDS1984rcpt] = useState(!props.DS1984rcpt ? "" : props.DS1984rcpt);
	const [oneDS1984, setDS1984] = useState<DS1984>(!contentparent1984.ST_DS1984rows ? dummies.dummyDS1984 : contentparent1984.ST_DS1984rows);

	const contentparent: any = useSelector((state: any) => state.DS21CardReducer);
	const [dsAttributesOfOneNOV, setdsAttributesOfOneNOV] = useState<tOneNOV>(contentparent.ST_ATTRIBUTESOFONENOV);

	const [IsValidNOVnumberState, setIsValidNOVnumberState] = useState(false);
	const [isLoading, setLoading] = useState(true);
	const [DateServedState, setDateServedState] = useState("");

	React.useEffect(() => {
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS1704Atable init useEffect: start");
		// GetUsedTicketRange

		if (DS1984rcpt.length > 9) {
			// get the DS1984 and use its first NOV# as NOVnumberState
			allServices.PTTSgetServices.anDS1984withCallBack(DS1984rcpt, cb, er);

		}
		else {
			const bRslt: boolean = fIsValidNOVnumber("IsGoodNOVnumber", "High", NOVnumberState);
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("init useEffect bRslt:", bRslt);

			const lcllstOfDS1704s: any = [{
				id: 1,
				POC: "", // place of occurrence -- district issuing ticket
				NOVnumber: 1 === 1 ? (!NOVnumberState || NOVnumberState.length < 8 ? "" : NOVnumberState) : "",
				NOVdate: moment(new Date().toUTCString()).format("YYYY-MM-DD"), // YYYY-MM-DD
				Address: "", // where ticket was issued to respondent
				ReasonForVoid: "", // selection made from the dropdown list
				VerifiedBy: "", // uid of the supervisors who authorized the void
			}];
			// build 19 more rows of lstDS1704 without NOV# etc. filled in
			for (var i = 2; i <= 20; i++) {
				const aRow: aDS1704rows = {
					id: i,
					POC: "", // place of occurrence -- district issuing ticket
					NOVnumber: i === 1 ? NOVnumberState : "",
					NOVdate: moment((new Date()).toUTCString()).format("YYYY-MM-DD"), // YYYY-MM-DD
					Address: "", // where ticket was issued to respondent
					ReasonForVoid: "", // selection made from the dropdown list
					VerifiedBy: "", // uid of the supervisors who authorized the void
				};
				lcllstOfDS1704s.push(aRow);
			}

			setlstOfDS1704s(lcllstOfDS1704s);
        }

	}, []);

	React.useEffect(() => {
		const lcllstOfDS1704s: any = [{
			id: 1,
			POC: "", // place of occurrence -- district issuing ticket
			NOVnumber: 1 === 1 ? (!NOVnumberState || NOVnumberState.length < 8 ? "" : NOVnumberState) : "",
			NOVdate: moment(new Date().toUTCString()).format("YYYY-MM-DD"), // YYYY-MM-DD
			Address: "", // where ticket was issued to respondent
			ReasonForVoid: "", // selection made from the dropdown list
			VerifiedBy: "", // uid of the supervisors who authorized the void
		}];
		// build 19 more rows of lstDS1704 without NOV# etc. filled in
		for (var i = 2; i <= 20; i++) {
			const aRow: aDS1704rows = {
				id: i,
				POC: "", // place of occurrence -- district issuing ticket
				NOVnumber: i === 1 ? NOVnumberState : "",
				NOVdate: moment((new Date()).toUTCString()).format("YYYY-MM-DD"), // YYYY-MM-DD
				Address: "", // where ticket was issued to respondent
				ReasonForVoid: "", // selection made from the dropdown list
				VerifiedBy: "", // uid of the supervisors who authorized the void
			};
			lcllstOfDS1704s.push(aRow);
		}

		setlstOfDS1704s(lcllstOfDS1704s);

	}, [NOVnumberState])

	React.useEffect(() => {
		setNOVnumberState(oneDS1984.BoxNumberedFrom);
	}, [oneDS1984])

	React.useEffect(() => {
		console.log("useEffect for valid NOV", IsValidNOVnumberState)
		if (!NOVnumberState) {
			setLoading(false); // no lookup needed when query string doesn't have NOV#
        }
		else if (NOVnumberState.length > 7) {
			setLoading(!IsValidNOVnumberState);
		}
		else {
			setLoading(false); // no lookup needed when query string doesn't have NOV#
        }
	}, [IsValidNOVnumberState])

	React.useEffect(() => {

		const dte: Date = new Date(dsAttributesOfOneNOV.DateServedYear,
			dsAttributesOfOneNOV.DateServedMonth - 1,
			dsAttributesOfOneNOV.DateServedDay);
		const strDte: string = moment(dte).format('YYYY-MM-DD');
		console.log("strDte for dsAttributesOfOneNOV change: ", strDte);
		setDateServedState(strDte);

	}, [dsAttributesOfOneNOV.ECBSummonsNumber,
		dsAttributesOfOneNOV.DateServedYear,
		dsAttributesOfOneNOV.DateServedMonth,
		dsAttributesOfOneNOV.DateServedDay,
	])

	const cb = (response: any) => {
		let rdata1984: DS1984 = dummies.dummyDS1984;
		setDS1984(rdata1984);
	}

	const er = (response: any) => {
		alert("DS1704Atable/er: " + response);
	}

	function loggedInUser(): string {
		return (localStorage.getItem('userid') + "").toString(); // to avoid possibly null error
		//const j: any = JSON.parse(curUser);
		//return j.data.userPreferences.userName.toString();
	}

	function handleCreateDS1704A(aDS1704: aDS1704rows) {
		// alert("CRUD dbo.DS1704");
		console.log("CRUD dbo.DS1704 with ", aDS1704);
		if (aDS1704.ReasonForVoid === null || aDS1704.ReasonForVoid.length < 2) {
			alert("PTTS needs you to choose a reason for the void from the drop down.");
			return;
        }
		const DS1704row: tDS1704 = {
			ECBSummonsNumber: dsAttributesOfOneNOV.ECBSummonsNumber,
			TimeServed: new Date(),
			PlaceOfOccurence: aDS1704.Address,
			Remarks: "",
			ModifiedBy: loggedInUser(),
			ModifiedOn: new Date(), // determined by SPROC [dbo].[DS1704_InsertUpdateRecord]
			VoidDate: new Date(), 
			VoidReasonId: parseInt(aDS1704.ReasonForVoid),
			CreatedBy: loggedInUser(), // determined by SPROC [dbo].[DS1704_InsertUpdateRecord]
			CreatedDate: new Date(), // determined by SPROC [dbo].[DS1704_InsertUpdateRecord]
			IsActive: true, // determined by SPROC [dbo].[DS1704_InsertUpdateRecord]
			ApprovedBy: "", // determined by SPROC [dbo].[DS1704_InsertUpdateRecord]
			District: "", // determined by SPROC [dbo].[DS1704_InsertUpdateRecord]
        }
	};

	function handleGetDS21CardForThisDS1704(rcpt: string) {
		history.push('/PTTS/DS21Card/' + rcpt)
	}


	function handleGetNOVForThisDS1704(NOVno: string) {
		history.push('/PTTS/NOVdetails/' + NOVno)
	}

	function fIsValidNOVnumber(aDSform: string, LowHigh: string, TgtVal: string): boolean {
		if (!TgtVal || TgtVal.length < 8) {
			setIsValidNOVnumberState(true); // artificial when no query string sent
			return false;
		}
		let rdata: UsedTicketRange[] | undefined = undefined;
		let pIsValidNOVnumber: boolean = false;

		const searchECBsummonsNumber: string =
			TgtVal.length < 9
				? PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(Number.parseInt(TgtVal))
				: TgtVal;
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Find a ticket range with NOV ", searchECBsummonsNumber);

		(async () => {
			const tryThisRange = (
				{
					ECBSummonsNumberFrom: LowHigh === "Low"
						? TgtVal
						: searchECBsummonsNumber,
					ECBSummonsNumberTo: LowHigh === "High"
						? TgtVal
						: searchECBsummonsNumber,
					DSform: aDSform,
					ReceiptNumber: "",
				}
			);
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("tryThisRange");
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange);
			pIsValidNOVnumber = false;
			const response = await allServices.PTTSgetServices.refUsedTicketNumbers(tryThisRange);
			// axios always returns an object with a "data" child
			rdata = await response.data;
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(tryThisRange)
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
			if (!rdata) {
				pIsValidNOVnumber = false;
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Ticket number bad based on no rdata object.");
				setIsValidNOVnumberState(pIsValidNOVnumber);
			}
			else if (!rdata.length ? true : rdata.length === 0) {
				pIsValidNOVnumber = false;
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(`Ticket number ${searchECBsummonsNumber} bad based on empty [].`);
				setIsValidNOVnumberState(pIsValidNOVnumber);
			}
			else {
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("found a ticket range for ", searchECBsummonsNumber);
				pIsValidNOVnumber =
					Number.parseInt(tryThisRange.ECBSummonsNumberFrom) > 0
						&& Number.parseInt(tryThisRange.ECBSummonsNumberTo) > 0
						? true : false;
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber and non-zeroes",pIsValidNOVnumber);

				//if (!pIsValidNOVnumber) {
				//	if (!rdata![rdata.length - 1]) { // https://stackoverflow.com/questions/55190059/typescript-property-type-does-not-exist-on-type-never
				//		alert(" Cannot find a DS-1984 for this summons number.");
				//	}
				//}
				//else {
				//	alert("NOV Number is OKay.");
				//	setNOVnumberState(searchECBsummonsNumber);
				//}

				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber at end of ASYNC",pIsValidNOVnumber);
				setIsValidNOVnumberState(pIsValidNOVnumber);
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("IsValidNOVnumberState at end of ASYNC",IsValidNOVnumberState);
				return pIsValidNOVnumber;
			}

		})();

		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("pIsValidNOVnumber after ASYNC", pIsValidNOVnumber);
		return pIsValidNOVnumber;

	};

	const handleLinkToParent1984 = (e: any) => {
		history.push('/PTTS/existingDS1984/' + e.target.textContent)
	}

	const handleOnChange = (e: any) => {
		e.preventDefault();

		switch (e.target.name) {

			case "fldNOVnumber":
				let dummy2n: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				if (isNaN(dummy2n)) {
					alert("NOV number is NaN.");
					break;
				}
				if (dummy2n > 299999999) {
					alert("Expected first summons number is supposed to start with 1 or 2 and be nine-digits.  You entered a number that is too big.")
					break;
				} // less than 8 digits, not-a-number, or nine or more digits starting with 3 (Sanitation only prefixes 1 or 2)
				//            12345678
				setNOVnumberState(dummy2n.toString());
				if (dummy2n > 99999999) {
					dummy2n = Math.trunc(dummy2n / 10);
					//const msg1: string = "check NOV number" + dummy2n.toString();
					//alert(msg1);
					const dummy2: string = PTTSfunctions.Commonfunctions.Append8WithECBcheckSum(dummy2n);
					if (!fIsValidNOVnumber("IsGoodNOVnumber", "Low", dummy2)) {
						// true means validation is OKay, value is proper
						if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("fIsValidNOVnumber has not found NOV number.")
					}
				}
				break;
			case "fldNOVnumberFromTableFirstRow":
				const targetRow: number = e.target.id;
				lstOfDS1704s[targetRow].NOVnumber = e.target.value;
				if (!fIsValidNOVnumber("IsGoodNOVnumber", "Low", e.target.value)) {
					// true means validation is OKay, value is proper
					lstOfDS1704s[targetRow].ReasonForVoid = "invalid NOV number";
				}
				else {
					lstOfDS1704s[targetRow].ReasonForVoid = "type a reason here";
                }
				break;
			default:
				// code block
				const msg: string = "No onChange for " + e.target.name;
				alert(msg);
		}
	}


	if (isLoading) {
		return (<Loader />);
	}
	else {
		return (
			<Grid container>
				{
				!NOVnumberState
					? <Grid item xs={12} component={Paper} className={classes.paper}></Grid>
					:<Grid item xs={12} component={Paper} className={classes.paper}>
					<FormControl  >
						<ErrorSummary errors={errors} />

						<InputLabel htmlFor="idNOVnumber">{NOVnumberState.length < 9
							? <div>Search for this ECB Summons Number: {NOVnumberState} </div>
							: IsValidNOVnumberState
								? < div > {NOVnumberState} is valid; click 'Go to {NOVnumberState}'.</div>
								: < div > {NOVnumberState} is not on any existing DS-1984.</div>}
						</InputLabel>
						<Input className={classes.styleforticketrangetext}
							type="number"
							id="idNOVnumber"
							onChange={(e) => handleOnChange(e)}
							placeholder="9-digit ECB Summons Number"
							name="fldNOVnumber"
							ref={register({
								required: { value: true, message: "PTTS expects the first NOV number (nine digits)." },
								min: {
									value: 100000000,
									message: "Too small. Individual ECB Summons are nine digit numbers starting with 1 or 2."
								},
								max: {
									value: 299999999,
									message: "Too big.  Individual ECB Summons are nine digit numbers starting with 1 or 2."
								},
							})}
							value={NOVnumberState}
						// defaultValue versus value --> value lets me change characters in the currently active control textbox
						/>
						<div>&nbsp;</div>
						<ErrorMessage
							errors={errors}
							name="fldNOVnumber"
							as={<ErrorMessageContainer />}
						/>
						{IsValidNOVnumberState
							?
							<button type="submit" onClick={(e) => { e.preventDefault(); history.push('/PTTS/NOVdetails/' + NOVnumberState); }}>Go to {NOVnumberState}</button>
							:
							<div>&nbsp;</div>
						}
						<Divider />
					</FormControl>
				</Grid>
                }
				<Grid item xs={12} component={Paper} className={classes.paper}>

					<Table className={classes.table} aria-label="a dense table" >
						<TableHead>
							<TableRow>
								<TableCell size="small" align="right">District</TableCell>
								<TableCell size="small" align="center">Ticket No</TableCell>
								<TableCell size="small" align="right">Date of Issue/Date</TableCell>
								<TableCell size="small" align="left">Address</TableCell>
								<TableCell size="small" align="left">Reason for Void</TableCell>
								<TableCell size="medium" align="left">Verified By</TableCell>
								<TableCell size="medium" align="left">Click to Create DS1704A</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{
								
								// @ts-ignore: Object is possibly 'null'.
								isLoading //  || !dsListPayCodeFilter || dsListPayCodeFilter === undefined
									|| lstOfDS1704s.length < 2
									? <TableRow><TableCell size="small">building a list of 20 rows ...{lstOfDS1704s.length }</TableCell></TableRow>
									: lstOfDS1704s.map((n: aDS1704rows) => {
												//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("n is:");
												//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(n);
										return (
											<TableRow
												key={("000000000" + n.id.toString()).substr(-8) + "row"}
												hover
											>
												<TableCell size="small" key={("000000000" + n.id.toString()).substr(-8) + "POC"} align="center">{`${n.POC}`}</TableCell>
												<TableCell size="small" key={("000000000" + n.id.toString()).substr(-8) + "TKTNO"} align="left">
													{!n.NOVnumber || n.NOVnumber.length === 0
														? <FormControl id={("000000000" + n.id.toString()).substr(-8) + "FCid"}>
															{/* https://stackoverflow.com/questions/65395896/there-are-multiple-inputbase-components-inside-a-formcontrol */}
															<Input autoFocus className={classes.styleforticketrangetext}
																type="number"
																id={("000000000" + n.id.toString()).substr(-8) + "input"}
																onChange={(e) => handleOnChange(e)}
																placeholder="9-digit ECB Summons Number"
																name="fldNOVnumberFromTableFirstRow"
																value={n.NOVnumber}
															// defaultValue versus value --> value lets me change characters in the currently active Input control
															/>
															<div>&nbsp;</div>
															<ErrorMessage
																errors={errors}
																name="fldNOVnumberFromTableFirstRow"
																as={<ErrorMessageContainer />}
															/>
														</FormControl>
														: <div>{n.NOVnumber}</div>
													}
												</TableCell>
												<TableCell size="small" key={("000000000" + n.id.toString()).substr(-8) + "DATE"} align="right">{!n.NOVdate ? "na" : moment(n.NOVdate).format("M/D/YYYY")}</TableCell>
												<TableCell size="small" key={("000000000" + n.id.toString()).substr(-8) + "ADDRESS"} align="left">{n.Address}</TableCell>
												<TableCell size="medium" key={("000000000" + n.id.toString()).substr(-8) + "REASON"} align="left" onClick={handleLinkToParent1984}><Link href="#">{n.ReasonForVoid}</Link></TableCell>
												<TableCell size="small" key={("000000000" + n.id.toString()).substr(-8) + "VERIFIEDBY"} align="left" onClick={handleLinkToParent1984}><Link href="#">{n.VerifiedBy}</Link></TableCell>
												<TableCell size="small" key={n.id} align="right" onClick={() => handleCreateDS1704A(n)}><Link href="#">Void</Link></TableCell>
											</TableRow>
											);
									}
								)
							}
						</TableBody>
					</Table>
				</Grid>

			</Grid>


			)

	}			 // each voided NOV has one associated DS1704A; this is the UI creating the DS1704A record

}

const useStyles = makeStyles({
	paper: {
		width: "100%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 360,
		overflowX: "visible",
	},
	table: {
		width: "100%",
		margingTop: 80,
		minWidth: 1000,
		display: "inline-block",
	},
	cell: {
		minWidth: 100,
		maxWidth: 220,
	},
	styleforticketrangetext: {
		marginBottom: 0,
		marginTop: 0,
		lineHeight: 1,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 14,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 300,
		minWidth: 250,
	}
});
