﻿import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import moment from 'moment';
import * as React from 'react';
import { useCallback, useEffect, useState } from 'react';
import Background from '../assets/images/DS249_1204.png';
import { Container, Row, Col } from 'reactstrap';



export function DS249HearingSchedule() {
	const classes = useStyles();
	//let dateObj = new Date(2020, 11, 27);
	//const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
	//	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	//let monthEnds = [31, 28, 31, 30, 31, 30,
	//	31, 31, 30, 31, 30, 31];
	//monthEnds[1] = dateObj.getFullYear() % 4 === 0 ? 29 : 28;
	//let month = monthNames[dateObj.getMonth()];
	//let day = String(dateObj.getDate()).padStart(2, '0');
	//let year = dateObj.getFullYear();
	//let output = month + '\n' + day + ',' + year;
	//const SunThisWeek: number = dateObj.getDate() - dateObj.getDay() ; // Sunday is zero
	//const SatThisWeek: number = dateObj.getDate() + 6 - dateObj.getDay(); // Saturday is six


	//	const [codeCat, setCodeCat] = useState(null);

	const [vacationSchedule, setvacationSchedule] = useState('');

	const [respondentName, setrespondentName] = useState('');

	const [complaint, setComplaint] = useState(null);

	const [indexNo, setindexNo] = useState(16401); //  indexNo2 is a string representation of the indexNo; however there can be more than one row for each distinct indexNo2

	const [convDAWCtoYYYYMMDD, setconvDAWCtoYYYYMMDD] = useState('');

	const [weekType, setweekType] = useState('');
	
	const Holiday: any = (YYYYMMDD: string) => {
		var holidays: any = { // keys are formatted as month,week,day
			"0,3,1": "Martin Luther King, Jr. Day",
			"1,3,1": "President's Day",
		//	"2,1,0": "Daylight Savings Time Begins",
		//	"3,3,3": "Administrative Assistants Day",
		//	"4,1,0": "Mother's Day",
		//	"4,-1,1": "Memorial Day",
		//	"5,2,0": "Father's Day",
		//	"6,2,0": "Parents Day",
			"8,1,1": "Labor Day",
		//	"8,1,0": "Grandparents Day",
		//	"8,-1,0": "Gold Star Mothers Day",
			"9,2,1": "Columbus Day",
		//	"10,0,0": "Daylight Savings Time Ends",
			"10,1,2": "Election Day",
			"10,1,4": "Veterans Day",
			"10,4,4": "Thanksgiving Day"
		};
		let dteDDMMMYYYY: any = moment(YYYYMMDD);
		const dteLastMonInMonth: any = moment(YYYYMMDD)
		dteLastMonInMonth.endOf('month').add(-1, 'w').day(1);
		const DDMMM: string = dteDDMMMYYYY.format("DDMMM");
		if (DDMMM === '03Jul' || DDMMM === '25May' || DDMMM === '20Jan') {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(DDMMM);
		}
		// order of the next three is important; day(1) increments the moment object
		let aDOW: number = dteDDMMMYYYY.day();
		let aMonth: number = dteDDMMMYYYY.day(1).month(); // ZZ
		let aWeekInTheMonth: number = Math.ceil(dteDDMMMYYYY.date() / 7);
		let arrItem: string = aMonth.toString() + "," + aWeekInTheMonth.toString() + "," + aDOW.toString();
		let bIs: string = holidays[arrItem];

		dteDDMMMYYYY = moment(YYYYMMDD); // restore to date passed in ZZ
		if (dteDDMMMYYYY.add(1, 'd').day() === 6 && DDMMM === "24Dec") { // AA
			return "Christmas Day (Observed)"; 
		}
		dteDDMMMYYYY = moment(YYYYMMDD); // restore to date passed in AA
		if (dteDDMMMYYYY.add(-1, 'd').day() === 0 && DDMMM === "26Dec") { // BB need to cancel prev .add()
			return "Christmas Day (Observed)";
		}
		dteDDMMMYYYY = moment(YYYYMMDD); // restore to date passed in BB
		if (dteDDMMMYYYY.add(-3, 'd').day() === 6 && DDMMM === "03Jan") { // CC
			return "New Years Day (Observed)";
		}
		dteDDMMMYYYY = moment(YYYYMMDD); // restore to date passed in CC
		if (dteDDMMMYYYY.add(-1, 'd').day() === 0 && DDMMM === "02Jan") { // DD
			return "New Years Day (Observed)";
		}
		dteDDMMMYYYY = moment(YYYYMMDD); // restore to date passed in DD
		if (dteDDMMMYYYY.add(1, 'd').day() === 6 && DDMMM === "03Jul") { // EE
			return "Independence Day (Observed)";
		}
		dteDDMMMYYYY = moment(YYYYMMDD); // restore to date passed in EE
		if (dteDDMMMYYYY.add(-1, 'd').day() === 0 && DDMMM === "05Jul") { // FF
			return "Independence Day (Observed)";
		}
		dteDDMMMYYYY = moment(YYYYMMDD); // restore to date passed in FF
		if (dteLastMonInMonth.format("YYYYMMDD") === dteDDMMMYYYY.format("YYYYMMDD")  && dteDDMMMYYYY.month() === 4) {
			return "Memorial Day";
        }
		if (!bIs && DDMMM !== '25Dec' && DDMMM !== '01Jan' && DDMMM !== '04Jul') {
			return " not a holiday";
		}
		else {
			if (dteDDMMMYYYY.day() !== 6 && dteDDMMMYYYY.day() !== 0 && DDMMM === '25Dec') {
				return "Christmas Day"
			}
			else if (dteDDMMMYYYY.day() !== 6 && dteDDMMMYYYY.day() !== 0 && DDMMM === '01Jan') {
				return "New Years Day"
			}
			else if (dteDDMMMYYYY.day() !== 6 && dteDDMMMYYYY.day() !== 0 && DDMMM === '04Jul') {
				return "Independence Day"
			}
			else {
				return bIs;

            }
		}
	}

	// const callAPIx: any = async (anindexNo: number) => {
	//	const response1: any = await fetch('https://localhost:44333/api/DS249data');
	//	const data1: any = await response1.json();
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ data1 });
	//	const item = data1[7];
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ item });
	//	await setCodeCat(item);
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ codeCat });  // still null at this point ???

	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ indexNo });
	//	const response2: any = await fetch('https://localhost:44333/IndexNumb?IndexNumb=' + anindexNo);
	//	const data2: any = await response2.json();
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ data2 });
	//	await setComplaint(data2);
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ complaint }); // still null at this point ???

	//	const item32 = data2.VacationSchedule;
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ item32 });
	//	await setVacationSchedule(' ' + item32 + ',');

	//	const sName: any = data2.RespondentFName.trim() + ' ' + (data2.RespondentMName + ' ').trim() + data2.RespondentLName.trim();
	//	await setrespondentName(sName);
	// };

	const callAPI: any = useCallback(async (indexNo: string) => {
		return await fetch('https://localhost:44333/IndexNumb?IndexNumb=' + indexNo)
			.then(res => {
				return res.json();
			})
			.then(rcvd => {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ rcvd });
				setComplaint(rcvd);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ complaint });  // still null at this point ???


				const item32 = rcvd.vacationSchedule;
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ item32 });
				setvacationSchedule(' ' + item32 + ',');

				const sName: any = rcvd.RespondentFName.trim() + ' ' + (rcvd.RespondentMName + ' ').trim() + rcvd.RespondentLName.trim();
				setrespondentName(sName);

			})
			.catch(err => {
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log({ err });
			});
	}, [complaint])
	useEffect(() => {
		callAPI(indexNo);
	}, [indexNo, callAPI]);


	const callconvDAWCtoYYYYMMDD: any =  (dafc: number) => {
		const lclYYYYMMDD: string = moment().add(dafc, 'd').format("YYYYMMDD");
		 setconvDAWCtoYYYYMMDD(lclYYYYMMDD)
	};


	const lm: any = () => setDaysAwayFromCurrent(DaysAwayFromCurrent - moment().add(DaysAwayFromCurrent, 'd').diff(moment().add(DaysAwayFromCurrent, 'd').add(-1, 'M')) / 86400000);
	const lw: any = () => setDaysAwayFromCurrent(DaysAwayFromCurrent - 7);
	const nw: any = () => setDaysAwayFromCurrent(DaysAwayFromCurrent + 7);
	const nm: any = () => setDaysAwayFromCurrent(DaysAwayFromCurrent + moment().add(DaysAwayFromCurrent, 'd').diff(moment().add(DaysAwayFromCurrent, 'd').add(1, 'M')) / -86400000);

	const [DaysAwayFromCurrent, setDaysAwayFromCurrent] = useState(0);

	const [sSunday, setSunday] = useState(moment().day(0 + DaysAwayFromCurrent).format("D MMM YYYY"));
	const [sMonday, setMonday] = useState(moment().day(1 + DaysAwayFromCurrent).format("D MMM YYYY"));
	const [sTuesday, setTuesday] = useState(moment().day(2 + DaysAwayFromCurrent).format("D MMM YYYY"));
	const [sWednesday, setWednesday] = useState(moment().day(3 + DaysAwayFromCurrent).format("D MMM YYYY"));
	const [sThursday, setThursday] = useState(moment().day(4 + DaysAwayFromCurrent).format("D MMM YYYY"));
	const [sFriday, setFriday] = useState(moment().day(5 + DaysAwayFromCurrent).format("D MMM YYYY"));
	const [sSaturday, setSaturday] = useState(moment().day(6 + DaysAwayFromCurrent).format("D MMM YYYY"));

	const [iChartWeek, setChartWeek] = useState(moment().day(1).week() - 1);
	const [bVacThisWeek, setVacThisWeek] = useState(false);


	const [sDDMMMYYYYsun, setDDMMMYYYYsun] = useState(moment().add(DaysAwayFromCurrent, 'd').day(0).format("YYYY-MM-DD"));
	const [sDDMMMYYYYmon, setDDMMMYYYYmon] = useState(moment().add(DaysAwayFromCurrent, 'd').day(1).format("YYYY-MM-DD"));
	const [sDDMMMYYYYtue, setDDMMMYYYYtue] = useState(moment().add(DaysAwayFromCurrent, 'd').day(2).format("YYYY-MM-DD"));
	const [sDDMMMYYYYwed, setDDMMMYYYYwed] = useState(moment().add(DaysAwayFromCurrent, 'd').day(3).format("YYYY-MM-DD"));
	const [sDDMMMYYYYthu, setDDMMMYYYYthu] = useState(moment().add(DaysAwayFromCurrent, 'd').day(4).format("YYYY-MM-DD"));
	const [sDDMMMYYYYfri, setDDMMMYYYYfri] = useState(moment().add(DaysAwayFromCurrent, 'd').day(5).format("YYYY-MM-DD"));
	const [sDDMMMYYYYsat, setDDMMMYYYYsat] = useState(moment().add(DaysAwayFromCurrent, 'd').day(6).format("YYYY-MM-DD"));

	const [sDDMMMYYYYsunh, setDDMMMYYYYsunh] = useState(Holiday(sDDMMMYYYYsun));
	const [sDDMMMYYYYmonh, setDDMMMYYYYmonh] = useState(Holiday(sDDMMMYYYYmon));
	const [sDDMMMYYYYtueh, setDDMMMYYYYtueh] = useState(Holiday(sDDMMMYYYYtue));
	const [sDDMMMYYYYwedh, setDDMMMYYYYwedh] = useState(Holiday(sDDMMMYYYYwed));
	const [sDDMMMYYYYthuh, setDDMMMYYYYthuh] = useState(Holiday(sDDMMMYYYYthu));
	const [sDDMMMYYYYfrih, setDDMMMYYYYfrih] = useState(Holiday(sDDMMMYYYYfri));
	const [sDDMMMYYYYsath, setDDMMMYYYYsath] = useState(Holiday(sDDMMMYYYYsat));

	//const callDDMMMYYYYsun: any = (dafc: number) => { setDDMMMYYYYsun(moment().add(dafc, 'd').day(0).format("YYYY-MM-DD")) };
	//const callDDMMMYYYYmon: any = (dafc: number) => { setDDMMMYYYYmon(moment().add(dafc, 'd').day(1).format("YYYY-MM-DD")) };
	//const callDDMMMYYYYtue: any = (dafc: number) => { setDDMMMYYYYtue(moment().add(dafc, 'd').day(2).format("YYYY-MM-DD")) };
	//const callDDMMMYYYYwed: any = (dafc: number) => { setDDMMMYYYYwed(moment().add(dafc, 'd').day(3).format("YYYY-MM-DD")) };
	//const callDDMMMYYYYthu: any = (dafc: number) => { setDDMMMYYYYthu(moment().add(dafc, 'd').day(4).format("YYYY-MM-DD")) };
	//const callDDMMMYYYYfri: any = (dafc: number) => { setDDMMMYYYYfri(moment().add(dafc, 'd').day(5).format("YYYY-MM-DD")) };
	//const callDDMMMYYYYsat: any = (dafc: number) => { setDDMMMYYYYsat(moment().add(dafc, 'd').day(6).format("YYYY-MM-DD")) };

	//const callDDMMMYYYYsunh: any = (dafc: number) => { setDDMMMYYYYsunh(Holiday(moment().add(dafc, 'd').day(0).format("YYYY-MM-DD"))) };
	//const callDDMMMYYYYmonh: any = (dafc: number) => { setDDMMMYYYYmonh(Holiday(moment().add(dafc, 'd').day(1).format("YYYY-MM-DD"))) };
	//const callDDMMMYYYYtueh: any = (dafc: number) => { setDDMMMYYYYtueh(Holiday(moment().add(dafc, 'd').day(2).format("YYYY-MM-DD"))) };
	//const callDDMMMYYYYwedh: any = (dafc: number) => { setDDMMMYYYYwedh(Holiday(moment().add(dafc, 'd').day(3).format("YYYY-MM-DD"))) };
	//const callDDMMMYYYYthuh: any = (dafc: number) => { setDDMMMYYYYthuh(Holiday(moment().add(dafc, 'd').day(4).format("YYYY-MM-DD"))) };
	//const callDDMMMYYYYfrih: any = (dafc: number) => { setDDMMMYYYYfrih(Holiday(moment().add(dafc, 'd').day(5).format("YYYY-MM-DD"))) };
	//const callDDMMMYYYYsath: any = (dafc: number) => { setDDMMMYYYYsath(Holiday(moment().add(dafc, 'd').day(6).format("YYYY-MM-DD"))) };

	//const callSunday: any = (dafc: number) => { setSunday(moment().add(dafc, 'd').day(0).format("D MMM YYYY") ) };
	//const callMonday: any = (dafc: number) => { setMonday(moment().add(dafc, 'd').day(1).format("D MMM YYYY") ) };
	//const callTuesday: any = (dafc: number) => { setTuesday(moment().add(dafc, 'd').day(2).format("D MMM YYYY") ) };
	//const callWednesday: any = (dafc: number) => { setWednesday(moment().add(dafc, 'd').day(3).format("D MMM YYYY") ) };
	//const callThursday: any = (dafc: number) => { setThursday(moment().add(dafc, 'd').day(4).format("D MMM YYYY") ) };
	//const callFriday: any = (dafc: number) => { setFriday(moment().add(dafc, 'd').day(5).format("D MMM YYYY") ) };
	//const callSaturday: any = (dafc: number) => { setSaturday(moment().add(dafc, 'd').day(6).format("D MMM YYYY")) };

	// const callDayx: any = (dafc: number) => {
	//	setSunday(moment().add(dafc, 'd').day(0).format("D MMM YYYY"));
	//	setMonday(moment().add(dafc, 'd').day(1).format("D MMM YYYY"));
	//	setTuesday(moment().add(dafc, 'd').day(2).format("D MMM YYYY"));
	//	setWednesday(moment().add(dafc, 'd').day(3).format("D MMM YYYY"));
	//	setThursday(moment().add(dafc, 'd').day(4).format("D MMM YYYY"));
	//	setFriday(moment().add(dafc, 'd').day(5).format("D MMM YYYY"));
	//	setSaturday(moment().add(dafc, 'd').day(6).format("D MMM YYYY"));

	//	setDDMMMYYYYsun(moment().add(dafc, 'd').day(0).format("YYYY-MM-DD"));
	//	setDDMMMYYYYmon(moment().add(dafc, 'd').day(1).format("YYYY-MM-DD"));
	//	setDDMMMYYYYtue(moment().add(dafc, 'd').day(2).format("YYYY-MM-DD"));
	//	setDDMMMYYYYwed(moment().add(dafc, 'd').day(3).format("YYYY-MM-DD"));
	//	setDDMMMYYYYthu(moment().add(dafc, 'd').day(4).format("YYYY-MM-DD"));
	//	setDDMMMYYYYfri(moment().add(dafc, 'd').day(5).format("YYYY-MM-DD"));
	//	setDDMMMYYYYsat(moment().add(dafc, 'd').day(6).format("YYYY-MM-DD"));

	//	setDDMMMYYYYsunh(Holiday(moment().add(dafc, 'd').day(0).format("YYYY-MM-DD")));
	//	setDDMMMYYYYmonh(Holiday(moment().add(dafc, 'd').day(1).format("YYYY-MM-DD")));
	//	setDDMMMYYYYtueh(Holiday(moment().add(dafc, 'd').day(2).format("YYYY-MM-DD")));
	//	setDDMMMYYYYwedh(Holiday(moment().add(dafc, 'd').day(3).format("YYYY-MM-DD")));
	//	setDDMMMYYYYthuh(Holiday(moment().add(dafc, 'd').day(4).format("YYYY-MM-DD")));
	//	setDDMMMYYYYfrih(Holiday(moment().add(dafc, 'd').day(5).format("YYYY-MM-DD")));
	//	setDDMMMYYYYsath(Holiday(moment().add(dafc, 'd').day(6).format("YYYY-MM-DD")));

	//	callChartWeek(dafc);
	//	callconvDAWCtoYYYYMMDD(dafc);
	// };


	const callDay: any = useCallback((dafc: number) => {
		setSunday(moment().add(dafc, 'd').day(0).format("D MMM YYYY"));
		setMonday(moment().add(dafc, 'd').day(1).format("D MMM YYYY"));
		setTuesday(moment().add(dafc, 'd').day(2).format("D MMM YYYY"));
		setWednesday(moment().add(dafc, 'd').day(3).format("D MMM YYYY"));
		setThursday(moment().add(dafc, 'd').day(4).format("D MMM YYYY"));
		setFriday(moment().add(dafc, 'd').day(5).format("D MMM YYYY"));
		setSaturday(moment().add(dafc, 'd').day(6).format("D MMM YYYY"));

		setDDMMMYYYYsun(moment().add(dafc, 'd').day(0).format("YYYY-MM-DD"));
		setDDMMMYYYYmon(moment().add(dafc, 'd').day(1).format("YYYY-MM-DD"));
		setDDMMMYYYYtue(moment().add(dafc, 'd').day(2).format("YYYY-MM-DD"));
		setDDMMMYYYYwed(moment().add(dafc, 'd').day(3).format("YYYY-MM-DD"));
		setDDMMMYYYYthu(moment().add(dafc, 'd').day(4).format("YYYY-MM-DD"));
		setDDMMMYYYYfri(moment().add(dafc, 'd').day(5).format("YYYY-MM-DD"));
		setDDMMMYYYYsat(moment().add(dafc, 'd').day(6).format("YYYY-MM-DD"));

		setDDMMMYYYYsunh(Holiday(moment().add(dafc, 'd').day(0).format("YYYY-MM-DD")));
		setDDMMMYYYYmonh(Holiday(moment().add(dafc, 'd').day(1).format("YYYY-MM-DD")));
		setDDMMMYYYYtueh(Holiday(moment().add(dafc, 'd').day(2).format("YYYY-MM-DD")));
		setDDMMMYYYYwedh(Holiday(moment().add(dafc, 'd').day(3).format("YYYY-MM-DD")));
		setDDMMMYYYYthuh(Holiday(moment().add(dafc, 'd').day(4).format("YYYY-MM-DD")));
		setDDMMMYYYYfrih(Holiday(moment().add(dafc, 'd').day(5).format("YYYY-MM-DD")));
		setDDMMMYYYYsath(Holiday(moment().add(dafc, 'd').day(6).format("YYYY-MM-DD")));

		const iCW = moment().add(dafc, 'd').day(1).week() - 1;
		setChartWeek(iCW);
		// ("0" + iCW).slice(-2) left-pad with one zero so I get two digits for numbers less than 10
		const bRslt: boolean = vacationSchedule.search(("0" + iCW).slice(-2) + ',') < 0 ? false : true;
		setVacThisWeek(
			bRslt
		);
		const sRslt: string = !bRslt ? 'Reg Week' : 'Vac Week';
		setweekType(
			sRslt
		);

		callconvDAWCtoYYYYMMDD(dafc);

	}, [vacationSchedule])
	useEffect(() => {
		callDay(DaysAwayFromCurrent);
	}, [DaysAwayFromCurrent, callDay]);


	// const callChartWeek: any = async (dafc: number) => {
	//	const iCW = moment().add(dafc, 'd').day(1).week() - 1;
	//	await setChartWeek(iCW);
	//	// ("0" + iCW).slice(-2) left-pad with one zero so I get two digits for numbers less than 10
	//	const bRslt: boolean = VacationSchedule.search(("0" + iCW).slice(-2) + ',') < 0 ? false : true;
	//	await setVacThisWeek(
	//		bRslt
	//	);
	//	const sRslt: string = !bRslt ? 'Reg Week' : 'Vac Week';
	//	await setweekType(
	//		sRslt
	//		);
	// };

	// useEffect(() => { callAPI(indexNo) }, [callAPI, indexNo]);

	//useEffect(() => { callChartWeek(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);

	//useEffect(() => { callconvDAWCtoYYYYMMDD(DaysAwayFromCurrent) }, [DaysAwayFromCurrent])

	// useEffect(() => { callDay(DaysAwayFromCurrent) }, [callDay, DaysAwayFromCurrent])

	//useEffect(() => { callDDMMMYYYYsun(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callDDMMMYYYYmon(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callDDMMMYYYYtue(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callDDMMMYYYYwed(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callDDMMMYYYYthu(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callDDMMMYYYYfri(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callDDMMMYYYYsat(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);

	//useEffect(() => { callDDMMMYYYYsunh(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callDDMMMYYYYmonh(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callDDMMMYYYYtueh(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callDDMMMYYYYwedh(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callDDMMMYYYYthuh(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callDDMMMYYYYfrih(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callDDMMMYYYYsath(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);

	//useEffect(() => { callSunday(DaysAwayFromCurrent) }, [DaysAwayFromCurrent] );
	//useEffect(() => { callMonday(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callTuesday(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callWednesday(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callThursday(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callFriday(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);
	//useEffect(() => { callSaturday(DaysAwayFromCurrent) }, [DaysAwayFromCurrent]);

	setindexNo(16401);
	return (
		<div>
		<Container>
				<Row className={classes.ScheduleTitle}>
					<h2>Schedule the Hearing Date for {respondentName}</h2>
					</Row>
				<Row className={classes.ScheduleTitle}>
					<Col md="4" className={classes.ButtonStyleLeft}>
						<Button onClick={lw}> Prev </Button>
					</Col>
					<Col md="4" className={classes.DateRange}>
						{sSunday}
						&nbsp;<b>-</b>&nbsp;
						{sSaturday} 
					</Col>
					<Col md="4" className={classes.ButtonStyleRight}>
						<Button onClick={lw}> Next </Button>
					</Col>
				</Row>
				<Row className={classes.IndexTitle}>
					<Col md="6" className={classes.IndexNumber}>&nbsp;{indexNo}</Col>
					<Col md="6" className={classes.DateSelected}>&nbsp;{convDAWCtoYYYYMMDD}</Col>
				</Row>
				<Row className={classes.WeekDayNames}>
					<Col md="1">{!sDDMMMYYYYsunh || sDDMMMYYYYsunh === ' not a holiday' ? 'Sunday' : sDDMMMYYYYsunh}</Col>
					<Col md="2">{!sDDMMMYYYYmonh || sDDMMMYYYYmonh === ' not a holiday' ? 'Monday' : sDDMMMYYYYmonh}</Col>
					<Col md="2">{!sDDMMMYYYYtueh || sDDMMMYYYYtueh === ' not a holiday' ? 'Tuesday' : sDDMMMYYYYtueh}</Col>
					<Col md="2">{!sDDMMMYYYYwedh || sDDMMMYYYYwedh === ' not a holiday' ? 'Wednesday' : sDDMMMYYYYwedh}</Col>
					<Col md="2">{!sDDMMMYYYYthuh || sDDMMMYYYYthuh === ' not a holiday' ? 'Thursday' : sDDMMMYYYYthuh}</Col>
					<Col md="2">{!sDDMMMYYYYfrih || sDDMMMYYYYfrih === ' not a holiday' ? 'Friday' : sDDMMMYYYYfrih}</Col>
					<Col md="1">{!sDDMMMYYYYsath || sDDMMMYYYYsath === ' not a holiday' ? 'Saturday' : sDDMMMYYYYsath}</Col>
				</Row>
			</Container>
			<p>----------------------------------</p>
		<Container>
			<div>{indexNo}
			</div>
			<div className={classes.VacationSchedule}>
				for&nbsp;{respondentName}&nbsp;is:&nbsp;{vacationSchedule}
			</div>
			<div>
				Monday&nbsp;{sMonday}&nbsp;begins&nbsp;Chart&nbsp;Week&nbsp;{iChartWeek} {weekType}
			</div>
			<div>
				{convDAWCtoYYYYMMDD}&nbsp;is&nbsp;{Holiday(`${ convDAWCtoYYYYMMDD }`)}
			</div>
			<div>
				<Button variant="contained" color="primary" onClick={lm}> month before </Button>
				<Button variant="contained" color="primary" onClick={lw}> prev week </Button>
				Sunday: {sSunday}
				&nbsp;<b>through</b>&nbsp;
				Saturday: {sSaturday} 
				<Button variant="contained" color="primary" type="submit" onClick={nw}> week after </Button> 
				<Button variant="contained" color="primary" type="submit" onClick={nm}> month later </Button>
				{/*}<p>{moment().add(DaysAwayFromCurrent, 'd').diff(moment().add(DaysAwayFromCurrent, 'd').add(1, 'w')) / 86400000}</p>{*/}
				<p>Days Away From Current {DaysAwayFromCurrent}</p>
			</div>
			<div className="container ">
				{/*}
				<div className="row">
					<div className="border border-dark col-md-1">.col-md-1</div>
                    <div className="col-md-1">
						<img src={logo} className="img-fluid " alt=" " />
						<div className="card-img-overlay d-flex align-items-center">
							<div className="card-text">
								Light overlay
                            </div>
                        </div>
                    </div>
					<div className="shadow p-4 mb-4 bg-white col-md-1">.col-md-1</div>
					<div className="col-md-1">.col-md-1</div>
					<div className="border border-dark col-md-1">.col-md-1</div>

				</div>
				{*/}
				<Row>
					<div className="border col-md-1">{!sDDMMMYYYYsunh || sDDMMMYYYYsunh === ' not a holiday' ? 'Sunday' : sDDMMMYYYYsunh}</div>
					<div className="border col-md-2">{!sDDMMMYYYYmonh || sDDMMMYYYYmonh === ' not a holiday' ? 'Monday' : sDDMMMYYYYmonh}</div>
					<div className="border col-md-2">{!sDDMMMYYYYtueh || sDDMMMYYYYtueh === ' not a holiday' ? 'Tuesday' : sDDMMMYYYYtueh}</div>
					<div className="border col-md-2">{!sDDMMMYYYYwedh || sDDMMMYYYYwedh === ' not a holiday' ? 'Wednesday' : sDDMMMYYYYwedh}</div>
					<div className="border col-md-2">{!sDDMMMYYYYthuh || sDDMMMYYYYthuh === ' not a holiday' ? 'Thursday' : sDDMMMYYYYthuh}</div>
					<div className="border col-md-2">{!sDDMMMYYYYfrih || sDDMMMYYYYfrih === ' not a holiday' ? 'Friday' : sDDMMMYYYYfrih}</div>
					<div className="border col-md-1">{!sDDMMMYYYYsath || sDDMMMYYYYsath === ' not a holiday' ? 'Saturday' : sDDMMMYYYYsath}</div>
				</Row> 
				<Row>
					<Col md="1" style={{ backgroundColor: '#f1f1f1' }}>{sSunday}</Col>
					<div className="border col-md-2" style={bVacThisWeek ? { backgroundColor: '#A9A9A9' } : !sDDMMMYYYYmonh || sDDMMMYYYYmonh === ' not a holiday' ? { backgroundColor: '#90EE90' } : { backgroundColor: '#f1f1f1' }}>{sMonday}</div>
					<div className="border col-md-2" style={bVacThisWeek ? { backgroundColor: '#A9A9A9' } : !sDDMMMYYYYtueh || sDDMMMYYYYtueh === ' not a holiday' ? { backgroundColor: '#90EE90' } : { backgroundColor: '#f1f1f1' }}>{sTuesday}</div>
					<div className="border col-md-2" style={bVacThisWeek ? { backgroundColor: '#A9A9A9' } : !sDDMMMYYYYwedh || sDDMMMYYYYwedh === ' not a holiday'  ? { backgroundColor: '#90EE90' } : { backgroundColor: '#f1f1f1' }}>{sWednesday}</div>
					<div className="border col-md-2" style={bVacThisWeek ? { backgroundColor: '#A9A9A9' } : !sDDMMMYYYYthuh || sDDMMMYYYYthuh === ' not a holiday' ? { backgroundColor: '#90EE90' } : { backgroundColor: '#f1f1f1' }}>{sThursday}</div>
					<div className="border col-md-2" style={bVacThisWeek ? { backgroundColor: '#A9A9A9' } : !sDDMMMYYYYfrih || sDDMMMYYYYfrih === ' not a holiday' ? { backgroundColor: '#90EE90' } : { backgroundColor: '#f1f1f1' }}>{sFriday}</div>
					<Col md="1" style={{ backgroundColor: '#f1f1f1' }}>{sSaturday}</Col>
				</Row>
			</div>
		</Container>
		</div>

	);
}


const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		height: "100%",
		paddingLeft: 15,
		paddingRight: 15,
		opacity: 60,
		backgroundImage: `url( ${Background} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '820px 1030px',
		backgroundPosition: 'left top',
	},
	DateRange: {
		color: 'black',
		display: 'flex',
		fontWeight: 'bold',
		alignContent: 'center',
		marginTop: 10,
		paddingLeft: 70,
	},
	DateSelected: {
		display: 'flex',
		flexFlow: 'row nowrap',
		'&::before': {
			content: '"Date Selected:"',
			display: 'flex',
			marginTop: 0,
			color: 'black',
		},
		alignContent: 'right',
		marginTop: 10,
		paddingLeft: 150,
    },
	ButtonStyleLeft: {
		color: 'green',
		display: 'flex',
		flexFlow: 'row nowrap',
		'&::before': {
			content: '"< "',
			display: 'flex',
			marginTop: 10,
			color: 'green',
		},
		paddingLeft: '200px',
		marginTop: 0,
	},
	ButtonStyleRight: {
		color: 'green',
		display: 'flex',
		flexFlow: 'row nowrap',
		'&::after': {
			content: '" >"',
			display: 'flex',
			marginTop: 10,
			color: 'green',
		},
		alignContent: 'left',
		marginTop: 0,
	},
	ScheduleTitle: {
		backgroundColor: 'light grey',
	},
	IndexTitle: {
		backgroundColor: 'white',
		color: 'black',
	},
	IndexNumber: {
		display: 'flex',
		flexFlow: 'row nowrap',
		'&::before': {
			content: '"Index No."',
			display: 'flex',
			marginTop: 0,
			color: 'black',
		},
		alignContent: 'left',
		marginTop: 10,
	},
	WeekDayNames: {
		backgroundColor: 'black',
		color: 'white',
		lineHeight: '30px',
	},
	VacationSchedule: {
		// https://github.com/mui-org/material-ui/issues/14153
		marginTop: 20,
		marginLeft: 30,
		width: 400,
		display: 'flex',
		flexFlow: 'row nowrap',
		'&::before': {
			content: '"Vacation Schedule: "',
			display: 'block',
		}
	},
	bordered: {
		borderWidth: 2,
		borderColor: 'blue',
	},
	weekend: {
		backgroundColor: 'lt grey',
		borderColor: 'black',
	},
}));