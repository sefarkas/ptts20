﻿// prettier-ignore
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
//import Draggable from 'react-draggable';
import { useActions } from "../../actions";
import * as DS21Actions from "../../actions/DS21";
import Background from '../../assets/images/DS249_1204.png';

interface Props {
	open: boolean;
	onClose: () => void;
}

export function DS21Dialog(props: Props) {
	const { open, onClose } = props;
	const classes = useStyles();
	const [newDS21Text, setNewDS21Text] = React.useState("");
	const cDS21Actions: any = useActions(DS21Actions);

	const handleCreate = (event: any) => {
		cDS21Actions.insDS21({
			id: Math.random(),
			completed: false,
			DS21ReceiptNo: event.target.value,
			TicketRange: '1000-1999',
			CartonNoRange: '10-89',
			NoOfParts: 4,
			ModifiedOn: Date.now,
			ModifiedBy: 'sef'
		});
		// reset text if user reopens the dialog
		setNewDS21Text("new");
		onClose();

	};

	const handleClose = () => {
		// reset text if user reopens the dialog
		setNewDS21Text("");
		onClose();

	};

	const handleUpdate = (event: any) => {
		cDS21Actions.uptDS21({
			id: Math.random(),
			completed: false,
			DS21ReceiptNo: event.target.value,
			TicketRange: '1000-1999',
			CartonNoRange: '10-89',
			NoOfParts: 4,
			ModifiedOn: Date.now,
			ModifiedBy: 'sef'
		});
		setNewDS21Text("updated");

	};

	return (
		<Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
			<DialogTitle>Add a new DS21</DialogTitle>
			<DialogContent>
				<DialogContentText>
					THE CITY OF NEW YORK Department of Sanitation<br />
					NOTICE OF VIOLATION & HEARING Form DS21
				</DialogContentText>
				<TextField
					autoFocus
					margin="dense"
					id="DS21ReceiptNo"
					label="RECEIPT NUMBER"
					type="string"
					fullWidth
					value={newDS21Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
				<TextField
					margin="dense"
					id="BoxNumberedFrom"
					label="First Box Number"
					type="string"
					size="medium"
					required
					value={newDS21Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
				<TextField
					margin="dense"
					id="BoxNumberedTo"
					label="Last Box Number"
					type="string"
					size="small"
					required
					value={newDS21Text}
					onChange={handleUpdate}
					className={classes.textField}
				/>
			</DialogContent>
			<DialogActions>
				<Button onClick={handleUpdate} color="secondary">
					Save/Update
				</Button>
				<Button onClick={handleCreate} color="primary">
					MAKE NEW DS21
				</Button>
				<Button onClick={handleClose} color="primary">
					Done
				</Button>
			</DialogActions>

		</Dialog>

	);
}

const useStyles = makeStyles({
	newDS21: {
		flexGrow: 1,
		height: "10%",
		paddingLeft: 15,
		paddingRight: 15,
		opacity: 60,
		backgroundImage: `url( ${Background} )`,
		backgroundRepeat: 'no-repeat',
		backgroundSize: '82px 103px',
		backgroundPosition: 'left top',
	},
	pending: {
		backgroundColor: "white",
	},
	approved: {
		backgroundColor: "yellow"
	},
	textField: {
		width: "80%",
		margin: 20,
	},
});
