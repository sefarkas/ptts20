﻿import * as React from 'react';
import { useEffect, Fragment } from 'react';
import { useDispatch } from 'react-redux';
import { allActions } from '../actions';
import { history } from "../configureStore";


export function LogoutPageRPTDS156() {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(allActions.LoginADacts.actionTologoff());
        history.push('login');
    }, [dispatch]);


    return (
        <Fragment>
        </Fragment>
    );
}

