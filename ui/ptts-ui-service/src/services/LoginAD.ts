﻿import axios from 'axios';
import { USERSESSION } from "../constants/constantsAPI";
import { TYPES as LoginADconstants } from '../constants/constantsLoginAD';
import { API_NTI_AD_URI } from '../constants/constantsNTInetwork';
import { PTTSfunctions } from '../components/basicComponents';

interface Window {
    webkit: any;
}

declare let window: Window;

/**
* fetch API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
function fApiGetDataWithCallback(url: string, request: any, callBack: any, callBackFail: any) {
    axios.get(url, request)
        // tslint:disable-next-line:only-arrow-functions
        .then(function (response) {
            callBack(response.data);
        })
        // tslint:disable-next-line:only-arrow-functions
        .catch(function (error) {
            callBackFail(error);
        });
}

/**
* Post API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
function fApiPostDataWithCallback(url: string, request: any, callBack: any, callBackFail: any) {
    axios.post(url, request)
        // tslint:disable-next-line:only-arrow-functions
        .then(function (response) {
            callBack(response.data);
        })
        // tslint:disable-next-line:only-arrow-functions
        .catch(function (error) {
            callBackFail(error);
        });
}

/**
* fetch API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
//export function apiGeoLocationfromBrowser(actionType: any) {
//    navigator.geolocation.getCurrentPosition(successGeo.bind(this), errorGeo.bind(this), { enableHighAccuracy: true, timeout: 5000 })

//    function successGeo(position) {
//        return (dispatch: any) => dispatch({ type: actionType, payload: { "lat": position.coords.latitude, "lng": position.coords.longitude, "code": 0 } });
//    }

//    function errorGeo(error) {
//        return (dispatch: any) => dispatch({ type: actionType, payload: error });
//    }
//}

/**
* fetch API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
const fApiGetData = (url: string, request: any, actionType: any) => (dispatch: any) => {
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("entered apiGetData for " + url);
    const redMsgType = LoginADconstants.ACT_ERRMSG;

    axios.get(url, request)
        // tslint:disable-next-line:only-arrow-functions
        .then(function (response) {
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("apiGetData responded with the following:");
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(response);
            // tslint:disable-next-line:prefer-object-spread
            dispatch({ type: actionType, payload: response.data });
        })
        // tslint:disable-next-line:only-arrow-functions
        .catch(function (error) {
            dispatch({ type: redMsgType, payload: error + ' (trying: ' + url + ')' });
        });
}

/**
* fetch API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
const fApiGetNtiProfile = (url: string, request: any, dispatch: any, actionType: any) => {
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("entered apiGetData for " + url);
    const { ReferenceNumber } = request;
    const redMsgType = LoginADconstants.ACT_ERRMSG;
    axios.get(url + '/' + ReferenceNumber)
        // tslint:disable-next-line:only-arrow-functions
        .then(function (response) {
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("apiGetData responded with the following:");
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(response);
            return dispatch({ type: actionType, payload: response.data });;

        })
        .catch(function (error) {
            return dispatch({ type: redMsgType, payload: error + ' (trying: ' + url + ')' });
        });
}

/**
* fetch API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
const fApiGetDataForDropDownIa = (url: string, request: any, actionType: any) => (dispatch: any) => {
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("entered apiGetData for " + url);
    const redMsgType = LoginADconstants.ACT_ERRMSG;

    axios.get(url, request)
        // tslint:disable-next-line:only-arrow-functions
        .then(function (response) {
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("apiGetDataForDropDownIa responded with the following:");
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(response);
            const result = PTTSfunctions.Commonfunctions.parseArraytoReactSelect_IA(response);
            // tslint:disable-next-line:prefer-object-spread
            dispatch({ type: actionType, payload: result });
        })
        // tslint:disable-next-line:only-arrow-functions
        .catch(function (error) {
            dispatch({ type: redMsgType, payload: error + ' (trying: ' + url + ')' });
        });
}

/**
* fetch API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
const fApiFetchDropdownData = (url: string, request: any, actionType: any) => (dispatch: any) => {
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("apiFetchDropdownData entered");
    const headers = {
        'Content-Type': 'application/json',
    }

    axios.post(url, request, { headers })
        // tslint:disable-next-line:only-arrow-functions
        .then(function (response) {
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("apiFetchDropdownData api returned from " + url);
            const result = PTTSfunctions.Commonfunctions.parseArraytoReactSelect(response.data);
            // tslint:disable-next-line:prefer-object-spread
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("apiFetchDropdownData put result in " + actionType);;
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(response.data);
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(result);
            dispatch({ type: actionType, payload: result });
        })
        // tslint:disable-next-line:only-arrow-functions
        .catch(function (error) {
            dispatch({ type: actionType, payload: [] });
        });
}



/**
* fetch API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
function fApiPostDataWithHeader(url: string, options: any, callBack: any, callBackFail: any, actionType: any, dropdown?: any) {
    const jwtToken = sessionStorage.getItem('token');
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + jwtToken
    }

    axios.post(url, options, { headers })
        // tslint:disable-next-line:only-arrow-functions
        .then(function (response) {
            if (callBack) {
                callBack(response.data);
            } else if (dropdown) {
                const result = PTTSfunctions.Commonfunctions.parseArraytoReactSelect(response.data);
                return (dispatch: any) => dispatch({ type: actionType, payload: result });
            } else if (actionType) {
                return (dispatch: any) => dispatch({ type: actionType, payload: response.data });
            }
        })
        // tslint:disable-next-line:only-arrow-functions
        .catch(function (error) {
            if (error.response) {
                if (error.response.status === 401) {
                     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("trying to get refresh token");
                    const refreshToken = sessionStorage.getItem('refreshToken');
                    const refreshTokenAPI = USERSESSION.refreshtoken;
                    const requestObject = {
                        "token": jwtToken,
                        "refreshToken": refreshToken
                    }
                    allServices.apiPostDataWithCallback(refreshTokenAPI, requestObject, (data: any) => {
                        sessionStorage.setItem('token', data.token);
                        sessionStorage.setItem('refreshToken', data.refreshToken);
                         if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("RefreshToken Received");

                        if (window.webkit !== undefined) {
                            window.webkit.messageHandlers.logging.postMessage("RefreshToken Received");
                        }

                        return (dispatch: any) => dispatch(allServices.apiPostDataWithHeader(url, options, callBack, callBackFail, actionType));
                    }, () => {
                         if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log('Logout');

                        if (window.webkit !== undefined) {
                            window.webkit.messageHandlers.logging.postMessage("Logout");
                        }
                        // redirectToLogin();
                        alert("show the login component");
                    })
                } else {
                    if (callBackFail) {
                        callBackFail(error)
                    } else if (actionType) {
                        return (dispatch: any) => dispatch({ type: actionType, payload: false });
                    }
                }
            }
        });
}

/**
* delete API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
function fApiDELETEDataWithHeader(url: string, callBack: any) {
    const jwtToken = sessionStorage.getItem('token');
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + jwtToken
    }

    axios.delete(url, { headers })
        // tslint:disable-next-line:only-arrow-functions
        .then(function (response) {
            if (callBack) {
                callBack(response.data);
            }
        }
        )
        // tslint:disable-next-line:only-arrow-functions
        .catch(function (error) {
            if (error) {
                 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("delete failed")
            }
        });
}


/**
* fetch API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
function fApiGETDataWithHeader(url: string, options: any, callBack: any, callBackFail: any, actionType: any, dispatchRef: any, dropdown?: any) {
    const jwtToken = sessionStorage.getItem('token');
    const headers = {
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + jwtToken
    }

    axios.get(url, { headers })
        // tslint:disable-next-line:only-arrow-functions
        .then(function (response) {
            if (callBack) {
                callBack(response.data);
            } else if (dropdown) {
                const result = PTTSfunctions.Commonfunctions.parseArraytoReactSelect(response.data);
                return dispatchRef({ type: actionType, payload: result });
            }
            else if (actionType) {
                 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("apiGETDataWithHeader | dispatch response.data to " + actionType);
                 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(response.data);
                return dispatchRef({ type: actionType, payload: response.data }); // for agGrid, rowData is an array and not a string of json
            }
        })
        // tslint:disable-next-line:only-arrow-functions
        .catch(function (error) {
            if (error.response) {
                if (error.response.status === 401) {
                     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("trying to get refresh token");
                    const refreshToken = sessionStorage.getItem('refreshToken');
                    const refreshTokenAPI = USERSESSION.refreshtoken;
                    const requestObject = {
                        "token": jwtToken,
                        "refreshToken": refreshToken
                    }
                   allServices.apiPostDataWithCallback(refreshTokenAPI, requestObject, (data: any) => {
                        sessionStorage.setItem('token', data.token);
                        sessionStorage.setItem('refreshToken', data.refreshToken);
                       return dispatchRef(allServices.apiPostDataWithHeader(url, options, callBack, callBackFail, actionType));
                    }, () => {
                        alert("show the login component");

                        // redirectToLogin();
                    })
                } else {
                    if (callBackFail) {
                        callBackFail(error)
                    } else if (actionType) {
                        return dispatchRef({ type: actionType, payload: false });
                    }
                }
            }
        });
}

function fApiPostFetchAuthenAuthorDetails(request: any, dispatchRef: any): boolean {
    //const apiUrlAuthen = LOGINAD.validURI; // post
    //const apiUrlAuthor = LOGINAD.PTTSdiscoveredADgroupsForUID; // post
    //const apiUrlUserDs = LOGINAD.detailsURI; // post
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("exec'g apiPostFetchAuthenAuthorDetails");
    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(!dispatchRef ? "no dispatchRef obj" : dispatchRef);

    const apiUrlAuthen = API_NTI_AD_URI.adAuth; // post
    const apiUrlAuthor = API_NTI_AD_URI.adGroups; // post
    const apiUrlUserDs = API_NTI_AD_URI.adDetails; // post
    const apiUrlNTI = API_NTI_AD_URI.ntiProfile; // get

    const redCombinedPayload = LoginADconstants.ACT_ONELOGINOBJ;

    const redAuthenType = LoginADconstants.ACT_LOGINAD;
    const redAuthorType = LoginADconstants.ACT_ADGROUPS;
    const redMsgType = LoginADconstants.ACT_ERRMSG;

    const headers = {
        'Content-Type': 'application/json'
    }

    const actionArray: any = [];

    let isDone: boolean = false;

    axios.post(apiUrlAuthen, request, { headers })
        .then(function (responseAuthen) // logged in or not
        {
            if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("authen ready; has data?");
            if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(responseAuthen);
            actionArray.push(responseAuthen.data);
            if (!responseAuthen.data) {
                if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("authen ready; AD denied");
               localStorage.setItem('userauth', 'no data from AD');
                localStorage.setItem('userid', request.UserName);
               actionArray.push({
                    userName: request.UserName,
                    userMemberOfADgroupList: 'dummy AD group list'
                });
                actionArray.push({
                    userId: 0,
                    userName: 'dummyUID',
                    password: null,
                    firstName: "dummy",
                    lastName: "UID",
                    email: "dummyUID@dsny.nyc.gov",
                    createDate: "0001-01-01T00:00:00",
                    lastLoginDate: "0001-01-01T00:00:00",
                    isActive: 0,
                    roles: null,
                    userRoles: null,
                    authenticationMessage: 'authentication of ' + request.UserName + ' did not work'
                });
                isDone = onSuccess(actionArray, redCombinedPayload, dispatchRef);
                // done with logic; doesn't mean UID passed authentication
                dispatchRef({ type: LoginADconstants.ACT_AUTHINPROGRESS, payload: false })
            }
            else {
                if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("responseAuthen has data");
                localStorage.setItem('userauth', responseAuthen.data);
                localStorage.setItem('userid', request.UserName);

                axios.post(apiUrlAuthor, request, { headers })
                    .then(function (responseADgroups) // AD Groups user ID belongs to
                    {
                        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("responseADgroups has data?");
                        let rdata = responseADgroups.data;
                        rdata.push(
                        {
                            "userName": "sfarkas",
                            "userMemberOfADgroupList": "L130LEADER"
                        });
                        actionArray.push(rdata);// expecting an array of objects in response.data
                        axios.post(apiUrlUserDs, request, { headers })
                            .then(function (responseUserDs) // details about the user kept in AD

                            {
                                if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("responseUserDs has data?");
                                actionArray.push(responseUserDs.data);
                                const refnumber: string = responseUserDs.data.userId;
                                const lclRefNo = refnumber;
                                if (parseInt(lclRefNo) > 0) {
                                    if (lclRefNo.toString().length === 7) {
                                        axios.get(process.env.REACT_APP_PTTS20_API + "/nti/GetProfile/" + lclRefNo)
                                            .then((response: any) => {
                                                const rdata: any = response.data;
                                                actionArray.push(rdata);
                                                isDone = onSuccess(actionArray, redCombinedPayload, dispatchRef);
                                                dispatchRef({ type: LoginADconstants.ACT_AUTHINPROGRESS, payload: false })
                                            }).catch(function (error: string) {
                                                alert(error.toString() + " using (" + lclRefNo + ").");
                                                actionArray.push({ "msg": error.toString() + " using (" + lclRefNo + ")."});
                                                isDone = onError(error.toString(), redAuthorType, dispatchRef);
                                                dispatchRef({ type: LoginADconstants.ACT_AUTHINPROGRESS, payload: false })
                                            })
                                    }
                                    else {
                                         if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("numeric but not 7 digits -- officer")
                                        isDone = onError("numeric but not 7 digits -- officer", redAuthorType, dispatchRef);
                                        dispatchRef({ type: LoginADconstants.ACT_AUTHINPROGRESS, payload: false })
                                    }
                                }
                                else {
                                     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("non-numeric string");
                                    isDone = onError("on-numeric ref number -- officer", redAuthorType, dispatchRef);
                                    dispatchRef({ type: LoginADconstants.ACT_AUTHINPROGRESS, payload: false })
                                };
                            })
                            .catch(function (error: string) {
                                 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("trouble 3 with authentication: " + error);
                                isDone = onError(error, redAuthorType, dispatchRef);
                                dispatchRef({ type: LoginADconstants.ACT_AUTHINPROGRESS, payload: false })
                           });
                    })
                    .catch(function (error: string) {
                         if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("trouble 2 with authentication: " + error);
                        isDone = onError(error, redAuthorType, dispatchRef);
                        dispatchRef({ type: LoginADconstants.ACT_AUTHINPROGRESS, payload: false })
                    })
                    ;
            }

        })
        .catch(function (error: string) {
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("trouble 1 with authentication: " + error);
            isDone = onError(error, redAuthorType, dispatchRef);
            dispatchRef({ type: LoginADconstants.ACT_AUTHINPROGRESS, payload: false })
        })
        ; // order must be url, json input, then header options
    return isDone;

    function onSuccess(success: any, redType: any, dispatchRef: any): boolean {
        if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("auth success payload going to reducer: ", redType);
        dispatchRef({ type: redType, payload: success });
        return true;
    }
    function onError(error: string, redType: any, dispatchRef: any): boolean {
        alert(error);
        dispatchRef({ type: redMsgType, payload: error + ' (trying: ' + redType + ')' });
        return true;
    }
}

/**
* fetch API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
export const fApiPostFetchData = (url: string, request: any, actionType: any, dispatch: any) => async () => {

     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log('just before try in apiPostFetchData');

    const headers = {
        'Content-Type': 'application/json'
    }

    const redMsgType = LoginADconstants.ACT_ERRMSG;
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("try axios  in apiPostFetchData: ");

    axios.post(url, request, { headers })
        .then(function (response) {
            if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("apiPostFetchData response for ", actionType);
            if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(response);
            return onSuccess(response.data); // expecting an array of objects in response.data
        })
        .catch(function (error) {
            return onError(error);
        })
        ; // order must be url, json input, then header options

    function onSuccess(success: any) {
         if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("response from apiPostFetchData for " + url);
         if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(actionType);
         if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(success);

        dispatch({ type: actionType, payload: success });
        // return success;
    }
    function onError(error: any) {
         if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("err response from apiPostFetchData for " + url);
         if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(actionType);
         if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(error);

        dispatch({ type: redMsgType, payload: error + ' (trying: ' + actionType + ')' });
        // return error;
    }
}

function fApiSetIdForOneRequest(url: string, IdPayload: any, actionType: any, dispatch: any) {
    return dispatch({ type: actionType, payload: IdPayload });
}

/**
* fetch API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
function fApiPutData(url: string, request: any, actionType: any) {
    axios.put(url, request)
        // tslint:disable-next-line:only-arrow-functions
        .then(function (response) {
            return (dispatch: any) => dispatch({ type: actionType, payload: response.data });
        })
        // tslint:disable-next-line:only-arrow-functions
        .catch(function (error) {
            return (dispatch: any) => dispatch({ type: actionType, payload: false });
        });
}


/**
* fetch API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
function fApiPutDataWithHeader(url: string, options: any, callBack: any, callBackFail: any, actionType: any, dropdown?: any) {
    const jwtToken = sessionStorage.getItem('token');
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + jwtToken
    }

    axios.put(url, options, { headers })
        // tslint:disable-next-line:only-arrow-functions
        .then(function (response) {
            if (callBack) {
                callBack(response.data);
            } else if (dropdown) {
                const result = PTTSfunctions.Commonfunctions.parseArraytoReactSelect(response.data);
                return (dispatch: any) => dispatch({ type: actionType, payload: result });
            } else if (actionType) {
                return (dispatch: any) => dispatch({ type: actionType, payload: response.data });
            }
        })
        // tslint:disable-next-line:only-arrow-functions
        .catch(function (error) {
            if (error.response) {
                if (error.response.status === 401) {
                     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("trying to get refresh token");
                    const refreshToken = sessionStorage.getItem('refreshToken');
                    const refreshTokenAPI = USERSESSION.refreshtoken;
                    const requestObject = {
                        "token": jwtToken,
                        "refreshToken": refreshToken
                    }
                    allServices.apiPostDataWithCallback(refreshTokenAPI, requestObject, (data: any) => {
                        sessionStorage.setItem('token', data.token);
                        sessionStorage.setItem('refreshToken', data.refreshToken);
                        return (dispatch: any) => dispatch(allServices.apiPostDataWithHeader(url, options, callBack, callBackFail, actionType));
                    }, () => {
                        alert("show the login component");
                        // redirectToLogin();
                    })
                } else {
                    if (callBackFail) {
                        callBackFail(error)
                    } else if (actionType) {
                        return (dispatch: any) => dispatch({ type: actionType, payload: false });
                    }
                }
            }
        });
}

/**
* fetch API data
* @param request API request Object
* @param url API URL
* @param dispatch Dispact object to dispatch to reducers
* @param actionType ACtion Type for the Actions
*/
function fApiPutDataWithHeaderMultiPartListing(url: string, options: any, callBack: any, callBackFail: any, actionType: any) {
    const jwtToken = sessionStorage.getItem('token');
    const headers = {
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer ' + jwtToken
    }

    const formData = new FormData();

    Object.keys(options).forEach(function (key) {
        if (key !== "ListingPhotos") {
            formData.append(key, options[key]);
        }
    });

    if (options.ListingPhotos) {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < options.ListingPhotos.length; i++) {
            if (options.ListingPhotos[i]) {
                formData.append("ListingPhotos", options.ListingPhotos[i]);
            }
        }
    }



    axios.put(url, formData, { headers })
        // tslint:disable-next-line:only-arrow-functions
        .then(function (response) {
            if (callBack) {
                callBack(response.data);
            } else if (actionType) {
                return (dispatch: any) => dispatch({ type: actionType, payload: response.data });
            }
        })
        // tslint:disable-next-line:only-arrow-functions
        .catch(function (error) {
            if (error.response) {
                if (error.response.status === 401) {
                     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("trying to get refresh token");
                    const refreshToken = sessionStorage.getItem('refreshToken');
                    const refreshTokenAPI = USERSESSION.refreshtoken;
                    const requestObject = {
                        "token": jwtToken,
                        "refreshToken": refreshToken
                    }
                    allServices.apiPostDataWithCallback(refreshTokenAPI, requestObject, (data: any) => {
                        sessionStorage.setItem('token', data.token);
                        sessionStorage.setItem('refreshToken', data.refreshToken);
                        return (dispatch: any) => dispatch(allServices.apiPostDataWithHeader(url, options, callBack, callBackFail, actionType));
                    }, () => {
                        alert("show the login component");
                        // redirectToLogin();
                    })
                } else {
                    if (callBackFail) {
                        callBackFail(error)
                    } else if (actionType) {
                        return (dispatch: any) => dispatch({ type: actionType, payload: false });
                    }
                }
            }
        }
        );
}


export const allServices = {
    apiGetDataWithCallback: fApiGetDataWithCallback,
    apiPostDataWithCallback: fApiPostDataWithCallback,
    apiGetData: fApiGetData,
    apiGetNtiProfile: fApiGetNtiProfile,
    apiGetDataForDropDownIa: fApiGetDataForDropDownIa,
    apiFetchDropdownData: fApiFetchDropdownData,
    apiPostDataWithHeader: fApiPostDataWithHeader,
    apiDELETEDataWithHeader: fApiDELETEDataWithHeader,
    apiGETDataWithHeader: fApiGETDataWithHeader,
    apiPostFetchAuthenAuthorDetails: fApiPostFetchAuthenAuthorDetails,
    apiPostFetchData: fApiPostFetchData,
    apiSetIdForOneRequest: fApiSetIdForOneRequest,
    apiPutData: fApiPutData,
    apiPutDataWithHeader: fApiPutDataWithHeader,
    apiPutDataWithHeaderMultiPartListing: fApiPutDataWithHeaderMultiPartListing

}