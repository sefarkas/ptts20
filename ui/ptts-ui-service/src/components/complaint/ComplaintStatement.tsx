﻿import React from 'react';
import { useFormContext } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import { Button } from "@material-ui/core";
import styles from "./complaintStyles";
import { Complainant } from "../complaint";
import { withStyles } from '@material-ui/core/styles';
import { handleInputChange } from 'react-select/src/utils';
//import { StyledButton } from "../basicComponents";

const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

interface Props {
	complainants: any;
	complainantOptions: any;
	districtOptions: any;
	titleOptions: any;
	handleComplainantChange: (e: any, control?: string, idx?: number) => void;
	handleInputChange: (e: any, caller: string) => void;
	handleAddComplainant: (event: any) => void;
	handleRemoveComplainant: (event: any, idx: number) => void;	
	handleOpenTemplate: () => void;
	handleComplainantRefDataChange: (title: string, location: string, idx: number) => void;
}

export function ComplaintStatement(props: Props) {
	const classes = styles();
	const methods = useFormContext();

	return (
		<Grid container className={classes.grid} spacing={0}>
			<Grid item xs={3}>
				<label className={classes.labelReadOnlyData}>Section 3</label>
				<label className={classes.labelSectionTitle}>Complainant's Statement</label>
			</Grid>	
				{props.complainants.map(function (d: any, idx: number) {
					return (
						<Complainant
							key={idx}
							complainant={d}
							idx={idx}
							districtOptions={props.districtOptions}
							titleOptions={props.titleOptions}
							complainantOptions={props.complainantOptions}
							handleComplainantChange={props.handleComplainantChange}
							handleInputChange={props.handleInputChange}
							handleRemoveComplainant={props.handleRemoveComplainant}
							handleComplainantRefDataChange={props.handleComplainantRefDataChange}/>
					)
				})}
					<Grid container item xs={12} spacing={0}>
						<Grid item xs={3} />
						<Grid item xs={9}>
							<StyledButton
								className={classes.buttonInner}
								variant="outlined"
								color="primary"
								onClick={props.handleAddComplainant}>
								+ Add Statement
				        </StyledButton>
							<StyledButton
								className={classes.buttonInner}
								variant="outlined"
								color="primary"
								onClick={props.handleOpenTemplate}>
								Use Template
				        </StyledButton>
						</Grid>
					</Grid>
				</Grid>	
	);
}

