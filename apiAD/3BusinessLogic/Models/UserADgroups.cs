﻿using System.Collections.Generic;

namespace ONB.Models
{
    public class MemberOfADgroups
    {
        public string UserName
        {
            get; set;
        }
        public List<UserMemberOfThisADgroup> UserMemberOfADgroupList
        {
            get; set;
        }

        public MemberOfADgroups GetADgroups( ONB.Helper.LDAPHelper.ValidateUser objUser )
        {
            MemberOfADgroups lADgroups = new MemberOfADgroups()
            {
                UserName = objUser.UserName,
                UserMemberOfADgroupList = new List<UserMemberOfThisADgroup>(),
            };
            return lADgroups;
        }

    }

    public class UserMemberOfThisADgroup
    {
        public string UserName
        {
            get; set;
        }
        public string UserMemberOfADgroupList
        {
            get; set;
        }
    }


}