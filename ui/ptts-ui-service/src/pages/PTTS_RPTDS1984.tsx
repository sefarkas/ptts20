﻿import { Button, Grid } from "@material-ui/core";
import { Theme, withStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { DS1984Dialog, DS1984Table } from "../components/DS1984";

const StyledButton = withStyles({
	root: {
		background: '#2b995f',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '15 15 15 15px',
	},
	label: {
		color: 'white',
	},
})(Button);


const useStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: 20,
		[theme.breakpoints.down("md")]: {
			paddingTop: 50,
			paddingLeft: 15,
			paddingRight: 15,
		},
	},
	paper: {
		width: "70%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 230,
	},
	buttonContainer: {
		width: "100%",
		display: "flex",
		justifyContent: "flex-end",
	},

	button: {
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	dialog: {
		minHeight: 500
	}
}));

export function PTTSDS1984() {
	const classes = useStyles();
	const [open, setOpen] = React.useState(false);

	const handleClose = () => {
		setOpen(false);
	};

	const handleAddDS1984 = () => {
		setOpen(true);
	};

	return (
		<Grid container className={classes.root}>
			<DS1984Dialog open={open} onClose={handleClose} />
			<Grid item xs={8} >
				<div className={classes.paper} >
					DS1984 List
				</div>
			</Grid>
			<Grid item xs={4}>
				<div className={classes.buttonContainer}>
					<StyledButton
						className={classes.button}
						variant="contained"
						color="secondary"
						onClick={handleAddDS1984}
					>
						+ Add DS1984
					</StyledButton>
				</div>
			</Grid>
			<Grid item xs={12}>
				<DS1984Table />
			</Grid>
		</Grid>
	);
}

export function PTTSDS1984subset(props: any) {
	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("1984 dashboard props are: ", props);

	const classes = useStyles();
	const [open, setOpen] = React.useState(false);

	const DS1983rcpt = !props ? ""
		: !props.match ? ""
			: !props.match.params ? ""
				: props.match.params.DS1983rcpt;

	const handleClose = () => {
		setOpen(false);
	};

	const handleAddDS1984 = () => {
		setOpen(true);
	};

	return (
		<Grid container className={classes.root}>
			<DS1984Dialog open={open} onClose={handleClose} />
			<Grid item xs={8} >
				<div className={ classes.paper } >
					DS1984 List (for DS-1983 <b>{DS1983rcpt}</b>)
				</div>
			</Grid>
			<Grid item xs={4}>
				<div className={classes.buttonContainer}>
					<StyledButton
						className={classes.button}
						variant="contained"
						color="secondary"
						onClick={handleAddDS1984}
					>
						+ Add DS1984
					</StyledButton>
				</div>
			</Grid>
			<Grid item xs={12}>
				<DS1984Table props={DS1983rcpt} />
			</Grid>
		</Grid>
	);
}


