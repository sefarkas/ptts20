﻿import { ECB603Action, ECB603Actions, ECB603 } from "../model";
import createReducer from "./createReducer";

export const ECB603List = createReducer<ECB603[]>([], {
	[ECB603Actions.ADD_ECB603](state: ECB603[], action: ECB603Action) {
		return [...state, action.payload];
	},
	[ECB603Actions.COMPLETE_ECB603](state: ECB603[], action: ECB603Action) {
		// search after ECB603 item with the given id and set completed to true
		return state.map(t =>
			t.iD === action.payload ? { ...t, completed: true } : t
		);
	},
	[ECB603Actions.REVERT2INCOMPLETE_ECB603](state: ECB603[], action: ECB603Action) {
		// search after ECB603 item with the given id and set completed to false
		return state.map(t =>
			t.iD === action.payload ? { ...t, completed: false } : t
		);
	},
	[ECB603Actions.DELETE_ECB603](state: ECB603[], action: ECB603Action) {
		// remove all ECB603 with the given id
		return state.filter(t => t.iD !== action.payload);
	},
});
