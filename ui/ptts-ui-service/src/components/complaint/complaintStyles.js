﻿import { makeStyles } from "@material-ui/styles";

const complaintStyles = makeStyles({
	reactselect: {
		maxWidth: "90%",
		height: 20,
		marginBottom: 15,
	},
	link: {
		color: "green",
		fontSize: 12,
	},
	fileLink: {
		color: "green",
		fontSize: 12,		
		verticalAlign: "middle",
	},
	logo: {
		height: "20px",
		width: "22px",
		verticalAlign: "middle",
		marginLeft: "-2px;"
	},
	deleteIcon: {
		height: "20px",
		width: "25px",
		verticalAlign: "middle",
		marginLeft: "-2px;"
	},
	deleteFileIcon: {
		height: "20px",
		width: "25px",
		verticalAlign: "middle",
		marginLeft: "7px",
		marginTop: "7px",
		marginBottom: "5px"
	},
	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		fontSize: 12,
		fontWeight: 800,
		color: "#2e2e2d",
	},
	labelSectionTitle: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		fontSize: 14,
		fontWeight: 900,
		color: "#2e2e2d",
	},
	labelReadOnlyData: {
		marginBottom: 10,
		marginTop: 10,
		color: "#2e2e2d",
		fontSize: 12,
		fontWeight: 500
	},
	labelX: {
		lineHeight: 2,
		textAlign: "left",
		fontSize: 12,
		fontWeight: 800,
		color: "#2e2e2d",
		width: "90%"
	},
	labelY: {
		lineHeight: 2,
		textAlign: "right",
		fontSize: 12,
		fontWeight: 800,
		color: "#2e2e2d",
		width: "90%"
	},
	labelRemove: {
		lineHeight: 2,
		textAlign: "right",
		fontSize: 12,
		fontWeight: 800,
		color: "#bf1650",
		width: "90%"
	},
	labelRemoveXS3: {
		lineHeight: 2,
		textAlign: "right",
		fontSize: 12,
		fontWeight: 800,
		color: "#bf1650",
		width: "80%"
	},
	labelRemoveFile: {
		lineHeight: 2,	
		textAlign: "left",
		marginTop: "5px",		
		fontSize: 12,
		fontWeight: 800,
		color: "#bf1650",
		width: "90%",
		display: "inline-block",
	},
	labelOptions: {
		fontSize: 13,
		fontWeight: 700,
		color: "#2e2e2d",
		marginLeft: "15px"
	},
	button: {
		margin: "10px",
		color: "green",
		width: "110px",
		height: "35px",
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	buttonInner: {
		marginTop: "10px",
		paddingTop: "0px",
		marginRight: "20px",
		width: "125px",
		height: "30px",
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	buttonTemplate: {
		marginTop: "30px",
		paddingTop: "0px",
		marginRight: "20px",
		width: "125px",
		height: "30px",
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	buttonInnerDelete: {
		marginTop: "10px",
		marginRight: "20px",
		width: "125px",
		height: "30px",
		backgroundColor: "#952717",
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	grid: {
		background: "white",
		marginLeft: "20px",
		marginRight: "20px",
		width: "calc(100% - 40px)",
		paddingTop: "10px",
		paddingLeft: "10px",
		paddingBottom: "10px",
		marginBottom: "7px;"
	},
	gridCell: {
		paddingBottom: "5px",
	},
	gridSpacerCell: {
		paddingBottom: "0px",
	},
	control: {
		backgroundColor: "#edeff0"
	},
	formContainer: {
		backgroundColor: "#edeff0",
		width: "100%",
		fontSize: 12,
		fontWeight: 500
	},
	placeholder: {
		fontSize: 12,
		fontWeight: 500,
		fontFamily: "Roboto, Helvetica, Arial, sans-serif",
	},
	h2: {
		marginTop: "5px",
		marginBottom: "15px",
		marginLeft: "20px",
		fontSize: 26

	},
	h5: {
		marginTop: "5px",
		marginBottom: "10px",
		fontSize: 14
	},
	gridActions: {
		background: "#edeff0",
		marginLeft: "20px",
		marginRight: "20px",
		width: "calc(100% - 40px)",
	},
	divMargin: {
		marginLeft: "10px",
	},
	divOptions: {
		width: "45%",
		backgroundColor: "#edeff0",
	},
	divOptionItem: {
		paddingTop: "7px",
		paddingBottom: "7px",
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	datepicker: {
		width: "90%",
		height: 38,
		marginBottom: 0,
		fontSize: 13,
		borderRadius: 1,
		border: "1px solid",
		borderColor: "#c5c8c5",
		'&:focus': {
			outlineColor: '#2b995f',
		}
	},
	reactselectLarge: {
		width: "95%",
		height: 20,
		marginBottom: 15
	},
	textArea: {
		width: "95%",
		borderColor: "#c5c8c5",
		border: "1px solid",
	},
	dropFileArea: {
		width: "95%",
		borderColor: "#c5c8c5",
		border: "1px dashed",
		minHeight: "120px",
	},
	fileArea: {
		width: "95%",
		borderColor: "#c5c8c5",
		border: "1px dashed",		
	},
	input: {
		display: "block",
		width: "90%",
		borderRadius: 1,
		border: "1px solid",
		borderColor: "#c5c8c5",
		padding: "10px 10px",
		fontSize: 12,
		'&:focus': {
			outlineColor: '#2b995f',
		}
	},
	MuiDropzoneAreaText: {
		marginBottom: "10px"
	},
	checkbox: {
		paddingTop: "0px",
		paddingBottom: "0px"
	},
	checkboxInner: {
		paddingTop: "0px",
		paddingBottom: "0px",
		paddingLeft: "20px",
		paddingRight: "0px"
	},
	error: {
		fontSize: 11,
		color: "#ef1207",
	},
	labelInline: {
		lineHeight: 2,
		textAlign: "left",
		display: "inline-block",
		fontSize: 12,
		fontWeight: 800,
		color: "#2e2e2d",
	},
	hr: {
		width: "97%",
		marginLeft: "1.5%",
		marginRight: "1.5%",
		marginTop: "0",
		marginBottom: "0",
		height: "1px",
		border: "none",
		backgroundColor: "#c5c8c5",
	},	
});

export default complaintStyles;