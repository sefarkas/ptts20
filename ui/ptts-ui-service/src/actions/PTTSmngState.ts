﻿import { tInProgress } from "../reducers";
import { TYPES as APIconstants } from '../constants/constantsAPI';

// ...

function fActionAxiosInProgress(paramPayload: tInProgress, dispatchRef: any): boolean {

	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log('try to logoff');
	try {
		localStorage.removeItem('userauth');
		dispatchRef({ type: APIconstants.ACT_AXIOSNPROGRESS, payload: paramPayload });
		return true;
	}
	catch (error) {
		return false;
	}

}
// ...

export const allActions = {
	AxiosInProgress: fActionAxiosInProgress
}