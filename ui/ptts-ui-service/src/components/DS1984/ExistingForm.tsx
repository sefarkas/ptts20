﻿import { Button } from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useEffect, useState } from "react"; // , useReducer
import { useForm } from "react-hook-form"; // , FormProvider
import { isNullOrUndefined } from 'util';
import { allServices } from '../../services';
import { PTTSfunctions } from '../basicComponents';
import { history } from "../../configureStore";
import { DS1984 } from '../../model';

type postDataResult<T> = {
	success: boolean;
	errors?: { [P in keyof T]?: string[] };
};

type FormData = {
	ds1984ReceiptNumber: string;
};

export function ExistingDS1984Form(props: any) {
	//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props);
	//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props.match.params.onercpt);

	const [isLoading, setLoading] = React.useState(false);

	const [lastgoodrcpt, setlastgoodrcpt] = useState("");
	const [rcpt, setrcpt] = useState(props.match.params.onercpt);

	const [oneDS1984form, setoneDS1984form] = useState<DS1984>(
		{
			ID: 0,
			ReceiptNumber: "",
			District: "",
			ReceivingOfficerName: "",
			ReceivingOfficerTitle: "",
			BoxNumberedFrom: 0,
			BoxNumberedTo: 0,
			ReceivingOfficerSignature: "",
			DistributorName: "",
			DistributorSignature: "",
			DistributorSignedDate: new Date(),
			TaxRegNumber: "",
			FormStatus: "",
			AcknowledgedBy: "",
			ModifiedOn: new Date(),
			ModifiedBy: "",
			RecOfficerRefNo: "",
			DistributorRefNo: "",
			DS1983ReceiptNo: "",
			DistrictId: 0,
			CreatedBy: "",
			CreatedDate: new Date(),
			Paycode: "",
			IsVoid: false
		});

	const [IDState, setIDState] = useState(0);
	const [ReceiptNumberState, setReceiptNumberState] = useState("");
	const [DistrictState, setDistrictState] = useState("");
	const [ReceivingOfficerNameState, setReceivingOfficerNameState] = useState("");
	const [ReceivingOfficerTitleState, setReceivingOfficerTitleState] = useState("");
	const [BoxNumberedFromState, setBoxNumberedFromState] = useState(0);
	const [BoxNumberedToState, setBoxNumberedToState] = useState(0);
	const [ReceivingOfficerSignatureState, setReceivingOfficerSignatureState] = useState("");
	const [DistributorNameState, setDistributorNameState] = useState("");
	const [DistributorSignatureState, setDistributorSignatureState] = useState("");
	const [DistributorSignedDateState, setDistributorSignedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [TaxRegNumberState, setTaxRegNumberState] = useState("");
	const [FormStatusState, setFormStatusState] = useState("");
	const [AcknowledgedByState, setAcknowledgedByState] = useState("");
	const [ModifiedOnState, setModifiedOnState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [ModifiedByState, setModifiedByState] = useState("");
	const [RecOfficerRefNoState, setRecOfficerRefNoState] = useState("");
	const [DistributorRefNoState, setDistributorRefNoState] = useState("");
	const [DS1983ReceiptNoState, setDS1983ReceiptNoState] = useState("");
	const [DistrictIdState, setDistrictIdState] = useState(0);
	const [CreatedByState, setCreatedByState] = useState("");
	const [CreatedDateState, setCreatedDateState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [PaycodeState, setPaycodeState] = useState("");
	const [IsVoidState, setIsVoidState] = useState(false);


	//useEffect(() => {
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useEffect after onChange handle.");
	//	setoneDS1984form({ // take the individual hook const and set each oneDS1984form object properties
	//		ID: IDState,
	//		ReceiptNumber: ReceiptNumberState,
	//		District: DistrictState,
	//		ReceivingOfficerName: ReceivingOfficerNameState,
	//		ReceivingOfficerTitle: ReceivingOfficerTitleState,
	//		BoxNumberedFrom: BoxNumberedFromState,
	//		BoxNumberedTo: BoxNumberedToState,
	//		ReceivingOfficerSignature: ReceivingOfficerSignatureState,
	//		DistributorName: DistributorNameState,
	//		DistributorSignature: DistributorSignatureState,
	//		DistributorSignedDate: DistributorSignedDateState,
	//		TaxRegNumber: TaxRegNumberState,
	//		FormStatus: FormStatusState,
	//		AcknowledgedBy: AcknowledgedByState,
	//		ModifiedOn: ModifiedOnState,
	//		ModifiedBy: ModifiedByState,
	//		RecOfficerRefNo: RecOfficerRefNoState,
	//		DistributorRefNo: DistributorRefNoState,
	//		DS1983ReceiptNo: DS1983ReceiptNoState,
	//		DistrictId: DistrictIdState,
	//		CreatedBy: CreatedByState,
	//		CreatedDate: CreatedDateState,
	//		Paycode: PaycodeState,
	//		IsVoid: IsVoidState,
	//	});
	//},
	//	[
	//		IDState,
	//		ReceiptNumberState,
	//		DistrictState,
	//		ReceivingOfficerNameState,
	//		ReceivingOfficerTitleState,
	//		BoxNumberedFromState,
	//		BoxNumberedToState,
	//		ReceivingOfficerSignatureState,
	//		DistributorNameState,
	//		DistributorSignatureState,
	//		DistributorSignedDateState,
	//		TaxRegNumberState,
	//		FormStatusState,
	//		AcknowledgedByState,
	//		ModifiedOnState,
	//		ModifiedByState,
	//		RecOfficerRefNoState,
	//		DistributorRefNoState,
	//		DS1983ReceiptNoState,
	//		DistrictIdState,
	//		CreatedByState,
	//		CreatedDateState,
	//		PaycodeState,
	//		IsVoidState,
	//	]
	//);

	const handleOnChange = (e: any) => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
		switch (e.target.name) {
			case "fldID": setIDState(e.target.value); break;
			case "fldReceiptNumber": setReceiptNumberState(e.target.value); break;
			case "fldDistrict": setDistrictState(e.target.value); break;
			case "fldReceivingOfficerName": setReceivingOfficerNameState(e.target.value); break;
			case "fldReceivingOfficerTitle": setReceivingOfficerTitleState(e.target.value); break;
			//case "fldBoxNumberedFrom": setBoxNumberedFromState(e.target.value); break;
			//case "fldBoxNumberedTo": setBoxNumberedToState(e.target.value); break;
			case "fldReceivingOfficerSignature": setReceivingOfficerSignatureState(e.target.value); break;
			case "fldDistributorName": setDistributorNameState(e.target.value); break;
			case "fldDistributorSignature": setDistributorSignatureState(e.target.value); break;
			case "fldDistributorSignedDate": setDistributorSignedDateState(e.target.value); break;
			case "fldTaxRegNumber": setTaxRegNumberState(e.target.value); break;
			case "fldFormStatus": setFormStatusState(e.target.value); break;
			case "fldAcknowledgedBy": setAcknowledgedByState(e.target.value); break;
			case "fldModifiedOn": setModifiedOnState(e.target.value); break;
			case "fldModifiedBy": setModifiedByState(e.target.value); break;
			case "fldRecOfficerRefNo": setRecOfficerRefNoState(e.target.value); break;
			case "fldDistributorRefNo": setDistributorRefNoState(e.target.value); break;
			case "fldDS1983ReceiptNo": setDS1983ReceiptNoState(e.target.value); break;
			case "fldDistrictId": setDistrictIdState(e.target.value); break;
			case "fldCreatedBy": setCreatedByState(e.target.value); break;
			case "fldCreatedDate": setCreatedDateState(e.target.value); break;
			case "fldPaycode": setPaycodeState(e.target.value); break;
			case "fldIsVoid": setIsVoidState(e.target.value); break;


			case "fldBoxNumberedFrom":
				let dummy2: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				setBoxNumberedFromState(dummy2);
				let updatedTktNo2 = dummy2 + 25 - 1;
				dummy2 > 9999999 && 25 > 0 ? setBoxNumberedToState(updatedTktNo2) : setBoxNumberedToState(0);
				dummy2 = 0;
				break;

			default:
				// code block
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.value);
		}
	}


	useEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("initial useEffect triggered for " + rcpt);
		if (!isNullOrUndefined(rcpt)) {
			if (rcpt.length > 0 && rcpt !== lastgoodrcpt) {
				setlastgoodrcpt(rcpt);
				(async () => {
					setLoading(true);
					const response = await allServices.PTTSgetServices.anDS1984(rcpt);
					const rspJson = await response.data;
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("api response for ", rcpt);
					 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rspJson);
					if (!isNullOrUndefined(rspJson)) {
						if (rspJson.BoxNumberedFrom !== undefined) {
							setIDState(rspJson.ID);
							setReceiptNumberState(rspJson.ReceiptNumber);
							setDistrictState(rspJson.District);
							setReceivingOfficerNameState(rspJson.ReceivingOfficerName);
							setReceivingOfficerTitleState(rspJson.ReceivingOfficerTitle);
							setBoxNumberedFromState(rspJson.BoxNumberedFrom);
							setBoxNumberedToState(rspJson.BoxNumberedTo);
							setReceivingOfficerSignatureState(rspJson.ReceivingOfficerSignature);
							setDistributorNameState(rspJson.DistributorName);
							setDistributorSignatureState(rspJson.DistributorSignature);
							setDistributorSignedDateState(rspJson.DistributorSignedDate);
							setTaxRegNumberState(rspJson.TaxRegNumber);
							setFormStatusState(rspJson.FormStatus);
							setAcknowledgedByState(rspJson.AcknowledgedBy);
							setModifiedOnState(rspJson.ModifiedOn);
							setModifiedByState(rspJson.ModifiedBy);
							setRecOfficerRefNoState(rspJson.RecOfficerRefNo);
							setDistributorRefNoState(rspJson.DistributorRefNo);
							setDS1983ReceiptNoState(rspJson.DS1983ReceiptNo);
							setDistrictIdState(rspJson.DistrictId);
							setCreatedByState(rspJson.CreatedBy);
							setCreatedDateState(rspJson.CreatedDate);
							setPaycodeState(rspJson.Paycode);
							setIsVoidState(rspJson.IsVoid);
						}
					}
					setLoading(false);
				})();
			}
		}
	}, []);

	useEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useEffect triggered for " + rcpt);
		if (!isNullOrUndefined(rcpt)) {
			if (rcpt.length > 0 && rcpt !== lastgoodrcpt) {
				setlastgoodrcpt(rcpt);
				(async () => {
					setLoading(true);
					const response = await allServices.PTTSgetServices.anDS1984(rcpt);
					const rspJson = await response.data;
					if (!isNullOrUndefined(rspJson)) {
						if (rspJson.BoxNumberedFrom !== undefined) {
							setoneDS1984form({
								ID: rspJson.ID,
								ReceiptNumber: rspJson.ReceiptNumber,
								District: rspJson.District,
								ReceivingOfficerName: rspJson.ReceivingOfficerName,
								ReceivingOfficerTitle: rspJson.ReceivingOfficerTitle,
								BoxNumberedFrom: rspJson.BoxNumberedFrom,
								BoxNumberedTo: rspJson.BoxNumberedTo,
								ReceivingOfficerSignature: rspJson.ReceivingOfficerSignature,
								DistributorName: rspJson.DistributorName,
								DistributorSignature: rspJson.DistributorSignature,
								DistributorSignedDate: rspJson.DistributorSignedDate,
								TaxRegNumber: rspJson.TaxRegNumber,
								FormStatus: rspJson.FormStatus,
								AcknowledgedBy: rspJson.AcknowledgedBy,
								ModifiedOn: rspJson.ModifiedOn,
								ModifiedBy: rspJson.ModifiedBy,
								RecOfficerRefNo: rspJson.RecOfficerRefNo,
								DistributorRefNo: rspJson.DistributorRefNo,
								DS1983ReceiptNo: rspJson.DS1983ReceiptNo,
								DistrictId: rspJson.DistrictId,
								CreatedBy: rspJson.CreatedBy,
								CreatedDate: rspJson.CreatedDate,
								Paycode: rspJson.Paycode,
								IsVoid: rspJson.IsVoid,
							});
						}
					}
					setLoading(false);

				})();
			}
		}
	}, [lastgoodrcpt]);

	const { register, handleSubmit, errors, formState } = useForm<FormData>();

	const submitForm = async (data: FormData, e: any) => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Submission starting", oneDS1984form);

		const response = await allServices.PTTSiudServices.iudDS1984(oneDS1984form);
		// response is the new receipt number
		if (!response) {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("I am having trouble with saving DS1984 values.");
			// any error message is in the CustomerOrderNo property in the controller
		}
		else {
			history.push('/PTTS/existingDS1984/' + oneDS1984form.ReceiptNo);
		}
		e.target.reset(); // reset after form submit
	};

	const onCancel = () => {
		history.push('/PTTS/home');
	};

	const newDS1984 = () => {
		history.push('/PTTS/newDS1984');
	};

	const newDS1986 = (rcpt84: any) => {
		if (!rcpt84) {
			alert("PTTS does not know which DS-1984 to associate with a new DS-1986.")
		}
		else {
			history.push('/PTTS/newDS1986', rcpt84);
		}
        
		
	};

	const classes = useStyles();

	if (isLoading) {
		return (<div>Loading ...</div>);
	}
	else {

		const renderActionButtons = () => {
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("oneDS1984form.DS1984ReceiptNo is ", oneDS1984form.DS1984ReceiptNo);
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(oneDS1984form.DS1984ReceiptNo);
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(!isNullOrUndefined(oneDS1984form.DS1984ReceiptNo));
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(!oneDS1984form.DS1984ReceiptNo ? 0 : oneDS1984form.DS1984ReceiptNo.length);
			if (!isNullOrUndefined(oneDS1984form.DS1984ReceiptNo)) // prevent save/update
			{
				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							type="submit"
						>
							Update
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="secondary"
							onClick={() => newDS1984()}
						>
							Make new DS1984
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="secondary"
							onClick={() => newDS1986(oneDS1984form.DS1984ReceiptNo)}
						>
							Make new DS1986
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}
			else if (PTTSfunctions.Commonfunctions2.isNotDefined(oneDS1984form.DS1984ReceiptNo)) // prevent save/update
			{
				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							type="submit"
						>
							Save new
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={() => newDS1984()}
						>
							Make new DS1984
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}
			else {

				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							type="submit"
						>
							Update
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}

		};

		return (
			<form onSubmit={handleSubmit(submitForm)}>
				<Grid className={classes.formposition}>
					<Grid container className={classes.darkborder}>
						<Grid item xs={2} className={classes.styleforlogo}>
							<img src={PTTSfunctions.Logos.DSNYLogo.logoImg} alt="Sanitation Enforcement" />
						</Grid>
						<Grid item xs={9} className={classes.styleforformtitle}>
							<div className={classes.styleforformtitletext}>THE CITY OF NEW YORK Department of Sanitation</div>
							<div className={classes.styleforformtitletext}>RECEIPT NO:&nbsp;
							<div className={classes.styleforformreceipttextred}>{oneDS1984form.ReceiptNumber}</div></div>
							<div className={classes.styleforformtitletext}>ECB - SUMMONS ISSUED RECEIPT  DS 1984 (4-95)</div>
						</Grid>
					</Grid>
					<div>&nbsp;</div>
					<div>&nbsp;</div>
					<Grid container>
						<Grid item xs={12}>
							{renderActionButtons()}
						</Grid>
					</Grid>
				</Grid>
				<Grid className={classes.formposition}>
					<Grid container className={classes.darkborder}>
						<Grid item xs={12} >
							<div className={classes.stylefortopacknowledgementtext}>I,&nbsp;
							<i>{ReceivingOfficerNameState}</i>&nbsp;working at&nbsp;<i>{ReceivingOfficerTitleState}</i>&nbsp;
							</div>
							<div className={classes.stylefortopacknowledgementtext}><br />&nbsp;assigned to this District or location:&nbsp;{DistrictState}
							</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={12} className={classes.stylefortopacknowledgement}>
							<div className={classes.stylefortopacknowledgementtext}>
								<div>
									&nbsp;acknowledged receipt of 25 (ECB) Notice of Violations 
									</div>
								<div>&nbsp;numbered from:&nbsp;
								<input className={classes.styleforticketrangetext} type="number" onChange={(e) => handleOnChange(e)}
									name="fldBoxNumberedFrom"
									value={BoxNumberedFromState}
								// defaultValue versus value --> value lets me change characters in the currently active control textbox
								/>
								&nbsp;to:&nbsp;
								<input className={classes.styleforticketrangetext} type="number" onChange={(e) => handleOnChange(e)}
									name="fldBoxNumberedTo" ref={register}
									value={BoxNumberedToState}
									/>
								</div>
							</div>
						</Grid>
					</Grid>
					<Grid item xs={12} className={classes.darkborder}>
						<div className={classes.stylefortopacknowledgementtext}>
							<div>
								&nbsp;Notice of Violations must be issued in pre-wrapped packs of 25.
								</div>
							<div>
								&nbsp;All Notice of Violations (issued) will be written in numerical order.
								</div>
						</div>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={4}>
							<Grid container>
								<Grid item xs={12} >

									<div className={classes.stylefortopdistributionrow} >DISTRIBUTOR'S NAME</div>
									<div >
										<input className={classes.stylefortopdistributionrowtextwide}
											type="text" onChange={(e) => handleOnChange(e)}
											name="fldDistributorName" ref={register}
											value={DistributorNameState}
										/>
									</div>
								</Grid>
								<Grid item xs={12} >
									<div >SIGNATURE</div>
								</Grid>
							</Grid>
						</Grid>
						<Grid item xs={2}>
							<div >DATE</div>
							<div >
								<input className={classes.stylefortopdistributionrowtext}
									type="text" onChange={(e) => handleOnChange(e)}
									name="fldDistributorSignedDate" ref={register}
									value={PTTSfunctions.Commonfunctions.formatDateSlashes((DistributorSignedDateState))}
								/>
							</div>
						</Grid>
						<Grid item xs={4}  >
							<Grid container>
								<Grid item xs={12}>
									<div className={classes.stylefortopdistributionrow}>RECEIVING OFFICER'S NAME</div>
									<div className={classes.thinborderLeft} >
										<input className={classes.stylefortopdistributionrowtextwide}
											type="text" onChange={(e) => handleOnChange(e)}
											name="fldReceivingOfficerName" ref={register}
											value={ReceivingOfficerNameState}
										/>
									</div>
								</Grid>
								<Grid container className={classes.thinborderTop}>
									<Grid item xs={6}>
										<div >SIGNATURE</div>
									</Grid>
									<Grid item xs={6} className={classes.thinborderLeft}>
										<div className={classes.stylefortopdistributionrow}>TITLE</div>
										<div >
											<input className={classes.stylefortopdistributionrowtext}
												type="text" onChange={(e) => handleOnChange(e)}
												name="fldReceivingOfficerTitleState" ref={register}
												value={ReceivingOfficerTitleState}
											/>
										</div>
									</Grid>
								</Grid>

							</Grid>
						</Grid>
						<Grid item xs={2}>
							<Grid container>
								<Grid item xs={12} className={classes.thinborderLeft}>
									<div className={classes.stylefortopdistributionrow}>TAX REG. #</div>
									<div className={classes.thinborderLeft}>
										<input className={classes.stylefortopdistributionrowtext} type="text" onChange={(e) => handleOnChange(e)} name="fldBadgeNo" ref={register}
											value={TaxRegNumberState}
										/>
									</div>
								</Grid>
								<Grid item xs={12} className={classes.thinborderTop}>
									<div className={classes.stylefortopdistributionrow} >DATE</div>
									<div className={classes.thinborderLeft}>
										<input className={classes.stylefortopdistributionrowtext} type="text" onChange={(e) => handleOnChange(e)} name="fldMessengerSignedDate" ref={register}
											value={PTTSfunctions.Commonfunctions.formatDateSlashes((ModifiedOnState))}
										/>
									</div>
								</Grid>
							</Grid>
						</Grid>
					</Grid>
					<Grid item xs={12} className={classes.stylefortopacknowledgement}>
						<div className={classes.stylefortopacknowledgementtext}>
							<StyledButton
								className={classes.button}
								variant="outlined"
								color="secondary"
								type="submit"
							>
								Give 25 tickets to { TaxRegNumberState }
							</StyledButton>
							<StyledButton
								className={classes.button}
								variant="outlined"
								color="primary"
								onClick={onCancel}>
								Edit
						</StyledButton>
						</div>
					</Grid>
					<Grid item xs={12}>
						{renderActionButtons()}
					</Grid>
				</Grid>

			</form>
		)
	}

};


const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5,
		marginTop: 0
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const useStyles = makeStyles({
	root: {
		flexGrow: 1,
		height: "100%",
		paddingLeft: 15,
		paddingRight: 15,

	},
	formposition: {
		marginLeft: 280,
	},
	darkborder: {
		borderWidth: 3,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderTop: {
		borderTopWidth: 2,
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderLeft: {
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderTopWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	styleforlogo: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforsubmit: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforformtitle: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
	},
	styleforformtitletext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "black",
		minWidth: 500,
	},
	styleforformorder: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "center",
	},
	styleforformordertext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "center",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 500,
		color: "black",
	},
	styleforticketrange: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
	},
	stylefortopacknowledgement: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	stylefortopacknowledgementtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 320,
		minWidth: 130
	},
	stylefortopdistributionrow: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
		maxWidth: 300,
		wordWrap: "break-word",
	},
	stylefortopdistributionrowtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
		wordWrap: "break-word",
	},
	stylefortopdistributionrowtextwide: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		minWidth: 300,
		maxWidth: 600,
	},
	styleforformreceipt: {
		verticalAlign: "center",
		borderTopStyle: "none",
	},
	styleforformreceipttext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		display: "inline",
		marginLeft: 10,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
		textAlign: "center",
	},
	styleforformreceipttextred: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "red",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
	},
	button: {
		marginTop: 20,
	},

	formstyle: {
		maxWidth: "100%",
		padding: 0,
	},

	input: {
		display: "block",
		borderRadius: 4,
		border: "1px solid",
		padding: "10px 15px",
		marginBottom: 10,
		fontSize: 14,
		maxWidth: 400,
		minWidth: 200,
	},

	reactselect: {
		maxWidth: 400,
	},

	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginBottom: 13,
		marginTop: 20,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
	},
	buttonSubmit: {
		background: "#0e8c1b",
		color: "white",
		border: "none",
		marginTop: 20,
		padding: 20,
		fontSize: 16,
		fontWeight: 100,
		width: 200,
	}
});
