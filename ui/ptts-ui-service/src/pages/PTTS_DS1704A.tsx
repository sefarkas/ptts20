﻿import * as React from 'react';
import { Fragment, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { allActions } from '../actions';
import { PTTSfunctions } from '../components/basicComponents';
import Loader from '../components/basicComponents/Loader';
import { DS1704AforEditing, DS1704Asingle, DS1704Atable, Instructions, PageForDS1704BulkForDS21 } from '../components/DS1704';
import { DS1984, tDS1704 } from '../model';
import { dummies } from '../reducers';
import { allServices } from '../services';

export function PageForDS1704A(props: any) {

    const dispatch = useDispatch();
    const contentparent1704: any = useSelector((state: any) => state.DS1704Reducer);
    //console.log("PageForDS1704A props", props);
    const [DS1984rcpt, setDS1984rcpt] = useState(!props.match.params.oneDS1984rcpt ? "" : props.match.params.oneDS1984rcpt); // match.params because of using ROUTER to get here
    const [NOVnumber, setNOVnumber] = useState(!props.match.params.oneNOVnumber ? "" : props.match.params.oneNOVnumber); // match.params because of using ROUTER to get here
    const [DS1704s, setDS1704s] = useState<tDS1704[]>(!contentparent1704.ST1704rows ? [] : contentparent1704.ST1704rows);
    const [oneDS1704, setoneDS1704] = useState<tDS1704>(!DS1704s ? dummies.dummyDS1704 : DS1704s[0])
    const [isLoading, setLoading] = useState(!oneDS1704 ? true : oneDS1704.ECBSummonsNumber !== NOVnumber);

    useEffect(() => {
        console.log("launch PageForDS1704A async");
        if (!!NOVnumber) {
            (async () => {
                await allServices.PTTSgetServices.oneDS1704(NOVnumber, cb, er); // axios always returns an object with a "data" child
            })();
        }
        if (!!DS1984rcpt) {
            setLoading(false);
        }
     }, []);

    const cb = (response: any) => {
        let rdata1704: tDS1704 = !response.data ? dummies.dummyDS1704 : response.data; // axios always returns an object with a "data" child
        if (rdata1704.ECBSummonsNumber === NOVnumber) {
            dispatch(allActions.DS1704acts.addDS1704(rdata1704, dispatch));
        }

        setoneDS1704(rdata1704);
        setLoading(false); // either way, I have what I need from the DB
    }

    const er = (response: any) => {
        alert("PageForDS1704A/er: " + response);
        let rdata1704: tDS1704 = dummies.dummyDS1704;
        setoneDS1704(rdata1704);
    }

    try {
            if (isLoading) {
                return (<Loader />);
            }
            else if (!!DS1984rcpt) {
                return ( // big prop is expected to be a DS1984 receipt number
                <Fragment>
                    <PageForDS1704BulkForDS21 DS1984rcpt={DS1984rcpt} />
                </Fragment >
                );
            }
            //else if (!PTTSfunctions.Commonfunctions2.isNotDefined(NOVnumber) && NOVnumber.length > 9) {
            //    return ( // big prop NOVnumber is expected to be a DS1984 receipt number; list all 25
            //        <Fragment>
            //            <Instructions />
            //            <DS1704Atable DS1984rcpt={ NOVnumber } />
            //        </Fragment >
            //    );
            //}
            else if (PTTSfunctions.Commonfunctions2.isNotDefined(NOVnumber) || NOVnumber.length !== 9) {
                return ( // prop is unexpected, make a page to enter NOV#s
                    <Fragment>
                        <Instructions />
                        <DS1704Atable />
                    </Fragment >
                );
            }
            else {
                if (!oneDS1704) {
                    return ( // make a new 1704 for NOV#
                        <Fragment>
                            <DS1704AforEditing aNovNumber={NOVnumber} />
                        </Fragment>
                    );
                }
                else if (oneDS1704.ECBSummonsNumber !== NOVnumber) {
                    return ( // make a new 1704 for NOV#
                        <Fragment>
                            <DS1704AforEditing aNovNumber={NOVnumber} />
                        </Fragment>
                    );
                }
                else {
                    return ( // found 1704 for NOV#
                        <Fragment> 
                            <DS1704Asingle aNovNumber={NOVnumber} />
                        </Fragment>
                    );
                }

            }}
    catch (error) {
        return ( // problem with 1704 for NOV#
            <Fragment>
                <div>{error} while trying to get DS1704 for {NOVnumber} </div>
            </Fragment>
        );
    }

}


