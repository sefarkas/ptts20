﻿import { Button, Grid } from "@material-ui/core";
import { Theme, withStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { DS1986Dialog, DS1986Table } from "../components/DS1986";

const StyledButton = withStyles({
	root: {
		background: '#2b995f',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '15 15 15 15px',
	},
	label: {
		color: 'white',
	},
})(Button);


const useStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: 20,
		[theme.breakpoints.down("md")]: {
			paddingTop: 50,
			paddingLeft: 15,
			paddingRight: 15,
		},
	},
	paper: {
		width: "70%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 230,
	},
	buttonContainer: {
		width: "100%",
		display: "flex",
		justifyContent: "flex-end",
	},

	button: {
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	dialog: {
		minHeight: 500
	}
}));

export function PTTSDS1986() {
	const classes = useStyles();
	const [open, setOpen] = React.useState(false);

	const handleClose = () => {
		setOpen(false);
	};

	const handleAddDS1986 = () => {
		setOpen(true);
	};

	return (
		<Grid container className={classes.root}>
			<DS1986Dialog open={open} onClose={handleClose} />
			<Grid item xs={8} >
				<div className={classes.paper} >
					DS1986 List
				</div>
			</Grid>
			<Grid item xs={4}>
				<div className={classes.buttonContainer}>
					<StyledButton
						className={classes.button}
						variant="contained"
						color="secondary"
						onClick={handleAddDS1986}
					>
						+ Add DS1986
					</StyledButton>
				</div>
			</Grid>
			<Grid item xs={12}>
				<DS1986Table />
			</Grid>
		</Grid>
	);
}

export function PTTSDS1986subset(props: any) {
	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("1986 dashboard props are: ", props);

	const classes = useStyles();
	const [open, setOpen] = React.useState(false);

	const DS1984rcpt = !props ? ""
		: !props.match ? ""
			: !props.match.params ? ""
				: props.match.params.DS1984rcpt;

	const handleClose = () => {
		setOpen(false);
	};

	const handleAddDS1986 = () => {
		setOpen(true);
	};

	return (
		<Grid container className={classes.root}>
			<DS1986Dialog open={open} onClose={handleClose} />
			<Grid item xs={8} >
				<div className={classes.paper} >
					DS1986 List (for DS-1984 <b>{DS1984rcpt}</b>)
				</div>
			</Grid>
			<Grid item xs={4}>
				<div className={classes.buttonContainer}>
					<StyledButton
						className={classes.button}
						variant="contained"
						color="secondary"
						onClick={handleAddDS1986}
					>
						+ Add DS1986
					</StyledButton>
				</div>
			</Grid>
			<Grid item xs={12}>
				<DS1986Table props={DS1984rcpt} />
			</Grid>
		</Grid>
	);
}


