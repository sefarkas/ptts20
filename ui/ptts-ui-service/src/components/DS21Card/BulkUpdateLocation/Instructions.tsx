﻿import { Grid, makeStyles, Paper } from '@material-ui/core';
import React from 'react';


export function Instructions() {
	const classes = useStyles();

	return (
		<div className={classes.styleforBorder}>
			<Grid item xs={6} component={Paper} className={classes.styleforInstructions}>
				<div >
					INSTRUCTION : <br />
					<ol className={classes.styleforOL}>
						<li>Enter the ticket number in the "Enter Ticket No." column.</li>
						<li>Press the "Tab" button on the keyboard to enter the next ticket number.</li>
						<li>Select the ticket status (at the bottom of this page) from the drop down list <br />after all the tickets has been entered. </li>
						<li>Click on the "Update" button to update the tickets entered.</li>
						<li>If the "Ticket Status" has not been selected then <b>"Please select the ticket status to update tickets."</b> error message will be displayed.</li>
						<li>If the tickets are updated then successful message will be displayed next to every ticket number.<br />
						e.g., <i>"Ticket number 1234567890 updated to status 'Mail Room Has Ticket Possession' successfully."</i></li>
						<li>Click on "Clear All" button to reset the form for new entry.</li>
					</ol>
				</div>
			</Grid>
			<Grid item xs={6} component={Paper} className={classes.paper}>
				&nbsp;
			</Grid>
		</div>
	)
}

const useStyles = makeStyles({
	paper: {
		width: "100%",
		display: "block",
		fontFamily: "Arial",
		fontSize: "large",
		fontWeight: 400,
		marginLeft: 360,
		overflowX: "visible",
	},
	styleforInstructions: {
		marginBottom: 13,
		marginTop: 10,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 12,
		fontWeight: "normal",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "red",
		maxWidth: 550,
		minWidth: 300,
	},
	styleforOL: {
		listStyleType: "decimal"
	},
	styleforBorder: {
		borderStyle: "solid",
		borderWidth: 2,
	},
	styleforTab: {
		marginLeft: '.5rem'
	}
});