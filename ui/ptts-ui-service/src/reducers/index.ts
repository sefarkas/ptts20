import { History } from "history";
import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import { Action, combineReducers } from "redux";
import { KnownActionDS1704, KnownActionDS21Card, KnownActionDS21HDR } from "../actions";
import { KnownActionNTI } from '../actions/actionsNTIprofile';
import { KnownActionAD } from '../actions/LoginAD';
import { TYPES as LoginADconstants } from '../constants/constantsLoginAD';
import { TYPES as NTIconstants } from '../constants/constantsNTIprofile';
import { Complaint, tADgroup, tUserDetails, tUserNTIdetails, userAction } from "../model";
import { DS1704Reducer as vR, dummyDS1704, dummyDS1704row } from "./DS1704";
import { DS1984Reducer as d84R, dummyDS1984 } from "./DS1984";
import {
    DS21CardReducer as cR, dummyDS21Card, dummyNOV, dummyBulkUpdateThisNOV,
    dummyDSspecialDistrict, dummyViolationCode, dummyServiceType, dummyTicketStatusOption
} from "./DS21Card";
import { DS21HDRReducer as hR, dummyDS21HDR, dummyDS21HDRefIUD } from "./DS21HDR";
import { dummyNTIp } from "./reducerNTI";

export * from "./ECB603";

const dummyDetails: tUserDetails = {
    aDgroups: null,
    authenticated: false,
    authenticationMessage: "init",
    createDate: "0001-01-01T00:00:00",
    email: "webDev@dsny.nyc.gov",
    firstName: "Web",
    isActive: 0,
    lastLoginDate: "0001-01-01T00:00:00",
    lastName: "developer",
    password: null,
    roles: null,
    userId: 0,
    userName: LoginADconstants.WEBDEV,
    userRoles: null,
    voiceTelephoneNumber: "(212) 555-1212",
}

export const dummies = {
    dummyDS21HDR, dummyDS21Card, dummyNOV, dummyNTIp, dummyDetails,
    dummyDS21HDRefIUD, dummyDS1704, dummyDS1704row, dummyBulkUpdateThisNOV, dummyDS1984,
    dummyDSspecialDistrict, dummyViolationCode, dummyServiceType, dummyTicketStatusOption
}

type KnownAction = KnownActionAD | KnownActionNTI | KnownActionDS21HDR | KnownActionDS21Card | KnownActionDS1704;

export const inThisInstant: any = new Date().toLocaleDateString();

export type tInProgress = {
    url: string;
    inProgress: boolean;
}

export type tGiantState = {
    complaintList: Complaint[];
    loggedIn: boolean;
	//ecb603List: ECB603[];
    ST_AXIOSINPROGRESS: tInProgress[];
    ST_AUTHINPROGRESS: boolean;
    ST_LOGINAD: boolean;
    ST_DELIBERATELOGOUT: boolean;
    ST_USERDETAILS: tUserDetails;
    ST_ADGROUPS: tADgroup[];
    ST_MSG: string;
    ST_FAILEDLOGINATTEMPTS: number;

    ST_NTIPROFILE: tUserNTIdetails;
    ST_NTIMSG: string;

    //ST_25NOVS: tOneNOV[];
    //ST_DS21HDRATTRIBUTES: tDS21Card;
    //ST_DS21CardMSG: string;

    //ST_DS21HDRrows: DS21HDR[],
    //ST_DS21HDRmsg: string,


}

export const giantState: tGiantState = {
    complaintList: [],

    loggedIn: false,
    ST_AXIOSINPROGRESS: [],
    ST_AUTHINPROGRESS: false,
    ST_LOGINAD: false,
    ST_DELIBERATELOGOUT: false,
    ST_USERDETAILS: dummyDetails,
    ST_ADGROUPS: [{
        userName: 'xyz',
        userMemberOfADgroupList: 'xyz'
    }],
    ST_MSG: '',
    ST_FAILEDLOGINATTEMPTS: 0,

    ST_NTIPROFILE: dummyNTIp,
    ST_NTIMSG: '',

    // ST_DS21HDRrows: [dummyDS21HDR],
    // ST_DS21HDRmsg: '',

     //ST_25NOVS: [],
     //ST_DS21HDRATTRIBUTES: dummies.dummyDS21Card,
     //ST_DS21CardMSG: '',

}

export const gR = (state: tGiantState
    | undefined = giantState
    , incomingAction: Action) => {

    const action = incomingAction as KnownAction;
     if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("giantReducer hit. Looking for " + action.type);

    switch (action.type) {

        case userAction.LOGIN_REQUEST:
            return {
                ...state, loggingIn: true,

            };
        case userAction.LOGIN_SUCCESS:
            return {
                ...state, loggedIn: true,
            };
        case userAction.LOGIN_FAILURE:
            return { ...state, loggedIn: false };
        case userAction.LOGOUT:
            return { ...state, loggedIn: false };
        case LoginADconstants.ACT_AUTHINPROGRESS:
            return {
                ...state,
                ST_AUTHINPROGRESS: action.payload
            }
        case LoginADconstants.ACT_ONELOGINOBJ:
            return {
                ...state, ST_LOGINAD: action.payload[0] // expected either true, false, or undefined 
                , ST_ADGROUPS: action.payload[1]
                , ST_USERDETAILS: action.payload[2]
                , ST_NTIPROFILE: action.payload[3]
                , ST_DELIBERATELOGOUT: false // the fact your're trying to log in it no longer matters if you logged out
                , ST_FAILEDLOGINATTEMPTS: action.payload[0] ? 0 : state.ST_FAILEDLOGINATTEMPTS + 1
                , ST_AUTHINPROGRESS: false
            };
        case LoginADconstants.ACT_LOGINAD:
            if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE')
                if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("redLogin for " + action.type + " " + action.payload);
            return {
                ...state, ST_LOGINAD: action.payload, ST_DELIBERATELOGOUT: !action.payload // expected either true, false, or undefined // , ST_TABLE: 'force ONB to pick a new component -- 1'
                , ST_FAILEDLOGINATTEMPTS: action.payload ? 0 : state.ST_FAILEDLOGINATTEMPTS + 1
            };
        case LoginADconstants.ACT_EXPLICITLOGOUTRESET:
            return {
                ...state,
                ST_DELIBERATELOGOUT: action.payload,
            };
        case LoginADconstants.ACT_EXPLICITLOGOUT:
            return {
                ...state, ST_LOGINAD: !action.payload,
                ST_DELIBERATELOGOUT: action.payload,
                ST_REHYDRATING: !action.payload,
                ST_FAILEDLOGINATTEMPTS: action.payload ? 0 : state.ST_FAILEDLOGINATTEMPTS,
                ST_ADGROUPS: null,
                ST_USERDETAILS: null,
            };

        case LoginADconstants.ACT_ADGROUPS:
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("put this payload in ST_ADGROUPS " + action.payload);
            return {

                ...state, ST_ADGROUPS: action.payload // , ST_TABLE: 'force ONB to pick a new component -- 2'
            };
        case LoginADconstants.ACT_USERDETAILS:
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("put this payload in ST_USERDETAILS ");
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(action.payload);
            return {
                ...state, ST_USERDETAILS: action.payload // , ST_TABLE: 'force ONB to pick a new component -- 3'
            };
        case LoginADconstants.ACT_ERRMSG:
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("put this payload in ST_ADGROUPS " + action.payload);
            return {

                ...state, ST_MSG: action.payload
            };

        case NTIconstants.ACT_NTIPROFILE:
              if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("put this payload in ST_NTIPROFILE ");
              if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(action.payload);
            return {
                ...state, ST_NTIPROFILE: action.payload
            };


        //case DS21Actions.aDS21HDRROWS:
        //    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("payload for ", action.type);
        //    if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(action.payload)
        //    return {
        //        ...state, ST_DS21HDRrows: action.payload
        //    }
        //case DS21Actions.HDRmsg:
        //    return { ...state, ST_DS21HDRmsg: "landed in default of the DS21HDR reducer" }

        //case DS21CardActions.UPDATE_DS21CARD:
        //    return {
        //        ...state, ST_DS21HDRATTRIBUTES: action.payload
        //    };
        //case DS21CardActions.aDS21_CARDNOV_TABLE:
        //    return {
        //        ...state, ST_25NOVS: action.payload
        //    };
        //case DS21CardActions.CardMSG:
        //    {
        //        return { ...state, ST_DS21CardMSG: "landed in default of the DS21Card reducer" }
        //    }

        default:
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("landed in default of the giant reducer, action.type was ", action.type);
            return { ...state }
    }
};

export const rootReducer = (history: History) =>
     combineReducers({ // order is important! putting DS21CardReducer at the end broke the login authorization
        // the ... makes the reducer undetectable??
		// ...NTIReducer,
		DS21CardReducer: cR,
		// ...complaintReducer,
		DS21HDRReducer: hR,
		// ...ECB603Reducer,
		// ...authenticationReducer,
		// ...LoginADReducer,
         DS1704Reducer: vR,
         DS1984Reducer: d84R,
        giantReducer :gR,
	});



