﻿export interface DS1986problems {
    [x: string]: any;
    Id: number;
    Code1: string;
    Description: string;
    Type: string;
    OrderSequence: number;
}

export interface DS1986 {
    [x: string]: any;
    ID: number;
    ReferenceNo: string;
    Name: string;
    District: string;
    BadgeNo: string;
    NovasNo: string;
    Ds1984No: string;
    DS1986ErrorId: number;
    DS1986BDate: Date;
    OtherRemarks: string;
    Remarks: string;
    CreatedBy: string;
    CreatedDate: Date;
    ModifiedBy: string;
    ModifiedDate: Date;
    DS1984District: number;
    ReceiptNo: string;
    DS1984DistrictName: string;
}

export interface efDS1986 {
    ID: number;
    ReferenceNo: string;
    Name: string;
    District: string;
    BadgeNo: string;
    NovasNo: string;
    Ds1984No: string;
    DS1986ErrorId: number;
    DS1986BDate: Date;
    OtherRemarks: string;
    Remarks: string;
    CreatedBy: string;
    CreatedDate: Date;
    ModifiedBy: string;
    ModifiedDate: string;
    DS1984District: number;
    ReceiptNo: string;
    DS1984DistrictName: string;
}

export enum DS1986Actions {
    ADD_DS1986 = "ADD_DS1986",
    UPDATE_DS1986 = "UPDATE_DS1986",
    DELETE_DS1986 = "DELETE_DS1986",
    COMPLETE_DS1986 = "COMPLETE_DS1986",
    REVERT2INCOMPLETE_DS1986 = "REVERT2INCOMPLETE_DS1986",
    RESET_DS1986 = "RESET_DS1986List",
}

interface DS1986ActionType<T, P> {
    type: T;
    payload: P;
}

export type DS1986Action =
    | DS1986ActionType<typeof DS1986Actions.ADD_DS1986, DS1986>
    | DS1986ActionType<typeof DS1986Actions.COMPLETE_DS1986, number>
    | DS1986ActionType<typeof DS1986Actions.REVERT2INCOMPLETE_DS1986, number>
    | DS1986ActionType<typeof DS1986Actions.UPDATE_DS1986, DS1986>
    | DS1986ActionType<typeof DS1986Actions.DELETE_DS1986, number>
    ;
