// prettier-ignore
import { AppBar, Badge, Divider, Drawer as DrawerMui, Hidden, IconButton, List, ListItem, ListItemIcon, ListItemText, Toolbar, Typography, useMediaQuery } from "@material-ui/core";
import { Theme } from "@material-ui/core/styles";
import FormatListNumberedIcon from "@material-ui/icons/FormatListNumbered";
import HomeIcon from "@material-ui/icons/Home";
import MenuIcon from "@material-ui/icons/Menu";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useSelector } from "react-redux";
import { Route, Router } from "react-router-dom";
import { history } from "./configureStore";
import { Complaint } from "./model";
import { HomePage, ComplaintsPage, LoginPage, LogoutPage, NewComplaintPage } from "./pages";
import { tGiantState } from "./reducers/index";
import { withRoot } from "./AppWithRoot";
import PrivateRoute from "./components/PrivateRoute";
import { Fragment } from "react";
import { referenceActions } from './actions';

const LoginLogo = {
	logoImg: require('../src/assets/images/dsny_logo.png')
}

function Routes() {
	const classes = useStyles();

	return (
		<div className={classes.content}>
			 <PrivateRoute exact={true}  path="/" component={HomePage} />
			 <PrivateRoute exact={true}  path="/ds249/" component={HomePage} />
			 <PrivateRoute exact={true}  path="/ds249/home" component={HomePage} />
			 <PrivateRoute exact={true} path="/ds249/complaints" component={ComplaintsPage} />
			<PrivateRoute exact={true} path="/ds249/newComplaint" component={NewComplaintPage} />
			 <Route exact={true} path="/ds249/login" component={LoginPage} />
			 <Route exact={true} path="/ds249/logout" component={LogoutPage} />			
		</div>
	);
}

function Drawer(props: { complaintList: Complaint[] }) {
	const classes = useStyles();

	return (
		<div>
			<div className={classes.drawerHeader} />
			<Divider />
			<List>
				<ListItem button onClick={() => history.push("/ds249/home")}>
					<ListItemIcon>
						<HomeCustomIcon complaintList={props.complaintList}/>
					</ListItemIcon>
					<ListItemText primary="Home" />
				</ListItem>
			</List>
			<Divider />
			<List>
				<ListItem button onClick={() => history.push("/ds249/complaints")}>
					<ListItemIcon>
						<FormatListNumberedIcon />
					</ListItemIcon>
					<ListItemText primary="New Complaint" />
				</ListItem>
			</List>
			<List>
				<ListItem button onClick={() => history.push("/ds249/logout")}>
					<ListItemIcon>
						<ExitToAppIcon />
					</ListItemIcon>
					<ListItemText primary="Logout" />
				</ListItem>
			</List>
		</div>
	);
}

function App() {
	const classes = useStyles();
	const [mobileOpen, setMobileOpen] = React.useState(true);
	const complaintList = useSelector((state: tGiantState) => state.complaintList);
	const isLoggedIn = useSelector((state: any) => state.authentication.loggedIn);

	const [isLoading, setLoading] = React.useState(false);

	React.useEffect(() => {	
		
			(async () => {
				setLoading(true);
				const response = await referenceActions.payrollLocations();
				setLoading(false);
			})();
	
	}, []);

	const isMobile = useMediaQuery((theme: Theme) =>
		theme.breakpoints.down("sm")
	);

	const handleDrawerToggle = () => {
		setMobileOpen(!mobileOpen);
	};

	return (
		 <Router history={history}>
			<div className={classes.root}>
				<div className={classes.appFrame}>
					<AppBar className={classes.appBar}>
						<Toolbar>
							<IconButton
								color="inherit"
								aria-label="open drawer"
								onClick={handleDrawerToggle}
								className={classes.navIconHide}>
								<MenuIcon />
							</IconButton>						
							<img src={LoginLogo.logoImg} alt="" className={classes.logo} />
							<span className={classes.divLogoText}> sanitation </span>
				            <span className={classes.divLogoText2}> Employee Disciplinary Complaint System</span>
							<span className={classes.divLogoText2}>  | </span>
							<span className={classes.divLogoText2}> DS249 </span>
						</Toolbar>
					</AppBar>
					{isLoggedIn ? <Fragment><Hidden mdUp>
						<DrawerMui
							variant="temporary"
							anchor={"left"}
							open={mobileOpen}
							classes={{
								paper: classes.drawerPaper,
							}}
							onClose={handleDrawerToggle}
							ModalProps={{
								keepMounted: true, // Better open performance on mobile.
							}}
						>
							<Drawer complaintList={complaintList} />
						</DrawerMui>
					</Hidden>
					<Hidden smDown>
						<DrawerMui
							variant="permanent"
							open
							classes={{
								paper: classes.drawerPaper,
							}}
						>
							<Drawer complaintList={complaintList} />
						</DrawerMui>
					</Hidden></Fragment> : null}
					<Routes />
				</div>
			</div>
		</Router>
	);
}

function HomeCustomIcon(props: { complaintList: Complaint[] }) {
	let uncompletedTodos = props.complaintList.filter(t => t.completed === false);

	if (uncompletedTodos.length > 0) {
		return (
			<Badge color="secondary" badgeContent={uncompletedTodos.length}>
				<HomeIcon />
			</Badge>
		);
	} else {
		return <HomeIcon />;
	}
}

const drawerWidth = 240;
const useStyles = makeStyles((theme: Theme) => ({
	root: {
		width: "100%",
		height: "100%",
		zIndex: 1,
		overflowY: "scroll",
		overflowX: "hidden"
	},
	appFrame: {
		position: "relative",
		display: "flex",
		width: "100%",
		height: "100%",
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		position: "absolute",
		backgroundColor: "#244514",
	},
	navIconHide: {
		[theme.breakpoints.up("md")]: {
			display: "none",
		},
	},
	drawerHeader: { ...theme.mixins.toolbar },
	drawerPaper: {
		width: 250,
		backgroundColor: theme.palette.background.default,
		[theme.breakpoints.up("md")]: {
			width: drawerWidth,
			position: "relative",
			height: "100%",
		},
	},
	content: {
		backgroundColor: theme.palette.background.default,
		width: "100%",
		height: "calc(100% - 56px)",
		marginTop: 56,
		[theme.breakpoints.up("sm")]: {
			height: "calc(100% - 64px)",
			marginTop: 64,
		},
	},
	logo: {
		height: "50px",
		width: "50px"
	},	
	divLogoText: {
		color: "white",
		fontSize: 30,
		fontWeight: 500,
		verticalAlign: "top",
		margin: ".5%"
	},
	divLogoText2: {
		color: "white",
		fontSize: 16,
		margin: ".5%"
	}
}));

export default withRoot(App);
