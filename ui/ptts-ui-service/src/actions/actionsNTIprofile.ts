﻿import { allServices } from "../services";
import { API_NTI_AD_URI } from "../constants/constantsNTInetwork";
import { TYPES as ntiConstants } from '../constants/constantsNTIprofile';
export interface InterForActionNtiProfile { type: typeof ntiConstants.ACT_NTIPROFILE, payload: {} }

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
export type KnownActionNTI = InterForActionNtiProfile;

//// ----------------
//// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
//// They don't directly mutate state, but they can have external side-effects (such as loading data).
//export const actionCreators = {
//    actionFetchNtiProfile: (refnumber: string) => ({ type: ntiConstants.ACT_NTIPROFILE } as any as InterForActionNtiProfile)
//};


function fActionFetchNtiProfile(refnumber: string, dispatchRef: any) {
      if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log('try to get user details for this ref#');
      if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(refnumber);
    const apiUrl = API_NTI_AD_URI.ntiProfile;

    return allServices.LADgetServices.apiGetNtiProfile(apiUrl, {
        "ReferenceNumber": refnumber
    }, dispatchRef, ntiConstants.ACT_NTIPROFILE);

}


export const allActions = {
    actionFetchNtiProfile: fActionFetchNtiProfile
}