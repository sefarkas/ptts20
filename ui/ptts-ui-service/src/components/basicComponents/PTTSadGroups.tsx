﻿import { makeStyles } from "@material-ui/core";
import * as React from "react";
import { Fragment, useReducer } from "react";
import { useSelector } from "react-redux";
import { history } from "../../configureStore";
import { tGiantState } from "../../reducers";
import { isNullOrUndefined } from "util";
import { tADgroup, tDS21HDR, tOneNOV } from "../../model";
import { ENFHQ } from "../../constants/constantsNavigationADgroups";


function DS21NOVsIAWpayCodesExtractedFromADGroupMembership(dsNOVlist: tOneNOV[], content: any): tOneNOV[] {
	const dsListFiltered: tOneNOV[] = [];
	const PAYC: any = PTTSuserIsMemberOf(content.giantReducer.ST_ADGROUPS);

	if (isNullOrUndefined(PAYC[0])) {
		return dsNOVlist.slice(0, 29)
	}
	else {
		/*
		 * ST_ADGROUPS: {
				userName: 'xyz',
				userMemberOfADgroupList: 'xyz'
			},
		 * */
		const PAYCasc: string[] = PAYC.sort(function (a: string, b: string) {
			var nameA = !a ? "" : a.toUpperCase(); // ignore upper and lowercase
			var nameB = !b ? "" : b.toUpperCase(); // ignore upper and lowercase
			if (nameA < nameB) {
				return -1; // ASC
			}
			if (nameA > nameB) {
				return 1;
			}
			// names must be equal
			return 0;
		})
		const PAYCuniq: string[] = [];
		let lastPAYC: string = "lastPAYC";
		let currPAYC: string = "";
		for (var iIndex = 0; iIndex < PAYCasc.length; iIndex++) {
			currPAYC = PAYCasc[iIndex];
			currPAYC = isNullOrUndefined(currPAYC) ? "" : currPAYC.substr(0, 4);
			if (lastPAYC !== currPAYC && lastPAYC.length > 0 && currPAYC.length > 0) {
				PAYCuniq.push(currPAYC);
				lastPAYC = currPAYC;
			};
		}

		for (var iIndex = 0; iIndex < PAYCuniq.length; iIndex++) {
			for (var jIndex = 0; jIndex < dsNOVlist.length; jIndex++) {
				if (dsNOVlist[jIndex].ReceiptNumber.indexOf(PAYCuniq[iIndex]) > -1) {
					dsListFiltered.push(dsNOVlist[jIndex]);
				}
			};
		}

	}

	return dsListFiltered;
}


function DS21hdrIAWpayCodesExtractedFromADGroupMembership(dsList: tDS21HDR[], content: any): tDS21HDR[] {
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("start DS21hdrIAWpayCodesExtractedFromADGroupMembership");
	const dsListFiltered: tDS21HDR[] = [];
	console.log("content is:");
	console.log(content);
	const PAYC: string[] = PTTSuserIsMemberOf(content.giantReducer.ST_ADGROUPS);

	if (isNullOrUndefined(PAYC[0])) {
		return dsList.slice(0, 29)
	}
	else {
		/*
		 * ST_ADGROUPS: {
				userName: 'xyz',
				userMemberOfADgroupList: 'xyz'
			},
		 * */
		const PAYCasc: string[] = PAYC.sort(function (a: string, b: string) {
			var nameA = !a ? "" : a.toUpperCase(); // ignore upper and lowercase
			var nameB = !b ? "" : b.toUpperCase(); // ignore upper and lowercase
			if (nameA < nameB) {
				return -1;
			}
			if (nameA > nameB) {
				return 1;
			}
			// names must be equal
			return 0;
		})
		const PAYCuniq: string[] = [];
		let lastPAYC: string = "lastPAYC";
		let currPAYC: string = "";
		for (var iIndex = 0; iIndex < PAYCasc.length; iIndex++) {
			currPAYC = PAYCasc[iIndex];
			currPAYC = isNullOrUndefined(currPAYC) ? "" : currPAYC.substr(0, 4);
			if (lastPAYC !== currPAYC && lastPAYC.length > 0 && currPAYC.length > 0) {
				PAYCuniq.push(currPAYC);
				lastPAYC = currPAYC;
			};
		}

		for (var iIndex = 0; iIndex < PAYCuniq.length; iIndex++) {
			for (var jIndex = 0; jIndex < dsList.length; jIndex++) {
				if (dsList[jIndex].DS1984Number.indexOf(PAYCuniq[iIndex]) > -1) {
					dsListFiltered.push(dsList[jIndex]);
				}
			};
		}

	}

	return dsListFiltered;
}

 function isENFHQ(ST_ADGROUPS: [{ userName: string, userMemberOfADgroupList: string }], PAYC: string): boolean {

	if (isNullOrUndefined(ST_ADGROUPS)) { return false };

	for (var i = 0; i < ST_ADGROUPS.length; i++) {
		if (ST_ADGROUPS[i].userMemberOfADgroupList === PAYC) {
			return true;
		}
	}

	return false;

}

function PTTSuserIsMemberOf(ST_ADGROUPS: tADgroup[]): string[] {
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("start PTTSuserIsMemberOf");

	let PTTSgroups: string[] = [];
	try {
		for (var i = 0; i < ST_ADGROUPS.length; i++) {
			if (ST_ADGROUPS[i].userMemberOfADgroupList.length > 6) {
				// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("test this: ", ST_ADGROUPS[i].userMemberOfADgroupList)
				if (
					ST_ADGROUPS[i].userMemberOfADgroupList.substr(-6) === "LEADER" // last six characters, substring(-6) doesn't work
					|| ST_ADGROUPS[i].userMemberOfADgroupList.substr(-5) === "CLERK"
					|| ST_ADGROUPS[i].userMemberOfADgroupList.substr(-5) === "AGENT"
				) {
					// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("put this in the list: ", ST_ADGROUPS[i].userMemberOfADgroupList)
					PTTSgroups.push(ST_ADGROUPS[i].userMemberOfADgroupList);
				}
			}

		}

		if (PTTSgroups.length === 0) {
			PTTSgroups.push("No PTTS AD Group for " + localStorage.getItem("userid"));
		}
	}
	catch (e) {
		PTTSgroups.push("No PTTS AD Group because " + e.toString());
	}

	return PTTSgroups;

}

function isADinENFHQ(ST_ADGROUPS: tADgroup[]): boolean {

	if (isNullOrUndefined(ST_ADGROUPS)) { return false };

	for (var i = 0; i < ST_ADGROUPS.length; i++) {
		for (var j = 0; j < ENFHQ.length; j++) {
			if (ST_ADGROUPS[i].userMemberOfADgroupList === ENFHQ[j].userMemberOfADgroupList) {
				return true;
			}
		}
	}

	return false;

}

const PTTSmembership = () => {

	const content: tGiantState = useSelector((state: any) => state.giantReducer);
	const arr: string[] = PTTSuserIsMemberOf(content.ST_ADGROUPS);
	if (!arr) { return <div>still working ...</div> };
	let keyID: number = 0;
	return (
		<div>
			{arr.map(oneChoice => {
			keyID++
				return (
					<div key={keyID}>
						{oneChoice}
					</div>
				);
			})
			}
		</div>
		)
}

const RenderPTTSmembership = () => { // name must start with capital R because of TypeScript
	// https://stackoverflow.com/questions/37414304/typescript-complains-property-does-not-exist-on-type-jsx-intrinsicelements-whe
	const classes = useStyles();
	const content: tGiantState = useSelector((state: any) => state.giantReducer);

	const onCancel = () => {
		history.push('/PTTS/logout');
	};

	const hdr: string = `PTTS Membership for `;
	const uid: string = `${content.ST_USERDETAILS.firstName} ${content.ST_USERDETAILS.lastName}:`

	if (!content.ST_LOGINAD) {
		return (
			<div>
				<button onClick={onCancel}>Please login again.</button>
			</div>
		)
	}
	else {

		return (
			<div className={classes.table}>
				<div>
					{hdr}
				</div>
				<div>
					{uid}
				</div>
				<Fragment>
					<PTTSmembership />
				</Fragment>
			</div>
		); 
    }

}


const useStyles = makeStyles({
	paper: {
		width: "70%",
		display: "block",
	},
	table: {
		width: "100%",
		marginLeft: 0,
	},
	cell: {
		minWidth: 100,
		maxWidth: 220,
	}
});

// ...

export const commonf: any = {
	RenderPTTSmembership, PTTSuserIsMemberOf, isADinENFHQ, isENFHQ,
	DS21hdrIAWpayCodesExtractedFromADGroupMembership,
	DS21NOVsIAWpayCodesExtractedFromADGroupMembership,
}