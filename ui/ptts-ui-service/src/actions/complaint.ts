import { ComplaintAction, ComplaintActions, Complaint } from "../model";

export function addComplaint(complaint: Complaint): ComplaintAction {
	return {
		type: ComplaintActions.ADD_COMPLAINT,
		payload: complaint,
	};
}

// Async Function expample with redux-thunk
export function completeComplaint(complaintId: number) {
	// here you could do API eg

	return (dispatch: Function, getState: Function) => {
		dispatch({ type: ComplaintActions.COMPLETE_COMPLAINT, payload: complaintId });
	};
}

export function uncompleteComplaint(complaintId: number): ComplaintAction {
	return {
		type: ComplaintActions.REVERT2INCOMPLETE_COMPLAINT,
		payload: complaintId,
	};
}

export function deleteTodo(complaintId: number): ComplaintAction {
	return {
		type: ComplaintActions.DELETE_COMPLAINT,
		payload: complaintId,
	};
}

export function newComplaint(referenceNumber: string): ComplaintAction {
	return {
		type: ComplaintActions.START_NEW_COMPLAINT,
		payload: referenceNumber,
	};
}
