﻿import { makeStyles } from "@material-ui/styles";

const styles = makeStyles({
	form: {
		height: 287,
		maxHeight: 287
	},
	reactselect: {
		maxWidth: "calc(100% - 20px)",
		marginLeft: 10,
		marginTop: 0,
		fontSize: 12,
		'.control': {
			height: 25
		}
	},
	reactselectsmall: {
		width: 100,
		marginLeft: 5,
		marginRight: 10,
		marginTop: 0,
		fontSize: 12
	},
	link: {
		color: "green",
		fontSize: 12
	},
	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "inline-block",
		marginBottom: 0,
		marginTop: 5,
		marginLeft: 10,
		fontSize: 12,
		fontWeight: 800,
		color: "#2e2e2d",
	},
	labelDisabled: {
		marginBottom: 10,
		marginTop: 10,
		marginLeft: 10,
		paddingBottom: 10,
		color: "#959292",
		fontSize: 10
	},
	labelTemplateText: {
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginBottom: 5,
		marginTop: 5,
		fontSize: 12,
		fontWeight: 500,
		color: "#2e2e2d",
	},
	labelTemplateText2: {
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginBottom: 5,
		marginTop: 5,
		marginLeft: 10,
		fontSize: 12,
		fontWeight: 500,
		color: "#2e2e2d",
	},
	button: {
		height: "45px",
		width: "50%",
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	grid: {
		background: "white",
		marginTop: "2px;"
	},
	gridCell: {
		paddingTop: "2px",
	},
	gridCellTemplate: {
		paddingTop: "5px",
		paddingLeft: "10px",
		paddingBottom: "5px",
	},
	gridCellTemplateFirstLine: {
		paddingTop: "10px",
		paddingLeft: "10px",
		paddingBottom: "5px",
	},
	gridCellTemplateLastLine: {
		paddingTop: "5px",
		paddingLeft: "10px",
		paddingBottom: "10px",
	},
	gridCellSpacer9: {
		paddingTop: "5px",
		paddingLeft: "10px",
		paddingBottom: "9px",
	},
	h2: {
		marginTop: "10px",
		marginBottom: "5px",
		marginLeft: "10px",
		fontSize: 15,
	},
	h3: {
		marginTop: "5px",
		marginBottom: "5px",
		marginLeft: "10px",
		fontSize: 11,
	},
	error: {
		fontSize: 12,
		color: "#ef1207",
	},
	datepicker: {
		width: 130,
		height: 30,
		marginBottom: 0,
		padding: "5px 5px",
		marginLeft: "5px",
		marginRight: "5px",
		fontSize: 12,
		borderRadius: 1,
		border: "1px solid",
		borderColor: "#c5c8c5",
		'&:focus': {
			outlineColor: '#2b995f',
		}
	},
	input: {
		width: 90,
		height: 30,
		borderRadius: 1,
		border: "1px solid",
		borderColor: "#c5c8c5",
		padding: "10px 10px",
		marginLeft: "5px",
		marginRight: "5px",
		fontSize: 12,
		'&:focus': {
			outlineColor: '#2b995f',
		},
	},
	inputLeft: {
		width: 90,
		height: 30,
		borderRadius: 1,
		border: "1px solid",
		borderColor: "#c5c8c5",
		padding: "10px 10px",
		marginRight: "5px",
		fontSize: 12,
		'&:focus': {
			outlineColor: '#2b995f',
		}
	},
});

export default styles;