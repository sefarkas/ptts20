﻿import { Typography } from "@material-ui/core";
import React, { useEffect } from 'react';
import { useFormContext } from "react-hook-form";
import Grid from '@material-ui/core/Grid';
import ReactSelect, { components } from "react-select";
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers';
import styles from "./complaintStyles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";

interface Props {
	idx: number;
	charge: any;
	chargeOptions: any;
	districtOptions: any;
	subChargeOptions: any;
	handleChange: (e: any, control?: string, idx?: number) => void;
	handleRemoveCharge: (event: any, idx: number) => void;	
}

library.add(faSearch);

const CaretDownIcon = () => {
	return <FontAwesomeIcon icon="search" />;
};

const DropdownIndicator = (props: any) => {
	return (
		<components.DropdownIndicator {...props}>
			<CaretDownIcon />
		</components.DropdownIndicator>
	);
};

const pad = (n: any) => {
	return ("00" + n).slice(-2);
}

const todaysDate = () => {
	const tempDate = new Date();
	const currDate = tempDate.getFullYear() + '-' + pad((tempDate.getMonth() + 1)) + '-' + pad((tempDate.getDate()));
	return currDate;
}

const DeleteIcon = {
	deleteIcon: require('../../assets/images/delete_icon.png')
};

export function ComplaintCharge(props: Props) {
	const classes = styles();
	const { register, errors } = useFormContext();

	return (
		<React.Fragment key={"charges" + props.idx}>
			<Grid container item xs={9} key={"gridcharges" + props.idx} spacing={0}>
				<Grid item xs={6}>
					<label className={classes.labelX} key={"labelcharge" + props.idx}>Charges</label></Grid>
				<Grid item xs={6}>
					{props.idx > 0 ? <span onClick={e => props.handleRemoveCharge(e, props.idx)}><Typography className={classes.labelRemove}><img src={DeleteIcon.deleteIcon} alt="" className={classes.deleteIcon} />Remove</Typography></span> : null}
				</Grid>
				<Grid item xs={12} className={classes.gridCell} key={"gridcellcharge" + props.idx}>
					<ReactSelect
						onChange={e => props.handleChange(e, "charges", props.idx)}
						options={props.chargeOptions}
						key={"charge" + props.idx}
						value={props.charge.chargeName}
						placeholder="Enter charge code. Eg. 1.4"
						className={classes.reactselectLarge}
						theme={theme => ({
							...theme,
							borderRadius: 1,
							colors: {
								...theme.colors,
								primary25: '#d2f6e3',
								primary50: '#2b995f',
								primary: '#2b995f',
							},
						})}
						components={{ DropdownIndicator }} /></Grid>
				{props.charge.showSubCharges === true ? <Grid item xs={12} className={classes.gridCell} key={"gridcellsubcharge" + props.idx}>
					<label className={classes.label} key={"labelsubcharge" + props.idx}>Subcharges</label>
					<ReactSelect
						onChange={e => props.handleChange(e, "subcharges", props.idx)}
						options={props.subChargeOptions}
						key={"subcharge" + props.idx}
						value={props.charge.subChargeName}
						placeholder="Enter subcharge code."
						className={classes.reactselectLarge}
						theme={theme => ({
							...theme,
							borderRadius: 1,
							colors: {
								...theme.colors,
								primary25: '#d2f6e3',
								primary50: '#2b995f',
								primary: '#2b995f',
							},
						})}
						components={{ DropdownIndicator }}
					/></Grid> : null}
				<Grid item xs={6} className={classes.gridCell} key={"gridcelllocation" + props.idx}>
					<label className={classes.label} key={"labellocation" + props.idx}>Incident Location</label>
					<ReactSelect
						onChange={e => props.handleChange(e, "incidentLocation", props.idx)}
						options={props.districtOptions}
						key={"location" + props.idx}
						value={props.charge.incidentLocation}
						placeholder="Select One"
						className={classes.reactselect}
						theme={theme => ({
							...theme,
							borderRadius: 1,
							colors: {
								...theme.colors,
								primary25: '#d2f6e3',
								primary50: '#2b995f',
								primary: '#2b995f',
							},
						})}
					/>
				</Grid>
				<Grid item xs={6} className={classes.gridCell} key={"gridcelldate" + props.idx}>
					<label className={classes.label} key={"labeldate" + props.idx}>Incident Date</label>
					<input
						type="date"
						key={"date" + props.idx}
						value={props.charge.incidentDate}
						placeholder="Select Date"
						max={todaysDate()}
						className={classes.datepicker}
						onChange={e => props.handleChange(e, "incidentDate", props.idx)}
					/></Grid>
			</Grid>
			<Grid item xs={3} className={classes.gridSpacerCell} />
		</React.Fragment>
	);
}

