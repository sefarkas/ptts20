﻿/* Environment
    Dev - 1
    Staging (Beta Users) - 2
    Alt Staging (QA) - 3
    Prod - 4
 */
export const INPUT_ERR_MESSAGE = "Please enter valid input";
export const KEY_USERINFO = "UserInfo";

export const Env = process.env.REACT_APP_ENV_TYPE!; // exclamation point is necessary for reading this value

export const ACTIVE_ENV_URL = () => {
    switch (Env) {
        case "1":
            //  return "http://localhost:12685/"; // s://msdlva-dsndfd02.csc.nycnet/donate-web/";
            return "https://localhost:44391/PTTS20/api"; // "http://10.155.230.134:1958/api/";
        case "2":
            return "https://msswvw-dsndfdv1.csc.nycnet/donate-web/";
        case "3":
            return "https://msswvw-dsndfdv1.csc.nycnet/donate-web/";
        case "4":
            // return "https://msswvw-dsndfdv1.csc.nycnet/donate-web/";
            return "http://mstlva-dsnsmt14.csc.nycnet:30707/api/";
        case "5":
            // return "http://mstlva-dsnsmt20.csc.nycnet:30707/api/";
            return "http://10.155.230.134:1958/api/";
        default:
            // return "https://msdlva-dsndfd02.csc.nycnet/donate-web/";
            return "http://10.155.230.134:1958/api/";
    }
};

export const BASE_URL = `${ACTIVE_ENV_URL()}`;


// HomePage APIs
export const USERSESSION = {
    login: BASE_URL + "user/api/account/login",
    dashboardlogin: BASE_URL + "user/api/account/dashboardlogin",
    logout: BASE_URL + "user/api/account/logout",
    refreshtoken: BASE_URL + "user/api/token/refresh",
    forgotPassword: BASE_URL + "user/api/account/RequestPasswordReset?email=",
    ChangePassword: BASE_URL + "user/api/account/ResetPassword",
}

// Login with Active Directory credentials APIs
export const LOGINAD = {
    // login: BASE_URL + "user/api/account/login",
    login: "https://msswvw-dnsdnyvp.csc.nycnet/DSNYApi/api/User/ValidateUserLogin",
    // https://stackoverflow.com/questions/43871637/no-access-control-allow-origin-header-is-present-on-the-requested-resource-whe 
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
    // https://stackoverflow.com/questions/12458444/enabling-cross-origin-resource-sharing-on-iis7
    // https://support.secureauth.com/hc/en-us/articles/360031272451-OpenID-error-Blocked-by-CORS-policy-No-Access-Control-Allow-Origin-header-is-present-on-the-requested-resource-
    // https://docs.microsoft.com/en-us/iis/extensions/cors-module/cors-module-configuration-reference#cors-configuration
    //
    // docker container has no DLL to interact with AD, so I deployed a AdSvc on WinSvr2016
    validURI: "http://10.155.230.134:44389/" + "api/User/ValidateUserCredentials",  // post
    detailsURI: "http://10.155.230.134:44389/" + "api/User/UserDetails",  // post
    dashboardlogin: BASE_URL + "user/api/account/dashboardlogin",
    logout: BASE_URL + "user/api/account/logout",
    refreshtoken: BASE_URL + "user/api/token/refresh",
    forgotPassword: BASE_URL + "user/api/account/RequestPasswordReset?email=",
    ChangePassword: BASE_URL + "user/api/account/ResetPassword",
    PTTSdiscoveredADgroupsForUID: "http://10.155.230.134:44389/" + "api/User/UserADgroupMembership",  // post
}


export const TYPES = {
    ACT_AXIOSNPROGRESS: 'AxiosInProgressWhenTrue',
}