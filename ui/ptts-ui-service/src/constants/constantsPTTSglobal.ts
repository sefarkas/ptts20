﻿
export const TYPES = {
    FORM_603_LIST: "ECB603s",
    FORM_603_LAST: "ECB603_Last_Chosen",
    FORM_1983_LIST: "DS1983s",
    FORM_1983_LAST: "DS1983_Last_Chosen",
    FORM_1984_LIST: "DS1984s",
    FORM_1984_LAST: "DS1984_Last_Chosen",
    FORM_1986_LIST: "DS1986s",
    FORM_1986_LAST: "DS1986_Last_Chosen",
    FORM_21_LIST:   "DS21HDRrows",
    FORM_21_LAST:   "DS21HDR_Last_Chosen",
    FORM_NOV_LIST:  "NOVs",
    FORM_NOV_LAST: "NOV_Last_Chosen",
    ACT_ERRMSG: "PTTS_exception_message_for_user",
}

