﻿// prettier-ignore
import {
	Checkbox, IconButton, Link, Paper, Table, TableBody, TableCell,
	TableContainer, TableHead, TableRow
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useState } from "react";
import { allActions } from "../../actions";
import { history } from "../../configureStore";
import { DS1983 } from "../../model";
import { allServices } from "../../services";
import { PTTSfunctions} from "../../components/basicComponents";
import { DS1983basics } from ".";

export function DS1983Table() {
	const classes = useStyles();
	const dummyDS1983: DS1983 = {
		ReceiptNumber: "",
		MessengerName: "",
		SummonsCount: 0,
		SummonsFromNumber: 0,
		SummonsToNumber: 0,
		Borough: "",
		DistributorName: "",
		DistributorSignature: "",
		DistributorSignedDate: new Date(),
		MessengerSignature: "",
		MessengerTitle: "",
		MessengerSignedDate: new Date(),
		AcknowledgedBy: "",
		ID: 0,
		BoxNumber: 0,
		FormStatus: "",
		ModifiedOn: new Date(),
		ModifiedBy: "",
		BadgeNo: "",
		DistributorRefNo: "",
		CreatedBy: "",
		CreatedDate: new Date(),
		DistrictID: 0,
		PayCode: "",
		IsVoid: false,

	}

	const [dsList, setdsList] = useState([dummyDS1983]);
	const [Loading, setLoading] = useState(false);

	React.useEffect(() => {
		(async () => {
			setLoading(true);
			const response = await allServices.PTTSgetServices.DS1983List(); // axios always returns an object with a "data" child
			const rdata: DS1983[] = response.data;
			if (!rdata) {
				setLoading(true);
			}
			else {
				const rdataDesc: DS1983[] = rdata.sort(function (a, b) {
					var nameA = !a.ReceiptNumber ? "" : a.ReceiptNumber.toUpperCase(); // ignore upper and lowercase
					var nameB = !b.ReceiptNumber ? "" : b.ReceiptNumber.toUpperCase(); // ignore upper and lowercase
					if (nameA < nameB) {
						return 1;
					}
					if (nameA > nameB) {
						return -1;
					}
					// names must be equal
					return 0;
				}).slice(0, 29); // DESC
				setdsList(rdataDesc);
				setLoading(false);
			}
		})();
	}, []);

	// React.useEffect(() => {
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("dsList is");
	//	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(dsList);
	// }, [dsList]);

	const handleCellClick = (e: any) => {
		history.push('/PTTS/existingDS1983/' + e.target.textContent)
	}

	function handleRelatedDS1984s(rcpt: string) {
		history.push('/PTTS/someDS1984/'+ rcpt)
	}

	function handleNewDS1984s(rcpt: string, fromNumber: number, toNumber: number) {
		const payload: DS1983basics = {
			DS1983receiptNumber: rcpt,
			DS1983fromTktNumber: fromNumber,
			DS1983toTktNumber: toNumber,
		};
		history.push('/PTTS/newDS1984/', payload)
    }

	return (
		<TableContainer component={Paper}>
			<Table className={classes.table} size="small" aria-label="a dense table">
				<TableHead>
					<TableRow>
						<TableCell size="small" align="right">DS1983 Receipt No</TableCell>
						<TableCell size="small" align="right">Ticket Range</TableCell>
						<TableCell size="small" align="right">Status</TableCell>
						<TableCell size="small" align="right">Pay Code</TableCell>
						<TableCell size="small" align="right">Last modified on</TableCell>
						<TableCell size="small" align="left">Modified by</TableCell>
						<TableCell size="small" align="left">new DS1984</TableCell>
						<TableCell size="small" align="left">related DS1984</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{
						// @ts-ignore: Object is possibly 'null'.
						Loading || !dsList || dsList === undefined ? <TableRow><TableCell>working ...</TableCell></TableRow> : dsList.map((n: DS1983) => {
							//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("n is:");
							//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(n);
							if (!n.ID) {
								return (<TableRow key={"-1"}><TableCell key={"0"}>Working ...</TableCell></TableRow>)
							}
							else {
								return (
									<TableRow
										key={n.ID.toString() + "row"}
										hover
									>
										<TableCell key={n.ID.toString() + "rcptno"} align="right" onClick={handleCellClick}><Link href="#">{n.ReceiptNumber}</Link></TableCell>
										<TableCell key={n.ID.toString() + "TKTRNG"} size="small" align="right">{n.SummonsFromNumber}-{n.SummonsToNumber}</TableCell>
										<TableCell key={n.ID.toString() + "STATUS"} size="small" align="right">{n.FormStatus}</TableCell>
										<TableCell key={n.ID.toString() + "PAYCOD"} size="small" align="right">{n.PayCode}</TableCell>
										<TableCell key={n.ID.toString() + "MODFON"} size="small" align="right">{n.ModifiedOn}</TableCell>
										<TableCell key={n.ID.toString() + "MODFBY"} size="small" align="left">{n.ModifiedBy}</TableCell>
										<TableCell key={n.ID.toString() + "NEW1984"} align="left" onClick={() => handleNewDS1984s(n.ReceiptNumber, n.SummonsFromNumber, n.SummonsToNumber)}><Link href="#">Make new DS1984</Link></TableCell>
										<TableCell key={n.ID.toString() + "URL1984"} align="left" onClick={() => handleRelatedDS1984s(n.ReceiptNumber)}><Link href="#">Link to its DS1984s</Link></TableCell>
										<TableCell size="small" align="left" key={n.ID.toString() + "ico"} >
											<IconButton
												aria-label="Delete"
												color="default"
												onClick={() =>
													allActions.DS1983acts.DS1983deleteTodo(n.ID)
												}
											>
												<DeleteIcon />
											</IconButton>
										</TableCell>
									</TableRow>
								);
							}

						})}
				</TableBody>
			</Table>
		</TableContainer>
	);
}

const useStyles = makeStyles({
	paper: {
		width: "70%",
		display: "block",
	},
	table: {
		width: "100%",
		marginLeft: 0,
	},
	cell: {
		minWidth: 100,
		maxWidth: 280,
	}
});
