﻿import { Button, Checkbox, Box, Input, TextField } from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import { createStyles, Theme, withStyles } from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useEffect, useRef, useState } from "react"; // , useReducer
import { useForm } from "react-hook-form"; // , FormProvider
import { allServices } from '../../services';
import { PTTSfunctions } from '../../components/basicComponents';
import { history } from "../../configureStore";
import { tDS21Card, tDS21HDR, tOneNOV } from '../../model';
import { isNullOrUndefined } from "util";
import { useDispatch, useSelector } from "react-redux";
import { dummies } from "../../reducers";
import { allActions } from "../../actions";
import { ErrorMessage } from "@hookform/error-message";
import { ErrorMessageContainer } from "../basicComponents/validation";
import moment from "moment";

import {
	
	KeyboardDatePicker
} from '@material-ui/pickers';

type postDataResult<T> = {
	success: boolean;
	errors?: { [P in keyof T]?: string[] };
};

type FormData = {
	DS1984ReceiptNumber: string;
};

export function DS21CardBottom(props: any) {
	if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props);
	//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props.match.params.onercpt);
	const dispatch = useDispatch();
	const currUser: string = useSelector((state: any) => state.giantReducer.ST_USERDETAILS.userName);
	const colDS21HDR: tDS21HDR[] = useSelector((state: any) => state.DS21HDRReducer.ST_DS21HDRrowsFLTRD);
	const NOVs: tOneNOV[] = useSelector((state: any) => state.DS21CardReducer.ST_25NOVS)
	const JulianZero: Date = new Date(0);
	const dateToday: Date = new Date();
	const mountedRef = useRef(true);
	const classes = useStyles();


	const [IDState, setIDState] = useState(0);
	const [IssuingOfficerSignatureState, setIssuingOfficerSignatureState] = useState("");
	const [CommandState, setCommandState] = useState("");
	const [IssuingOfficerNameState, setIssuingOfficerNameState] = useState("");
	const [TaxRegNumberState, setTaxRegNumberState] = useState("");
	const [AgencyState, setAgencyState] = useState(0);
	const [DateCardStartedState, setDateCardStartedState] = useState(JulianZero);
	const [DateReturnedState, setDateReturnedState] = useState(JulianZero);
	const [AssignedDistrictState, setAssignedDistrictState] = useState("");
	const [IssuingOfficerTitleState, setIssuingOfficerTitleState] = useState("");
	const [OfficerInChargeNameState, setOfficerInChargeNameState] = useState("");
	const [OfficerInChargeTitleState, setOfficerInChargeTitleState] = useState("");
	const [DateCardCompletedState, setDateCardCompletedState] = useState(JulianZero);
	const [RemarksState, setRemarksState] = useState("");
	const [DS1984NumberState, setDS1984NumberState] = useState(props.onercpt);
	const [DateCompleted_MMState, setDateCompleted_MMState] = useState("00");
	const [DateCompleted_DDState, setDateCompleted_DDState] = useState("00");
	const [ModifiedOnState, setModifiedOnState] = useState(JulianZero);
	const [ModifiedByState, setModifiedByState] = useState("");
	const [CreatedByState, setCreatedByState] = useState("");
	const [CreatedOnState, setCreatedOnState] = useState(JulianZero);
	const [FormStatusIDState, setFormStatusIDState] = useState(-1);

	const [CertifyingOfficerRefNoState, setCertifyingOfficerRefNoState] = useState("");
	const [OfficerInChargeRefNoState, setOfficerInChargeRefNoState] = useState("");

	const [isLoading, setLoading] = React.useState(false);

	const [rcpt, setrcpt] = useState(props.onercpt);
	const [NotarizedOrVoid, setNotarizedOrVoid] = useState(props.NotarizedOrVoid); // 25 Voids, 25 Notarized; values from 0 to 50

	const [oneDS21Card, setoneDS21Card] = useState<tDS21Card>(dummies.dummyDS21Card);
	const [SaveCardRequestInProgress, setSaveCardRequestInProgress] = useState(false);

	const [DDstate, setDDstate] = useState("");
	const [MMstate, setMMstate] = useState("");

	const [origFormStatusID, setOrigFormStatusID] = useState(FormStatusIDState > 2 ? FormStatusIDState : NotarizedOrVoid > 24 ? 2 : 1);
	const [CurrFormStatusID, setCurrFormStatusID] = useState(FormStatusIDState > 2 ? FormStatusIDState : NotarizedOrVoid > 24 ? 2 : 1);

	const [tEF, settEF] = useState(dummies.dummyDS21HDRefIUD);


	function MM(dteDate: Date): void {
		console.log("MM() started", dteDate);
		let strRslt: string = "";
		if (!DateCardCompletedState) {
			console.log("why does CardComplete have this value: ", DateCardCompletedState);
			strRslt = "MM";
		}
		else {
			try {
				if (dteDate.getFullYear() === JulianZero.getFullYear()) {
					strRslt = "";
				}
				else {
					const lclMMn: number = dteDate.getMonth() + 1;
					if (lclMMn < 10) {
						strRslt = "0" + lclMMn.toString()
					}
					else {
						strRslt = lclMMn.toString()
                    }
				}
			}
			catch (error) { strRslt = error.toString(); };
		}
		console.log("MM() result: ", strRslt);
		setMMstate(strRslt);

	}

	function MMinit(aDate?: Date | undefined): string {
		if (!aDate) {
			return "00";
		}
		if (DateCardCompletedState.getFullYear() === JulianZero.getFullYear()) {
			return "";
		}
		const lclMMn: number = aDate.getMonth() + 1;
		if (lclMMn < 10) {
			return "0" + lclMMn.toString()
		}
		return lclMMn.toString();
	}

	function DD(dteDate: Date): void {
		let strRslt: string = "";
		const lclDDn: number = dteDate.getDate();
		if (!DateCardCompletedState) {
			strRslt = "DD";
		}
		else if (dteDate.getFullYear() === JulianZero.getFullYear()) {
			strRslt = "";
		}
		else if (lclDDn < 10) {
			strRslt = "0" + lclDDn.toString()
		}
		else {
			strRslt = lclDDn.toString()
        }
		setDDstate(strRslt);
	}

	function DDinit(aDate?: Date | undefined): string {
		if (!aDate) {
			return "00";
		}
		if (DateCardCompletedState.getFullYear() === JulianZero.getFullYear()) {
			return "";
		}
		const lclDDn: number = aDate.getDate();
		if (lclDDn < 10) {
			return "0" + lclDDn.toString()
		}
		return lclDDn.toString();
	}

	const handleOnChange = (e: any) => {
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
		switch (e.target.name) {
			case "fldDateCardStarted":
				try {
					// e.target.value has YYYY-MM-DD; need to convert to Date
					console.log("fldDateCardStarted changes to this new value: ", e.target.value);
					const dteSelected: Date = PTTSfunctions.Commonfunctions.MomentToDate(e.target.value);
					console.log("compare dteSelected to ", dateToday);
					if (dteSelected <= dateToday) {
						console.log("put this into state: ", dteSelected);
						setDateCardStartedState(dteSelected);
					}
					else { throw("Pick today or a date in the past."); }
				}
				catch (error) {
					alert(error.toString());
					throw (error);
                }
				break;
			case "fldDateReturned":
				try {
					// e.target.value has YYYY-MM-DD; need to convert to Date
					const dteSelected: Date = PTTSfunctions.Commonfunctions.MomentToDate(e.target.value);
					if ((dteSelected) >= DateCardStartedState)
						{ setDateReturnedState(dteSelected); }
					else { throw ("Pick a date after card started."); }
				}
				catch (error) {
					alert(error.toString())
					throw (error);
				}
				break;
			case "fldIssuingOfficerName":
				setIssuingOfficerNameState(e.target.value);
				break;
			case "fldAssignedDistrict":
				setAssignedDistrictState(e.target.value);
				break;
			case "fldIssuingOfficerTitle":
				setIssuingOfficerTitleState(e.target.value);
				break;
			case "fldOfficerInChargeName":
				setOfficerInChargeNameState(e.target.value);
				break;
			case "fldOfficerInChargeTitle":
				setOfficerInChargeTitleState(e.target.value);
				break;
			case "fldDateCardCompleted":
				try {
					// e.target.value has YYYY-MM-DD; need to convert to Date
					const dteCompleted: Date = PTTSfunctions.Commonfunctions.MomentToDate(e.target.value)
					if ((dteCompleted) > DateCardStartedState) {

						setDateCardCompletedState(dteCompleted);
						MM(dteCompleted);
						DD(dteCompleted);
					}
					else {
						throw("Pick a date after " + DateCardStartedState);
					}

				}
				catch (error) {
					alert(error.toString())
					throw (error);
				}
				break;
			case "fldRemarks": setRemarksState(e.target.value); break;

			case "fldCertifyingOfficerREFNO":
				if (e.target.value.length === 7) setCertifyingOfficerRefNoState(e.target.value);
				break;

			case "fldOfficerInChargeREFNO":
				if (e.target.value.length === 7) setOfficerInChargeRefNoState(e.target.value);
				break;

			case "chkBOROsentTo":
				if (e.target.value) setCurrFormStatusID(3);
				else setCurrFormStatusID(origFormStatusID);
				break;

			case "chkBOROrec":
				if (e.target.value) setCurrFormStatusID(4);
				else setCurrFormStatusID(origFormStatusID);
				break;

			case "chkBOROrej":
				if (e.target.value) setCurrFormStatusID(5);
				else setCurrFormStatusID(origFormStatusID);
				break;

			case "chkHQsentTo":
				if (e.target.value) setCurrFormStatusID(6);
				else setCurrFormStatusID(origFormStatusID);
				break;

			case "chkHQrec":
				if (e.target.value) setCurrFormStatusID(7);
				else setCurrFormStatusID(origFormStatusID);
				break;

			case "chkHQrej":
				if (e.target.value) setCurrFormStatusID(8);
				else setCurrFormStatusID(origFormStatusID);
				break;

			default:
				// code block
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.value);
		}
		MakeInSynch();

	}

	function MakeInSynch() {
		if (!allActions.DS21acts.DS21HDRsyncWithCard(dispatch, oneDS21Card, currUser,
			CertifyingOfficerRefNoState, parseInt(NOVs[0].ECBSummonsNumber), parseInt(NOVs[24].ECBSummonsNumber))) { throw "Reducers out-of-synch for DS21 " + oneDS21Card.DS1984Number }
    }

	const { register, handleSubmit, errors, formState } = useForm<FormData>();

	const onCancel = () => {
		history.push('/PTTS/home');
	};

	const newDS21 = () => {
		history.push('/PTTS/newDS21');
	};

	const newDS1986 = (rcpt84: any) => {
		if (!rcpt84) {
			alert("PTTS does not know which DS-1984 to associate with a new DS-1986.")
		}
		else {
			history.push('/PTTS/newDS1986', rcpt84);
		}


	};

	useEffect(() => {
		const aDS21Card: tDS21Card[] = [dummies.dummyDS21Card];
		colDS21HDR.forEach(function (aRow: tDS21HDR) {
			if (aRow.DS1984Number === rcpt) {
				setCreatedByState(aRow.CreatedBy);
				setCreatedOnState(!aRow.CreatedOn ? JulianZero : aRow.CreatedOn);
				//setCreatedOnYYYYMMDDState(PTTSfunctions.Commonfunctions.formatDate(aRow.CreatedOn);
				setModifiedByState(aRow.ModifiedBy);
				setModifiedOnState(!aRow.ModifiedOn ? JulianZero : aRow.ModifiedOn);
				//setModifiedOnYYYYMMDDState(PTTSfunctions.Commonfunctions.formatDate(aRow.ModifiedOn);

				setAgencyState(aRow.Agency > 0 ? aRow.Agency : 0);
				setAssignedDistrictState(aRow.AssignedDistrict);
				setCommandState(!aRow.Command ? "awaiting certification" : aRow.Command.length > 0 ? aRow.Command : "awaiting certification");
				setDateCardCompletedState(!aRow.DateCardCompleted ? JulianZero : aRow.DateCardCompleted);
				//setDateCardCompletedYYYYMMDDState(PTTSfunctions.Commonfunctions.formatDate(aRow.DateCardCompleted);
				console.log("HDR row for a Card");
				console.log(aRow);
				setDateCardStartedState(!aRow.DateCardStarted ? JulianZero : aRow.DateCardStarted);
				//setDateCardStartedYYYYMMDDState(PTTSfunctions.Commonfunctions.formatDate(aRow.DateCardStarted));
				setDateCompleted_DDState(!aRow.DateCardCompleted ? DDstate: DDinit(aRow.DateCardCompleted));
				setDateCompleted_MMState(!aRow.DateCardCompleted ? MMstate : MMinit(aRow.DateCardCompleted));
				setDateReturnedState(aRow.DateReturned);
				setDS1984NumberState(rcpt);
				setIDState(aRow.ID);
				setIssuingOfficerNameState(aRow.IssuingOfficerName);
				setIssuingOfficerSignatureState(!aRow.Command ? aRow.IssuingOfficerSignature : "electronically certified");
				setIssuingOfficerTitleState(aRow.IssuingOfficerTitle);
				setOfficerInChargeNameState(aRow.OfficerInChargeName);
				setOfficerInChargeTitleState(aRow.OfficerInChargeTitle);
				setRemarksState("can you see this?"); // aRow.Remarks;
				setTaxRegNumberState(TaxRegNumberState.length > 0 ? TaxRegNumberState : aRow.TaxRegNumber);

				setFormStatusIDState(aRow.FormStatusID > 2 ? aRow.FormStatusID : NotarizedOrVoid > 24 ? 2 : 1);
			}
		});

		return () => {
			mountedRef.current = false
		}
	},
		[])

	useEffect(() => {
		setoneDS21Card( // update the object all at once because it is passed around by reference
			{
			"ID": IDState,
			"IssuingOfficerSignature": IssuingOfficerSignatureState,
			"Command": CommandState,
			"IssuingOfficerName": IssuingOfficerNameState,
			"TaxRegNumber": TaxRegNumberState,
			"Agency": AgencyState,
			"DateCardStarted": DateCardStartedState,
			"DateReturned": DateReturnedState,
			"AssignedDistrict": AssignedDistrictState,
			"IssuingOfficerTitle": IssuingOfficerTitleState,
			"OfficerInChargeName": OfficerInChargeNameState,
			"OfficerInChargeTitle": OfficerInChargeTitleState,
			"DateCardCompleted": DateCardCompletedState,
			"Remarks": RemarksState,
			"DS1984Number": DS1984NumberState,
			"DateCompleted_MM": DateCompleted_MMState,
			"DateCompleted_DD": DateCompleted_DDState,
			"ModifiedOn": ModifiedOnState,
			"ModifiedBy": ModifiedByState,
			"CreatedBy": CreatedByState,
			"CreatedOn": CreatedOnState,
			"FormStatusID": FormStatusIDState,
			}
		);
	},
		[IDState,
		IssuingOfficerSignatureState,
		CommandState,
		IssuingOfficerNameState,
		TaxRegNumberState,
		AgencyState,
		DateCardStartedState,
		DateReturnedState,
		AssignedDistrictState,
		IssuingOfficerTitleState,
		OfficerInChargeNameState,
		OfficerInChargeTitleState,
		DateCardCompletedState,
		RemarksState,
		DS1984NumberState,
		DateCompleted_MMState,
		DateCompleted_DDState,
		ModifiedOnState,
		ModifiedByState,
		CreatedByState,
		CreatedOnState
		]
	);

	useEffect(() => {
		MakeInSynch();
		}
		, [oneDS21Card]
	)

	useEffect(() => {
		if (parseInt(CertifyingOfficerRefNoState) > 0) {
			if (CertifyingOfficerRefNoState.toString().length === 7) {
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("REF#", CertifyingOfficerRefNoState);
				(async () => {
					setLoading(true);

					const response: any = await allServices.EISgetServices.EISemployeeDetailsRN(CertifyingOfficerRefNoState); // axios always returns an object with a "data" child
					const rdata: any = response.data;
					if (!rdata) {
						setLoading(true);
					}
					else {
						if (!mountedRef.current) return null
						if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("EIS Data");
						if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
						if (PTTSfunctions.Commonfunctions2.isNotDefined(rdata.employeeModel)) {
							setCertifyingOfficerRefNoState("Invalid REF#: " + CertifyingOfficerRefNoState);
						}
						else {
							try {

								if (isNullOrUndefined(rdata.jobModel.dsnyInformation)) { throw "rdata.jobModel.dsnyInformation missing" };
								let eisPayCodeCorrelID: string = rdata.jobModel.dsnyInformation.payCodeId.correlation
								eisPayCodeCorrelID = eisPayCodeCorrelID.substring(eisPayCodeCorrelID.length - 4);
								//setPaycodeState( // this is an integral part of the DS-1984 Receipt# that will be composed by UID
								//	// actual paycode may be different; real one needs a lookup using this CorrelationID string
								//	eisPayCodeCorrelID
								//);
								//setIssuingOfficerTitleState(rdata.jobModel.dsnyInformation.titleId.correlation);
								//if (isNullOrUndefined(rdata.employeeModel)) { throw "rdata.employeeModel missing" };
								//setIssuingOfficerNameState(rdata.employeeModel.names[0].firstName.substring(0, 1)
								//	+ ' ' + rdata.employeeModel.names[0].lastName);
								//if (isNullOrUndefined(rdata.jobModel.badgeNumber)) { throw "rdata.jobModel.badgeNumber missing" };
								//setTaxRegNumberState(isNullOrUndefined(rdata.jobModel.badgeNumber)
								//	? "NA"
								//	: rdata.jobModel.badgeNumber.badgeNumber);  // need to change this to TAX ID aka. Pension Number


								setIssuingOfficerTitleState(rdata.jobModel.dsnyInformation.titleId.correlation + " (per EIS)");
								setIssuingOfficerNameState(rdata.employeeModel.names[0].firstName.substring(0, 1)
									+ ' ' + rdata.employeeModel.names[0].lastName);
								setTaxRegNumberState(isNullOrUndefined(rdata.jobModel.badgeNumber)
									? "NA"
									: rdata.jobModel.badgeNumber.badgeNumber);
								setCommandState(eisPayCodeCorrelID);
								alert("IssuingOfficer agency from EIS: " + rdata.jobModel.jobLocation.departmentId.correlation.substring(-7).substring(3));
								setAgencyState(parseInt(rdata.jobModel.jobLocation.departmentId.correlation.substring(-7).substring(3)));

								if (!allActions.DS21acts.DS21HDRsyncWithCard(dispatch, oneDS21Card, currUser,
									CertifyingOfficerRefNoState, parseInt(NOVs[0].ECBSummonsNumber), parseInt(NOVs[24].ECBSummonsNumber))) { throw "Reducers out-of-synch for DS21 " + oneDS21Card.DS1984Number }
							}
							catch (e) {
								alert(e.toString() + " using (" + CertifyingOfficerRefNoState + ").");
								setIssuingOfficerTitleState("type it in later");
								setTaxRegNumberState("type one in later");
							}

						}
						setLoading(false);
					}
				})();

				(async () => {
					setLoading(true);

					const response: any = await allServices.NTIgetServices.NTIemployeeDetailsRN(CertifyingOfficerRefNoState); // axios always returns an object with a "data" child
					const rdata: any = response.data;
					if (!rdata) {
						setLoading(true);
					}
					else {
						if (!mountedRef.current) return null
						if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("NTI Data");
						if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
						if (PTTSfunctions.Commonfunctions2.isNotDefined(rdata[0])) {
							setCertifyingOfficerRefNoState("Invalid REF#: " + CertifyingOfficerRefNoState);
						}
						else {
							try {

								//setIssuingOfficerTitleState(rdata[0].Title);
								//setIssuingOfficerNameState(rdata[0].Name);
								//setTaxRegNumberState(rdata[0].PensionNumber);
								//setCommandState(rdata[0].Command);
								//setAgencyState(rdata[0].AgencyId);

								setIssuingOfficerTitleState(rdata[0].Title);
								setIssuingOfficerNameState(rdata[0].Name);
								setTaxRegNumberState(rdata[0].PensionNumber);
								setCommandState(rdata[0].Command);
								setAgencyState(rdata[0].AgencyId);
								console.log("set Loading after NTI lookup");

								if (!allActions.DS21acts.DS21HDRsyncWithCard(dispatch, oneDS21Card, currUser,
									CertifyingOfficerRefNoState, parseInt(NOVs[0].ECBSummonsNumber), parseInt(NOVs[24].ECBSummonsNumber))) { throw "Reducers out-of-synch for DS21 " + oneDS21Card.DS1984Number }

								setLoading(false);
							}
							catch (e) {
								alert(e.toString() + " using " + CertifyingOfficerRefNoState + ".");
								setIssuingOfficerTitleState("type it in later");
								setTaxRegNumberState("type one in later");
							}

						}
					}
				})();
			}
			else {
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("numeric but not 7 digits -- officer")
			}
		}
		else {
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("part of Officer's last name");
		};
	},
		[CertifyingOfficerRefNoState]
	);

	useEffect(() => {
		if (parseInt(OfficerInChargeRefNoState) > 0) {
			if (OfficerInChargeRefNoState.toString().length === 7) {
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("REF#", OfficerInChargeRefNoState);
				(async () => {
					setLoading(true);

					const response: any = await allServices.EISgetServices.EISemployeeDetailsRN(OfficerInChargeRefNoState); // axios always returns an object with a "data" child
					const rdata: any = response.data;
					if (!rdata) {
						setLoading(true);
					}
					else {
						if (!mountedRef.current) return null
						if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("EIS Data");
						if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
						if (PTTSfunctions.Commonfunctions2.isNotDefined(rdata.employeeModel)) {
							setOfficerInChargeRefNoState("Invalid REF#: " + OfficerInChargeRefNoState);
						}
						else {
							try {

								if (isNullOrUndefined(rdata.jobModel.dsnyInformation)) { throw "rdata.jobModel.dsnyInformation missing" };

								//setOfficerInChargeTitleState(rdata.jobModel.dsnyInformation.titleId.correlation);
								if (isNullOrUndefined(rdata.employeeModel)) { throw "rdata.employeeModel missing" };
								//setOfficerInChargeNameState(rdata.employeeModel.names[0].firstName.substring(0, 1)
								//	+ ' ' + rdata.employeeModel.names[0].lastName);

								setOfficerInChargeTitleState(rdata.jobModel.dsnyInformation.titleId.correlation + " (per EIS)");
								setOfficerInChargeNameState(rdata.employeeModel.names[0].firstName.substring(0, 1)
									+ ' ' + rdata.employeeModel.names[0].lastName);

								if (!allActions.DS21acts.DS21HDRsyncWithCard(dispatch, oneDS21Card, currUser,
									CertifyingOfficerRefNoState, parseInt(NOVs[0].ECBSummonsNumber), parseInt(NOVs[24].ECBSummonsNumber))) { throw "Reducers out-of-synch for DS21 " + oneDS21Card.DS1984Number }
							}
							catch (e) {
								alert(e.toString() + " from EIS lookup using (" + OfficerInChargeRefNoState + ").");
								if (e.toString() === "rdata.jobModel.dsnyInformation missing") {
									setIssuingOfficerTitleState( // this is an integral part of the DS-1984 Receipt# that will be composed by UID
										// actual paycode may be different; real one needs a lookup using this CorrelationID string
										"UNKN"
									)
								};
							}

						}
						setLoading(false);
					}
				})();

				(async () => {
					setLoading(true);

					const response: any = await allServices.NTIgetServices.NTIemployeeDetailsRN(OfficerInChargeRefNoState); // axios always returns an object with a "data" child
					if (!mountedRef.current) return null
					const rdata: any = response.data;
					if (!rdata) {
						setLoading(true);
					}
					else {
						if (!mountedRef.current) return null
						if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("NTI Data");
						if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(rdata);
						if (PTTSfunctions.Commonfunctions2.isNotDefined(rdata[0])) {
							setOfficerInChargeRefNoState("Invalid REF#: " + OfficerInChargeRefNoState);
						}
						else {
							try {

								//setOfficerInChargeTitleState(rdata[0].Title);
								//setOfficerInChargeNameState(rdata[0].Name);

								oneDS21Card.OfficerInChargeTitle = rdata[0].Title + " (per NTI Core)";
								oneDS21Card.OfficerInChargeName = rdata[0].Name;

								if (!allActions.DS21acts.DS21HDRsyncWithCard(dispatch, oneDS21Card, currUser,
									CertifyingOfficerRefNoState, parseInt(NOVs[0].ECBSummonsNumber), parseInt(NOVs[24].ECBSummonsNumber))) { throw "Reducers out-of-synch for DS21 " + oneDS21Card.DS1984Number }

								console.log("set Loading after Officer-In-Charge NTI lookup");
								setLoading(false);
							}
							catch (e) {
								alert(e.toString() + " for NTI lookup using " + OfficerInChargeRefNoState + ".");
								//setIssuingOfficerTitleState("type it in later");
							}

						}
					}
				})();
			}
			else {
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("numeric but not 7 digits -- officer")
			}
		}
		else {
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("part of Officer's last name");
		};
	},
		[OfficerInChargeRefNoState]
	);

	useEffect(() => {
		(async () => {
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Submission starting?", SaveCardRequestInProgress);
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(oneDS21Card);
			if (SaveCardRequestInProgress) {



				const response = await allServices.PTTSiudServices.iudDS21HDR(tEF);
				// response has the DS1984 receipt number or an exception message
				if (!response) {
					if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("I am having trouble with saving DS21 values.");
					// Do not alert because the await will return after the if(!response) is first evaluated.
				}
				else {
					if (!mountedRef.current) return null
					if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("return from iudDS21HDR with this response:");
					if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(response);
					history.push('/PTTS/DS21Card/' + response.data); // <-- Address bar will have an error msg if necessary.
				}
			}

			setSaveCardRequestInProgress(false);


			return () => {
				mountedRef.current = false
			}
		})();
	},
		[SaveCardRequestInProgress]);

	const renderDS21CompleteStatus = () => {
		//ID	Description
		//1	Less than 25 complete
		//2	All 25 complete
		//3	DS21 sent to borough
		//4	DS21 received by borough
		//5	DS21 rejected by borough
		//6	DS21 sent to head quarters
		//7	DS21 received by headquarters
		//8	DS21 rejected by headquarters
		return (
			<React.Fragment>
				<Box display="flex"
					flexWrap="nowrap"
					border={1}>
					<Grid item xs={1} className={classes.styleForCheckBox}><Box borderRight={1}><Checkbox checked={NotarizedOrVoid < 25} /></Box></Grid>
					<Grid item xs={11} className={classes.styleForCheckBoxCompanion}>Less than 25 complete</Grid>
				</Box>
				<Box display="flex"
					flexWrap="nowrap"
					border={1}>
					<Grid item xs={1} className={classes.styleForCheckBox}><Box borderRight={1}><Checkbox checked={NotarizedOrVoid > 24} /></Box></Grid>
					<Grid item xs={11} className={classes.styleForCheckBoxCompanion}>All 25 complete</Grid>
				</Box>
				<Box display="flex"
					flexWrap="nowrap" className={NotarizedOrVoid > 24 ? classes.styleForCheckBox : classes.styleForCheckBoxHidden}
					border={1}>
					<Grid item xs={1}><Box borderRight={1}>
						<Checkbox
							name="chkBOROsentTo"
							onClick={(e) => handleOnChange(e)}
							checked={CurrFormStatusID === 3} />
					</Box></Grid>
					<Grid item xs={11} className={classes.styleForCheckBoxCompanion}>DS21 sent to borough</Grid>
				</Box>
				<Box display="flex"
					flexWrap="nowrap" className={CurrFormStatusID > 2 ? classes.styleForCheckBox : classes.styleForCheckBoxHidden}
					border={1}>
					<Grid item xs={1}><Box borderRight={1}>
						<Checkbox
							name="chkBOROrec"
							onClick={(e) => handleOnChange(e)}
							checked={CurrFormStatusID === 4} />
					</Box></Grid>
					<Grid item xs={11} className={classes.styleForCheckBoxCompanion}>DS21 receieved at borough</Grid>
				</Box>
				<Box display="flex"
					flexWrap="nowrap" className={CurrFormStatusID > 3 ? classes.styleForCheckBox : classes.styleForCheckBoxHidden}
					border={1}>
					<Grid item xs={1}><Box borderRight={1}>
						<Checkbox
							name="chkBOROrej"
							onClick={(e) => handleOnChange(e)}
							checked={CurrFormStatusID === 5} />
					</Box></Grid>
					<Grid item xs={11} className={classes.styleForCheckBoxCompanion}>DS21 rejected by borough</Grid>
				</Box>
				<Box display="flex"
					flexWrap="nowrap" className={CurrFormStatusID > 3 ? classes.styleForCheckBox : classes.styleForCheckBoxHidden}
					border={1}>
					<Grid item xs={1}><Box borderRight={1}>
						<Checkbox
							name="chkHQsentTo"
							onClick={(e) => handleOnChange(e)}
							checked={CurrFormStatusID === 6} />
					</Box></Grid>
					<Grid item xs={11} className={classes.styleForCheckBoxCompanion}>DS21 sent to headquarters</Grid>
				</Box>
				<Box display="flex"
					flexWrap="nowrap" className={CurrFormStatusID > 5 ? classes.styleForCheckBox : classes.styleForCheckBoxHidden}
					border={1}>
					<Grid item xs={1}><Box borderRight={1}>
						<Checkbox
							name="chkHQrec"
							onClick={(e) => handleOnChange(e)}
							checked={CurrFormStatusID === 7} />
					</Box></Grid>
					<Grid item xs={11} className={classes.styleForCheckBoxCompanion}>DS21 received by headquarters</Grid>
				</Box>
				<Box display="flex"
					flexWrap="nowrap" className={CurrFormStatusID > 6 ? classes.styleForCheckBox : classes.styleForCheckBoxHidden}
					border={1}>
					<Grid item xs={1}><Box borderRight={1}>
						<Checkbox
							name="chkHQrej"
							onClick={(e) => handleOnChange(e)}
							checked={CurrFormStatusID === 8} />
					</Box></Grid>
					<Grid item xs={11} className={classes.styleForCheckBoxCompanion}>DS21 rejected by headquarters</Grid>
				</Box>
			</React.Fragment>
		);
	}


	function renderValidationMsgs() {
		return (
			<Grid item xs={12}>
				<ErrorMessage
					errors={errors}
					name="fldDateCardStarted"
					as={<ErrorMessageContainer />}
				/>
			</Grid>
		)
	}

	const DateCardStartedIsValid = () => {
		//console.log("PTTSfunctions.Commonfunctions2.isDateType(DateCardStartedState)", PTTSfunctions.Commonfunctions2.isDateType(DateCardStartedState));
		//console.log("DateCardStartedState.getFullYear()", moment(DateCardStartedState.toString()).toDate().getFullYear());
		//console.log("JulianZero.getFullYear()", JulianZero.getFullYear());
		//console.log("DateCardStartedState.substr(0,4)", DateCardStartedState.substr(0, 4));
		//console.log("JulianZeroYYYYmmDD.substr(0,4)", JulianZeroYYYYmmDD.substr(0, 4));
		const aBol: boolean =
			!(PTTSfunctions.Commonfunctions2.isDateType(DateCardStartedState))
			////!((oneDS21Card.DateCardStarted))
			? false
				:
			!(moment(DateCardStartedState.toString()).toDate().getFullYear() === JulianZero.getFullYear())
		//console.log("DateCardStartedIsValid", aBol);
		//console.log("DateCardStarted Year is -> ", !(PTTSfunctions.Commonfunctions2.isDateType(oneDS21Card.DateCardStarted)) ? "cannot use getFullYear()" : oneDS21Card.DateCardStarted.getFullYear());
		return aBol;
	}

	const submitForm = (e: any) => {
		// convert tDS21Card to tEfDS21HDR
		settEF({
			Year: moment(oneDS21Card.DateCardCompleted.toString()).toDate().getFullYear(),
			IssuingOfficerSignature: oneDS21Card.IssuingOfficerSignature,
			Command: oneDS21Card.Command,
			IssuingOfficerName: oneDS21Card.IssuingOfficerName,
			TaxRegNumber: oneDS21Card.TaxRegNumber,
			Agency: !oneDS21Card.Agency ? "" : oneDS21Card.Agency.toString(),
			DateCardStarted: (oneDS21Card.DateCardStarted),
			DateReturned: oneDS21Card.DateReturned,
			AssignedDistrict: oneDS21Card.AssignedDistrict,
			IssuingOfficerTitle: oneDS21Card.IssuingOfficerTitle,
			OfficerInChargeName: oneDS21Card.OfficerInChargeName,
			OfficerInChargeTitle: oneDS21Card.OfficerInChargeTitle,
			DateCardCompleted: oneDS21Card.DateCardCompleted,
			Remarks: oneDS21Card.Remarks,
			DS1984Number: oneDS21Card.DS1984Number,
			ModifiedBy: currUser,
			FormStatusID: CurrFormStatusID,
			// *
			//ID	Description
			//1	Less than 25 complete
			//2	All 25 complete
			//3	DS21 sent to borough
			//4	DS21 received by borough
			//5	DS21 rejected by borough
			//6	DS21 sent to head quarters
			//7	DS21 received by headquarters
			//8	DS21 rejected by headquarters
			//
		});

		setSaveCardRequestInProgress(true);
	};

	const dispDateCardStarted: string = !DateCardStartedIsValid() ? moment(JulianZero.toString()).format("YYYY-MM-DD") : moment(DateCardStartedState).format("YYYY-MM-DD");
	const renderDS21WorkFlow = () => {
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("oneDS21Card has this: ");
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(oneDS21Card);
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DateCardStartedIsValid", DateCardStartedIsValid());
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("moment format of DateCardStartedState", moment(DateCardStartedState).format("YYYY-MM-DD"));
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("dispDateCardStarted", dispDateCardStarted);

		return (
			<React.Fragment>
				<Box display="flex" className={classes.styleForBottomButtons}
					flexWrap="wrap"
					border={1}>
					<Grid item xs={3} className={classes.styleForCheckBoxCompanion}>Date card started:</Grid>
					<Grid item xs={9}>
							<KeyboardDatePicker
								name="fldDateCardStarted"
								value={!DateCardStartedState ? JulianZero : DateCardStartedState}
								initialFocusedDate={new Date() }
								onChange={fldDate => {
									setDateCardStartedState(moment(!fldDate ? JulianZero.toString() : fldDate.toString()).toDate());
								}}
								variant="inline"
								format="MM/dd/yyyy"
							/>
					</Grid>
					<Grid item xs={3}
						className={
								!DateCardStartedIsValid()
										? classes.styleforWorkFlowTextMMDDhidden
										: classes.styleForCheckBoxCompanion}
					>Date tickets sent to ENF HQ:</Grid>
					<Grid item xs={9}
						className={
							!DateCardStartedIsValid()
										? classes.styleforWorkFlowTextMMDDhidden
										: classes.styleForBottomButtonsLeft}
						><Input readOnly={false}
						type="date"
						onChange={(e) => handleOnChange(e)}
						name="fldDateReturned" ref={register}
							defaultValue={!DateReturnedState ? " not entered " : PTTSfunctions.Commonfunctions.formatDateMMdYYYY(DateReturnedState)} />
					</Grid>
					<Grid item xs={3} className={classes.styleForBottomButtonsLeft}>Assigned District</Grid>
					{
						// DS1984 [District]
						// ,[ReceivingOfficerName]
						// ,[ReceivingOfficerTitle]
						// is the same as DS21 [AssignedDistrict]
						// , [IssuingOfficerName]
						// , [IssuingOfficerTitle] 
						// Book of 25 is not supposed to be shared.
						// DS1984 says who accepted the book of 25.
						// DS21 says what happened to the book of 25.
						// OfficerInChargeName has to be provided via an employee REF#
					}
					<Grid item xs={9} className={classes.styleForBottomButtonsLeft}><Input readOnly={true} className={classes.styleforWorkFlowText} type="text"
						placeholder="e.g., BKS10"
						name="fldAssignedDistrict" ref={register}
						value={!AssignedDistrictState ? "" : AssignedDistrictState} /></Grid>
					<Grid item xs={3} className={classes.styleForBottomButtonsLeft}>Issuing Officers Name & Location :</Grid>
					<Grid item xs={4} className={classes.styleForBottomButtonsLeft}>
						<Input readOnly={true} className={classes.styleforWorkFlowText} type="text"
						placeholder="full name"
						name="fldIssuingOfficerName" ref={register}
							value={!IssuingOfficerNameState ? "" : IssuingOfficerNameState} /></Grid>
					<Grid item xs={5} className={classes.styleForBottomButtonsLeft}><Input readOnly={true} className={classes.styleforWorkFlowText} type="text"
						placeholder="district or location name"
						name="fldIssuingOfficerTitle" ref={register}
						value={!IssuingOfficerTitleState ? "" : IssuingOfficerTitleState} /></Grid>
					<Grid item xs={3} className={classes.styleForBottomButtonsLeft}>Officer in charge name:</Grid>
					<Grid item xs={9} className={classes.styleForBottomButtonsLeft}><Input readOnly={false} className={classes.styleforWorkFlowText} type="text"
						onChange={(e) => handleOnChange(e)}
						name="fldOfficerInChargeREFNO"
						placeholder="REF# w/zeroes"
						ref={register({
							required: { value: true, message: "PTTS expects a reference number of the Officer-In-Charge." },
							pattern: {
								value: /^[A-Za-z]|([\w]+['][\w]+)|^\d{7}$/,
								message: "Distributor: Enter a seven digit REF# so PTTS can look up employee information."
							}
						})}
						defaultValue={
								!OfficerInChargeNameState ? "not entered"
									: OfficerInChargeNameState.length > 0
										? OfficerInChargeNameState
										: "unavailable"
						}
					/></Grid>
					<Grid item xs={3} className={classes.styleForBottomButtonsLeft}>Officer in charge title:</Grid>
					<Grid item xs={9} className={classes.styleForBottomButtonsLeft}><Input readOnly={true} className={classes.styleforWorkFlowText} type="text"
						name="fldOfficerInChargeTitle" ref={register}
						placeholder="district or location name"
						value={!OfficerInChargeTitleState ? "" : OfficerInChargeTitleState} /></Grid>
					<Grid item xs={12} className={classes.styleForBottomButtonsLeft}>
						<li>Issuing Officer Must Issue All 25 NOVs</li>
						<li>Issue NOVs In Numerical Order</li>
						<li>Do Not Share Pack</li>
						<li>Return Voids With Days Work</li>
						<li>Keep All Yellow Copies For One Year</li>
						<div>Cards Must Be Submitted Upon Completion</div>
					</Grid>
					<Grid item xs={3} className={
						!DateCardStartedIsValid()
								? classes.styleforWorkFlowTextMMDDhidden
								: classes.styleForCheckBoxCompanion
					}>Date Card Completed <br />and Sent To Summons Control</Grid>
					<Grid item xs={3} className={
						!DateCardStartedIsValid()
								? classes.styleforWorkFlowTextMMDDhidden
								: classes.styleForBottomButtonsLeft}
					>
						<Input readOnly={false}
						type="date"
						onChange={(e) => handleOnChange(e)}
							name="fldDateCardCompleted"
							ref={register}

							defaultValue={!DateCardCompletedState ? " not entered " : PTTSfunctions.Commonfunctions.formatDateMMdYYYY(DateCardCompletedState)}
						/>&nbsp;&nbsp;
					</Grid>
					<Grid item xs={3} className={
						!DateCardStartedIsValid()
									? classes.styleforWorkFlowTextMMDDhidden
									: classes.styleForBottomButtonsLeft}
					>
						<Input readOnly={true} className={ classes.styleForMMDD } type="text" placeholder="mm"
							value={MMstate}
						/>&nbsp;
						<Input readOnly={true} className={classes.styleForMMDD}  type="text"  placeholder="dd"
							value={DDstate}
						/>
					</Grid>
					<Grid item xs={12} className={
							!DateCardStartedIsValid()
									? classes.styleforWorkFlowTextMMDDhidden
									: classes.styleForBottomButtonsLeft
					}>1824 Shore Parkway</Grid>
					<Grid item xs={12} className={classes.styleForBottomButtonsLeft}>&nbsp;</Grid>
					<Grid item xs={12} className={classes.styleForBottomButtonsLeft}>Remarks</Grid>
					<Grid item xs={12} className={classes.styleForBottomButtonsLeft}><TextField rowsMax={4}
						onChange={(e) => handleOnChange(e)}
						name="fldRemarks" ref={register}
						aria-label="maximum height" placeholder="notes about special circumstances (optional)"
						className={classes.styleforWorkFlowText} /></Grid>
					<Grid item xs={12} className={classes.styleForBottomButtonsLeft}>&nbsp;</Grid>
				</Box>
				<Grid item xs={12}>
					&nbsp;
					</Grid>
			</React.Fragment>
		);
	}

	const renderDS21Certification = () => {
		return (
			<React.Fragment>
				<div>I Certify that the summonses listed above have been properly served and delivered to summons control</div>
				<Grid item xs={12}>
					<Box display="flex"
						flexWrap="nowrap"
						border={1}>
						<Grid item xs={4} className={classes.styleForGroup}>
							Rank/Signature Of Summons Issuing Officer<br />
							<input className={classes.styleForGroup} type="text"
								onChange={(e) => handleOnChange(e)}
								name="fldCertifyingOfficerREFNO"
								placeholder="REF# w/zeroes"
								ref={register({
									required: { value: true, message: "PTTS expects a reference number for the Distributor." },
									pattern: {
										value: /^[A-Za-z]|([\w]+['][\w]+)|^\d{7}$/,
										message: "Distributor: Enter a seven digit REF# so PTTS can look up employee information."
									}
								})}
								defaultValue={
									CommandState.length === 0
										? CertifyingOfficerRefNoState
										: IssuingOfficerNameState.length > 0
											? "Electronically Certified"
											: CertifyingOfficerRefNoState
								}
							/>
						</Grid>

						<Grid item xs={4} className={classes.styleForGroup}>
							<Box borderLeft={1}>Command<br />
								{CommandState.length === 0
									? "awaiting certification"
									: CommandState}
							</Box></Grid>

						<Grid item xs={4} className={classes.styleForGroup}>
							<Box border={1}><div>DS 1984 #<br />{rcpt}</div></Box>
						</Grid>
					</Box>
				</Grid>
				<Grid item xs={12}>
					<Box display="flex"
						flexWrap="nowrap"
						border={1}>
						<Grid item xs={4} className={classes.styleForGroup}>Surname, First<br />
							{CommandState.length === 0 ? "awaiting certification" : IssuingOfficerNameState}
						</Grid>

						<Grid item xs={4} className={classes.styleForGroup}>
							<Box borderLeft={1}>Tax Registry No.<br />
								{CommandState.length === 0 ? "awaiting certification" :
									TaxRegNumberState}
							</Box></Grid>
						<Grid item xs={4} className={classes.styleForGroup}>
							<Box borderLeft={1}>Agency<br />
								{CommandState.length === 0
									? "awaiting certification" 
									: AgencyState.toString()
										}
							</Box></Grid>
					</Box>
				</Grid>
			</React.Fragment>
		);
	}

	if (isLoading) {
		return (<div>Loading ...</div>);
	}
	else {
		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("rcpt is ", rcpt);
		return (
			<form onSubmit={handleSubmit(submitForm)}>
				<Grid container className={classes.styleForBottomContainer}>

					<Grid item xs={3}>
						{renderDS21CompleteStatus()}
					</Grid>

					<Grid item xs={1}>
						&nbsp;
					</Grid>
					<Grid item xs={8}>
						{renderDS21Certification()}
					</Grid>
					<Grid item xs={12} className={classes.styleForDivider}>
						&nbsp;
					</Grid>
					<Grid item xs={12} className={classes.styleForBottomButtons}>
						<Box display="flex"
							flexWrap="nowrap"
							border={2}>
							<Grid item xs={6} className={classes.styleForBottomButtonsRight}>
								<Button onClick={(e) => submitForm(e)}
									variant="contained" color="primary">
									Save (in-progress) Card
									</Button></Grid>
							<Grid item xs={6} >
								<Box >
									{CertifyingOfficerRefNoState > "" ?
										<Button className={classes.styleForBottomButtonsLeft}
											variant="contained" color="secondary">
											Card Completed
									</Button> : <div>&nbsp;</div>}
								</Box></Grid>
						</Box>
					</Grid>
					<Grid item xs={12} >
						<Box display="flex"
							flexWrap="nowrap"
							border={2}>
							&nbsp;
						</Box>
					</Grid>
					<Grid item xs={12} >
						<Box display="flex"
							flexWrap="nowrap"
							border={2}>
							{renderDS21WorkFlow()}
						</Box>
					</Grid>
				</Grid>
			</form>
		)
	}

};


const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5,
		marginTop: 0
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
	root: {
		flexGrow: 1,
		height: "100%",
		paddingLeft: 15,
		paddingRight: 15,

	},
	formposition: {
		marginLeft: 280,
	},
	darkborder: {
		borderWidth: 3,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderTop: {
		borderTopWidth: 2,
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderLeft: {
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderTopWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	styleforlogo: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforsubmit: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforformtitle: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
	},
	styleforformtitletext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "black",
		minWidth: 500,
	},
	styleforformorder: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "center",
	},
	styleforformordertext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "center",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 500,
		color: "black",
	},
	styleforticketrange: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
	},
	stylefortopacknowledgement: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	stylefortopacknowledgementtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 320,
		minWidth: 130
	},
	stylefortopdistributionrow: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
		maxWidth: 300,
		wordWrap: "break-word",
	},
	stylefortopdistributionrowtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
		wordWrap: "break-word",
	},
	stylefortopdistributionrowtextwide: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		minWidth: 300,
		maxWidth: 600,
	},
	styleforformreceipt: {
		verticalAlign: "center",
		borderTopStyle: "none",
	},
	styleforformreceipttext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		display: "inline",
		marginLeft: 10,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
		textAlign: "center",
	},
	styleforformreceipttextred: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "red",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
	},
	button: {
		marginTop: 20,
	},

	formstyle: {
		maxWidth: "100%",
		padding: 0,
	},

	input: {
		display: "block",
		borderRadius: 4,
		border: "1px solid",
		padding: "10px 15px",
		marginBottom: 10,
		fontSize: 14,
		maxWidth: 400,
		minWidth: 200,
	},

	reactselect: {
		maxWidth: 400,
	},

	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginBottom: 13,
		marginTop: 20,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
	},
	buttonSubmit: {
		background: "#0e8c1b",
		color: "white",
		border: "none",
		marginTop: 20,
		padding: 20,
		fontSize: 16,
		fontWeight: 100,
		width: 200,
	},
	styleForBottomContainer: {
		wrap: 'nowrap',
		maxWidth: "85%",
	},
	styleforWorkFlowText: {
		width: "100%",
	},
	styleforWorkFlowTextMMDD: {
		width: 30,
	},
	styleforWorkFlowTextMMDDhidden: {
		display: "none",
	},
	styleForBottomButtons: {
		textAlign: "left",
		border: 2,
	},
	styleForBottomButtonsLeft: {
		textAlign: "left",
		paddingLeft: 5,
	}, styleForBottomButtonsRight: {
		textAlign: "right",
		borderLeft: 2,
		paddingRight: 5,
	}, styleForBottomButtonsCenter: {
		textAlign: "center",
		border: 2,
		paddingRight: 5,
		paddingLeft: 5,
	},
	styleForDivider: {
		paddingTop: 100,
	},
	styleForGroup: {
		border: 2,
		borderColor: 'black',
	},
	styleForCheckBoxCompanion: {
		alignContent: 'left',
		paddingTop: 10,
		paddingLeft: 5,
	},
	styleForCheckBox: {
		textAlign: 'right',
	},
	styleForCheckBoxHidden: {
		textAlign: 'right',
		display: 'none'
	},
	styleForMMDD: {
		maxWidth: "3ch",
	},
	textField: {
		marginLeft: theme.spacing(1),
		marginRight: theme.spacing(1),
		width: 200,
		},
	}),
);
