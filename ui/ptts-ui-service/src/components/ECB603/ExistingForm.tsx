﻿import { Button } from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useEffect, useState } from "react"; // , useReducer
import { useForm } from "react-hook-form"; // , FormProvider
import { isNullOrUndefined } from 'util';
import { PTTSfunctions } from '../basicComponents';
import { allServices } from '../../services';
import { history } from "../../configureStore";
import { ECB603 } from '../../model';

type postDataResult<T> = {
	success: boolean;
	errors?: { [P in keyof T]?: string[] };
};

type FormData = {
	ecbReceiptnumber: string;
};

export function ExistingECB603Form(props: any) {
	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props);
	 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props.match.params.onercpt);

	const [isLoading, setLoading] = React.useState(false);

	const [lastgoodrcpt, setlastgoodrcpt] = useState("");
	const [rcpt, setrcpt] = useState(props.match.params.onercpt);

	const [oneECB603form, setoneECB603form] = useState<ECB603>(
		{
			CartonNoFrom: 100,
			CartonNoTo: 8,
			CartonQty: 1000,
			CreatedBy: "xyz",
			CreatedOn: new Date(),
			CustomerOrderNo: "",
			DS1983ReceiptNo: "320200720L140000001205",
			FactoryJobNo: "8",
			ID: 1301,
			IsVoid: false,
			ModifiedOn: new Date(),
			ModifiedBy: "mranjan",
			NoOfParts: 4,
			OrderNo: "05-128417-A",
			ReceiptNo: "620200522ENHQ000001301",
			TicketNoFrom: 21241001,
			TicketNoTo: 21242000,
			Completed: true
		});


	const [CartonNoFromState, setCartonNoFromState] = useState(0);
	const [CartonNoToState, setCartonNoToState] = useState(0);
	const [CartonQtyState, setCartonQtyState] = useState(0);
	const [CreatedByState, setCreatedByState] = useState("uidToDo");
	const [CreatedOnState, setCreatedOnState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [CustomerOrderNoState, setCustomerOrderNoState] = useState("text");
	const [DS1983ReceiptNoState, setDS1983ReceiptNoState] = useState("text");
	const [FactoryJobNoState, setFactoryJobNoState] = useState("text");
	const [IDState, setIDState] = useState(0);
	const [IsVoidState, setIsVoidState] = useState(false);
	const [ModifiedByState, setModifiedByState] = useState("uidToDo");
	const [ModifiedOnState, setModifiedOnState] = useState(PTTSfunctions.Commonfunctions.todaysdate);
	const [NoOfPartsState, setNoOfPartsState] = useState(0);
	const [OrderNoState, setOrderNoState] = useState("text");
	const [ReceiptNoState, setReceiptNoState] = useState("text");
	const [TicketNoFromState, setTicketNoFromState] = useState(0);
	const [TicketNoToState, setTicketNoToState] = useState(0);
	const [CompletedState, setCompletedState] = useState(false);


	useEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useEffect after onChange handle.");
		setoneECB603form({ // take the individual hook const and set each oneECB603form object properties
			CartonNoFrom: CartonNoFromState,
			CartonNoTo: CartonNoToState,
			CartonQty: CartonQtyState,
			CreatedBy: CreatedByState,
			CreatedOn: new Date(CreatedOnState),
			CustomerOrderNo: CustomerOrderNoState,
			DS1983ReceiptNo: "",
			FactoryJobNo: FactoryJobNoState,
			ID: IDState,
			IsVoid: IsVoidState,
			ModifiedBy: "useEffect",
			ModifiedOn: new Date(ModifiedOnState),
			NoOfParts: NoOfPartsState,
			OrderNo: OrderNoState,
			ReceiptNo: ReceiptNoState,
			TicketNoFrom: TicketNoFromState,
			TicketNoTo: TicketNoToState,
			Completed: CompletedState,

		});
	},
		[
		CartonNoFromState,
		CartonNoToState,
		CartonQtyState,
		// CreatedByState,
		// CreatedOnState, should be set when object is first created; no update allowed
		CustomerOrderNoState,
		// DS1983ReceiptNoState,
		FactoryJobNoState,
		// IDState,
		// IsVoidState,
		// ModifiedByState,
		// ModifiedOnState,
		NoOfPartsState,
		OrderNoState,
		// ReceiptNoState,
		TicketNoFromState,
		// TicketNoToState, recalculated when TicketNoFromState or CartonQtyState changes
		CompletedState,
	]
	);

	const handleOnChange = (e: any) => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
		switch (e.target.name) {
			case "fldCartonNoFrom": setCartonNoFromState(e.target.value); break;
			case "fldCartonNoTo":
				setCartonNoToState(e.target.value);
				break;
			case "fldCartonQty":
				let dummy1: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				setCartonQtyState(dummy1);
				let dummy3: number = Number.parseInt(TicketNoFromState.toString());
				let updatedTktNo = dummy1 + Number.parseInt(TicketNoFromState.toString()) - 1;
				dummy3 > 9999999 && dummy1 > 0 ? setTicketNoToState(updatedTktNo) : dummy1 = 0;
				break;
			// case "fldCreatedBy": setCreatedByState(e.target.value); break;
			// case "fldCreatedOn": setCreatedOnState(e.target.value); break;
			case "fldCustomerOrderNo": setCustomerOrderNoState(e.target.value); break;
			case "fldDS1983ReceiptNo": setDS1983ReceiptNoState(e.target.value); break;
			case "fldFactoryJobNo": setFactoryJobNoState(e.target.value); break;
			// case "fldID": setIDState(e.target.value); break;
			case "fldIsVoid": setIsVoidState(e.target.value); break;
			// case "fldModifiedBy": setModifiedByState(e.target.value); break;
			// case "fldModifiedOn": setModifiedOnState(e.target.value); break;
			case "fldNoOfParts": setNoOfPartsState(e.target.value); break;
			case "fldOrderNo":
				// oneECB603form.OrderNo = e.target.value;
				setOrderNoState(e.target.value);
				break;
			// case "fldReceiptNo": setReceiptNoState(e.target.value); break;
			case "fldTicketNoFrom":
				let dummy2: number = Number.parseInt(e.target.value); // trick to do nothing when starting ticket number is less than 8 digits
				setTicketNoFromState(dummy2);
				let updatedTktNo2 = dummy2 + Number.parseInt(CartonQtyState.toString()) - 1;
				dummy2 > 9999999 && CartonQtyState > 0 ? setTicketNoToState(updatedTktNo2) : dummy2 = 0;
				break;
			// case "fldTicketNoTo": setTicketNoToState(e.target.value); break;
			case "fldCompleted": setCompletedState(e.target.value); break;


			default:
				// code block
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
				 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.value);
		}
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("At least one new property value in:")
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(oneECB603form);
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(OrderNoState);
	}


	useEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useEffect triggered for " + rcpt);
		if (!isNullOrUndefined(rcpt)) {
			if (rcpt.length > 0 && rcpt !== lastgoodrcpt) {
				setlastgoodrcpt(rcpt);
				(async () => {
					setLoading(true);
					const response = await allServices.PTTSgetServices.anECB603(rcpt);
					const rspJson = await response.data;
					if (!isNullOrUndefined(rspJson)) {
						if (rspJson.CartonNoFrom !== undefined) {
							setCartonNoFromState(rspJson.CartonNoFrom);
							setCartonNoToState(rspJson.CartonNoTo);
							setCartonQtyState(rspJson.CartonQty);
							setCreatedByState(rspJson.CreatedBy);
							setCreatedOnState(rspJson.CreatedOn);
							setCustomerOrderNoState(rspJson.CustomerOrderNo);
							setDS1983ReceiptNoState(rspJson.DS1983ReceiptNo);
							setFactoryJobNoState(rspJson.FactoryJobNo);
							setIDState(rspJson.ID);
							setIsVoidState(rspJson.IsVoid);
							setModifiedOnState(PTTSfunctions.Commonfunctions.todaysdate);
							setModifiedByState("useEffect");
							setNoOfPartsState(rspJson.NoOfParts);
							setOrderNoState(rspJson.OrderNo);
							setReceiptNoState(rspJson.ReceiptNo);
							setTicketNoFromState(rspJson.TicketNoFrom);
							setTicketNoToState(rspJson.TicketNoTo);
							setCompletedState(true);
						}
					}
					setLoading(false);
				})();
			}
		}
	}, []);

	useEffect(() => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("useEffect triggered for " + rcpt);
		if (!isNullOrUndefined(rcpt)) {
			if (rcpt.length > 0 && rcpt !== lastgoodrcpt) {
				setlastgoodrcpt(rcpt);
				(async () => {
					setLoading(true);
					const response = await allServices.PTTSgetServices.anECB603(rcpt);
					const rspJson = await response.data;
					if (!isNullOrUndefined(rspJson)) {
						if (rspJson.CartonNoFrom !== undefined) {
							setoneECB603form({
								CartonNoFrom: rspJson.CartonNoFrom,
								CartonNoTo: rspJson.CartonNoTo,
								CartonQty: rspJson.CartonQty,
								CreatedBy: rspJson.CreatedBy,
								CreatedOn: rspJson.CreatedOn,
								CustomerOrderNo: rspJson.CustomerOrderNo,
								DS1983ReceiptNo: rspJson.DS1983ReceiptNo, // "null" until DS1983 is created for this carton
								FactoryJobNo: rspJson.FactoryJobNo,
								ID: rspJson.ID,
								IsVoid: rspJson.IsVoid,
								ModifiedOn: new Date(),
								ModifiedBy: "useEffect",
								NoOfParts: rspJson.NoOfParts,
								OrderNo: rspJson.OrderNo,
								ReceiptNo: rspJson.ReceiptNo,
								TicketNoFrom: rspJson.TicketNoFrom,
								TicketNoTo: rspJson.TicketNoTo,
								Completed: true
							});
						}
					}
					setLoading(false);

				})();
			}
		}
	}, [lastgoodrcpt]);

	const { register, handleSubmit, errors, formState } = useForm<FormData>();

	const submitForm = async (data: FormData, e: any) => {
		 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Submission starting", oneECB603form);

		const response = await allServices.PTTSiudServices.iudECB603(oneECB603form);
		// response is the new receipt number
		if (!response) {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("I am having trouble with saving ECB603 values.");
			// any error message is in the CustomerOrderNo property in the controller
		}
		else {
			history.push('/PTTS/existingECB603/' + oneECB603form.ReceiptNo);
		}
		e.target.reset(); // reset after form submit
	};

	const onCancel = () => {
		history.push('/PTTS/home');
	};

	const newECB603 = () => {
		history.push('/PTTS/newECB603');
	};

	const newDS1983 = () => {
		history.push('/PTTS/newDS1983', oneECB603form );
	};

	const classes = useStyles();

	if (isLoading) {
		return (<div>Loading ...</div>);
	}
	else {

		const renderActionButtons = () => {
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("oneECB603form.DS1983ReceiptNo is ", oneECB603form.DS1983ReceiptNo);
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(oneECB603form.DS1983ReceiptNo);
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(!isNullOrUndefined(oneECB603form.DS1983ReceiptNo));
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(!oneECB603form.DS1983ReceiptNo ? 0 : oneECB603form.DS1983ReceiptNo.length);
			if (!isNullOrUndefined(oneECB603form.DS1983ReceiptNo)) // prevent save/update
			{
				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							type="submit"
						>
							Update
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="secondary"
							onClick={() => newECB603()}
						>
							Make new ECB603
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="secondary"
							onClick={() => newDS1983()}
						>
							Create DS-1983
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}
			else if (PTTSfunctions.Commonfunctions2.isNotDefined(oneECB603form.DS1983ReceiptNo)) // prevent save/update
			{
				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={() => newECB603()}
						>
							Make new ECB603
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="secondary"
							onClick={() => newDS1983()}
						>
							Create DS-1983
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}
			else {

				return (
					<div>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							type="submit"
						>
							Update
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={() => newDS1983()}
						>
							Create DS-1983
							</StyledButton>
						<StyledButton
							className={classes.button}
							variant="outlined"
							color="primary"
							onClick={onCancel}>
							Cancel
						</StyledButton>
					</div>
				)
			}

		};

		return (
			<form onSubmit={handleSubmit(submitForm)}>
				<Grid className={classes.formposition}>
					<Grid container className={classes.darkborder}>
						<Grid item xs={2} className={classes.styleforlogo}>
							<img src={PTTSfunctions.Logos.DSNYLogo.logoImg} alt="Sanitation Enforcement" />
						</Grid>
						<Grid item xs={9} className={classes.styleforformtitle}>
							<div className={classes.styleforformtitletext}>THE CITY OF NEW YORK Department of Sanitation</div>
							<div className={classes.styleforformtitletext}>NOTICE OF VIOLATION & HEARING Form ECB603</div>
						</Grid>
					</Grid>
					<div>&nbsp;</div>
					<div>&nbsp;</div>
					<Grid container>
						<Grid item xs={12}>
							{renderActionButtons()}
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={11} className={classes.styleforformreceipt}>
							<div className={classes.styleforformreceipttext}>RECEIPT NUMBER</div>
							<div className={classes.styleforformreceipttext}>{oneECB603form.ReceiptNo}</div>
						</Grid>

					</Grid>
				</Grid>
				<Grid className={classes.formposition}>
					<Grid container className={classes.darkborder}>
						<Grid item xs={2} className={classes.styleforformorder}>
							<div className={classes.styleforformreceipttext}>No. of Parts<br /></div>
							<div className={classes.styleforformreceipttext}>
								<input type="number" onChange={(e) => handleOnChange(e)} name="fldNoOfParts" ref={register}
									value={NoOfPartsState}
								/>
							</div>
						</Grid>
						<Grid item xs={8} className={classes.styleforformtitle}>
							<div className={classes.styleforformtitletext}>NOTICE OF VIOLATION & HEARING Form ECB603</div>
						</Grid>
						<Grid item xs={2} className={classes.styleforformorder}>
							<div className={classes.styleforformreceipttext}>QTY. THIS CARTON</div>
							<div className={classes.styleforformreceipttext}>
								<input type="number" onChange={(e) => handleOnChange(e)} name="fldCartonQty" ref={register}
									value={CartonQtyState}
								/>
							</div>
						</Grid>
					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={5} className={classes.styleforformorder}>
							<div className={classes.styleforformordertext}>First Ticket Number</div>
							<div className={classes.styleforformordertext}>
								<input type="text" onChange={(e) => handleOnChange(e)} name="fldTicketNoFrom" ref={register}
									value={TicketNoFromState}
								/>
							</div>
						</Grid>
						<Grid item xs={5} className={classes.styleforformorder}>
							<div className={classes.styleforformordertext}>Last Ticket Number (r/o)</div>
							<div className={classes.styleforformordertext}>
								<input type="text" onChange={(e) => handleOnChange(e)} name="fldTicketNoTo" ref={register}
									value={TicketNoToState}
								/>
							</div>
						</Grid>
						<Grid item xs={2} className={classes.styleforformorder}>
							<div className={classes.styleforformreceipttext}>CARTON NUMBER</div>
							<div className={classes.styleforformreceipttext}>
								<input type="number" onChange={(e) => handleOnChange(e)} name="fldCartonNoTo" ref={register}
									value={CartonNoToState}
								/>
								<br />of<br />
								<input type="number" onChange={(e) => handleOnChange(e)} name="fldCartonNoFrom" ref={register}
									value={CartonNoFromState}
								/>
							</div>
						</Grid>

					</Grid>
					<Grid container className={classes.darkborder}>
						<Grid item xs={10} className={classes.styleforformorder}>
							<div className={classes.styleforformordertext}>OUR ORDER NO.</div>
							<div className={classes.styleforformordertext}>
								<input type="text" onChange={(e) => handleOnChange(e)} name="fldOrderNo" ref={register}
									value={OrderNoState}
								/>
							</div>
							<div className={classes.styleforformordertext}>CUSTOMER ORDER NO.</div>
							<div className={classes.styleforformordertext}>
								<input type="text" onChange={(e) => handleOnChange(e)} name="fldCustomerOrderNo" ref={register}
									value={CustomerOrderNoState}
								/>
							</div>
						</Grid>
						<Grid item xs={2} className={classes.styleforformorder}>
							<div className={classes.styleforformreceipttext}>FACTORY JOB NO.</div>
							<div className={classes.styleforformreceipttext}>
								<input type="text" onChange={(e) => handleOnChange(e)} name="fldFactoryJobNo" ref={register}
									value={FactoryJobNoState}
								/>
							</div>
						</Grid>

					</Grid>
					<Grid item xs={12}>
						{renderActionButtons()}
					</Grid>
				</Grid>

			</form>
		)
	}

};


const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const useStyles = makeStyles({
	root: {
		flexGrow: 1,
		height: "100%",
		paddingLeft: 15,
		paddingRight: 15,

	},
	formposition: {
		marginLeft: 280,
	},
	darkborder: {
		borderWidth: 3,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	styleforlogo: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforsubmit: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforformtitle: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
	},
	styleforformtitletext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "black",
	},
	styleforformorder: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "center",
	},
	styleforformordertext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "center",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 500,
		color: "black",
	},
	styleforformreceipt: {
		verticalAlign: "center",
		borderTopStyle: "none",
	},
	styleforformreceipttext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		display: "inline",
		marginLeft: 10,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
		textAlign: "center",
	},
	button: {
		marginTop: 20,
	},

	formstyle: {
		maxWidth: "100%",
		padding: 0,
	},

	input: {
		display: "block",
		borderRadius: 4,
		border: "1px solid",
		padding: "10px 15px",
		marginBottom: 10,
		fontSize: 14,
		maxWidth: 400,
		minWidth: 200,
	},

	reactselect: {
		maxWidth: 400,
	},

	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginBottom: 13,
		marginTop: 20,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
	},
	buttonSubmit: {
		background: "#0e8c1b",
		color: "white",
		border: "none",
		marginTop: 20,
		padding: 20,
		fontSize: 16,
		fontWeight: 100,
		width: 200,
	}
});
