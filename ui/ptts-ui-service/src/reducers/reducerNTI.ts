﻿
import { Action } from 'redux';
import { KnownActionNTI } from '../actions/actionsNTIprofile';
import { TYPES as NTIconstants } from '../constants/constantsNTIprofile';
import { tUserNTIdetails } from '../model';

export type InterForInitialState = { // all caps is signal to later developers to not change this value
    ST_NTIPROFILE: any,
    ST_NTIMSG: string
};

export const dummyNTIp: tUserNTIdetails = {
    "NTIemployeeID": "1078926",
    "NTIemployeeType": 2,
    "NTIssn": "1620",
    "NTIbadgeNumber": "",
    "NTIlastName": "Farkas",
    "NTIfirstName": "Steven",
    "NTImiddleName": "E",
    "NTItitleID": "10050",
    "NTIsuffix": "00",
    "NTItitle": "COMPUTER SYSTEMS MANAGER",
    "NTIchartNumber": "000",
    "NTIlocationID": "8278068",
    "NTIlocation": "BIT",
    "NTIlocationDesc": "Mgt Info Syst Office",
    "NTIhomePhoneNumber": "718/938-2408",
    "NTIaddress1": "371 Madison Street",
    "NTIaddress2": "Apt 501",
    "NTIcity": "New York",
    "NTIstate": "NY",
    "NTIzip": "10002",
    "NTIbirthDate": "1958-02-07T00",
    "NTIdateLastWorked": "1931-07-15T00",
    "NTIdateNYCHire": "2007-09-24T00",
    "NTIdateDSNYHire": "2007-09-24T00",
    "NTIeffectiveDate": "2007-09-24T00",
    "NTIvacationSchedule": "",
    "NTIphoto": "System.Byte[]",
    "NTIdateProbationEnd": "1931-07-15T00",
    "NTImostRecentDate": "2019-07-31T05",
};


export const INITIAL_STATE: InterForInitialState = { // all caps is signal to later developers to not change this value
    ST_NTIPROFILE: dummyNTIp,
    ST_NTIMSG: ''
};


export type tNTIState = {
    readonly NTIReducer: InterForInitialState;
};


export const NTIReducer = (state: InterForInitialState | undefined = INITIAL_STATE, incomingAction: Action) => {
    const action = incomingAction as KnownActionNTI;
      if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("ntiReducer hit. Looking for " + action.type);

    switch (action.type) {
        case NTIconstants.ACT_NTIPROFILE:
              if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("put this payload in ST_NTIPROFILE ");
              if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(action.payload);
            return {
                ...state, ST_NTIPROFILE: action.payload
            };

        default:
              if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("landed in default of the NTIprofile reducer");
            return { ...state }
    }
};


