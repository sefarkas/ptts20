﻿import axios from "axios";

export const referenceActions = {
    employeeDetail,
    employeeListByLastName,
    employeeByRefNumber,
    districts,
    statementTemplates,
    charges,
    chargesWithSubcharges,
    payrollLocations,
};

function employeeDetail(RefNumber: any) {
    return axios.get(`http://10.155.230.147:62772/crusher/eis-gateway-service/api/eis/personnel/` + RefNumber)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function statementTemplates() {
    return axios.get(`${process.env.REACT_APP_DS249_API}/ReferenceData/StatementTemplateType`)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}


function charges() {
    return axios.get(`${process.env.REACT_APP_DS249_API}/ReferenceData/Charge`)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function chargesWithSubcharges() {
    return axios.get(`${process.env.REACT_APP_DS249_API}/ReferenceData/ChargeSubCharge`)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function employeeListByLastName(LastName: string) {
    return axios.get(`${process.env.REACT_APP_DS249_API}/Employee/GetEmployeeByLastName/` + LastName)
        .then((response: any) => {
          //   if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(response);
            return response;            
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function employeeByRefNumber(RefNumber: string) {
    return axios.get(`${process.env.REACT_APP_DS249_API}/Employee/GetEmployeeByRefNo/` + RefNumber)
        .then((response: any) => {
            return response;
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}


function payrollLocations() {
    return axios.get(`${process.env.REACT_APP_DS249_API}/Employee/GetBusinessUnitAddresses`)
        .then((response: any) => {
             if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(response);
            localStorage.setItem('payrollLocations', JSON.stringify(response));
        },
            (error: { toString: () => any; }) => {
                return error;
            });
}

function districts() {

}

