﻿// DS21Card is a set of 25 NOVs and one row of DS21HDR appearing as the attributes in the bottom of the page
import { Action } from 'redux';
import { KnownActionDS21HDR } from '../actions/DS21';
// import { commonf as PTTSfunctions } from '../components/basicComponents/functions';
import { DS21CardActions, DS21HDRActions, tDS21HDR, tEfDS21HDRiud } from '../model';

type KnownAction = KnownActionDS21HDR;

export type tInterForInitialState = { // all caps is signal to later developers to not change this value
    ST_DS21HDRrows: tDS21HDR[];
    ST_DS21HDRrowsFLTRD: tDS21HDR[];
    ST_DS21HDRmsg: string;
    ST_CARDCOMPLETE: boolean;
};

const todaysDate: Date = new Date();
const inThisInstant: any = new Date().toLocaleDateString();
const julianZero: Date = new Date(0); // PTTSfunctions.Commonfunctions.todaysdate);

// const inThisInstant_yyyyMMdd: any = PTTSfunctions.formatDateYYYYmmDD(new Date());

export const dummyDS21HDRefIUD: tEfDS21HDRiud = {
    Year: 1980,
    IssuingOfficerSignature: "",
    Command: "",
    IssuingOfficerName: "",
    TaxRegNumber: "",
    Agency: "",
    DateCardStarted: todaysDate,
    DateReturned: todaysDate,
    AssignedDistrict: "",
    IssuingOfficerTitle: "",
    OfficerInChargeName: "",
    OfficerInChargeTitle: "",
    DateCardCompleted: todaysDate,
    Remarks: "",
    DS1984Number: "",
    ModifiedBy: "",
    FormStatusID: 0,
};

export const dummyDS21HDR: tDS21HDR = {
    ID: 0,
    Year: 0,
    IssuingOfficerSignature: "",
    Command: "",
    IssuingOfficerName: "",
    TaxRegNumber: "",
    Agency: 0,
    DateCardStarted: inThisInstant,
    DateReturned: inThisInstant,
    AssignedDistrict: "",
    IssuingOfficerTitle: "",
    OfficerInChargeName: "",
    OfficerInChargeTitle: "",
    DateCardCompleted: julianZero,
    Remarks: "",
    DS1984Number: "",
    ModifiedOn: julianZero,
    ModifiedBy: "",
    FormStatusID: 0,
    IssuingOfficerRefNo: "",
    CreatedBy: "",
    CreatedOn: inThisInstant,
    BoxNumberedFrom: 0,
    BoxNumberedTo: 0,
}

export const INITIAL_STATE: tInterForInitialState =  { // all caps is signal to later developers to not change this value
    ST_DS21HDRrows: [dummyDS21HDR],
    ST_DS21HDRrowsFLTRD: [dummyDS21HDR],
	ST_DS21HDRmsg: '',
    ST_CARDCOMPLETE: false,
};

export type tDS21HDRState = {
	readonly DS21HDRReducer: tInterForInitialState;
};


//UPDATE_DS21 = "UPDATE_DS21",
//COMPLETE_DS21 = "COMPLETE_DS21",
//REVERT2INCOMPLETE_DS21 = "REVERT2INCOMPLETE_DS21",
//DS21_CARD_TABLE = "NOVs_For_One_DS21Card",
//MSG = 'SYSTEM_MSG_TO_USER', // some sort of exception

export const DS21HDRReducer = (state: tInterForInitialState | undefined = INITIAL_STATE, incomingAction: Action) => {
	const action = incomingAction as KnownAction;
	  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("DS21HDRReducer hit. Looking for " + action.type);

    let whichID: number = -1;

    switch (action.type) {
        case DS21HDRActions.aDS21HDRROWSFLTRDadd:
            return {
                ...state, ST_DS21HDRrowsFLTRD: action.payload
            }
        case DS21HDRActions.aDS21HDRROWSadd:
            return {
                ...state, ST_DS21HDRrows: action.payload
            }
        case DS21HDRActions.aDS21HDRROWSFLTRDupt:
            //return {
            //    ...state, ST_DS21HDRrowsFLTRD: action.payload
            //}
            console.log("enter DS21HDRActions.aDS21HDRROWSFLTRDupt");
            let i: number = -1;
            for (i = 0; i < state.ST_DS21HDRrowsFLTRD.length; i++) {
                if (state.ST_DS21HDRrowsFLTRD[i].DS1984Number === action.payload.DS1984Number) {
                    console.log("found it in the filtered HDR rows",i);
                    whichID = i;
                }
            }
            if (whichID > -1) state.ST_DS21HDRrowsFLTRD[whichID] =
            { // https://medium.com/hackernoon/redux-patterns-add-edit-remove-objects-in-an-array-6ee70cab2456
                ...state.ST_DS21HDRrowsFLTRD[whichID],
                ...action.payload
            }
            // state.ST_DS21HDRrowsFLTRD.splice(whichID, 1); // second parameter of splice is the number of elements to remove
            return {
                ...state // , ST_DS21HDRrowsFLTRD: action.payload
            }
        case DS21HDRActions.aDS21HDRROWSupt:
            console.log("enter DS21HDRActions.aDS21HDRROWSupt");
            let j: number = -1;
            for (j = 0; j < state.ST_DS21HDRrows.length; j++) {
                if (state.ST_DS21HDRrows[j].DS1984Number === action.payload.DS1984Number) {
                    console.log("found it in the HDR rows", j);
                    whichID = j;
                }
            }
            if(whichID > -1) state.ST_DS21HDRrows[whichID] =
            {
                ...state.ST_DS21HDRrows[whichID],
                ...action.payload
            }
            // state.ST_DS21HDRrows.splice(whichID, 1); // second parameter of splice is the number of elements to remove
            return {
                ...state //, ST_DS21HDRrows: action.payload
			}
        case DS21HDRActions.HDRmsg:
            return { ...state, ST_DS21HDRmsg: "landed in default of the DS21HDR reducer" }
        case DS21HDRActions.REVERT2INCOMPLETE_DS21:
            {
                return { ...state, ST_CARDCOMPLETE: action.payload }
            }
		default:
			  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("landed in default of the DS21HDR reducer");
			return { ...state };

	}
};


