import * as React from 'react';
import { useSelector } from 'react-redux';
import {
    Redirect, Route,

    RouteProps, useHistory
} from 'react-router-dom';
import { PTTSfunctions } from "../components/basicComponents";


interface PrivateRouteProps extends RouteProps {
    // tslint:disable-next-line:no-any
    component: any;
}

const PrivateRoute = (props: PrivateRouteProps) => {
    const { component: Component,  ...rest } = props;
    return (
        <Route
            {...rest}
            render={(routeProps) =>
                localStorage.getItem('userauth') ? (
                    <Component {...routeProps} />
                ) : (
                        <Redirect
                            to={{
                                pathname: '/PTTS/login',
                                state: { from: routeProps.location }
                            }}
                        />
                    )
            }
        />
    );
};

export const PrivateRouteHQ = (props: PrivateRouteProps) => {

    const content = useSelector((state: any) => state.giantReducer);
    const lclIsENFHQ: boolean = PTTSfunctions.ADfunctions.isADinENFHQ(content.ST_ADGROUPS)
    const { component: Component, ...rest } = props;

    let history = useHistory();

    //useEffect(() => {
    //    if (!lclIsENFHQ && !isNullOrUndefined(localStorage.getItem("userid"))) {
    //        alert("Your UID (" + localStorage.getItem("userid") + ") isn't configured for that list.");
    //        history.push('/PTTS/home');
    //    }
    //    else if (isNullOrUndefined(localStorage.getItem("userid"))) {
    //        alert("You need to enter your DSNYAD user ID and password first.");
    //        history.push('/PTTS/logout');
    //    }
    //}, [])

    return (
        <Route
            {...rest}
            render={(routeProps) =>
                localStorage.getItem('userauth') ? (
                    lclIsENFHQ
                        ? <Component {...routeProps} />
                        : (
                            <Redirect
                                to={{
                                    pathname: '/PTTS/home',
                                    state: { from: routeProps.location }
                                }}
                            />
                        )
                ) : (
                        <Redirect
                            to={{
                                pathname: '/PTTS/login',
                                state: { from: routeProps.location }
                            }}
                        />
                    )
            }
        />
    );
};

export default PrivateRoute;