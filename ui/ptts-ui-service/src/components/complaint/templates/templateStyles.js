﻿import { makeStyles } from "@material-ui/styles";

const templateStyles = makeStyles({
	box:
	{
		background: "white",
		minHeight: "276px",
		lineHeight: "2.30em",		
    },
	reactselectsmall: {
		width: 80,
		height: 30,
		top: 1,		
		marginLeft: 0,
		marginRight: 0,
		marginBottom: 0,
		fontSize: 12,
		lineHeight:"20px"
	},
	reactselectmedium: {
		width: 150,
		height: 30,
		top: 1,
		marginLeft: 0,
		marginRight: 0,
		marginBottom: 0,
		fontSize: 12,
		lineHeight: "20px"
	},
	link: {
		color: "green",
		fontSize: 12
	},
	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "inline-block",
		marginBottom: 0,
		marginTop: 5,
		marginLeft: 10,
		fontSize: 12,
		fontWeight: 800,
		color: "#2e2e2d",
	},	
	labelTemplateText: {
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginBottom: 5,
		marginTop: 5,
		fontSize: 12,
		fontWeight: 500,
		color: "#2e2e2d",
	},	
	button: {
		height: "45px",
		width: "50%",
		'&:hover': {
			backgroundColor: "#7bb998"
		}
	},
	grid: {
		background: "white",
		marginTop: "2px;"
	},
	gridCell: {
		paddingTop: "2px",
	},
	gridCellTemplate: {
		paddingTop: "10px",
		paddingLeft: "10px",
		paddingRight: "10px",
		paddingBottom: "10px",
	},		
	error: {
		fontSize: 12,
		color: "#ef1207",
	},
	datepicker: {
		width: 120,
		height: 30,
		marginBottom: 0,		
		padding: "5px 1px",
		marginLeft: "0px",
		marginRight: "0px",
		fontSize: 12,
		borderRadius: 1,
		border: "1px solid",
		borderColor: "#c5c8c5",
		'&:focus': {
			outlineColor: '#2b995f',
		}
	},
	input: {
		width: 80,
		height: 30,
		borderRadius: 1,
		border: "1px solid",
		borderColor: "#c5c8c5",	
		padding: "5px 5px",		
		marginLeft: "0px",
		marginRight: "0px",
		fontSize: 12,
		'&:focus': {
			outlineColor: '#2b995f',
		},
	},
	inputLong: {
		width: 150,
		height: 30,
		borderRadius: 1,
		border: "1px solid",
		borderColor: "#c5c8c5",
		padding: "5px 5px",
		marginLeft: "0px",
		marginRight: "0px",
		fontSize: 12,
		'&:focus': {
			outlineColor: '#2b995f',
		},
	}
});

export default templateStyles;