﻿import { Button, Select, FormControlLabel , FormLabel, FormControl, Checkbox, Box, Link, MenuItem, RadioGroup } from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { Fragment, useEffect, useRef, useState } from "react"; // , useReducer
import { Controller, useForm } from "react-hook-form"; // , FormProvider
import { allServices } from '../../services';
import { PTTSfunctions } from '../basicComponents';
import { history } from "../../configureStore";
import { tSpecialDistrict, tDS21_TicketStatus, tOneNOV, tTicketServiceType, tViolationCode, tDS1704 } from "../../model";
import { dummies } from "../../reducers";
import { Instructions } from "./Instructions";
import { ErrorMessage } from "@hookform/error-message";
import { ErrorMessageContainer } from "../basicComponents/validation";
import { isNullOrUndefined } from "util";
import { allActions } from "../../actions";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../basicComponents/Loader";
import moment from "moment";

type postDataResult<T> = {
	success: boolean;
	errors?: { [P in keyof T]?: string[] };
};

type FormData = {
	DS1984ReceiptNumber: string;
};

interface tProps {
	oneNOVnumber: string;
}

export function EditableNOVdetail(props: tProps) {

	// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(" EditableNOVdetail props:");
	// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props);
	//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(props.match.params.onercpt);
	const dispatch = useDispatch();
	const mountedRef = useRef(true);

	const contentparent: any = useSelector((state: any) => state.DS21CardReducer);

	const [NOVnumberState, setNOVnumberState] = useState(Number.parseInt(props.oneNOVnumber.toString()));

	const [dsAttributesOfOneNOV, setdsAttributesOfOneNOV] = useState<tOneNOV>(contentparent.ST_ATTRIBUTESOFONENOV);

	const defaultValues = {
		Native: "",
		TextField: "",
		Select: "",
		ReactSelect: "",
		Checkbox: false,
		switch: false,
		RadioGroup: ""
	};

	//const [dsPOC, setdsPOC] = !contentparent.ST_DS21_MENU_POC ? [dummyDSspecialDistrict] : contentparent.ST_DS21_MENU_POC;
	//const [dsVCs, setdsVCs] = !contentparent.ST_DS21_MENU_VC ? [dummyViolationCode] : contentparent.ST_DS21_MENU_VC;
	//const [dsTSTs, setdsTSTs] = !contentparent.ST_DS21_MENU_TST ? [dummyServiceType] : contentparent.ST_DS21_MENU_TST;

	const [isLoadingNOV, setLoadingNOV] = React.useState(contentparent.ST_ATTRIBUTESOFONENOV === dummies.dummyNOV);
	const [isLoadingPOC, setLoadingPOC] = React.useState(contentparent.ST_DS21_MENU_POC.length < 2); // list of districts ready
	const [isLoadingVC, setLoadingVC] = React.useState(contentparent.ST_DS21_MENU_VC.length < 2); // list of violation codes ready
	const [isLoadingTSO, setLoadingTSO] = React.useState(true); // constantly calculate the correct status and status available options
	const [isLoadingTST, setLoadingTST] = React.useState(contentparent.ST_DS21_MENU_TST.length < 2); // list of service types ready
	const [isLoading, setLoading] = React.useState(contentparent.ST_ATTRIBUTESOFONENOV === dummies.dummyNOV
		|| isLoadingPOC || isLoadingVC || isLoadingTST
		|| contentparent.ST_ATTRIBUTESOFONENOV.ECBSummonsNumber !== NOVnumberState // this condition clears current NOV details and refreshes the page with a new set of NOV details
	);


	const [dsTSOs, setdsTSOs] = useState([dummies.dummyTicketStatusOption]);

	const [DistOfOccurrenceState, setDistOfOccurrenceState] = useState(contentparent.ST_ATTRIBUTESOFONENOV.DistOfOccurrence);

	const [ViolationCodeState, setViolationCodeState] = useState(contentparent.ST_ATTRIBUTESOFONENOV.Code);

	const [ServiceTypeState, setServiceTypeState] = useState(contentparent.ST_ATTRIBUTESOFONENOV.ViolationType);

	const [TicketStatusState, setTicketStatusState] = useState(contentparent.ST_ATTRIBUTESOFONENOV.TicketStatusID);

	const [CreatedByState, setCreatedByState] = useState(contentparent.ST_ATTRIBUTESOFONENOV.CreatedBy);

	// const [NotarizedState, setNotarizedState] = useState(contentparent.ST_ATTRIBUTESOFONENOV.Notarized);

	// const [VoidedState, setVoidedState] = useState(contentparent.ST_ATTRIBUTESOFONENOV.Voided);

	const [CreatedDateState, setCreatedDateState] = useState(contentparent.ST_ATTRIBUTESOFONENOV.CreatedOn.length > 2
		? contentparent.ST_ATTRIBUTESOFONENOV.CreatedOn
		: PTTSfunctions.Commonfunctions.todaysDatePaded);


	const [DateServedState, setDateServedState] = useState(""); // !dte ? PTTSfunctions.Commonfunctions2.todaysUTCdate() : dte;

	const [DateServedMonthState, setDateServedMonthState] = useState(contentparent.ST_ATTRIBUTESOFONENOV.DateServedMonth);

	const [DateServedDayState, setDateServedDayState] = useState(contentparent.ST_ATTRIBUTESOFONENOV.DateServedDay);

	const [DateServedYearState, setDateServedYearState] = useState(contentparent.ST_ATTRIBUTESOFONENOV.DateServedYear);

	const [oneDS1704, setoneDS1704] = useState(dummies.dummyDS1704);

	const { control } = useForm({ defaultValues });

	let eventCurrentTarget: any = null;

	const { register, handleSubmit, errors, formState } = useForm<FormData>();

	const submitForm = async (data: FormData, e: any) => {

		setdsAttributesOfOneNOV(dummies.dummyNOV);

		if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Submission starting", dsAttributesOfOneNOV);

		const response = allActions.DS21acts.iudNOV(dispatch, dsAttributesOfOneNOV, loggedInUser());
		// response is the new receipt number
		if (!response) {
			 if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("I am having trouble with saving DS21 values.");
			// any error message is in the CustomerOrderNo property in the controller
		}
		else {
			history.push('/PTTS/NOVdetails/' + dsAttributesOfOneNOV.ECBSummonsNumber);
		}
		e.target.reset(); // reset after form submit
	};

	const classes = useStyles();

	function onChangeNotarized(e: any) {
		dsAttributesOfOneNOV.Notarized = !dsAttributesOfOneNOV.Notarized;
		setLoadingNOV(!allActions.DS21acts.iudNOV(dispatch, dsAttributesOfOneNOV, loggedInUser())); // force a screen redraw
    }

	function onChangeVoided(e: any) {
		dsAttributesOfOneNOV.Voided = !dsAttributesOfOneNOV.Voided;
		setLoadingNOV(!allActions.DS21acts.iudNOV(dispatch, dsAttributesOfOneNOV, loggedInUser())); // force a screen redraw
    }

	function handleGetDS1704A(oneNOVnumber: string) {
		history.push('/PTTS/DS1704/' + oneNOVnumber)
	}

	const renderMenuPOC = (whichPOC: string) => {
		if (contentparent.ST_DS21_MENU_POC.length < 2) { return <div>still working ...</div> };

		return contentparent.ST_DS21_MENU_POC.map((oneChoice: tSpecialDistrict) => {
			return <MenuItem
				selected={!whichPOC
				? false
					: whichPOC.length > 0
						? whichPOC.includes(oneChoice.Description)
						: false}
				key={oneChoice.Id.toString()}
				value={oneChoice.Description}>
				{oneChoice.Description}
			</MenuItem>;
		});
	};

    const renderMenuVC = (whichVC: string) => {
		if (contentparent.ST_DS21_MENU_VC.length < 2) { return <div>still working ...</div> };

		return contentparent.ST_DS21_MENU_VC.map((oneChoice: tViolationCode) => {
			return (
				<MenuItem
					selected={!whichVC
						? false
						: whichVC.length > 0
							? whichVC.includes(oneChoice.ViolationCode1)
							: false }
					key={oneChoice.Id.toString()}
					value={oneChoice.ViolationCode1}>
					{oneChoice.ViolationCode1}
				</MenuItem>
			);
		});
	};

	const renderMenuTSO = (currentTSO: number) => {
		return PTTSfunctions.Commonfunctions2.fTSOjsxElement(currentTSO,
			isLoadingTSO, isLoadingNOV,
			dsTSOs, dsAttributesOfOneNOV);
    }

	const renderMenuVoidNotarized = (lclTicketStatusState: number ) => {
		if (isLoadingTSO || isLoadingNOV) { return <div>still working ...</div> };
		return (
			<Fragment>
				<Grid item xs = {12}>
					<FormControlLabel
						key={1}
						disabled={isNullOrUndefined(contentparent.ST_ATTRIBUTESOFONENOV.Voided) ? false : contentparent.ST_ATTRIBUTESOFONENOV.Voided} // if someone voids, they ought not change notarized state afterwards
						control={<Checkbox
							checked={contentparent.ST_ATTRIBUTESOFONENOV.Notarized}
							name="chkNotarized"
							onChange={e => onChangeNotarized(e)}
						/>}
						label={"Notarized"}
					/>
				</Grid>
				<Grid item xs={6}>
					<FormControlLabel
						key={2}
						
						control={<Checkbox
							checked={contentparent.ST_ATTRIBUTESOFONENOV.Voided}
							name="chkVoid"
							onChange={e => onChangeVoided(e)}
						/>}
						label={"Void"}
						/>
				</Grid>
				<Grid item xs={6}>
					{render1704newOrEdit() }
				</Grid>
			</Fragment>
		);
	};

	const renderMenuTST = (whichTST: string) => {
		if (contentparent.ST_DS21_MENU_TST.length < 2) { return <div>still working ...</div> };

		return contentparent.ST_DS21_MENU_TST.map((oneChoice: tTicketServiceType) => {
			return (
				<MenuItem
					selected={!whichTST
						? false
						: whichTST.length > 0
							? whichTST.includes(oneChoice.ViolationType1)
							: false}
					key={oneChoice.Id}
					value={oneChoice.ViolationType1}>
					{oneChoice.ViolationType1}
				</MenuItem>
			)
		});
	};

	const renderEmpty = () => {
		return (<div>&nbsp;</div>);
    }

	const renderPOC = () => {
		return (
			<div className={classes.stylefortopacknowledgementtext}>
				<Controller
					className={classes.styleforDropDownText}
					as={
						<Select
							value={DistOfOccurrenceState}
							onMouseDown={handleDropDownChangePOC}
							name="fldPOC"
							ref={register({
								required: { value: true, message: "You must pick a location where you served the summons." },
								minLength: { value: 3, message: "You have to pick a location where you served the summons." }
							})}
						>
							{renderMenuPOC(DistOfOccurrenceState)}
						</Select>
					}
					name="Select"
					id="fldPOC"
					control={control}
					defaultValue={DistOfOccurrenceState}
				/>
				<div>&nbsp;</div>
				<ErrorMessage
					errors={errors}
					name="fldPOC"
					as={<ErrorMessageContainer />}
				/>
			</div>
			)
    }

	const renderVC = () => {
		if (contentparent.ST_DS21_MENU_VC.length < 2)
			return (<div>Loading ...</div>)
		else
			return (
				<div >
					<Controller
						className={classes.styleforDropDownText}
						as={
							<Select
								value={ViolationCodeState}
								onMouseDown={handleDropDownChangeVC}
								name="fldVC"
								ref={register({
									required: { value: true, message: "You must pick the violation code of the summons." },
									minLength: { value: 3, message: "You must pick the violation code of the summons." }
								})}
							>
								{renderMenuVC(ViolationCodeState)}
							</Select>
						}
						name="Select"
						id="fldVC"
						control={control}
						defaultValue={ViolationCodeState}
					/>
					<div>&nbsp;</div>
					<ErrorMessage
						errors={errors}
						name="fldVC"
						as={<ErrorMessageContainer />}
					/>
				</div>
		)
	}

	const renderTSO = () => { // ticket status options
		if (isLoadingTSO || isLoadingNOV)
			return (<div>Loading ...</div>)
		else
			return (
				<div >
					<FormControl component="fieldset" >
						<FormLabel component="legend">Ticket Location</FormLabel>
						<RadioGroup aria-label="Ticket Location" name="optTicketLocation"
							value={TicketStatusState} onChange={handleOptionChangeTL}>
							{renderMenuTSO(TicketStatusState)}
						</RadioGroup>
					</FormControl>
				</div>
			)
	}

	const renderVoidNotarized = () => {
		if (isLoadingTSO || isLoadingNOV)
			return (<div>Loading ...</div>)
		else
			return (
				<div >
					<FormControl component="fieldset" >
						<FormLabel component="legend">Ticket Status</FormLabel>
						<RadioGroup aria-label="Ticket Status" name="optTicketStatus"
							value={TicketStatusState} >
							{renderMenuVoidNotarized(TicketStatusState)}
						</RadioGroup>
					</FormControl>
				</div>
			)
	}

	const renderTST = () => {
		if (contentparent.ST_DS21_MENU_TST.length < 2)
			return (<div>Loading ...</div>)
		else
			return (
				<div >
					<Controller
						className={classes.styleforDropDownText}
						as={
							<Select
								value={ServiceTypeState}
								onMouseDown={handleDropDownChangeTST}
								name="fldTST"
								ref={register({
									required: { value: true, message: "You must pick the violation code of the summons." },
									minLength: { value: 3, message: "You must pick the violation code of the summons." }
								})}
							>
								{renderMenuTST(ServiceTypeState)}
							</Select>
						}
						name="Select"
						id="fldTST"
						control={control}
						defaultValue={ServiceTypeState}
					/>
					<div>&nbsp;</div>
					<ErrorMessage
						errors={errors}
						name="fldTST"
						as={<ErrorMessageContainer />}
					/>
				</div>
			)
	}

	const render1704newOrEdit = () => {
		if (dsAttributesOfOneNOV.Voided) {
			return (
					<div><Link href="#"
					onClick={() => handleGetDS1704A(NOVnumberState.toString())}
					>
					{!oneDS1704 || oneDS1704.ECBSummonsNumber !== NOVnumberState.toString() ? `create its DS1704A` : `view related DS1704A`}
					</Link>
					</div>				
				)
        }
    }

	useEffect(() => {
		console.log("initial useEffect");
		console.log("ViolationCodeState ", ViolationCodeState);
		console.log("DistOfOccurrenceState ", DistOfOccurrenceState);
		console.log("ServiceTypeState ", ServiceTypeState); // ViolationType
		console.log("dsAttributesOfOneNOV", dsAttributesOfOneNOV);
		console.log("contentparent.ST_ATTRIBUTESOFONENOV", contentparent.ST_ATTRIBUTESOFONENOV);
		console.log(" ------ ");

		if (contentparent.ST_ATTRIBUTESOFONENOV === dummies.dummyNOV
			|| contentparent.ST_ATTRIBUTESOFONENOV.ECBSummonsNumber !== NOVnumberState )
		{
			(async () => {
				let rdata: tOneNOV = dummies.dummyNOV;
				try {
					const response1 = await allServices.PTTSgetServices.oneNOV(NOVnumberState); // axios always returns an object with a "data" child
					rdata = response1.data;
					let rBoolean: boolean = false;
					try {
						rBoolean = allActions.DS21acts.iudNOV(dispatch, rdata, loggedInUser())
						// rBoolean = await allServices.PTTSiudServices.iudNOV(dsAttributesOfOneNOV, loggedInUser()); // axios always returns an object with a "data" child

						const dte: Date = new Date(rdata.DateServedYear,
							rdata.DateServedMonth - 1,
							rdata.DateServedDay);
						const strDte: string = moment(dte.toUTCString()).format('YYYY-MM-DD');
						console.log("allServices.PTTSgetServices.oneNOV strDte: ", strDte);
						setDateServedState(strDte);
						setdsAttributesOfOneNOV(rdata); // replace NOV with details with newly retrieved
						setLoadingTSO(true); // replace location, notarized, and void based on newly retrieved NOV details

					}
					catch (error) {
						alert(error.toString());
						rBoolean = false;
					}
					if (rBoolean) {
						console.log("successful NOV update");
					}
					setLoadingNOV(!rBoolean);
				}
				catch {
					rdata = dummies.dummyNOV;
					setLoadingNOV(true);
				}
			})();
		}

		if (contentparent.ST_ATTRIBUTESOFONENOV.ECBSummonsNumber !== dsAttributesOfOneNOV.ECBSummonsNumber)
		{

			const dte: Date = new Date(contentparent.ST_ATTRIBUTESOFONENOV.DateServedYear,
			contentparent.ST_ATTRIBUTESOFONENOV.DateServedMonth - 1,
			contentparent.ST_ATTRIBUTESOFONENOV.DateServedDay);
			const strDte: string = moment(dte).format('YYYY-MM-DD');
			console.log("strDte after NOV# changes: ", strDte);
			setDateServedState(strDte);

			setdsAttributesOfOneNOV(contentparent.ST_ATTRIBUTESOFONENOV);
		}

		if (contentparent.ST_DS21_MENU_POC.length < 2)(async () => {
			//setLoadingPOC(true);
			const responsePOC = await allServices.PTTSgetServices.refSpecialDistricts(); // axios always returns an object with a "data" child
			const rdataPOC: tSpecialDistrict[] = responsePOC.data;
			if (!rdataPOC) {
				setLoadingPOC(true);
			}
			else {
				const rdataPOCsorted: tSpecialDistrict[] = rdataPOC.sort(function (a, b) {
					var nameA = !a.Description ? "" : a.Description.toUpperCase(); // ignore upper and lowercase
					var nameB = !b.Description ? "" : b.Description.toUpperCase(); // ignore upper and lowercase
					if (nameA < nameB) {
						return -1;
					}
					if (nameA > nameB) {
						return 1;
					}
					// names must be equal
					return 0;
				}); // ASC
				setLoadingPOC(!allActions.DS21acts.MenuChoicesPOC(rdataPOCsorted, dispatch));
				//setdsPOC(rdataPOCsorted);
				//setLoadingPOC(false);
			}
		})();

		if (contentparent.ST_DS21_MENU_VC.length < 2)(async () => {
			const responseVC = await allServices.PTTSgetServices.refViolationCodes(); // axios always returns an object with a "data" child
			const rdataVC: tViolationCode[] = responseVC.data;
			if (!rdataVC) {
				setLoadingVC(true);
			}
			else {
				let ind: number = 0;
				for (ind = 0; ind < rdataVC.length; ind++)
					{ rdataVC[ind].Id = ind + 1; }
				const rdataVCsorted: tViolationCode[] = rdataVC.sort(function (a, b) {
					var nameA = !a.ViolationCode1 ? "" : a.ViolationCode1.toUpperCase(); // ignore upper and lowercase
					var nameB = !b.ViolationCode1 ? "" : b.ViolationCode1.toUpperCase(); // ignore upper and lowercase
					if (nameA < nameB) {
						return -1;
					}
					if (nameA > nameB) {
						return 1;
					}
					// names must be equal
					return 0;
				}); // ASC
				setLoadingVC(!allActions.DS21acts.MenuChoicesVC(rdataVCsorted, dispatch));
				//setdsVCs(rdataVCsorted);
				//setLoadingVC(false);
			}
		})();

		(async () => {
			// setLoadingTSO(true);
			const responseTSO = await allServices.PTTSgetServices.refTicketStatusOptions(); // axios always returns an object with a "data" child
			const rdataTSO: tDS21_TicketStatus[] = responseTSO.data;
			if (!rdataTSO) {
				setLoadingTSO(true);
			}
			else {
				let ind: number = 0;
				for (ind = 0; ind < rdataTSO.length; ind++) { rdataTSO[ind].Id = ind + 1; }
				const rdataTSOsorted: tDS21_TicketStatus[] = rdataTSO.sort(function (a, b) {
					var nameA = !a.ID; 
					var nameB = !b.ID; 
					if (nameA < nameB) {
						return -1;
					}
					if (nameA > nameB) {
						return 1;
					}
					// names must be equal
					return 0;
				}); // ASC
				setdsTSOs(rdataTSOsorted);
				setLoadingTSO(false);
			}
		})();

		if (contentparent.ST_DS21_MENU_TST.length < 2)(async () => {
				const responseTST = await allServices.PTTSgetServices.refViolationTypes(); // axios always returns an object with a "data" child
				if (!responseTST) {
					setLoadingTST(true);
				}
				else {
					const rdataTST: tTicketServiceType[] = responseTST.data;
					if (!rdataTST) {
						setLoadingTST(true);
					}
					else {
						let ind: number = 0;
						for (ind = 0; ind < rdataTST.length; ind++) { rdataTST[ind].Id = ind + 1; }
						const rdataTSTsorted: tTicketServiceType[] = rdataTST.sort(function (a, b) {
							var nameA = !a.ViolationType1 ? "" : a.ViolationType1.toUpperCase(); // ignore upper and lowercase
							var nameB = !b.ViolationType1 ? "" : b.ViolationType1.toUpperCase(); // ignore upper and lowercase
							if (nameA < nameB) {
								return -1;
							}
							if (nameA > nameB) {
								return 1;
							}
							// names must be equal
							return 0;
						}); // ASC
						setLoadingTST(!allActions.DS21acts.MenuChoicesTST(rdataTSTsorted, dispatch));
						//setdsTSTs(rdataTSTsorted);
						//setLoadingTST(false);
					}
				}

			})();

		(async () => {
			let rdata1704: tDS1704 = dummies.dummyDS1704;
			try {
				const response1 = await allServices.PTTSgetServices.oneDS1704(NOVnumberState); // axios always returns an object with a "data" child
				rdata1704 = response1.data;
			}
			catch {
				rdata1704 = dummies.dummyDS1704;
			}
			setoneDS1704(rdata1704);
		})();

		return () => {
			mountedRef.current = false
		}

	}, []);

	useEffect(() => {
		if (dsAttributesOfOneNOV === dummies.dummyNOV) setdsAttributesOfOneNOV(contentparent.ST_ATTRIBUTESOFONENOV);

		console.log(" useEffect for isLoading");
		console.log("isLoadingNOV", isLoadingNOV);
		console.log("isLoadingPOC ", isLoadingPOC);
		console.log("isLoadingVC ", isLoadingVC);
		console.log("isLoadingTST ", isLoadingTST);
		console.log("isLoadingTSO", isLoadingTSO);
		console.log("isLoading", isLoadingNOV || isLoadingPOC || isLoadingVC || isLoadingTST || isLoadingTSO);
		console.log("ViolationCodeState ", ViolationCodeState);
		console.log("DistOfOccurrenceState ", DistOfOccurrenceState);
		console.log("ServiceTypeState ", ServiceTypeState); // ViolationType
		console.log("dsAttributesOfOneNOV", dsAttributesOfOneNOV);
		console.log("contentparent.ST_ATTRIBUTESOFONENOV", contentparent.ST_ATTRIBUTESOFONENOV);
		console.log(" ------ ");

		//if (contentparent.ST_DS21_MENU_VC.length > 2 && ViolationCodeState.length > 0) {
		//	setViolationCodeState(!contentparent.ST_ATTRIBUTESOFONENOV.Code
		//		? ""
		//		: ViolationCodeState !== contentparent.ST_ATTRIBUTESOFONENOV.Code
		//			? ViolationCodeState
		//			: contentparent.ST_ATTRIBUTESOFONENOV.Code)
		//};

		//if (contentparent.ST_DS21_MENU_POC.length > 2 && DistOfOccurrenceState.length > 0) {
		//	setDistOfOccurrenceState(!contentparent.ST_ATTRIBUTESOFONENOV.DistOfOccurrence
		//	? ""
		//	: DistOfOccurrenceState !== contentparent.ST_ATTRIBUTESOFONENOV.DistOfOccurrence
		//		? DistOfOccurrenceState
		//		: contentparent.ST_ATTRIBUTESOFONENOV.DistOfOccurrence);
  //      }

		//if (contentparent.ST_DS21_MENU_TST.length > 2 && ServiceTypeState.length > 0) {
		//	setServiceTypeState(!contentparent.ST_ATTRIBUTESOFONENOV.ViolationType
		//		? ""
		//		: ServiceTypeState !== contentparent.ST_ATTRIBUTESOFONENOV.ViolationType
		//			? ServiceTypeState
		//			: contentparent.ST_ATTRIBUTESOFONENOV.ViolationType);
		//}

		if (!isLoadingNOV && !isLoadingTSO) // needed to update the screen without a full redraw
		{
			setTicketStatusState(
				(contentparent.ST_ATTRIBUTESOFONENOV.Notarized || contentparent.ST_ATTRIBUTESOFONENOV.Voided)
				&& (contentparent.ST_ATTRIBUTESOFONENOV.TicketStatusID === 2 || contentparent.ST_ATTRIBUTESOFONENOV.TicketStatusID === 13) 
				? 0 // assume returned to HQ
					: TicketStatusState !== contentparent.ST_ATTRIBUTESOFONENOV.TicketStatusID
						? contentparent.ST_ATTRIBUTESOFONENOV.TicketStatusID
						: TicketStatusState);
        }

		// if (!isLoadingNOV && !!dsAttributesOfOneNOV) {
		//	setNotarizedState(contentparent.ST_ATTRIBUTESOFONENOV.Notarized);
		//	setVoidedState(contentparent.ST_ATTRIBUTESOFONENOV.Voided);
		//	setDateServedMonthState(contentparent.ST_ATTRIBUTESOFONENOV.DateServedMonth);
		//	setDateServedDayState(contentparent.ST_ATTRIBUTESOFONENOV.DateServedDay);
		//	setDateServedYearState(contentparent.ST_ATTRIBUTESOFONENOV.DateServedYear);
		//	setDateServedState(new Date(contentparent.ST_ATTRIBUTESOFONENOV.DateServedYear,
		//		contentparent.ST_ATTRIBUTESOFONENOV.DateServedMonth,
		//		contentparent.ST_ATTRIBUTESOFONENOV.DateServedDay,
		//		0, 0, 0, 0).toUTCString())
		//	}
		// if (!(isLoadingNOV && isLoadingPOC && isLoadingVC && isLoadingTST && isLoadingTSO)) {
		setLoading(isLoadingNOV || isLoadingPOC || isLoadingVC || isLoadingTST || isLoadingTSO);
		// any item still loading causes isLoading to by true, e.g., true || false || false || false || false equals true
		// }
	}, [isLoadingNOV, isLoadingPOC, isLoadingVC, isLoadingTST, isLoadingTSO,])

	useEffect(() => {

		(async () => {
			// let rdata: tOneNOV = dummies.dummyNOV;
			try {
				// const response1 = await allServices.PTTSgetServices.oneNOV(NOVnumberState); // axios always returns an object with a "data" child
				// rdata = response1.data;
				let rBoolean: boolean = false;
				try {
					// rBoolean = allActions.DS21acts.iudNOV(dispatch, rdata, loggedInUser())
					if (dsAttributesOfOneNOV !== dummies.dummyNOV) {
						rBoolean = await allServices.PTTSiudServices.iudNOV(dsAttributesOfOneNOV, loggedInUser()); // axios always returns an object with a "data" child
					}
				}
				catch (error1) {
					alert(error1.toString());
					rBoolean = false;
				}
				if (rBoolean) {

					const dte: Date = new Date(dsAttributesOfOneNOV.DateServedYear,
						dsAttributesOfOneNOV.DateServedMonth - 1,
						dsAttributesOfOneNOV.DateServedDay);
					const strDte: string = moment(dte).format('YYYY-MM-DD');
					console.log("strDte for dsAttributesOfOneNOV change: ", strDte);
					setDateServedState(strDte);

					console.log("successful NOV update to match DB with attributes of dsAttributesOfOneNOV");
				}
				// setLoadingNOV(!rBoolean);
			}
			catch (error2)  {
				alert(error2.toString());
				// rdata = dummies.dummyNOV;
				// setLoadingNOV(true);
			}
		})();
	}, [dsAttributesOfOneNOV.DateServedYear,
		dsAttributesOfOneNOV.DateServedMonth,
		dsAttributesOfOneNOV.DateServedDay,
		dsAttributesOfOneNOV.Notarized,
		dsAttributesOfOneNOV.Voided,
		dsAttributesOfOneNOV.Code,
		dsAttributesOfOneNOV.DistOfOccurrence,
		dsAttributesOfOneNOV.ViolationType
	])
	// const iud = async () => {
	//	let rBoolean: boolean = false;
	//	try {
	//		rBoolean = await allServices.PTTSiudServices.iudNOV(dsAttributesOfOneNOV, loggedInUser()); // axios always returns an object with a "data" child
	//	}
	//	catch(error) {
	//		rBoolean = false;
	//		console.log(error.toString());
	//	}
	//	if (rBoolean) {
	//		console.log("successful NOV update");
	//	}
 //   }

	// useEffect(() => {
	//	if ((isLoadingNOV) && dsAttributesOfOneNOV.ECBSummonsNumber.length > 3) { // block dummy from being sent
	//		console.log("update DB row for ", dsAttributesOfOneNOV.ECBSummonsNumber);
	//		(async () => {
	//			await iud();
	//			setLoadingNOV(false);
	//		})();

	//		return () => {
	//			mountedRef.current = false
	//		}
	//	}

	// },
	//	[isLoadingNOV])

	useEffect(() => {
		if ((isLoadingTSO) && contentparent.ST_ATTRIBUTESOFONENOV.ECBSummonsNumber.length > 3) { // block dummy from being sent

			(async () => {
				// await iud();
				let rBoolean: boolean = false;
				try {
					rBoolean = allActions.DS21acts.iudNOV(dispatch, dsAttributesOfOneNOV, loggedInUser())
					// rBoolean = await allServices.PTTSiudServices.iudNOV(dsAttributesOfOneNOV, loggedInUser()); // axios always returns an object with a "data" child
				}
				catch (error) {
					rBoolean = false;
					console.log(error.toString());
				}
				if (rBoolean) {
					console.log("successful NOV update");
				}
				setLoadingTSO(!rBoolean);
			})();

			return () => {
				mountedRef.current = false
			}
		}

	},
		[isLoadingTSO])

	const handleDropDownChangePOC = async (event: any) => {
		let newSelection: string = "Pick one";
		if (PTTSfunctions.Commonfunctions2.isNotDefined(event.target.innerText)) {
			eventCurrentTarget = event.target;
			alert("DropDown event object is marked 'notDefined'");
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(eventCurrentTarget);
		}
		else {
			newSelection = event.target.innerText;
			// await setDistOfOccurrenceState(newSelection);
			//  if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Borough is" ,newSelection);
			dsAttributesOfOneNOV.DistOfOccurrence = newSelection;
			setLoadingNOV(!allActions.DS21acts.iudNOV(dispatch, dsAttributesOfOneNOV, loggedInUser()))
		}
		return newSelection;
	};

	const handleDropDownChangeVC = async (event: any) => {
		let newSelectionVC: string = "Pick one";
		if (PTTSfunctions.Commonfunctions2.isNotDefined(event.target.innerText)) {
			eventCurrentTarget = event.target;
			alert("DropDown event object is marked 'notDefined'");
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(eventCurrentTarget);
		}
		else {
			newSelectionVC = event.target.innerText;
			// await setViolationCodeState(newSelectionVC);
			dsAttributesOfOneNOV.Code = newSelectionVC;
			setLoadingNOV(!allActions.DS21acts.iudNOV(dispatch, dsAttributesOfOneNOV, loggedInUser()))
		}
		return newSelectionVC;
	};

	const handleOptionChangeTL = async (event: any) => {
		let newSelectionTL: number = -1;
		if (PTTSfunctions.Commonfunctions2.isNotDefined(event.target.value)) {
			eventCurrentTarget = event.target;
			alert("OptionChange event object is marked 'notDefined'");
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(eventCurrentTarget);
		}
		else {
			newSelectionTL = Number.parseInt(event.target.value);
			setLoadingTSO(true);
			// await setTicketStatusState(newSelectionTL);
			dsAttributesOfOneNOV.TicketStatusID = newSelectionTL;
			setLoadingNOV(!allActions.DS21acts.iudNOV(dispatch, dsAttributesOfOneNOV, loggedInUser()))
		}
		return newSelectionTL;
	};

	const handleDropDownChangeTST = async (event: any) => {
		let newSelectionTST: string = "Pick one";
		if (PTTSfunctions.Commonfunctions2.isNotDefined(event.target.innerText)) {
			eventCurrentTarget = event.target;
			alert("DropDown event object is marked 'notDefined'");
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(eventCurrentTarget);
		}
		else {
			newSelectionTST = event.target.innerText;
			// await setServiceTypeState(newSelectionTST);
			dsAttributesOfOneNOV.ViolationType = newSelectionTST;
			setLoadingNOV(!allActions.DS21acts.iudNOV(dispatch, dsAttributesOfOneNOV, loggedInUser()))
		}
		return newSelectionTST;
	};

	function handleGetDS1984() {
		// alert("history.push('/PTTS/existingDS1984/" + DS1984rcpt + "'");
		history.push('/PTTS/existingDS1984/' + dsAttributesOfOneNOV.DS1984Number)
	}

	function backToDS21list(rcpt: string) {
		history.push('/PTTS/DS21Card/' + rcpt)
	}

	function loggedInUser(): string {
		return (localStorage.getItem('userid') + "").toString(); // to avoid possibly null error
		//const j: any = JSON.parse(curUser);
		//return j.data.userPreferences.userName.toString();
	}

	const handleOnChange = (e: any) => {
		e.preventDefault();
		let bNeedDbUpdate: boolean = false;
		switch (e.target.name) {
			case "fldDateServed":
				//const aDte: string = new Date(e.target.value).toUTCString(); // 2021-01-08
				// setDateServedMonthState(parseInt(aDte.toString().substr(5, 2)));
				// setDateServedDayState(parseInt(aDte.toString().substr(8, 2)));
				// setDateServedYearState(parseInt(aDte.toString().substr(0, 4)));
				// setDateServedState(aDte);

				try {
					dsAttributesOfOneNOV.DateServedMonth = new Date(e.target.value).getUTCMonth() + 1; // parseInt(aDte.toString().substr(5, 2));
					dsAttributesOfOneNOV.DateServedDay = new Date(e.target.value).getUTCDate(); //parseInt(aDte.toString().substr(8, 2));
					dsAttributesOfOneNOV.DateServedYear = new Date(e.target.value).getUTCFullYear(); //parseInt(aDte.toString().substr(0, 4));

					const dte: Date = new Date(dsAttributesOfOneNOV.DateServedYear,
						dsAttributesOfOneNOV.DateServedMonth - 1,
						dsAttributesOfOneNOV.DateServedDay);
					const strDte: string = moment(dte).format('YYYY-MM-DD');
					console.log("newly stored strDte: ", strDte);
					setDateServedState(strDte);

					bNeedDbUpdate = true;
				}
				catch (error) {
					alert(error.toString());
				}

				break;
			// case "fldDateServedMonth": setDateServedMonthState(e.target.value); break;
			// case "fldDateServedDay": setDateServedDayState(e.target.value); break;
			// case "fldDateServedYear": setDateServedYearState(e.target.value); break;

			default:
				// code block
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.name);
				if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(e.target.value);
		}

		if (CreatedByState.length < 2) {
			if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("created by ", loggedInUser);
			//setCreatedByState(loggedInUser);
			dsAttributesOfOneNOV.CreatedBy = loggedInUser();
		}

		if (dsAttributesOfOneNOV.ModifiedOn.valueOf !== PTTSfunctions.Commonfunctions.todaysdate) {
			//setCreatedDateState(PTTSfunctions.Commonfunctions.todaysdate);
			dsAttributesOfOneNOV.ModifiedOn = PTTSfunctions.Commonfunctions.todaysdate();
		}

		if (bNeedDbUpdate) { // from switch statement above
			setLoadingNOV(!allActions.DS21acts.iudNOV(dispatch, dsAttributesOfOneNOV, loggedInUser())); // trigger useEffect to run the IUD for NOVs
        }

	}

	if (isLoading ) {
		return (<Loader />);
	}
	else {
		// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log("Details for ECB Summons Number ", NOVnumberState);
		// if (process.env.REACT_APP_CONSOLE_LOG_ON === 'TRUE') console.log(dsAttributesOfOneNOV);
		console.log("!dsAttributesOfOneNOV", !dsAttributesOfOneNOV);
		console.log("dsAttributesOfOneNOV.Notarized || dsAttributesOfOneNOV.Voided", dsAttributesOfOneNOV.Notarized || dsAttributesOfOneNOV.Voided);
		console.log("DateServedState: ", DateServedState );
		return (
			<form onSubmit={handleSubmit(submitForm)}>
				<Grid container className={classes.styleForBottomContainer}>
					<Grid className={ classes.stylefortopdistributionrowtextwide} item xs={12}>Ticket Details</Grid>
					<Instructions/>
					<Grid item xs={12}>
						<Button href="#" onClick={() => backToDS21list(dsAttributesOfOneNOV.DS1984Number)}>
						Back to DS21 Card containing {!NOVnumberState ? "" : NOVnumberState}
						</Button>
					</Grid>
					<Grid item xs={12}>Ticket Number {!NOVnumberState ? "" : NOVnumberState}</Grid>
					<Grid item xs={4}>DS 1984 Receipt No:</Grid>
					<Grid item xs={4}>{!dsAttributesOfOneNOV ? "looking ..." : <Link href="#" onClick={() => handleGetDS1984()}>{dsAttributesOfOneNOV.DS1984Number}</Link>}</Grid>
					<Grid item xs={4}><Link href="#">DS156 including {NOVnumberState}</Link></Grid>
					<Grid item xs={12}>&nbsp;</Grid>
					<Grid item xs={12}>
						<Box display="flex"
							flexWrap="nowrap"
							border={1}>
								<Grid item xs={2} className={classes.styleForGroup}>ECB Summons<br />Number</Grid>
								<Grid item xs={2}><Box borderLeft={1}>Code<br />&nbsp;<br />&nbsp;</Box></Grid>
								<Grid item xs={3}><Box borderLeft={1}>Ticket<br />Service<br />Types</Box></Grid>
								<Grid item xs={3}><Box borderLeft={1}>Date Served<br />&nbsp;<br />&nbsp;</Box></Grid>
								<Grid item xs={2}><Box borderLeft={1}>Dist. of<br />Occurrence<br />&nbsp;</Box></Grid>
						</Box>
					</Grid>
					<Grid container>
						<Grid item xs={2} className={classes.styleforNOVattributes}>{NOVnumberState}</Grid>
						<Grid item xs={1}>
							<label className={classes.styleforNOVattributes}>{contentparent.ST_ATTRIBUTESOFONENOV.Code}</label>
						</Grid>
						<Grid item xs={1}>
							{!dsAttributesOfOneNOV ? renderEmpty() : dsAttributesOfOneNOV.Notarized || dsAttributesOfOneNOV.Voided ? renderEmpty() : renderVC()}
						</Grid>
						<Grid item xs={1}>
							<label className={classes.styleforNOVattributes}>{!dsAttributesOfOneNOV ? renderEmpty() : contentparent.ST_ATTRIBUTESOFONENOV.ViolationType}</label>
						</Grid>
						<Grid item xs={2}>
							{!dsAttributesOfOneNOV ? renderEmpty() : dsAttributesOfOneNOV.Notarized || dsAttributesOfOneNOV.Voided ? renderEmpty() : renderTST()}
						</Grid>
						<Grid item xs={3} className={classes.styleforNOVattributes}>
							{
								!dsAttributesOfOneNOV
								? <input
									placeholder="Pick a date"
									defaultValue={moment().format('l')}
									className={classes.datepicker}
									max={PTTSfunctions.Commonfunctions.todaysDatePaded()}
									// value={DateServedState.format('l')}
									type="date"
									onChange={(e) => handleOnChange(e)}
									name="fldDateServed" ref={register}
								/>
									: dsAttributesOfOneNOV.Notarized || dsAttributesOfOneNOV.Voided
										? DateServedState
										: DateServedState.length < 3
											? <input
												placeholder="Pick a date"

												className={classes.datepicker}
												max={PTTSfunctions.Commonfunctions.todaysDatePaded()}

												type="date"
												onChange={(e) => handleOnChange(e)}
												name="fldDateServed" ref={register}
											/>
											: <input
												placeholder="Pick another date"
												defaultValue={DateServedState} // must be YYYY-MM-DD
												className={classes.datepicker}
												max={PTTSfunctions.Commonfunctions.todaysDatePaded()}

												type="date"
												onChange={(e) => handleOnChange(e)}
												name="fldDateServed" ref={register}
											/>
						}
						</Grid>
						<Grid item xs={1}>
							<label className={classes.styleforNOVattributes}>{contentparent.ST_ATTRIBUTESOFONENOV.DistOfOccurrence}</label>
						</Grid>
						<Grid item xs={1}>
							{!dsAttributesOfOneNOV
								? renderEmpty()
								: dsAttributesOfOneNOV.Notarized || dsAttributesOfOneNOV.Voided
								? renderEmpty()
								: renderPOC()}
							</Grid>
					</Grid>
					<Grid container>
						<Grid item xs={6}>
							{!dsAttributesOfOneNOV ? renderEmpty() : renderTSO() }
							</Grid>
						<Grid item xs={6}>
							{!dsAttributesOfOneNOV ? renderEmpty() : renderVoidNotarized()}
							</Grid>
					</Grid>
				</Grid>
			</form>
		)
	}

};


const StyledButton = withStyles({
	root: {
		background: '#3d3d3e',
		borderRadius: 3,
		border: 0,
		color: 'white',
		padding: '0 5px',
		marginRight: 5,
		marginTop: 0
	},
	label: {
		textTransform: 'capitalize',
		color: 'white',
		fontSize: 12
	},
})(Button);

const useStyles = makeStyles({
	root: {
		flexGrow: 1,
		height: "100%",
		paddingLeft: 15,
		paddingRight: 15,

	},
	formposition: {
		marginLeft: 280,
	},
	darkborder: {
		borderWidth: 3,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderTop: {
		borderTopWidth: 2,
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	thinborderLeft: {
		borderLeftWidth: 2,
		borderBottomWidth: 0,
		borderRightWidth: 0,
		borderTopWidth: 0,
		borderStyle: "solid",
		paddingTop: 0,
		paddingBottom: 0,
		marginTop: 0,
		marginBottom: 0,
	},
	styleforlogo: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforsubmit: {
		textAlign: "center",
		marginTop: "5px",
		marginBottom: "5px",
	},
	styleforformtitle: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
	},
	styleforformtitletext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "black",
		minWidth: 500,
	},
	styleforformorder: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "center",
	},
	styleforformordertext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "center",
		display: "block",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 500,
		color: "black",
	},
	styleforticketrange: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	styleforticketrangetext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
	},
	stylefortopacknowledgement: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
	},
	stylefortopacknowledgementtext: {
		paddingBottom: 13,
		paddingTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 320,
		minWidth: 130
	},
	styleforNOVattributes: {
		paddingBottom: 13,
		paddingTop: 20,
		textAlign: "left",
		display: "block",
		marginLeft: 10,
		fontSize: 14,
		fontWeight: 600,
		color: "black",
		minWidth: 20,
	},
	stylefortopdistributionrow: {
		borderLeftStyle: "solid",
		verticalAlign: "center",
		textAlign: "left",
		maxWidth: 300,
		wordWrap: "break-word",
	},
	stylefortopdistributionrowtext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		maxWidth: 150,
		minWidth: 80,
		wordWrap: "break-word",
	},
	stylefortopdistributionrowtextwide: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 24,
		fontWeight: "bold",
		whiteSpace: "normal",
		fontStyle: "normal",
		color: "black",
		minWidth: 300,
		maxWidth: 600,
	},
	stylefortopdistributionrowtextwideRed: {
		marginBottom: 13,
		paddingTop: 2,
		paddingBottom: 2,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontFamily: "Arial",
		fontSize: 12,
		color: "red",
		fontWeight: "normal",
		whiteSpace: "normal",
		fontStyle: "normal",
		minWidth: 300,
		maxWidth: 600,
	},
	styleforformreceipt: {
		verticalAlign: "center",
		borderTopStyle: "none",
	},
	styleforformreceipttext: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		display: "inline",
		marginLeft: 10,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
		textAlign: "center",
	},
	styleforformreceipttextred: {
		marginBottom: 13,
		marginTop: 20,
		lineHeight: 2,
		textAlign: "left",
		display: "inline",
		marginLeft: 10,
		fontSize: 18,
		fontWeight: 700,
		color: "red",
		minWidth: 100,
		width: 200,
		maxWidth: 250,
	},
	button: {
		marginTop: 20,
	},

	formstyle: {
		maxWidth: "100%",
		padding: 0,
	},

	input: {
		display: "block",
		borderRadius: 4,
		border: "1px solid",
		padding: "10px 15px",
		marginBottom: 10,
		fontSize: 14,
		maxWidth: 400,
		minWidth: 200,
	},

	reactselect: {
		maxWidth: 400,
	},

	label: {
		lineHeight: 2,
		textAlign: "left",
		display: "block",
		marginBottom: 13,
		marginTop: 20,
		fontSize: 14,
		fontWeight: 500,
		color: "black",
	},
	buttonSubmit: {
		background: "#0e8c1b",
		color: "white",
		border: "none",
		marginTop: 20,
		padding: 20,
		fontSize: 16,
		fontWeight: 100,
		width: 200,
	},
	styleForBottomContainer: {
		wrap: 'nowrap',
		maxWidth: "85%",
	},
	styleforWorkFlowText: {
		width: "100%",
	},
	styleforWorkFlowTextMMDD: {
		width: 30,
	},
	styleForBottomButtons: {
		textAlign: "left",
		border: 2,
	},
	styleForBottomButtonsLeft: {
		textAlign: "left",
		paddingLeft: 5,
	}, styleForBottomButtonsRight: {
		textAlign: "right",
		borderLeft: 2,
		paddingRight: 5,
	}, styleForBottomButtonsCenter: {
		textAlign: "center",
		border: 2,
		paddingRight: 5,
		paddingLeft: 5,
	},
	styleForDivider: {
		paddingTop: 100,
	},
	styleForGroup: {
		border: 2,
		borderColor: 'black',
	},
	styleForCheckBoxCompanion: {
		alignContent: 'left',
		paddingTop: 10,
		paddingLeft: 5,
	},
	styleForCheckBox: {
		textAlign: 'right',
	},
	styleforDropDownText: {
		textAlign: "left",
		display: "block",
		marginLeft: 10,
		fontSize: 12,
		fontWeight: 400,
		color: "black",
		minWidth: 20,
	},
	datepicker: {
		width: "90%",
		height: 38,
		marginBottom: 0,
		fontSize: 12,
		borderRadius: 1,
	}
});
