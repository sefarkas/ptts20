﻿import { ECB603Action, ECB603Actions, ECB603 } from "../model";

function fAddECB603(oECB603: ECB603): ECB603Action {
	return {
		type: ECB603Actions.ADD_ECB603,
		payload: oECB603,
	};
}

function fUptECB603(oECB603: ECB603): ECB603Action {
	return {
		type: ECB603Actions.UPDATE_ECB603,
		payload: oECB603,
	};
}


// Async Function expample with redux-thunk
function fCompleteECB603(ECB603Id: number, dispatchRef: any) {
	// here you could do API eg

	return dispatchRef({ type: ECB603Actions.COMPLETE_ECB603, payload: ECB603Id });
}

function fUncompleteECB603(ECB603Id: number): ECB603Action {
	return {
		type: ECB603Actions.REVERT2INCOMPLETE_ECB603,
		payload: ECB603Id,
	};
}

function fECB603deleteTodo(ECB603Id: number): ECB603Action {
	return {
		type: ECB603Actions.DELETE_ECB603,
		payload: ECB603Id,
	};
}

export const allActions = {
	addECB603: fAddECB603,
	uptECB603: fUptECB603,
	completeECB603: fCompleteECB603,
	uncompleteECB603: fUncompleteECB603,
	ECB603deleteTodo: fECB603deleteTodo
}